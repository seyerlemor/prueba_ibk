/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.Cliente;

import java.util.List;

/**
 * The persistence utility for the cliente service. This utility wraps {@link ClientePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ClientePersistence
 * @see ClientePersistenceImpl
 * @generated
 */
public class ClienteUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Cliente cliente) {
		getPersistence().clearCache(cliente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Cliente> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Cliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Cliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Cliente update(Cliente cliente) throws SystemException {
		return getPersistence().update(cliente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Cliente update(Cliente cliente, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(cliente, serviceContext);
	}

	/**
	* Returns the cliente where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	*
	* @param idExpediente the id expediente
	* @return the matching cliente
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente findByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException {
		return getPersistence().findByC_E(idExpediente);
	}

	/**
	* Returns the cliente where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idExpediente the id expediente
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente fetchByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_E(idExpediente);
	}

	/**
	* Returns the cliente where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idExpediente the id expediente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente fetchByC_E(
		long idExpediente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_E(idExpediente, retrieveFromCache);
	}

	/**
	* Removes the cliente where idExpediente = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @return the cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente removeByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException {
		return getPersistence().removeByC_E(idExpediente);
	}

	/**
	* Returns the number of clientes where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the number of matching clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_E(idExpediente);
	}

	/**
	* Returns the cliente where idDatoCliente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	*
	* @param idDatoCliente the id dato cliente
	* @return the matching cliente
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente findByDC_ID(
		long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException {
		return getPersistence().findByDC_ID(idDatoCliente);
	}

	/**
	* Returns the cliente where idDatoCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idDatoCliente the id dato cliente
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente fetchByDC_ID(
		long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDC_ID(idDatoCliente);
	}

	/**
	* Returns the cliente where idDatoCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idDatoCliente the id dato cliente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente fetchByDC_ID(
		long idDatoCliente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDC_ID(idDatoCliente, retrieveFromCache);
	}

	/**
	* Removes the cliente where idDatoCliente = &#63; from the database.
	*
	* @param idDatoCliente the id dato cliente
	* @return the cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente removeByDC_ID(
		long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException {
		return getPersistence().removeByDC_ID(idDatoCliente);
	}

	/**
	* Returns the number of clientes where idDatoCliente = &#63;.
	*
	* @param idDatoCliente the id dato cliente
	* @return the number of matching clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDC_ID(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDC_ID(idDatoCliente);
	}

	/**
	* Caches the cliente in the entity cache if it is enabled.
	*
	* @param cliente the cliente
	*/
	public static void cacheResult(pe.com.ibk.pepper.model.Cliente cliente) {
		getPersistence().cacheResult(cliente);
	}

	/**
	* Caches the clientes in the entity cache if it is enabled.
	*
	* @param clientes the clientes
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Cliente> clientes) {
		getPersistence().cacheResult(clientes);
	}

	/**
	* Creates a new cliente with the primary key. Does not add the cliente to the database.
	*
	* @param idDatoCliente the primary key for the new cliente
	* @return the new cliente
	*/
	public static pe.com.ibk.pepper.model.Cliente create(long idDatoCliente) {
		return getPersistence().create(idDatoCliente);
	}

	/**
	* Removes the cliente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDatoCliente the primary key of the cliente
	* @return the cliente that was removed
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente remove(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException {
		return getPersistence().remove(idDatoCliente);
	}

	public static pe.com.ibk.pepper.model.Cliente updateImpl(
		pe.com.ibk.pepper.model.Cliente cliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(cliente);
	}

	/**
	* Returns the cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	*
	* @param idDatoCliente the primary key of the cliente
	* @return the cliente
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente findByPrimaryKey(
		long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException {
		return getPersistence().findByPrimaryKey(idDatoCliente);
	}

	/**
	* Returns the cliente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDatoCliente the primary key of the cliente
	* @return the cliente, or <code>null</code> if a cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Cliente fetchByPrimaryKey(
		long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idDatoCliente);
	}

	/**
	* Returns all the clientes.
	*
	* @return the clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Cliente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clientes
	* @param end the upper bound of the range of clientes (not inclusive)
	* @return the range of clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Cliente> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clientes
	* @param end the upper bound of the range of clientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Cliente> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the clientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of clientes.
	*
	* @return the number of clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ClientePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ClientePersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ClientePersistence.class.getName());

			ReferenceRegistry.registerReference(ClienteUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ClientePersistence persistence) {
	}

	private static ClientePersistence _persistence;
}