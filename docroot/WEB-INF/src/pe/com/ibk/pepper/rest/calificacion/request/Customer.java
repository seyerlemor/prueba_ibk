
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Customer {

    private Integer typeCustomer;
    private List<IdentityDocument> identityDocument;
    private List<Address> address;
    private String civilState;
    private String employmentSituation;
    
    public Customer() {
		super();
	}

	@JsonCreator
	public Customer(Integer typeCustomer,
			List<IdentityDocument> identityDocument, List<Address> address,
			String civilState, String employmentSituation) {
		super();
		this.typeCustomer = typeCustomer;
		this.identityDocument = identityDocument;
		this.address = address;
		this.civilState = civilState;
		this.employmentSituation = employmentSituation;
	}

    @JsonProperty("typeCustomer")
	public Integer getTypeCustomer() {
		return typeCustomer;
	}

	public void setTypeCustomer(Integer typeCustomer) {
		this.typeCustomer = typeCustomer;
	}

	@JsonProperty("identityDocument")
	public List<IdentityDocument> getIdentityDocument() {
		return identityDocument;
	}

	public void setIdentityDocument(List<IdentityDocument> identityDocument) {
		this.identityDocument = identityDocument;
	}

	@JsonProperty("address")
	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	@JsonProperty("civilState")
	public String getCivilState() {
		return civilState;
	}

	public void setCivilState(String civilState) {
		this.civilState = civilState;
	}

	@JsonProperty("employmentSituation")
	public String getEmploymentSituation() {
		return employmentSituation;
	}

	public void setEmploymentSituation(String employmentSituation) {
		this.employmentSituation = employmentSituation;
	}

}
