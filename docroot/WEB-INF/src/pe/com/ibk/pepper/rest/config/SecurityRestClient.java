package pe.com.ibk.pepper.rest.config;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import pe.com.ibk.pepper.util.Constantes;
import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;
import pe.com.ibk.pepper.util.Utils;

public class SecurityRestClient {
	private static final String SSL = "SSL";
	private static final String TLS = "TLSv1.2";

	private static Log logger = LogFactoryUtil.getLog(SecurityRestClient.class);

	private SecurityRestClient() {
		throw new IllegalAccessError("Clase Estática");
	}

	public static Builder init(String host, String path, String user,
			String password, String tipoServicio) {
		try {
			HostnameVerifier hostnameVerifier = new InsecureHostnameVerifier();
			Client client = ClientBuilder.newBuilder()
					.sslContext(getSSLContextCertified(tipoServicio))
					.hostnameVerifier(hostnameVerifier).build();
			HttpAuthenticationFeature feature = HttpAuthenticationFeature
					.basic(user, password);
			client.register(feature);
			WebTarget target = client.target(host).path(path);
			return target.request(MediaType.APPLICATION_JSON_TYPE);
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	/**
	 * Retorna el protocolo SSL del contexto actual
	 * 
	 * @return SSLContext Contexto con protocolo SSL
	 */
	public static SSLContext getSSLContextCertified(String tipoServicio) {
		SSLContext context = null;
		try {
			String resourceDir = PrefsPropsUtil.getString(
					PortalKeys.REST_UBICACION_JKS,
					PortalValues.REST_UBICACION_JKS);
			SslConfigurator sslConfig = null;
			if (tipoServicio.equals(ConstantesRest.SERVICIO_API)) {
				sslConfig = SslConfigurator
						.newInstance()
						.keyStoreFile(
								resourceDir
										+ File.separator
										+ PrefsPropsUtil.getString(
												PortalKeys.REST_API_JKS_NAME,
												PortalValues.REST_API_JKS_NAME))
						.keyPassword(
								PrefsPropsUtil.getString(
										PortalKeys.REST_API_JKS_PASSW,
										PortalValues.REST_API_JKS_PASSW))
						.keyStoreType("JKS").securityProtocol(SSL);
				context = sslConfig.createSSLContext();
			} else if (tipoServicio.equals(ConstantesRest.SERVICIO_BUS)) {
				sslConfig = SslConfigurator
						.newInstance()
						.keyStoreFile(
								resourceDir
										+ File.separator
										+ PrefsPropsUtil.getString(
												PortalKeys.REST_BUS_JKS_NAME,
												PortalValues.REST_BUS_JKS_NAME))
						.keyPassword(
								PrefsPropsUtil.getString(
										PortalKeys.REST_BUS_JKS_PASSW,
										PortalValues.REST_BUS_JKS_PASSW))
						.keyStoreType("JKS").securityProtocol(SSL);
				context = sslConfig.createSSLContext();
			}
		} catch (Exception e) {
			logger.error("Excepcion en el metodo getSSLContextCertified", e);
		}
		return context;
	}

	public static Builder initAzure(String host, String path, String user,
			String password, Map<String, String> queryParameters,
			Map<String, String> templates) {
		try {
			HostnameVerifier hostnameVerifier = new InsecureHostnameVerifier();
			Client client = ClientBuilder.newBuilder()
					.sslContext(getTrustAllSSLContextCertified())
					.hostnameVerifier(hostnameVerifier).build();
			if(Utils.validateBlankAndNull(user) && Utils.validateBlankAndNull(password)){
				HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(user, password);
				client.register(feature);
			}
			WebTarget target = client.target(host).path(path);
			if (queryParameters != null) {
				for (Entry<String, String> entry : queryParameters.entrySet()) {
					target = target
							.queryParam(entry.getKey(), entry.getValue());
				}
			}
			if (templates != null) {
				for (Entry<String, String> entry : templates.entrySet()) {
					target = target.resolveTemplate(entry.getKey(),
							entry.getValue());
				}
			}
			return target.request(Constantes.APPLICATION_JSON_TYPE_UTF8);
		} catch (Exception e) {
			logger.error(
					"Excepcion el la inicializacion de SecurityRestClient ", e);
		}
		return null;
	}

	public static SSLContext getTrustAllSSLContextCertified() {
		SSLContext context = null;
		try {
			context = SSLContext.getInstance(TLS);
			final TrustManager[] trustManagerArray = { new NullX509TrustManager() };
			context.init(null, trustManagerArray, null);
		} catch (Exception e) {
			logger.error(
					"Excepcion en el metodo getTrustAllSSLContextCertified", e);
		}
		return context;
	}
}
