/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.LogQRadar;

import java.util.List;

/**
 * The persistence utility for the log q radar service. This utility wraps {@link LogQRadarPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see LogQRadarPersistence
 * @see LogQRadarPersistenceImpl
 * @generated
 */
public class LogQRadarUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LogQRadar logQRadar) {
		getPersistence().clearCache(logQRadar);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LogQRadar> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LogQRadar> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LogQRadar> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static LogQRadar update(LogQRadar logQRadar)
		throws SystemException {
		return getPersistence().update(logQRadar);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static LogQRadar update(LogQRadar logQRadar,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(logQRadar, serviceContext);
	}

	/**
	* Caches the log q radar in the entity cache if it is enabled.
	*
	* @param logQRadar the log q radar
	*/
	public static void cacheResult(pe.com.ibk.pepper.model.LogQRadar logQRadar) {
		getPersistence().cacheResult(logQRadar);
	}

	/**
	* Caches the log q radars in the entity cache if it is enabled.
	*
	* @param logQRadars the log q radars
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.LogQRadar> logQRadars) {
		getPersistence().cacheResult(logQRadars);
	}

	/**
	* Creates a new log q radar with the primary key. Does not add the log q radar to the database.
	*
	* @param idLogQRadar the primary key for the new log q radar
	* @return the new log q radar
	*/
	public static pe.com.ibk.pepper.model.LogQRadar create(int idLogQRadar) {
		return getPersistence().create(idLogQRadar);
	}

	/**
	* Removes the log q radar with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idLogQRadar the primary key of the log q radar
	* @return the log q radar that was removed
	* @throws pe.com.ibk.pepper.NoSuchLogQRadarException if a log q radar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.LogQRadar remove(int idLogQRadar)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchLogQRadarException {
		return getPersistence().remove(idLogQRadar);
	}

	public static pe.com.ibk.pepper.model.LogQRadar updateImpl(
		pe.com.ibk.pepper.model.LogQRadar logQRadar)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(logQRadar);
	}

	/**
	* Returns the log q radar with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchLogQRadarException} if it could not be found.
	*
	* @param idLogQRadar the primary key of the log q radar
	* @return the log q radar
	* @throws pe.com.ibk.pepper.NoSuchLogQRadarException if a log q radar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.LogQRadar findByPrimaryKey(
		int idLogQRadar)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchLogQRadarException {
		return getPersistence().findByPrimaryKey(idLogQRadar);
	}

	/**
	* Returns the log q radar with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idLogQRadar the primary key of the log q radar
	* @return the log q radar, or <code>null</code> if a log q radar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.LogQRadar fetchByPrimaryKey(
		int idLogQRadar)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idLogQRadar);
	}

	/**
	* Returns all the log q radars.
	*
	* @return the log q radars
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.LogQRadar> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the log q radars.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.LogQRadarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of log q radars
	* @param end the upper bound of the range of log q radars (not inclusive)
	* @return the range of log q radars
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.LogQRadar> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the log q radars.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.LogQRadarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of log q radars
	* @param end the upper bound of the range of log q radars (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of log q radars
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.LogQRadar> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the log q radars from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of log q radars.
	*
	* @return the number of log q radars
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LogQRadarPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LogQRadarPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					LogQRadarPersistence.class.getName());

			ReferenceRegistry.registerReference(LogQRadarUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(LogQRadarPersistence persistence) {
	}

	private static LogQRadarPersistence _persistence;
}