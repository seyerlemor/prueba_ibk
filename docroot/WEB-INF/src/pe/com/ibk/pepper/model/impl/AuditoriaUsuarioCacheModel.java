/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.AuditoriaUsuario;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing AuditoriaUsuario in entity cache.
 *
 * @author Interbank
 * @see AuditoriaUsuario
 * @generated
 */
public class AuditoriaUsuarioCacheModel implements CacheModel<AuditoriaUsuario>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idAuditoriaUsuario=");
		sb.append(idAuditoriaUsuario);
		sb.append(", idUsuarioSession=");
		sb.append(idUsuarioSession);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AuditoriaUsuario toEntityModel() {
		AuditoriaUsuarioImpl auditoriaUsuarioImpl = new AuditoriaUsuarioImpl();

		auditoriaUsuarioImpl.setIdAuditoriaUsuario(idAuditoriaUsuario);
		auditoriaUsuarioImpl.setIdUsuarioSession(idUsuarioSession);

		if (fechaRegistro == Long.MIN_VALUE) {
			auditoriaUsuarioImpl.setFechaRegistro(null);
		}
		else {
			auditoriaUsuarioImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		auditoriaUsuarioImpl.resetOriginalValues();

		return auditoriaUsuarioImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idAuditoriaUsuario = objectInput.readLong();
		idUsuarioSession = objectInput.readLong();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idAuditoriaUsuario);
		objectOutput.writeLong(idUsuarioSession);
		objectOutput.writeLong(fechaRegistro);
	}

	public long idAuditoriaUsuario;
	public long idUsuarioSession;
	public long fechaRegistro;
}