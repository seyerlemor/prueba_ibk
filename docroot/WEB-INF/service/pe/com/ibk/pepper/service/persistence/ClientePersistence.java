/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.Cliente;

/**
 * The persistence interface for the cliente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ClientePersistenceImpl
 * @see ClienteUtil
 * @generated
 */
public interface ClientePersistence extends BasePersistence<Cliente> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ClienteUtil} to access the cliente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the cliente where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	*
	* @param idExpediente the id expediente
	* @return the matching cliente
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente findByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException;

	/**
	* Returns the cliente where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idExpediente the id expediente
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente fetchByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the cliente where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idExpediente the id expediente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente fetchByC_E(long idExpediente,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the cliente where idExpediente = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @return the cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente removeByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException;

	/**
	* Returns the number of clientes where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the number of matching clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countByC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the cliente where idDatoCliente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	*
	* @param idDatoCliente the id dato cliente
	* @return the matching cliente
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente findByDC_ID(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException;

	/**
	* Returns the cliente where idDatoCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idDatoCliente the id dato cliente
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente fetchByDC_ID(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the cliente where idDatoCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idDatoCliente the id dato cliente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente fetchByDC_ID(long idDatoCliente,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the cliente where idDatoCliente = &#63; from the database.
	*
	* @param idDatoCliente the id dato cliente
	* @return the cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente removeByDC_ID(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException;

	/**
	* Returns the number of clientes where idDatoCliente = &#63;.
	*
	* @param idDatoCliente the id dato cliente
	* @return the number of matching clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countByDC_ID(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the cliente in the entity cache if it is enabled.
	*
	* @param cliente the cliente
	*/
	public void cacheResult(pe.com.ibk.pepper.model.Cliente cliente);

	/**
	* Caches the clientes in the entity cache if it is enabled.
	*
	* @param clientes the clientes
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Cliente> clientes);

	/**
	* Creates a new cliente with the primary key. Does not add the cliente to the database.
	*
	* @param idDatoCliente the primary key for the new cliente
	* @return the new cliente
	*/
	public pe.com.ibk.pepper.model.Cliente create(long idDatoCliente);

	/**
	* Removes the cliente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDatoCliente the primary key of the cliente
	* @return the cliente that was removed
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente remove(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException;

	public pe.com.ibk.pepper.model.Cliente updateImpl(
		pe.com.ibk.pepper.model.Cliente cliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	*
	* @param idDatoCliente the primary key of the cliente
	* @return the cliente
	* @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente findByPrimaryKey(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteException;

	/**
	* Returns the cliente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDatoCliente the primary key of the cliente
	* @return the cliente, or <code>null</code> if a cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Cliente fetchByPrimaryKey(long idDatoCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the clientes.
	*
	* @return the clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Cliente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clientes
	* @param end the upper bound of the range of clientes (not inclusive)
	* @return the range of clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Cliente> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clientes
	* @param end the upper bound of the range of clientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Cliente> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the clientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of clientes.
	*
	* @return the number of clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}