/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.DirecEstandarLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class DirecEstandarClp extends BaseModelImpl<DirecEstandar>
	implements DirecEstandar {
	public DirecEstandarClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return DirecEstandar.class;
	}

	@Override
	public String getModelClassName() {
		return DirecEstandar.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idDetalleDireccion;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdDetalleDireccion(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idDetalleDireccion;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDetalleDireccion", getIdDetalleDireccion());
		attributes.put("idDireccion", getIdDireccion());
		attributes.put("tipoVia", getTipoVia());
		attributes.put("nombreVia", getNombreVia());
		attributes.put("numero", getNumero());
		attributes.put("manzana", getManzana());
		attributes.put("pisoLote", getPisoLote());
		attributes.put("interior", getInterior());
		attributes.put("urbanizacion", getUrbanizacion());
		attributes.put("referencia", getReferencia());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idDetalleDireccion = (Long)attributes.get("idDetalleDireccion");

		if (idDetalleDireccion != null) {
			setIdDetalleDireccion(idDetalleDireccion);
		}

		Long idDireccion = (Long)attributes.get("idDireccion");

		if (idDireccion != null) {
			setIdDireccion(idDireccion);
		}

		String tipoVia = (String)attributes.get("tipoVia");

		if (tipoVia != null) {
			setTipoVia(tipoVia);
		}

		String nombreVia = (String)attributes.get("nombreVia");

		if (nombreVia != null) {
			setNombreVia(nombreVia);
		}

		String numero = (String)attributes.get("numero");

		if (numero != null) {
			setNumero(numero);
		}

		String manzana = (String)attributes.get("manzana");

		if (manzana != null) {
			setManzana(manzana);
		}

		String pisoLote = (String)attributes.get("pisoLote");

		if (pisoLote != null) {
			setPisoLote(pisoLote);
		}

		String interior = (String)attributes.get("interior");

		if (interior != null) {
			setInterior(interior);
		}

		String urbanizacion = (String)attributes.get("urbanizacion");

		if (urbanizacion != null) {
			setUrbanizacion(urbanizacion);
		}

		String referencia = (String)attributes.get("referencia");

		if (referencia != null) {
			setReferencia(referencia);
		}
	}

	@Override
	public long getIdDetalleDireccion() {
		return _idDetalleDireccion;
	}

	@Override
	public void setIdDetalleDireccion(long idDetalleDireccion) {
		_idDetalleDireccion = idDetalleDireccion;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setIdDetalleDireccion",
						long.class);

				method.invoke(_direcEstandarRemoteModel, idDetalleDireccion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdDireccion() {
		return _idDireccion;
	}

	@Override
	public void setIdDireccion(long idDireccion) {
		_idDireccion = idDireccion;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setIdDireccion", long.class);

				method.invoke(_direcEstandarRemoteModel, idDireccion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoVia() {
		return _tipoVia;
	}

	@Override
	public void setTipoVia(String tipoVia) {
		_tipoVia = tipoVia;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoVia", String.class);

				method.invoke(_direcEstandarRemoteModel, tipoVia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreVia() {
		return _nombreVia;
	}

	@Override
	public void setNombreVia(String nombreVia) {
		_nombreVia = nombreVia;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreVia", String.class);

				method.invoke(_direcEstandarRemoteModel, nombreVia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNumero() {
		return _numero;
	}

	@Override
	public void setNumero(String numero) {
		_numero = numero;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setNumero", String.class);

				method.invoke(_direcEstandarRemoteModel, numero);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getManzana() {
		return _manzana;
	}

	@Override
	public void setManzana(String manzana) {
		_manzana = manzana;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setManzana", String.class);

				method.invoke(_direcEstandarRemoteModel, manzana);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPisoLote() {
		return _pisoLote;
	}

	@Override
	public void setPisoLote(String pisoLote) {
		_pisoLote = pisoLote;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setPisoLote", String.class);

				method.invoke(_direcEstandarRemoteModel, pisoLote);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getInterior() {
		return _interior;
	}

	@Override
	public void setInterior(String interior) {
		_interior = interior;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setInterior", String.class);

				method.invoke(_direcEstandarRemoteModel, interior);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrbanizacion() {
		return _urbanizacion;
	}

	@Override
	public void setUrbanizacion(String urbanizacion) {
		_urbanizacion = urbanizacion;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setUrbanizacion", String.class);

				method.invoke(_direcEstandarRemoteModel, urbanizacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getReferencia() {
		return _referencia;
	}

	@Override
	public void setReferencia(String referencia) {
		_referencia = referencia;

		if (_direcEstandarRemoteModel != null) {
			try {
				Class<?> clazz = _direcEstandarRemoteModel.getClass();

				Method method = clazz.getMethod("setReferencia", String.class);

				method.invoke(_direcEstandarRemoteModel, referencia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDirecEstandarRemoteModel() {
		return _direcEstandarRemoteModel;
	}

	public void setDirecEstandarRemoteModel(
		BaseModel<?> direcEstandarRemoteModel) {
		_direcEstandarRemoteModel = direcEstandarRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _direcEstandarRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_direcEstandarRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DirecEstandarLocalServiceUtil.addDirecEstandar(this);
		}
		else {
			DirecEstandarLocalServiceUtil.updateDirecEstandar(this);
		}
	}

	@Override
	public DirecEstandar toEscapedModel() {
		return (DirecEstandar)ProxyUtil.newProxyInstance(DirecEstandar.class.getClassLoader(),
			new Class[] { DirecEstandar.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DirecEstandarClp clone = new DirecEstandarClp();

		clone.setIdDetalleDireccion(getIdDetalleDireccion());
		clone.setIdDireccion(getIdDireccion());
		clone.setTipoVia(getTipoVia());
		clone.setNombreVia(getNombreVia());
		clone.setNumero(getNumero());
		clone.setManzana(getManzana());
		clone.setPisoLote(getPisoLote());
		clone.setInterior(getInterior());
		clone.setUrbanizacion(getUrbanizacion());
		clone.setReferencia(getReferencia());

		return clone;
	}

	@Override
	public int compareTo(DirecEstandar direcEstandar) {
		long primaryKey = direcEstandar.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DirecEstandarClp)) {
			return false;
		}

		DirecEstandarClp direcEstandar = (DirecEstandarClp)obj;

		long primaryKey = direcEstandar.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{idDetalleDireccion=");
		sb.append(getIdDetalleDireccion());
		sb.append(", idDireccion=");
		sb.append(getIdDireccion());
		sb.append(", tipoVia=");
		sb.append(getTipoVia());
		sb.append(", nombreVia=");
		sb.append(getNombreVia());
		sb.append(", numero=");
		sb.append(getNumero());
		sb.append(", manzana=");
		sb.append(getManzana());
		sb.append(", pisoLote=");
		sb.append(getPisoLote());
		sb.append(", interior=");
		sb.append(getInterior());
		sb.append(", urbanizacion=");
		sb.append(getUrbanizacion());
		sb.append(", referencia=");
		sb.append(getReferencia());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.DirecEstandar");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idDetalleDireccion</column-name><column-value><![CDATA[");
		sb.append(getIdDetalleDireccion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idDireccion</column-name><column-value><![CDATA[");
		sb.append(getIdDireccion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoVia</column-name><column-value><![CDATA[");
		sb.append(getTipoVia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreVia</column-name><column-value><![CDATA[");
		sb.append(getNombreVia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numero</column-name><column-value><![CDATA[");
		sb.append(getNumero());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>manzana</column-name><column-value><![CDATA[");
		sb.append(getManzana());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pisoLote</column-name><column-value><![CDATA[");
		sb.append(getPisoLote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>interior</column-name><column-value><![CDATA[");
		sb.append(getInterior());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>urbanizacion</column-name><column-value><![CDATA[");
		sb.append(getUrbanizacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>referencia</column-name><column-value><![CDATA[");
		sb.append(getReferencia());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idDetalleDireccion;
	private long _idDireccion;
	private String _tipoVia;
	private String _nombreVia;
	private String _numero;
	private String _manzana;
	private String _pisoLote;
	private String _interior;
	private String _urbanizacion;
	private String _referencia;
	private BaseModel<?> _direcEstandarRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}