package pe.com.ibk.pepper.bean;

public class FormConyugeBean {

	String firstname;
	String secondname;
	String paternalsurname;
	String maternalsurname;
	String birthdate;
	
	
	public FormConyugeBean(String firstname, String secondname,
			String paternalsurname, String maternalsurname, String birthdate) {
		super();
		this.firstname = firstname;
		this.secondname = secondname;
		this.paternalsurname = paternalsurname;
		this.maternalsurname = maternalsurname;
		this.birthdate = birthdate;
	}

	public FormConyugeBean(){
		
	}
	@Override
	public String toString() {
		return "firstname=" + firstname + ", secondname="
				+ secondname + ", paternalsurname=" + paternalsurname
				+ ", maternalsurname=" + maternalsurname + ", birthdate="
				+ birthdate;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSecondname() {
		return secondname;
	}

	public void setSecondname(String secondname) {
		this.secondname = secondname;
	}

	public String getPaternalsurname() {
		return paternalsurname;
	}

	public void setPaternalsurname(String paternalsurname) {
		this.paternalsurname = paternalsurname;
	}

	public String getMaternalsurname() {
		return maternalsurname;
	}

	public void setMaternalsurname(String maternalsurname) {
		this.maternalsurname = maternalsurname;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	
	
}
