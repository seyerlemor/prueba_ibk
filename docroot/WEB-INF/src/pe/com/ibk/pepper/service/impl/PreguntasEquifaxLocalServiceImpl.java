/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;

import pe.com.ibk.pepper.NoSuchPreguntasEquifaxException;
import pe.com.ibk.pepper.model.PreguntasEquifax;
import pe.com.ibk.pepper.service.base.PreguntasEquifaxLocalServiceBaseImpl;

/**
 * The implementation of the preguntas equifax local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.PreguntasEquifaxLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.PreguntasEquifaxLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.PreguntasEquifaxLocalServiceUtil
 */
public class PreguntasEquifaxLocalServiceImpl
	extends PreguntasEquifaxLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.PreguntasEquifaxLocalServiceUtil} to access the preguntas equifax local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(PreguntasEquifaxLocalServiceImpl.class);
	
	@Override
	public PreguntasEquifax registrarPreguntasEquifax(PreguntasEquifax preguntasEquifax) throws SystemException{
		Date now = DateUtil.newDate();
		if (preguntasEquifax.isNew()) {
			preguntasEquifax.setFechaRegistro(now);
		}
		preguntasEquifaxPersistence.update(preguntasEquifax);
		return preguntasEquifax;
	}	

	@Override
	public PreguntasEquifax buscarPreguntasEquifaxporId(long idPreguntasEquifax){
		PreguntasEquifax preguntasEquifax = null;
		try {
			preguntasEquifax = preguntasEquifaxPersistence.findByPE_ID(idPreguntasEquifax);
		} catch (NoSuchPreguntasEquifaxException e) {
			_log.error("Error en clase PreguntasEquifaxServiceImpl metodo buscarPreguntasEquifaxporId:: NO EXISTE EL USUARIOSESSION ", e );
		} catch (SystemException e) {
			_log.error("Error en clase PreguntasEquifaxServiceImpl metodo buscarPreguntasEquifaxporId:: ", e );
		}
		return preguntasEquifax;
	}		
		
	
	
}