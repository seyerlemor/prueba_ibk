/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClienteLocalServiceUtil;
import pe.com.ibk.pepper.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ClienteClp extends BaseModelImpl<Cliente> implements Cliente {
	public ClienteClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Cliente.class;
	}

	@Override
	public String getModelClassName() {
		return Cliente.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idDatoCliente;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdDatoCliente(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idDatoCliente;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDatoCliente", getIdDatoCliente());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("primerNombre", getPrimerNombre());
		attributes.put("segundoNombre", getSegundoNombre());
		attributes.put("apellidoPaterno", getApellidoPaterno());
		attributes.put("apellidoMaterno", getApellidoMaterno());
		attributes.put("sexo", getSexo());
		attributes.put("correo", getCorreo());
		attributes.put("telefono", getTelefono());
		attributes.put("estadoCivil", getEstadoCivil());
		attributes.put("situacionLaboral", getSituacionLaboral());
		attributes.put("rucEmpresaTrabajo", getRucEmpresaTrabajo());
		attributes.put("nivelEducacion", getNivelEducacion());
		attributes.put("ocupacion", getOcupacion());
		attributes.put("cargoActual", getCargoActual());
		attributes.put("actividadNegocio", getActividadNegocio());
		attributes.put("antiguedadLaboral", getAntiguedadLaboral());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("porcentajeParticipacion", getPorcentajeParticipacion());
		attributes.put("ingresoMensualFijo", getIngresoMensualFijo());
		attributes.put("ingresoMensualVariable", getIngresoMensualVariable());
		attributes.put("documento", getDocumento());
		attributes.put("terminosCondiciones", getTerminosCondiciones());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("flagPEP", getFlagPEP());
		attributes.put("fechaNacimiento", getFechaNacimiento());
		attributes.put("operador", getOperador());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idDatoCliente = (Long)attributes.get("idDatoCliente");

		if (idDatoCliente != null) {
			setIdDatoCliente(idDatoCliente);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String primerNombre = (String)attributes.get("primerNombre");

		if (primerNombre != null) {
			setPrimerNombre(primerNombre);
		}

		String segundoNombre = (String)attributes.get("segundoNombre");

		if (segundoNombre != null) {
			setSegundoNombre(segundoNombre);
		}

		String apellidoPaterno = (String)attributes.get("apellidoPaterno");

		if (apellidoPaterno != null) {
			setApellidoPaterno(apellidoPaterno);
		}

		String apellidoMaterno = (String)attributes.get("apellidoMaterno");

		if (apellidoMaterno != null) {
			setApellidoMaterno(apellidoMaterno);
		}

		String sexo = (String)attributes.get("sexo");

		if (sexo != null) {
			setSexo(sexo);
		}

		String correo = (String)attributes.get("correo");

		if (correo != null) {
			setCorreo(correo);
		}

		String telefono = (String)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}

		String estadoCivil = (String)attributes.get("estadoCivil");

		if (estadoCivil != null) {
			setEstadoCivil(estadoCivil);
		}

		String situacionLaboral = (String)attributes.get("situacionLaboral");

		if (situacionLaboral != null) {
			setSituacionLaboral(situacionLaboral);
		}

		String rucEmpresaTrabajo = (String)attributes.get("rucEmpresaTrabajo");

		if (rucEmpresaTrabajo != null) {
			setRucEmpresaTrabajo(rucEmpresaTrabajo);
		}

		String nivelEducacion = (String)attributes.get("nivelEducacion");

		if (nivelEducacion != null) {
			setNivelEducacion(nivelEducacion);
		}

		String ocupacion = (String)attributes.get("ocupacion");

		if (ocupacion != null) {
			setOcupacion(ocupacion);
		}

		String cargoActual = (String)attributes.get("cargoActual");

		if (cargoActual != null) {
			setCargoActual(cargoActual);
		}

		String actividadNegocio = (String)attributes.get("actividadNegocio");

		if (actividadNegocio != null) {
			setActividadNegocio(actividadNegocio);
		}

		String antiguedadLaboral = (String)attributes.get("antiguedadLaboral");

		if (antiguedadLaboral != null) {
			setAntiguedadLaboral(antiguedadLaboral);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String porcentajeParticipacion = (String)attributes.get(
				"porcentajeParticipacion");

		if (porcentajeParticipacion != null) {
			setPorcentajeParticipacion(porcentajeParticipacion);
		}

		String ingresoMensualFijo = (String)attributes.get("ingresoMensualFijo");

		if (ingresoMensualFijo != null) {
			setIngresoMensualFijo(ingresoMensualFijo);
		}

		String ingresoMensualVariable = (String)attributes.get(
				"ingresoMensualVariable");

		if (ingresoMensualVariable != null) {
			setIngresoMensualVariable(ingresoMensualVariable);
		}

		String documento = (String)attributes.get("documento");

		if (documento != null) {
			setDocumento(documento);
		}

		String terminosCondiciones = (String)attributes.get(
				"terminosCondiciones");

		if (terminosCondiciones != null) {
			setTerminosCondiciones(terminosCondiciones);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		String flagPEP = (String)attributes.get("flagPEP");

		if (flagPEP != null) {
			setFlagPEP(flagPEP);
		}

		Date fechaNacimiento = (Date)attributes.get("fechaNacimiento");

		if (fechaNacimiento != null) {
			setFechaNacimiento(fechaNacimiento);
		}

		String operador = (String)attributes.get("operador");

		if (operador != null) {
			setOperador(operador);
		}
	}

	@Override
	public long getIdDatoCliente() {
		return _idDatoCliente;
	}

	@Override
	public void setIdDatoCliente(long idDatoCliente) {
		_idDatoCliente = idDatoCliente;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdDatoCliente", long.class);

				method.invoke(_clienteRemoteModel, idDatoCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdExpediente", long.class);

				method.invoke(_clienteRemoteModel, idExpediente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPrimerNombre() {
		return _primerNombre;
	}

	@Override
	public void setPrimerNombre(String primerNombre) {
		_primerNombre = primerNombre;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPrimerNombre", String.class);

				method.invoke(_clienteRemoteModel, primerNombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSegundoNombre() {
		return _segundoNombre;
	}

	@Override
	public void setSegundoNombre(String segundoNombre) {
		_segundoNombre = segundoNombre;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setSegundoNombre", String.class);

				method.invoke(_clienteRemoteModel, segundoNombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getApellidoPaterno() {
		return _apellidoPaterno;
	}

	@Override
	public void setApellidoPaterno(String apellidoPaterno) {
		_apellidoPaterno = apellidoPaterno;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setApellidoPaterno",
						String.class);

				method.invoke(_clienteRemoteModel, apellidoPaterno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getApellidoMaterno() {
		return _apellidoMaterno;
	}

	@Override
	public void setApellidoMaterno(String apellidoMaterno) {
		_apellidoMaterno = apellidoMaterno;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setApellidoMaterno",
						String.class);

				method.invoke(_clienteRemoteModel, apellidoMaterno);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSexo() {
		return _sexo;
	}

	@Override
	public void setSexo(String sexo) {
		_sexo = sexo;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setSexo", String.class);

				method.invoke(_clienteRemoteModel, sexo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCorreo() {
		return _correo;
	}

	@Override
	public void setCorreo(String correo) {
		_correo = correo;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setCorreo", String.class);

				method.invoke(_clienteRemoteModel, correo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTelefono() {
		return _telefono;
	}

	@Override
	public void setTelefono(String telefono) {
		_telefono = telefono;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTelefono", String.class);

				method.invoke(_clienteRemoteModel, telefono);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstadoCivil() {
		return _estadoCivil;
	}

	@Override
	public void setEstadoCivil(String estadoCivil) {
		_estadoCivil = estadoCivil;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setEstadoCivil", String.class);

				method.invoke(_clienteRemoteModel, estadoCivil);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSituacionLaboral() {
		return _situacionLaboral;
	}

	@Override
	public void setSituacionLaboral(String situacionLaboral) {
		_situacionLaboral = situacionLaboral;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setSituacionLaboral",
						String.class);

				method.invoke(_clienteRemoteModel, situacionLaboral);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRucEmpresaTrabajo() {
		return _rucEmpresaTrabajo;
	}

	@Override
	public void setRucEmpresaTrabajo(String rucEmpresaTrabajo) {
		_rucEmpresaTrabajo = rucEmpresaTrabajo;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setRucEmpresaTrabajo",
						String.class);

				method.invoke(_clienteRemoteModel, rucEmpresaTrabajo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNivelEducacion() {
		return _nivelEducacion;
	}

	@Override
	public void setNivelEducacion(String nivelEducacion) {
		_nivelEducacion = nivelEducacion;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setNivelEducacion",
						String.class);

				method.invoke(_clienteRemoteModel, nivelEducacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOcupacion() {
		return _ocupacion;
	}

	@Override
	public void setOcupacion(String ocupacion) {
		_ocupacion = ocupacion;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setOcupacion", String.class);

				method.invoke(_clienteRemoteModel, ocupacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCargoActual() {
		return _cargoActual;
	}

	@Override
	public void setCargoActual(String cargoActual) {
		_cargoActual = cargoActual;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setCargoActual", String.class);

				method.invoke(_clienteRemoteModel, cargoActual);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActividadNegocio() {
		return _actividadNegocio;
	}

	@Override
	public void setActividadNegocio(String actividadNegocio) {
		_actividadNegocio = actividadNegocio;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setActividadNegocio",
						String.class);

				method.invoke(_clienteRemoteModel, actividadNegocio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAntiguedadLaboral() {
		return _antiguedadLaboral;
	}

	@Override
	public void setAntiguedadLaboral(String antiguedadLaboral) {
		_antiguedadLaboral = antiguedadLaboral;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setAntiguedadLaboral",
						String.class);

				method.invoke(_clienteRemoteModel, antiguedadLaboral);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_clienteRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPorcentajeParticipacion() {
		return _porcentajeParticipacion;
	}

	@Override
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		_porcentajeParticipacion = porcentajeParticipacion;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPorcentajeParticipacion",
						String.class);

				method.invoke(_clienteRemoteModel, porcentajeParticipacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIngresoMensualFijo() {
		return _ingresoMensualFijo;
	}

	@Override
	public void setIngresoMensualFijo(String ingresoMensualFijo) {
		_ingresoMensualFijo = ingresoMensualFijo;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIngresoMensualFijo",
						String.class);

				method.invoke(_clienteRemoteModel, ingresoMensualFijo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIngresoMensualVariable() {
		return _ingresoMensualVariable;
	}

	@Override
	public void setIngresoMensualVariable(String ingresoMensualVariable) {
		_ingresoMensualVariable = ingresoMensualVariable;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIngresoMensualVariable",
						String.class);

				method.invoke(_clienteRemoteModel, ingresoMensualVariable);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDocumento() {
		return _documento;
	}

	@Override
	public void setDocumento(String documento) {
		_documento = documento;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setDocumento", String.class);

				method.invoke(_clienteRemoteModel, documento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTerminosCondiciones() {
		return _terminosCondiciones;
	}

	@Override
	public void setTerminosCondiciones(String terminosCondiciones) {
		_terminosCondiciones = terminosCondiciones;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTerminosCondiciones",
						String.class);

				method.invoke(_clienteRemoteModel, terminosCondiciones);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	@Override
	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDocumento", String.class);

				method.invoke(_clienteRemoteModel, tipoDocumento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagPEP() {
		return _flagPEP;
	}

	@Override
	public void setFlagPEP(String flagPEP) {
		_flagPEP = flagPEP;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagPEP", String.class);

				method.invoke(_clienteRemoteModel, flagPEP);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaNacimiento() {
		return _fechaNacimiento;
	}

	@Override
	public void setFechaNacimiento(Date fechaNacimiento) {
		_fechaNacimiento = fechaNacimiento;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaNacimiento", Date.class);

				method.invoke(_clienteRemoteModel, fechaNacimiento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOperador() {
		return _operador;
	}

	@Override
	public void setOperador(String operador) {
		_operador = operador;

		if (_clienteRemoteModel != null) {
			try {
				Class<?> clazz = _clienteRemoteModel.getClass();

				Method method = clazz.getMethod("setOperador", String.class);

				method.invoke(_clienteRemoteModel, operador);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getClienteRemoteModel() {
		return _clienteRemoteModel;
	}

	public void setClienteRemoteModel(BaseModel<?> clienteRemoteModel) {
		_clienteRemoteModel = clienteRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clienteRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clienteRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ClienteLocalServiceUtil.addCliente(this);
		}
		else {
			ClienteLocalServiceUtil.updateCliente(this);
		}
	}

	@Override
	public Cliente toEscapedModel() {
		return (Cliente)ProxyUtil.newProxyInstance(Cliente.class.getClassLoader(),
			new Class[] { Cliente.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ClienteClp clone = new ClienteClp();

		clone.setIdDatoCliente(getIdDatoCliente());
		clone.setIdExpediente(getIdExpediente());
		clone.setPrimerNombre(getPrimerNombre());
		clone.setSegundoNombre(getSegundoNombre());
		clone.setApellidoPaterno(getApellidoPaterno());
		clone.setApellidoMaterno(getApellidoMaterno());
		clone.setSexo(getSexo());
		clone.setCorreo(getCorreo());
		clone.setTelefono(getTelefono());
		clone.setEstadoCivil(getEstadoCivil());
		clone.setSituacionLaboral(getSituacionLaboral());
		clone.setRucEmpresaTrabajo(getRucEmpresaTrabajo());
		clone.setNivelEducacion(getNivelEducacion());
		clone.setOcupacion(getOcupacion());
		clone.setCargoActual(getCargoActual());
		clone.setActividadNegocio(getActividadNegocio());
		clone.setAntiguedadLaboral(getAntiguedadLaboral());
		clone.setFechaRegistro(getFechaRegistro());
		clone.setPorcentajeParticipacion(getPorcentajeParticipacion());
		clone.setIngresoMensualFijo(getIngresoMensualFijo());
		clone.setIngresoMensualVariable(getIngresoMensualVariable());
		clone.setDocumento(getDocumento());
		clone.setTerminosCondiciones(getTerminosCondiciones());
		clone.setTipoDocumento(getTipoDocumento());
		clone.setFlagPEP(getFlagPEP());
		clone.setFechaNacimiento(getFechaNacimiento());
		clone.setOperador(getOperador());

		return clone;
	}

	@Override
	public int compareTo(Cliente cliente) {
		long primaryKey = cliente.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClienteClp)) {
			return false;
		}

		ClienteClp cliente = (ClienteClp)obj;

		long primaryKey = cliente.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(55);

		sb.append("{idDatoCliente=");
		sb.append(getIdDatoCliente());
		sb.append(", idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", primerNombre=");
		sb.append(getPrimerNombre());
		sb.append(", segundoNombre=");
		sb.append(getSegundoNombre());
		sb.append(", apellidoPaterno=");
		sb.append(getApellidoPaterno());
		sb.append(", apellidoMaterno=");
		sb.append(getApellidoMaterno());
		sb.append(", sexo=");
		sb.append(getSexo());
		sb.append(", correo=");
		sb.append(getCorreo());
		sb.append(", telefono=");
		sb.append(getTelefono());
		sb.append(", estadoCivil=");
		sb.append(getEstadoCivil());
		sb.append(", situacionLaboral=");
		sb.append(getSituacionLaboral());
		sb.append(", rucEmpresaTrabajo=");
		sb.append(getRucEmpresaTrabajo());
		sb.append(", nivelEducacion=");
		sb.append(getNivelEducacion());
		sb.append(", ocupacion=");
		sb.append(getOcupacion());
		sb.append(", cargoActual=");
		sb.append(getCargoActual());
		sb.append(", actividadNegocio=");
		sb.append(getActividadNegocio());
		sb.append(", antiguedadLaboral=");
		sb.append(getAntiguedadLaboral());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append(", porcentajeParticipacion=");
		sb.append(getPorcentajeParticipacion());
		sb.append(", ingresoMensualFijo=");
		sb.append(getIngresoMensualFijo());
		sb.append(", ingresoMensualVariable=");
		sb.append(getIngresoMensualVariable());
		sb.append(", documento=");
		sb.append(getDocumento());
		sb.append(", terminosCondiciones=");
		sb.append(getTerminosCondiciones());
		sb.append(", tipoDocumento=");
		sb.append(getTipoDocumento());
		sb.append(", flagPEP=");
		sb.append(getFlagPEP());
		sb.append(", fechaNacimiento=");
		sb.append(getFechaNacimiento());
		sb.append(", operador=");
		sb.append(getOperador());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(85);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Cliente");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idDatoCliente</column-name><column-value><![CDATA[");
		sb.append(getIdDatoCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>primerNombre</column-name><column-value><![CDATA[");
		sb.append(getPrimerNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>segundoNombre</column-name><column-value><![CDATA[");
		sb.append(getSegundoNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>apellidoPaterno</column-name><column-value><![CDATA[");
		sb.append(getApellidoPaterno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>apellidoMaterno</column-name><column-value><![CDATA[");
		sb.append(getApellidoMaterno());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sexo</column-name><column-value><![CDATA[");
		sb.append(getSexo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>correo</column-name><column-value><![CDATA[");
		sb.append(getCorreo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>telefono</column-name><column-value><![CDATA[");
		sb.append(getTelefono());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estadoCivil</column-name><column-value><![CDATA[");
		sb.append(getEstadoCivil());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>situacionLaboral</column-name><column-value><![CDATA[");
		sb.append(getSituacionLaboral());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rucEmpresaTrabajo</column-name><column-value><![CDATA[");
		sb.append(getRucEmpresaTrabajo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nivelEducacion</column-name><column-value><![CDATA[");
		sb.append(getNivelEducacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ocupacion</column-name><column-value><![CDATA[");
		sb.append(getOcupacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cargoActual</column-name><column-value><![CDATA[");
		sb.append(getCargoActual());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>actividadNegocio</column-name><column-value><![CDATA[");
		sb.append(getActividadNegocio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>antiguedadLaboral</column-name><column-value><![CDATA[");
		sb.append(getAntiguedadLaboral());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>porcentajeParticipacion</column-name><column-value><![CDATA[");
		sb.append(getPorcentajeParticipacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ingresoMensualFijo</column-name><column-value><![CDATA[");
		sb.append(getIngresoMensualFijo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ingresoMensualVariable</column-name><column-value><![CDATA[");
		sb.append(getIngresoMensualVariable());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>documento</column-name><column-value><![CDATA[");
		sb.append(getDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>terminosCondiciones</column-name><column-value><![CDATA[");
		sb.append(getTerminosCondiciones());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDocumento</column-name><column-value><![CDATA[");
		sb.append(getTipoDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagPEP</column-name><column-value><![CDATA[");
		sb.append(getFlagPEP());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaNacimiento</column-name><column-value><![CDATA[");
		sb.append(getFechaNacimiento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>operador</column-name><column-value><![CDATA[");
		sb.append(getOperador());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idDatoCliente;
	private long _idExpediente;
	private String _primerNombre;
	private String _segundoNombre;
	private String _apellidoPaterno;
	private String _apellidoMaterno;
	private String _sexo;
	private String _correo;
	private String _telefono;
	private String _estadoCivil;
	private String _situacionLaboral;
	private String _rucEmpresaTrabajo;
	private String _nivelEducacion;
	private String _ocupacion;
	private String _cargoActual;
	private String _actividadNegocio;
	private String _antiguedadLaboral;
	private Date _fechaRegistro;
	private String _porcentajeParticipacion;
	private String _ingresoMensualFijo;
	private String _ingresoMensualVariable;
	private String _documento;
	private String _terminosCondiciones;
	private String _tipoDocumento;
	private String _flagPEP;
	private Date _fechaNacimiento;
	private String _operador;
	private BaseModel<?> _clienteRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}