/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LogQRadar}.
 * </p>
 *
 * @author Interbank
 * @see LogQRadar
 * @generated
 */
public class LogQRadarWrapper implements LogQRadar, ModelWrapper<LogQRadar> {
	public LogQRadarWrapper(LogQRadar logQRadar) {
		_logQRadar = logQRadar;
	}

	@Override
	public Class<?> getModelClass() {
		return LogQRadar.class;
	}

	@Override
	public String getModelClassName() {
		return LogQRadar.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idLogQRadar", getIdLogQRadar());
		attributes.put("prioridad", getPrioridad());
		attributes.put("severidad", getSeveridad());
		attributes.put("recurso", getRecurso());
		attributes.put("utc", getUtc());
		attributes.put("logCreador", getLogCreador());
		attributes.put("tipoEvento", getTipoEvento());
		attributes.put("registroUsuario", getRegistroUsuario());
		attributes.put("origenEvento", getOrigenEvento());
		attributes.put("respuesta", getRespuesta());
		attributes.put("ambiente", getAmbiente());
		attributes.put("producto", getProducto());
		attributes.put("idProducto", getIdProducto());
		attributes.put("fechaHora", getFechaHora());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idLogQRadar = (Integer)attributes.get("idLogQRadar");

		if (idLogQRadar != null) {
			setIdLogQRadar(idLogQRadar);
		}

		Integer prioridad = (Integer)attributes.get("prioridad");

		if (prioridad != null) {
			setPrioridad(prioridad);
		}

		Integer severidad = (Integer)attributes.get("severidad");

		if (severidad != null) {
			setSeveridad(severidad);
		}

		Integer recurso = (Integer)attributes.get("recurso");

		if (recurso != null) {
			setRecurso(recurso);
		}

		Date utc = (Date)attributes.get("utc");

		if (utc != null) {
			setUtc(utc);
		}

		String logCreador = (String)attributes.get("logCreador");

		if (logCreador != null) {
			setLogCreador(logCreador);
		}

		String tipoEvento = (String)attributes.get("tipoEvento");

		if (tipoEvento != null) {
			setTipoEvento(tipoEvento);
		}

		String registroUsuario = (String)attributes.get("registroUsuario");

		if (registroUsuario != null) {
			setRegistroUsuario(registroUsuario);
		}

		String origenEvento = (String)attributes.get("origenEvento");

		if (origenEvento != null) {
			setOrigenEvento(origenEvento);
		}

		String respuesta = (String)attributes.get("respuesta");

		if (respuesta != null) {
			setRespuesta(respuesta);
		}

		String ambiente = (String)attributes.get("ambiente");

		if (ambiente != null) {
			setAmbiente(ambiente);
		}

		String producto = (String)attributes.get("producto");

		if (producto != null) {
			setProducto(producto);
		}

		Integer idProducto = (Integer)attributes.get("idProducto");

		if (idProducto != null) {
			setIdProducto(idProducto);
		}

		Date fechaHora = (Date)attributes.get("fechaHora");

		if (fechaHora != null) {
			setFechaHora(fechaHora);
		}
	}

	/**
	* Returns the primary key of this log q radar.
	*
	* @return the primary key of this log q radar
	*/
	@Override
	public int getPrimaryKey() {
		return _logQRadar.getPrimaryKey();
	}

	/**
	* Sets the primary key of this log q radar.
	*
	* @param primaryKey the primary key of this log q radar
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_logQRadar.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id log q radar of this log q radar.
	*
	* @return the id log q radar of this log q radar
	*/
	@Override
	public int getIdLogQRadar() {
		return _logQRadar.getIdLogQRadar();
	}

	/**
	* Sets the id log q radar of this log q radar.
	*
	* @param idLogQRadar the id log q radar of this log q radar
	*/
	@Override
	public void setIdLogQRadar(int idLogQRadar) {
		_logQRadar.setIdLogQRadar(idLogQRadar);
	}

	/**
	* Returns the prioridad of this log q radar.
	*
	* @return the prioridad of this log q radar
	*/
	@Override
	public int getPrioridad() {
		return _logQRadar.getPrioridad();
	}

	/**
	* Sets the prioridad of this log q radar.
	*
	* @param prioridad the prioridad of this log q radar
	*/
	@Override
	public void setPrioridad(int prioridad) {
		_logQRadar.setPrioridad(prioridad);
	}

	/**
	* Returns the severidad of this log q radar.
	*
	* @return the severidad of this log q radar
	*/
	@Override
	public int getSeveridad() {
		return _logQRadar.getSeveridad();
	}

	/**
	* Sets the severidad of this log q radar.
	*
	* @param severidad the severidad of this log q radar
	*/
	@Override
	public void setSeveridad(int severidad) {
		_logQRadar.setSeveridad(severidad);
	}

	/**
	* Returns the recurso of this log q radar.
	*
	* @return the recurso of this log q radar
	*/
	@Override
	public int getRecurso() {
		return _logQRadar.getRecurso();
	}

	/**
	* Sets the recurso of this log q radar.
	*
	* @param recurso the recurso of this log q radar
	*/
	@Override
	public void setRecurso(int recurso) {
		_logQRadar.setRecurso(recurso);
	}

	/**
	* Returns the utc of this log q radar.
	*
	* @return the utc of this log q radar
	*/
	@Override
	public java.util.Date getUtc() {
		return _logQRadar.getUtc();
	}

	/**
	* Sets the utc of this log q radar.
	*
	* @param utc the utc of this log q radar
	*/
	@Override
	public void setUtc(java.util.Date utc) {
		_logQRadar.setUtc(utc);
	}

	/**
	* Returns the log creador of this log q radar.
	*
	* @return the log creador of this log q radar
	*/
	@Override
	public java.lang.String getLogCreador() {
		return _logQRadar.getLogCreador();
	}

	/**
	* Sets the log creador of this log q radar.
	*
	* @param logCreador the log creador of this log q radar
	*/
	@Override
	public void setLogCreador(java.lang.String logCreador) {
		_logQRadar.setLogCreador(logCreador);
	}

	/**
	* Returns the tipo evento of this log q radar.
	*
	* @return the tipo evento of this log q radar
	*/
	@Override
	public java.lang.String getTipoEvento() {
		return _logQRadar.getTipoEvento();
	}

	/**
	* Sets the tipo evento of this log q radar.
	*
	* @param tipoEvento the tipo evento of this log q radar
	*/
	@Override
	public void setTipoEvento(java.lang.String tipoEvento) {
		_logQRadar.setTipoEvento(tipoEvento);
	}

	/**
	* Returns the registro usuario of this log q radar.
	*
	* @return the registro usuario of this log q radar
	*/
	@Override
	public java.lang.String getRegistroUsuario() {
		return _logQRadar.getRegistroUsuario();
	}

	/**
	* Sets the registro usuario of this log q radar.
	*
	* @param registroUsuario the registro usuario of this log q radar
	*/
	@Override
	public void setRegistroUsuario(java.lang.String registroUsuario) {
		_logQRadar.setRegistroUsuario(registroUsuario);
	}

	/**
	* Returns the origen evento of this log q radar.
	*
	* @return the origen evento of this log q radar
	*/
	@Override
	public java.lang.String getOrigenEvento() {
		return _logQRadar.getOrigenEvento();
	}

	/**
	* Sets the origen evento of this log q radar.
	*
	* @param origenEvento the origen evento of this log q radar
	*/
	@Override
	public void setOrigenEvento(java.lang.String origenEvento) {
		_logQRadar.setOrigenEvento(origenEvento);
	}

	/**
	* Returns the respuesta of this log q radar.
	*
	* @return the respuesta of this log q radar
	*/
	@Override
	public java.lang.String getRespuesta() {
		return _logQRadar.getRespuesta();
	}

	/**
	* Sets the respuesta of this log q radar.
	*
	* @param respuesta the respuesta of this log q radar
	*/
	@Override
	public void setRespuesta(java.lang.String respuesta) {
		_logQRadar.setRespuesta(respuesta);
	}

	/**
	* Returns the ambiente of this log q radar.
	*
	* @return the ambiente of this log q radar
	*/
	@Override
	public java.lang.String getAmbiente() {
		return _logQRadar.getAmbiente();
	}

	/**
	* Sets the ambiente of this log q radar.
	*
	* @param ambiente the ambiente of this log q radar
	*/
	@Override
	public void setAmbiente(java.lang.String ambiente) {
		_logQRadar.setAmbiente(ambiente);
	}

	/**
	* Returns the producto of this log q radar.
	*
	* @return the producto of this log q radar
	*/
	@Override
	public java.lang.String getProducto() {
		return _logQRadar.getProducto();
	}

	/**
	* Sets the producto of this log q radar.
	*
	* @param producto the producto of this log q radar
	*/
	@Override
	public void setProducto(java.lang.String producto) {
		_logQRadar.setProducto(producto);
	}

	/**
	* Returns the id producto of this log q radar.
	*
	* @return the id producto of this log q radar
	*/
	@Override
	public int getIdProducto() {
		return _logQRadar.getIdProducto();
	}

	/**
	* Sets the id producto of this log q radar.
	*
	* @param idProducto the id producto of this log q radar
	*/
	@Override
	public void setIdProducto(int idProducto) {
		_logQRadar.setIdProducto(idProducto);
	}

	/**
	* Returns the fecha hora of this log q radar.
	*
	* @return the fecha hora of this log q radar
	*/
	@Override
	public java.util.Date getFechaHora() {
		return _logQRadar.getFechaHora();
	}

	/**
	* Sets the fecha hora of this log q radar.
	*
	* @param fechaHora the fecha hora of this log q radar
	*/
	@Override
	public void setFechaHora(java.util.Date fechaHora) {
		_logQRadar.setFechaHora(fechaHora);
	}

	@Override
	public boolean isNew() {
		return _logQRadar.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_logQRadar.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _logQRadar.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_logQRadar.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _logQRadar.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _logQRadar.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_logQRadar.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _logQRadar.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_logQRadar.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_logQRadar.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_logQRadar.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LogQRadarWrapper((LogQRadar)_logQRadar.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.LogQRadar logQRadar) {
		return _logQRadar.compareTo(logQRadar);
	}

	@Override
	public int hashCode() {
		return _logQRadar.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.LogQRadar> toCacheModel() {
		return _logQRadar.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.LogQRadar toEscapedModel() {
		return new LogQRadarWrapper(_logQRadar.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.LogQRadar toUnescapedModel() {
		return new LogQRadarWrapper(_logQRadar.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _logQRadar.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _logQRadar.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_logQRadar.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LogQRadarWrapper)) {
			return false;
		}

		LogQRadarWrapper logQRadarWrapper = (LogQRadarWrapper)obj;

		if (Validator.equals(_logQRadar, logQRadarWrapper._logQRadar)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public LogQRadar getWrappedLogQRadar() {
		return _logQRadar;
	}

	@Override
	public LogQRadar getWrappedModel() {
		return _logQRadar;
	}

	@Override
	public void resetOriginalValues() {
		_logQRadar.resetOriginalValues();
	}

	private LogQRadar _logQRadar;
}