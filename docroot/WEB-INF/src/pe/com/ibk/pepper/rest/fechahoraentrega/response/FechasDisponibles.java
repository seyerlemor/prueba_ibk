package pe.com.ibk.pepper.rest.fechahoraentrega.response;

import java.util.List;

public class FechasDisponibles {
	private List<Object> item;
	
	public FechasDisponibles() {
		super();
	}

	public FechasDisponibles(List<Object> item) {
		super();
		this.item = item;
	}

	public List<Object> getItem() {
		return item;
	}

	public void setItem(List<Object> item) {
		this.item = item;
	}
	
}
