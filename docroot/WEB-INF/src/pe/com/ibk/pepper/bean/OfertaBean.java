package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class OfertaBean  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String brandProduct;
	private String typeProduct;
	private String creditLine;
	private String productRate;
	private String customerType;
	private String brandDescription;
	private String descriptionType;
	private String memberShip;
	private String tea;
	private String safeDeduction;
	private String imageProduct;
	private String minimumCreditLine;
	private String tcDetail;
	private String formatCreditLine;
	
	public String getBrandProduct() {
		return brandProduct;
	}
	public void setBrandProduct(String brandProduct) {
		this.brandProduct = brandProduct;
	}
	public String getTypeProduct() {
		return typeProduct;
	}
	public void setTypeProduct(String typeProduct) {
		this.typeProduct = typeProduct;
	}
	public String getCreditLine() {
		return creditLine;
	}
	public void setCreditLine(String creditLine) {
		this.creditLine = creditLine;
	}
	public String getProductRate() {
		return productRate;
	}
	public void setProductRate(String productRate) {
		this.productRate = productRate;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getBrandDescription() {
		return brandDescription;
	}
	public void setBrandDescription(String brandDescription) {
		this.brandDescription = brandDescription;
	}
	public String getDescriptionType() {
		return descriptionType;
	}
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}
	public String getMemberShip() {
		return memberShip;
	}
	public void setMemberShip(String memberShip) {
		this.memberShip = memberShip;
	}
	public String getTea() {
		return tea;
	}
	public void setTea(String tea) {
		this.tea = tea;
	}
	public String getSafeDeduction() {
		return safeDeduction;
	}
	public void setSafeDeduction(String safeDeduction) {
		this.safeDeduction = safeDeduction;
	}
	public String getImageProduct() {
		return imageProduct;
	}
	public void setImageProduct(String imageProduct) {
		this.imageProduct = imageProduct;
	}
	public String getMinimumCreditLine() {
		return minimumCreditLine;
	}
	public void setMinimumCreditLine(String minimumCreditLine) {
		this.minimumCreditLine = minimumCreditLine;
	}
	public String getTcDetail() {
		return tcDetail;
	}
	public void setTcDetail(String tcDetail) {
		this.tcDetail = tcDetail;
	}
	public String getFormatCreditLine() {
		return formatCreditLine;
	}
	public void setFormatCreditLine(String formatCreditLine) {
		this.formatCreditLine = formatCreditLine;
	}
	@Override
	public String toString() {
		return "OfertaBean [brandProduct=" + brandProduct + ", typeProduct="
				+ typeProduct + ", creditLine=" + creditLine + ", productRate="
				+ productRate + ", customerType=" + customerType
				+ ", brandDescription=" + brandDescription
				+ ", descriptionType=" + descriptionType + ", memberShip="
				+ memberShip + ", tea=" + tea + ", safeDeduction="
				+ safeDeduction + ", imageProduct=" + imageProduct
				+ ", minimumCreditLine=" + minimumCreditLine + ", tcDetail="
				+ tcDetail + ", formatCreditLine=" + formatCreditLine + "]";
	}
	
}
