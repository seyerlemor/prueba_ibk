/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Servicios}.
 * </p>
 *
 * @author Interbank
 * @see Servicios
 * @generated
 */
public class ServiciosWrapper implements Servicios, ModelWrapper<Servicios> {
	public ServiciosWrapper(Servicios servicios) {
		_servicios = servicios;
	}

	@Override
	public Class<?> getModelClass() {
		return Servicios.class;
	}

	@Override
	public String getModelClassName() {
		return Servicios.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idServicio", getIdServicio());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("nombre", getNombre());
		attributes.put("codigo", getCodigo());
		attributes.put("descripcion", getDescripcion());
		attributes.put("response", getResponse());
		attributes.put("messageBus", getMessageBus());
		attributes.put("tipo", getTipo());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("respuestaInterna", getRespuestaInterna());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idServicio = (Long)attributes.get("idServicio");

		if (idServicio != null) {
			setIdServicio(idServicio);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Long response = (Long)attributes.get("response");

		if (response != null) {
			setResponse(response);
		}

		String messageBus = (String)attributes.get("messageBus");

		if (messageBus != null) {
			setMessageBus(messageBus);
		}

		String tipo = (String)attributes.get("tipo");

		if (tipo != null) {
			setTipo(tipo);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String respuestaInterna = (String)attributes.get("respuestaInterna");

		if (respuestaInterna != null) {
			setRespuestaInterna(respuestaInterna);
		}
	}

	/**
	* Returns the primary key of this servicios.
	*
	* @return the primary key of this servicios
	*/
	@Override
	public long getPrimaryKey() {
		return _servicios.getPrimaryKey();
	}

	/**
	* Sets the primary key of this servicios.
	*
	* @param primaryKey the primary key of this servicios
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_servicios.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id servicio of this servicios.
	*
	* @return the id servicio of this servicios
	*/
	@Override
	public long getIdServicio() {
		return _servicios.getIdServicio();
	}

	/**
	* Sets the id servicio of this servicios.
	*
	* @param idServicio the id servicio of this servicios
	*/
	@Override
	public void setIdServicio(long idServicio) {
		_servicios.setIdServicio(idServicio);
	}

	/**
	* Returns the id expediente of this servicios.
	*
	* @return the id expediente of this servicios
	*/
	@Override
	public long getIdExpediente() {
		return _servicios.getIdExpediente();
	}

	/**
	* Sets the id expediente of this servicios.
	*
	* @param idExpediente the id expediente of this servicios
	*/
	@Override
	public void setIdExpediente(long idExpediente) {
		_servicios.setIdExpediente(idExpediente);
	}

	/**
	* Returns the nombre of this servicios.
	*
	* @return the nombre of this servicios
	*/
	@Override
	public java.lang.String getNombre() {
		return _servicios.getNombre();
	}

	/**
	* Sets the nombre of this servicios.
	*
	* @param nombre the nombre of this servicios
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_servicios.setNombre(nombre);
	}

	/**
	* Returns the codigo of this servicios.
	*
	* @return the codigo of this servicios
	*/
	@Override
	public java.lang.String getCodigo() {
		return _servicios.getCodigo();
	}

	/**
	* Sets the codigo of this servicios.
	*
	* @param codigo the codigo of this servicios
	*/
	@Override
	public void setCodigo(java.lang.String codigo) {
		_servicios.setCodigo(codigo);
	}

	/**
	* Returns the descripcion of this servicios.
	*
	* @return the descripcion of this servicios
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _servicios.getDescripcion();
	}

	/**
	* Sets the descripcion of this servicios.
	*
	* @param descripcion the descripcion of this servicios
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_servicios.setDescripcion(descripcion);
	}

	/**
	* Returns the response of this servicios.
	*
	* @return the response of this servicios
	*/
	@Override
	public long getResponse() {
		return _servicios.getResponse();
	}

	/**
	* Sets the response of this servicios.
	*
	* @param response the response of this servicios
	*/
	@Override
	public void setResponse(long response) {
		_servicios.setResponse(response);
	}

	/**
	* Returns the message bus of this servicios.
	*
	* @return the message bus of this servicios
	*/
	@Override
	public java.lang.String getMessageBus() {
		return _servicios.getMessageBus();
	}

	/**
	* Sets the message bus of this servicios.
	*
	* @param messageBus the message bus of this servicios
	*/
	@Override
	public void setMessageBus(java.lang.String messageBus) {
		_servicios.setMessageBus(messageBus);
	}

	/**
	* Returns the tipo of this servicios.
	*
	* @return the tipo of this servicios
	*/
	@Override
	public java.lang.String getTipo() {
		return _servicios.getTipo();
	}

	/**
	* Sets the tipo of this servicios.
	*
	* @param tipo the tipo of this servicios
	*/
	@Override
	public void setTipo(java.lang.String tipo) {
		_servicios.setTipo(tipo);
	}

	/**
	* Returns the fecha registro of this servicios.
	*
	* @return the fecha registro of this servicios
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _servicios.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this servicios.
	*
	* @param fechaRegistro the fecha registro of this servicios
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_servicios.setFechaRegistro(fechaRegistro);
	}

	/**
	* Returns the respuesta interna of this servicios.
	*
	* @return the respuesta interna of this servicios
	*/
	@Override
	public java.lang.String getRespuestaInterna() {
		return _servicios.getRespuestaInterna();
	}

	/**
	* Sets the respuesta interna of this servicios.
	*
	* @param respuestaInterna the respuesta interna of this servicios
	*/
	@Override
	public void setRespuestaInterna(java.lang.String respuestaInterna) {
		_servicios.setRespuestaInterna(respuestaInterna);
	}

	@Override
	public boolean isNew() {
		return _servicios.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_servicios.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _servicios.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_servicios.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _servicios.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _servicios.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_servicios.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _servicios.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_servicios.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_servicios.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_servicios.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ServiciosWrapper((Servicios)_servicios.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.Servicios servicios) {
		return _servicios.compareTo(servicios);
	}

	@Override
	public int hashCode() {
		return _servicios.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.Servicios> toCacheModel() {
		return _servicios.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.Servicios toEscapedModel() {
		return new ServiciosWrapper(_servicios.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.Servicios toUnescapedModel() {
		return new ServiciosWrapper(_servicios.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _servicios.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _servicios.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_servicios.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ServiciosWrapper)) {
			return false;
		}

		ServiciosWrapper serviciosWrapper = (ServiciosWrapper)obj;

		if (Validator.equals(_servicios, serviciosWrapper._servicios)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Servicios getWrappedServicios() {
		return _servicios;
	}

	@Override
	public Servicios getWrappedModel() {
		return _servicios;
	}

	@Override
	public void resetOriginalValues() {
		_servicios.resetOriginalValues();
	}

	private Servicios _servicios;
}