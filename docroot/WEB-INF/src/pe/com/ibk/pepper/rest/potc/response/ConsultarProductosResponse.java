
package pe.com.ibk.pepper.rest.potc.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class ConsultarProductosResponse implements Serializable{

	private static final long serialVersionUID = 8044920979905533505L;
	
	@SerializedName(value="productos")
	private Productos productos;

	public Productos getProductos() {
		return productos;
	}

	public void setProductos(Productos productos) {
		this.productos = productos;
	}
	

}
