/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.Ubigeo;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Ubigeo in entity cache.
 *
 * @author Interbank
 * @see Ubigeo
 * @generated
 */
public class UbigeoCacheModel implements CacheModel<Ubigeo>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{idUbigeo=");
		sb.append(idUbigeo);
		sb.append(", codigo=");
		sb.append(codigo);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", codDepartamento=");
		sb.append(codDepartamento);
		sb.append(", codProvincia=");
		sb.append(codProvincia);
		sb.append(", codDistrito=");
		sb.append(codDistrito);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Ubigeo toEntityModel() {
		UbigeoImpl ubigeoImpl = new UbigeoImpl();

		ubigeoImpl.setIdUbigeo(idUbigeo);

		if (codigo == null) {
			ubigeoImpl.setCodigo(StringPool.BLANK);
		}
		else {
			ubigeoImpl.setCodigo(codigo);
		}

		if (nombre == null) {
			ubigeoImpl.setNombre(StringPool.BLANK);
		}
		else {
			ubigeoImpl.setNombre(nombre);
		}

		if (codDepartamento == null) {
			ubigeoImpl.setCodDepartamento(StringPool.BLANK);
		}
		else {
			ubigeoImpl.setCodDepartamento(codDepartamento);
		}

		if (codProvincia == null) {
			ubigeoImpl.setCodProvincia(StringPool.BLANK);
		}
		else {
			ubigeoImpl.setCodProvincia(codProvincia);
		}

		if (codDistrito == null) {
			ubigeoImpl.setCodDistrito(StringPool.BLANK);
		}
		else {
			ubigeoImpl.setCodDistrito(codDistrito);
		}

		ubigeoImpl.resetOriginalValues();

		return ubigeoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idUbigeo = objectInput.readLong();
		codigo = objectInput.readUTF();
		nombre = objectInput.readUTF();
		codDepartamento = objectInput.readUTF();
		codProvincia = objectInput.readUTF();
		codDistrito = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idUbigeo);

		if (codigo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigo);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (codDepartamento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codDepartamento);
		}

		if (codProvincia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codProvincia);
		}

		if (codDistrito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codDistrito);
		}
	}

	public long idUbigeo;
	public String codigo;
	public String nombre;
	public String codDepartamento;
	public String codProvincia;
	public String codDistrito;
}