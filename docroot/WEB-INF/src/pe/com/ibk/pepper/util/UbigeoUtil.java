package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.com.ibk.pepper.bean.DireccionClienteBean;
import pe.com.ibk.pepper.bean.JsonLiquidBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.model.Ubigeo;
import pe.com.ibk.pepper.service.UbigeoLocalServiceUtil;

/*
 * The Class UbigeoUtil.
 *
 * @autor : Interbank Modulo Tarjeta
 */
public class UbigeoUtil {

	/** The Constant _log. */
	private static final Log _log = LogFactoryUtil.getLog(UbigeoUtil.class);
	
	/**
	 * Instantiates a new ubigeo util.
	 */
	private UbigeoUtil()
	{
		
	}
	
    /**
     * Gets the list ubigeo.
     *
     * @param codDepa the cod depa
     * @param codProv the cod prov
     * @param codDist the cod dist
     * @return the list ubigeo
     * @throws WebServicesException the web services exception
     */
	public static List<Ubigeo> getListUbigeo(String codDepa,
			String codProv, String codDist) throws WebServicesException {
		List<Ubigeo> lstUbigeo = new ArrayList<>();

		try {
			if (!codProv.equals(Constantes.DEFAULT_COD_UBIGEO))
				lstUbigeo = UbigeoLocalServiceUtil.getUbigeoByD_P_N_D(codDepa, codProv, codDist);
			else
				lstUbigeo = UbigeoLocalServiceUtil.getUbigeoByD_N_P_D(codDepa, codProv, codDist);
		} catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e);
			}
			throw new WebServicesException(Constantes.ESTADO_ERROR_DATA, Constantes.COD_PARAM_E_OBT_UBIGEO, 
					Constantes.NAME_PARAM_E_OBT_UBIGEO, "Error getListUbigeo codDepa :".concat(codDepa)
					.concat(" cod Prov").concat(codProv).concat(" Cod Distri:").concat(codDist));
		}
		return lstUbigeo;
	}
    /**
     * Gets the departamentos.
     *
     * @return the departamentos
     */
    
    public static List<Ubigeo> getDepartamentos(){
    	
    	List<Ubigeo> lstUbigeo= new ArrayList<>();
		try {
			lstUbigeo =UbigeoLocalServiceUtil.getUbigeoByCodProvincia(Constantes.DEFAULT_COD_UBIGEO);
		} catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e);
			}
			_log.error("Error al traer el Ubigeo departamentos "+ e);
		}
		return lstUbigeo;
	}
    
    
   /**
    * Gets lst Ubigeo json in obj JsonBean 
    *
    * @param codigoProvincia the codigo provincia
    * @param codigoDepartamento the codigo departamento
    * @param codigoDistrito the codigo distrito
    * @return the departamentos json
    * @throws WebServicesException the web services exception
    */
    
  	public static List<JsonLiquidBean> getUbigeosJson(String codigoDepartamento,String codigoProvincia,String codigoDistrito) throws WebServicesException{
  		
  		List<Ubigeo> lstUbigeo;
  		if(codigoDepartamento.equals(Constantes.DEFAULT_COD_UBIGEO))
  			lstUbigeo=getDepartamentos();
  		else 
  			lstUbigeo=getListUbigeo(codigoDepartamento, codigoProvincia, codigoDistrito);

  		List<JsonLiquidBean> lstUbigeoJson=new ArrayList<>();
  		
  		if(!lstUbigeo.isEmpty())
  		{
  			for(Ubigeo obj:lstUbigeo){
  				if(codigoDepartamento.equals(Constantes.DEFAULT_COD_UBIGEO))
  				   lstUbigeoJson.add(new JsonLiquidBean(obj.getCodDepartamento(),StringsUtil.getStringCapitalize(obj.getNombre())));
  				else
  					lstUbigeoJson.add(new JsonLiquidBean(obj.getCodigo(),StringsUtil.getStringCapitalize(obj.getNombre())));
			}
  		}

  		return lstUbigeoJson;
  	}
  	
  	/**
	 * Buscar list ubigeo.
	 *
	 * @param lstDept the lst dept
	 * @param codBusqueda the cod busqueda
	 * @param opcion the opcion
	 * @return the map
	 */
	public static Map<String, Object> findListUbigeoBYId(List<Ubigeo> lstDept,String codBusqueda,int opcion) {
		
		Map<String, Object> respuesta = new HashMap<>();
		boolean encontrado=Boolean.FALSE;
		String valor=StringPool.BLANK;
		
		for(Ubigeo item: lstDept)
		{
			switch (opcion) {
			case 1:
					if(item.getCodDepartamento().equals(codBusqueda))
					{
						encontrado=Boolean.TRUE;
						valor=item.getNombre();
						break;
					}
				break;
			case 2:
				    if(item.getCodProvincia().equals(codBusqueda))
					{
						encontrado=Boolean.TRUE;
						valor=item.getNombre();
						break;
					}
				break;
			case 3:
					if(item.getCodDistrito().equals(codBusqueda))
					{
						encontrado=Boolean.TRUE;
						valor=item.getNombre();
						break;
					}
				break;
			default:
				break;
			}
		}
		respuesta.put(Constantes.NAME_V_A_STC_ENCON_UBI, encontrado);
		respuesta.put(Constantes.NAME_V_A_STC_VALOR_UBI,valor);
		return respuesta;
	}
  	 	
  	/**
     * *.
     *
     * @param codUbigeo the cod ubigeo
     * @return the ubigeo by cod
  * @throws WebServicesException 
     */
 	
 	public static DireccionClienteBean getUbigeoByCod(String codUbigeo) throws WebServicesException
 	{
 		DireccionClienteBean direccion= new DireccionClienteBean();
 		
 		Map<String, Object> respuesta=findListUbigeoBYId(UbigeoUtil.getDepartamentos(),codUbigeo.substring(0,2),1);
     	if ((Boolean)respuesta.get(Constantes.NAME_V_A_STC_ENCON_UBI)) {
     		direccion.setDepartamento(respuesta.get(Constantes.NAME_V_A_STC_VALOR_UBI).toString());
 		}
     
 		respuesta=findListUbigeoBYId(getListUbigeo(codUbigeo.substring(0,2),Constantes.DEFAULT_COD_UBIGEO,Constantes.DEFAULT_COD_UBIGEO),codUbigeo.substring(2,4),2);
 		if ((Boolean)respuesta.get(Constantes.NAME_V_A_STC_ENCON_UBI)) {
     		direccion.setProvincia(respuesta.get(Constantes.NAME_V_A_STC_VALOR_UBI).toString());
 		}
 		
 		respuesta=findListUbigeoBYId(getListUbigeo(codUbigeo.substring(0,2),codUbigeo.substring(2,4),Constantes.DEFAULT_COD_UBIGEO),codUbigeo.substring(4,6),3);
 		if ((Boolean)respuesta.get(Constantes.NAME_V_A_STC_ENCON_UBI)) {
     		direccion.setDistrito(respuesta.get(Constantes.NAME_V_A_STC_VALOR_UBI).toString());
 		}
 		return direccion;
 	}
}
