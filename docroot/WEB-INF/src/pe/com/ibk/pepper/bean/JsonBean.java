package pe.com.ibk.pepper.bean;

import java.io.Serializable;

/**
 * The Class JsonBean.
 * @author Interbank Modulo Tarjeta
 */
public class JsonBean implements Serializable{

	@Override
	public String toString() {
		return "JsonBean [key=" + key + ", value=" + value + ", valueOpcional="
				+ valueOpcional + "]";
	}
	private static final long serialVersionUID = 1L;

	private String key;
	private String value;
	private String valueOpcional;
	
	public JsonBean(String key, String value)
	{
	  	this.key=key;
	  	this.value=value;
	}
	
	public JsonBean(String key, String value, String valueOpcional)
	{
	  	this.key=key;
	  	this.value=value;
	  	this.valueOpcional=valueOpcional;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getValueOpcional() {
		return valueOpcional;
	}
	public void setValueOpcional(String valueOpcional) {
		this.valueOpcional = valueOpcional;
	}
	
}
