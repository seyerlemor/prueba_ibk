package pe.com.ibk.pepper.util.cola;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import pe.com.ibk.pepper.bean.SolicitudBean;

public class QueueUtil {
	
	private static AsyncDataWriter asyncWriter;
	DataToBeInserted data;
	
	private static final Log _log = LogFactoryUtil.getLog(QueueUtil.class);
	
	public void adicionarElementoCola(Integer tabla, SolicitudBean pasobean){
		
		data = new DataToBeInserted(tabla, pasobean);
		_log.debug("Agregando dato a la cola:" + tabla);
		getAsyncWriter().accept(data);
		
	}

	public static AsyncDataWriter getAsyncWriter() {
		if(asyncWriter==null){
			asyncWriter =  new AsyncDataWriter();
			
			asyncWriter.start();
		}
		return asyncWriter;
	}

	public static void setAsyncWriter(AsyncDataWriter asyncWriter) {
		QueueUtil.asyncWriter = asyncWriter;
	}
	
}