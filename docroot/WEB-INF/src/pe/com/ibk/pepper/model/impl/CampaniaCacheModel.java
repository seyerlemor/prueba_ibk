/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.Campania;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Campania in entity cache.
 *
 * @author Interbank
 * @see Campania
 * @generated
 */
public class CampaniaCacheModel implements CacheModel<Campania>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{idCampania=");
		sb.append(idCampania);
		sb.append(", idExpediente=");
		sb.append(idExpediente);
		sb.append(", codigoCampania=");
		sb.append(codigoCampania);
		sb.append(", nombreCampania=");
		sb.append(nombreCampania);
		sb.append(", codigoOferta=");
		sb.append(codigoOferta);
		sb.append(", nombreOferta=");
		sb.append(nombreOferta);
		sb.append(", codigoTratamiento=");
		sb.append(codigoTratamiento);
		sb.append(", nombreTratamiento=");
		sb.append(nombreTratamiento);
		sb.append(", codigoUnico=");
		sb.append(codigoUnico);
		sb.append(", codigoProducto=");
		sb.append(codigoProducto);
		sb.append(", core=");
		sb.append(core);
		sb.append(", nombreProducto=");
		sb.append(nombreProducto);
		sb.append(", tipoCampania=");
		sb.append(tipoCampania);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Campania toEntityModel() {
		CampaniaImpl campaniaImpl = new CampaniaImpl();

		campaniaImpl.setIdCampania(idCampania);
		campaniaImpl.setIdExpediente(idExpediente);

		if (codigoCampania == null) {
			campaniaImpl.setCodigoCampania(StringPool.BLANK);
		}
		else {
			campaniaImpl.setCodigoCampania(codigoCampania);
		}

		if (nombreCampania == null) {
			campaniaImpl.setNombreCampania(StringPool.BLANK);
		}
		else {
			campaniaImpl.setNombreCampania(nombreCampania);
		}

		if (codigoOferta == null) {
			campaniaImpl.setCodigoOferta(StringPool.BLANK);
		}
		else {
			campaniaImpl.setCodigoOferta(codigoOferta);
		}

		if (nombreOferta == null) {
			campaniaImpl.setNombreOferta(StringPool.BLANK);
		}
		else {
			campaniaImpl.setNombreOferta(nombreOferta);
		}

		if (codigoTratamiento == null) {
			campaniaImpl.setCodigoTratamiento(StringPool.BLANK);
		}
		else {
			campaniaImpl.setCodigoTratamiento(codigoTratamiento);
		}

		if (nombreTratamiento == null) {
			campaniaImpl.setNombreTratamiento(StringPool.BLANK);
		}
		else {
			campaniaImpl.setNombreTratamiento(nombreTratamiento);
		}

		if (codigoUnico == null) {
			campaniaImpl.setCodigoUnico(StringPool.BLANK);
		}
		else {
			campaniaImpl.setCodigoUnico(codigoUnico);
		}

		if (codigoProducto == null) {
			campaniaImpl.setCodigoProducto(StringPool.BLANK);
		}
		else {
			campaniaImpl.setCodigoProducto(codigoProducto);
		}

		if (core == null) {
			campaniaImpl.setCore(StringPool.BLANK);
		}
		else {
			campaniaImpl.setCore(core);
		}

		if (nombreProducto == null) {
			campaniaImpl.setNombreProducto(StringPool.BLANK);
		}
		else {
			campaniaImpl.setNombreProducto(nombreProducto);
		}

		if (tipoCampania == null) {
			campaniaImpl.setTipoCampania(StringPool.BLANK);
		}
		else {
			campaniaImpl.setTipoCampania(tipoCampania);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			campaniaImpl.setFechaRegistro(null);
		}
		else {
			campaniaImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		campaniaImpl.resetOriginalValues();

		return campaniaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idCampania = objectInput.readLong();
		idExpediente = objectInput.readLong();
		codigoCampania = objectInput.readUTF();
		nombreCampania = objectInput.readUTF();
		codigoOferta = objectInput.readUTF();
		nombreOferta = objectInput.readUTF();
		codigoTratamiento = objectInput.readUTF();
		nombreTratamiento = objectInput.readUTF();
		codigoUnico = objectInput.readUTF();
		codigoProducto = objectInput.readUTF();
		core = objectInput.readUTF();
		nombreProducto = objectInput.readUTF();
		tipoCampania = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idCampania);
		objectOutput.writeLong(idExpediente);

		if (codigoCampania == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoCampania);
		}

		if (nombreCampania == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreCampania);
		}

		if (codigoOferta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoOferta);
		}

		if (nombreOferta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreOferta);
		}

		if (codigoTratamiento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoTratamiento);
		}

		if (nombreTratamiento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreTratamiento);
		}

		if (codigoUnico == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoUnico);
		}

		if (codigoProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoProducto);
		}

		if (core == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(core);
		}

		if (nombreProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreProducto);
		}

		if (tipoCampania == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoCampania);
		}

		objectOutput.writeLong(fechaRegistro);
	}

	public long idCampania;
	public long idExpediente;
	public String codigoCampania;
	public String nombreCampania;
	public String codigoOferta;
	public String nombreOferta;
	public String codigoTratamiento;
	public String nombreTratamiento;
	public String codigoUnico;
	public String codigoProducto;
	public String core;
	public String nombreProducto;
	public String tipoCampania;
	public long fechaRegistro;
}