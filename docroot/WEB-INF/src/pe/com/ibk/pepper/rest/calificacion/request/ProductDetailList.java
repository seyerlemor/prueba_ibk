
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProductDetailList {

    private ProductDetail productDetail;

    @JsonCreator
    public ProductDetailList() {
		super();
	}

	@JsonCreator
	public ProductDetailList(ProductDetail productDetail) {
		super();
		this.productDetail = productDetail;
	}

    @JsonProperty("productDetail")
	public ProductDetail getProductDetail() {
		return productDetail;
	}

	public void setProductDetail(ProductDetail productDetail) {
		this.productDetail = productDetail;
	}

}
