
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IdentityDocument {

    private String type;
    private String number;
    
    @JsonCreator
	public IdentityDocument() {
		super();
	}
    
    @JsonCreator
	public IdentityDocument(String type, String number) {
		super();
		this.type = type;
		this.number = number;
	}

    @JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
    
}
