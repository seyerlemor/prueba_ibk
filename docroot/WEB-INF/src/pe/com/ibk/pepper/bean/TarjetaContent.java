package pe.com.ibk.pepper.bean;

import java.util.List;

import pe.com.ibk.pepper.util.Entity;

public class TarjetaContent {

	private String titulo;
	private String tituloTarjeta;
	private String imagenTarjeta;
	private String descripcion;
	private String pregunta;
	private String textoBotonIzquierdo;
	private String textoBotonDerecho;
	private String html;
	private List<Entity> icons;
	private String subTitulo;
	private String htmlBeneficios;

	public String getSubTitulo() {
		return subTitulo;
	}

	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}

	public TarjetaContent() { }

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTituloTarjeta() {
		return tituloTarjeta;
	}

	public void setTituloTarjeta(String tituloTarjeta) {
		this.tituloTarjeta = tituloTarjeta;
	}

	public String getImagenTarjeta() {
		return imagenTarjeta;
	}

	public void setImagenTarjeta(String imagenTarjeta) {
		this.imagenTarjeta = imagenTarjeta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getTextoBotonIzquierdo() {
		return textoBotonIzquierdo;
	}

	public void setTextoBotonIzquierdo(String textoBotonIzquierdo) {
		this.textoBotonIzquierdo = textoBotonIzquierdo;
	}

	public String getTextoBotonDerecho() {
		return textoBotonDerecho;
	}

	public void setTextoBotonDerecho(String textoBotonDerecho) {
		this.textoBotonDerecho = textoBotonDerecho;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public List<Entity> getIcons() {
		return icons;
	}

	public void setIcons(List<Entity> icons) {
		this.icons = icons;
	}

	public String getHtmlBeneficios() {
		return htmlBeneficios;
	}

	public void setHtmlBeneficios(String htmlBeneficios) {
		this.htmlBeneficios = htmlBeneficios;
	}

}
