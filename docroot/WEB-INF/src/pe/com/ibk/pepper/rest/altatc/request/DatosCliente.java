
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DatosCliente {

    private String apellidoPaterno;
    private String segundoNombre;
    private String referenciaUbicacion;
    private String nombreVia;
    private String fechaNacimiento;
    private String codigoPaisNacimiento;
    private String nombreUrbanizacion;
    private String codigoOcupacion;
    private String codigoSituacionLaboral;
    private String codigoPaisResidencia;
    private String idManzana;
    private String provincia;
    private String codigoUnico;
    private String nombreEmpresa;
    private String apellidoMaterno;
    private String numeroCalle;
    private String codigoPaisNacionalidad;
    private String numeroDocumento;
    private String tipoNacionalidad;
    private String email;
    private String idInterior;
    private String numeroTelefono;
    private String distrito;
    private String codigoUbigeo;
    private String primerNombre;
    private String tipoVia;
    private String estadoCivil;
    private String tipoDocumento;
    private String departamento;
    private String sexo;
    private String idLote;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DatosCliente() {
    }

    /**
     * 
     * @param nombreUrbanizacion
     * @param estadoCivil
     * @param idManzana
     * @param numeroDocumento
     * @param tipoNacionalidad
     * @param sexo
     * @param apellidoPaterno
     * @param idLote
     * @param codigoUbigeo
     * @param provincia
     * @param fechaNacimiento
     * @param primerNombre
     * @param numeroTelefono
     * @param referenciaUbicacion
     * @param departamento
     * @param codigoSituacionLaboral
     * @param tipoDocumento
     * @param apellidoMaterno
     * @param nombreEmpresa
     * @param distrito
     * @param codigoOcupacion
     * @param idInterior
     * @param codigoPaisNacimiento
     * @param nombreVia
     * @param email
     * @param tipoVia
     * @param codigoPaisResidencia
     * @param segundoNombre
     * @param codigoPaisNacionalidad
     * @param numeroCalle
     * @param codigoUnico
     */
    public DatosCliente(String apellidoPaterno, String segundoNombre, String referenciaUbicacion, String nombreVia, String fechaNacimiento, String codigoPaisNacimiento, String nombreUrbanizacion, String codigoOcupacion, String codigoSituacionLaboral, String codigoPaisResidencia, String idManzana, String provincia, String nombreEmpresa, String apellidoMaterno, String numeroCalle, String codigoPaisNacionalidad, String numeroDocumento, String tipoNacionalidad, String email, String idInterior, String numeroTelefono, String distrito, String codigoUbigeo, String primerNombre, String tipoVia, String estadoCivil, String tipoDocumento, String departamento, String sexo, String idLote, String codigoUnico) {
        super();
        this.apellidoPaterno = apellidoPaterno;
        this.segundoNombre = segundoNombre;
        this.referenciaUbicacion = referenciaUbicacion;
        this.nombreVia = nombreVia;
        this.fechaNacimiento = fechaNacimiento;
        this.codigoPaisNacimiento = codigoPaisNacimiento;
        this.nombreUrbanizacion = nombreUrbanizacion;
        this.codigoOcupacion = codigoOcupacion;
        this.codigoSituacionLaboral = codigoSituacionLaboral;
        this.codigoPaisResidencia = codigoPaisResidencia;
        this.idManzana = idManzana;
        this.provincia = provincia;
        this.nombreEmpresa = nombreEmpresa;
        this.apellidoMaterno = apellidoMaterno;
        this.numeroCalle = numeroCalle;
        this.codigoPaisNacionalidad = codigoPaisNacionalidad;
        this.numeroDocumento = numeroDocumento;
        this.tipoNacionalidad = tipoNacionalidad;
        this.email = email;
        this.idInterior = idInterior;
        this.numeroTelefono = numeroTelefono;
        this.distrito = distrito;
        this.codigoUbigeo = codigoUbigeo;
        this.primerNombre = primerNombre;
        this.tipoVia = tipoVia;
        this.estadoCivil = estadoCivil;
        this.tipoDocumento = tipoDocumento;
        this.departamento = departamento;
        this.sexo = sexo;
        this.idLote = idLote;
        this.codigoUnico = codigoUnico;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getReferenciaUbicacion() {
        return referenciaUbicacion;
    }

    public void setReferenciaUbicacion(String referenciaUbicacion) {
        this.referenciaUbicacion = referenciaUbicacion;
    }

    public String getNombreVia() {
        return nombreVia;
    }

    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCodigoPaisNacimiento() {
        return codigoPaisNacimiento;
    }

    public void setCodigoPaisNacimiento(String codigoPaisNacimiento) {
        this.codigoPaisNacimiento = codigoPaisNacimiento;
    }

    public String getNombreUrbanizacion() {
        return nombreUrbanizacion;
    }

    public void setNombreUrbanizacion(String nombreUrbanizacion) {
        this.nombreUrbanizacion = nombreUrbanizacion;
    }

    public String getCodigoOcupacion() {
        return codigoOcupacion;
    }

    public void setCodigoOcupacion(String codigoOcupacion) {
        this.codigoOcupacion = codigoOcupacion;
    }

    public String getCodigoSituacionLaboral() {
        return codigoSituacionLaboral;
    }

    public void setCodigoSituacionLaboral(String codigoSituacionLaboral) {
        this.codigoSituacionLaboral = codigoSituacionLaboral;
    }

    public String getCodigoPaisResidencia() {
        return codigoPaisResidencia;
    }

    public void setCodigoPaisResidencia(String codigoPaisResidencia) {
        this.codigoPaisResidencia = codigoPaisResidencia;
    }

    public String getIdManzana() {
        return idManzana;
    }

    public void setIdManzana(String idManzana) {
        this.idManzana = idManzana;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNumeroCalle() {
        return numeroCalle;
    }

    public void setNumeroCalle(String numeroCalle) {
        this.numeroCalle = numeroCalle;
    }

    public String getCodigoPaisNacionalidad() {
        return codigoPaisNacionalidad;
    }

    public void setCodigoPaisNacionalidad(String codigoPaisNacionalidad) {
        this.codigoPaisNacionalidad = codigoPaisNacionalidad;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoNacionalidad() {
        return tipoNacionalidad;
    }

    public void setTipoNacionalidad(String tipoNacionalidad) {
        this.tipoNacionalidad = tipoNacionalidad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdInterior() {
        return idInterior;
    }

    public void setIdInterior(String idInterior) {
        this.idInterior = idInterior;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCodigoUbigeo() {
        return codigoUbigeo;
    }

    public void setCodigoUbigeo(String codigoUbigeo) {
        this.codigoUbigeo = codigoUbigeo;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getTipoVia() {
        return tipoVia;
    }

    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getIdLote() {
        return idLote;
    }

    public void setIdLote(String idLote) {
        this.idLote = idLote;
    }

	public String getCodigoUnico() {
		return codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}

	@Override
	public String toString() {
		return "DatosCliente [apellidoPaterno=" + apellidoPaterno
				+ ", segundoNombre=" + segundoNombre + ", referenciaUbicacion="
				+ referenciaUbicacion + ", nombreVia=" + nombreVia
				+ ", fechaNacimiento=" + fechaNacimiento
				+ ", codigoPaisNacimiento=" + codigoPaisNacimiento
				+ ", nombreUrbanizacion=" + nombreUrbanizacion
				+ ", codigoOcupacion=" + codigoOcupacion
				+ ", codigoSituacionLaboral=" + codigoSituacionLaboral
				+ ", codigoPaisResidencia=" + codigoPaisResidencia
				+ ", idManzana=" + idManzana + ", provincia=" + provincia
				+ ", nombreEmpresa=" + nombreEmpresa + ", apellidoMaterno="
				+ apellidoMaterno + ", numeroCalle=" + numeroCalle
				+ ", codigoPaisNacionalidad=" + codigoPaisNacionalidad
				+ ", numeroDocumento=" + numeroDocumento
				+ ", tipoNacionalidad=" + tipoNacionalidad + ", email=" + email
				+ ", idInterior=" + idInterior + ", numeroTelefono="
				+ numeroTelefono + ", distrito=" + distrito + ", codigoUbigeo="
				+ codigoUbigeo + ", primerNombre=" + primerNombre
				+ ", tipoVia=" + tipoVia + ", estadoCivil=" + estadoCivil
				+ ", tipoDocumento=" + tipoDocumento + ", departamento="
				+ departamento + ", sexo=" + sexo + ", idLote=" + idLote
				+ ", codigoUnico=" + codigoUnico + "]";
	}

    
}
