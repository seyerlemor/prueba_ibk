/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import pe.com.ibk.pepper.service.AuditoriaClienteLocalServiceUtil;
import pe.com.ibk.pepper.service.AuditoriaUsuarioLocalServiceUtil;
import pe.com.ibk.pepper.service.CampaniaLocalServiceUtil;
import pe.com.ibk.pepper.service.ClienteLocalServiceUtil;
import pe.com.ibk.pepper.service.ClienteTransaccionLocalServiceUtil;
import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.DirecEstandarLocalServiceUtil;
import pe.com.ibk.pepper.service.DireccionClienteLocalServiceUtil;
import pe.com.ibk.pepper.service.ExpedienteLocalServiceUtil;
import pe.com.ibk.pepper.service.LogQRadarLocalServiceUtil;
import pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil;
import pe.com.ibk.pepper.service.ParametroHijoPOServiceUtil;
import pe.com.ibk.pepper.service.ParametroPadrePOLocalServiceUtil;
import pe.com.ibk.pepper.service.PreguntasEquifaxLocalServiceUtil;
import pe.com.ibk.pepper.service.ProductoLocalServiceUtil;
import pe.com.ibk.pepper.service.ServiciosLocalServiceUtil;
import pe.com.ibk.pepper.service.TblCodigoPromocionLocalServiceUtil;
import pe.com.ibk.pepper.service.TblMaestroLocalServiceUtil;
import pe.com.ibk.pepper.service.UbigeoLocalServiceUtil;
import pe.com.ibk.pepper.service.UsuarioSessionLocalServiceUtil;

/**
 * @author Interbank
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			AuditoriaClienteLocalServiceUtil.clearService();

			AuditoriaUsuarioLocalServiceUtil.clearService();

			CampaniaLocalServiceUtil.clearService();

			ClienteLocalServiceUtil.clearService();

			ClienteTransaccionLocalServiceUtil.clearService();

			DireccionClienteLocalServiceUtil.clearService();

			DirecEstandarLocalServiceUtil.clearService();

			ExpedienteLocalServiceUtil.clearService();

			LogQRadarLocalServiceUtil.clearService();

			ParametroHijoPOLocalServiceUtil.clearService();

			ParametroHijoPOServiceUtil.clearService();
			ParametroPadrePOLocalServiceUtil.clearService();

			PreguntasEquifaxLocalServiceUtil.clearService();

			ProductoLocalServiceUtil.clearService();

			ServiciosLocalServiceUtil.clearService();

			TblCodigoPromocionLocalServiceUtil.clearService();

			TblMaestroLocalServiceUtil.clearService();

			UbigeoLocalServiceUtil.clearService();

			UsuarioSessionLocalServiceUtil.clearService();
		}
	}
}