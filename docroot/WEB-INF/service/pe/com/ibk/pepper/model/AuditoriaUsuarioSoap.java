/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class AuditoriaUsuarioSoap implements Serializable {
	public static AuditoriaUsuarioSoap toSoapModel(AuditoriaUsuario model) {
		AuditoriaUsuarioSoap soapModel = new AuditoriaUsuarioSoap();

		soapModel.setIdAuditoriaUsuario(model.getIdAuditoriaUsuario());
		soapModel.setIdUsuarioSession(model.getIdUsuarioSession());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static AuditoriaUsuarioSoap[] toSoapModels(AuditoriaUsuario[] models) {
		AuditoriaUsuarioSoap[] soapModels = new AuditoriaUsuarioSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AuditoriaUsuarioSoap[][] toSoapModels(
		AuditoriaUsuario[][] models) {
		AuditoriaUsuarioSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AuditoriaUsuarioSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AuditoriaUsuarioSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AuditoriaUsuarioSoap[] toSoapModels(
		List<AuditoriaUsuario> models) {
		List<AuditoriaUsuarioSoap> soapModels = new ArrayList<AuditoriaUsuarioSoap>(models.size());

		for (AuditoriaUsuario model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AuditoriaUsuarioSoap[soapModels.size()]);
	}

	public AuditoriaUsuarioSoap() {
	}

	public long getPrimaryKey() {
		return _idAuditoriaUsuario;
	}

	public void setPrimaryKey(long pk) {
		setIdAuditoriaUsuario(pk);
	}

	public long getIdAuditoriaUsuario() {
		return _idAuditoriaUsuario;
	}

	public void setIdAuditoriaUsuario(long idAuditoriaUsuario) {
		_idAuditoriaUsuario = idAuditoriaUsuario;
	}

	public long getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	public void setIdUsuarioSession(long idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private long _idAuditoriaUsuario;
	private long _idUsuarioSession;
	private Date _fechaRegistro;
}