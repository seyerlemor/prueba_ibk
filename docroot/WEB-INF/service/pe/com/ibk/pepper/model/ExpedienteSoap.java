/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class ExpedienteSoap implements Serializable {
	public static ExpedienteSoap toSoapModel(Expediente model) {
		ExpedienteSoap soapModel = new ExpedienteSoap();

		soapModel.setIdExpediente(model.getIdExpediente());
		soapModel.setDocumento(model.getDocumento());
		soapModel.setEstado(model.getEstado());
		soapModel.setPaso(model.getPaso());
		soapModel.setPagina(model.getPagina());
		soapModel.setTipoFlujo(model.getTipoFlujo());
		soapModel.setFlagR1(model.getFlagR1());
		soapModel.setFlagCampania(model.getFlagCampania());
		soapModel.setFlagReniec(model.getFlagReniec());
		soapModel.setFlagEquifax(model.getFlagEquifax());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setTipoCampania(model.getTipoCampania());
		soapModel.setFlagCalificacion(model.getFlagCalificacion());
		soapModel.setFlagCliente(model.getFlagCliente());
		soapModel.setIdUsuarioSession(model.getIdUsuarioSession());
		soapModel.setStrJson(model.getStrJson());
		soapModel.setFormHtml(model.getFormHtml());
		soapModel.setCodigoHtml(model.getCodigoHtml());
		soapModel.setPeriodoRegistro(model.getPeriodoRegistro());
		soapModel.setModificacion(model.getModificacion());
		soapModel.setFlagTipificacion(model.getFlagTipificacion());
		soapModel.setFlagActualizarRespuesta(model.getFlagActualizarRespuesta());
		soapModel.setFlagObtenerInformacion(model.getFlagObtenerInformacion());
		soapModel.setFlagConsultarAfiliacion(model.getFlagConsultarAfiliacion());
		soapModel.setFlagGeneracionToken(model.getFlagGeneracionToken());
		soapModel.setFlagVentaTC(model.getFlagVentaTC());
		soapModel.setFlagValidacionToken(model.getFlagValidacionToken());
		soapModel.setFlagRecompraGenPOTC(model.getFlagRecompraGenPOTC());
		soapModel.setFlagRecompraConPOTC(model.getFlagRecompraConPOTC());
		soapModel.setFlagExtornoGenPOTC(model.getFlagExtornoGenPOTC());
		soapModel.setFlagExtornoConPOTC(model.getFlagExtornoConPOTC());
		soapModel.setIp(model.getIp());
		soapModel.setNavegador(model.getNavegador());
		soapModel.setPepperType(model.getPepperType());

		return soapModel;
	}

	public static ExpedienteSoap[] toSoapModels(Expediente[] models) {
		ExpedienteSoap[] soapModels = new ExpedienteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ExpedienteSoap[][] toSoapModels(Expediente[][] models) {
		ExpedienteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ExpedienteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ExpedienteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ExpedienteSoap[] toSoapModels(List<Expediente> models) {
		List<ExpedienteSoap> soapModels = new ArrayList<ExpedienteSoap>(models.size());

		for (Expediente model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ExpedienteSoap[soapModels.size()]);
	}

	public ExpedienteSoap() {
	}

	public long getPrimaryKey() {
		return _idExpediente;
	}

	public void setPrimaryKey(long pk) {
		setIdExpediente(pk);
	}

	public long getIdExpediente() {
		return _idExpediente;
	}

	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;
	}

	public String getDocumento() {
		return _documento;
	}

	public void setDocumento(String documento) {
		_documento = documento;
	}

	public String getEstado() {
		return _estado;
	}

	public void setEstado(String estado) {
		_estado = estado;
	}

	public String getPaso() {
		return _paso;
	}

	public void setPaso(String paso) {
		_paso = paso;
	}

	public String getPagina() {
		return _pagina;
	}

	public void setPagina(String pagina) {
		_pagina = pagina;
	}

	public String getTipoFlujo() {
		return _tipoFlujo;
	}

	public void setTipoFlujo(String tipoFlujo) {
		_tipoFlujo = tipoFlujo;
	}

	public String getFlagR1() {
		return _flagR1;
	}

	public void setFlagR1(String flagR1) {
		_flagR1 = flagR1;
	}

	public String getFlagCampania() {
		return _flagCampania;
	}

	public void setFlagCampania(String flagCampania) {
		_flagCampania = flagCampania;
	}

	public String getFlagReniec() {
		return _flagReniec;
	}

	public void setFlagReniec(String flagReniec) {
		_flagReniec = flagReniec;
	}

	public String getFlagEquifax() {
		return _flagEquifax;
	}

	public void setFlagEquifax(String flagEquifax) {
		_flagEquifax = flagEquifax;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getTipoCampania() {
		return _tipoCampania;
	}

	public void setTipoCampania(String tipoCampania) {
		_tipoCampania = tipoCampania;
	}

	public String getFlagCalificacion() {
		return _flagCalificacion;
	}

	public void setFlagCalificacion(String flagCalificacion) {
		_flagCalificacion = flagCalificacion;
	}

	public String getFlagCliente() {
		return _flagCliente;
	}

	public void setFlagCliente(String flagCliente) {
		_flagCliente = flagCliente;
	}

	public int getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	public void setIdUsuarioSession(int idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;
	}

	public String getStrJson() {
		return _strJson;
	}

	public void setStrJson(String strJson) {
		_strJson = strJson;
	}

	public String getFormHtml() {
		return _formHtml;
	}

	public void setFormHtml(String formHtml) {
		_formHtml = formHtml;
	}

	public String getCodigoHtml() {
		return _codigoHtml;
	}

	public void setCodigoHtml(String codigoHtml) {
		_codigoHtml = codigoHtml;
	}

	public String getPeriodoRegistro() {
		return _periodoRegistro;
	}

	public void setPeriodoRegistro(String periodoRegistro) {
		_periodoRegistro = periodoRegistro;
	}

	public Date getModificacion() {
		return _modificacion;
	}

	public void setModificacion(Date modificacion) {
		_modificacion = modificacion;
	}

	public String getFlagTipificacion() {
		return _flagTipificacion;
	}

	public void setFlagTipificacion(String flagTipificacion) {
		_flagTipificacion = flagTipificacion;
	}

	public String getFlagActualizarRespuesta() {
		return _flagActualizarRespuesta;
	}

	public void setFlagActualizarRespuesta(String flagActualizarRespuesta) {
		_flagActualizarRespuesta = flagActualizarRespuesta;
	}

	public String getFlagObtenerInformacion() {
		return _flagObtenerInformacion;
	}

	public void setFlagObtenerInformacion(String flagObtenerInformacion) {
		_flagObtenerInformacion = flagObtenerInformacion;
	}

	public String getFlagConsultarAfiliacion() {
		return _flagConsultarAfiliacion;
	}

	public void setFlagConsultarAfiliacion(String flagConsultarAfiliacion) {
		_flagConsultarAfiliacion = flagConsultarAfiliacion;
	}

	public String getFlagGeneracionToken() {
		return _flagGeneracionToken;
	}

	public void setFlagGeneracionToken(String flagGeneracionToken) {
		_flagGeneracionToken = flagGeneracionToken;
	}

	public String getFlagVentaTC() {
		return _flagVentaTC;
	}

	public void setFlagVentaTC(String flagVentaTC) {
		_flagVentaTC = flagVentaTC;
	}

	public String getFlagValidacionToken() {
		return _flagValidacionToken;
	}

	public void setFlagValidacionToken(String flagValidacionToken) {
		_flagValidacionToken = flagValidacionToken;
	}

	public String getFlagRecompraGenPOTC() {
		return _flagRecompraGenPOTC;
	}

	public void setFlagRecompraGenPOTC(String flagRecompraGenPOTC) {
		_flagRecompraGenPOTC = flagRecompraGenPOTC;
	}

	public String getFlagRecompraConPOTC() {
		return _flagRecompraConPOTC;
	}

	public void setFlagRecompraConPOTC(String flagRecompraConPOTC) {
		_flagRecompraConPOTC = flagRecompraConPOTC;
	}

	public String getFlagExtornoGenPOTC() {
		return _flagExtornoGenPOTC;
	}

	public void setFlagExtornoGenPOTC(String flagExtornoGenPOTC) {
		_flagExtornoGenPOTC = flagExtornoGenPOTC;
	}

	public String getFlagExtornoConPOTC() {
		return _flagExtornoConPOTC;
	}

	public void setFlagExtornoConPOTC(String flagExtornoConPOTC) {
		_flagExtornoConPOTC = flagExtornoConPOTC;
	}

	public String getIp() {
		return _ip;
	}

	public void setIp(String ip) {
		_ip = ip;
	}

	public String getNavegador() {
		return _navegador;
	}

	public void setNavegador(String navegador) {
		_navegador = navegador;
	}

	public String getPepperType() {
		return _pepperType;
	}

	public void setPepperType(String pepperType) {
		_pepperType = pepperType;
	}

	private long _idExpediente;
	private String _documento;
	private String _estado;
	private String _paso;
	private String _pagina;
	private String _tipoFlujo;
	private String _flagR1;
	private String _flagCampania;
	private String _flagReniec;
	private String _flagEquifax;
	private Date _fechaRegistro;
	private String _tipoCampania;
	private String _flagCalificacion;
	private String _flagCliente;
	private int _idUsuarioSession;
	private String _strJson;
	private String _formHtml;
	private String _codigoHtml;
	private String _periodoRegistro;
	private Date _modificacion;
	private String _flagTipificacion;
	private String _flagActualizarRespuesta;
	private String _flagObtenerInformacion;
	private String _flagConsultarAfiliacion;
	private String _flagGeneracionToken;
	private String _flagVentaTC;
	private String _flagValidacionToken;
	private String _flagRecompraGenPOTC;
	private String _flagRecompraConPOTC;
	private String _flagExtornoGenPOTC;
	private String _flagExtornoConPOTC;
	private String _ip;
	private String _navegador;
	private String _pepperType;
}