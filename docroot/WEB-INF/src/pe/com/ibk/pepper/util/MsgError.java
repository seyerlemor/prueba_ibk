package pe.com.ibk.pepper.util;

public class MsgError {
	
	public static final String ERROR_GET_OPERADORES="ERROR al obtener la lista de operadores--: ";
	public static final String ERROR_GET_PARAMETROSERVMAP="ERROR al obtener la lista parametros por defecto para ws--: ";
	public static final String ERROR_GESTOR_SERV_LISTA_NEGRA="ERROR GestorServicio Lista Negra--: ";
	public static final String ERROR_GESTOR_SERV_CONSULTAR_RENIEC="ERROR GestorServicio consultarReniec--: ";
	public static final String ERROR_CONTROLADOR_SERV_VALIDAR_CUENTA="ERROR Controlador de validarCuenta--:";
	public static final String ERROR_CONTROLADOR_SERV_RENIEC="ERROR Controlador de RENIEC--:";
	public static final String ERROR_CONTROLADOR_SERV_VALIDAR_EQUIFAX="ERROR Controlador de EQUIFAX--:";
	public static final String ERROR_CONTROLADOR_SERV_DATOS_PERSONALES="ERROR Controlador datos personales--:";
	public static final String MSJ_ERROR_P1="ERROR [ ## ] Nombre del Util--: ";
	public static final String MSJ_ERROR_P2=" ,TRAZA ::: ";
	public static final String ERROR_GET_TARJETAS="ERROR al obtener la lista de tarjetas--: ";
	public static final String ERROR_GESTOR_SERV_FECHA_HORA="ERROR GestorServicio Consultar Fecha hora--: ";
	
	public static final String ERROR_GESTOR_SERV_CONSULTAR_PREGUNTAS_EQUIFAX="ERROR GestorServicio consultarPreguntasEquifax--: ";
	public static final String ERROR_GESTOR_SERV_VALIDAR_PREGUNTAS_EQUIFAX="ERROR GestorServicio validarPreguntasEquifax--: ";
	public static final String ERROR_GESTOR_SERV_OBTENER_INFORMACION_PERSONA="ERROR GestorServicio obtenerInformacionPersona--: ";
	public static final String ERROR_GESTOR_SERV_GENERACION_CLAVE="ERROR GestorServicio generarClave--: ";
	public static final String ERROR_GET_OCUPACION="ERROR al obtener la lista de ocupacion--: ";
	public static final String ERROR_GET_CARGO="ERROR al obtener la lista de cargo--: ";
	public static final String ERROR_GESTOR_SERV_VALIDAR_RUC="ERROR GestorServicio validarRuc--: ";
	public static final String ERROR_GET_TIPOVIA="ERROR al obtener tipo via--: ";
	public static final String ERROR_VALIDAR_RUC="ERROR Validar Ruc--:";
	public static final String ERROR_GESTOR_SERV_CONSULTAR_CAMPANIA="ERROR GestorServicio consultarCampania--: ";
	public static final String ERROR_NO_ENCONTRO_PARAMETROS="Error al traer datos sunat de Interbank";
	public static final String ERROR_GET_LABORAL="ERROR al obtener la lista de situacion laboral--: ";
	public static final String ERROR_GET_DEPARTAMENTO="ERROR al obtener la lista de departamento--: ";
	public static final String ERROR_GET_EST_CIVIL="ERROR al obtener la lista de estado civil--: ";
	public static final String ERROR_GESTOR_SERV_ANTIFRAUDE="ERROR GestorServicio Antifraude--: ";
	
	public static final String ERROR_PALABRA_NO_ENCONTRADA = "palabra no encontrada: ";
	public static final String ERROR_MENSAJE_SESSION_TRANSACCION = "--sessionTransaccion--";
	public static final String ERROR_MENSAJE_TRAZA_ERROR = " TrazaError:";
	public static final String ERROR_GESTOR_SERV_EVALUAR_CALIFIACION_CDA="ERROR GestorServicio evaluarCalificacionCDA--: ";
	public static final String ERROR_GESTOR_SERV_ACTUALIZAR_RESPUESTA="ERROR GestorServicio actualizarRespuesta--: ";
	public static final String ERROR_GESTOR_SERV_TIPIFICACION_INTENTOS="ERROR GestorServicio tipificacionRegistroIntento--: ";
	public static final String ERROR_GESTOR_SERV_CONSULTAR_AFILIACION="ERROR GestorServicio consultarAfiliacion--: ";
	public static final String ERROR_GESTOR_SERV_GENERAR_TOKEN="ERROR GestorServicio generarToken--: ";
	public static final String ERROR_GESTOR_SERV_VALIDAR_TOKEN="ERROR GestorServicio validarToken--: ";
	public static final String ERROR_GESTOR_SERV_VENTA_TC_PROVISIONAL="ERROR GestorServicio ventaTCProvisional--: ";
	public static final String ERROR_GESTOR_SERV_CONSULTAR_POTC="ERROR GestorServicio consultarPOTC--: ";
	public static final String ERROR_GESTOR_SERV_GENERAR_POTC="ERROR GestorServicio generarPOTC--: ";
	public static final String ERROR_GESTOR_SERV_CONSULTAR_PROV="ERROR GestorServicio consultarProvisional--: ";
	public static final String ERROR_GESTOR_SERV_OBT_TARJETA="ERROR GestorServicio obtenerTarjeta--: ";
	public static final String ERROR_GESTOR_SERV_CONS_TARJETA="ERROR GestorServicio consultarTarjeta--: ";
	public static final String ERROR_GESTOR_SERV_ENVIO_CORREO="ERROR GestorServicio envioCorreo--: ";
	public static final String ERROR_GESTOR_SERV_LOG_REINGRESO="ERROR GestorServicio logicaReingreso--: ";
	public static final String ERROR_GESTOR_SERV_CONSULTAR_EFL="ERROR GestorServicio consultarCalificacionEFL--: ";
	public static final String ERROR_GESTOR_SERV_REGISTRO_AUDITORIA="ERROR GestorServicio registroAuditoria--: ";
	
	private MsgError(){}
}
