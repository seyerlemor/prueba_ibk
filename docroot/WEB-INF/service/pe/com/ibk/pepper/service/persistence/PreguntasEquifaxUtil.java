/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.PreguntasEquifax;

import java.util.List;

/**
 * The persistence utility for the preguntas equifax service. This utility wraps {@link PreguntasEquifaxPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see PreguntasEquifaxPersistence
 * @see PreguntasEquifaxPersistenceImpl
 * @generated
 */
public class PreguntasEquifaxUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PreguntasEquifax preguntasEquifax) {
		getPersistence().clearCache(preguntasEquifax);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PreguntasEquifax> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PreguntasEquifax> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PreguntasEquifax> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static PreguntasEquifax update(PreguntasEquifax preguntasEquifax)
		throws SystemException {
		return getPersistence().update(preguntasEquifax);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static PreguntasEquifax update(PreguntasEquifax preguntasEquifax,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(preguntasEquifax, serviceContext);
	}

	/**
	* Returns the preguntas equifax where idPregunta = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchPreguntasEquifaxException} if it could not be found.
	*
	* @param idPregunta the id pregunta
	* @return the matching preguntas equifax
	* @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a matching preguntas equifax could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax findByPE_ID(
		long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchPreguntasEquifaxException {
		return getPersistence().findByPE_ID(idPregunta);
	}

	/**
	* Returns the preguntas equifax where idPregunta = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idPregunta the id pregunta
	* @return the matching preguntas equifax, or <code>null</code> if a matching preguntas equifax could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax fetchByPE_ID(
		long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPE_ID(idPregunta);
	}

	/**
	* Returns the preguntas equifax where idPregunta = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idPregunta the id pregunta
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching preguntas equifax, or <code>null</code> if a matching preguntas equifax could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax fetchByPE_ID(
		long idPregunta, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPE_ID(idPregunta, retrieveFromCache);
	}

	/**
	* Removes the preguntas equifax where idPregunta = &#63; from the database.
	*
	* @param idPregunta the id pregunta
	* @return the preguntas equifax that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax removeByPE_ID(
		long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchPreguntasEquifaxException {
		return getPersistence().removeByPE_ID(idPregunta);
	}

	/**
	* Returns the number of preguntas equifaxs where idPregunta = &#63;.
	*
	* @param idPregunta the id pregunta
	* @return the number of matching preguntas equifaxs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByPE_ID(long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPE_ID(idPregunta);
	}

	/**
	* Caches the preguntas equifax in the entity cache if it is enabled.
	*
	* @param preguntasEquifax the preguntas equifax
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.PreguntasEquifax preguntasEquifax) {
		getPersistence().cacheResult(preguntasEquifax);
	}

	/**
	* Caches the preguntas equifaxs in the entity cache if it is enabled.
	*
	* @param preguntasEquifaxs the preguntas equifaxs
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.PreguntasEquifax> preguntasEquifaxs) {
		getPersistence().cacheResult(preguntasEquifaxs);
	}

	/**
	* Creates a new preguntas equifax with the primary key. Does not add the preguntas equifax to the database.
	*
	* @param idPregunta the primary key for the new preguntas equifax
	* @return the new preguntas equifax
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax create(
		long idPregunta) {
		return getPersistence().create(idPregunta);
	}

	/**
	* Removes the preguntas equifax with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idPregunta the primary key of the preguntas equifax
	* @return the preguntas equifax that was removed
	* @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a preguntas equifax with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax remove(
		long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchPreguntasEquifaxException {
		return getPersistence().remove(idPregunta);
	}

	public static pe.com.ibk.pepper.model.PreguntasEquifax updateImpl(
		pe.com.ibk.pepper.model.PreguntasEquifax preguntasEquifax)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(preguntasEquifax);
	}

	/**
	* Returns the preguntas equifax with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchPreguntasEquifaxException} if it could not be found.
	*
	* @param idPregunta the primary key of the preguntas equifax
	* @return the preguntas equifax
	* @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a preguntas equifax with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax findByPrimaryKey(
		long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchPreguntasEquifaxException {
		return getPersistence().findByPrimaryKey(idPregunta);
	}

	/**
	* Returns the preguntas equifax with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idPregunta the primary key of the preguntas equifax
	* @return the preguntas equifax, or <code>null</code> if a preguntas equifax with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.PreguntasEquifax fetchByPrimaryKey(
		long idPregunta)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idPregunta);
	}

	/**
	* Returns all the preguntas equifaxs.
	*
	* @return the preguntas equifaxs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.PreguntasEquifax> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the preguntas equifaxs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.PreguntasEquifaxModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of preguntas equifaxs
	* @param end the upper bound of the range of preguntas equifaxs (not inclusive)
	* @return the range of preguntas equifaxs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.PreguntasEquifax> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the preguntas equifaxs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.PreguntasEquifaxModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of preguntas equifaxs
	* @param end the upper bound of the range of preguntas equifaxs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of preguntas equifaxs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.PreguntasEquifax> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the preguntas equifaxs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of preguntas equifaxs.
	*
	* @return the number of preguntas equifaxs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PreguntasEquifaxPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PreguntasEquifaxPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					PreguntasEquifaxPersistence.class.getName());

			ReferenceRegistry.registerReference(PreguntasEquifaxUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(PreguntasEquifaxPersistence persistence) {
	}

	private static PreguntasEquifaxPersistence _persistence;
}