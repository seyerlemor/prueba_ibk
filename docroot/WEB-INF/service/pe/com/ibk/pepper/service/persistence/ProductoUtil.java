/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.Producto;

import java.util.List;

/**
 * The persistence utility for the producto service. This utility wraps {@link ProductoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ProductoPersistence
 * @see ProductoPersistenceImpl
 * @generated
 */
public class ProductoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Producto producto) {
		getPersistence().clearCache(producto);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Producto> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Producto> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Producto> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Producto update(Producto producto) throws SystemException {
		return getPersistence().update(producto);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Producto update(Producto producto,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(producto, serviceContext);
	}

	/**
	* Returns the producto where idProducto = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	*
	* @param idProducto the id producto
	* @return the matching producto
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto findByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException {
		return getPersistence().findByPC_ID(idProducto);
	}

	/**
	* Returns the producto where idProducto = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idProducto the id producto
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto fetchByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPC_ID(idProducto);
	}

	/**
	* Returns the producto where idProducto = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idProducto the id producto
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto fetchByPC_ID(
		long idProducto, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPC_ID(idProducto, retrieveFromCache);
	}

	/**
	* Removes the producto where idProducto = &#63; from the database.
	*
	* @param idProducto the id producto
	* @return the producto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto removeByPC_ID(
		long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException {
		return getPersistence().removeByPC_ID(idProducto);
	}

	/**
	* Returns the number of productos where idProducto = &#63;.
	*
	* @param idProducto the id producto
	* @return the number of matching productos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPC_ID(idProducto);
	}

	/**
	* Returns the producto where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	*
	* @param idExpediente the id expediente
	* @return the matching producto
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto findByPC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException {
		return getPersistence().findByPC_E(idExpediente);
	}

	/**
	* Returns the producto where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idExpediente the id expediente
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto fetchByPC_E(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPC_E(idExpediente);
	}

	/**
	* Returns the producto where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idExpediente the id expediente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto fetchByPC_E(
		long idExpediente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPC_E(idExpediente, retrieveFromCache);
	}

	/**
	* Removes the producto where idExpediente = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @return the producto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto removeByPC_E(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException {
		return getPersistence().removeByPC_E(idExpediente);
	}

	/**
	* Returns the number of productos where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the number of matching productos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByPC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByPC_E(idExpediente);
	}

	/**
	* Caches the producto in the entity cache if it is enabled.
	*
	* @param producto the producto
	*/
	public static void cacheResult(pe.com.ibk.pepper.model.Producto producto) {
		getPersistence().cacheResult(producto);
	}

	/**
	* Caches the productos in the entity cache if it is enabled.
	*
	* @param productos the productos
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Producto> productos) {
		getPersistence().cacheResult(productos);
	}

	/**
	* Creates a new producto with the primary key. Does not add the producto to the database.
	*
	* @param idProducto the primary key for the new producto
	* @return the new producto
	*/
	public static pe.com.ibk.pepper.model.Producto create(long idProducto) {
		return getPersistence().create(idProducto);
	}

	/**
	* Removes the producto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProducto the primary key of the producto
	* @return the producto that was removed
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto remove(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException {
		return getPersistence().remove(idProducto);
	}

	public static pe.com.ibk.pepper.model.Producto updateImpl(
		pe.com.ibk.pepper.model.Producto producto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(producto);
	}

	/**
	* Returns the producto with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	*
	* @param idProducto the primary key of the producto
	* @return the producto
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto findByPrimaryKey(
		long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException {
		return getPersistence().findByPrimaryKey(idProducto);
	}

	/**
	* Returns the producto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idProducto the primary key of the producto
	* @return the producto, or <code>null</code> if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Producto fetchByPrimaryKey(
		long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idProducto);
	}

	/**
	* Returns all the productos.
	*
	* @return the productos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Producto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the productos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of productos
	* @param end the upper bound of the range of productos (not inclusive)
	* @return the range of productos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Producto> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the productos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of productos
	* @param end the upper bound of the range of productos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of productos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Producto> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the productos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of productos.
	*
	* @return the number of productos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ProductoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ProductoPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ProductoPersistence.class.getName());

			ReferenceRegistry.registerReference(ProductoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ProductoPersistence persistence) {
	}

	private static ProductoPersistence _persistence;
}