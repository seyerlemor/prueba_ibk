/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.Servicios;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Servicios in entity cache.
 *
 * @author Interbank
 * @see Servicios
 * @generated
 */
public class ServiciosCacheModel implements CacheModel<Servicios>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{idServicio=");
		sb.append(idServicio);
		sb.append(", idExpediente=");
		sb.append(idExpediente);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", codigo=");
		sb.append(codigo);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", response=");
		sb.append(response);
		sb.append(", messageBus=");
		sb.append(messageBus);
		sb.append(", tipo=");
		sb.append(tipo);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", respuestaInterna=");
		sb.append(respuestaInterna);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Servicios toEntityModel() {
		ServiciosImpl serviciosImpl = new ServiciosImpl();

		serviciosImpl.setIdServicio(idServicio);
		serviciosImpl.setIdExpediente(idExpediente);

		if (nombre == null) {
			serviciosImpl.setNombre(StringPool.BLANK);
		}
		else {
			serviciosImpl.setNombre(nombre);
		}

		if (codigo == null) {
			serviciosImpl.setCodigo(StringPool.BLANK);
		}
		else {
			serviciosImpl.setCodigo(codigo);
		}

		if (descripcion == null) {
			serviciosImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			serviciosImpl.setDescripcion(descripcion);
		}

		serviciosImpl.setResponse(response);

		if (messageBus == null) {
			serviciosImpl.setMessageBus(StringPool.BLANK);
		}
		else {
			serviciosImpl.setMessageBus(messageBus);
		}

		if (tipo == null) {
			serviciosImpl.setTipo(StringPool.BLANK);
		}
		else {
			serviciosImpl.setTipo(tipo);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			serviciosImpl.setFechaRegistro(null);
		}
		else {
			serviciosImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (respuestaInterna == null) {
			serviciosImpl.setRespuestaInterna(StringPool.BLANK);
		}
		else {
			serviciosImpl.setRespuestaInterna(respuestaInterna);
		}

		serviciosImpl.resetOriginalValues();

		return serviciosImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idServicio = objectInput.readLong();
		idExpediente = objectInput.readLong();
		nombre = objectInput.readUTF();
		codigo = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		response = objectInput.readLong();
		messageBus = objectInput.readUTF();
		tipo = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
		respuestaInterna = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idServicio);
		objectOutput.writeLong(idExpediente);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (codigo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigo);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		objectOutput.writeLong(response);

		if (messageBus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(messageBus);
		}

		if (tipo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipo);
		}

		objectOutput.writeLong(fechaRegistro);

		if (respuestaInterna == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(respuestaInterna);
		}
	}

	public long idServicio;
	public long idExpediente;
	public String nombre;
	public String codigo;
	public String descripcion;
	public long response;
	public String messageBus;
	public String tipo;
	public long fechaRegistro;
	public String respuestaInterna;
}