package pe.com.ibk.pepper.rest.config;

import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MultivaluedMap;

public class SecurityRestClientGeneralV2 {
	
	public static Builder getSecurityRestClient(String host, String path, String headerName, String headerValue, String tipo, MultivaluedMap<String, Object> headers){
		
		Builder builder = null;
		
		try {
			builder = SecurityRestClientV2.init(host, path, headerName, headerValue, tipo, headers);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return builder;
		
	}

}
