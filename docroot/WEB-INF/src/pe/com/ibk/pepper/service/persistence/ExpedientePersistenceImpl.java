/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchExpedienteException;
import pe.com.ibk.pepper.model.Expediente;
import pe.com.ibk.pepper.model.impl.ExpedienteImpl;
import pe.com.ibk.pepper.model.impl.ExpedienteModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the expediente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ExpedientePersistence
 * @see ExpedienteUtil
 * @generated
 */
public class ExpedientePersistenceImpl extends BasePersistenceImpl<Expediente>
	implements ExpedientePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ExpedienteUtil} to access the expediente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ExpedienteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, ExpedienteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, ExpedienteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_E_D_F = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, ExpedienteImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByE_D_F",
			new String[] { String.class.getName(), Date.class.getName() },
			ExpedienteModelImpl.DOCUMENTO_COLUMN_BITMASK |
			ExpedienteModelImpl.FECHAREGISTRO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_E_D_F = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByE_D_F",
			new String[] { String.class.getName(), Date.class.getName() });

	/**
	 * Returns the expediente where documento = &#63; and fechaRegistro = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	 *
	 * @param documento the documento
	 * @param fechaRegistro the fecha registro
	 * @return the matching expediente
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente findByE_D_F(String documento, Date fechaRegistro)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = fetchByE_D_F(documento, fechaRegistro);

		if (expediente == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("documento=");
			msg.append(documento);

			msg.append(", fechaRegistro=");
			msg.append(fechaRegistro);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchExpedienteException(msg.toString());
		}

		return expediente;
	}

	/**
	 * Returns the expediente where documento = &#63; and fechaRegistro = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param documento the documento
	 * @param fechaRegistro the fecha registro
	 * @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByE_D_F(String documento, Date fechaRegistro)
		throws SystemException {
		return fetchByE_D_F(documento, fechaRegistro, true);
	}

	/**
	 * Returns the expediente where documento = &#63; and fechaRegistro = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param documento the documento
	 * @param fechaRegistro the fecha registro
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByE_D_F(String documento, Date fechaRegistro,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { documento, fechaRegistro };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_E_D_F,
					finderArgs, this);
		}

		if (result instanceof Expediente) {
			Expediente expediente = (Expediente)result;

			if (!Validator.equals(documento, expediente.getDocumento()) ||
					!Validator.equals(fechaRegistro,
						expediente.getFechaRegistro())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_EXPEDIENTE_WHERE);

			boolean bindDocumento = false;

			if (documento == null) {
				query.append(_FINDER_COLUMN_E_D_F_DOCUMENTO_1);
			}
			else if (documento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_E_D_F_DOCUMENTO_3);
			}
			else {
				bindDocumento = true;

				query.append(_FINDER_COLUMN_E_D_F_DOCUMENTO_2);
			}

			boolean bindFechaRegistro = false;

			if (fechaRegistro == null) {
				query.append(_FINDER_COLUMN_E_D_F_FECHAREGISTRO_1);
			}
			else {
				bindFechaRegistro = true;

				query.append(_FINDER_COLUMN_E_D_F_FECHAREGISTRO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDocumento) {
					qPos.add(documento);
				}

				if (bindFechaRegistro) {
					qPos.add(CalendarUtil.getTimestamp(fechaRegistro));
				}

				List<Expediente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_F,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ExpedientePersistenceImpl.fetchByE_D_F(String, Date, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Expediente expediente = list.get(0);

					result = expediente;

					cacheResult(expediente);

					if ((expediente.getDocumento() == null) ||
							!expediente.getDocumento().equals(documento) ||
							(expediente.getFechaRegistro() == null) ||
							!expediente.getFechaRegistro().equals(fechaRegistro)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_F,
							finderArgs, expediente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_D_F,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Expediente)result;
		}
	}

	/**
	 * Removes the expediente where documento = &#63; and fechaRegistro = &#63; from the database.
	 *
	 * @param documento the documento
	 * @param fechaRegistro the fecha registro
	 * @return the expediente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente removeByE_D_F(String documento, Date fechaRegistro)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = findByE_D_F(documento, fechaRegistro);

		return remove(expediente);
	}

	/**
	 * Returns the number of expedientes where documento = &#63; and fechaRegistro = &#63;.
	 *
	 * @param documento the documento
	 * @param fechaRegistro the fecha registro
	 * @return the number of matching expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByE_D_F(String documento, Date fechaRegistro)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_E_D_F;

		Object[] finderArgs = new Object[] { documento, fechaRegistro };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EXPEDIENTE_WHERE);

			boolean bindDocumento = false;

			if (documento == null) {
				query.append(_FINDER_COLUMN_E_D_F_DOCUMENTO_1);
			}
			else if (documento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_E_D_F_DOCUMENTO_3);
			}
			else {
				bindDocumento = true;

				query.append(_FINDER_COLUMN_E_D_F_DOCUMENTO_2);
			}

			boolean bindFechaRegistro = false;

			if (fechaRegistro == null) {
				query.append(_FINDER_COLUMN_E_D_F_FECHAREGISTRO_1);
			}
			else {
				bindFechaRegistro = true;

				query.append(_FINDER_COLUMN_E_D_F_FECHAREGISTRO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDocumento) {
					qPos.add(documento);
				}

				if (bindFechaRegistro) {
					qPos.add(CalendarUtil.getTimestamp(fechaRegistro));
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_E_D_F_DOCUMENTO_1 = "expediente.documento IS NULL AND ";
	private static final String _FINDER_COLUMN_E_D_F_DOCUMENTO_2 = "expediente.documento = ? AND ";
	private static final String _FINDER_COLUMN_E_D_F_DOCUMENTO_3 = "(expediente.documento IS NULL OR expediente.documento = '') AND ";
	private static final String _FINDER_COLUMN_E_D_F_FECHAREGISTRO_1 = "expediente.fechaRegistro IS NULL";
	private static final String _FINDER_COLUMN_E_D_F_FECHAREGISTRO_2 = "expediente.fechaRegistro = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_E_D_P = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, ExpedienteImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByE_D_P",
			new String[] { String.class.getName(), String.class.getName() },
			ExpedienteModelImpl.DOCUMENTO_COLUMN_BITMASK |
			ExpedienteModelImpl.PERIODOREGISTRO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_E_D_P = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByE_D_P",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns the expediente where documento = &#63; and periodoRegistro = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	 *
	 * @param documento the documento
	 * @param periodoRegistro the periodo registro
	 * @return the matching expediente
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente findByE_D_P(String documento, String periodoRegistro)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = fetchByE_D_P(documento, periodoRegistro);

		if (expediente == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("documento=");
			msg.append(documento);

			msg.append(", periodoRegistro=");
			msg.append(periodoRegistro);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchExpedienteException(msg.toString());
		}

		return expediente;
	}

	/**
	 * Returns the expediente where documento = &#63; and periodoRegistro = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param documento the documento
	 * @param periodoRegistro the periodo registro
	 * @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByE_D_P(String documento, String periodoRegistro)
		throws SystemException {
		return fetchByE_D_P(documento, periodoRegistro, true);
	}

	/**
	 * Returns the expediente where documento = &#63; and periodoRegistro = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param documento the documento
	 * @param periodoRegistro the periodo registro
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByE_D_P(String documento, String periodoRegistro,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { documento, periodoRegistro };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_E_D_P,
					finderArgs, this);
		}

		if (result instanceof Expediente) {
			Expediente expediente = (Expediente)result;

			if (!Validator.equals(documento, expediente.getDocumento()) ||
					!Validator.equals(periodoRegistro,
						expediente.getPeriodoRegistro())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_EXPEDIENTE_WHERE);

			boolean bindDocumento = false;

			if (documento == null) {
				query.append(_FINDER_COLUMN_E_D_P_DOCUMENTO_1);
			}
			else if (documento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_E_D_P_DOCUMENTO_3);
			}
			else {
				bindDocumento = true;

				query.append(_FINDER_COLUMN_E_D_P_DOCUMENTO_2);
			}

			boolean bindPeriodoRegistro = false;

			if (periodoRegistro == null) {
				query.append(_FINDER_COLUMN_E_D_P_PERIODOREGISTRO_1);
			}
			else if (periodoRegistro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_E_D_P_PERIODOREGISTRO_3);
			}
			else {
				bindPeriodoRegistro = true;

				query.append(_FINDER_COLUMN_E_D_P_PERIODOREGISTRO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDocumento) {
					qPos.add(documento);
				}

				if (bindPeriodoRegistro) {
					qPos.add(periodoRegistro);
				}

				List<Expediente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_P,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ExpedientePersistenceImpl.fetchByE_D_P(String, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Expediente expediente = list.get(0);

					result = expediente;

					cacheResult(expediente);

					if ((expediente.getDocumento() == null) ||
							!expediente.getDocumento().equals(documento) ||
							(expediente.getPeriodoRegistro() == null) ||
							!expediente.getPeriodoRegistro()
										   .equals(periodoRegistro)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_P,
							finderArgs, expediente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_D_P,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Expediente)result;
		}
	}

	/**
	 * Removes the expediente where documento = &#63; and periodoRegistro = &#63; from the database.
	 *
	 * @param documento the documento
	 * @param periodoRegistro the periodo registro
	 * @return the expediente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente removeByE_D_P(String documento, String periodoRegistro)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = findByE_D_P(documento, periodoRegistro);

		return remove(expediente);
	}

	/**
	 * Returns the number of expedientes where documento = &#63; and periodoRegistro = &#63;.
	 *
	 * @param documento the documento
	 * @param periodoRegistro the periodo registro
	 * @return the number of matching expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByE_D_P(String documento, String periodoRegistro)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_E_D_P;

		Object[] finderArgs = new Object[] { documento, periodoRegistro };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EXPEDIENTE_WHERE);

			boolean bindDocumento = false;

			if (documento == null) {
				query.append(_FINDER_COLUMN_E_D_P_DOCUMENTO_1);
			}
			else if (documento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_E_D_P_DOCUMENTO_3);
			}
			else {
				bindDocumento = true;

				query.append(_FINDER_COLUMN_E_D_P_DOCUMENTO_2);
			}

			boolean bindPeriodoRegistro = false;

			if (periodoRegistro == null) {
				query.append(_FINDER_COLUMN_E_D_P_PERIODOREGISTRO_1);
			}
			else if (periodoRegistro.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_E_D_P_PERIODOREGISTRO_3);
			}
			else {
				bindPeriodoRegistro = true;

				query.append(_FINDER_COLUMN_E_D_P_PERIODOREGISTRO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDocumento) {
					qPos.add(documento);
				}

				if (bindPeriodoRegistro) {
					qPos.add(periodoRegistro);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_E_D_P_DOCUMENTO_1 = "expediente.documento IS NULL AND ";
	private static final String _FINDER_COLUMN_E_D_P_DOCUMENTO_2 = "expediente.documento = ? AND ";
	private static final String _FINDER_COLUMN_E_D_P_DOCUMENTO_3 = "(expediente.documento IS NULL OR expediente.documento = '') AND ";
	private static final String _FINDER_COLUMN_E_D_P_PERIODOREGISTRO_1 = "expediente.periodoRegistro IS NULL";
	private static final String _FINDER_COLUMN_E_D_P_PERIODOREGISTRO_2 = "expediente.periodoRegistro = ?";
	private static final String _FINDER_COLUMN_E_D_P_PERIODOREGISTRO_3 = "(expediente.periodoRegistro IS NULL OR expediente.periodoRegistro = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_E_ID = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, ExpedienteImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByE_ID",
			new String[] { Long.class.getName() },
			ExpedienteModelImpl.IDEXPEDIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_E_ID = new FinderPath(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByE_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the expediente where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching expediente
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente findByE_ID(long idExpediente)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = fetchByE_ID(idExpediente);

		if (expediente == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idExpediente=");
			msg.append(idExpediente);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchExpedienteException(msg.toString());
		}

		return expediente;
	}

	/**
	 * Returns the expediente where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByE_ID(long idExpediente) throws SystemException {
		return fetchByE_ID(idExpediente, true);
	}

	/**
	 * Returns the expediente where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idExpediente the id expediente
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByE_ID(long idExpediente, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idExpediente };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_E_ID,
					finderArgs, this);
		}

		if (result instanceof Expediente) {
			Expediente expediente = (Expediente)result;

			if ((idExpediente != expediente.getIdExpediente())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_EXPEDIENTE_WHERE);

			query.append(_FINDER_COLUMN_E_ID_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				List<Expediente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ExpedientePersistenceImpl.fetchByE_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Expediente expediente = list.get(0);

					result = expediente;

					cacheResult(expediente);

					if ((expediente.getIdExpediente() != idExpediente)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_ID,
							finderArgs, expediente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Expediente)result;
		}
	}

	/**
	 * Removes the expediente where idExpediente = &#63; from the database.
	 *
	 * @param idExpediente the id expediente
	 * @return the expediente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente removeByE_ID(long idExpediente)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = findByE_ID(idExpediente);

		return remove(expediente);
	}

	/**
	 * Returns the number of expedientes where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @return the number of matching expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByE_ID(long idExpediente) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_E_ID;

		Object[] finderArgs = new Object[] { idExpediente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EXPEDIENTE_WHERE);

			query.append(_FINDER_COLUMN_E_ID_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_E_ID_IDEXPEDIENTE_2 = "expediente.idExpediente = ?";

	public ExpedientePersistenceImpl() {
		setModelClass(Expediente.class);
	}

	/**
	 * Caches the expediente in the entity cache if it is enabled.
	 *
	 * @param expediente the expediente
	 */
	@Override
	public void cacheResult(Expediente expediente) {
		EntityCacheUtil.putResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteImpl.class, expediente.getPrimaryKey(), expediente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_F,
			new Object[] {
				expediente.getDocumento(), expediente.getFechaRegistro()
			}, expediente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_P,
			new Object[] {
				expediente.getDocumento(), expediente.getPeriodoRegistro()
			}, expediente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_ID,
			new Object[] { expediente.getIdExpediente() }, expediente);

		expediente.resetOriginalValues();
	}

	/**
	 * Caches the expedientes in the entity cache if it is enabled.
	 *
	 * @param expedientes the expedientes
	 */
	@Override
	public void cacheResult(List<Expediente> expedientes) {
		for (Expediente expediente : expedientes) {
			if (EntityCacheUtil.getResult(
						ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
						ExpedienteImpl.class, expediente.getPrimaryKey()) == null) {
				cacheResult(expediente);
			}
			else {
				expediente.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all expedientes.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ExpedienteImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ExpedienteImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the expediente.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Expediente expediente) {
		EntityCacheUtil.removeResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteImpl.class, expediente.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(expediente);
	}

	@Override
	public void clearCache(List<Expediente> expedientes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Expediente expediente : expedientes) {
			EntityCacheUtil.removeResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
				ExpedienteImpl.class, expediente.getPrimaryKey());

			clearUniqueFindersCache(expediente);
		}
	}

	protected void cacheUniqueFindersCache(Expediente expediente) {
		if (expediente.isNew()) {
			Object[] args = new Object[] {
					expediente.getDocumento(), expediente.getFechaRegistro()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_E_D_F, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_F, args,
				expediente);

			args = new Object[] {
					expediente.getDocumento(), expediente.getPeriodoRegistro()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_E_D_P, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_P, args,
				expediente);

			args = new Object[] { expediente.getIdExpediente() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_E_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_ID, args,
				expediente);
		}
		else {
			ExpedienteModelImpl expedienteModelImpl = (ExpedienteModelImpl)expediente;

			if ((expedienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_E_D_F.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						expediente.getDocumento(), expediente.getFechaRegistro()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_E_D_F, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_F, args,
					expediente);
			}

			if ((expedienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_E_D_P.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						expediente.getDocumento(),
						expediente.getPeriodoRegistro()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_E_D_P, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_D_P, args,
					expediente);
			}

			if ((expedienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_E_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { expediente.getIdExpediente() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_E_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_E_ID, args,
					expediente);
			}
		}
	}

	protected void clearUniqueFindersCache(Expediente expediente) {
		ExpedienteModelImpl expedienteModelImpl = (ExpedienteModelImpl)expediente;

		Object[] args = new Object[] {
				expediente.getDocumento(), expediente.getFechaRegistro()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_E_D_F, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_D_F, args);

		if ((expedienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_E_D_F.getColumnBitmask()) != 0) {
			args = new Object[] {
					expedienteModelImpl.getOriginalDocumento(),
					expedienteModelImpl.getOriginalFechaRegistro()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_E_D_F, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_D_F, args);
		}

		args = new Object[] {
				expediente.getDocumento(), expediente.getPeriodoRegistro()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_E_D_P, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_D_P, args);

		if ((expedienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_E_D_P.getColumnBitmask()) != 0) {
			args = new Object[] {
					expedienteModelImpl.getOriginalDocumento(),
					expedienteModelImpl.getOriginalPeriodoRegistro()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_E_D_P, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_D_P, args);
		}

		args = new Object[] { expediente.getIdExpediente() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_E_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_ID, args);

		if ((expedienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_E_ID.getColumnBitmask()) != 0) {
			args = new Object[] { expedienteModelImpl.getOriginalIdExpediente() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_E_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_E_ID, args);
		}
	}

	/**
	 * Creates a new expediente with the primary key. Does not add the expediente to the database.
	 *
	 * @param idExpediente the primary key for the new expediente
	 * @return the new expediente
	 */
	@Override
	public Expediente create(long idExpediente) {
		Expediente expediente = new ExpedienteImpl();

		expediente.setNew(true);
		expediente.setPrimaryKey(idExpediente);

		return expediente;
	}

	/**
	 * Removes the expediente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idExpediente the primary key of the expediente
	 * @return the expediente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a expediente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente remove(long idExpediente)
		throws NoSuchExpedienteException, SystemException {
		return remove((Serializable)idExpediente);
	}

	/**
	 * Removes the expediente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the expediente
	 * @return the expediente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a expediente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente remove(Serializable primaryKey)
		throws NoSuchExpedienteException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Expediente expediente = (Expediente)session.get(ExpedienteImpl.class,
					primaryKey);

			if (expediente == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchExpedienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(expediente);
		}
		catch (NoSuchExpedienteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Expediente removeImpl(Expediente expediente)
		throws SystemException {
		expediente = toUnwrappedModel(expediente);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(expediente)) {
				expediente = (Expediente)session.get(ExpedienteImpl.class,
						expediente.getPrimaryKeyObj());
			}

			if (expediente != null) {
				session.delete(expediente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (expediente != null) {
			clearCache(expediente);
		}

		return expediente;
	}

	@Override
	public Expediente updateImpl(pe.com.ibk.pepper.model.Expediente expediente)
		throws SystemException {
		expediente = toUnwrappedModel(expediente);

		boolean isNew = expediente.isNew();

		Session session = null;

		try {
			session = openSession();

			if (expediente.isNew()) {
				session.save(expediente);

				expediente.setNew(false);
			}
			else {
				session.merge(expediente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ExpedienteModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
			ExpedienteImpl.class, expediente.getPrimaryKey(), expediente);

		clearUniqueFindersCache(expediente);
		cacheUniqueFindersCache(expediente);

		return expediente;
	}

	protected Expediente toUnwrappedModel(Expediente expediente) {
		if (expediente instanceof ExpedienteImpl) {
			return expediente;
		}

		ExpedienteImpl expedienteImpl = new ExpedienteImpl();

		expedienteImpl.setNew(expediente.isNew());
		expedienteImpl.setPrimaryKey(expediente.getPrimaryKey());

		expedienteImpl.setIdExpediente(expediente.getIdExpediente());
		expedienteImpl.setDocumento(expediente.getDocumento());
		expedienteImpl.setEstado(expediente.getEstado());
		expedienteImpl.setPaso(expediente.getPaso());
		expedienteImpl.setPagina(expediente.getPagina());
		expedienteImpl.setTipoFlujo(expediente.getTipoFlujo());
		expedienteImpl.setFlagR1(expediente.getFlagR1());
		expedienteImpl.setFlagCampania(expediente.getFlagCampania());
		expedienteImpl.setFlagReniec(expediente.getFlagReniec());
		expedienteImpl.setFlagEquifax(expediente.getFlagEquifax());
		expedienteImpl.setFechaRegistro(expediente.getFechaRegistro());
		expedienteImpl.setTipoCampania(expediente.getTipoCampania());
		expedienteImpl.setFlagCalificacion(expediente.getFlagCalificacion());
		expedienteImpl.setFlagCliente(expediente.getFlagCliente());
		expedienteImpl.setIdUsuarioSession(expediente.getIdUsuarioSession());
		expedienteImpl.setStrJson(expediente.getStrJson());
		expedienteImpl.setFormHtml(expediente.getFormHtml());
		expedienteImpl.setCodigoHtml(expediente.getCodigoHtml());
		expedienteImpl.setPeriodoRegistro(expediente.getPeriodoRegistro());
		expedienteImpl.setModificacion(expediente.getModificacion());
		expedienteImpl.setFlagTipificacion(expediente.getFlagTipificacion());
		expedienteImpl.setFlagActualizarRespuesta(expediente.getFlagActualizarRespuesta());
		expedienteImpl.setFlagObtenerInformacion(expediente.getFlagObtenerInformacion());
		expedienteImpl.setFlagConsultarAfiliacion(expediente.getFlagConsultarAfiliacion());
		expedienteImpl.setFlagGeneracionToken(expediente.getFlagGeneracionToken());
		expedienteImpl.setFlagVentaTC(expediente.getFlagVentaTC());
		expedienteImpl.setFlagValidacionToken(expediente.getFlagValidacionToken());
		expedienteImpl.setFlagRecompraGenPOTC(expediente.getFlagRecompraGenPOTC());
		expedienteImpl.setFlagRecompraConPOTC(expediente.getFlagRecompraConPOTC());
		expedienteImpl.setFlagExtornoGenPOTC(expediente.getFlagExtornoGenPOTC());
		expedienteImpl.setFlagExtornoConPOTC(expediente.getFlagExtornoConPOTC());
		expedienteImpl.setIp(expediente.getIp());
		expedienteImpl.setNavegador(expediente.getNavegador());
		expedienteImpl.setPepperType(expediente.getPepperType());

		return expedienteImpl;
	}

	/**
	 * Returns the expediente with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the expediente
	 * @return the expediente
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a expediente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente findByPrimaryKey(Serializable primaryKey)
		throws NoSuchExpedienteException, SystemException {
		Expediente expediente = fetchByPrimaryKey(primaryKey);

		if (expediente == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchExpedienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return expediente;
	}

	/**
	 * Returns the expediente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	 *
	 * @param idExpediente the primary key of the expediente
	 * @return the expediente
	 * @throws pe.com.ibk.pepper.NoSuchExpedienteException if a expediente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente findByPrimaryKey(long idExpediente)
		throws NoSuchExpedienteException, SystemException {
		return findByPrimaryKey((Serializable)idExpediente);
	}

	/**
	 * Returns the expediente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the expediente
	 * @return the expediente, or <code>null</code> if a expediente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Expediente expediente = (Expediente)EntityCacheUtil.getResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
				ExpedienteImpl.class, primaryKey);

		if (expediente == _nullExpediente) {
			return null;
		}

		if (expediente == null) {
			Session session = null;

			try {
				session = openSession();

				expediente = (Expediente)session.get(ExpedienteImpl.class,
						primaryKey);

				if (expediente != null) {
					cacheResult(expediente);
				}
				else {
					EntityCacheUtil.putResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
						ExpedienteImpl.class, primaryKey, _nullExpediente);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ExpedienteModelImpl.ENTITY_CACHE_ENABLED,
					ExpedienteImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return expediente;
	}

	/**
	 * Returns the expediente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idExpediente the primary key of the expediente
	 * @return the expediente, or <code>null</code> if a expediente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Expediente fetchByPrimaryKey(long idExpediente)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idExpediente);
	}

	/**
	 * Returns all the expedientes.
	 *
	 * @return the expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Expediente> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the expedientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of expedientes
	 * @param end the upper bound of the range of expedientes (not inclusive)
	 * @return the range of expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Expediente> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the expedientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of expedientes
	 * @param end the upper bound of the range of expedientes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Expediente> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Expediente> list = (List<Expediente>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EXPEDIENTE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EXPEDIENTE;

				if (pagination) {
					sql = sql.concat(ExpedienteModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Expediente>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Expediente>(list);
				}
				else {
					list = (List<Expediente>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the expedientes from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Expediente expediente : findAll()) {
			remove(expediente);
		}
	}

	/**
	 * Returns the number of expedientes.
	 *
	 * @return the number of expedientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EXPEDIENTE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the expediente persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.Expediente")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Expediente>> listenersList = new ArrayList<ModelListener<Expediente>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Expediente>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ExpedienteImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_EXPEDIENTE = "SELECT expediente FROM Expediente expediente";
	private static final String _SQL_SELECT_EXPEDIENTE_WHERE = "SELECT expediente FROM Expediente expediente WHERE ";
	private static final String _SQL_COUNT_EXPEDIENTE = "SELECT COUNT(expediente) FROM Expediente expediente";
	private static final String _SQL_COUNT_EXPEDIENTE_WHERE = "SELECT COUNT(expediente) FROM Expediente expediente WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "expediente.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Expediente exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Expediente exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ExpedientePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idExpediente", "documento", "estado", "paso", "pagina",
				"tipoFlujo", "flagR1", "flagCampania", "flagReniec",
				"flagEquifax", "fechaRegistro", "tipoCampania",
				"flagCalificacion", "flagCliente", "idUsuarioSession", "strJson",
				"formHtml", "codigoHtml", "periodoRegistro", "modificacion",
				"flagTipificacion", "flagActualizarRespuesta",
				"flagObtenerInformacion", "flagConsultarAfiliacion",
				"flagGeneracionToken", "flagVentaTC", "flagValidacionToken",
				"flagRecompraGenPOTC", "flagRecompraConPOTC",
				"flagExtornoGenPOTC", "flagExtornoConPOTC", "ip", "navegador",
				"pepperType"
			});
	private static Expediente _nullExpediente = new ExpedienteImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Expediente> toCacheModel() {
				return _nullExpedienteCacheModel;
			}
		};

	private static CacheModel<Expediente> _nullExpedienteCacheModel = new CacheModel<Expediente>() {
			@Override
			public Expediente toEntityModel() {
				return _nullExpediente;
			}
		};
}