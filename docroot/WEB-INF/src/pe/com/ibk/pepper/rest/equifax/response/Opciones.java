
package pe.com.ibk.pepper.rest.equifax.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Opciones implements Serializable {

	private static final long serialVersionUID = -1426118941056609116L;
	
	@SerializedName("opcion")
	private List<Opcion> opcion;

	public List<Opcion> getOpcion() {
		return opcion;
	}

	public void setOpcion(List<Opcion> opcion) {
		this.opcion = opcion;
	}
    
}
