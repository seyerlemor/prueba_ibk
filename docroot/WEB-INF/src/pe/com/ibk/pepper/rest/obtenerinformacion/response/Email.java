package pe.com.ibk.pepper.rest.obtenerinformacion.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Email implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@SerializedName("subtype")
	private String subtype;
	
	@SerializedName("email")
	private String email;

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
