
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DatosToken {

    private String operador;

    /**
     * 
     * @param operador
     */
    public DatosToken(String operador) {
        super();
        this.operador = operador;
    }

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	@Override
	public String toString() {
		return "DatosToken [operador=" + operador + "]";
	}

}
