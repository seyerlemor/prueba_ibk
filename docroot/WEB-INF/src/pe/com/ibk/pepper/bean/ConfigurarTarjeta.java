package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class ConfigurarTarjeta implements Serializable  {

	private static final long serialVersionUID = 1L;
	private String nombre;
	private String diaDePago;
	private String deliveryaddress;
	private String addressDepartamento;
	private String addressProvincia;
	private String addressDistrito; 
	private String addressType;
	private String direccionEntregaDescripcionTipovia;
	private String addressName;
	private String addressNro;
	private String addressMz;
	private String addressLt;
	private String addressInt;
	private String emailSaleConstancy;
	private String estadoCuentaCboxEmail;
	private String emailStateAccount;
	private String estadoCuentaCboxDirecFisica;
	private String direccionEntregaEstadoCuenta;
	private String estadoCuentaDepartamentoH;
	private String estadoCuentaProvinciaH;
	private String estadoCuentaDistritoH;
	private String estadoCuentaTipovia;
	private String estadoCuentaDescripcionTipovia;
	private String estadoCuentaNomvia;
	private String estadoCuentaNumero;
	private String estadoCuentaMzna;
	private String estadoCuentaLote;
	private String estadoCuentaInterior;
	private String cboxSuroTc;
	private String politicasTratamientoDatos;
	private String creditLine;
	private String deliverydate;
	private String deliveryTime;

	
	public String getCreditLine() {
		return creditLine;
	}
	public void setCreditLine(String creditLine) {
		this.creditLine = creditLine;
	}
	public String getDireccionEntregaDescripcionTipovia() {
		return direccionEntregaDescripcionTipovia;
	}
	public void setDireccionEntregaDescripcionTipovia(
			String direccionEntregaDescripcionTipovia) {
		this.direccionEntregaDescripcionTipovia = direccionEntregaDescripcionTipovia;
	}
	public String getEstadoCuentaDescripcionTipovia() {
		return estadoCuentaDescripcionTipovia;
	}
	public void setEstadoCuentaDescripcionTipovia(
			String estadoCuentaDescripcionTipovia) {
		this.estadoCuentaDescripcionTipovia = estadoCuentaDescripcionTipovia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDiaDePago() {
		return diaDePago;
	}
	public void setDiaDePago(String diaDePago) {
		this.diaDePago = diaDePago;
	}
	public String getEstadoCuentaCboxEmail() {
		return estadoCuentaCboxEmail;
	}
	public void setEstadoCuentaCboxEmail(String estadoCuentaCboxEmail) {
		this.estadoCuentaCboxEmail = estadoCuentaCboxEmail;
	}
	public String getEstadoCuentaCboxDirecFisica() {
		return estadoCuentaCboxDirecFisica;
	}
	public void setEstadoCuentaCboxDirecFisica(String estadoCuentaCboxDirecFisica) {
		this.estadoCuentaCboxDirecFisica = estadoCuentaCboxDirecFisica;
	}
	public String getDireccionEntregaEstadoCuenta() {
		return direccionEntregaEstadoCuenta;
	}
	public void setDireccionEntregaEstadoCuenta(String direccionEntregaEstadoCuenta) {
		this.direccionEntregaEstadoCuenta = direccionEntregaEstadoCuenta;
	}
	public String getEstadoCuentaTipovia() {
		return estadoCuentaTipovia;
	}
	public void setEstadoCuentaTipovia(String estadoCuentaTipovia) {
		this.estadoCuentaTipovia = estadoCuentaTipovia;
	}
	public String getEstadoCuentaNomvia() {
		return estadoCuentaNomvia;
	}
	public void setEstadoCuentaNomvia(String estadoCuentaNomvia) {
		this.estadoCuentaNomvia = estadoCuentaNomvia;
	}
	public String getEstadoCuentaNumero() {
		return estadoCuentaNumero;
	}
	public void setEstadoCuentaNumero(String estadoCuentaNumero) {
		this.estadoCuentaNumero = estadoCuentaNumero;
	}
	public String getEstadoCuentaMzna() {
		return estadoCuentaMzna;
	}
	public void setEstadoCuentaMzna(String estadoCuentaMzna) {
		this.estadoCuentaMzna = estadoCuentaMzna;
	}
	public String getEstadoCuentaLote() {
		return estadoCuentaLote;
	}
	public void setEstadoCuentaLote(String estadoCuentaLote) {
		this.estadoCuentaLote = estadoCuentaLote;
	}
	public String getEstadoCuentaInterior() {
		return estadoCuentaInterior;
	}
	public void setEstadoCuentaInterior(String estadoCuentaInterior) {
		this.estadoCuentaInterior = estadoCuentaInterior;
	}
	public String getCboxSuroTc() {
		return cboxSuroTc;
	}
	public void setCboxSuroTc(String cboxSuroTc) {
		this.cboxSuroTc = cboxSuroTc;
	}
	public String getPoliticasTratamientoDatos() {
		return politicasTratamientoDatos;
	}
	public void setPoliticasTratamientoDatos(String politicasTratamientoDatos) {
		this.politicasTratamientoDatos = politicasTratamientoDatos;
	}
	public String getEstadoCuentaDepartamentoH() {
		return estadoCuentaDepartamentoH;
	}
	public void setEstadoCuentaDepartamentoH(String estadoCuentaDepartamentoH) {
		this.estadoCuentaDepartamentoH = estadoCuentaDepartamentoH;
	}
	public String getEstadoCuentaProvinciaH() {
		return estadoCuentaProvinciaH;
	}
	public void setEstadoCuentaProvinciaH(String estadoCuentaProvinciaH) {
		this.estadoCuentaProvinciaH = estadoCuentaProvinciaH;
	}
	public String getEstadoCuentaDistritoH() {
		return estadoCuentaDistritoH;
	}
	public void setEstadoCuentaDistritoH(String estadoCuentaDistritoH) {
		this.estadoCuentaDistritoH = estadoCuentaDistritoH;
	}
	public String getAddressDepartamento() {
		return addressDepartamento;
	}
	public void setAddressDepartamento(String addressDepartamento) {
		this.addressDepartamento = addressDepartamento;
	}
	public String getAddressProvincia() {
		return addressProvincia;
	}
	public void setAddressProvincia(String addressProvincia) {
		this.addressProvincia = addressProvincia;
	}
	public String getAddressDistrito() {
		return addressDistrito;
	}
	public void setAddressDistrito(String addressDistrito) {
		this.addressDistrito = addressDistrito;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getAddressNro() {
		return addressNro;
	}
	public void setAddressNro(String addressNro) {
		this.addressNro = addressNro;
	}
	public String getAddressMz() {
		return addressMz;
	}
	public void setAddressMz(String addressMz) {
		this.addressMz = addressMz;
	}
	public String getAddressLt() {
		return addressLt;
	}
	public void setAddressLt(String addressLt) {
		this.addressLt = addressLt;
	}
	public String getAddressInt() {
		return addressInt;
	}
	public void setAddressInt(String addressInt) {
		this.addressInt = addressInt;
	}
	public String getEmailSaleConstancy() {
		return emailSaleConstancy;
	}
	public void setEmailSaleConstancy(String emailSaleConstancy) {
		this.emailSaleConstancy = emailSaleConstancy;
	}
	public String getEmailStateAccount() {
		return emailStateAccount;
	}
	public void setEmailStateAccount(String emailStateAccount) {
		this.emailStateAccount = emailStateAccount;
	}
	public String getDeliveryaddress() {
		return deliveryaddress;
	}
	public void setDeliveryaddress(String deliveryaddress) {
		this.deliveryaddress = deliveryaddress;
	}
	public String getDeliverydate() {
		return deliverydate;
	}
	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	@Override
	public String toString() {
		return "ConfigurarTarjeta [nombre=" + nombre + ", diaDePago="
				+ diaDePago + ", deliveryaddress=" + deliveryaddress
				+ ", addressDepartamento=" + addressDepartamento
				+ ", addressProvincia=" + addressProvincia
				+ ", addressDistrito=" + addressDistrito + ", addressType="
				+ addressType + ", direccionEntregaDescripcionTipovia="
				+ direccionEntregaDescripcionTipovia + ", addressName="
				+ addressName + ", addressNro=" + addressNro + ", addressMz="
				+ addressMz + ", addressLt=" + addressLt + ", addressInt="
				+ addressInt + ", emailSaleConstancy=" + emailSaleConstancy
				+ ", estadoCuentaCboxEmail=" + estadoCuentaCboxEmail
				+ ", emailStateAccount=" + emailStateAccount
				+ ", estadoCuentaCboxDirecFisica="
				+ estadoCuentaCboxDirecFisica
				+ ", direccionEntregaEstadoCuenta="
				+ direccionEntregaEstadoCuenta + ", estadoCuentaDepartamentoH="
				+ estadoCuentaDepartamentoH + ", estadoCuentaProvinciaH="
				+ estadoCuentaProvinciaH + ", estadoCuentaDistritoH="
				+ estadoCuentaDistritoH + ", estadoCuentaTipovia="
				+ estadoCuentaTipovia + ", estadoCuentaDescripcionTipovia="
				+ estadoCuentaDescripcionTipovia + ", estadoCuentaNomvia="
				+ estadoCuentaNomvia + ", estadoCuentaNumero="
				+ estadoCuentaNumero + ", estadoCuentaMzna=" + estadoCuentaMzna
				+ ", estadoCuentaLote=" + estadoCuentaLote
				+ ", estadoCuentaInterior=" + estadoCuentaInterior
				+ ", cboxSuroTc=" + cboxSuroTc + ", politicasTratamientoDatos="
				+ politicasTratamientoDatos + ", creditLine="
				+ creditLine + ", deliverydate=" + deliverydate
				+ ", deliveryTime=" + deliveryTime + "]";
	}
	
}
