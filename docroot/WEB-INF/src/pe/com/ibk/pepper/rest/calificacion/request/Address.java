
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Address {

    private String departament;
    private String province;
    private String district;
    
    public Address() {
		super();
	}

	@JsonCreator
	public Address(String departament, String province, String district) {
		super();
		this.departament = departament;
		this.province = province;
		this.district = district;
	}

    @JsonProperty("departament")
	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	@JsonProperty("province")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@JsonProperty("district")
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

}
