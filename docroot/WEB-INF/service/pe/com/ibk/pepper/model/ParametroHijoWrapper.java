/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ParametroHijo}.
 * </p>
 *
 * @author Interbank
 * @see ParametroHijo
 * @generated
 */
public class ParametroHijoWrapper implements ParametroHijo,
	ModelWrapper<ParametroHijo> {
	public ParametroHijoWrapper(ParametroHijo parametroHijo) {
		_parametroHijo = parametroHijo;
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroHijo.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroHijo.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroHijo", getIdParametroHijo());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("codigo", getCodigo());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("orden", getOrden());
		attributes.put("dato1", getDato1());
		attributes.put("dato2", getDato2());
		attributes.put("dato3", getDato3());
		attributes.put("dato4", getDato4());
		attributes.put("dato5", getDato5());
		attributes.put("dato6", getDato6());
		attributes.put("dato7", getDato7());
		attributes.put("dato8", getDato8());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroHijo = (Long)attributes.get("idParametroHijo");

		if (idParametroHijo != null) {
			setIdParametroHijo(idParametroHijo);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Integer orden = (Integer)attributes.get("orden");

		if (orden != null) {
			setOrden(orden);
		}

		String dato1 = (String)attributes.get("dato1");

		if (dato1 != null) {
			setDato1(dato1);
		}

		String dato2 = (String)attributes.get("dato2");

		if (dato2 != null) {
			setDato2(dato2);
		}

		String dato3 = (String)attributes.get("dato3");

		if (dato3 != null) {
			setDato3(dato3);
		}

		String dato4 = (String)attributes.get("dato4");

		if (dato4 != null) {
			setDato4(dato4);
		}

		String dato5 = (String)attributes.get("dato5");

		if (dato5 != null) {
			setDato5(dato5);
		}

		String dato6 = (String)attributes.get("dato6");

		if (dato6 != null) {
			setDato6(dato6);
		}

		String dato7 = (String)attributes.get("dato7");

		if (dato7 != null) {
			setDato7(dato7);
		}

		String dato8 = (String)attributes.get("dato8");

		if (dato8 != null) {
			setDato8(dato8);
		}
	}

	/**
	* Returns the primary key of this parametro hijo.
	*
	* @return the primary key of this parametro hijo
	*/
	@Override
	public long getPrimaryKey() {
		return _parametroHijo.getPrimaryKey();
	}

	/**
	* Sets the primary key of this parametro hijo.
	*
	* @param primaryKey the primary key of this parametro hijo
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_parametroHijo.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id parametro hijo of this parametro hijo.
	*
	* @return the id parametro hijo of this parametro hijo
	*/
	@Override
	public long getIdParametroHijo() {
		return _parametroHijo.getIdParametroHijo();
	}

	/**
	* Sets the id parametro hijo of this parametro hijo.
	*
	* @param idParametroHijo the id parametro hijo of this parametro hijo
	*/
	@Override
	public void setIdParametroHijo(long idParametroHijo) {
		_parametroHijo.setIdParametroHijo(idParametroHijo);
	}

	/**
	* Returns the group ID of this parametro hijo.
	*
	* @return the group ID of this parametro hijo
	*/
	@Override
	public long getGroupId() {
		return _parametroHijo.getGroupId();
	}

	/**
	* Sets the group ID of this parametro hijo.
	*
	* @param groupId the group ID of this parametro hijo
	*/
	@Override
	public void setGroupId(long groupId) {
		_parametroHijo.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this parametro hijo.
	*
	* @return the company ID of this parametro hijo
	*/
	@Override
	public long getCompanyId() {
		return _parametroHijo.getCompanyId();
	}

	/**
	* Sets the company ID of this parametro hijo.
	*
	* @param companyId the company ID of this parametro hijo
	*/
	@Override
	public void setCompanyId(long companyId) {
		_parametroHijo.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this parametro hijo.
	*
	* @return the user ID of this parametro hijo
	*/
	@Override
	public long getUserId() {
		return _parametroHijo.getUserId();
	}

	/**
	* Sets the user ID of this parametro hijo.
	*
	* @param userId the user ID of this parametro hijo
	*/
	@Override
	public void setUserId(long userId) {
		_parametroHijo.setUserId(userId);
	}

	/**
	* Returns the user uuid of this parametro hijo.
	*
	* @return the user uuid of this parametro hijo
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroHijo.getUserUuid();
	}

	/**
	* Sets the user uuid of this parametro hijo.
	*
	* @param userUuid the user uuid of this parametro hijo
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_parametroHijo.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this parametro hijo.
	*
	* @return the user name of this parametro hijo
	*/
	@Override
	public java.lang.String getUserName() {
		return _parametroHijo.getUserName();
	}

	/**
	* Sets the user name of this parametro hijo.
	*
	* @param userName the user name of this parametro hijo
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_parametroHijo.setUserName(userName);
	}

	/**
	* Returns the create date of this parametro hijo.
	*
	* @return the create date of this parametro hijo
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _parametroHijo.getCreateDate();
	}

	/**
	* Sets the create date of this parametro hijo.
	*
	* @param createDate the create date of this parametro hijo
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_parametroHijo.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this parametro hijo.
	*
	* @return the modified date of this parametro hijo
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _parametroHijo.getModifiedDate();
	}

	/**
	* Sets the modified date of this parametro hijo.
	*
	* @param modifiedDate the modified date of this parametro hijo
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_parametroHijo.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the codigo padre of this parametro hijo.
	*
	* @return the codigo padre of this parametro hijo
	*/
	@Override
	public java.lang.String getCodigoPadre() {
		return _parametroHijo.getCodigoPadre();
	}

	/**
	* Sets the codigo padre of this parametro hijo.
	*
	* @param codigoPadre the codigo padre of this parametro hijo
	*/
	@Override
	public void setCodigoPadre(java.lang.String codigoPadre) {
		_parametroHijo.setCodigoPadre(codigoPadre);
	}

	/**
	* Returns the codigo of this parametro hijo.
	*
	* @return the codigo of this parametro hijo
	*/
	@Override
	public java.lang.String getCodigo() {
		return _parametroHijo.getCodigo();
	}

	/**
	* Sets the codigo of this parametro hijo.
	*
	* @param codigo the codigo of this parametro hijo
	*/
	@Override
	public void setCodigo(java.lang.String codigo) {
		_parametroHijo.setCodigo(codigo);
	}

	/**
	* Returns the nombre of this parametro hijo.
	*
	* @return the nombre of this parametro hijo
	*/
	@Override
	public java.lang.String getNombre() {
		return _parametroHijo.getNombre();
	}

	/**
	* Sets the nombre of this parametro hijo.
	*
	* @param nombre the nombre of this parametro hijo
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_parametroHijo.setNombre(nombre);
	}

	/**
	* Returns the descripcion of this parametro hijo.
	*
	* @return the descripcion of this parametro hijo
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _parametroHijo.getDescripcion();
	}

	/**
	* Sets the descripcion of this parametro hijo.
	*
	* @param descripcion the descripcion of this parametro hijo
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_parametroHijo.setDescripcion(descripcion);
	}

	/**
	* Returns the estado of this parametro hijo.
	*
	* @return the estado of this parametro hijo
	*/
	@Override
	public boolean getEstado() {
		return _parametroHijo.getEstado();
	}

	/**
	* Returns <code>true</code> if this parametro hijo is estado.
	*
	* @return <code>true</code> if this parametro hijo is estado; <code>false</code> otherwise
	*/
	@Override
	public boolean isEstado() {
		return _parametroHijo.isEstado();
	}

	/**
	* Sets whether this parametro hijo is estado.
	*
	* @param estado the estado of this parametro hijo
	*/
	@Override
	public void setEstado(boolean estado) {
		_parametroHijo.setEstado(estado);
	}

	/**
	* Returns the orden of this parametro hijo.
	*
	* @return the orden of this parametro hijo
	*/
	@Override
	public int getOrden() {
		return _parametroHijo.getOrden();
	}

	/**
	* Sets the orden of this parametro hijo.
	*
	* @param orden the orden of this parametro hijo
	*/
	@Override
	public void setOrden(int orden) {
		_parametroHijo.setOrden(orden);
	}

	/**
	* Returns the dato1 of this parametro hijo.
	*
	* @return the dato1 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato1() {
		return _parametroHijo.getDato1();
	}

	/**
	* Sets the dato1 of this parametro hijo.
	*
	* @param dato1 the dato1 of this parametro hijo
	*/
	@Override
	public void setDato1(java.lang.String dato1) {
		_parametroHijo.setDato1(dato1);
	}

	/**
	* Returns the dato2 of this parametro hijo.
	*
	* @return the dato2 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato2() {
		return _parametroHijo.getDato2();
	}

	/**
	* Sets the dato2 of this parametro hijo.
	*
	* @param dato2 the dato2 of this parametro hijo
	*/
	@Override
	public void setDato2(java.lang.String dato2) {
		_parametroHijo.setDato2(dato2);
	}

	/**
	* Returns the dato3 of this parametro hijo.
	*
	* @return the dato3 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato3() {
		return _parametroHijo.getDato3();
	}

	/**
	* Sets the dato3 of this parametro hijo.
	*
	* @param dato3 the dato3 of this parametro hijo
	*/
	@Override
	public void setDato3(java.lang.String dato3) {
		_parametroHijo.setDato3(dato3);
	}

	/**
	* Returns the dato4 of this parametro hijo.
	*
	* @return the dato4 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato4() {
		return _parametroHijo.getDato4();
	}

	/**
	* Sets the dato4 of this parametro hijo.
	*
	* @param dato4 the dato4 of this parametro hijo
	*/
	@Override
	public void setDato4(java.lang.String dato4) {
		_parametroHijo.setDato4(dato4);
	}

	/**
	* Returns the dato5 of this parametro hijo.
	*
	* @return the dato5 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato5() {
		return _parametroHijo.getDato5();
	}

	/**
	* Sets the dato5 of this parametro hijo.
	*
	* @param dato5 the dato5 of this parametro hijo
	*/
	@Override
	public void setDato5(java.lang.String dato5) {
		_parametroHijo.setDato5(dato5);
	}

	/**
	* Returns the dato6 of this parametro hijo.
	*
	* @return the dato6 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato6() {
		return _parametroHijo.getDato6();
	}

	/**
	* Sets the dato6 of this parametro hijo.
	*
	* @param dato6 the dato6 of this parametro hijo
	*/
	@Override
	public void setDato6(java.lang.String dato6) {
		_parametroHijo.setDato6(dato6);
	}

	/**
	* Returns the dato7 of this parametro hijo.
	*
	* @return the dato7 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato7() {
		return _parametroHijo.getDato7();
	}

	/**
	* Sets the dato7 of this parametro hijo.
	*
	* @param dato7 the dato7 of this parametro hijo
	*/
	@Override
	public void setDato7(java.lang.String dato7) {
		_parametroHijo.setDato7(dato7);
	}

	/**
	* Returns the dato8 of this parametro hijo.
	*
	* @return the dato8 of this parametro hijo
	*/
	@Override
	public java.lang.String getDato8() {
		return _parametroHijo.getDato8();
	}

	/**
	* Sets the dato8 of this parametro hijo.
	*
	* @param dato8 the dato8 of this parametro hijo
	*/
	@Override
	public void setDato8(java.lang.String dato8) {
		_parametroHijo.setDato8(dato8);
	}

	@Override
	public boolean isNew() {
		return _parametroHijo.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_parametroHijo.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _parametroHijo.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_parametroHijo.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _parametroHijo.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _parametroHijo.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_parametroHijo.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _parametroHijo.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_parametroHijo.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_parametroHijo.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_parametroHijo.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ParametroHijoWrapper((ParametroHijo)_parametroHijo.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.ParametroHijo parametroHijo) {
		return _parametroHijo.compareTo(parametroHijo);
	}

	@Override
	public int hashCode() {
		return _parametroHijo.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.ParametroHijo> toCacheModel() {
		return _parametroHijo.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroHijo toEscapedModel() {
		return new ParametroHijoWrapper(_parametroHijo.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroHijo toUnescapedModel() {
		return new ParametroHijoWrapper(_parametroHijo.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _parametroHijo.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _parametroHijo.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_parametroHijo.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroHijoWrapper)) {
			return false;
		}

		ParametroHijoWrapper parametroHijoWrapper = (ParametroHijoWrapper)obj;

		if (Validator.equals(_parametroHijo, parametroHijoWrapper._parametroHijo)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ParametroHijo getWrappedParametroHijo() {
		return _parametroHijo;
	}

	@Override
	public ParametroHijo getWrappedModel() {
		return _parametroHijo;
	}

	@Override
	public void resetOriginalValues() {
		_parametroHijo.resetOriginalValues();
	}

	private ParametroHijo _parametroHijo;
}