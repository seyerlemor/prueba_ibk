/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link pe.com.ibk.pepper.service.http.ParametroHijoPOServiceSoap}.
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.http.ParametroHijoPOServiceSoap
 * @generated
 */
public class ParametroHijoPOSoap implements Serializable {
	public static ParametroHijoPOSoap toSoapModel(ParametroHijoPO model) {
		ParametroHijoPOSoap soapModel = new ParametroHijoPOSoap();

		soapModel.setIdParametroHijo(model.getIdParametroHijo());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCodigoPadre(model.getCodigoPadre());
		soapModel.setCodigo(model.getCodigo());
		soapModel.setNombre(model.getNombre());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setEstado(model.getEstado());
		soapModel.setOrden(model.getOrden());
		soapModel.setDato1(model.getDato1());
		soapModel.setDato2(model.getDato2());
		soapModel.setDato3(model.getDato3());
		soapModel.setDato4(model.getDato4());
		soapModel.setDato5(model.getDato5());
		soapModel.setDato6(model.getDato6());
		soapModel.setDato7(model.getDato7());
		soapModel.setDato8(model.getDato8());

		return soapModel;
	}

	public static ParametroHijoPOSoap[] toSoapModels(ParametroHijoPO[] models) {
		ParametroHijoPOSoap[] soapModels = new ParametroHijoPOSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ParametroHijoPOSoap[][] toSoapModels(
		ParametroHijoPO[][] models) {
		ParametroHijoPOSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ParametroHijoPOSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ParametroHijoPOSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ParametroHijoPOSoap[] toSoapModels(
		List<ParametroHijoPO> models) {
		List<ParametroHijoPOSoap> soapModels = new ArrayList<ParametroHijoPOSoap>(models.size());

		for (ParametroHijoPO model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ParametroHijoPOSoap[soapModels.size()]);
	}

	public ParametroHijoPOSoap() {
	}

	public long getPrimaryKey() {
		return _idParametroHijo;
	}

	public void setPrimaryKey(long pk) {
		setIdParametroHijo(pk);
	}

	public long getIdParametroHijo() {
		return _idParametroHijo;
	}

	public void setIdParametroHijo(long idParametroHijo) {
		_idParametroHijo = idParametroHijo;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getCodigoPadre() {
		return _codigoPadre;
	}

	public void setCodigoPadre(String codigoPadre) {
		_codigoPadre = codigoPadre;
	}

	public String getCodigo() {
		return _codigo;
	}

	public void setCodigo(String codigo) {
		_codigo = codigo;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public boolean getEstado() {
		return _estado;
	}

	public boolean isEstado() {
		return _estado;
	}

	public void setEstado(boolean estado) {
		_estado = estado;
	}

	public int getOrden() {
		return _orden;
	}

	public void setOrden(int orden) {
		_orden = orden;
	}

	public String getDato1() {
		return _dato1;
	}

	public void setDato1(String dato1) {
		_dato1 = dato1;
	}

	public String getDato2() {
		return _dato2;
	}

	public void setDato2(String dato2) {
		_dato2 = dato2;
	}

	public String getDato3() {
		return _dato3;
	}

	public void setDato3(String dato3) {
		_dato3 = dato3;
	}

	public String getDato4() {
		return _dato4;
	}

	public void setDato4(String dato4) {
		_dato4 = dato4;
	}

	public String getDato5() {
		return _dato5;
	}

	public void setDato5(String dato5) {
		_dato5 = dato5;
	}

	public String getDato6() {
		return _dato6;
	}

	public void setDato6(String dato6) {
		_dato6 = dato6;
	}

	public String getDato7() {
		return _dato7;
	}

	public void setDato7(String dato7) {
		_dato7 = dato7;
	}

	public String getDato8() {
		return _dato8;
	}

	public void setDato8(String dato8) {
		_dato8 = dato8;
	}

	private long _idParametroHijo;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _codigoPadre;
	private String _codigo;
	private String _nombre;
	private String _descripcion;
	private boolean _estado;
	private int _orden;
	private String _dato1;
	private String _dato2;
	private String _dato3;
	private String _dato4;
	private String _dato5;
	private String _dato6;
	private String _dato7;
	private String _dato8;
}