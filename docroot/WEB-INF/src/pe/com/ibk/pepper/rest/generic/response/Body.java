package pe.com.ibk.pepper.rest.generic.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import pe.com.ibk.pepper.rest.calificacion.response.Evaluation;
import pe.com.ibk.pepper.rest.campania.response.ConsultarLeadResponse;
import pe.com.ibk.pepper.rest.equifax.response.ConsultaPreguntasResponse;
import pe.com.ibk.pepper.rest.equifax.response.ValidacionPreguntasResponse;
import pe.com.ibk.pepper.rest.fechahoraentrega.response.ConsultarFechaHoraEntregaResponse;
import pe.com.ibk.pepper.rest.listanegra.response.ValidarListaNegraResponse;
import pe.com.ibk.pepper.rest.potc.response.ConsultarProductosResponse;
import pe.com.ibk.pepper.rest.potc.response.ConsultarProvisionalResponse;

public class Body implements Serializable{

	private static final long serialVersionUID = -6187989385558298871L;
	
	@SerializedName(value="validarListaNegraResponse")
	ValidarListaNegraResponse validarListaNegraResponse;
	
	@SerializedName("consultaPreguntasResponse")
	private ConsultaPreguntasResponse consultaPreguntasResponse;
	
	@SerializedName("validacionPreguntasResponse")
	private ValidacionPreguntasResponse validacionPreguntasResponse;
	
	@SerializedName(value="evaluation")
	private Evaluation evaluation;
	
	@SerializedName("consultarFechaHoraEntregaResponse")
	private ConsultarFechaHoraEntregaResponse consultarFechaHoraEntregaResponse;

	@SerializedName("consultarProvisionalResponse")
	private ConsultarProvisionalResponse consultarProvisionalResponse;
	
	@SerializedName("consultarProductosResponse")
	private ConsultarProductosResponse consultarProductosResponse;
	
	@SerializedName("ConsultarLeadResponse")
	private ConsultarLeadResponse consultarLeadResponse;
	
	public ValidarListaNegraResponse getValidarListaNegraResponse() {
		return validarListaNegraResponse;
	}

	public void setValidarListaNegraResponse(
			ValidarListaNegraResponse validarListaNegraResponse) {
		this.validarListaNegraResponse = validarListaNegraResponse;
	}

	public ConsultaPreguntasResponse getConsultaPreguntasResponse() {
		return consultaPreguntasResponse;
	}

	public void setConsultaPreguntasResponse(
			ConsultaPreguntasResponse consultaPreguntasResponse) {
		this.consultaPreguntasResponse = consultaPreguntasResponse;
	}

	public ValidacionPreguntasResponse getValidacionPreguntasResponse() {
		return validacionPreguntasResponse;
	}

	public void setValidacionPreguntasResponse(
			ValidacionPreguntasResponse validacionPreguntasResponse) {
		this.validacionPreguntasResponse = validacionPreguntasResponse;
	}

	public Evaluation getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}

	public ConsultarFechaHoraEntregaResponse getConsultarFechaHoraEntregaResponse() {
		return consultarFechaHoraEntregaResponse;
	}

	public void setConsultarFechaHoraEntregaResponse(
			ConsultarFechaHoraEntregaResponse consultarFechaHoraEntregaResponse) {
		this.consultarFechaHoraEntregaResponse = consultarFechaHoraEntregaResponse;
	}

	public ConsultarProvisionalResponse getConsultarProvisionalResponse() {
		return consultarProvisionalResponse;
	}

	public void setConsultarProvisionalResponse(
			ConsultarProvisionalResponse consultarProvisionalResponse) {
		this.consultarProvisionalResponse = consultarProvisionalResponse;
	}

	public ConsultarProductosResponse getConsultarProductosResponse() {
		return consultarProductosResponse;
	}

	public void setConsultarProductosResponse(
			ConsultarProductosResponse consultarProductosResponse) {
		this.consultarProductosResponse = consultarProductosResponse;
	}

	public ConsultarLeadResponse getConsultarLeadResponse() {
		return consultarLeadResponse;
	}

	public void setConsultarLeadResponse(ConsultarLeadResponse consultarLeadResponse) {
		this.consultarLeadResponse = consultarLeadResponse;
	}

}
