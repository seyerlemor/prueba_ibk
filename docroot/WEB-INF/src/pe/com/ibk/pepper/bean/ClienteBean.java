package pe.com.ibk.pepper.bean;

import java.io.Serializable;
import java.util.Date;

import pe.com.ibk.pepper.rest.obtenerinformacion.response.Direcciones;

public class ClienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idCliente;
	private String tipoDocumento;
	private String dniNumber;
	private long idExpediente;
	private String nombreParaMostrar;
	private String firstname;
	private String middlename;
	private String nombres;
	private String lastname;
	private String motherslastname;
	private String gender;
	private String emailAddress;
	private String confirmEmailAddress;
	private String cellPhoneNumber;
	private String direccion;
	private String civilStatus;
	private String ruc;
	private String nivelEducacion;
	private String ocupacion;
	private String cargoActual;
	private String actividadNegocio;
	private String porcentajeParticipacion;
	private String ingresoMensualFijo;
	private String ingresoMensualVariable;
	private String ventaMensual;
	private String birthday;
	private String day;
	private String month;
	private String year;
	private Date fechaRegistro;
	private String cellPhoneOperator;
	private String celular;
	private String terminos;
	private String terminosCondiciones;
	private String companyName;
    private Direcciones direcciones;
	private String flagCliente;
	private String codigoUnico;
	private String flagPEP;
	private String addressDepartamento;
	private String addressProvincia;
	private String addressDistrito;
	private String addressType;
	private String descripcionTipoVia;
	private String addressName;
	private String addressNro;
	private String addressMz;
	private String addressLt;
	private String addressInt;
	private String departamentoLaboral;
	private String provinciaLaboral;
	private String distritoLaboral;
	private String fechaIngresoEmpresa;
	private String personaExpuestaPublica;
	private String tipoDocumentoConyuge;
	private String documentoConyuge;
	private String primerNombreConyuge;
	private String segundoNombreConyuge;
	private String apellidoPaternoConyuge;
	private String apellidoMaternoConyuge;
	private String fechaNacimientoConyuge;
	private String sexoConyuge;
	private String flagDireccion;
	//Nuevo ObtenerInformacion
	private String descripOperador;
	private String nroOperacion;
	private String terminosAyuda;
	private String ip;
	private String employmentStatus;
	private String emailAddressOtp;
	private String cellPhoneNumberOtp;
	
	public ClienteBean() {
		super();
		direcciones = new Direcciones();
	}
	
	public String getTerminosAyuda() {
		return terminosAyuda;
	}
	public void setTerminosAyuda(String terminosAyuda) {
		this.terminosAyuda = terminosAyuda;
	}
	public String getDescripOperador() {
		return descripOperador;
	}
	public void setDescripOperador(String descripOperador) {
		this.descripOperador = descripOperador;
	}
	
	public long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getDniNumber() {
		return dniNumber;
	}
	public void setDniNumber(String dniNumber) {
		this.dniNumber = dniNumber;
	}
	public long getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(long idExpediente) {
		this.idExpediente = idExpediente;
	}
	public String getNombreParaMostrar() {
		return nombreParaMostrar;
	}
	public void setNombreParaMostrar(String nombreParaMostrar) {
		this.nombreParaMostrar = nombreParaMostrar;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMotherslastname() {
		return motherslastname;
	}
	public void setMotherslastname(String motherslastname) {
		this.motherslastname = motherslastname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getConfirmEmailAddress() {
		return confirmEmailAddress;
	}
	public void setConfirmEmailAddress(String confirmEmailAddress) {
		this.confirmEmailAddress = confirmEmailAddress;
	}
	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}
	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCivilStatus() {
		return civilStatus;
	}
	public void setCivilStatus(String civilStatus) {
		this.civilStatus = civilStatus;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getNivelEducacion() {
		return nivelEducacion;
	}
	public void setNivelEducacion(String nivelEducacion) {
		this.nivelEducacion = nivelEducacion;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public String getCargoActual() {
		return cargoActual;
	}
	public void setCargoActual(String cargoActual) {
		this.cargoActual = cargoActual;
	}
	public String getActividadNegocio() {
		return actividadNegocio;
	}
	public void setActividadNegocio(String actividadNegocio) {
		this.actividadNegocio = actividadNegocio;
	}
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public String getIngresoMensualFijo() {
		return ingresoMensualFijo;
	}
	public void setIngresoMensualFijo(String ingresoMensualFijo) {
		this.ingresoMensualFijo = ingresoMensualFijo;
	}
	public String getIngresoMensualVariable() {
		return ingresoMensualVariable;
	}
	public void setIngresoMensualVariable(String ingresoMensualVariable) {
		this.ingresoMensualVariable = ingresoMensualVariable;
	}
	public String getVentaMensual() {
		return ventaMensual;
	}
	public void setVentaMensual(String ventaMensual) {
		this.ventaMensual = ventaMensual;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getCellPhoneOperator() {
		return cellPhoneOperator;
	}
	public void setCellPhoneOperator(String cellPhoneOperator) {
		this.cellPhoneOperator = cellPhoneOperator;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getTerminos() {
		return terminos;
	}
	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}
	public String getTerminosCondiciones() {
		return terminosCondiciones;
	}
	public void setTerminosCondiciones(String terminosCondiciones) {
		this.terminosCondiciones = terminosCondiciones;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFlagCliente() {
		return flagCliente;
	}
	public void setFlagCliente(String flagCliente) {
		this.flagCliente = flagCliente;
	}
	public String getCodigoUnico() {
		return codigoUnico;
	}
	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}
	public String getFlagPEP() {
		return flagPEP;
	}
	public void setFlagPEP(String flagPEP) {
		this.flagPEP = flagPEP;
	}
	public String getAddressDepartamento() {
		return addressDepartamento;
	}
	public void setAddressDepartamento(String addressDepartamento) {
		this.addressDepartamento = addressDepartamento;
	}
	public String getAddressProvincia() {
		return addressProvincia;
	}
	public void setAddressProvincia(String addressProvincia) {
		this.addressProvincia = addressProvincia;
	}
	public String getAddressDistrito() {
		return addressDistrito;
	}
	public void setAddressDistrito(String addressDistrito) {
		this.addressDistrito = addressDistrito;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getDescripcionTipoVia() {
		return descripcionTipoVia;
	}
	public void setDescripcionTipoVia(String descripcionTipoVia) {
		this.descripcionTipoVia = descripcionTipoVia;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getAddressNro() {
		return addressNro;
	}
	public void setAddressNro(String addressNro) {
		this.addressNro = addressNro;
	}
	public String getAddressMz() {
		return addressMz;
	}
	public void setAddressMz(String addressMz) {
		this.addressMz = addressMz;
	}
	public String getAddressLt() {
		return addressLt;
	}
	public void setAddressLt(String addressLt) {
		this.addressLt = addressLt;
	}
	public String getAddressInt() {
		return addressInt;
	}
	public void setAddressInt(String addressInt) {
		this.addressInt = addressInt;
	}
	public String getDepartamentoLaboral() {
		return departamentoLaboral;
	}
	public void setDepartamentoLaboral(String departamentoLaboral) {
		this.departamentoLaboral = departamentoLaboral;
	}
	public String getProvinciaLaboral() {
		return provinciaLaboral;
	}
	public void setProvinciaLaboral(String provinciaLaboral) {
		this.provinciaLaboral = provinciaLaboral;
	}
	public String getDistritoLaboral() {
		return distritoLaboral;
	}
	public void setDistritoLaboral(String distritoLaboral) {
		this.distritoLaboral = distritoLaboral;
	}
	public String getFechaIngresoEmpresa() {
		return fechaIngresoEmpresa;
	}
	public void setFechaIngresoEmpresa(String fechaIngresoEmpresa) {
		this.fechaIngresoEmpresa = fechaIngresoEmpresa;
	}
	public String getPersonaExpuestaPublica() {
		return personaExpuestaPublica;
	}
	public void setPersonaExpuestaPublica(String personaExpuestaPublica) {
		this.personaExpuestaPublica = personaExpuestaPublica;
	}
	public String getTipoDocumentoConyuge() {
		return tipoDocumentoConyuge;
	}
	public void setTipoDocumentoConyuge(String tipoDocumentoConyuge) {
		this.tipoDocumentoConyuge = tipoDocumentoConyuge;
	}
	public String getDocumentoConyuge() {
		return documentoConyuge;
	}
	public void setDocumentoConyuge(String documentoConyuge) {
		this.documentoConyuge = documentoConyuge;
	}
	public String getPrimerNombreConyuge() {
		return primerNombreConyuge;
	}
	public void setPrimerNombreConyuge(String primerNombreConyuge) {
		this.primerNombreConyuge = primerNombreConyuge;
	}
	public String getSegundoNombreConyuge() {
		return segundoNombreConyuge;
	}
	public void setSegundoNombreConyuge(String segundoNombreConyuge) {
		this.segundoNombreConyuge = segundoNombreConyuge;
	}
	public String getApellidoPaternoConyuge() {
		return apellidoPaternoConyuge;
	}
	public void setApellidoPaternoConyuge(String apellidoPaternoConyuge) {
		this.apellidoPaternoConyuge = apellidoPaternoConyuge;
	}
	public String getApellidoMaternoConyuge() {
		return apellidoMaternoConyuge;
	}
	public void setApellidoMaternoConyuge(String apellidoMaternoConyuge) {
		this.apellidoMaternoConyuge = apellidoMaternoConyuge;
	}
	public String getFechaNacimientoConyuge() {
		return fechaNacimientoConyuge;
	}
	public void setFechaNacimientoConyuge(String fechaNacimientoConyuge) {
		this.fechaNacimientoConyuge = fechaNacimientoConyuge;
	}
	public String getSexoConyuge() {
		return sexoConyuge;
	}
	public void setSexoConyuge(String sexoConyuge) {
		this.sexoConyuge = sexoConyuge;
	}
	public String getFlagDireccion() {
		return flagDireccion;
	}
	public void setFlagDireccion(String flagDireccion) {
		this.flagDireccion = flagDireccion;
	}
	public String getNroOperacion() {
		return nroOperacion;
	}
	public void setNroOperacion(String nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public Direcciones getDirecciones() {
		return direcciones;
	}
	public void setDirecciones(Direcciones direcciones) {
		this.direcciones = direcciones;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public String getEmailAddressOtp() {
		return emailAddressOtp;
	}
	public void setEmailAddressOtp(String emailAddressOtp) {
		this.emailAddressOtp = emailAddressOtp;
	}
	public String getCellPhoneNumberOtp() {
		return cellPhoneNumberOtp;
	}
	public void setCellPhoneNumberOtp(String cellPhoneNumberOtp) {
		this.cellPhoneNumberOtp = cellPhoneNumberOtp;
	}
	@Override
	public String toString() {
		return "ClienteBean [idCliente=" + idCliente + ", tipoDocumento="
				+ tipoDocumento + ", dniNumber=" + dniNumber
				+ ", idExpediente=" + idExpediente + ", nombreParaMostrar="
				+ nombreParaMostrar + ", firstname=" + firstname
				+ ", middlename=" + middlename + ", nombres=" + nombres
				+ ", lastname=" + lastname + ", motherslastname="
				+ motherslastname + ", gender=" + gender + ", emailAddress="
				+ emailAddress + ", confirmEmailAddress=" + confirmEmailAddress
				+ ", cellPhoneNumber=" + cellPhoneNumber + ", direccion="
				+ direccion + ", civilStatus=" + civilStatus + ", ruc=" + ruc
				+ ", nivelEducacion=" + nivelEducacion + ", ocupacion="
				+ ocupacion + ", cargoActual=" + cargoActual
				+ ", actividadNegocio=" + actividadNegocio
				+ ", porcentajeParticipacion=" + porcentajeParticipacion
				+ ", ingresoMensualFijo=" + ingresoMensualFijo
				+ ", ingresoMensualVariable=" + ingresoMensualVariable
				+ ", ventaMensual=" + ventaMensual + ", birthday=" + birthday
				+ ", day=" + day + ", month=" + month + ", year=" + year
				+ ", fechaRegistro=" + fechaRegistro + ", cellPhoneOperator="
				+ cellPhoneOperator + ", celular=" + celular + ", terminos="
				+ terminos + ", terminosCondiciones=" + terminosCondiciones
				+ ", companyName=" + companyName + ", direcciones="
				+ direcciones + ", flagCliente=" + flagCliente
				+ ", codigoUnico=" + codigoUnico + ", flagPEP=" + flagPEP
				+ ", addressDepartamento=" + addressDepartamento
				+ ", addressProvincia=" + addressProvincia
				+ ", addressDistrito=" + addressDistrito + ", addressType="
				+ addressType + ", descripcionTipoVia=" + descripcionTipoVia
				+ ", addressName=" + addressName + ", addressNro=" + addressNro
				+ ", addressMz=" + addressMz + ", addressLt=" + addressLt
				+ ", addressInt=" + addressInt + ", departamentoLaboral="
				+ departamentoLaboral + ", provinciaLaboral="
				+ provinciaLaboral + ", distritoLaboral=" + distritoLaboral
				+ ", fechaIngresoEmpresa=" + fechaIngresoEmpresa
				+ ", personaExpuestaPublica=" + personaExpuestaPublica
				+ ", tipoDocumentoConyuge=" + tipoDocumentoConyuge
				+ ", documentoConyuge=" + documentoConyuge
				+ ", primerNombreConyuge=" + primerNombreConyuge
				+ ", segundoNombreConyuge=" + segundoNombreConyuge
				+ ", apellidoPaternoConyuge=" + apellidoPaternoConyuge
				+ ", apellidoMaternoConyuge=" + apellidoMaternoConyuge
				+ ", fechaNacimientoConyuge=" + fechaNacimientoConyuge
				+ ", sexoConyuge=" + sexoConyuge + ", flagDireccion="
				+ flagDireccion + ", descripOperador=" + descripOperador
				+ ", nroOperacion=" + nroOperacion + ", terminosAyuda="
				+ terminosAyuda + ", ip=" + ip + ", employmentStatus="
				+ employmentStatus + ", emailAddressOtp=" + emailAddressOtp
				+ ", cellPhoneNumberOtp=" + cellPhoneNumberOtp + "]";
	}
	
}