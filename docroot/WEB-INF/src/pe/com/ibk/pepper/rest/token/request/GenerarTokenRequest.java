
package pe.com.ibk.pepper.rest.token.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import pe.com.ibk.pepper.rest.generic.request.HttpHeadersV2;

public class GenerarTokenRequest {

	private HttpHeadersV2 httpHeader;
    private String usuarioId;
    private String id;
    private String email;
    private String phoneNumber;
    private String phoneOperator;
    private String sendMethod;
    private String channel;
    private String messageId;
    private List<Parameter> parameters;

	@JsonCreator
	public GenerarTokenRequest(HttpHeadersV2 httpHeader, String usuarioId, String id, String email,
			String phoneNumber, String phoneOperator, String sendMethod,
			String channel, String messageId, List<Parameter> parameters) {
		super();
        this.httpHeader = httpHeader;
		this.usuarioId = usuarioId;
		this.id = id;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.phoneOperator = phoneOperator;
		this.sendMethod = sendMethod;
		this.channel = channel;
		this.messageId = messageId;
		this.parameters = parameters;
	}
	
	@JsonProperty("HttpHeader")
    public HttpHeadersV2 getHttpHeader() {
        return httpHeader;
    }

    public void setHttpHeader(HttpHeadersV2 httpHeader) {
        this.httpHeader = httpHeader;
    }

	@JsonProperty("usuario_id")
	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonProperty("phone_operator")
	public String getPhoneOperator() {
		return phoneOperator;
	}

	public void setPhoneOperator(String phoneOperator) {
		this.phoneOperator = phoneOperator;
	}

	@JsonProperty("send_method")
	public String getSendMethod() {
		return sendMethod;
	}

	public void setSendMethod(String sendMethod) {
		this.sendMethod = sendMethod;
	}

	@JsonProperty("channel")
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	@JsonProperty("message_id")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@JsonProperty("parameters")
	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}
	
}
