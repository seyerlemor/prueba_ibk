package pe.com.ibk.pepper.rest.listanegra.request;

public class Phone {
	private String type;
	private String areaCode;
	private String phone;
	
	public Phone() {
		super();
	}
	
	public Phone(String type, String areaCode, String phone) {
		super();
		this.type = type;
		this.areaCode = areaCode;
		this.phone = phone;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
