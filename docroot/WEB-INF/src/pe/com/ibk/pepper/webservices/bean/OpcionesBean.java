package pe.com.ibk.pepper.webservices.bean;

import java.math.BigInteger;

public class OpcionesBean {

	private BigInteger numeroOpcion;
	private String descripcionOpcion;
	
	public BigInteger getNumeroOpcion() {
		return numeroOpcion;
	}
	public void setNumeroOpcion(BigInteger numeroOpcion) {
		this.numeroOpcion = numeroOpcion;
	}
	public String getDescripcionOpcion() {
		return descripcionOpcion;
	}
	public void setDescripcionOpcion(String descripcionOpcion) {
		this.descripcionOpcion = descripcionOpcion;
	}
	
	
}
