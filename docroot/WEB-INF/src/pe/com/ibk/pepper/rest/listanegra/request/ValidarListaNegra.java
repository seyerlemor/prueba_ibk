
package pe.com.ibk.pepper.rest.listanegra.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValidarListaNegra {

    private Documento documento;
    private Telefono telefono;

    @JsonCreator
    public ValidarListaNegra(Documento documento, Telefono telefono) {
        super();
        this.documento = documento;
        this.telefono = telefono;
    }

    @JsonProperty("documento")
    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @JsonProperty("telefono")
    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("documento", documento).append("telefono", telefono).toString();
    }

}
