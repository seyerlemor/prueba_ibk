
package pe.com.ibk.pepper.rest.campania.response;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Quota implements Serializable
{

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("amount")
    @Expose
    private String amount;
    private final static long serialVersionUID = 7034743871286723422L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Quota() {
    }

    /**
     * 
     * @param number
     * @param amount
     */
    public Quota(String number, String amount) {
        super();
        this.number = number;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Quota withNumber(String number) {
        this.number = number;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Quota withAmount(String amount) {
        this.amount = amount;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("number", number).append("amount", amount).toString();
    }

}
