/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.Producto;

/**
 * The persistence interface for the producto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ProductoPersistenceImpl
 * @see ProductoUtil
 * @generated
 */
public interface ProductoPersistence extends BasePersistence<Producto> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProductoUtil} to access the producto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the producto where idProducto = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	*
	* @param idProducto the id producto
	* @return the matching producto
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto findByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException;

	/**
	* Returns the producto where idProducto = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idProducto the id producto
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto fetchByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the producto where idProducto = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idProducto the id producto
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto fetchByPC_ID(long idProducto,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the producto where idProducto = &#63; from the database.
	*
	* @param idProducto the id producto
	* @return the producto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto removeByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException;

	/**
	* Returns the number of productos where idProducto = &#63;.
	*
	* @param idProducto the id producto
	* @return the number of matching productos
	* @throws SystemException if a system exception occurred
	*/
	public int countByPC_ID(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the producto where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	*
	* @param idExpediente the id expediente
	* @return the matching producto
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto findByPC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException;

	/**
	* Returns the producto where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idExpediente the id expediente
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto fetchByPC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the producto where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idExpediente the id expediente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching producto, or <code>null</code> if a matching producto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto fetchByPC_E(long idExpediente,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the producto where idExpediente = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @return the producto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto removeByPC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException;

	/**
	* Returns the number of productos where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the number of matching productos
	* @throws SystemException if a system exception occurred
	*/
	public int countByPC_E(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the producto in the entity cache if it is enabled.
	*
	* @param producto the producto
	*/
	public void cacheResult(pe.com.ibk.pepper.model.Producto producto);

	/**
	* Caches the productos in the entity cache if it is enabled.
	*
	* @param productos the productos
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Producto> productos);

	/**
	* Creates a new producto with the primary key. Does not add the producto to the database.
	*
	* @param idProducto the primary key for the new producto
	* @return the new producto
	*/
	public pe.com.ibk.pepper.model.Producto create(long idProducto);

	/**
	* Removes the producto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProducto the primary key of the producto
	* @return the producto that was removed
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto remove(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException;

	public pe.com.ibk.pepper.model.Producto updateImpl(
		pe.com.ibk.pepper.model.Producto producto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the producto with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	*
	* @param idProducto the primary key of the producto
	* @return the producto
	* @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto findByPrimaryKey(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchProductoException;

	/**
	* Returns the producto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idProducto the primary key of the producto
	* @return the producto, or <code>null</code> if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Producto fetchByPrimaryKey(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the productos.
	*
	* @return the productos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Producto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the productos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of productos
	* @param end the upper bound of the range of productos (not inclusive)
	* @return the range of productos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Producto> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the productos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of productos
	* @param end the upper bound of the range of productos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of productos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Producto> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the productos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of productos.
	*
	* @return the number of productos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}