
package pe.com.ibk.pepper.rest.campania.response;



public class Detalle {

    private String flujoIngreso;
    private String idMoneda;
    private String marcaTarjeta;
    private String monto;
    private String tasa2;
    private String tipoCliente;
    private String tipoTarjeta;
    
	public String getMarcaTarjeta() {
		return marcaTarjeta;
	}
	public void setMarcaTarjeta(String marcaTarjeta) {
		this.marcaTarjeta = marcaTarjeta;
	}
	public String getFlujoIngreso() {
		return flujoIngreso;
	}
	public void setFlujoIngreso(String flujoIngreso) {
		this.flujoIngreso = flujoIngreso;
	}
	public String getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getTasa2() {
		return tasa2;
	}
	public void setTasa2(String tasa2) {
		this.tasa2 = tasa2;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
    
}
