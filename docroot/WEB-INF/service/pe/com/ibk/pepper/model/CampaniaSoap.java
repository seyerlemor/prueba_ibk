/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class CampaniaSoap implements Serializable {
	public static CampaniaSoap toSoapModel(Campania model) {
		CampaniaSoap soapModel = new CampaniaSoap();

		soapModel.setIdCampania(model.getIdCampania());
		soapModel.setIdExpediente(model.getIdExpediente());
		soapModel.setCodigoCampania(model.getCodigoCampania());
		soapModel.setNombreCampania(model.getNombreCampania());
		soapModel.setCodigoOferta(model.getCodigoOferta());
		soapModel.setNombreOferta(model.getNombreOferta());
		soapModel.setCodigoTratamiento(model.getCodigoTratamiento());
		soapModel.setNombreTratamiento(model.getNombreTratamiento());
		soapModel.setCodigoUnico(model.getCodigoUnico());
		soapModel.setCodigoProducto(model.getCodigoProducto());
		soapModel.setCore(model.getCore());
		soapModel.setNombreProducto(model.getNombreProducto());
		soapModel.setTipoCampania(model.getTipoCampania());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static CampaniaSoap[] toSoapModels(Campania[] models) {
		CampaniaSoap[] soapModels = new CampaniaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CampaniaSoap[][] toSoapModels(Campania[][] models) {
		CampaniaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CampaniaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CampaniaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CampaniaSoap[] toSoapModels(List<Campania> models) {
		List<CampaniaSoap> soapModels = new ArrayList<CampaniaSoap>(models.size());

		for (Campania model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CampaniaSoap[soapModels.size()]);
	}

	public CampaniaSoap() {
	}

	public long getPrimaryKey() {
		return _idCampania;
	}

	public void setPrimaryKey(long pk) {
		setIdCampania(pk);
	}

	public long getIdCampania() {
		return _idCampania;
	}

	public void setIdCampania(long idCampania) {
		_idCampania = idCampania;
	}

	public long getIdExpediente() {
		return _idExpediente;
	}

	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;
	}

	public String getCodigoCampania() {
		return _codigoCampania;
	}

	public void setCodigoCampania(String codigoCampania) {
		_codigoCampania = codigoCampania;
	}

	public String getNombreCampania() {
		return _nombreCampania;
	}

	public void setNombreCampania(String nombreCampania) {
		_nombreCampania = nombreCampania;
	}

	public String getCodigoOferta() {
		return _codigoOferta;
	}

	public void setCodigoOferta(String codigoOferta) {
		_codigoOferta = codigoOferta;
	}

	public String getNombreOferta() {
		return _nombreOferta;
	}

	public void setNombreOferta(String nombreOferta) {
		_nombreOferta = nombreOferta;
	}

	public String getCodigoTratamiento() {
		return _codigoTratamiento;
	}

	public void setCodigoTratamiento(String codigoTratamiento) {
		_codigoTratamiento = codigoTratamiento;
	}

	public String getNombreTratamiento() {
		return _nombreTratamiento;
	}

	public void setNombreTratamiento(String nombreTratamiento) {
		_nombreTratamiento = nombreTratamiento;
	}

	public String getCodigoUnico() {
		return _codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {
		_codigoUnico = codigoUnico;
	}

	public String getCodigoProducto() {
		return _codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		_codigoProducto = codigoProducto;
	}

	public String getCore() {
		return _core;
	}

	public void setCore(String core) {
		_core = core;
	}

	public String getNombreProducto() {
		return _nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		_nombreProducto = nombreProducto;
	}

	public String getTipoCampania() {
		return _tipoCampania;
	}

	public void setTipoCampania(String tipoCampania) {
		_tipoCampania = tipoCampania;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private long _idCampania;
	private long _idExpediente;
	private String _codigoCampania;
	private String _nombreCampania;
	private String _codigoOferta;
	private String _nombreOferta;
	private String _codigoTratamiento;
	private String _nombreTratamiento;
	private String _codigoUnico;
	private String _codigoProducto;
	private String _core;
	private String _nombreProducto;
	private String _tipoCampania;
	private Date _fechaRegistro;
}