package pe.com.ibk.pepper.rest.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import pe.com.ibk.pepper.bean.RegistroAuditoriaBean;
import pe.com.ibk.pepper.bean.ServiceBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.gestion.GestorServicioRest;
import pe.com.ibk.pepper.rest.config.InsecureHostnameVerifier;
import pe.com.ibk.pepper.rest.config.NullX509TrustManager;
import pe.com.ibk.pepper.util.Constantes;
import pe.com.ibk.pepper.util.ServicioUtil;
import pe.com.ibk.pepper.util.Utils;

public class RestServiceUtil {
	private static final Log _log = LogFactoryUtil.getLog(RestServiceUtil.class);
	private static final GestorServicioRest gestorService = new GestorServicioRest();
	
	private RestServiceUtil(){}
	
	public static String getSecurityRestClient(ServiceBean serviceBean) throws WebServicesException {
		  String responseString = StringPool.BLANK;
		  try{
			  Client client = ClientBuilder.newBuilder().sslContext(getTrustAllSSLContextCertified()).hostnameVerifier(new InsecureHostnameVerifier()).build();
			  if(serviceBean.getFlagAutenticacion()){
				  HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(Utils.REST_USUARIO, Utils.REST_PASSWORD);
				  client.register(feature);
			  }
			  Builder buildClient = ServicioUtil.obtenerDatosParametroServicio(client, serviceBean);
			  buildClient = ServicioUtil.obtenerDatosParametroCabecera(buildClient, serviceBean.getRequestString());
			  ServiceBean service = ServicioUtil.removerNodo(serviceBean);
			  gestorService.registroAuditoria(new RegistroAuditoriaBean(service.getMessageId(), Constantes.SERVICEID_LOG, service.getEndPoint().concat(service.getPath()), Constantes.TRAMA_TIPO_REQUEST, service.getRequestString()));
			  Response response = ServicioUtil.invocarServicio(buildClient, service);
			  responseString = Validator.isNotNull(response)?response.readEntity(String.class): StringPool.BLANK;
			  gestorService.registroAuditoria(new RegistroAuditoriaBean(serviceBean.getMessageId(), Constantes.SERVICEID_LOG, serviceBean.getEndPoint().concat(serviceBean.getPath()), Constantes.TRAMA_TIPO_RESPONSE, responseString));
			  if(!StringPool.BLANK.equals(responseString) && Constantes.RESULT_STATUS!=response.getStatus())
					throw new WebServicesException(serviceBean.getNombreWs(), serviceBean.getHtmlError(), serviceBean.getNombreWs(), Utils.getHeaderError(serviceBean.getRequestString(), responseString, serviceBean.getNombreWs()));
		  }catch (Exception e) {
			  if(serviceBean.getEsRestrictivo()){
				  _log.error(e);
				  throw new WebServicesException(serviceBean.getNombreWs(), serviceBean.getHtmlError(), e);
			  }
		  }
		 return responseString;
	}
	
	public static SSLContext getTrustAllSSLContextCertified() {
		SSLContext context = null;
		try {
			context = SSLContext.getInstance(Constantes.TLS);
			final TrustManager[] trustManagerArray = { new NullX509TrustManager() };
			context.init(null, trustManagerArray, null);
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			_log.error(e);
		}
		return context;
	}
  
	public static SSLContext getSSLContextCertified() {
		SslConfigurator sslConfig = SslConfigurator.newInstance().securityProtocol(Constantes.SSL);
	return sslConfig.createSSLContext();
  }
}
