package pe.com.ibk.pepper.gestion;

import org.springframework.stereotype.Service;

import pe.com.ibk.pepper.bean.RegistroAuditoriaBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.rest.registroauditoria.request.RegistroAuditoriaRequest;
import pe.com.ibk.pepper.rest.util.RestRegistroAuditoriaServiceUtil;
import pe.com.ibk.pepper.util.Constantes;

@Service
public class GestorServicioRest {

		public void registroAuditoria(RegistroAuditoriaBean registroAuditoriaBean) throws WebServicesException {	
			RegistroAuditoriaRequest registroAuditoriaRequest = new RegistroAuditoriaRequest(registroAuditoriaBean.getMessageId(), Constantes.SERVICEID_LOG
					, registroAuditoriaBean.getDestinationId(), registroAuditoriaBean.getMessageType(), registroAuditoriaBean.getMessage());		
			RestRegistroAuditoriaServiceUtil.registroAudtoriaLog(registroAuditoriaRequest);		
		}
	}
