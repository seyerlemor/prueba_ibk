package pe.com.ibk.pepper.controller;

import com.google.gson.Gson;
import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortlet;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import pe.com.ibk.pepper.bean.ClienteBean;
import pe.com.ibk.pepper.bean.ComercioBean;
import pe.com.ibk.pepper.bean.ExpedienteBean;
import pe.com.ibk.pepper.bean.FHEntregaBean;
import pe.com.ibk.pepper.bean.JsonBean;
import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.bean.UsuarioSessionBean;
import pe.com.ibk.pepper.exceptions.SessionException;
import pe.com.ibk.pepper.exceptions.ValidationException;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.model.Ubigeo;
import pe.com.ibk.pepper.model.impl.ParametroHijoPOImpl;
import pe.com.ibk.pepper.rest.config.ConfiguracionRestUtil;
import pe.com.ibk.pepper.rest.service.GestorRegistroService;
import pe.com.ibk.pepper.rest.service.GestorServiciosRestService;
import pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil;
import pe.com.ibk.pepper.service.UbigeoLocalServiceUtil;
import pe.com.ibk.pepper.util.Constantes;
import pe.com.ibk.pepper.util.DatesUtil;
import pe.com.ibk.pepper.util.GsonUtil;
import pe.com.ibk.pepper.util.MsgError;
import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;
import pe.com.ibk.pepper.util.RequestParam;
import pe.com.ibk.pepper.util.ResponseMessage;
import pe.com.ibk.pepper.util.SessionKeys;
import pe.com.ibk.pepper.util.StringHalconUtil;
import pe.com.ibk.pepper.util.Utils;

public class SolicitudController extends LiferayPortlet {

	@Autowired
	private GestorServiciosRestService gestorService;
	
	@Autowired
	private GestorRegistroService gestorRegistroService;
	
	private static final Log _log = LogFactoryUtil.getLog(SolicitudController.class);
	
	@RequestMapping
	public String defaultView(PortletRequest request) {
		return validateUrl(request);
	}

	@SuppressWarnings("unchecked")
	public List<JsonBean> setChildByFatherParam(String childName){
		List<JsonBean> lstResult = new ArrayList<>();
		try{
			List<ParametroHijoPO> list = (List<ParametroHijoPO>) MultiVMPoolUtil.getCache(childName.concat(RequestParam.R_CACHE)).get(childName);
			if(Validator.isNull(list)  || !(list instanceof ParametroHijoPOImpl)){
				list = ParametroHijoPOLocalServiceUtil.getParametroHijosByCodigoPadre(childName, QueryUtil.ALL_POS , QueryUtil.ALL_POS);
				MultiVMPoolUtil.getCache(childName.concat(RequestParam.R_CACHE)).put(childName, (Serializable)list);
			}
			for (ParametroHijoPO child : list) 
				lstResult.add(new JsonBean(child.getCodigo(), StringHalconUtil.toCapitalize(child.getNombre())));
		} catch (SystemException e) { 
			_log.error(childName.concat(StringPool.COMMA_AND_SPACE), e);
		}
		return lstResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<JsonBean> setDepartamentos(){	
		List<JsonBean> lstResult = new ArrayList<>();
		try {
			List<Ubigeo> listDepts;
			/**/
			listDepts = (List<Ubigeo>) MultiVMPoolUtil.getCache(RequestParam.LIST_DEPT_CACHE).get(RequestParam.LIST_DEPT);
			if(Validator.isNull(listDepts)  || !(listDepts instanceof ParametroHijoPOImpl)){
				listDepts = UbigeoLocalServiceUtil.getUbigeoByCodProvincia(Constantes.DEFAULT_COD_UBIGEO);	
				MultiVMPoolUtil.getCache(RequestParam.LIST_DEPT_CACHE).put(RequestParam.LIST_DEPT, (Serializable)listDepts);
			}
			/**/
			
			for (Ubigeo ubigeo : listDepts) {
				lstResult.add(new JsonBean(ubigeo.getCodDepartamento(), StringHalconUtil.toCapitalize(ubigeo.getNombre())));
			}
		} catch (SystemException e) { 
			_log.error(MsgError.ERROR_GET_DEPARTAMENTO+e.toString(), e);
		}
		return lstResult;
	}
	
	public void addSessionUser(SolicitudBean pasoBeanSession, PortletRequest request){	
		Gson gson = new Gson();
		String request123 = gson.toJson(pasoBeanSession);
		String requestString = ConfiguracionRestUtil.limpiarJsonRequest(request123);
		SessionKeys.addSessionPortal(request, SessionKeys.PASO_BEAN, requestString);
	}
	
	public void addSessionCommerce(ComercioBean comercioBean, PortletRequest request){	
		Gson gson = new Gson();
		String requestComercio = gson.toJson(comercioBean);
		String requestString = ConfiguracionRestUtil.limpiarJsonRequest(requestComercio);
		SessionKeys.addSessionPortal(request, SessionKeys.COMERCIO_BEAN, requestString);
	}
	
	protected String validateUrl(PortletRequest request) {
		JSONObject json = JSONFactoryUtil.createJSONObject();
		String code = Constantes.REDIRECT_URL_BLANK;
		SolicitudBean pasoBean = new SolicitudBean();
		json.put(RequestParam.CODE, ResponseMessage.SUCCESS.getCode());
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String codigoServicioError = StringPool.BLANK;
		try{
			PortletPreferences preferences = request.getPreferences();
			String urlActual = PortalUtil.getCurrentURL(request);
			String pasoSolicitud = HttpUtil.getParameter(urlActual, RequestParam.PASO, Boolean.FALSE);
			String jsonPaso = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN)):StringPool.BLANK;
			String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
			ComercioBean commerceText = StringPool.BLANK.equalsIgnoreCase(jsonCommerce)?new ComercioBean():JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
			pasoBean = StringPool.BLANK.equalsIgnoreCase(jsonPaso)?new SolicitudBean():JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			validateStepSession(pasoSolicitud, pasoBean, request);
			switch (pasoSolicitud) { 
				case Constantes.URL_NAME_TIPO_FLUJO:
					UsuarioSessionBean usuarioSessionBean = Utils.obtenerEstablecimientoTienda(request);
					if(Validator.isNotNull(usuarioSessionBean) && themeDisplay.isSignedIn()){
						request.setAttribute(RequestParam.TRADE_NAME, Utils.getTradeName(usuarioSessionBean.getEstablecimiento(), usuarioSessionBean.getTienda()));
						request.setAttribute(RequestParam.TRADE_URL, Utils.getTradeUrl(usuarioSessionBean));
					}else throw new WebServicesException();
					break;
				case Constantes.URL_NAME_INICIO_FLUJO:
					ExpedienteBean expediente = Utils.obtenerContenidoComercio(request);
					pasoSolicitud = expediente.getHtmlCode();
					codigoServicioError = expediente.getCodigoServicioError();
					commerceText = expediente.getComercioBean();
					request.setAttribute(Constantes.STC_VA_LIST_OPERA_VALOR, setChildByFatherParam(Constantes.LIST_OPERA_VALOR));
					break;
				case Constantes.URL_OTP_SMS:
					pasoBean.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_SMS);
					request.setAttribute(RequestParam.FORMAT_CELL_NUMBRER, Utils.replaceCadenaX(pasoBean.getClienteBean().getCellPhoneNumberOtp(), Constantes.TELEPHONE_FIELD_TYPE));
					break;
				case Constantes.URL_OTP_EMAIL:
					pasoBean.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_EMAIL);
					pasoBean = gestorService.generarToken(pasoBean, preferences.getValue(RequestParam.PATH_GENERAR_TOKEN,StringPool.BLANK));
					request.setAttribute(RequestParam.FORMAT_EMAIL_NUMBRER, Utils.replaceCadenaX(pasoBean.getClienteBean().getEmailAddressOtp(), Constantes.EMAIL_FIELD_TYPE));
					break;
				case Constantes.URL_OTP_BANCA_SMS:
					pasoBean.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_SMS);
					pasoBean = gestorService.generarToken(pasoBean, preferences.getValue(RequestParam.PATH_GENERAR_TOKEN,StringPool.BLANK));
					request.setAttribute(RequestParam.FORMAT_CELL_NUMBRER, Utils.validateOtpType(Constantes.TYPE_OTP_SMS, pasoBean)?Utils.replaceCadenaX(pasoBean.getClienteBean().getCellPhoneNumberOtp(), Constantes.TELEPHONE_FIELD_TYPE):StringPool.BLANK);
					break;
				case Constantes.URL_OTP_MULT:
				case Constantes.URL_OTP_VALIDACION:
					request.setAttribute(RequestParam.FORMAT_EMAIL_NUMBRER, Utils.validateOtpType(Constantes.TYPE_OTP_EMAIL, pasoBean)?Utils.replaceCadenaX(pasoBean.getClienteBean().getEmailAddressOtp(), Constantes.EMAIL_FIELD_TYPE):StringPool.BLANK);
					request.setAttribute(RequestParam.FORMAT_CELL_NUMBRER, Utils.validateOtpType(Constantes.TYPE_OTP_SMS, pasoBean)?Utils.replaceCadenaX(pasoBean.getClienteBean().getCellPhoneNumberOtp(), Constantes.TELEPHONE_FIELD_TYPE):StringPool.BLANK);
					request.setAttribute(RequestParam.VALIDATE_PEPPER_TYPE, Constantes.PEPPER_TYPE_PHYSICAL.equalsIgnoreCase(pasoBean.getExpedienteBean().getPepperType())?Boolean.TRUE:Boolean.FALSE);
					break;
				case Constantes.URL_VAL_TOKEN:
					request.setAttribute(RequestParam.FORMAT_EMAIL_NUMBRER,  Utils.replaceCadenaX(pasoBean.getClienteBean().getEmailAddressOtp(), Constantes.EMAIL_FIELD_TYPE));
					request.setAttribute(RequestParam.FORMAT_CELL_NUMBRER, Utils.replaceCadenaX(pasoBean.getClienteBean().getCellPhoneNumberOtp(), Constantes.TELEPHONE_FIELD_TYPE));
					break;
				case Constantes.URL_RESULTADO_TC:
					request.setAttribute(RequestParam.CAMPAIGNS_OFFERS, pasoBean.getOfertasBean().getOfertaBean());
					commerceText.getDynamicTextBean().setV201002(StringUtil.replace(commerceText.getDynamicTextBean().getV201002(), Constantes.TXT_REPLACE_NOMBRE, pasoBean.getClienteBean().getFirstname()));
					break;
				case Constantes.URL_EQUIFAX:
					pasoBean = getEquifaxQuestions(request, pasoBean);
					break;
				case Constantes.URL_CALIFICACION:
					loadDataCDA(pasoBean, request);
					break;
				case Constantes.URL_CONF_TC:
					request.setAttribute(Constantes.STC_VA_LIST_TIPO_VIA, setChildByFatherParam(Constantes.LIST_TIPO_VIA));
					request.setAttribute(Constantes.STC_VA_LIST_DEPARTAMENTOS,setDepartamentos());
					Boolean flagFechaHora3G = Constantes.TIPO_3G.equals(preferences.getValue(RequestParam.TIPO_FECHA_HORA,StringPool.BLANK))?Boolean.TRUE:Boolean.FALSE;
					int serviceCode = 0;
					if(flagFechaHora3G){
						pasoBean = gestorService.consultarFechaHoraApi3G(pasoBean, pasoBean.getClienteBean().getDirecciones().getDireccion().get(0).getUbigeo(), preferences.getValue(RequestParam.PATH_FECHA_HORA,StringPool.BLANK));
						serviceCode = pasoBean.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getCourier().getIsAvailable()?1:0;
					}else{
						pasoBean = gestorService.consultarFechaHoraApi2G(pasoBean, pasoBean.getClienteBean().getDirecciones().getDireccion().get(0), preferences.getValue(RequestParam.PATH_FECHA_HORA,StringPool.BLANK));
						Boolean flagDisponibilidad = Validator.isNotNull(pasoBean.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getFlagDisponibilidad())?Boolean.parseBoolean(pasoBean.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getFlagDisponibilidad()):Boolean.FALSE;
						serviceCode = flagDisponibilidad?1:0;
					}
					request.setAttribute(RequestParam.CODE, serviceCode); 
					if(serviceCode==1){
						JSONObject jsonAddress = Utils.loadDeliveryAddress(pasoBean, flagFechaHora3G, pasoBean.getClienteBean().getDirecciones().getDireccion().get(0));
						request.setAttribute(RequestParam.DATES, jsonAddress.getString(RequestParam.DATES)); 
						request.setAttribute(RequestParam.SCHEDULE, jsonAddress.getString(RequestParam.SCHEDULE)); 
					}
					pasoBean.setFhEntregaBean(new FHEntregaBean());
					request.setAttribute(RequestParam.VALIDATE_LINE, Utils.validateLine(pasoBean));
					request.setAttribute(RequestParam.FORMAT_CREDIT_LINE, Utils.currencyFormat(pasoBean.getProductoBean().getLineaCredito()));
					commerceText.getDynamicTextBean().setV501002(StringUtil.replace(commerceText.getDynamicTextBean().getV501002(), Constantes.TXT_REPLACE_TARJETA, pasoBean.getProductoBean().getDescripcionTipoProducto()));
					break;
				case Constantes.URL_FINAL:
					request.setAttribute(RequestParam.FORMAT_EXPIRATION_DATE, Utils.formatExpirationDate(pasoBean.getProductoBean().getFechaVencProvisional()));
					request.setAttribute(RequestParam.FORMAT_CREDIT_LINE, Utils.currencyFormat(pasoBean.getProductoBean().getLineaCredito()));
					request.setAttribute(RequestParam.CSS_TEXT_COLOR, Utils.setTextColor(pasoBean));
					break;
				case Constantes.URL_EXITO:
					request.setAttribute(RequestParam.FORMAT_CREDIT_LINE, Utils.currencyFormat(pasoBean.getProductoBean().getLineaCredito()));
					break;
				case Constantes.URL_NAME_RECOMPRA:
				case Constantes.URL_NAME_EXTORNO:
					ExpedienteBean expedienteBean = Utils.obtenerContenidoComercio(request);
					pasoSolicitud = expedienteBean.getHtmlCode();
					codigoServicioError = expedienteBean.getCodigoServicioError();
					commerceText = expedienteBean.getComercioBean();
					break;
				case Constantes.HTML_POTC:
					request.setAttribute(RequestParam.FORMAT_EMAIL_NUMBRER,  Utils.replaceCadenaX(pasoBean.getClienteBean().getEmailAddressOtp(), Constantes.EMAIL_FIELD_TYPE));
					request.setAttribute(RequestParam.FORMAT_CELL_NUMBRER, Utils.replaceCadenaX(pasoBean.getClienteBean().getCellPhoneNumberOtp(), Constantes.TELEPHONE_FIELD_TYPE));
					request.setAttribute(RequestParam.TIPO_MENSAJE, Constantes.TIPO_FLUJO_EXTORNO.equals(pasoBean.getExpedienteBean().getTipoFlujo())?"E":StringPool.BLANK);
					request.setAttribute(RequestParam.FECHA_OPERACION, DatesUtil.formatoFechaHoraRegistroGMT5());
					break;
				default:
					String errorCode = Validator.isNotNull(pasoBean.getExpedienteBean().getCodigoServicioError()) && !StringPool.BLANK.equals(pasoBean.getExpedienteBean().getCodigoServicioError())?pasoBean.getExpedienteBean().getCodigoServicioError():pasoSolicitud;
					pasoSolicitud = getURLError(errorCode, request);
					break;
			}
			String templateContent = Utils.getTemplateContent(request, pasoSolicitud);
			request.setAttribute(RequestParam.STEP_CONTENT, !StringPool.BLANK.equals(templateContent)?JournalArticleLocalServiceUtil.getLatestArticle(themeDisplay.getScopeGroupId(), templateContent):StringPool.BLANK);
			request.setAttribute(RequestParam.PASO_BEAN, showDataByStep(pasoSolicitud, pasoBean));
			request.setAttribute(RequestParam.COMMERCE_TEXT,commerceText.getDynamicTextBean());
			code = pasoSolicitud;
		}catch (WebServicesException e) {
			codigoServicioError = e.getCodServicioError();
			code = Constantes.REDIRECT_URL_BLANK;
			_log.error(MsgError.MSJ_ERROR_P1.replace("##",e.getNombreWS())+MsgError.MSJ_ERROR_P2+e.getTrazaMessage(), e);
		} catch (PortalException | ValidationException e ) {
			codigoServicioError = Constantes.HTML_ERROR_VALIDACION;
			code = Constantes.REDIRECT_URL_BLANK;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		} catch (SystemException e) {
			codigoServicioError = Constantes.HTML_ERROR_URL;
			code = Constantes.REDIRECT_URL_BLANK;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			pasoBean.getExpedienteBean().setCodigoServicioError(codigoServicioError);
			if(!StringPool.BLANK.equals(codigoServicioError) && Constantes.REDIRECT_URL_BLANK.equals(code))
				request.setAttribute(RequestParam.REDIRECCIONAR_URL, pasoBean.getExpedienteBean().getCodigoServicioError());
			addSessionUser(pasoBean, request);
		}
		return code;
	}
	
	public SolicitudBean verificarSesionBean(String jsonBeanSession, String stepCurrent) throws SessionException
	{
		SolicitudBean pasoBean = new SolicitudBean();
		try {
			if(stepCurrent.equals(Constantes.PASO_1) || stepCurrent.equals(Constantes.PASO_0))
				return pasoBean;
			if(Validator.isBlank(jsonBeanSession))
				throw new SessionException(Constantes.COD_S_E_OBT_BEAN,Constantes.NAME_S_E_OBT_BEAN);
			pasoBean=GsonUtil.objectOfJSON(jsonBeanSession, SolicitudBean.class);
		}
		catch (Exception e) {
			 _log.error(" Error al Obtener PasoBean de Session", e);
			throw new SessionException(Constantes.COD_S_E_OBT_BEAN,Constantes.NAME_S_E_OBT_BEAN);
		}
		return pasoBean;
	}
	
	public String metodoAutenticacion(SolicitudBean pasoBeanSession, PortletRequest request){
		String content = StringPool.BLANK;
		try {
			PortletPreferences preferences = request.getPreferences();
			SolicitudBean solicitudBean = pasoBeanSession;
			if(Constantes.CLIENTE_SI.equals(pasoBeanSession.getClienteBean().getFlagCliente()))
				solicitudBean = gestorService.validarBancaSMS(pasoBeanSession, preferences.getValue(RequestParam.ENDPOINT_BANCA_SMS,StringPool.BLANK), preferences.getValue(RequestParam.PATH_BANCA_SMS,StringPool.BLANK));
			if(Validator.isNotNull(solicitudBean.getExpedienteBean().getFlagAfiliacionBancaSMS()) && solicitudBean.getExpedienteBean().getFlagAfiliacionBancaSMS())
				content = Constantes.URL_OTP_BANCA_SMS;
			else if(Validator.isNotNull(solicitudBean.getClienteBean().getEmailAddressOtp()) && Validator.isNotNull(solicitudBean.getClienteBean().getCellPhoneNumberOtp())){
				content = Constantes.URL_OTP_MULT;
				solicitudBean.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_MULTIPLE);
			}else if (Validator.isNotNull(solicitudBean.getClienteBean().getEmailAddressOtp())){
				content = Constantes.URL_OTP_EMAIL;
				solicitudBean.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_EMAIL);
			}else if (Validator.isNotNull(solicitudBean.getClienteBean().getCellPhoneNumberOtp())){
				content = Constantes.URL_OTP_SMS;
				solicitudBean.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_SMS);
			}else
				content = Constantes.URL_EQUIFAX;
		} catch (WebServicesException e) {
			content = Constantes.HTML_ERROR;
			_log.error(e);
		}
		return content;
	}

	public String metodoAutenticacionFisico(SolicitudBean pasoBeanSession){
		String content;
		if(Validator.isNotNull(pasoBeanSession.getClienteBean().getEmailAddressOtp()) && Validator.isNotNull(pasoBeanSession.getClienteBean().getCellPhoneNumberOtp())){
			content = Constantes.URL_OTP_MULT;
			pasoBeanSession.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_MULTIPLE);
		}else if (Validator.isNotNull(pasoBeanSession.getClienteBean().getCellPhoneNumberOtp())){
			content = Constantes.URL_OTP_SMS;
			pasoBeanSession.getProductoBean().setTipoSeguro(Constantes.TYPE_OTP_SMS);
		}else
			content = Constantes.URL_EQUIFAX;
		return content;
	}
	
	public String validateAddress(SolicitudBean pasoBean){
		String flagDireccion = Constantes.FLAG_TRUE;
		try{
			String flagCliente = pasoBean.getClienteBean().getFlagCliente();
			if(!pasoBean.getClienteBean().getDirecciones().getDireccion().isEmpty() 
					&& flagCliente.equals(Constantes.CLIENTE_SI) 
					&& Constantes.CODIGO_CORRESP_PRINCI.equals(pasoBean.getClienteBean().getDirecciones().getDireccion().get(0).getCodigoUso()) 
					&& Constantes.FLAG_DIRECCION_ESTANDAR.equals(pasoBean.getClienteBean().getDirecciones().getDireccion().get(0).getFlagDireccionEstandar()))
						flagDireccion = Constantes.FLAG_FALSE;
		}catch(Exception e){
			_log.error("Error en Exception: ", e);
		}
		return flagDireccion;
	}
	
	protected String getURLError(String desEstado, PortletRequest request) {
		String urlError = desEstado.toLowerCase();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), urlError);
			String articleId = journalArticle.getArticleId(); 
			long groupId = themeDisplay.getScopeGroupId();
			request.setAttribute("articleId", articleId);
			request.setAttribute("groupId", groupId);
			request.setAttribute("errorPage", urlError);
			request.setAttribute("errorMessage", Utils.validateErrorType(urlError));
		} catch (Exception e) {
			_log.error("No se puede obtener datos del contenido:".concat(urlError), e);
		}
		return Constantes.ERROR_FILE.concat(urlError);
	}
	
	public void loadDataCDA(SolicitudBean pasoBean, PortletRequest request){
		request.setAttribute(Constantes.STC_VA_LIST_OCUPACION, setChildByFatherParam(Constantes.LIST_OCUPA_ACTUA));
		request.setAttribute(Constantes.STC_VA_LIST_SITUACION_LABORAL, setChildByFatherParam(Constantes.LIST_SITUA_LABOR));
		request.setAttribute(Constantes.STC_VA_LIST_DEPARTAMENTOS,setDepartamentos());
		pasoBean.getClienteBean().setFlagDireccion(validateAddress(pasoBean));
		request.setAttribute(Constantes.STC_VA_LIST_ESTADO_CIVIL, setChildByFatherParam(Constantes.LIST_ESTADO_CIVIL));
		request.setAttribute(Constantes.STC_VA_LIST_TIPO_VIA, setChildByFatherParam(Constantes.LIST_TIPO_VIA));
		request.setAttribute(Constantes.STC_VA_LIST_OPERA_VALOR, setChildByFatherParam(Constantes.LIST_OPERA_VALOR));
		request.setAttribute("flagFlujoCampania",pasoBean.getProductoBean().getFlujoCampana()==Constantes.FLUJO_REGULAR?Boolean.TRUE:Boolean.FALSE);
	}
	
	public ComercioBean validateContentCommerce(PortletRequest request){
		ComercioBean commerceText = new ComercioBean();
		try {
			String urlActual = PortalUtil.getCurrentURL(request);
			if(Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))){
				String jsonCommerce = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN));
				commerceText = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
			}else{
				ParametroHijoPO ph = ParametroHijoPOLocalServiceUtil.getParametroHijoByCodigoAndCodigoPadreAndDato2(HttpUtil.getParameter(urlActual, RequestParam.HCID, Boolean.FALSE), HttpUtil.getParameter(urlActual, RequestParam.HCID, Boolean.FALSE), Constantes.TBL_MAESTRO);
				if(Validator.isNotNull(ph))
					commerceText = Utils.loadCommerceData(ph, request);
			}
		} catch (SystemException e) {
			_log.error("No se pudo obtener el contenido del comercio solicitado", e);
		}
		return commerceText;
	}
	
	public SolicitudBean validateCDA(SolicitudBean pasoBean, PortletRequest request) throws WebServicesException{
		try{
			PortletPreferences preferences = request.getPreferences();
			int flujoCampana = pasoBean.getProductoBean().getFlujoCampana();
			String flagDireccion = Constantes.FLAG_TRUE;
			if(Validator.isNotNull(pasoBean.getExpedienteBean().getExpedienteCDA()) && !StringPool.BLANK.equalsIgnoreCase(pasoBean.getExpedienteBean().getExpedienteCDA()))
				pasoBean.getExpedienteBean().setFlagCalificacion(Boolean.TRUE);
			if(!pasoBean.getClienteBean().getDirecciones().getDireccion().isEmpty() 
					&& Constantes.CLIENTE_SI.equals(pasoBean.getClienteBean().getFlagCliente()) 
					&& Constantes.CODIGO_CORRESP_PRINCI.equals(pasoBean.getClienteBean().getDirecciones().getDireccion().get(0).getCodigoUso()) 
					&& Constantes.FLAG_DIRECCION_ESTANDAR.equals(pasoBean.getClienteBean().getDirecciones().getDireccion().get(0).getFlagDireccionEstandar()))
						flagDireccion = Constantes.FLAG_FALSE;
			if((Constantes.FLUJO_FAST_TRACK == flujoCampana || Constantes.FLUJO_100_APROBADO == flujoCampana) 
					&& !Boolean.parseBoolean(flagDireccion) && (Validator.isNull(pasoBean.getExpedienteBean().getExpedienteCDA()) || StringPool.BLANK.equalsIgnoreCase(pasoBean.getExpedienteBean().getExpedienteCDA())))
					pasoBean = gestorService.evaluarCalificacion(pasoBean, preferences.getValue(RequestParam.ENDPOINT_CALIFICACION,StringPool.BLANK), preferences.getValue(RequestParam.PATH_CALIFICACION,StringPool.BLANK));
			else if(Constantes.FLUJO_REGULAR == flujoCampana)
				pasoBean.getClienteBean().getDirecciones().setDireccion(null);
			else if(Constantes.FLUJO_FAST_TRACK != flujoCampana && Constantes.FLUJO_100_APROBADO != flujoCampana)
				pasoBean.getProductoBean().setFlujoCampana(Constantes.FLUJO_REGULAR);
		}catch (WebServicesException e) {
			_log.error(e);
			throw e;
		}
		return pasoBean;
	}
	
	public SolicitudBean showDataByStep(String step, SolicitudBean pasoBean){
		SolicitudBean stepBean = new SolicitudBean();
		switch (step) {
		case Constantes.ERROR_FILE+Constantes.URL_TIENE_TC:
			stepBean.getProductoBean().setDescripcionTipoProducto(pasoBean.getProductoBean().getDescripcionTipoProducto());
			stepBean.getProductoBean().setMarcaProducto(pasoBean.getProductoBean().getMarcaProducto());
			stepBean.getProductoBean().setTipoProducto(pasoBean.getProductoBean().getTipoProducto());
			break;
		case Constantes.URL_OTP_SMS:
		case Constantes.URL_OTP_EMAIL:
		case Constantes.URL_OTP_MULT:
		case Constantes.URL_VAL_TOKEN:
		case Constantes.URL_RESULTADO_TC:
			stepBean.getClienteBean().setFirstname(pasoBean.getClienteBean().getFirstname());
			break;
		case Constantes.URL_OTP_VALIDACION:
		case Constantes.URL_OTP_BANCA_SMS:
			stepBean.getClienteBean().setFirstname(pasoBean.getClienteBean().getFirstname());
			stepBean.getProductoBean().setTipoSeguro(pasoBean.getProductoBean().getTipoSeguro());
			break;
		case Constantes.URL_EQUIFAX:
			stepBean.getClienteBean().setFirstname(pasoBean.getClienteBean().getFirstname());
			stepBean.getClienteBean().setDniNumber(pasoBean.getClienteBean().getDniNumber());
			break;
		case Constantes.URL_CALIFICACION:
			stepBean.getProductoBean().setFlujoCampana(pasoBean.getProductoBean().getFlujoCampana());
			stepBean.getClienteBean().setCivilStatus(pasoBean.getClienteBean().getCivilStatus());
			stepBean.getClienteBean().setFlagDireccion(pasoBean.getClienteBean().getFlagDireccion());
			stepBean.getClienteBean().setFlagCliente(pasoBean.getClienteBean().getFlagCliente());
			break;
		case Constantes.URL_CONF_TC:
			stepBean.getProductoBean().setLineaCredito(pasoBean.getProductoBean().getLineaCredito());
			stepBean.getProductoBean().setLineaCreditoMinima(pasoBean.getProductoBean().getLineaCreditoMinima());
			stepBean.getProductoBean().setDescripcionTipoProducto(pasoBean.getProductoBean().getDescripcionTipoProducto());
			stepBean.getClienteBean().getDirecciones().setDireccion(pasoBean.getClienteBean().getDirecciones().getDireccion());
			stepBean.getClienteBean().setEmailAddress(pasoBean.getClienteBean().getEmailAddress());
			stepBean.getProductoBean().setMarcaProducto(pasoBean.getProductoBean().getMarcaProducto());
			stepBean.getProductoBean().setTipoProducto(pasoBean.getProductoBean().getTipoProducto());
			break;
		case Constantes.URL_FINAL:
			stepBean.getProductoBean().setNumTarjetaProvisional(Utils.formatCard(pasoBean.getProductoBean().getNumTarjetaProvisional()));
			stepBean.getProductoBean().setUniquePromotionalCode(pasoBean.getProductoBean().getUniquePromotionalCode());
			stepBean.getProductoBean().setCvvCode(pasoBean.getProductoBean().getCvvCode());
			stepBean.getProductoBean().setCodigo4csc(pasoBean.getProductoBean().getCodigo4csc());
			stepBean.getProductoBean().setCvv(pasoBean.getProductoBean().getCvv());
			stepBean.getProductoBean().setFechaVencProvisional(pasoBean.getProductoBean().getFechaVencProvisional());
			stepBean.getProductoBean().setDescripcionTipoProducto(pasoBean.getProductoBean().getDescripcionTipoProducto());
			stepBean.getProductoBean().setDiaDePago(pasoBean.getProductoBean().getDiaDePago());
			stepBean.getProductoBean().setTea(pasoBean.getProductoBean().getTea());
			stepBean.getProductoBean().setSeguroDesgravamen(pasoBean.getProductoBean().getSeguroDesgravamen());
			stepBean.getProductoBean().setMarcaProducto(pasoBean.getProductoBean().getMarcaProducto());
			stepBean.getProductoBean().setTipoProducto(pasoBean.getProductoBean().getTipoProducto());
			stepBean.getProductoBean().setProvisionalLinePepper(pasoBean.getProductoBean().getProvisionalLinePepper());
			stepBean.getProductoBean().setCobroMembresia(pasoBean.getProductoBean().getCobroMembresia());
			stepBean.getProductoBean().setEmailStateAccount(pasoBean.getProductoBean().getEmailStateAccount());
			stepBean.getExpedienteBean().setFlagConsultarFechaHora(pasoBean.getExpedienteBean().getFlagConsultarFechaHora());
			break;
		case Constantes.URL_EXITO:
			stepBean.getClienteBean().setFirstname(pasoBean.getClienteBean().getFirstname());
			stepBean.getProductoBean().setDiaDePago(pasoBean.getProductoBean().getDiaDePago());
			stepBean.getProductoBean().setDescripcionTipoProducto(pasoBean.getProductoBean().getDescripcionTipoProducto());
			stepBean.getProductoBean().setTea(pasoBean.getProductoBean().getTea());
			stepBean.getProductoBean().setSeguroDesgravamen(pasoBean.getProductoBean().getSeguroDesgravamen());
			stepBean.getProductoBean().setMarcaProducto(pasoBean.getProductoBean().getMarcaProducto());
			stepBean.getProductoBean().setTipoProducto(pasoBean.getProductoBean().getTipoProducto());
			stepBean.getProductoBean().setCobroMembresia(pasoBean.getProductoBean().getCobroMembresia());
			stepBean.getProductoBean().setEmailStateAccount(pasoBean.getProductoBean().getEmailStateAccount());
			break;
		case Constantes.URL_POTC:
			stepBean.getClienteBean().setFirstname(pasoBean.getClienteBean().getFirstname());
			break;
		default:
			stepBean = null;
			break;
		}
    	return stepBean;
    }
	
	public void validateStepSession(String step, SolicitudBean pasoBean, PortletRequest request) throws ValidationException{
		Boolean flagStepSession = Boolean.FALSE;
		String paso = pasoBean.getExpedienteBean().getPaso();
		switch (step) {
		case Constantes.URL_NAME_TIPO_FLUJO:
		case Constantes.URL_NAME_INICIO_FLUJO:
		case Constantes.URL_NAME_RECOMPRA:
		case Constantes.URL_NAME_EXTORNO:
			flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_RESULTADO_TC:
			if(Constantes.PASO_2.equalsIgnoreCase(paso))
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_OTP_SMS:
		case Constantes.URL_OTP_EMAIL:
		case Constantes.URL_OTP_MULT:
		case Constantes.URL_OTP_VALIDACION:
			if(Constantes.PASO_3.equalsIgnoreCase(paso) && Constantes.CLIENTE_SI.equalsIgnoreCase(pasoBean.getClienteBean().getFlagCliente()))
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_OTP_BANCA_SMS:
			if(Constantes.PASO_3.equalsIgnoreCase(paso) && pasoBean.getExpedienteBean().getFlagAfiliacionBancaSMS())
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_EQUIFAX:
			if(Constantes.PASO_3.equalsIgnoreCase(paso))
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_CALIFICACION:
			if(Constantes.PASO_4.equalsIgnoreCase(paso))
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_CONF_TC:
			if(Constantes.PASO_5.equalsIgnoreCase(paso))
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_EXITO:
			if(Constantes.PASO_6.equalsIgnoreCase(paso))
				flagStepSession = Boolean.TRUE;
			break;
		case Constantes.URL_FINAL:
			if(Constantes.PASO_6.equalsIgnoreCase(paso) && GetterUtil.getBoolean(SessionKeys.getSessionPortal(request, SessionKeys.FLAG_PAN))){
				flagStepSession = Boolean.TRUE;
				SessionKeys.addSessionPortal(request, SessionKeys.FLAG_PAN, Boolean.FALSE);
			}
			break;
		case Constantes.URL_POTC:
			if(Constantes.PASO_6.equalsIgnoreCase(paso) && pasoBean.getOtcBean().getAceptado())
				flagStepSession = Boolean.TRUE;
			break;
		default:
			flagStepSession = validateStepErrorSession(step,pasoBean);
			break;
		}
		if (!flagStepSession)
			throw new ValidationException("Intenta ingresar a un paso no autorizado");
	}
	
	public Boolean validateStepErrorSession(String step, SolicitudBean pasoBean) throws ValidationException{
		Boolean flagStepSession = Boolean.FALSE;
		String paso = pasoBean.getExpedienteBean().getPaso();
		switch (step) {
			case Constantes.URL_TIENE_TC:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNotNull(pasoBean.getExpedienteBean().getFlagCampania()) && !pasoBean.getExpedienteBean().getFlagCampania() && Constantes.CLIENTE_SI.equalsIgnoreCase(pasoBean.getClienteBean().getFlagCliente()) )
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.URL_NO_CAMPANIA:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNotNull(pasoBean.getExpedienteBean().getFlagCampania()) && !pasoBean.getExpedienteBean().getFlagCampania())
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_CONS_CAMP:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNull(pasoBean.getExpedienteBean().getFlagCampania()))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_OBT_INFO:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && (Validator.isNull(pasoBean.getExpedienteBean().getFlagObtenerInformacion()) || !pasoBean.getExpedienteBean().getFlagObtenerInformacion()))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_LISTA_NEGRA:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNull(pasoBean.getExpedienteBean().getFlagR1() || !pasoBean.getExpedienteBean().getFlagR1()))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.URL_BLACK_LIST:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNotNull(pasoBean.getExpedienteBean().getFlagR1()) && pasoBean.getExpedienteBean().getFlagR1())
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_NO_VIA:
			case Constantes.HTML_ERROR_TRAMTE_PROCESO:
			case Constantes.HTML_ERROR_LOGICA_REINGRESO:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNotNull(pasoBean.getExpedienteBean().getFlagLogicaReingreso()) && !pasoBean.getExpedienteBean().getFlagLogicaReingreso())
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_CONTINUAR:
				if(Constantes.PASO_2.equalsIgnoreCase(paso))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_CONSULTAR_TARJETA:
			case Constantes.HTML_ERROR_OBTENER_TARJETA:
				if(Constantes.PASO_2.equalsIgnoreCase(paso) && Validator.isNotNull(pasoBean.getExpedienteBean().getFlagObtenerTarjeta()) && !pasoBean.getExpedienteBean().getFlagObtenerTarjeta())
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_GENERAR_TOKEN:
				if(Constantes.PASO_3.equalsIgnoreCase(paso) && Constantes.CLIENTE_SI.equalsIgnoreCase(pasoBean.getClienteBean().getFlagCliente()) &&  Validator.isNotNull(pasoBean.getExpedienteBean().getFlagGeneracionToken()) && !pasoBean.getExpedienteBean().getFlagGeneracionToken())
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_CONSULTAR_EQUIFAX:
				if(Constantes.PASO_3.equalsIgnoreCase(paso))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_VALIDAR_TOKEN:
			case Constantes.HTML_TOKEN_INVALIDO:
			case Constantes.HTML_ERROR_GENERAR_POTC:
			case Constantes.HTML_ERROR_POTC:
			case Constantes.HTML_ERROR_BANCA_SMS:
				if(Constantes.PASO_4.equalsIgnoreCase(paso) && Constantes.CLIENTE_SI.equalsIgnoreCase(pasoBean.getClienteBean().getFlagCliente()))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_VALIDAR_EQUIFAX:
			case Constantes.HTML_EQUIFAX_DESAPROBADO:
				if(Constantes.PASO_4.equalsIgnoreCase(paso))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_NO_CALIFICA:
			case Constantes.HTML_ERROR_CALIFICACION:
				if(Validator.isNotNull(pasoBean.getExpedienteBean().getFlagCalificacion()) && !pasoBean.getExpedienteBean().getFlagCalificacion())
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_FECHA_HORA:
				if(Constantes.PASO_5.equalsIgnoreCase(paso))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_ALTA_TC:
				if(Constantes.PASO_6.equalsIgnoreCase(paso) && (Validator.isNull(pasoBean.getExpedienteBean().getFlagVentaTC()) || !pasoBean.getExpedienteBean().getFlagVentaTC()))
					flagStepSession = Boolean.TRUE;
				break;
			case Constantes.HTML_ERROR_VALIDACION:
			case Constantes.HTML_ERROR_URL:
			case Constantes.HTML_ERROR:
			case Constantes.HTML_ERROR_REDIRECCION:
			case Constantes.HTML_ERROR_ANTIFRAUDE:	
				flagStepSession = Boolean.TRUE;
				break;
			default:
				flagStepSession = Boolean.FALSE;
				break;
		}
		return flagStepSession;
	}
	
	public UsuarioSessionBean addUserSessionBean(ResourceRequest request, ComercioBean comercioBean){
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		UsuarioSessionBean usuarioSessionBean = Utils.obtenerEstablecimientoTienda(request);
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		usuarioSessionBean.setIdSession(httpServletRequest.getSession().getId());
		usuarioSessionBean.setUserId(themeDisplay.isSignedIn()?usuarioSessionBean.getUserId():0);
		usuarioSessionBean.setEstado(Constantes.ESTADO_ACTIVO);
		usuarioSessionBean.setTienda(themeDisplay.isSignedIn()?usuarioSessionBean.getTienda():comercioBean.getCommerceName());
		usuarioSessionBean.setEstablecimiento(themeDisplay.isSignedIn()?usuarioSessionBean.getEstablecimiento():comercioBean.getCommerceName());
		usuarioSessionBean.setVendedor(themeDisplay.isSignedIn()?usuarioSessionBean.getVendedor():comercioBean.getCommerceName());
		long idSession = gestorRegistroService.registrarUsuarioSession(usuarioSessionBean);
		usuarioSessionBean.setIdUsuarioSession(idSession);
		return usuarioSessionBean;
	}
	
	public SolicitudBean setearDatosSolicitud(ResourceRequest request, ClienteBean clienteBean, String flagLPDP, String flowType){
		String strLpdp = Validator.isNull(flagLPDP)?Constantes.RESPUESTA_NO:Constantes.RESPUESTA_SI;
		SolicitudBean pasoBean = new SolicitudBean();
		HttpServletRequest requestUtil = PortalUtil.getHttpServletRequest(request);
		pasoBean.getExpedienteBean().setDispositivo(Utils.getDevice(request));
		pasoBean.getExpedienteBean().setIp(Utils.getClientIp(requestUtil));
		String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
		ComercioBean comercioBean = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
		UsuarioSessionBean usuarioSessionBean = addUserSessionBean(request, comercioBean);
		pasoBean.getExpedienteBean().setCommerceId(usuarioSessionBean.getEstablecimiento());
		pasoBean.getExpedienteBean().setScreenName(usuarioSessionBean.getVendedor());
		pasoBean.setUsuarioSessionBean(usuarioSessionBean);
		String messageId = Utils.getMessageId(comercioBean.getPepperOnlineType());
		_log.debug(messageId);
		pasoBean.getExpedienteBean().setMessageId(messageId);
		pasoBean.getExpedienteBean().setIdUsuarioSession(new BigDecimal(usuarioSessionBean.getIdUsuarioSession()).intValue());
		pasoBean.getExpedienteBean().setPepperType(Constantes.PEPPER_TYPE_MATERIAL.equals(comercioBean.getPepperOnlineType())?Constantes.PEPPER_TYPE_PHYSICAL:Constantes.PEPPER_TYPE_ONLINE);
		pasoBean.getExpedienteBean().setDniNumber(clienteBean.getDniNumber());
		pasoBean.getClienteBean().setDniNumber(clienteBean.getDniNumber());
		pasoBean.getClienteBean().setCellPhoneNumber(clienteBean.getCellPhoneNumber());
		pasoBean.getClienteBean().setCellPhoneOperator(clienteBean.getCellPhoneOperator());
		pasoBean.getExpedienteBean().setTipoFlujo(flowType);
		pasoBean.getExpedienteBean().setEstado(Constantes.ESTADO_PENDIENTE);
		pasoBean.getProductoBean().setFlagLPDP(strLpdp);
		pasoBean.getProductoBean().setProvisionalLinePepper(comercioBean.getEquifaxMax());
		pasoBean.getClienteBean().setEmailAddress(clienteBean.getEmailAddress());
		return pasoBean;
	}
	
	public SolicitudBean getEquifaxQuestions(PortletRequest request, SolicitudBean pasoBean) throws WebServicesException{
		SolicitudBean paso = new SolicitudBean();
		try{
			PortletPreferences preferences = request.getPreferences();
			String modelo = PrefsPropsUtil.getString(PortalKeys.REST_EQUIFAX_MODELO2,PortalValues.REST_EQUIFAX_MODELO2);
			pasoBean.getEquifaxBean().setModelo(modelo);
			paso = gestorService.consultarPreguntasEquifax(pasoBean, preferences.getValue(RequestParam.PATH_CONSULTAR_PREGUNTAS,StringPool.BLANK));
			paso.getClienteBean().setNroOperacion(paso.getEquifaxBean().getNroOperacion());
			Integer cantidadPreguntas = paso.getEquifaxBean().getPreguntas().size();
			if(cantidadPreguntas > 0) 
				request.setAttribute("lstPreguntaBean",paso.getEquifaxBean().getPreguntas());
			else
				throw new WebServicesException(Constantes.CONS_PREG_EQUIFAX, Constantes.HTML_ERROR_CONSULTAR_EQUIFAX, Constantes.CONS_PREG_EQUIFAX, StringPool.BLANK);
		} catch (Exception e) {
			throw new WebServicesException(Constantes.CONS_PREG_EQUIFAX, Constantes.HTML_ERROR_CONSULTAR_EQUIFAX, Constantes.CONS_PREG_EQUIFAX, StringPool.BLANK, e);
		}
		
		return paso;
	}
	
	public SolicitudBean validarServiciosEnCampania(SolicitudBean pasoBean, ResourceRequest request) throws WebServicesException{
		String contentHTML = StringPool.BLANK;
		SolicitudBean paso = new SolicitudBean();
		try{
			PortletPreferences preferences = request.getPreferences();
			paso = gestorService.validarListaNegra2G(pasoBean, preferences.getValue(RequestParam.PATH_LISTA_NEGRA,StringPool.BLANK));
			Boolean resultado = paso.getExpedienteBean().getFlagR1();
			paso.getExpedienteBean().setFlagR1(resultado);
			if (resultado || Validator.isNull(resultado)) 
				contentHTML = Constantes.URL_BLACK_LIST;
			else{
				paso = gestorService.validarLogicaReingreso(paso, preferences.getValue(RequestParam.ENDPOINT_LOGICA_REINGRESO,StringPool.BLANK), preferences.getValue(RequestParam.PATH_LOGICA_REINGRESO,StringPool.BLANK));
				paso = Utils.getRedirectPageRetryLogic(paso); 
				contentHTML = paso.getExpedienteBean().getCodigoServicioError();
				contentHTML = Constantes.HTML_CONTINUAR.equals(paso.getExpedienteBean().getHtmlCode())?Constantes.URL_RESULTADO_TC:contentHTML;	
			}
		} catch (WebServicesException e) {
			_log.error(e);
			throw e;
		}finally{
			paso.getExpedienteBean().setHtmlCode(contentHTML);
		}
		return paso;
	}
	
}