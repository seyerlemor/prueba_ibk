package pe.com.ibk.pepper.util;

public class Constantes {
	
	public static final String SSL = "SSL";
	public static final String TLS = "TLSv1.2";
	
	public static final String ESTADO_PENDIENTE = "P";
	public static final String ESTADO_FINALIZADO = "F";
	public static final String ESTADO_RECHAZADO = "R";
	public static final String TIPO_FORMULARIO = "FRM";
	public static final String TIPO_HTML = "HTML";

	public static final String TIPO_FLUJO_VENTA_TC = "10";
	public static final String TIPO_FLUJO_RECOMPRA = "20";
	public static final String TIPO_FLUJO_EXTORNO = "30";

	public static final String URL_TARJETA = "/solicitar/tarjeta/credito/";
	
	public static final String SUCCESSFUL = "SUCCESSFUL";
	public static final String ERROR = "ERROR";
	
	public static final Integer RESULT_STATUS = 200;
	public static final Integer RESULT_ERROR = 403;
	public static final String BUS_RESPONSE_CODE = "502";
	public static final String GESTOR_SERVICIO_REST = "GestorServicioRest";
	public static final String RESPONSE_TEXT_SIN_VALOR = "Sin Valor";
	public static final String VALOR_NULO_PEPPER = "valorNuloPepper";

	public static final String PASO_0 = "00";
	public static final String PASO_1 = "10";
	public static final String PASO_2 = "20";
	public static final String PASO_3 = "30";
	public static final String PASO_4 = "40";
	public static final String PASO_5 = "50";
	public static final String PASO_6 = "60";
	public static final String PASO_7 = "70";
	public static final String PAGINA_1 = "10";
	public static final String PAGINA_2 = "20";
	public static final String PAGINA_3 = "30";
	public static final String PAGINA_4 = "40";
	
	public static final String RESULTADO_APROBADO = "APROBADO";
	public static final String RESULTADO_DESAPROBADO = "DESAPROBADO";

	public static final String INTENTO_SI = "SI";
	public static final String INTENTO_NO = "NO";

	public static final String FLAG_TRUE = "true";
	public static final String FLAG_FALSE = "false";
	
	public static final String TIPO_INTEGRANTE1 = "1";
	public static final String TIPO_INTEGRANTE2 = "2";
	public static final String ESTADO_ACTIVO = "A";
	public static final String ESTADO_INACTIVO = "I";
	public static final String ESTADO_TIPO_CASADO = "M";
	public static final String ESTADO_TIPO_SOLTERO = "U";
	public static final String ESTADO_TIPO_CONVIVIENTE = "O";
	public static final String CLIENTE_SI = "S";
	public static final String CLIENTE_NO = "N";
	public static final String NUMERO_CLIENTE_SI = "1";
	public static final String NUMERO_CLIENTE_NO = "0";
	public static final String UBIGEO_LIMA_DEPARTAMENTO = "15";
	public static final String UBIGEO_LIMA_PROVINCIA = "01";
	public static final String UBIGEO_LIMA_DISTRITO = "01";
	public static final String LIMA = "LIMA";
	public static final String UBIGEO_LIMA = "150101";
	public static final String CODIGO_CORRESP_PRINCI = "PRINCI";
	public static final String CODIGO_CORRESP_00EMPE = "00EMPE";
	public static final String CODIGO_CORRESP_EMAPER = "EMAPER";
	public static final String CODIGO_USO_PRINCI = "PRINCI";
	public static final String CODIGO_USO_ESTADO = "ESTADO";
	public static final String CODIGO_USO_CORR = "CORR";
	public static final String CODIGO_USO_CORRES = "CORRES";
	public static final String CODIGO_USO_LABORAL = "LABORAL";
	public static final String FLAG_DIRECCION_ESTANDAR = "S";
	public static final String ERROR_RESPONSE_CDA = "3";
	//
	public static final String ORIG_ERROR_RENIEC = "RENIEC";
	public static final String ORIG_ERROR_EQUIFAX = "EQUIFAX";
	public static final String WS_RENIEC_HIT = "hit";
	public static final String WS_RENIEC_ERROR = "error";
	public static final String EQUIFAX_RESULTADO_APROBADO = "APROBADA";
	public static final String PASO_CONSULTA_RAPIDA = "10";
	
	// Consulta Campania - Tipo tarjetas
	public static final String TARJETA_MARCA_VISA = "01";
	public static final String TARJETA_MARCA_AMEX = "02";
	public static final String TARJETA_MARCA_MASTERCARD = "04";
	
	public static final int NRO_PRIMER_INTENTO = 1;
	public static final String VALOR_COMBO_OTRA_DIRECCION = "00";
	public static final String VALOR_COMBO_MISMA_DIRECCION = "999";
	public static final String VALOR_CERO="0";
	
	public static final String LIST_OPERA_VALOR="LIST_OPERA_VALOR";
	public static final String LIST_TIPO_VIA="LIST_TIPO_VIA";
	public static final String LIST_OCUPA_ACTUA="LIST_OCUPA_ACTUA";
	public static final String LIST_CARGO_ACTUA="LIST_CARGO_ACTUA";
	public static final String LIST_SITUA_LABOR="LIST_SITUA_LABOR";
	public static final String LIST_INGRE_LABOR="LIST_INGRE_LABOR";
	public static final String LIST_ACTIV_NEGOC="LIST_ACTIV_NEGOC";
	public static final String SOLICITUD_TARJETA="SOLICITUD_TARJETA";
	public static final String LIST_NIVEL_EDUCA="LIST_NIVEL_EDUCA";
	public static final String LIST_ESTADO_CIVIL="LIST_ESTADO_CIVIL";
	
	public static final String TBL_MAESTRO = "TBL_MAESTRO";
		
	public static final String WS_VLD_LST_NGR="WS_VLD_LST_NGR";
	public static final String WS_CONS_CAMP="WS_CONS_CAMP";
	public static final String WS_CONS_RENIEC="WS_CONS_RENIEC";
	public static final String WS_CONS_PREG_EQFX="WS_CONS_PREG_EQFX";
	public static final String WS_VAL_PREG_EQFX="WS_VAL_PREG_EQFX";
	public static final String WS_OBT_INFO_PERS="WS_OBT_INFO_PERS";
	public static final String WS_CALIF_CDA="WS_CALIF_CDA";
	public static final String WS_TIPIFICAR="=WS_TIPIFICAR";
	public static final String WS_ACT_RESP="WS_ACT_RESP";
	public static final String WS_AUT_TOKEN="WS_AUT_TOKEN";
	public static final String WS_CON_AFILIACION="WS_CON_AFILIACION";
	public static final String WS_VENTA_TC="WS_VENTA_TC_PROVIS";
	public static final String WS_OBT_TARJETA="WS_OBT_TARJETA"; 
	public static final String WS_CONS_TARJETA="WS_CONS_TARJETA"; 
	public static final String WS_CALIF_CDA2="WS_CALIF_CDA2";
	public static final String WS_VAL_TOKEN="WS_VAL_TOKEN";
	public static final String WS_GEN_POTC="WS_GEN_POTC";
	public static final String WS_CONS_POTC="WS_CONS_POTC";	
	public static final String WS_CONS_PROV="WS_CONS_PROV";
	public static final String WS_ENV_CORREO="WS_ENV_CORREO";
	public static final String WS_LOG_REINGRESO="WS_LOG_REINGRESO"; 
	public static final String WS_CALIF_EFL="WS_CALIF_EFL"; 
	public static final String WS_VAL_RUC="WS_VAL_RUC";
	
	public static final String TIPO_POTC_EXTORNO = "APPLE-DEV";
	public static final String TIPO_POTC_RECOMPRA = "APPLE-PAGO";
	
	public static final String TIPO_POTC_EXTORNO_DESC = "EXTORNO";
	public static final String TIPO_POTC_RECOMPRA_DESC = "RECOMPRA";
	
	//Se agregra codigo y nombre de cada servicio
	public static final String COD_VALID_LISTA_NEGRA = "01048";
	public static final String COD_CONS_CAMPANIA = "01061";
	public static final String COD_CONS_RENIEC = "01067";
	public static final String COD_CONS_PREG_EQUIFAX = "01130";
	public static final String COD_VALID_PREG_EQUIFAX = "01131";
	public static final String COD_TIPIF_INTENTO = "01158";
	public static final String COD_OBT_INFO_PERSONA = "01145";
	public static final String COD_CALIF_CDA = "00000";
	public static final String COD_ACT_RESPUESTA = "01062";
	public static final String COD_CONS_AFILIACION = "15001";
	public static final String COD_GEN_TOKEN = "GEN_TOKEN";
	public static final String COD_VALID_TOKEN = "VAL_TOKEN";
	public static final String COD_VENTA_TC = "VENTA_TC";
	public static final String COD_CONS_POTC = "CON_POTC";
	public static final String COD_GEN_POTC = "GEN_POTC";
	public static final String COD_CONS_PROV = "CONS_PROV";
	public static final String COD_OBT_TARJETA = "OBT_TRJ";
	public static final String COD_CONS_TARJETA = "CONS_TRJ";
	public static final String COD_ENVIO_CORREO = "ENV_COR";
	public static final String COD_LOG_REINGRESO = "LOG_REING";
	public static final String COD_CONS_EFL = "CON_EFL";
	public static final String COD_VALIDAR_RUC = "CON_EFL";
	public static final String COD_CONS_FECHA_HORA = "CON_FH";
	public static final String COD_ANTIFRAUDE = "ANTIFRAUDE";
	public static final String COD_BANCASMS = "BANCA_SMS";
		
	public static final String VALID_LISTA_NEGRA = "Validar Lista Negra";
	public static final String CONS_CAMPANIA = "Consultar Campania";
	public static final String CONS_RENIEC = "Consultar Reniec";
	public static final String CONS_PREG_EQUIFAX = "Consultar Preguntas Equifax";
	public static final String VALID_PREG_EQUIFAX = "Validar Preguntas Equifax";
	public static final String TIPIF_INTENTO = "Tipificacion Intento";	
	public static final String OBT_INFO_PERSONA = "Obtener Informacion Persona";
	public static final String CALIF_CDA = "Calificacion CDA";
	public static final String ACT_RESPUESTA = "Actualizar Respuesta";
	public static final String CONS_AFILIACION = "Consultar Afiliacion";	
	public static final String GEN_TOKEN = "Generacion Token";
	public static final String VENTA_TC = "Venta TC Provisional";
	public static final String CONS_POTC = "Consultar POTC";
	public static final String GEN_POTC = "Generar POTC";
	public static final String VALID_TOKEN = "Validar Token";
	public static final String CONS_PROV = "Consultar Provisional";
	public static final String OBT_TARJETA = "Obtener Tarjetas";
	public static final String CONS_TARJETA = "Consultar Tarjetas";
	public static final String ENVIO_CORREO = "Notificacion Envio Correo";
	public static final String LOG_REINGRESO = "Logica Reingreso";
	public static final String CALIF_EFL = "Calificacion EFL";
	public static final String REG_AUDITORIA = "Registro de Auditoria";
	public static final String VALIDAR_RUC = "Validar Ruc";
	public static final String VALIDAR_RECAPTCHA = "Validar Recaptcha";
	public static final String CONS_FECHA_HORA = "Consultar Fecha Hora";
	public static final String RETRY_LOGIC = "RetryLogic";
	public static final String ANTIFRAUDE = "ANTIFRAUDE";

	/* Codigos de tablas Pepper, excepcion: principal + detalle + ubigeo*/
	public static final Integer T_AUDITORIA_CLIENTE = 100;
	public static final Integer T_AUDITORIA_USUARIO = 110;
	public static final int T_CABECERA_EQUIFAX = 120;
	public static final int T_CAMPANIA = 130;
	public static final int T_CLIENTE_EXPEDIENTE = 140;
	public static final int T_CLIENTE_TRANSACCION = 150;
	public static final int T_DATOS_CLIENTE = 160;
	public static final int T_DATOS_FAMILIA = 170;
	public static final int T_DIRECCION_CLIENTE = 180;
	public static final int T_DIRECCION_ESTANDAR = 190;
	public static final int T_OTC = 200;
	public static final int T_PREGUNTAS_EQUIFAX = 210;
	public static final int T_PRODUCTO_CLIENTE = 220;
	public static final int T_RENIEC = 230;
	public static final int T_SERVICIOS = 240;
	public static final int T_USUARIO_SESSION = 250;
	public static final int T_LOGQRADAR = 260;
	
	/* Datos de preferencias en htmls de pantallas en contenidos */
	public static final String URL_NAME_INICIO = "inicio-sesion";
	public static final String URL_NAME_INICIO_FLUJO = "inicio";
	public static final String URL_NAME_TIPO_FLUJO = "options";
	public static final String URL_NAME_INICIO_HELPDESK = "inicio-helpdesk";
	public static final String URL_TIENE_TC = "tiene-tarjeta";
	public static final String URL_NAME_CONSULTA_RAPIDA= "consulta-rapida";
	public static final String URL_RENIEC="foto";
	public static final String URL_EQUIFAX="pregunta-ex";
	public static final String URL_NAME_NO_CAMPANIA="dejanos-tus-datos";
	public static final String URL_CALIFICACION="calificacion";
	public static final String URL_CONFIRMAR="resumen";
	public static final String URL_EXITO="exito";
	public static final String URL_FINAL = "resumen";
	public static final String URL_NAME_RECOMPRA = "recompra";
	public static final String URL_NAME_EXTORNO = "extorno";
	public static final String URL_NO_CAMPANIA = "no-campania";
	public static final String URL_NO_OFERTA_CAMPANIA= "no-es-lo-que-buscas";
	public static final String URL_CONF_TC = "configuracion";
	public static final String URL_VAL_TOKEN = "validacion-token";
	public static final String URL_F_RPTA_FALLO_EX = "gracias-por-intentarlo";
	public static final String URL_ERROR_R1 = "lo-sentimos-r";
	public static final String URL_ERROR_BLOQUE_EX = "lo-sentimos-b";
	public static final String URL_RESULTADO_TC="resultado-tc";
	public static final String URL_OTP_MULT="otp-options";
	public static final String URL_OTP_MULT_V2="otp-options-v2";
	public static final String URL_OTP_SMS="otp-sms";
	public static final String URL_OTP_EMAIL="otp-email";
	public static final String URL_OTP_BANCA_SMS = "otp-banca-sms";
	public static final String HTML_POTC = "potc";
	public static final String URL_BLACK_LIST="lista-negra";
	public static final String URL_OTP_VALIDACION="otp-validacion-codigo";
	public static final String HTML_NO_CALIFICA = "no-califica";
	public static final String HTML_EQUIFAX_DESAPROBADO = "no-equifax";
	public static final String HTML_TOKEN_INVALIDO = "token-invalido";
	public static final String HTML_NO_CLIENTE = "no-cliente";
	public static final String URL_POTC="potc";
	
	/*PAGINAS DE ERROR*/
	public static final String HTML_ERROR_CONSULTAR_EQUIFAX = "error-consultar-equifax";
	public static final String HTML_ERROR_VALIDAR_EQUIFAX = "error-validar-equifax";
	public static final String HTML_ERROR_LISTA_NEGRA = "error-lista";
	public static final String HTML_ERROR_DNI_NO_EXISTE_OBT_INF = "dni-no-existe";
	public static final String HTML_ERROR_OBT_INFO = "error-datos";
	public static final String HTML_ERROR_CONS_CAMP = "error-campania";
	public static final String HTML_ERROR_VAL_EQUIFAX = "respuesta-invalida";
	public static final String HTML_ERROR_NO_VIA = "error-no-via";
	public static final String HTML_ERROR_TRAMTE_PROCESO = "error-tramite-proceso";
	public static final String HTML_ERROR_ALTA_TC = "error-alta-tc";
	public static final String HTML_ERROR_POTC = "error-potc";
	public static final String HTML_ERROR = "error";
	public static final String HTML_CONTINUAR = "continuar";
	public static final String HTML_ERROR_URL = "error-url";
	public static final String ERROR_FILE = "../error/";
	public static final String HTML_ERROR_FECHA_HORA = "error-fechahora";
	public static final String HTML_ERROR_CALIFICACION = "error-calificacion";
	public static final String HTML_ERROR_CONSULTAR_TARJETA = "error-consultar-tarjeta";
	public static final String HTML_ERROR_GENERAR_TOKEN = "error-token";
	public static final String HTML_ERROR_VALIDAR_TOKEN = "error-validar-token";
	public static final String HTML_ERROR_LOGICA_REINGRESO = "error-logica-reingreso";
	public static final String HTML_ERROR_VALIDACION = "error-validation";
	public static final String HTML_ERROR_OBTENER_TARJETA = "error-obtener-tarjeta";
	public static final String HTML_ERROR_INTENTOS_EQUIFAX = "intentos-equifax";
	public static final String HTML_ERROR_ANTIFRAUDE = "error-antifraude";
	public static final String HTML_ERROR_GENERAR_POTC = "error-generar-potc";
	public static final String HTML_ERROR_BANCA_SMS = "error-banca-sms";
	/**/
	
	public static final String EVALUACION_CDA_NO_CALIFICA = "1";
	public static final String EVALUACION_CDA_CALIFICA = "2";
	
	public static final String TEXT_REDIRECCION_URL = "redirectURL";
	public static final String HTML_ERROR_REDIRECCION = "redirection";
	
	public static final String ROL_GERENTE_OPERACIONES = "GerenteOperacionesIShop";
	public static final String ROL_TEST = "test";
	public static final String ROL_HELPDESK = "HelpDesk";
	public static final String ESTABLECIMIENTO_ISHOP="iShop";
	public static final String ESTABLECIMIENTO_OTRO="O";
	
	public static final String TIPO_TARJETA_CREDITO = "C";
	public static final String NO_TIENE_TARJETA = "No";
	public static final String TIENE_TARJETA = "Si";
	
	public static final String RESPUESTA_SI = "S";
	public static final String RESPUESTA_NO = "N";
	
	/*MOTIVOS NO CALIFICA*/
	public static final String NUEVO_COLABORADOR="31370";
	public static final String NO_PUNTO_VENTA_COLABORADOR="30279";
	public static final String CODIGO_DEFAULT="0";
	public static final String NOMBRE_DEFAULT="Todos";
	
	public static final String NO_TIENE_CAMPANIA = "NO";
	public static final String TIENE_CAMPANIA = "SI";
	public static final String TIPO_CAMPANIA_EFL = "EFL";
	public static final String EFL_ACEPTADO = "Aceptado";
	public static final String SCORE_INICIAL = "430";
	public static final String RESPUESTA_CREDIBILIDAD = "Green";
	public static final String ESTADO_EFL = "complete";

	public static final String DISPOSITIVO_PC="1";
	public static final String DISPOSITIVO_MOBILE="2";
	
	/**/
	public static final String ID_LOGO_DEFAULT="interbank";
	
	public static final String DOCUMENTO_NO_EXISTE ="NRO. DE DOCUMENTO CONSULTADO NO EXISTE";
	public static final String CONYUGE_DIFERENTE = "EL DNI DEL CÓNYUGE DEBE SER DIFERENTE AL SOLICITANTE";
	public static final String DOCUMENTO_INVALIDO = "Número de documento inválido";
	public static final String CLIENTE_SIN_TARJETAS = "0010";
	
	public static final String SERVICEID_LOG = "PEPPER_ONLINE";
	public static final String TRAMA_TIPO_REQUEST = "REQUEST";
	public static final String TRAMA_TIPO_RESPONSE = "RESPONSE";
	
	public static final String PRE_CONSULTA_RAPIDA ="consultaRapida";
	
	/*QRADAR*/
	
	public static final int SVRD_ALERTA = 1;
	public static final int SVRD_CRITICO = 2;
	public static final int SVRD_ERROR = 3;
	public static final int SVRD_PELIGRO = 4;
	public static final int SVRD_AVISO = 5;
	public static final int SVRD_INFORMACION = 6;
	public static final int SVRD_DEBUG = 7;
	
	public static final int RECURSO_AUTORIZACION = 4;
	public static final int RECURSO_AUTENTICACION = 10;
	public static final int RECURSO_PCI = 16;
	public static final int RECURSO_DATOS_PERSONA = 17;
	public static final int RECURSO_OTROS = 18;
	
	public static final String EVENTO_LOGIN = "LOGIN";
	public static final String EVENTO_LOGOUT = "LOGOUT";
	public static final String EVENTO_ALTA = "ALTA";
	public static final String EVENTO_RECOMPRA = "RECOMPRA";
	public static final String EVENTO_EXTORNO = "EXTORNO";
	public static final String EVENTO_CLIENTE = "DATOS CLIENTE";
	public static final String EVENTO_CAMPANIA = "CAMPANA";
	public static final String EVENTO_MANIPULACION_URL = "MANIPULACION URL";
	public static final int ID_PEPPER = 1;
	
	public static final String AMBIENTE_UAT = "UAT";
	public static final String AMBIENTE_PRD = "PRD";
	
	public static final String SUCCESS = "SUCCESS";
	public static final String FAIL = "FAIL";
	
	public static final String TELEFONO_TIPO_CELULAR = "C";
	public static final String TYPE_2_PHONE = "2";
	public static final String MESSAGEID = "MESSAGEID";
	
	public static final String ID_TEST = "20159";
	
	public static final Integer TYPE_INTEGRANTE1 = 1;
	public static final Integer TYPE_INTEGRANTE2 = 2;
	
	public static final String COD_S_E_OBT_BEAN="SE0001";
	public static final String NAME_S_E_OBT_BEAN="Session Caducada";
	
	public static final String TYPE_OTP_SMS = "1";
	public static final String TYPE_OTP_EMAIL = "0";
	public static final String TYPE_OTP_MULTIPLE = "2";
	
	
	public static final String MSG_ERROR_JS_SOL_DAT_PER="No se pudo validar el DNI, por favor vuelve a intentarlo.";
	public static final String MSG_ERROR_SAME_DOC="El DNI del cónyuge debe ser diferente al solicitante.";
	public static final String MSG_ERROR_VALIDAR_RUC = "Estamos teniendo inconvenientes en validar el RUC";
	public static final String MSG_ERROR_VALIDAR_TOKEN = "Estamos teniendo inconvenientes en validar el Token";
	public static final String WS_EXCEPCION_DES_TRAZA = "Datos del Error: [ResponseType: ";
	public static final String REDIRECT_URL_BLANK = "../blank";
	
	public static final String STC_VA_LIST_DEPARTAMENTOS="lstDepartamentos";
	public static final String STC_VA_LIST_TIPO_VIA="lstTipoVia";
	public static final String STC_VA_LIST_OCUPACION="lstOcupacion";
	public static final String STC_VA_LIST_SITUACION_LABORAL="lstSituacionLaboral";
	public static final String STC_VA_LIST_TIEMPO_SERVICIO="lstTiempoServicio";
	public static final String STC_VA_LIST_GIRO_NEGOCIO="lstGiroNegocio";
	public static final String STC_VA_LIST_SEXO="lstSexo";
	public static final String STC_VA_LIST_ESTADO_CIVIL="lstEstadoCivil";
	public static final String STC_VA_LIST_OPERA_VALOR = "lstOperador";
	
	public static final String NAME_VA_REQ_VALOR="value";
	public static final String DEFAULT_COD_UBIGEO = "00";
	public static final String COD_TIPO_BUSQUEDA_PROVINCIA = "2";
	
	public static final String ESTADO_ERROR = "E";
	public static final String ESTADO_ERROR_WS = "EW";
	public static final String ESTADO_ERROR_DATA = "ED";
	public static final String ESTADO_ERROR_FLUJO = "EF";
	
	public static final String COD_PARAM_E_OBT_PRO_BD="PR0001";
	public static final String COD_PARAM_E_OBT_UBIGEO="PR0002";
	public static final String COD_PARAM_E_LIST_VACIA="PR0003";
	public static final String COD_PARAM_E_LIST_CASTEO="PR0004";
	
	public static final String NAME_PARAM_E_OBT_PRO_BD="No BD No Properties Parm";
	public static final String NAME_PARAM_E_OBT_UBIGEO="No se Obtuvo Ubigeo";
	public static final String NAME_PARAM_E_LIST_VACIA="Lista Vacia";
	public static final String NAME_PARAM_E_LIST_CASTEO="Casteo Obt. Lista";
	
	public static final String NAME_V_A_STC_ENCON_UBI="ENCONTRADO";
	public static final String NAME_V_A_STC_VALOR_UBI="VALOR";
	
	public static final String DIRECCION_TIPO_INGRESO_SERVICIO="S";
	public static final String DIRECCION_TIPO_INGRESO_MANUAL="M";
	public static final String DIRECCION_COD_USO_CORRESPONDENCIA="COR";
	public static final String DIRECCION_COD_USO_ESTADO_CUENTA="ECU";
	public static final String COD_TELEFONO_RESIDENCIA="R";
	
	public static final String COD_ADD_NRO = "NRO";
	public static final String COD_ADD_MZ = "MZ";
	public static final String COD_ADD_LT = "LT";
	public static final String COD_ADD_INT = "INT";
	
	public static final int FLUJO_FAST_TRACK = 1;
	public static final int FLUJO_REGULAR = 3;
	public static final int FLUJO_100_APROBADO = 5;
	
	public static final String EMAIL_FIELD_TYPE = "E";
	public static final String TELEPHONE_FIELD_TYPE = "T";
	
	public static final String APPLICATION_JSON_TYPE_UTF8 = "application/json;charset=UTF-8";
	
	public static final String ENVIO_EECC_EMAIL = "E-mail";
	
	public static final String TEXT_VALIDATION_ERROR = "Error en la clase ValidationException:>";
	
	public static final String TIPO_CODIGO_CVV2= "1";
	
	public static final String ID_CONTENT_RESULTADO_TC = "contenidoResultadoTC";
	public static final String ID_CONTENT_OTP = "contenidoOTP";
	public static final String ID_CONTENT_EQUIFAX = "contenidoEquifax";
	public static final String ID_CONTENT_CALIFICACION = "contenidoCalificacion";
	public static final String ID_CONTENT_CONFIGURACION = "contenidoConfiguracion";
	public static final String ID_CONTENT_RESUMEN = "contenidoResumen";
	
	public static final String PEPPER_TYPE_PHYSICAL = "F";
	public static final String PEPPER_TYPE_ONLINE = "O";
	public static final String CODIGO_PROMOCIONAL_NO_VALIDO = "00000";
	
	public static final String FECHA_HORA_2G = "2G";
	public static final String FECHA_HORA_3G = "3G";
	
	public static final String CONTENT = "Content";
	public static final String TXT_REPLACE_NOMBRE = "[NOMBRE]";
	public static final String TXT_REPLACE_TARJETA = "[TARJETA]";
	
	public static final String WITHOUT_FILE = "0";
	public static final String WITH_FILE = "1";
	
	public static final String PEPPER_TYPE_PROVISIONAL = "PROVISIONAL";
	public static final String PEPPER_TYPE_CARDLESS = "CARDLESS";
	public static final String PEPPER_TYPE_MATERIAL = "FISICO";
	public static final String PEPPER_TYPE_TAG = "PEPPER";
	
	public static final String FORMATO_FECHA_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String FORMATO_FECHA_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	
	public static final String HTML_SELECT_VALUE_0 = "<option value selected disabled>Seleccione</option>";
	
	/*DESCRIPCION MARCA TIPO TARJETA*/
	public static final String VISA_CLASICA = "0101";
	public static final String VISA_SIGNATURE ="0114";
	public static final String VISA_CASHBACK_SIGNATURE = "0116";
	public static final String VISA_PREMIA = "0117";
	public static final String VISA_CASHBACK_PLATINUM = "0121";
	public static final String VISA_CASHBACK_ORO = "0122";
	public static final String VISA_PLATINUM = "0123";
	public static final String VISA_INFINITIVE = "0129";
	public static final String VISA_ACCESS = "0131";
	public static final String AMERICAN_EXPRESS = "0203";
	public static final String AMERICAN_EXPRESS_GOLD = "0204";
	public static final String THE_PLATINUM_CARD = "0206";
	public static final String AMERICAN_EXPRESS_BLUE = "0207";
	public static final String AMERICAN_EXPRESS_PLATINUM = "0208";
	public static final String AMERICAN_EXPRESS_BLACK = "0209";
	public static final String MASTERCARD_CLASICA = "0401";
	public static final String MASTERCARD_ORO = "0402";
	public static final String MASTERCARD_PLATINUM = "0404";
	
	/*HTML*/
	public static final String OPEN_DIV_OPTION = "<div class=\"lh-form__radio\">";
	public static final String CLOSE_SPAN_LABEL_DIV = "</span></label></div>";
	public static final String CSS_WHITE_TEXT = "css-white-text";
	public static final String CSS_BLACK_TEXT = "css-black-text";
	
	public static final String TEXT_TYPE_PHYSICAL = "FISICO";
	public static final String TEXT_TYPE_ONLINE = "ONLINE";
	
	public static final String PEPPER_PHYSICAL_FINAL_MESSAGE = "Process";
	
	public static final String APPLICATION_VALIDATION = "Validacion de Solicitud";
	public static final String SERVICE_RESPONSE = "Respuesta del Servicio";
	public static final String NO_AUTHORIZATION = "Autorizacion";
	
	public static final String PATH_URL_VENTA = "/solicitar/tarjeta/credito/inicio";
	public static final String PATH_URL_RECOMPRA = "/solicitar/tarjeta/credito/recompra";
	public static final String PATH_URL_EXTORNO = "/solicitar/tarjeta/credito/extorno";
	
	public static final String INDICADOR_DEVOLUCION_CASHBACK = "1";
	public static final String CODIGO_VENDEDOR = "USMDA";
	
	
	/*TIPO ENVIO*/
	public static final String HTTP_GET = "GET";
	public static final String HTTP_POST = "POST";
	public static final String HTTP_PUT = "PUT";
	
	/*TIPO AUTENTICACION*/
	
	public static final String AUTENTICACION_OTP = "1";
	public static final String AUTENTICACION_EQUIFAX = "3";
	
	/*TIPO CAMPANIA*/
	public static final String TIPO_API = "API";
	public static final String TIPO_FW4 = "FW4";
	
	/*3G/2G*/
	public static final String TIPO_3G = "3G";
	public static final String TIPO_2G = "2G";
	
	private Constantes(){}
}
