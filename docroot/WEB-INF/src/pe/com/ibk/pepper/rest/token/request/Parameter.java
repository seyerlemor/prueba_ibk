
package pe.com.ibk.pepper.rest.token.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Parameter {

    private String key;
    private String value;
    
    @JsonCreator
    public Parameter(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

    @JsonProperty("key")
	public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
