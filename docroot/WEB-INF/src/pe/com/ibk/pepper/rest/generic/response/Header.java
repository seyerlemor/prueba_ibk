
package pe.com.ibk.pepper.rest.generic.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Header {

    private HeaderResponse headerResponse;
 
	public Header() {
		super();
	}
	
	@JsonCreator
	public Header(
			@JsonProperty("HeaderResponse")HeaderResponse headerResponse) {
		super();
		this.headerResponse = headerResponse;
	}
	
	public HeaderResponse getHeaderResponse() {
        return headerResponse;
    }

    public void setHeaderResponse(HeaderResponse headerResponse) {
        this.headerResponse = headerResponse;
    }

	@Override
	public String toString() {
		return "Header [headerResponse=" + headerResponse + "]";
	}

}
