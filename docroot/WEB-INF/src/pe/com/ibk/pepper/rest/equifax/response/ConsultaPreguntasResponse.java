
package pe.com.ibk.pepper.rest.equifax.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class ConsultaPreguntasResponse implements Serializable {

	private static final long serialVersionUID = -8243308276868983536L;
	
	@SerializedName("numeroOperacion")
	private String numeroOperacion;

	@SerializedName("preguntas")
    private Preguntas preguntas;

	public String getNumeroOperacion() {
		return numeroOperacion;
	}

	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}

	public Preguntas getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(Preguntas preguntas) {
		this.preguntas = preguntas;
	}

}
