package pe.com.ibk.pepper.bean;

import pe.com.ibk.pepper.rest.generic.response.HttpResponseV2;

public class FHEntregaBean {
	
	private HttpResponseV2 httpResponseV2;

	public FHEntregaBean() {
		super();
	}

	public HttpResponseV2 getHttpResponseV2() {
		return httpResponseV2;
	}

	public void setHttpResponseV2(HttpResponseV2 httpResponseV2) {
		this.httpResponseV2 = httpResponseV2;
	}
	
}
