package pe.com.ibk.pepper.bean;

import java.io.Serializable;
import java.util.Date;

public class UsuarioSessionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idUsuarioSession;
	private String idSession;
	private Date fechaRegistro;
	private String tienda;
	private String estado;
	private long userId;
	private long tiendaId;
	private String establecimiento;
	private long establecimientoId;
	private String vendedor;
	private long vendedorId;
	private long rolId;
	private String fechaRegistroCadena;
	
	public long getIdUsuarioSession() {
		return idUsuarioSession;
	}
	public void setIdUsuarioSession(long idUsuarioSession) {
		this.idUsuarioSession = idUsuarioSession;
	}
	public String getIdSession() {
		return idSession;
	}
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}
	public String getTienda() {
		return tienda;
	}
	public void setTienda(String tienda) {
		this.tienda = tienda;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getTiendaId() {
		return tiendaId;
	}
	public void setTiendaId(long tiendaId) {
		this.tiendaId = tiendaId;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getEstablecimiento() {
		return establecimiento;
	}
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	public long getEstablecimientoId() {
		return establecimientoId;
	}
	public void setEstablecimientoId(long establecimientoId) {
		this.establecimientoId = establecimientoId;
	}
	public String getVendedor() {
		return vendedor;
	}
	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	public long getVendedorId() {
		return vendedorId;
	}
	public void setVendedorId(long vendedorId) {
		this.vendedorId = vendedorId;
	}
	public long getRolId() {
		return rolId;
	}
	public void setRolId(long rolId) {
		this.rolId = rolId;
	}
	public String getFechaRegistroCadena() {
		return fechaRegistroCadena;
	}
	public void setFechaRegistroCadena(String fechaRegistroCadena) {
		this.fechaRegistroCadena = fechaRegistroCadena;
	}
	@Override
	public String toString() {
		return "UsuarioSessionBean [idUsuarioSession=" + idUsuarioSession
				+ ", idSession=" + idSession + ", fechaRegistro="
				+ fechaRegistro + ", tienda=" + tienda + ", estado=" + estado
				+ ", userId=" + userId + ", tiendaId=" + tiendaId
				+ ", establecimiento=" + establecimiento
				+ ", establecimientoId=" + establecimientoId + ", vendedor="
				+ vendedor + ", vendedorId=" + vendedorId + ", rolId=" + rolId
				+ ", fechaRegistroCadena=" + fechaRegistroCadena + "]";
	}
}
