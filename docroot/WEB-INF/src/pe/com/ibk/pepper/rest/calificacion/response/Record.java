
package pe.com.ibk.pepper.rest.calificacion.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Record implements Serializable{

	private static final long serialVersionUID = 6344832347606114777L;
	
	@SerializedName(value="recordCod")
	private String recordCod;

	public String getRecordCod() {
		return recordCod;
	}

	public void setRecordCod(String recordCod) {
		this.recordCod = recordCod;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
