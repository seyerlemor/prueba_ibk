/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.Producto;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Producto in entity cache.
 *
 * @author Interbank
 * @see Producto
 * @generated
 */
public class ProductoCacheModel implements CacheModel<Producto>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(61);

		sb.append("{idProducto=");
		sb.append(idProducto);
		sb.append(", idExpediente=");
		sb.append(idExpediente);
		sb.append(", tipoProducto=");
		sb.append(tipoProducto);
		sb.append(", codigoProducto=");
		sb.append(codigoProducto);
		sb.append(", moneda=");
		sb.append(moneda);
		sb.append(", marcaProducto=");
		sb.append(marcaProducto);
		sb.append(", lineaCredito=");
		sb.append(lineaCredito);
		sb.append(", diaPago=");
		sb.append(diaPago);
		sb.append(", tasaProducto=");
		sb.append(tasaProducto);
		sb.append(", nombreTitularProducto=");
		sb.append(nombreTitularProducto);
		sb.append(", tipoSeguro=");
		sb.append(tipoSeguro);
		sb.append(", costoSeguro=");
		sb.append(costoSeguro);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", flagEnvioEECCFisico=");
		sb.append(flagEnvioEECCFisico);
		sb.append(", flagEnvioEECCEmail=");
		sb.append(flagEnvioEECCEmail);
		sb.append(", flagTerminos=");
		sb.append(flagTerminos);
		sb.append(", flagCargoCompra=");
		sb.append(flagCargoCompra);
		sb.append(", claveventa=");
		sb.append(claveventa);
		sb.append(", codigoOperacion=");
		sb.append(codigoOperacion);
		sb.append(", fechaHoraOperacion=");
		sb.append(fechaHoraOperacion);
		sb.append(", codigoUnico=");
		sb.append(codigoUnico);
		sb.append(", numTarjetaTitular=");
		sb.append(numTarjetaTitular);
		sb.append(", numCuenta=");
		sb.append(numCuenta);
		sb.append(", fechaAltaTitular=");
		sb.append(fechaAltaTitular);
		sb.append(", fechaVencTitular=");
		sb.append(fechaVencTitular);
		sb.append(", numTarjetaProvisional=");
		sb.append(numTarjetaProvisional);
		sb.append(", fechaAltaProvisional=");
		sb.append(fechaAltaProvisional);
		sb.append(", fechaVencProvisional=");
		sb.append(fechaVencProvisional);
		sb.append(", RazonNoUsoTarjeta=");
		sb.append(RazonNoUsoTarjeta);
		sb.append(", Motivo=");
		sb.append(Motivo);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Producto toEntityModel() {
		ProductoImpl productoImpl = new ProductoImpl();

		productoImpl.setIdProducto(idProducto);
		productoImpl.setIdExpediente(idExpediente);

		if (tipoProducto == null) {
			productoImpl.setTipoProducto(StringPool.BLANK);
		}
		else {
			productoImpl.setTipoProducto(tipoProducto);
		}

		if (codigoProducto == null) {
			productoImpl.setCodigoProducto(StringPool.BLANK);
		}
		else {
			productoImpl.setCodigoProducto(codigoProducto);
		}

		if (moneda == null) {
			productoImpl.setMoneda(StringPool.BLANK);
		}
		else {
			productoImpl.setMoneda(moneda);
		}

		if (marcaProducto == null) {
			productoImpl.setMarcaProducto(StringPool.BLANK);
		}
		else {
			productoImpl.setMarcaProducto(marcaProducto);
		}

		if (lineaCredito == null) {
			productoImpl.setLineaCredito(StringPool.BLANK);
		}
		else {
			productoImpl.setLineaCredito(lineaCredito);
		}

		if (diaPago == null) {
			productoImpl.setDiaPago(StringPool.BLANK);
		}
		else {
			productoImpl.setDiaPago(diaPago);
		}

		if (tasaProducto == null) {
			productoImpl.setTasaProducto(StringPool.BLANK);
		}
		else {
			productoImpl.setTasaProducto(tasaProducto);
		}

		if (nombreTitularProducto == null) {
			productoImpl.setNombreTitularProducto(StringPool.BLANK);
		}
		else {
			productoImpl.setNombreTitularProducto(nombreTitularProducto);
		}

		if (tipoSeguro == null) {
			productoImpl.setTipoSeguro(StringPool.BLANK);
		}
		else {
			productoImpl.setTipoSeguro(tipoSeguro);
		}

		if (costoSeguro == null) {
			productoImpl.setCostoSeguro(StringPool.BLANK);
		}
		else {
			productoImpl.setCostoSeguro(costoSeguro);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			productoImpl.setFechaRegistro(null);
		}
		else {
			productoImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (flagEnvioEECCFisico == null) {
			productoImpl.setFlagEnvioEECCFisico(StringPool.BLANK);
		}
		else {
			productoImpl.setFlagEnvioEECCFisico(flagEnvioEECCFisico);
		}

		if (flagEnvioEECCEmail == null) {
			productoImpl.setFlagEnvioEECCEmail(StringPool.BLANK);
		}
		else {
			productoImpl.setFlagEnvioEECCEmail(flagEnvioEECCEmail);
		}

		if (flagTerminos == null) {
			productoImpl.setFlagTerminos(StringPool.BLANK);
		}
		else {
			productoImpl.setFlagTerminos(flagTerminos);
		}

		if (flagCargoCompra == null) {
			productoImpl.setFlagCargoCompra(StringPool.BLANK);
		}
		else {
			productoImpl.setFlagCargoCompra(flagCargoCompra);
		}

		if (claveventa == null) {
			productoImpl.setClaveventa(StringPool.BLANK);
		}
		else {
			productoImpl.setClaveventa(claveventa);
		}

		if (codigoOperacion == null) {
			productoImpl.setCodigoOperacion(StringPool.BLANK);
		}
		else {
			productoImpl.setCodigoOperacion(codigoOperacion);
		}

		if (fechaHoraOperacion == Long.MIN_VALUE) {
			productoImpl.setFechaHoraOperacion(null);
		}
		else {
			productoImpl.setFechaHoraOperacion(new Date(fechaHoraOperacion));
		}

		if (codigoUnico == null) {
			productoImpl.setCodigoUnico(StringPool.BLANK);
		}
		else {
			productoImpl.setCodigoUnico(codigoUnico);
		}

		if (numTarjetaTitular == null) {
			productoImpl.setNumTarjetaTitular(StringPool.BLANK);
		}
		else {
			productoImpl.setNumTarjetaTitular(numTarjetaTitular);
		}

		if (numCuenta == null) {
			productoImpl.setNumCuenta(StringPool.BLANK);
		}
		else {
			productoImpl.setNumCuenta(numCuenta);
		}

		if (fechaAltaTitular == Long.MIN_VALUE) {
			productoImpl.setFechaAltaTitular(null);
		}
		else {
			productoImpl.setFechaAltaTitular(new Date(fechaAltaTitular));
		}

		if (fechaVencTitular == Long.MIN_VALUE) {
			productoImpl.setFechaVencTitular(null);
		}
		else {
			productoImpl.setFechaVencTitular(new Date(fechaVencTitular));
		}

		if (numTarjetaProvisional == null) {
			productoImpl.setNumTarjetaProvisional(StringPool.BLANK);
		}
		else {
			productoImpl.setNumTarjetaProvisional(numTarjetaProvisional);
		}

		if (fechaAltaProvisional == Long.MIN_VALUE) {
			productoImpl.setFechaAltaProvisional(null);
		}
		else {
			productoImpl.setFechaAltaProvisional(new Date(fechaAltaProvisional));
		}

		if (fechaVencProvisional == Long.MIN_VALUE) {
			productoImpl.setFechaVencProvisional(null);
		}
		else {
			productoImpl.setFechaVencProvisional(new Date(fechaVencProvisional));
		}

		if (RazonNoUsoTarjeta == null) {
			productoImpl.setRazonNoUsoTarjeta(StringPool.BLANK);
		}
		else {
			productoImpl.setRazonNoUsoTarjeta(RazonNoUsoTarjeta);
		}

		if (Motivo == null) {
			productoImpl.setMotivo(StringPool.BLANK);
		}
		else {
			productoImpl.setMotivo(Motivo);
		}

		productoImpl.resetOriginalValues();

		return productoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idProducto = objectInput.readLong();
		idExpediente = objectInput.readLong();
		tipoProducto = objectInput.readUTF();
		codigoProducto = objectInput.readUTF();
		moneda = objectInput.readUTF();
		marcaProducto = objectInput.readUTF();
		lineaCredito = objectInput.readUTF();
		diaPago = objectInput.readUTF();
		tasaProducto = objectInput.readUTF();
		nombreTitularProducto = objectInput.readUTF();
		tipoSeguro = objectInput.readUTF();
		costoSeguro = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
		flagEnvioEECCFisico = objectInput.readUTF();
		flagEnvioEECCEmail = objectInput.readUTF();
		flagTerminos = objectInput.readUTF();
		flagCargoCompra = objectInput.readUTF();
		claveventa = objectInput.readUTF();
		codigoOperacion = objectInput.readUTF();
		fechaHoraOperacion = objectInput.readLong();
		codigoUnico = objectInput.readUTF();
		numTarjetaTitular = objectInput.readUTF();
		numCuenta = objectInput.readUTF();
		fechaAltaTitular = objectInput.readLong();
		fechaVencTitular = objectInput.readLong();
		numTarjetaProvisional = objectInput.readUTF();
		fechaAltaProvisional = objectInput.readLong();
		fechaVencProvisional = objectInput.readLong();
		RazonNoUsoTarjeta = objectInput.readUTF();
		Motivo = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idProducto);
		objectOutput.writeLong(idExpediente);

		if (tipoProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoProducto);
		}

		if (codigoProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoProducto);
		}

		if (moneda == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(moneda);
		}

		if (marcaProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(marcaProducto);
		}

		if (lineaCredito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lineaCredito);
		}

		if (diaPago == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(diaPago);
		}

		if (tasaProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tasaProducto);
		}

		if (nombreTitularProducto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreTitularProducto);
		}

		if (tipoSeguro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoSeguro);
		}

		if (costoSeguro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(costoSeguro);
		}

		objectOutput.writeLong(fechaRegistro);

		if (flagEnvioEECCFisico == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagEnvioEECCFisico);
		}

		if (flagEnvioEECCEmail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagEnvioEECCEmail);
		}

		if (flagTerminos == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagTerminos);
		}

		if (flagCargoCompra == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagCargoCompra);
		}

		if (claveventa == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(claveventa);
		}

		if (codigoOperacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoOperacion);
		}

		objectOutput.writeLong(fechaHoraOperacion);

		if (codigoUnico == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoUnico);
		}

		if (numTarjetaTitular == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numTarjetaTitular);
		}

		if (numCuenta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numCuenta);
		}

		objectOutput.writeLong(fechaAltaTitular);
		objectOutput.writeLong(fechaVencTitular);

		if (numTarjetaProvisional == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numTarjetaProvisional);
		}

		objectOutput.writeLong(fechaAltaProvisional);
		objectOutput.writeLong(fechaVencProvisional);

		if (RazonNoUsoTarjeta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(RazonNoUsoTarjeta);
		}

		if (Motivo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Motivo);
		}
	}

	public long idProducto;
	public long idExpediente;
	public String tipoProducto;
	public String codigoProducto;
	public String moneda;
	public String marcaProducto;
	public String lineaCredito;
	public String diaPago;
	public String tasaProducto;
	public String nombreTitularProducto;
	public String tipoSeguro;
	public String costoSeguro;
	public long fechaRegistro;
	public String flagEnvioEECCFisico;
	public String flagEnvioEECCEmail;
	public String flagTerminos;
	public String flagCargoCompra;
	public String claveventa;
	public String codigoOperacion;
	public long fechaHoraOperacion;
	public String codigoUnico;
	public String numTarjetaTitular;
	public String numCuenta;
	public long fechaAltaTitular;
	public long fechaVencTitular;
	public String numTarjetaProvisional;
	public long fechaAltaProvisional;
	public long fechaVencProvisional;
	public String RazonNoUsoTarjeta;
	public String Motivo;
}