/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.AuditoriaUsuarioLocalServiceUtil;
import pe.com.ibk.pepper.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class AuditoriaUsuarioClp extends BaseModelImpl<AuditoriaUsuario>
	implements AuditoriaUsuario {
	public AuditoriaUsuarioClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return AuditoriaUsuario.class;
	}

	@Override
	public String getModelClassName() {
		return AuditoriaUsuario.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idAuditoriaUsuario;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdAuditoriaUsuario(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idAuditoriaUsuario;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idAuditoriaUsuario", getIdAuditoriaUsuario());
		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idAuditoriaUsuario = (Long)attributes.get("idAuditoriaUsuario");

		if (idAuditoriaUsuario != null) {
			setIdAuditoriaUsuario(idAuditoriaUsuario);
		}

		Long idUsuarioSession = (Long)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public long getIdAuditoriaUsuario() {
		return _idAuditoriaUsuario;
	}

	@Override
	public void setIdAuditoriaUsuario(long idAuditoriaUsuario) {
		_idAuditoriaUsuario = idAuditoriaUsuario;

		if (_auditoriaUsuarioRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaUsuarioRemoteModel.getClass();

				Method method = clazz.getMethod("setIdAuditoriaUsuario",
						long.class);

				method.invoke(_auditoriaUsuarioRemoteModel, idAuditoriaUsuario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	@Override
	public void setIdUsuarioSession(long idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;

		if (_auditoriaUsuarioRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaUsuarioRemoteModel.getClass();

				Method method = clazz.getMethod("setIdUsuarioSession",
						long.class);

				method.invoke(_auditoriaUsuarioRemoteModel, idUsuarioSession);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_auditoriaUsuarioRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaUsuarioRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_auditoriaUsuarioRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAuditoriaUsuarioRemoteModel() {
		return _auditoriaUsuarioRemoteModel;
	}

	public void setAuditoriaUsuarioRemoteModel(
		BaseModel<?> auditoriaUsuarioRemoteModel) {
		_auditoriaUsuarioRemoteModel = auditoriaUsuarioRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _auditoriaUsuarioRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_auditoriaUsuarioRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AuditoriaUsuarioLocalServiceUtil.addAuditoriaUsuario(this);
		}
		else {
			AuditoriaUsuarioLocalServiceUtil.updateAuditoriaUsuario(this);
		}
	}

	@Override
	public AuditoriaUsuario toEscapedModel() {
		return (AuditoriaUsuario)ProxyUtil.newProxyInstance(AuditoriaUsuario.class.getClassLoader(),
			new Class[] { AuditoriaUsuario.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AuditoriaUsuarioClp clone = new AuditoriaUsuarioClp();

		clone.setIdAuditoriaUsuario(getIdAuditoriaUsuario());
		clone.setIdUsuarioSession(getIdUsuarioSession());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(AuditoriaUsuario auditoriaUsuario) {
		long primaryKey = auditoriaUsuario.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditoriaUsuarioClp)) {
			return false;
		}

		AuditoriaUsuarioClp auditoriaUsuario = (AuditoriaUsuarioClp)obj;

		long primaryKey = auditoriaUsuario.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idAuditoriaUsuario=");
		sb.append(getIdAuditoriaUsuario());
		sb.append(", idUsuarioSession=");
		sb.append(getIdUsuarioSession());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.AuditoriaUsuario");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idAuditoriaUsuario</column-name><column-value><![CDATA[");
		sb.append(getIdAuditoriaUsuario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idUsuarioSession</column-name><column-value><![CDATA[");
		sb.append(getIdUsuarioSession());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idAuditoriaUsuario;
	private long _idUsuarioSession;
	private Date _fechaRegistro;
	private BaseModel<?> _auditoriaUsuarioRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}