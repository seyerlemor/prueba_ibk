package pe.com.ibk.pepper.util;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.XMLGregorianCalendar;

import pe.com.ibk.pepper.exceptions.ValidationException;

public class Validador extends Validator{
	
	private static final Pattern PATRON_DNI = Pattern.compile("[0-9]{8}");
	private static final Pattern PATRON_CARNET_EXTRANJERIA = Pattern.compile("[a-zA-ZÃƒÂ±Ãƒâ€˜0-9]{4,11}");
	private static final Pattern PATRON_NUMERO_PASAPORTE = Pattern.compile("[a-zA-ZÃƒÂ±Ãƒâ€˜0-9]{7,11}");  
	private static final Pattern PATRON_EMAIL = Pattern.compile("[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}");
	private static final Pattern PATRON_TELEFONO=Pattern.compile("[0-9]{6,15}");
	private static final Pattern PATRON_CELULAR=Pattern.compile("[9]+[0-9]{8}");
	private static final Pattern PATRON_NOMBRE= Pattern.compile("[a-zA-ZÃ±Ã‘Ã¡Ã©Ã­Ã³ÃºÃ�Ã‰Ã�Ã“ÃšÃ¼ÃœZÃƒÂ±Ãƒâ€˜ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂºÃƒï¿½Ãƒâ€°Ãƒï¿½Ã¡Ã©Ã­Ã³ÃºÃƒâ€œÃƒÅ¡ÃƒÂ¼ÃƒÅ“0-9\\s-']{1,50}");
	private static final Pattern PATRON_APELLIDO=Pattern.compile("[a-zA-ZÃƒÂ±Ãƒâ€˜ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂºÃƒï¿½Ãƒâ€°Ãƒï¿½Ãƒâ€œÃƒÅ¡ÃƒÂ¼ÃƒÅ“0-9\\s-']{1,50}");
	private static final Pattern PATRON_DINERO_FORMAT1=Pattern.compile("[0-9]{1,8}+\\.[0-9]{0,2}");
	private static final Pattern PATRON_DINERO_FORMAT2=Pattern.compile("[1-9]+[0-9]{0,7}");
	private static final Pattern PATRON_ALPAPHETIC= Pattern.compile("[A-Za-zÃ¡Ã©Ã­Ã³ÃºÃ�Ã‰Ã�Ã“ÃšÃ‘Ã±0-9.@]*");
	private static final Pattern PATRON_NOMBRES= Pattern.compile("[a-zA-Z']{1,25}");
	
	private static Log _log = LogFactoryUtil.getLog(Validador.class);
	
	public static boolean isDNI(String dni) {
		if (isNull(dni)) {
			return false;
		}
		if ("00000000".equals(dni)) {
			return false;
		}
		Matcher matcher = PATRON_DNI.matcher(dni);
		return matcher.matches();
	}
	public static boolean isCarneExtranjeria(String ce) {
		if (isNull(ce)){
			return false;
		}
		Matcher matcher = PATRON_CARNET_EXTRANJERIA.matcher(ce);
		return matcher.matches();
	}
	
	public static boolean isNumeroPasaporte(String np){
		if (isNull(np)){
			return false;
		}
		Matcher matcher = PATRON_NUMERO_PASAPORTE.matcher(np);
		return matcher.matches();
	}
	public static boolean isEmail(String email) {
		if (isNull(email)) {
			return false;
		}
		Matcher matcher = PATRON_EMAIL.matcher(email);
		return matcher.matches();
	}
	public static boolean isCelular(String celular) {
		if (isNull(celular)) {
			return false;
		}
		Matcher matcher = PATRON_CELULAR.matcher(celular);
		return matcher.matches();
	}
	public static boolean isTelefono(String telefono) {
		if (isNull(telefono)) {
			return false;
		}
		Matcher matcher = PATRON_TELEFONO.matcher(telefono);
		return matcher.matches();
	}
	
	public static boolean isNombre(String nombres){
		if(isNull(nombres)){
			return false;
		}
		Matcher matcher = PATRON_NOMBRE.matcher(nombres);
		return matcher.matches();
	}
	public static boolean isNombres(String nombres){
		if(isNull(nombres)){
			return false;
		}
		Matcher matcher = PATRON_NOMBRES.matcher(nombres);
		return matcher.matches();
	}
	public static boolean isApelido(String apellidos){
		if(isNull(apellidos)){
			return false;
		}
		Matcher matcher = PATRON_APELLIDO.matcher(apellidos);
		return matcher.matches();
	}
	
	public static boolean isFormatDinero(String dinero){
		if(isNull(dinero)){
			return false;
		}
		Matcher matcher = PATRON_DINERO_FORMAT1.matcher(dinero);
		Matcher matcher1 = PATRON_DINERO_FORMAT2.matcher(dinero);
		if(matcher.matches() || matcher1.matches() ){
			return true;
		}
		
		return false;
	}

	 @SuppressWarnings("rawtypes")
	public static boolean esObjetoVacio(Object pObjeto) {
	        Method metodos[] = pObjeto.getClass().getMethods();
	        for (int i = 0; i < metodos.length; i++) {
	            Method metodo = metodos[i];
	            //Si es un metodo get o is
	            if ((metodo.getName().substring(0, 3).equalsIgnoreCase("get") || metodo.getName().substring(0, 2).equalsIgnoreCase("is")) && !metodo.getName().equals("getClass")) {
	                try {
	                    //Si es Byte
	                    if (metodo.getReturnType().equals(java.lang.Byte.class)) {
	                        Byte valor = (Byte)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es BigDecimal
	                    else if (metodo.getReturnType().equals(java.math.BigDecimal.class)) {
	                        BigDecimal valor = (BigDecimal)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es Long
	                    else if (metodo.getReturnType().equals(java.lang.Long.class)) {
	                    	Long valor = (Long)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es Integer
	                    else if (metodo.getReturnType().equals(java.lang.Integer.class)) {
	                    	Integer valor = (Integer)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    // Si es Double
	                    else if (metodo.getReturnType().equals(java.lang.Double.class)) {
	                        Double valor = (Double)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es un string
	                    else if (metodo.getReturnType().equals(java.lang.String.class)) {
	                        String valor = (String)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es una lista
	                    else if (metodo.getReturnType().equals(java.util.List.class)) {
	                        List objetosList = (List)metodo.invoke(pObjeto, new Object[0]);
	                        if(objetosList!=null && objetosList.size()>0){
	                        	return false;
	                        }
	                    }
	                    //Si es date
	                    else if (metodo.getReturnType().equals(java.util.Date.class)) {
	                        Date valor = (Date)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es XMLGregorianCalendar
	                    else if (metodo.getReturnType().equals(javax.xml.datatype.XMLGregorianCalendar.class)) {
	                    	XMLGregorianCalendar valor = (XMLGregorianCalendar)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            return false;
	                        }
	                    }
	                    //Si es Object
	                    else {
	                    	Object valor = (Object)metodo.invoke(pObjeto, new Object[0]);
	                        if(valor!=null){
	                            if(!Validador.esObjetoVacio(valor))
	                            	return false;
	                        }
	                    }
	                } catch (Exception e) {
	                	_log.error("Error en validacion de objeto vacio "+e.toString());
	                	//e.printStackTrace();
	                }
	            }
	        }
	        return true;
	    }

	 public static void isValidJson(Object object) throws ValidationException{
		 Gson gson = new Gson();
			String json = gson.toJson(object);
			if (isNotNull(json)){
				String JsonResponse = json.toLowerCase();
				JsonResponse = JsonResponse.replaceAll("\"", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("/", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("\\{", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("\\}", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("\\[", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("\\]", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll(",", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll(":", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("&", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("%", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("-", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll("_", StringPool.BLANK);
				JsonResponse = JsonResponse.replaceAll(StringPool.SPACE, StringPool.BLANK);
				Matcher matcher = PATRON_ALPAPHETIC.matcher(JsonResponse);
				if (!matcher.matches() || JsonResponse.contains("script") || JsonResponse.contains("alert")) {
					throw new ValidationException("Los datos ingresados son incorrectos.");
				}
			}
	 }
}