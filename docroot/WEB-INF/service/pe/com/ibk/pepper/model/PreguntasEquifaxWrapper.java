/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PreguntasEquifax}.
 * </p>
 *
 * @author Interbank
 * @see PreguntasEquifax
 * @generated
 */
public class PreguntasEquifaxWrapper implements PreguntasEquifax,
	ModelWrapper<PreguntasEquifax> {
	public PreguntasEquifaxWrapper(PreguntasEquifax preguntasEquifax) {
		_preguntasEquifax = preguntasEquifax;
	}

	@Override
	public Class<?> getModelClass() {
		return PreguntasEquifax.class;
	}

	@Override
	public String getModelClassName() {
		return PreguntasEquifax.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idPregunta", getIdPregunta());
		attributes.put("idCabecera", getIdCabecera());
		attributes.put("codigoPregunta", getCodigoPregunta());
		attributes.put("descripPregunta", getDescripPregunta());
		attributes.put("codigoRespuesta", getCodigoRespuesta());
		attributes.put("descripRespuesta", getDescripRespuesta());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idPregunta = (Long)attributes.get("idPregunta");

		if (idPregunta != null) {
			setIdPregunta(idPregunta);
		}

		Long idCabecera = (Long)attributes.get("idCabecera");

		if (idCabecera != null) {
			setIdCabecera(idCabecera);
		}

		String codigoPregunta = (String)attributes.get("codigoPregunta");

		if (codigoPregunta != null) {
			setCodigoPregunta(codigoPregunta);
		}

		String descripPregunta = (String)attributes.get("descripPregunta");

		if (descripPregunta != null) {
			setDescripPregunta(descripPregunta);
		}

		String codigoRespuesta = (String)attributes.get("codigoRespuesta");

		if (codigoRespuesta != null) {
			setCodigoRespuesta(codigoRespuesta);
		}

		String descripRespuesta = (String)attributes.get("descripRespuesta");

		if (descripRespuesta != null) {
			setDescripRespuesta(descripRespuesta);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this preguntas equifax.
	*
	* @return the primary key of this preguntas equifax
	*/
	@Override
	public long getPrimaryKey() {
		return _preguntasEquifax.getPrimaryKey();
	}

	/**
	* Sets the primary key of this preguntas equifax.
	*
	* @param primaryKey the primary key of this preguntas equifax
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_preguntasEquifax.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id pregunta of this preguntas equifax.
	*
	* @return the id pregunta of this preguntas equifax
	*/
	@Override
	public long getIdPregunta() {
		return _preguntasEquifax.getIdPregunta();
	}

	/**
	* Sets the id pregunta of this preguntas equifax.
	*
	* @param idPregunta the id pregunta of this preguntas equifax
	*/
	@Override
	public void setIdPregunta(long idPregunta) {
		_preguntasEquifax.setIdPregunta(idPregunta);
	}

	/**
	* Returns the id cabecera of this preguntas equifax.
	*
	* @return the id cabecera of this preguntas equifax
	*/
	@Override
	public long getIdCabecera() {
		return _preguntasEquifax.getIdCabecera();
	}

	/**
	* Sets the id cabecera of this preguntas equifax.
	*
	* @param idCabecera the id cabecera of this preguntas equifax
	*/
	@Override
	public void setIdCabecera(long idCabecera) {
		_preguntasEquifax.setIdCabecera(idCabecera);
	}

	/**
	* Returns the codigo pregunta of this preguntas equifax.
	*
	* @return the codigo pregunta of this preguntas equifax
	*/
	@Override
	public java.lang.String getCodigoPregunta() {
		return _preguntasEquifax.getCodigoPregunta();
	}

	/**
	* Sets the codigo pregunta of this preguntas equifax.
	*
	* @param codigoPregunta the codigo pregunta of this preguntas equifax
	*/
	@Override
	public void setCodigoPregunta(java.lang.String codigoPregunta) {
		_preguntasEquifax.setCodigoPregunta(codigoPregunta);
	}

	/**
	* Returns the descrip pregunta of this preguntas equifax.
	*
	* @return the descrip pregunta of this preguntas equifax
	*/
	@Override
	public java.lang.String getDescripPregunta() {
		return _preguntasEquifax.getDescripPregunta();
	}

	/**
	* Sets the descrip pregunta of this preguntas equifax.
	*
	* @param descripPregunta the descrip pregunta of this preguntas equifax
	*/
	@Override
	public void setDescripPregunta(java.lang.String descripPregunta) {
		_preguntasEquifax.setDescripPregunta(descripPregunta);
	}

	/**
	* Returns the codigo respuesta of this preguntas equifax.
	*
	* @return the codigo respuesta of this preguntas equifax
	*/
	@Override
	public java.lang.String getCodigoRespuesta() {
		return _preguntasEquifax.getCodigoRespuesta();
	}

	/**
	* Sets the codigo respuesta of this preguntas equifax.
	*
	* @param codigoRespuesta the codigo respuesta of this preguntas equifax
	*/
	@Override
	public void setCodigoRespuesta(java.lang.String codigoRespuesta) {
		_preguntasEquifax.setCodigoRespuesta(codigoRespuesta);
	}

	/**
	* Returns the descrip respuesta of this preguntas equifax.
	*
	* @return the descrip respuesta of this preguntas equifax
	*/
	@Override
	public java.lang.String getDescripRespuesta() {
		return _preguntasEquifax.getDescripRespuesta();
	}

	/**
	* Sets the descrip respuesta of this preguntas equifax.
	*
	* @param descripRespuesta the descrip respuesta of this preguntas equifax
	*/
	@Override
	public void setDescripRespuesta(java.lang.String descripRespuesta) {
		_preguntasEquifax.setDescripRespuesta(descripRespuesta);
	}

	/**
	* Returns the fecha registro of this preguntas equifax.
	*
	* @return the fecha registro of this preguntas equifax
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _preguntasEquifax.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this preguntas equifax.
	*
	* @param fechaRegistro the fecha registro of this preguntas equifax
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_preguntasEquifax.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _preguntasEquifax.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_preguntasEquifax.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _preguntasEquifax.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_preguntasEquifax.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _preguntasEquifax.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _preguntasEquifax.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_preguntasEquifax.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _preguntasEquifax.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_preguntasEquifax.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_preguntasEquifax.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_preguntasEquifax.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PreguntasEquifaxWrapper((PreguntasEquifax)_preguntasEquifax.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.PreguntasEquifax preguntasEquifax) {
		return _preguntasEquifax.compareTo(preguntasEquifax);
	}

	@Override
	public int hashCode() {
		return _preguntasEquifax.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.PreguntasEquifax> toCacheModel() {
		return _preguntasEquifax.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.PreguntasEquifax toEscapedModel() {
		return new PreguntasEquifaxWrapper(_preguntasEquifax.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.PreguntasEquifax toUnescapedModel() {
		return new PreguntasEquifaxWrapper(_preguntasEquifax.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _preguntasEquifax.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _preguntasEquifax.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_preguntasEquifax.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PreguntasEquifaxWrapper)) {
			return false;
		}

		PreguntasEquifaxWrapper preguntasEquifaxWrapper = (PreguntasEquifaxWrapper)obj;

		if (Validator.equals(_preguntasEquifax,
					preguntasEquifaxWrapper._preguntasEquifax)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public PreguntasEquifax getWrappedPreguntasEquifax() {
		return _preguntasEquifax;
	}

	@Override
	public PreguntasEquifax getWrappedModel() {
		return _preguntasEquifax;
	}

	@Override
	public void resetOriginalValues() {
		_preguntasEquifax.resetOriginalValues();
	}

	private PreguntasEquifax _preguntasEquifax;
}