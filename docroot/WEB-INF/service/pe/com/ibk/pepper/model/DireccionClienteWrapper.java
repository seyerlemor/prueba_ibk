/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DireccionCliente}.
 * </p>
 *
 * @author Interbank
 * @see DireccionCliente
 * @generated
 */
public class DireccionClienteWrapper implements DireccionCliente,
	ModelWrapper<DireccionCliente> {
	public DireccionClienteWrapper(DireccionCliente direccionCliente) {
		_direccionCliente = direccionCliente;
	}

	@Override
	public Class<?> getModelClass() {
		return DireccionCliente.class;
	}

	@Override
	public String getModelClassName() {
		return DireccionCliente.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDireccion", getIdDireccion());
		attributes.put("idDatoCliente", getIdDatoCliente());
		attributes.put("tipoDireccion", getTipoDireccion());
		attributes.put("codigoUso", getCodigoUso());
		attributes.put("flagDireccionEstandar", getFlagDireccionEstandar());
		attributes.put("via", getVia());
		attributes.put("departamento", getDepartamento());
		attributes.put("provincia", getProvincia());
		attributes.put("distrito", getDistrito());
		attributes.put("pais", getPais());
		attributes.put("ubigeo", getUbigeo());
		attributes.put("codigoPostal", getCodigoPostal());
		attributes.put("estado", getEstado());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idDireccion = (Long)attributes.get("idDireccion");

		if (idDireccion != null) {
			setIdDireccion(idDireccion);
		}

		Long idDatoCliente = (Long)attributes.get("idDatoCliente");

		if (idDatoCliente != null) {
			setIdDatoCliente(idDatoCliente);
		}

		String tipoDireccion = (String)attributes.get("tipoDireccion");

		if (tipoDireccion != null) {
			setTipoDireccion(tipoDireccion);
		}

		String codigoUso = (String)attributes.get("codigoUso");

		if (codigoUso != null) {
			setCodigoUso(codigoUso);
		}

		String flagDireccionEstandar = (String)attributes.get(
				"flagDireccionEstandar");

		if (flagDireccionEstandar != null) {
			setFlagDireccionEstandar(flagDireccionEstandar);
		}

		String via = (String)attributes.get("via");

		if (via != null) {
			setVia(via);
		}

		String departamento = (String)attributes.get("departamento");

		if (departamento != null) {
			setDepartamento(departamento);
		}

		String provincia = (String)attributes.get("provincia");

		if (provincia != null) {
			setProvincia(provincia);
		}

		String distrito = (String)attributes.get("distrito");

		if (distrito != null) {
			setDistrito(distrito);
		}

		String pais = (String)attributes.get("pais");

		if (pais != null) {
			setPais(pais);
		}

		String ubigeo = (String)attributes.get("ubigeo");

		if (ubigeo != null) {
			setUbigeo(ubigeo);
		}

		String codigoPostal = (String)attributes.get("codigoPostal");

		if (codigoPostal != null) {
			setCodigoPostal(codigoPostal);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this direccion cliente.
	*
	* @return the primary key of this direccion cliente
	*/
	@Override
	public long getPrimaryKey() {
		return _direccionCliente.getPrimaryKey();
	}

	/**
	* Sets the primary key of this direccion cliente.
	*
	* @param primaryKey the primary key of this direccion cliente
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_direccionCliente.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id direccion of this direccion cliente.
	*
	* @return the id direccion of this direccion cliente
	*/
	@Override
	public long getIdDireccion() {
		return _direccionCliente.getIdDireccion();
	}

	/**
	* Sets the id direccion of this direccion cliente.
	*
	* @param idDireccion the id direccion of this direccion cliente
	*/
	@Override
	public void setIdDireccion(long idDireccion) {
		_direccionCliente.setIdDireccion(idDireccion);
	}

	/**
	* Returns the id dato cliente of this direccion cliente.
	*
	* @return the id dato cliente of this direccion cliente
	*/
	@Override
	public long getIdDatoCliente() {
		return _direccionCliente.getIdDatoCliente();
	}

	/**
	* Sets the id dato cliente of this direccion cliente.
	*
	* @param idDatoCliente the id dato cliente of this direccion cliente
	*/
	@Override
	public void setIdDatoCliente(long idDatoCliente) {
		_direccionCliente.setIdDatoCliente(idDatoCliente);
	}

	/**
	* Returns the tipo direccion of this direccion cliente.
	*
	* @return the tipo direccion of this direccion cliente
	*/
	@Override
	public java.lang.String getTipoDireccion() {
		return _direccionCliente.getTipoDireccion();
	}

	/**
	* Sets the tipo direccion of this direccion cliente.
	*
	* @param tipoDireccion the tipo direccion of this direccion cliente
	*/
	@Override
	public void setTipoDireccion(java.lang.String tipoDireccion) {
		_direccionCliente.setTipoDireccion(tipoDireccion);
	}

	/**
	* Returns the codigo uso of this direccion cliente.
	*
	* @return the codigo uso of this direccion cliente
	*/
	@Override
	public java.lang.String getCodigoUso() {
		return _direccionCliente.getCodigoUso();
	}

	/**
	* Sets the codigo uso of this direccion cliente.
	*
	* @param codigoUso the codigo uso of this direccion cliente
	*/
	@Override
	public void setCodigoUso(java.lang.String codigoUso) {
		_direccionCliente.setCodigoUso(codigoUso);
	}

	/**
	* Returns the flag direccion estandar of this direccion cliente.
	*
	* @return the flag direccion estandar of this direccion cliente
	*/
	@Override
	public java.lang.String getFlagDireccionEstandar() {
		return _direccionCliente.getFlagDireccionEstandar();
	}

	/**
	* Sets the flag direccion estandar of this direccion cliente.
	*
	* @param flagDireccionEstandar the flag direccion estandar of this direccion cliente
	*/
	@Override
	public void setFlagDireccionEstandar(java.lang.String flagDireccionEstandar) {
		_direccionCliente.setFlagDireccionEstandar(flagDireccionEstandar);
	}

	/**
	* Returns the via of this direccion cliente.
	*
	* @return the via of this direccion cliente
	*/
	@Override
	public java.lang.String getVia() {
		return _direccionCliente.getVia();
	}

	/**
	* Sets the via of this direccion cliente.
	*
	* @param via the via of this direccion cliente
	*/
	@Override
	public void setVia(java.lang.String via) {
		_direccionCliente.setVia(via);
	}

	/**
	* Returns the departamento of this direccion cliente.
	*
	* @return the departamento of this direccion cliente
	*/
	@Override
	public java.lang.String getDepartamento() {
		return _direccionCliente.getDepartamento();
	}

	/**
	* Sets the departamento of this direccion cliente.
	*
	* @param departamento the departamento of this direccion cliente
	*/
	@Override
	public void setDepartamento(java.lang.String departamento) {
		_direccionCliente.setDepartamento(departamento);
	}

	/**
	* Returns the provincia of this direccion cliente.
	*
	* @return the provincia of this direccion cliente
	*/
	@Override
	public java.lang.String getProvincia() {
		return _direccionCliente.getProvincia();
	}

	/**
	* Sets the provincia of this direccion cliente.
	*
	* @param provincia the provincia of this direccion cliente
	*/
	@Override
	public void setProvincia(java.lang.String provincia) {
		_direccionCliente.setProvincia(provincia);
	}

	/**
	* Returns the distrito of this direccion cliente.
	*
	* @return the distrito of this direccion cliente
	*/
	@Override
	public java.lang.String getDistrito() {
		return _direccionCliente.getDistrito();
	}

	/**
	* Sets the distrito of this direccion cliente.
	*
	* @param distrito the distrito of this direccion cliente
	*/
	@Override
	public void setDistrito(java.lang.String distrito) {
		_direccionCliente.setDistrito(distrito);
	}

	/**
	* Returns the pais of this direccion cliente.
	*
	* @return the pais of this direccion cliente
	*/
	@Override
	public java.lang.String getPais() {
		return _direccionCliente.getPais();
	}

	/**
	* Sets the pais of this direccion cliente.
	*
	* @param pais the pais of this direccion cliente
	*/
	@Override
	public void setPais(java.lang.String pais) {
		_direccionCliente.setPais(pais);
	}

	/**
	* Returns the ubigeo of this direccion cliente.
	*
	* @return the ubigeo of this direccion cliente
	*/
	@Override
	public java.lang.String getUbigeo() {
		return _direccionCliente.getUbigeo();
	}

	/**
	* Sets the ubigeo of this direccion cliente.
	*
	* @param ubigeo the ubigeo of this direccion cliente
	*/
	@Override
	public void setUbigeo(java.lang.String ubigeo) {
		_direccionCliente.setUbigeo(ubigeo);
	}

	/**
	* Returns the codigo postal of this direccion cliente.
	*
	* @return the codigo postal of this direccion cliente
	*/
	@Override
	public java.lang.String getCodigoPostal() {
		return _direccionCliente.getCodigoPostal();
	}

	/**
	* Sets the codigo postal of this direccion cliente.
	*
	* @param codigoPostal the codigo postal of this direccion cliente
	*/
	@Override
	public void setCodigoPostal(java.lang.String codigoPostal) {
		_direccionCliente.setCodigoPostal(codigoPostal);
	}

	/**
	* Returns the estado of this direccion cliente.
	*
	* @return the estado of this direccion cliente
	*/
	@Override
	public java.lang.String getEstado() {
		return _direccionCliente.getEstado();
	}

	/**
	* Sets the estado of this direccion cliente.
	*
	* @param estado the estado of this direccion cliente
	*/
	@Override
	public void setEstado(java.lang.String estado) {
		_direccionCliente.setEstado(estado);
	}

	/**
	* Returns the fecha registro of this direccion cliente.
	*
	* @return the fecha registro of this direccion cliente
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _direccionCliente.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this direccion cliente.
	*
	* @param fechaRegistro the fecha registro of this direccion cliente
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_direccionCliente.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _direccionCliente.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_direccionCliente.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _direccionCliente.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_direccionCliente.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _direccionCliente.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _direccionCliente.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_direccionCliente.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _direccionCliente.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_direccionCliente.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_direccionCliente.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_direccionCliente.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DireccionClienteWrapper((DireccionCliente)_direccionCliente.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.DireccionCliente direccionCliente) {
		return _direccionCliente.compareTo(direccionCliente);
	}

	@Override
	public int hashCode() {
		return _direccionCliente.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.DireccionCliente> toCacheModel() {
		return _direccionCliente.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.DireccionCliente toEscapedModel() {
		return new DireccionClienteWrapper(_direccionCliente.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.DireccionCliente toUnescapedModel() {
		return new DireccionClienteWrapper(_direccionCliente.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _direccionCliente.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _direccionCliente.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_direccionCliente.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DireccionClienteWrapper)) {
			return false;
		}

		DireccionClienteWrapper direccionClienteWrapper = (DireccionClienteWrapper)obj;

		if (Validator.equals(_direccionCliente,
					direccionClienteWrapper._direccionCliente)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public DireccionCliente getWrappedDireccionCliente() {
		return _direccionCliente;
	}

	@Override
	public DireccionCliente getWrappedModel() {
		return _direccionCliente;
	}

	@Override
	public void resetOriginalValues() {
		_direccionCliente.resetOriginalValues();
	}

	private DireccionCliente _direccionCliente;
}