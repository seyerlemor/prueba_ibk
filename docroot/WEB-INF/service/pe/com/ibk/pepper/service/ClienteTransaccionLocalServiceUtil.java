/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ClienteTransaccion. This utility wraps
 * {@link pe.com.ibk.pepper.service.impl.ClienteTransaccionLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Interbank
 * @see ClienteTransaccionLocalService
 * @see pe.com.ibk.pepper.service.base.ClienteTransaccionLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.impl.ClienteTransaccionLocalServiceImpl
 * @generated
 */
public class ClienteTransaccionLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link pe.com.ibk.pepper.service.impl.ClienteTransaccionLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the cliente transaccion to the database. Also notifies the appropriate model listeners.
	*
	* @param clienteTransaccion the cliente transaccion
	* @return the cliente transaccion that was added
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion addClienteTransaccion(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addClienteTransaccion(clienteTransaccion);
	}

	/**
	* Creates a new cliente transaccion with the primary key. Does not add the cliente transaccion to the database.
	*
	* @param idClienteTransaccion the primary key for the new cliente transaccion
	* @return the new cliente transaccion
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion createClienteTransaccion(
		long idClienteTransaccion) {
		return getService().createClienteTransaccion(idClienteTransaccion);
	}

	/**
	* Deletes the cliente transaccion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idClienteTransaccion the primary key of the cliente transaccion
	* @return the cliente transaccion that was removed
	* @throws PortalException if a cliente transaccion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion deleteClienteTransaccion(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteClienteTransaccion(idClienteTransaccion);
	}

	/**
	* Deletes the cliente transaccion from the database. Also notifies the appropriate model listeners.
	*
	* @param clienteTransaccion the cliente transaccion
	* @return the cliente transaccion that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion deleteClienteTransaccion(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteClienteTransaccion(clienteTransaccion);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static pe.com.ibk.pepper.model.ClienteTransaccion fetchClienteTransaccion(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchClienteTransaccion(idClienteTransaccion);
	}

	/**
	* Returns the cliente transaccion with the primary key.
	*
	* @param idClienteTransaccion the primary key of the cliente transaccion
	* @return the cliente transaccion
	* @throws PortalException if a cliente transaccion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion getClienteTransaccion(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getClienteTransaccion(idClienteTransaccion);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the cliente transaccions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of cliente transaccions
	* @param end the upper bound of the range of cliente transaccions (not inclusive)
	* @return the range of cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> getClienteTransaccions(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getClienteTransaccions(start, end);
	}

	/**
	* Returns the number of cliente transaccions.
	*
	* @return the number of cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static int getClienteTransaccionsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getClienteTransaccionsCount();
	}

	/**
	* Updates the cliente transaccion in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clienteTransaccion the cliente transaccion
	* @return the cliente transaccion that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion updateClienteTransaccion(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateClienteTransaccion(clienteTransaccion);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static pe.com.ibk.pepper.model.ClienteTransaccion registrarClienteTransaccion(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().registrarClienteTransaccion(clienteTransaccion);
	}

	public static pe.com.ibk.pepper.model.ClienteTransaccion buscarClienteTransaccionporId(
		long idClienteTransaccion) {
		return getService().buscarClienteTransaccionporId(idClienteTransaccion);
	}

	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> getClienteTransaccionesByExpediente(
		long idExpediente, java.lang.String tipoFlujo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getClienteTransaccionesByExpediente(idExpediente, tipoFlujo);
	}

	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> obtenerTransacciones(
		long idExpediente, java.lang.String tipoFlujo) {
		return getService().obtenerTransacciones(idExpediente, tipoFlujo);
	}

	public static void clearService() {
		_service = null;
	}

	public static ClienteTransaccionLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ClienteTransaccionLocalService.class.getName());

			if (invokableLocalService instanceof ClienteTransaccionLocalService) {
				_service = (ClienteTransaccionLocalService)invokableLocalService;
			}
			else {
				_service = new ClienteTransaccionLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ClienteTransaccionLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ClienteTransaccionLocalService service) {
	}

	private static ClienteTransaccionLocalService _service;
}