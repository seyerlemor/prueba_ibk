/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.UsuarioSession;

import java.util.List;

/**
 * The persistence utility for the usuario session service. This utility wraps {@link UsuarioSessionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see UsuarioSessionPersistence
 * @see UsuarioSessionPersistenceImpl
 * @generated
 */
public class UsuarioSessionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(UsuarioSession usuarioSession) {
		getPersistence().clearCache(usuarioSession);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UsuarioSession> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UsuarioSession> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UsuarioSession> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static UsuarioSession update(UsuarioSession usuarioSession)
		throws SystemException {
		return getPersistence().update(usuarioSession);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static UsuarioSession update(UsuarioSession usuarioSession,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(usuarioSession, serviceContext);
	}

	/**
	* Returns the usuario session where idUsuarioSession = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUsuarioSessionException} if it could not be found.
	*
	* @param idUsuarioSession the id usuario session
	* @return the matching usuario session
	* @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a matching usuario session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession findByUS_ID(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUsuarioSessionException {
		return getPersistence().findByUS_ID(idUsuarioSession);
	}

	/**
	* Returns the usuario session where idUsuarioSession = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idUsuarioSession the id usuario session
	* @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession fetchByUS_ID(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUS_ID(idUsuarioSession);
	}

	/**
	* Returns the usuario session where idUsuarioSession = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idUsuarioSession the id usuario session
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession fetchByUS_ID(
		long idUsuarioSession, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUS_ID(idUsuarioSession, retrieveFromCache);
	}

	/**
	* Removes the usuario session where idUsuarioSession = &#63; from the database.
	*
	* @param idUsuarioSession the id usuario session
	* @return the usuario session that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession removeByUS_ID(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUsuarioSessionException {
		return getPersistence().removeByUS_ID(idUsuarioSession);
	}

	/**
	* Returns the number of usuario sessions where idUsuarioSession = &#63;.
	*
	* @param idUsuarioSession the id usuario session
	* @return the number of matching usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUS_ID(long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUS_ID(idUsuarioSession);
	}

	/**
	* Returns the usuario session where idSession = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUsuarioSessionException} if it could not be found.
	*
	* @param idSession the id session
	* @return the matching usuario session
	* @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a matching usuario session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession findByU_S_S(
		java.lang.String idSession)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUsuarioSessionException {
		return getPersistence().findByU_S_S(idSession);
	}

	/**
	* Returns the usuario session where idSession = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idSession the id session
	* @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession fetchByU_S_S(
		java.lang.String idSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByU_S_S(idSession);
	}

	/**
	* Returns the usuario session where idSession = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idSession the id session
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession fetchByU_S_S(
		java.lang.String idSession, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByU_S_S(idSession, retrieveFromCache);
	}

	/**
	* Removes the usuario session where idSession = &#63; from the database.
	*
	* @param idSession the id session
	* @return the usuario session that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession removeByU_S_S(
		java.lang.String idSession)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUsuarioSessionException {
		return getPersistence().removeByU_S_S(idSession);
	}

	/**
	* Returns the number of usuario sessions where idSession = &#63;.
	*
	* @param idSession the id session
	* @return the number of matching usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByU_S_S(java.lang.String idSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByU_S_S(idSession);
	}

	/**
	* Caches the usuario session in the entity cache if it is enabled.
	*
	* @param usuarioSession the usuario session
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession) {
		getPersistence().cacheResult(usuarioSession);
	}

	/**
	* Caches the usuario sessions in the entity cache if it is enabled.
	*
	* @param usuarioSessions the usuario sessions
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.UsuarioSession> usuarioSessions) {
		getPersistence().cacheResult(usuarioSessions);
	}

	/**
	* Creates a new usuario session with the primary key. Does not add the usuario session to the database.
	*
	* @param idUsuarioSession the primary key for the new usuario session
	* @return the new usuario session
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession create(
		long idUsuarioSession) {
		return getPersistence().create(idUsuarioSession);
	}

	/**
	* Removes the usuario session with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idUsuarioSession the primary key of the usuario session
	* @return the usuario session that was removed
	* @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a usuario session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession remove(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUsuarioSessionException {
		return getPersistence().remove(idUsuarioSession);
	}

	public static pe.com.ibk.pepper.model.UsuarioSession updateImpl(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(usuarioSession);
	}

	/**
	* Returns the usuario session with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchUsuarioSessionException} if it could not be found.
	*
	* @param idUsuarioSession the primary key of the usuario session
	* @return the usuario session
	* @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a usuario session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession findByPrimaryKey(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUsuarioSessionException {
		return getPersistence().findByPrimaryKey(idUsuarioSession);
	}

	/**
	* Returns the usuario session with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idUsuarioSession the primary key of the usuario session
	* @return the usuario session, or <code>null</code> if a usuario session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.UsuarioSession fetchByPrimaryKey(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idUsuarioSession);
	}

	/**
	* Returns all the usuario sessions.
	*
	* @return the usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.UsuarioSession> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the usuario sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of usuario sessions
	* @param end the upper bound of the range of usuario sessions (not inclusive)
	* @return the range of usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.UsuarioSession> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the usuario sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of usuario sessions
	* @param end the upper bound of the range of usuario sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.UsuarioSession> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the usuario sessions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of usuario sessions.
	*
	* @return the number of usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static UsuarioSessionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (UsuarioSessionPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					UsuarioSessionPersistence.class.getName());

			ReferenceRegistry.registerReference(UsuarioSessionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(UsuarioSessionPersistence persistence) {
	}

	private static UsuarioSessionPersistence _persistence;
}