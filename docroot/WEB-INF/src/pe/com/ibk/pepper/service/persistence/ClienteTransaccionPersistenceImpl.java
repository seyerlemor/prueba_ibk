/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchClienteTransaccionException;
import pe.com.ibk.pepper.model.ClienteTransaccion;
import pe.com.ibk.pepper.model.impl.ClienteTransaccionImpl;
import pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the cliente transaccion service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ClienteTransaccionPersistence
 * @see ClienteTransaccionUtil
 * @generated
 */
public class ClienteTransaccionPersistenceImpl extends BasePersistenceImpl<ClienteTransaccion>
	implements ClienteTransaccionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ClienteTransaccionUtil} to access the cliente transaccion persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ClienteTransaccionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED,
			ClienteTransaccionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED,
			ClienteTransaccionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_CT_ID = new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED,
			ClienteTransaccionImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCT_ID", new String[] { Long.class.getName() },
			ClienteTransaccionModelImpl.IDCLIENTETRANSACCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CT_ID = new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCT_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the cliente transaccion where idClienteTransaccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteTransaccionException} if it could not be found.
	 *
	 * @param idClienteTransaccion the id cliente transaccion
	 * @return the matching cliente transaccion
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion findByCT_ID(long idClienteTransaccion)
		throws NoSuchClienteTransaccionException, SystemException {
		ClienteTransaccion clienteTransaccion = fetchByCT_ID(idClienteTransaccion);

		if (clienteTransaccion == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idClienteTransaccion=");
			msg.append(idClienteTransaccion);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchClienteTransaccionException(msg.toString());
		}

		return clienteTransaccion;
	}

	/**
	 * Returns the cliente transaccion where idClienteTransaccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idClienteTransaccion the id cliente transaccion
	 * @return the matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion fetchByCT_ID(long idClienteTransaccion)
		throws SystemException {
		return fetchByCT_ID(idClienteTransaccion, true);
	}

	/**
	 * Returns the cliente transaccion where idClienteTransaccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idClienteTransaccion the id cliente transaccion
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion fetchByCT_ID(long idClienteTransaccion,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idClienteTransaccion };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CT_ID,
					finderArgs, this);
		}

		if (result instanceof ClienteTransaccion) {
			ClienteTransaccion clienteTransaccion = (ClienteTransaccion)result;

			if ((idClienteTransaccion != clienteTransaccion.getIdClienteTransaccion())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CLIENTETRANSACCION_WHERE);

			query.append(_FINDER_COLUMN_CT_ID_IDCLIENTETRANSACCION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idClienteTransaccion);

				List<ClienteTransaccion> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CT_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ClienteTransaccionPersistenceImpl.fetchByCT_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ClienteTransaccion clienteTransaccion = list.get(0);

					result = clienteTransaccion;

					cacheResult(clienteTransaccion);

					if ((clienteTransaccion.getIdClienteTransaccion() != idClienteTransaccion)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CT_ID,
							finderArgs, clienteTransaccion);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CT_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ClienteTransaccion)result;
		}
	}

	/**
	 * Removes the cliente transaccion where idClienteTransaccion = &#63; from the database.
	 *
	 * @param idClienteTransaccion the id cliente transaccion
	 * @return the cliente transaccion that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion removeByCT_ID(long idClienteTransaccion)
		throws NoSuchClienteTransaccionException, SystemException {
		ClienteTransaccion clienteTransaccion = findByCT_ID(idClienteTransaccion);

		return remove(clienteTransaccion);
	}

	/**
	 * Returns the number of cliente transaccions where idClienteTransaccion = &#63;.
	 *
	 * @param idClienteTransaccion the id cliente transaccion
	 * @return the number of matching cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCT_ID(long idClienteTransaccion)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CT_ID;

		Object[] finderArgs = new Object[] { idClienteTransaccion };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLIENTETRANSACCION_WHERE);

			query.append(_FINDER_COLUMN_CT_ID_IDCLIENTETRANSACCION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idClienteTransaccion);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CT_ID_IDCLIENTETRANSACCION_2 = "clienteTransaccion.idClienteTransaccion = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEXPEDIENTE =
		new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED,
			ClienteTransaccionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdExpediente",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE =
		new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED,
			ClienteTransaccionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdExpediente",
			new String[] { Long.class.getName(), String.class.getName() },
			ClienteTransaccionModelImpl.IDEXPEDIENTE_COLUMN_BITMASK |
			ClienteTransaccionModelImpl.TIPOFLUJO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEXPEDIENTE = new FinderPath(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdExpediente",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @return the matching cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ClienteTransaccion> findByIdExpediente(long idExpediente,
		String tipoFlujo) throws SystemException {
		return findByIdExpediente(idExpediente, tipoFlujo, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param start the lower bound of the range of cliente transaccions
	 * @param end the upper bound of the range of cliente transaccions (not inclusive)
	 * @return the range of matching cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ClienteTransaccion> findByIdExpediente(long idExpediente,
		String tipoFlujo, int start, int end) throws SystemException {
		return findByIdExpediente(idExpediente, tipoFlujo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param start the lower bound of the range of cliente transaccions
	 * @param end the upper bound of the range of cliente transaccions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ClienteTransaccion> findByIdExpediente(long idExpediente,
		String tipoFlujo, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE;
			finderArgs = new Object[] { idExpediente, tipoFlujo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEXPEDIENTE;
			finderArgs = new Object[] {
					idExpediente, tipoFlujo,
					
					start, end, orderByComparator
				};
		}

		List<ClienteTransaccion> list = (List<ClienteTransaccion>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClienteTransaccion clienteTransaccion : list) {
				if ((idExpediente != clienteTransaccion.getIdExpediente()) ||
						!Validator.equals(tipoFlujo,
							clienteTransaccion.getTipoFlujo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLIENTETRANSACCION_WHERE);

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

			boolean bindTipoFlujo = false;

			if (tipoFlujo == null) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_1);
			}
			else if (tipoFlujo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_3);
			}
			else {
				bindTipoFlujo = true;

				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ClienteTransaccionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				if (bindTipoFlujo) {
					qPos.add(tipoFlujo);
				}

				if (!pagination) {
					list = (List<ClienteTransaccion>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ClienteTransaccion>(list);
				}
				else {
					list = (List<ClienteTransaccion>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cliente transaccion
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion findByIdExpediente_First(long idExpediente,
		String tipoFlujo, OrderByComparator orderByComparator)
		throws NoSuchClienteTransaccionException, SystemException {
		ClienteTransaccion clienteTransaccion = fetchByIdExpediente_First(idExpediente,
				tipoFlujo, orderByComparator);

		if (clienteTransaccion != null) {
			return clienteTransaccion;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idExpediente=");
		msg.append(idExpediente);

		msg.append(", tipoFlujo=");
		msg.append(tipoFlujo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClienteTransaccionException(msg.toString());
	}

	/**
	 * Returns the first cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion fetchByIdExpediente_First(long idExpediente,
		String tipoFlujo, OrderByComparator orderByComparator)
		throws SystemException {
		List<ClienteTransaccion> list = findByIdExpediente(idExpediente,
				tipoFlujo, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cliente transaccion
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion findByIdExpediente_Last(long idExpediente,
		String tipoFlujo, OrderByComparator orderByComparator)
		throws NoSuchClienteTransaccionException, SystemException {
		ClienteTransaccion clienteTransaccion = fetchByIdExpediente_Last(idExpediente,
				tipoFlujo, orderByComparator);

		if (clienteTransaccion != null) {
			return clienteTransaccion;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idExpediente=");
		msg.append(idExpediente);

		msg.append(", tipoFlujo=");
		msg.append(tipoFlujo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClienteTransaccionException(msg.toString());
	}

	/**
	 * Returns the last cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion fetchByIdExpediente_Last(long idExpediente,
		String tipoFlujo, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByIdExpediente(idExpediente, tipoFlujo);

		if (count == 0) {
			return null;
		}

		List<ClienteTransaccion> list = findByIdExpediente(idExpediente,
				tipoFlujo, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the cliente transaccions before and after the current cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idClienteTransaccion the primary key of the current cliente transaccion
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next cliente transaccion
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion[] findByIdExpediente_PrevAndNext(
		long idClienteTransaccion, long idExpediente, String tipoFlujo,
		OrderByComparator orderByComparator)
		throws NoSuchClienteTransaccionException, SystemException {
		ClienteTransaccion clienteTransaccion = findByPrimaryKey(idClienteTransaccion);

		Session session = null;

		try {
			session = openSession();

			ClienteTransaccion[] array = new ClienteTransaccionImpl[3];

			array[0] = getByIdExpediente_PrevAndNext(session,
					clienteTransaccion, idExpediente, tipoFlujo,
					orderByComparator, true);

			array[1] = clienteTransaccion;

			array[2] = getByIdExpediente_PrevAndNext(session,
					clienteTransaccion, idExpediente, tipoFlujo,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClienteTransaccion getByIdExpediente_PrevAndNext(
		Session session, ClienteTransaccion clienteTransaccion,
		long idExpediente, String tipoFlujo,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLIENTETRANSACCION_WHERE);

		query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

		boolean bindTipoFlujo = false;

		if (tipoFlujo == null) {
			query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_1);
		}
		else if (tipoFlujo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_3);
		}
		else {
			bindTipoFlujo = true;

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ClienteTransaccionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(idExpediente);

		if (bindTipoFlujo) {
			qPos.add(tipoFlujo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clienteTransaccion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClienteTransaccion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63; from the database.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdExpediente(long idExpediente, String tipoFlujo)
		throws SystemException {
		for (ClienteTransaccion clienteTransaccion : findByIdExpediente(
				idExpediente, tipoFlujo, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(clienteTransaccion);
		}
	}

	/**
	 * Returns the number of cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoFlujo the tipo flujo
	 * @return the number of matching cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdExpediente(long idExpediente, String tipoFlujo)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEXPEDIENTE;

		Object[] finderArgs = new Object[] { idExpediente, tipoFlujo };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLIENTETRANSACCION_WHERE);

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

			boolean bindTipoFlujo = false;

			if (tipoFlujo == null) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_1);
			}
			else if (tipoFlujo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_3);
			}
			else {
				bindTipoFlujo = true;

				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				if (bindTipoFlujo) {
					qPos.add(tipoFlujo);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2 = "clienteTransaccion.idExpediente = ? AND ";
	private static final String _FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_1 = "clienteTransaccion.tipoFlujo IS NULL";
	private static final String _FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_2 = "clienteTransaccion.tipoFlujo = ?";
	private static final String _FINDER_COLUMN_IDEXPEDIENTE_TIPOFLUJO_3 = "(clienteTransaccion.tipoFlujo IS NULL OR clienteTransaccion.tipoFlujo = '')";

	public ClienteTransaccionPersistenceImpl() {
		setModelClass(ClienteTransaccion.class);
	}

	/**
	 * Caches the cliente transaccion in the entity cache if it is enabled.
	 *
	 * @param clienteTransaccion the cliente transaccion
	 */
	@Override
	public void cacheResult(ClienteTransaccion clienteTransaccion) {
		EntityCacheUtil.putResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionImpl.class, clienteTransaccion.getPrimaryKey(),
			clienteTransaccion);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CT_ID,
			new Object[] { clienteTransaccion.getIdClienteTransaccion() },
			clienteTransaccion);

		clienteTransaccion.resetOriginalValues();
	}

	/**
	 * Caches the cliente transaccions in the entity cache if it is enabled.
	 *
	 * @param clienteTransaccions the cliente transaccions
	 */
	@Override
	public void cacheResult(List<ClienteTransaccion> clienteTransaccions) {
		for (ClienteTransaccion clienteTransaccion : clienteTransaccions) {
			if (EntityCacheUtil.getResult(
						ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
						ClienteTransaccionImpl.class,
						clienteTransaccion.getPrimaryKey()) == null) {
				cacheResult(clienteTransaccion);
			}
			else {
				clienteTransaccion.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all cliente transaccions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ClienteTransaccionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ClienteTransaccionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the cliente transaccion.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ClienteTransaccion clienteTransaccion) {
		EntityCacheUtil.removeResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionImpl.class, clienteTransaccion.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(clienteTransaccion);
	}

	@Override
	public void clearCache(List<ClienteTransaccion> clienteTransaccions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ClienteTransaccion clienteTransaccion : clienteTransaccions) {
			EntityCacheUtil.removeResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
				ClienteTransaccionImpl.class, clienteTransaccion.getPrimaryKey());

			clearUniqueFindersCache(clienteTransaccion);
		}
	}

	protected void cacheUniqueFindersCache(
		ClienteTransaccion clienteTransaccion) {
		if (clienteTransaccion.isNew()) {
			Object[] args = new Object[] {
					clienteTransaccion.getIdClienteTransaccion()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CT_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CT_ID, args,
				clienteTransaccion);
		}
		else {
			ClienteTransaccionModelImpl clienteTransaccionModelImpl = (ClienteTransaccionModelImpl)clienteTransaccion;

			if ((clienteTransaccionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CT_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clienteTransaccion.getIdClienteTransaccion()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CT_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CT_ID, args,
					clienteTransaccion);
			}
		}
	}

	protected void clearUniqueFindersCache(
		ClienteTransaccion clienteTransaccion) {
		ClienteTransaccionModelImpl clienteTransaccionModelImpl = (ClienteTransaccionModelImpl)clienteTransaccion;

		Object[] args = new Object[] {
				clienteTransaccion.getIdClienteTransaccion()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CT_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CT_ID, args);

		if ((clienteTransaccionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CT_ID.getColumnBitmask()) != 0) {
			args = new Object[] {
					clienteTransaccionModelImpl.getOriginalIdClienteTransaccion()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CT_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CT_ID, args);
		}
	}

	/**
	 * Creates a new cliente transaccion with the primary key. Does not add the cliente transaccion to the database.
	 *
	 * @param idClienteTransaccion the primary key for the new cliente transaccion
	 * @return the new cliente transaccion
	 */
	@Override
	public ClienteTransaccion create(long idClienteTransaccion) {
		ClienteTransaccion clienteTransaccion = new ClienteTransaccionImpl();

		clienteTransaccion.setNew(true);
		clienteTransaccion.setPrimaryKey(idClienteTransaccion);

		return clienteTransaccion;
	}

	/**
	 * Removes the cliente transaccion with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idClienteTransaccion the primary key of the cliente transaccion
	 * @return the cliente transaccion that was removed
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion remove(long idClienteTransaccion)
		throws NoSuchClienteTransaccionException, SystemException {
		return remove((Serializable)idClienteTransaccion);
	}

	/**
	 * Removes the cliente transaccion with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the cliente transaccion
	 * @return the cliente transaccion that was removed
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion remove(Serializable primaryKey)
		throws NoSuchClienteTransaccionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ClienteTransaccion clienteTransaccion = (ClienteTransaccion)session.get(ClienteTransaccionImpl.class,
					primaryKey);

			if (clienteTransaccion == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchClienteTransaccionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clienteTransaccion);
		}
		catch (NoSuchClienteTransaccionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ClienteTransaccion removeImpl(
		ClienteTransaccion clienteTransaccion) throws SystemException {
		clienteTransaccion = toUnwrappedModel(clienteTransaccion);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clienteTransaccion)) {
				clienteTransaccion = (ClienteTransaccion)session.get(ClienteTransaccionImpl.class,
						clienteTransaccion.getPrimaryKeyObj());
			}

			if (clienteTransaccion != null) {
				session.delete(clienteTransaccion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clienteTransaccion != null) {
			clearCache(clienteTransaccion);
		}

		return clienteTransaccion;
	}

	@Override
	public ClienteTransaccion updateImpl(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion)
		throws SystemException {
		clienteTransaccion = toUnwrappedModel(clienteTransaccion);

		boolean isNew = clienteTransaccion.isNew();

		ClienteTransaccionModelImpl clienteTransaccionModelImpl = (ClienteTransaccionModelImpl)clienteTransaccion;

		Session session = null;

		try {
			session = openSession();

			if (clienteTransaccion.isNew()) {
				session.save(clienteTransaccion);

				clienteTransaccion.setNew(false);
			}
			else {
				session.merge(clienteTransaccion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ClienteTransaccionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clienteTransaccionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clienteTransaccionModelImpl.getOriginalIdExpediente(),
						clienteTransaccionModelImpl.getOriginalTipoFlujo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEXPEDIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE,
					args);

				args = new Object[] {
						clienteTransaccionModelImpl.getIdExpediente(),
						clienteTransaccionModelImpl.getTipoFlujo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEXPEDIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE,
					args);
			}
		}

		EntityCacheUtil.putResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
			ClienteTransaccionImpl.class, clienteTransaccion.getPrimaryKey(),
			clienteTransaccion);

		clearUniqueFindersCache(clienteTransaccion);
		cacheUniqueFindersCache(clienteTransaccion);

		return clienteTransaccion;
	}

	protected ClienteTransaccion toUnwrappedModel(
		ClienteTransaccion clienteTransaccion) {
		if (clienteTransaccion instanceof ClienteTransaccionImpl) {
			return clienteTransaccion;
		}

		ClienteTransaccionImpl clienteTransaccionImpl = new ClienteTransaccionImpl();

		clienteTransaccionImpl.setNew(clienteTransaccion.isNew());
		clienteTransaccionImpl.setPrimaryKey(clienteTransaccion.getPrimaryKey());

		clienteTransaccionImpl.setIdClienteTransaccion(clienteTransaccion.getIdClienteTransaccion());
		clienteTransaccionImpl.setIdUsuarioSession(clienteTransaccion.getIdUsuarioSession());
		clienteTransaccionImpl.setIdExpediente(clienteTransaccion.getIdExpediente());
		clienteTransaccionImpl.setEstado(clienteTransaccion.getEstado());
		clienteTransaccionImpl.setPaso(clienteTransaccion.getPaso());
		clienteTransaccionImpl.setPagina(clienteTransaccion.getPagina());
		clienteTransaccionImpl.setTipoFlujo(clienteTransaccion.getTipoFlujo());
		clienteTransaccionImpl.setStrJson(clienteTransaccion.getStrJson());
		clienteTransaccionImpl.setRestauracion(clienteTransaccion.getRestauracion());
		clienteTransaccionImpl.setFechaRegistro(clienteTransaccion.getFechaRegistro());

		return clienteTransaccionImpl;
	}

	/**
	 * Returns the cliente transaccion with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the cliente transaccion
	 * @return the cliente transaccion
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion findByPrimaryKey(Serializable primaryKey)
		throws NoSuchClienteTransaccionException, SystemException {
		ClienteTransaccion clienteTransaccion = fetchByPrimaryKey(primaryKey);

		if (clienteTransaccion == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchClienteTransaccionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clienteTransaccion;
	}

	/**
	 * Returns the cliente transaccion with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchClienteTransaccionException} if it could not be found.
	 *
	 * @param idClienteTransaccion the primary key of the cliente transaccion
	 * @return the cliente transaccion
	 * @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion findByPrimaryKey(long idClienteTransaccion)
		throws NoSuchClienteTransaccionException, SystemException {
		return findByPrimaryKey((Serializable)idClienteTransaccion);
	}

	/**
	 * Returns the cliente transaccion with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the cliente transaccion
	 * @return the cliente transaccion, or <code>null</code> if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ClienteTransaccion clienteTransaccion = (ClienteTransaccion)EntityCacheUtil.getResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
				ClienteTransaccionImpl.class, primaryKey);

		if (clienteTransaccion == _nullClienteTransaccion) {
			return null;
		}

		if (clienteTransaccion == null) {
			Session session = null;

			try {
				session = openSession();

				clienteTransaccion = (ClienteTransaccion)session.get(ClienteTransaccionImpl.class,
						primaryKey);

				if (clienteTransaccion != null) {
					cacheResult(clienteTransaccion);
				}
				else {
					EntityCacheUtil.putResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
						ClienteTransaccionImpl.class, primaryKey,
						_nullClienteTransaccion);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ClienteTransaccionModelImpl.ENTITY_CACHE_ENABLED,
					ClienteTransaccionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clienteTransaccion;
	}

	/**
	 * Returns the cliente transaccion with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idClienteTransaccion the primary key of the cliente transaccion
	 * @return the cliente transaccion, or <code>null</code> if a cliente transaccion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClienteTransaccion fetchByPrimaryKey(long idClienteTransaccion)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idClienteTransaccion);
	}

	/**
	 * Returns all the cliente transaccions.
	 *
	 * @return the cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ClienteTransaccion> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cliente transaccions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cliente transaccions
	 * @param end the upper bound of the range of cliente transaccions (not inclusive)
	 * @return the range of cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ClienteTransaccion> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the cliente transaccions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cliente transaccions
	 * @param end the upper bound of the range of cliente transaccions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ClienteTransaccion> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ClienteTransaccion> list = (List<ClienteTransaccion>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLIENTETRANSACCION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLIENTETRANSACCION;

				if (pagination) {
					sql = sql.concat(ClienteTransaccionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ClienteTransaccion>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ClienteTransaccion>(list);
				}
				else {
					list = (List<ClienteTransaccion>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the cliente transaccions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ClienteTransaccion clienteTransaccion : findAll()) {
			remove(clienteTransaccion);
		}
	}

	/**
	 * Returns the number of cliente transaccions.
	 *
	 * @return the number of cliente transaccions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLIENTETRANSACCION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the cliente transaccion persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.ClienteTransaccion")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ClienteTransaccion>> listenersList = new ArrayList<ModelListener<ClienteTransaccion>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ClienteTransaccion>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ClienteTransaccionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLIENTETRANSACCION = "SELECT clienteTransaccion FROM ClienteTransaccion clienteTransaccion";
	private static final String _SQL_SELECT_CLIENTETRANSACCION_WHERE = "SELECT clienteTransaccion FROM ClienteTransaccion clienteTransaccion WHERE ";
	private static final String _SQL_COUNT_CLIENTETRANSACCION = "SELECT COUNT(clienteTransaccion) FROM ClienteTransaccion clienteTransaccion";
	private static final String _SQL_COUNT_CLIENTETRANSACCION_WHERE = "SELECT COUNT(clienteTransaccion) FROM ClienteTransaccion clienteTransaccion WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clienteTransaccion.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ClienteTransaccion exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ClienteTransaccion exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ClienteTransaccionPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idClienteTransaccion", "idUsuarioSession", "idExpediente",
				"estado", "paso", "pagina", "tipoFlujo", "strJson",
				"restauracion", "fechaRegistro"
			});
	private static ClienteTransaccion _nullClienteTransaccion = new ClienteTransaccionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ClienteTransaccion> toCacheModel() {
				return _nullClienteTransaccionCacheModel;
			}
		};

	private static CacheModel<ClienteTransaccion> _nullClienteTransaccionCacheModel =
		new CacheModel<ClienteTransaccion>() {
			@Override
			public ClienteTransaccion toEntityModel() {
				return _nullClienteTransaccion;
			}
		};
}