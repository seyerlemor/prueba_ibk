<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="validarProvisional" var="validarProvisional" />

<div class="lh-ff lh-ff__pauta lh-ff__pauta1">
	<div class="lq-container">
		<div class="lh-ff-block lh-ff-block--pauta lh-ff-block--active">
			<div class="lq-row lq-clearfix">
				<style>
.lh-ff__pauta--img::before {
	background: -webkit-linear-gradient(-45deg, #33b768 0%, #2aa85b 100%);
	background: -o-linear-gradient(-45deg, #33b768 0%, #2aa85b 100%);
	background: linear-gradient(135deg, #33b768 0%, #2aa85b 100%);
}
</style>
				<div class="lh-ff__pauta__col1 lh-ff__pauta--img">
					${commerceText.v101001}
				</div>
				<div class="lh-ff__pauta__col2">
					<div class="lh-ff__pauta__col2-after"></div>
					<div class="lh-ff__pauta_content">
						<form:form action="${validarProvisional}" autocomplete="off"
							commandName="expedienteBean" class="lh-ff-form"
							novalidate="novalidate">
							<div class="lh-ff-section">
								<input type="hidden" name="tipoFlujo" id="tipoFlujo" value="30" />
								<h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">
									${renderRequest.preferences.getValue('recExt009','')}	
								</h4>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item lh-ff-group__item--sm">
										<label class="lh-ff__label" for="fct_dniNumber">
											${renderRequest.preferences.getValue('inicioDniNumberText','')}	
										</label>
										<div class="lh-ff-group__field">
											<input class="lh-input" id="fct_dniNumber"
												inputmode="numeric" maxlength="8" name="dniNumber"
												pattern="[0-9]*" type="tel" data-allowed-keys="integer"
												data-validate-required="numeric"
												data-validate-required-message="${renderRequest.preferences.getValue('inicioDniNumberRequiredMessage','')}"
												data-validate-length="8"
												data-validate-length-message="${renderRequest.preferences.getValue('inicioDniNumberLengthMessage','')}">
										</div>
									</div>
								</div>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item lh-ff-group__item--recaptcha">
										<div class="lh-ff-group__field">
											<div class="g-recaptcha" id="fpa_recaptcha"
												data-sitekey="${renderRequest.preferences.getValue('inicioRecaptchaSiteKey','')}"
												data-validate-recaptcha
												data-validate-recaptcha-message="${renderRequest.preferences.getValue('inicioRecaptchaRequiredMessage','')}">
											</div>
										</div>
									</div>
								</div>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item lh-ff-group__item--checkbox">
										<div class="lh-ff-group__field">
											<div class="lh-form__checkbox lh-form__checkbox--light">
												<input id="fct_checkProteccionDatos" type="checkbox" name="checkboxProteccionDatos" checked> 
												${commerceText.v101003}
											</div>
										</div>
									</div>
								</div>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item lh-ff-group__item--buttons">
										<button
											class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit">
											<span>${commerceText.v101005}</span>
										</button>
									</div>
								</div>
							</div>
							<div class="lh-form__error">
								<div class="lh-typo__p2 lh-form__error-message"
									data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
								<div class="lh-form__error-after"></div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			${commerceText.v101006}
		</div>
	</div>
</div>
<div class="lh-lightbox" id="fpa_lbLPDP">
	<div class="lq-container">
		<div class="lh-lightbox__container">
			<button class="lh-lightbox__close" type="button" aria-label="Close">
				<i class="icon lh-icon-close"></i>
			</button>
			<div class="lh-lightbox__content">
				${commerceText.v101004}
			</div>
		</div>
	</div>
</div>

