package pe.com.ibk.pepper.rest.fechahoraentrega.response;

public class Turno {
	private String id;
	private String hora;
	private String descripcion;
	
	public Turno() {
		super();
	}
	
	public Turno(String id, String hora, String descripcion) {
		super();
		this.id = id;
		this.hora = hora;
		this.descripcion = descripcion;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
