package pe.com.ibk.pepper.rest.potc.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AltaUsuario {
	
    private String grupo;
    private String modoCreacionUsuario;
    private String claveUsuario;
    private String nivelConfidencialidadUsuario;
    
    @JsonCreator
	public AltaUsuario(String grupo, String modoCreacionUsuario,
			String claveUsuario, String nivelConfidencialidadUsuario) {
		super();
		this.grupo = grupo;
		this.modoCreacionUsuario = modoCreacionUsuario;
		this.claveUsuario = claveUsuario;
		this.nivelConfidencialidadUsuario = nivelConfidencialidadUsuario;
	}
    
    @JsonProperty("grupo")
	public String getGrupo() {
		return grupo;
	}
    
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	
	@JsonProperty("modoCreacionUsuario")
	public String getModoCreacionUsuario() {
		return modoCreacionUsuario;
	}
	
	public void setModoCreacionUsuario(String modoCreacionUsuario) {
		this.modoCreacionUsuario = modoCreacionUsuario;
	}
	
	@JsonProperty("claveUsuario")
	public String getClaveUsuario() {
		return claveUsuario;
	}
	
	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}
	
	@JsonProperty("nivelConfidencialidadUsuario")
	public String getNivelConfidencialidadUsuario() {
		return nivelConfidencialidadUsuario;
	}
	
	public void setNivelConfidencialidadUsuario(String nivelConfidencialidadUsuario) {
		this.nivelConfidencialidadUsuario = nivelConfidencialidadUsuario;
	}
    
}
