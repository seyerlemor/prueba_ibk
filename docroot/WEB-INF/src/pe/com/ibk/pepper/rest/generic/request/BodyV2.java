
package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.calificacion.request.Evaluation;
import pe.com.ibk.pepper.rest.campania.request.ConsultarLead;
import pe.com.ibk.pepper.rest.equifax.request.ConsultaPreguntas;
import pe.com.ibk.pepper.rest.equifax.request.ValidacionPreguntas;
import pe.com.ibk.pepper.rest.listanegra.request.ValidarListaNegra;
import pe.com.ibk.pepper.rest.potc.request.ConsultarProductos;
import pe.com.ibk.pepper.rest.potc.request.ConsultarProvisional;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BodyV2 {

    private ValidarListaNegra validarListaNegra;
    private ConsultaPreguntas consultaPreguntas;
    private ValidacionPreguntas validacionPreguntas;
    private Evaluation evaluation;
    private ConsultarProvisional consultarProvisional;
    private ConsultarProductos consultarProductos;
    private ConsultarLead consultarLead;

    @JsonCreator
    public BodyV2(ConsultarLead consultarLead) {
		super();
		this.consultarLead = consultarLead;
	}

	@JsonCreator
    public BodyV2(ValidarListaNegra validarListaNegra) {
        super();
        this.validarListaNegra = validarListaNegra;
    }

    @JsonCreator
	public BodyV2(ValidacionPreguntas validacionPreguntas) {
		super();
		this.validacionPreguntas = validacionPreguntas;
	}

    @JsonCreator
    public BodyV2(ConsultaPreguntas consultaPreguntas) {
		super();
		this.consultaPreguntas = consultaPreguntas;
	}
    
    @JsonCreator
	public BodyV2(Evaluation evaluation) {
		super();
		this.evaluation = evaluation;
	}
    
    @JsonCreator
	public BodyV2(ConsultarProvisional consultarProvisional) {
		super();
		this.consultarProvisional = consultarProvisional;
	}

    @JsonCreator
	public BodyV2(ConsultarProductos consultarProductos) {
		super();
		this.consultarProductos = consultarProductos;
	}

	@JsonProperty("validarListaNegra")
    public ValidarListaNegra getValidarListaNegra() {
        return validarListaNegra;
    }

    public void setValidarListaNegra(ValidarListaNegra validarListaNegra) {
        this.validarListaNegra = validarListaNegra;
    }

    @JsonProperty("consultaPreguntas")
	public ConsultaPreguntas getConsultaPreguntas() {
		return consultaPreguntas;
	}

	public void setConsultaPreguntas(ConsultaPreguntas consultaPreguntas) {
		this.consultaPreguntas = consultaPreguntas;
	}

	@JsonProperty("validacionPreguntas")
	public ValidacionPreguntas getValidacionPreguntas() {
		return validacionPreguntas;
	}

	public void setValidacionPreguntas(ValidacionPreguntas validacionPreguntas) {
		this.validacionPreguntas = validacionPreguntas;
	}

	@JsonProperty("evaluation")
	public Evaluation getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}

	@JsonProperty("consultarProvisional")
	public ConsultarProvisional getConsultarProvisional() {
		return consultarProvisional;
	}

	public void setConsultarProvisional(ConsultarProvisional consultarProvisional) {
		this.consultarProvisional = consultarProvisional;
	}

	@JsonProperty("consultarProductos")
	public ConsultarProductos getConsultarProductos() {
		return consultarProductos;
	}

	public void setConsultarProductos(ConsultarProductos consultarProductos) {
		this.consultarProductos = consultarProductos;
	}

	@JsonProperty("ConsultarLead")
	public ConsultarLead getConsultarLead() {
		return consultarLead;
	}

	public void setConsultarLead(ConsultarLead consultarLead) {
		this.consultarLead = consultarLead;
	}

}
