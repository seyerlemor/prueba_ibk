
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InformacionEntrega {

    private String tipoDireccion;
    private String horaFin;
    private String codigoDepartamento;
    private String codigoDistrito;
    private String direccion;
    private String fechaEntrega;
    private String codigoProvincia;
    private String codigoTienda;
    private String referencia;
    private String email;
    private String horaInicio;
    private String numeroTelefono;

    /**
     * No args constructor for use in serialization
     * 
     */
    public InformacionEntrega() {
    }

    /**
     * 
     * @param direccion
     * @param numeroTelefono
     * @param horaInicio
     * @param codigoTienda
     * @param email
     * @param tipoDireccion
     * @param codigoProvincia
     * @param horaFin
     * @param fechaEntrega
     * @param codigoDepartamento
     * @param codigoDistrito
     * @param referencia
     */
    public InformacionEntrega(String tipoDireccion, String horaFin, String codigoDepartamento, String codigoDistrito, String direccion, String fechaEntrega, String codigoProvincia, String codigoTienda, String referencia, String email, String horaInicio, String numeroTelefono) {
        super();
        this.tipoDireccion = tipoDireccion;
        this.horaFin = horaFin;
        this.codigoDepartamento = codigoDepartamento;
        this.codigoDistrito = codigoDistrito;
        this.direccion = direccion;
        this.fechaEntrega = fechaEntrega;
        this.codigoProvincia = codigoProvincia;
        this.codigoTienda = codigoTienda;
        this.referencia = referencia;
        this.email = email;
        this.horaInicio = horaInicio;
        this.numeroTelefono = numeroTelefono;
    }

    public String getTipoDireccion() {
        return tipoDireccion;
    }

    public void setTipoDireccion(String tipoDireccion) {
        this.tipoDireccion = tipoDireccion;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    public String getCodigoDistrito() {
        return codigoDistrito;
    }

    public void setCodigoDistrito(String codigoDistrito) {
        this.codigoDistrito = codigoDistrito;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getCodigoProvincia() {
        return codigoProvincia;
    }

    public void setCodigoProvincia(String codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }

    public String getCodigoTienda() {
        return codigoTienda;
    }

    public void setCodigoTienda(String codigoTienda) {
        this.codigoTienda = codigoTienda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tipoDireccion", tipoDireccion).append("horaFin", horaFin).append("codigoDepartamento", codigoDepartamento).append("codigoDistrito", codigoDistrito).append("direccion", direccion).append("fechaEntrega", fechaEntrega).append("codigoProvincia", codigoProvincia).append("codigoTienda", codigoTienda).append("referencia", referencia).append("email", email).append("horaInicio", horaInicio).append("numeroTelefono", numeroTelefono).toString();
    }

}
