/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

/**
 * @author Interbank
 */
public interface ClienteFinder {
	public java.util.List<pe.com.ibk.pepper.model.Cliente> findByF_F_N_A_D(
		java.lang.String fechaDesde, java.lang.String fechaHasta,
		java.lang.String nombres, java.lang.String apellidos,
		java.lang.String DNI, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	public int countByF_F_N_A_D(java.lang.String fechaDesde,
		java.lang.String fechaHasta, java.lang.String nombres,
		java.lang.String apellidos, java.lang.String DNI)
		throws com.liferay.portal.kernel.exception.SystemException;
}