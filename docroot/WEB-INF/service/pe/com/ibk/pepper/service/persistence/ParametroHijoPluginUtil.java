/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.ParametroHijoPlugin;

import java.util.List;

/**
 * The persistence utility for the parametro hijo plugin service. This utility wraps {@link ParametroHijoPluginPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ParametroHijoPluginPersistence
 * @see ParametroHijoPluginPersistenceImpl
 * @generated
 */
public class ParametroHijoPluginUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ParametroHijoPlugin parametroHijoPlugin) {
		getPersistence().clearCache(parametroHijoPlugin);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ParametroHijoPlugin> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ParametroHijoPlugin> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ParametroHijoPlugin> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ParametroHijoPlugin update(
		ParametroHijoPlugin parametroHijoPlugin) throws SystemException {
		return getPersistence().update(parametroHijoPlugin);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ParametroHijoPlugin update(
		ParametroHijoPlugin parametroHijoPlugin, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(parametroHijoPlugin, serviceContext);
	}

	/**
	* Returns all the parametro hijo plugins where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_G(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_G(codigoPadre, estado);
	}

	/**
	* Returns a range of all the parametro hijo plugins where codigoPadre = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @return the range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_G(
		java.lang.String codigoPadre, boolean estado, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_G(codigoPadre, estado, start, end);
	}

	/**
	* Returns an ordered range of all the parametro hijo plugins where codigoPadre = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_G(
		java.lang.String codigoPadre, boolean estado, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_G(codigoPadre, estado, start, end, orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_G_First(
		java.lang.String codigoPadre, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_G_First(codigoPadre, estado, orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_G_First(
		java.lang.String codigoPadre, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_First(codigoPadre, estado, orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_G_Last(
		java.lang.String codigoPadre, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_G_Last(codigoPadre, estado, orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_G_Last(
		java.lang.String codigoPadre, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_Last(codigoPadre, estado, orderByComparator);
	}

	/**
	* Returns the parametro hijo plugins before and after the current parametro hijo plugin in the ordered set where codigoPadre = &#63; and estado = &#63;.
	*
	* @param idParametroHijo the primary key of the current parametro hijo plugin
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin[] findByC_G_PrevAndNext(
		long idParametroHijo, java.lang.String codigoPadre, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_G_PrevAndNext(idParametroHijo, codigoPadre, estado,
			orderByComparator);
	}

	/**
	* Removes all the parametro hijo plugins where codigoPadre = &#63; and estado = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByC_G(java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByC_G(codigoPadre, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_G(java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_G(codigoPadre, estado);
	}

	/**
	* Returns all the parametro hijo plugins where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @return the matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_N_D_G(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_N_D_G(codigoPadre, nombre, descripcion);
	}

	/**
	* Returns a range of all the parametro hijo plugins where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @return the range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_N_D_G(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_N_D_G(codigoPadre, nombre, descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the parametro hijo plugins where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_N_D_G(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_N_D_G(codigoPadre, nombre, descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_N_D_G_First(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_N_D_G_First(codigoPadre, nombre, descripcion,
			orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_N_D_G_First(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_N_D_G_First(codigoPadre, nombre, descripcion,
			orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_N_D_G_Last(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_N_D_G_Last(codigoPadre, nombre, descripcion,
			orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_N_D_G_Last(
		java.lang.String codigoPadre, java.lang.String nombre,
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_N_D_G_Last(codigoPadre, nombre, descripcion,
			orderByComparator);
	}

	/**
	* Returns the parametro hijo plugins before and after the current parametro hijo plugin in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param idParametroHijo the primary key of the current parametro hijo plugin
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin[] findByC_N_D_G_PrevAndNext(
		long idParametroHijo, java.lang.String codigoPadre,
		java.lang.String nombre, java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_N_D_G_PrevAndNext(idParametroHijo, codigoPadre,
			nombre, descripcion, orderByComparator);
	}

	/**
	* Removes all the parametro hijo plugins where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByC_N_D_G(java.lang.String codigoPadre,
		java.lang.String nombre, java.lang.String descripcion)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByC_N_D_G(codigoPadre, nombre, descripcion);
	}

	/**
	* Returns the number of parametro hijo plugins where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param nombre the nombre
	* @param descripcion the descripcion
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_N_D_G(java.lang.String codigoPadre,
		java.lang.String nombre, java.lang.String descripcion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_N_D_G(codigoPadre, nombre, descripcion);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPluginException} if it could not be found.
	*
	* @param codigo the codigo
	* @param estado the estado
	* @return the matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_G_E(
		java.lang.String codigo, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().findByC_G_E(codigo, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigo the codigo
	* @param estado the estado
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_G_E(
		java.lang.String codigo, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_G_E(codigo, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigo the codigo
	* @param estado the estado
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_G_E(
		java.lang.String codigo, boolean estado, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_G_E(codigo, estado, retrieveFromCache);
	}

	/**
	* Removes the parametro hijo plugin where codigo = &#63; and estado = &#63; from the database.
	*
	* @param codigo the codigo
	* @param estado the estado
	* @return the parametro hijo plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin removeByC_G_E(
		java.lang.String codigo, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().removeByC_G_E(codigo, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigo = &#63; and estado = &#63;.
	*
	* @param codigo the codigo
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_G_E(java.lang.String codigo, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_G_E(codigo, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPluginException} if it could not be found.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_C_G_E(
		java.lang.String codigo, java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().findByC_C_G_E(codigo, codigoPadre, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_C_G_E(
		java.lang.String codigo, java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_C_G_E(codigo, codigoPadre, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_C_G_E(
		java.lang.String codigo, java.lang.String codigoPadre, boolean estado,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_C_G_E(codigo, codigoPadre, estado,
			retrieveFromCache);
	}

	/**
	* Removes the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and estado = &#63; from the database.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the parametro hijo plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin removeByC_C_G_E(
		java.lang.String codigo, java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().removeByC_C_G_E(codigo, codigoPadre, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigo = &#63; and codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_C_G_E(java.lang.String codigo,
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_C_G_E(codigo, codigoPadre, estado);
	}

	/**
	* Returns all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @return the matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_D_G_E(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_D_G_E(codigoPadre, dato1, estado);
	}

	/**
	* Returns a range of all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @return the range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_D_G_E(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_D_G_E(codigoPadre, dato1, estado, start, end);
	}

	/**
	* Returns an ordered range of all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_D_G_E(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_D_G_E(codigoPadre, dato1, estado, start, end,
			orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_D_G_E_First(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_D_G_E_First(codigoPadre, dato1, estado,
			orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_D_G_E_First(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_D_G_E_First(codigoPadre, dato1, estado,
			orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_D_G_E_Last(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_D_G_E_Last(codigoPadre, dato1, estado,
			orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_D_G_E_Last(
		java.lang.String codigoPadre, java.lang.String dato1, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_D_G_E_Last(codigoPadre, dato1, estado,
			orderByComparator);
	}

	/**
	* Returns the parametro hijo plugins before and after the current parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param idParametroHijo the primary key of the current parametro hijo plugin
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin[] findByC_D_G_E_PrevAndNext(
		long idParametroHijo, java.lang.String codigoPadre,
		java.lang.String dato1, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_D_G_E_PrevAndNext(idParametroHijo, codigoPadre,
			dato1, estado, orderByComparator);
	}

	/**
	* Removes all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and estado = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByC_D_G_E(java.lang.String codigoPadre,
		java.lang.String dato1, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByC_D_G_E(codigoPadre, dato1, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_D_G_E(java.lang.String codigoPadre,
		java.lang.String dato1, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_D_G_E(codigoPadre, dato1, estado);
	}

	/**
	* Returns all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @return the matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_D_D_G_E(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_D_D_G_E(codigoPadre, dato1, dato2, estado);
	}

	/**
	* Returns a range of all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @return the range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_D_D_G_E(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_D_D_G_E(codigoPadre, dato1, dato2, estado, start,
			end);
	}

	/**
	* Returns an ordered range of all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findByC_D_D_G_E(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_D_D_G_E(codigoPadre, dato1, dato2, estado, start,
			end, orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_D_D_G_E_First(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_D_D_G_E_First(codigoPadre, dato1, dato2, estado,
			orderByComparator);
	}

	/**
	* Returns the first parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_D_D_G_E_First(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_D_D_G_E_First(codigoPadre, dato1, dato2, estado,
			orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_D_D_G_E_Last(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_D_D_G_E_Last(codigoPadre, dato1, dato2, estado,
			orderByComparator);
	}

	/**
	* Returns the last parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_D_D_G_E_Last(
		java.lang.String codigoPadre, java.lang.String dato1,
		java.lang.String dato2, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_D_D_G_E_Last(codigoPadre, dato1, dato2, estado,
			orderByComparator);
	}

	/**
	* Returns the parametro hijo plugins before and after the current parametro hijo plugin in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param idParametroHijo the primary key of the current parametro hijo plugin
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin[] findByC_D_D_G_E_PrevAndNext(
		long idParametroHijo, java.lang.String codigoPadre,
		java.lang.String dato1, java.lang.String dato2, boolean estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_D_D_G_E_PrevAndNext(idParametroHijo, codigoPadre,
			dato1, dato2, estado, orderByComparator);
	}

	/**
	* Removes all the parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByC_D_D_G_E(java.lang.String codigoPadre,
		java.lang.String dato1, java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByC_D_D_G_E(codigoPadre, dato1, dato2, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato1 the dato1
	* @param dato2 the dato2
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_D_D_G_E(java.lang.String codigoPadre,
		java.lang.String dato1, java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByC_D_D_G_E(codigoPadre, dato1, dato2, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPluginException} if it could not be found.
	*
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_D2_G_E(
		java.lang.String codigoPadre, java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().findByC_D2_G_E(codigoPadre, dato2, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_D2_G_E(
		java.lang.String codigoPadre, java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_D2_G_E(codigoPadre, dato2, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_D2_G_E(
		java.lang.String codigoPadre, java.lang.String dato2, boolean estado,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_D2_G_E(codigoPadre, dato2, estado,
			retrieveFromCache);
	}

	/**
	* Removes the parametro hijo plugin where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the parametro hijo plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin removeByC_D2_G_E(
		java.lang.String codigoPadre, java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().removeByC_D2_G_E(codigoPadre, dato2, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigoPadre = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_D2_G_E(java.lang.String codigoPadre,
		java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_D2_G_E(codigoPadre, dato2, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPluginException} if it could not be found.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the matching parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByC_C_D2_E(
		java.lang.String codigo, java.lang.String codigoPadre,
		java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .findByC_C_D2_E(codigo, codigoPadre, dato2, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_C_D2_E(
		java.lang.String codigo, java.lang.String codigoPadre,
		java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_C_D2_E(codigo, codigoPadre, dato2, estado);
	}

	/**
	* Returns the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro hijo plugin, or <code>null</code> if a matching parametro hijo plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByC_C_D2_E(
		java.lang.String codigo, java.lang.String codigoPadre,
		java.lang.String dato2, boolean estado, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_C_D2_E(codigo, codigoPadre, dato2, estado,
			retrieveFromCache);
	}

	/**
	* Removes the parametro hijo plugin where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; from the database.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the parametro hijo plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin removeByC_C_D2_E(
		java.lang.String codigo, java.lang.String codigoPadre,
		java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence()
				   .removeByC_C_D2_E(codigo, codigoPadre, dato2, estado);
	}

	/**
	* Returns the number of parametro hijo plugins where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63;.
	*
	* @param codigo the codigo
	* @param codigoPadre the codigo padre
	* @param dato2 the dato2
	* @param estado the estado
	* @return the number of matching parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_C_D2_E(java.lang.String codigo,
		java.lang.String codigoPadre, java.lang.String dato2, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByC_C_D2_E(codigo, codigoPadre, dato2, estado);
	}

	/**
	* Caches the parametro hijo plugin in the entity cache if it is enabled.
	*
	* @param parametroHijoPlugin the parametro hijo plugin
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.ParametroHijoPlugin parametroHijoPlugin) {
		getPersistence().cacheResult(parametroHijoPlugin);
	}

	/**
	* Caches the parametro hijo plugins in the entity cache if it is enabled.
	*
	* @param parametroHijoPlugins the parametro hijo plugins
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> parametroHijoPlugins) {
		getPersistence().cacheResult(parametroHijoPlugins);
	}

	/**
	* Creates a new parametro hijo plugin with the primary key. Does not add the parametro hijo plugin to the database.
	*
	* @param idParametroHijo the primary key for the new parametro hijo plugin
	* @return the new parametro hijo plugin
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin create(
		long idParametroHijo) {
		return getPersistence().create(idParametroHijo);
	}

	/**
	* Removes the parametro hijo plugin with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idParametroHijo the primary key of the parametro hijo plugin
	* @return the parametro hijo plugin that was removed
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin remove(
		long idParametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().remove(idParametroHijo);
	}

	public static pe.com.ibk.pepper.model.ParametroHijoPlugin updateImpl(
		pe.com.ibk.pepper.model.ParametroHijoPlugin parametroHijoPlugin)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(parametroHijoPlugin);
	}

	/**
	* Returns the parametro hijo plugin with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPluginException} if it could not be found.
	*
	* @param idParametroHijo the primary key of the parametro hijo plugin
	* @return the parametro hijo plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroHijoPluginException if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin findByPrimaryKey(
		long idParametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroHijoPluginException {
		return getPersistence().findByPrimaryKey(idParametroHijo);
	}

	/**
	* Returns the parametro hijo plugin with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idParametroHijo the primary key of the parametro hijo plugin
	* @return the parametro hijo plugin, or <code>null</code> if a parametro hijo plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijoPlugin fetchByPrimaryKey(
		long idParametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idParametroHijo);
	}

	/**
	* Returns all the parametro hijo plugins.
	*
	* @return the parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the parametro hijo plugins.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @return the range of parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the parametro hijo plugins.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro hijo plugins
	* @param end the upper bound of the range of parametro hijo plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijoPlugin> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the parametro hijo plugins from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of parametro hijo plugins.
	*
	* @return the number of parametro hijo plugins
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ParametroHijoPluginPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ParametroHijoPluginPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ParametroHijoPluginPersistence.class.getName());

			ReferenceRegistry.registerReference(ParametroHijoPluginUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ParametroHijoPluginPersistence persistence) {
	}

	private static ParametroHijoPluginPersistence _persistence;
}