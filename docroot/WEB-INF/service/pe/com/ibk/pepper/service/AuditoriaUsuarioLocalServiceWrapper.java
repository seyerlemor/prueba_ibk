/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AuditoriaUsuarioLocalService}.
 *
 * @author Interbank
 * @see AuditoriaUsuarioLocalService
 * @generated
 */
public class AuditoriaUsuarioLocalServiceWrapper
	implements AuditoriaUsuarioLocalService,
		ServiceWrapper<AuditoriaUsuarioLocalService> {
	public AuditoriaUsuarioLocalServiceWrapper(
		AuditoriaUsuarioLocalService auditoriaUsuarioLocalService) {
		_auditoriaUsuarioLocalService = auditoriaUsuarioLocalService;
	}

	/**
	* Adds the auditoria usuario to the database. Also notifies the appropriate model listeners.
	*
	* @param auditoriaUsuario the auditoria usuario
	* @return the auditoria usuario that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario addAuditoriaUsuario(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.addAuditoriaUsuario(auditoriaUsuario);
	}

	/**
	* Creates a new auditoria usuario with the primary key. Does not add the auditoria usuario to the database.
	*
	* @param idAuditoriaUsuario the primary key for the new auditoria usuario
	* @return the new auditoria usuario
	*/
	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario createAuditoriaUsuario(
		long idAuditoriaUsuario) {
		return _auditoriaUsuarioLocalService.createAuditoriaUsuario(idAuditoriaUsuario);
	}

	/**
	* Deletes the auditoria usuario with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idAuditoriaUsuario the primary key of the auditoria usuario
	* @return the auditoria usuario that was removed
	* @throws PortalException if a auditoria usuario with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario deleteAuditoriaUsuario(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.deleteAuditoriaUsuario(idAuditoriaUsuario);
	}

	/**
	* Deletes the auditoria usuario from the database. Also notifies the appropriate model listeners.
	*
	* @param auditoriaUsuario the auditoria usuario
	* @return the auditoria usuario that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario deleteAuditoriaUsuario(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.deleteAuditoriaUsuario(auditoriaUsuario);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _auditoriaUsuarioLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaUsuarioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaUsuarioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario fetchAuditoriaUsuario(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.fetchAuditoriaUsuario(idAuditoriaUsuario);
	}

	/**
	* Returns the auditoria usuario with the primary key.
	*
	* @param idAuditoriaUsuario the primary key of the auditoria usuario
	* @return the auditoria usuario
	* @throws PortalException if a auditoria usuario with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario getAuditoriaUsuario(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.getAuditoriaUsuario(idAuditoriaUsuario);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the auditoria usuarios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaUsuarioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria usuarios
	* @param end the upper bound of the range of auditoria usuarios (not inclusive)
	* @return the range of auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.AuditoriaUsuario> getAuditoriaUsuarios(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.getAuditoriaUsuarios(start, end);
	}

	/**
	* Returns the number of auditoria usuarios.
	*
	* @return the number of auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getAuditoriaUsuariosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.getAuditoriaUsuariosCount();
	}

	/**
	* Updates the auditoria usuario in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param auditoriaUsuario the auditoria usuario
	* @return the auditoria usuario that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario updateAuditoriaUsuario(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.updateAuditoriaUsuario(auditoriaUsuario);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _auditoriaUsuarioLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_auditoriaUsuarioLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _auditoriaUsuarioLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario registrarAuditoriaUsuario(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditoriaUsuarioLocalService.registrarAuditoriaUsuario(auditoriaUsuario);
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario buscarAuditoriaUsuarioporId(
		long idAuditoriaUsuario) {
		return _auditoriaUsuarioLocalService.buscarAuditoriaUsuarioporId(idAuditoriaUsuario);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public AuditoriaUsuarioLocalService getWrappedAuditoriaUsuarioLocalService() {
		return _auditoriaUsuarioLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedAuditoriaUsuarioLocalService(
		AuditoriaUsuarioLocalService auditoriaUsuarioLocalService) {
		_auditoriaUsuarioLocalService = auditoriaUsuarioLocalService;
	}

	@Override
	public AuditoriaUsuarioLocalService getWrappedService() {
		return _auditoriaUsuarioLocalService;
	}

	@Override
	public void setWrappedService(
		AuditoriaUsuarioLocalService auditoriaUsuarioLocalService) {
		_auditoriaUsuarioLocalService = auditoriaUsuarioLocalService;
	}

	private AuditoriaUsuarioLocalService _auditoriaUsuarioLocalService;
}