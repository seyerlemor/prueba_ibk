/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchParametroPadrePOException;
import pe.com.ibk.pepper.model.ParametroPadrePO;
import pe.com.ibk.pepper.model.impl.ParametroPadrePOImpl;
import pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the parametro padre p o service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ParametroPadrePOPersistence
 * @see ParametroPadrePOUtil
 * @generated
 */
public class ParametroPadrePOPersistenceImpl extends BasePersistenceImpl<ParametroPadrePO>
	implements ParametroPadrePOPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ParametroPadrePOUtil} to access the parametro padre p o persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ParametroPadrePOImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_C_G_E = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByC_G_E",
			new String[] { String.class.getName(), Boolean.class.getName() },
			ParametroPadrePOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroPadrePOModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_G_E = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_G_E",
			new String[] { String.class.getName(), Boolean.class.getName() });

	/**
	 * Returns the parametro padre p o where codigoPadre = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePOException} if it could not be found.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the matching parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByC_G_E(String codigoPadre, boolean estado)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByC_G_E(codigoPadre, estado);

		if (parametroPadrePO == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigoPadre=");
			msg.append(codigoPadre);

			msg.append(", estado=");
			msg.append(estado);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchParametroPadrePOException(msg.toString());
		}

		return parametroPadrePO;
	}

	/**
	 * Returns the parametro padre p o where codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByC_G_E(String codigoPadre, boolean estado)
		throws SystemException {
		return fetchByC_G_E(codigoPadre, estado, true);
	}

	/**
	 * Returns the parametro padre p o where codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByC_G_E(String codigoPadre, boolean estado,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { codigoPadre, estado };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_G_E,
					finderArgs, this);
		}

		if (result instanceof ParametroPadrePO) {
			ParametroPadrePO parametroPadrePO = (ParametroPadrePO)result;

			if (!Validator.equals(codigoPadre, parametroPadrePO.getCodigoPadre()) ||
					(estado != parametroPadrePO.getEstado())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PARAMETROPADREPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_G_E_CODIGOPADRE_2);
			}

			query.append(_FINDER_COLUMN_C_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				qPos.add(estado);

				List<ParametroPadrePO> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ParametroPadrePOPersistenceImpl.fetchByC_G_E(String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ParametroPadrePO parametroPadrePO = list.get(0);

					result = parametroPadrePO;

					cacheResult(parametroPadrePO);

					if ((parametroPadrePO.getCodigoPadre() == null) ||
							!parametroPadrePO.getCodigoPadre()
												 .equals(codigoPadre) ||
							(parametroPadrePO.getEstado() != estado)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E,
							finderArgs, parametroPadrePO);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ParametroPadrePO)result;
		}
	}

	/**
	 * Removes the parametro padre p o where codigoPadre = &#63; and estado = &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the parametro padre p o that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO removeByC_G_E(String codigoPadre, boolean estado)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = findByC_G_E(codigoPadre, estado);

		return remove(parametroPadrePO);
	}

	/**
	 * Returns the number of parametro padre p os where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the number of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_G_E(String codigoPadre, boolean estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_G_E;

		Object[] finderArgs = new Object[] { codigoPadre, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PARAMETROPADREPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_G_E_CODIGOPADRE_2);
			}

			query.append(_FINDER_COLUMN_C_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_G_E_CODIGOPADRE_1 = "parametroPadrePO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_G_E_CODIGOPADRE_2 = "parametroPadrePO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_G_E_CODIGOPADRE_3 = "(parametroPadrePO.codigoPadre IS NULL OR parametroPadrePO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_G_E_ESTADO_2 = "parametroPadrePO.estado = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_C_G = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByC_G",
			new String[] { String.class.getName() },
			ParametroPadrePOModelImpl.CODIGOPADRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_G = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_G",
			new String[] { String.class.getName() });

	/**
	 * Returns the parametro padre p o where codigoPadre = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePOException} if it could not be found.
	 *
	 * @param codigoPadre the codigo padre
	 * @return the matching parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByC_G(String codigoPadre)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByC_G(codigoPadre);

		if (parametroPadrePO == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigoPadre=");
			msg.append(codigoPadre);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchParametroPadrePOException(msg.toString());
		}

		return parametroPadrePO;
	}

	/**
	 * Returns the parametro padre p o where codigoPadre = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigoPadre the codigo padre
	 * @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByC_G(String codigoPadre)
		throws SystemException {
		return fetchByC_G(codigoPadre, true);
	}

	/**
	 * Returns the parametro padre p o where codigoPadre = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigoPadre the codigo padre
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByC_G(String codigoPadre,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { codigoPadre };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_G,
					finderArgs, this);
		}

		if (result instanceof ParametroPadrePO) {
			ParametroPadrePO parametroPadrePO = (ParametroPadrePO)result;

			if (!Validator.equals(codigoPadre, parametroPadrePO.getCodigoPadre())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PARAMETROPADREPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				List<ParametroPadrePO> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ParametroPadrePOPersistenceImpl.fetchByC_G(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ParametroPadrePO parametroPadrePO = list.get(0);

					result = parametroPadrePO;

					cacheResult(parametroPadrePO);

					if ((parametroPadrePO.getCodigoPadre() == null) ||
							!parametroPadrePO.getCodigoPadre()
												 .equals(codigoPadre)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G,
							finderArgs, parametroPadrePO);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ParametroPadrePO)result;
		}
	}

	/**
	 * Removes the parametro padre p o where codigoPadre = &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @return the parametro padre p o that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO removeByC_G(String codigoPadre)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = findByC_G(codigoPadre);

		return remove(parametroPadrePO);
	}

	/**
	 * Returns the number of parametro padre p os where codigoPadre = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @return the number of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_G(String codigoPadre) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_G;

		Object[] finderArgs = new Object[] { codigoPadre };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PARAMETROPADREPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_G_CODIGOPADRE_1 = "parametroPadrePO.codigoPadre IS NULL";
	private static final String _FINDER_COLUMN_C_G_CODIGOPADRE_2 = "parametroPadrePO.codigoPadre = ?";
	private static final String _FINDER_COLUMN_C_G_CODIGOPADRE_3 = "(parametroPadrePO.codigoPadre IS NULL OR parametroPadrePO.codigoPadre = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_N_G = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByN_G",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_N_G = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByN_G",
			new String[] { String.class.getName() });

	/**
	 * Returns all the parametro padre p os where nombre LIKE &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findByN_G(String nombre)
		throws SystemException {
		return findByN_G(nombre, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro padre p os where nombre LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of parametro padre p os
	 * @param end the upper bound of the range of parametro padre p os (not inclusive)
	 * @return the range of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findByN_G(String nombre, int start, int end)
		throws SystemException {
		return findByN_G(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro padre p os where nombre LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of parametro padre p os
	 * @param end the upper bound of the range of parametro padre p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findByN_G(String nombre, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_N_G;
		finderArgs = new Object[] { nombre, start, end, orderByComparator };

		List<ParametroPadrePO> list = (List<ParametroPadrePO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ParametroPadrePO parametroPadrePO : list) {
				if (!StringUtil.wildcardMatches(parametroPadrePO.getNombre(),
							nombre, CharPool.UNDERLINE, CharPool.PERCENT,
							CharPool.BACK_SLASH, true)) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PARAMETROPADREPO_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_N_G_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_N_G_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_N_G_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ParametroPadrePOModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (!pagination) {
					list = (List<ParametroPadrePO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroPadrePO>(list);
				}
				else {
					list = (List<ParametroPadrePO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first parametro padre p o in the ordered set where nombre LIKE &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByN_G_First(String nombre,
		OrderByComparator orderByComparator)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByN_G_First(nombre,
				orderByComparator);

		if (parametroPadrePO != null) {
			return parametroPadrePO;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroPadrePOException(msg.toString());
	}

	/**
	 * Returns the first parametro padre p o in the ordered set where nombre LIKE &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByN_G_First(String nombre,
		OrderByComparator orderByComparator) throws SystemException {
		List<ParametroPadrePO> list = findByN_G(nombre, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last parametro padre p o in the ordered set where nombre LIKE &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByN_G_Last(String nombre,
		OrderByComparator orderByComparator)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByN_G_Last(nombre,
				orderByComparator);

		if (parametroPadrePO != null) {
			return parametroPadrePO;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroPadrePOException(msg.toString());
	}

	/**
	 * Returns the last parametro padre p o in the ordered set where nombre LIKE &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByN_G_Last(String nombre,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByN_G(nombre);

		if (count == 0) {
			return null;
		}

		List<ParametroPadrePO> list = findByN_G(nombre, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the parametro padre p os before and after the current parametro padre p o in the ordered set where nombre LIKE &#63;.
	 *
	 * @param idParametroPadre the primary key of the current parametro padre p o
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO[] findByN_G_PrevAndNext(long idParametroPadre,
		String nombre, OrderByComparator orderByComparator)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = findByPrimaryKey(idParametroPadre);

		Session session = null;

		try {
			session = openSession();

			ParametroPadrePO[] array = new ParametroPadrePOImpl[3];

			array[0] = getByN_G_PrevAndNext(session, parametroPadrePO, nombre,
					orderByComparator, true);

			array[1] = parametroPadrePO;

			array[2] = getByN_G_PrevAndNext(session, parametroPadrePO, nombre,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ParametroPadrePO getByN_G_PrevAndNext(Session session,
		ParametroPadrePO parametroPadrePO, String nombre,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PARAMETROPADREPO_WHERE);

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_N_G_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_N_G_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_N_G_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ParametroPadrePOModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(parametroPadrePO);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ParametroPadrePO> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the parametro padre p os where nombre LIKE &#63; from the database.
	 *
	 * @param nombre the nombre
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByN_G(String nombre) throws SystemException {
		for (ParametroPadrePO parametroPadrePO : findByN_G(nombre,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(parametroPadrePO);
		}
	}

	/**
	 * Returns the number of parametro padre p os where nombre LIKE &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByN_G(String nombre) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_N_G;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PARAMETROPADREPO_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_N_G_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_N_G_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_N_G_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_N_G_NOMBRE_1 = "parametroPadrePO.nombre LIKE NULL";
	private static final String _FINDER_COLUMN_N_G_NOMBRE_2 = "parametroPadrePO.nombre LIKE ?";
	private static final String _FINDER_COLUMN_N_G_NOMBRE_3 = "(parametroPadrePO.nombre IS NULL OR parametroPadrePO.nombre LIKE '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED,
			ParametroPadrePOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			ParametroPadrePOModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the parametro padre p os where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findByGroupId(long groupId)
		throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro padre p os where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of parametro padre p os
	 * @param end the upper bound of the range of parametro padre p os (not inclusive)
	 * @return the range of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro padre p os where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of parametro padre p os
	 * @param end the upper bound of the range of parametro padre p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findByGroupId(long groupId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<ParametroPadrePO> list = (List<ParametroPadrePO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ParametroPadrePO parametroPadrePO : list) {
				if ((groupId != parametroPadrePO.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PARAMETROPADREPO_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ParametroPadrePOModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<ParametroPadrePO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroPadrePO>(list);
				}
				else {
					list = (List<ParametroPadrePO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first parametro padre p o in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByGroupId_First(groupId,
				orderByComparator);

		if (parametroPadrePO != null) {
			return parametroPadrePO;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroPadrePOException(msg.toString());
	}

	/**
	 * Returns the first parametro padre p o in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByGroupId_First(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ParametroPadrePO> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last parametro padre p o in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (parametroPadrePO != null) {
			return parametroPadrePO;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroPadrePOException(msg.toString());
	}

	/**
	 * Returns the last parametro padre p o in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByGroupId_Last(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<ParametroPadrePO> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the parametro padre p os before and after the current parametro padre p o in the ordered set where groupId = &#63;.
	 *
	 * @param idParametroPadre the primary key of the current parametro padre p o
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO[] findByGroupId_PrevAndNext(long idParametroPadre,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = findByPrimaryKey(idParametroPadre);

		Session session = null;

		try {
			session = openSession();

			ParametroPadrePO[] array = new ParametroPadrePOImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, parametroPadrePO,
					groupId, orderByComparator, true);

			array[1] = parametroPadrePO;

			array[2] = getByGroupId_PrevAndNext(session, parametroPadrePO,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ParametroPadrePO getByGroupId_PrevAndNext(Session session,
		ParametroPadrePO parametroPadrePO, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PARAMETROPADREPO_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ParametroPadrePOModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(parametroPadrePO);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ParametroPadrePO> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the parametro padre p os where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGroupId(long groupId) throws SystemException {
		for (ParametroPadrePO parametroPadrePO : findByGroupId(groupId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(parametroPadrePO);
		}
	}

	/**
	 * Returns the number of parametro padre p os where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroupId(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PARAMETROPADREPO_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "parametroPadrePO.groupId = ?";

	public ParametroPadrePOPersistenceImpl() {
		setModelClass(ParametroPadrePO.class);
	}

	/**
	 * Caches the parametro padre p o in the entity cache if it is enabled.
	 *
	 * @param parametroPadrePO the parametro padre p o
	 */
	@Override
	public void cacheResult(ParametroPadrePO parametroPadrePO) {
		EntityCacheUtil.putResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOImpl.class, parametroPadrePO.getPrimaryKey(),
			parametroPadrePO);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E,
			new Object[] {
				parametroPadrePO.getCodigoPadre(), parametroPadrePO.getEstado()
			}, parametroPadrePO);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G,
			new Object[] { parametroPadrePO.getCodigoPadre() }, parametroPadrePO);

		parametroPadrePO.resetOriginalValues();
	}

	/**
	 * Caches the parametro padre p os in the entity cache if it is enabled.
	 *
	 * @param parametroPadrePOs the parametro padre p os
	 */
	@Override
	public void cacheResult(List<ParametroPadrePO> parametroPadrePOs) {
		for (ParametroPadrePO parametroPadrePO : parametroPadrePOs) {
			if (EntityCacheUtil.getResult(
						ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
						ParametroPadrePOImpl.class,
						parametroPadrePO.getPrimaryKey()) == null) {
				cacheResult(parametroPadrePO);
			}
			else {
				parametroPadrePO.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all parametro padre p os.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ParametroPadrePOImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ParametroPadrePOImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the parametro padre p o.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ParametroPadrePO parametroPadrePO) {
		EntityCacheUtil.removeResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOImpl.class, parametroPadrePO.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(parametroPadrePO);
	}

	@Override
	public void clearCache(List<ParametroPadrePO> parametroPadrePOs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ParametroPadrePO parametroPadrePO : parametroPadrePOs) {
			EntityCacheUtil.removeResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
				ParametroPadrePOImpl.class, parametroPadrePO.getPrimaryKey());

			clearUniqueFindersCache(parametroPadrePO);
		}
	}

	protected void cacheUniqueFindersCache(ParametroPadrePO parametroPadrePO) {
		if (parametroPadrePO.isNew()) {
			Object[] args = new Object[] {
					parametroPadrePO.getCodigoPadre(),
					parametroPadrePO.getEstado()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E, args,
				parametroPadrePO);

			args = new Object[] { parametroPadrePO.getCodigoPadre() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G, args,
				parametroPadrePO);
		}
		else {
			ParametroPadrePOModelImpl parametroPadrePOModelImpl = (ParametroPadrePOModelImpl)parametroPadrePO;

			if ((parametroPadrePOModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_G_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroPadrePO.getCodigoPadre(),
						parametroPadrePO.getEstado()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E, args,
					parametroPadrePO);
			}

			if ((parametroPadrePOModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { parametroPadrePO.getCodigoPadre() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G, args,
					parametroPadrePO);
			}
		}
	}

	protected void clearUniqueFindersCache(ParametroPadrePO parametroPadrePO) {
		ParametroPadrePOModelImpl parametroPadrePOModelImpl = (ParametroPadrePOModelImpl)parametroPadrePO;

		Object[] args = new Object[] {
				parametroPadrePO.getCodigoPadre(), parametroPadrePO.getEstado()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G_E, args);

		if ((parametroPadrePOModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_G_E.getColumnBitmask()) != 0) {
			args = new Object[] {
					parametroPadrePOModelImpl.getOriginalCodigoPadre(),
					parametroPadrePOModelImpl.getOriginalEstado()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G_E, args);
		}

		args = new Object[] { parametroPadrePO.getCodigoPadre() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G, args);

		if ((parametroPadrePOModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					parametroPadrePOModelImpl.getOriginalCodigoPadre()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G, args);
		}
	}

	/**
	 * Creates a new parametro padre p o with the primary key. Does not add the parametro padre p o to the database.
	 *
	 * @param idParametroPadre the primary key for the new parametro padre p o
	 * @return the new parametro padre p o
	 */
	@Override
	public ParametroPadrePO create(long idParametroPadre) {
		ParametroPadrePO parametroPadrePO = new ParametroPadrePOImpl();

		parametroPadrePO.setNew(true);
		parametroPadrePO.setPrimaryKey(idParametroPadre);

		return parametroPadrePO;
	}

	/**
	 * Removes the parametro padre p o with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idParametroPadre the primary key of the parametro padre p o
	 * @return the parametro padre p o that was removed
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO remove(long idParametroPadre)
		throws NoSuchParametroPadrePOException, SystemException {
		return remove((Serializable)idParametroPadre);
	}

	/**
	 * Removes the parametro padre p o with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the parametro padre p o
	 * @return the parametro padre p o that was removed
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO remove(Serializable primaryKey)
		throws NoSuchParametroPadrePOException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ParametroPadrePO parametroPadrePO = (ParametroPadrePO)session.get(ParametroPadrePOImpl.class,
					primaryKey);

			if (parametroPadrePO == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchParametroPadrePOException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(parametroPadrePO);
		}
		catch (NoSuchParametroPadrePOException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ParametroPadrePO removeImpl(ParametroPadrePO parametroPadrePO)
		throws SystemException {
		parametroPadrePO = toUnwrappedModel(parametroPadrePO);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(parametroPadrePO)) {
				parametroPadrePO = (ParametroPadrePO)session.get(ParametroPadrePOImpl.class,
						parametroPadrePO.getPrimaryKeyObj());
			}

			if (parametroPadrePO != null) {
				session.delete(parametroPadrePO);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (parametroPadrePO != null) {
			clearCache(parametroPadrePO);
		}

		return parametroPadrePO;
	}

	@Override
	public ParametroPadrePO updateImpl(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO)
		throws SystemException {
		parametroPadrePO = toUnwrappedModel(parametroPadrePO);

		boolean isNew = parametroPadrePO.isNew();

		ParametroPadrePOModelImpl parametroPadrePOModelImpl = (ParametroPadrePOModelImpl)parametroPadrePO;

		Session session = null;

		try {
			session = openSession();

			if (parametroPadrePO.isNew()) {
				session.save(parametroPadrePO);

				parametroPadrePO.setNew(false);
			}
			else {
				session.merge(parametroPadrePO);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ParametroPadrePOModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((parametroPadrePOModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroPadrePOModelImpl.getOriginalGroupId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { parametroPadrePOModelImpl.getGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		EntityCacheUtil.putResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroPadrePOImpl.class, parametroPadrePO.getPrimaryKey(),
			parametroPadrePO);

		clearUniqueFindersCache(parametroPadrePO);
		cacheUniqueFindersCache(parametroPadrePO);

		return parametroPadrePO;
	}

	protected ParametroPadrePO toUnwrappedModel(
		ParametroPadrePO parametroPadrePO) {
		if (parametroPadrePO instanceof ParametroPadrePOImpl) {
			return parametroPadrePO;
		}

		ParametroPadrePOImpl parametroPadrePOImpl = new ParametroPadrePOImpl();

		parametroPadrePOImpl.setNew(parametroPadrePO.isNew());
		parametroPadrePOImpl.setPrimaryKey(parametroPadrePO.getPrimaryKey());

		parametroPadrePOImpl.setIdParametroPadre(parametroPadrePO.getIdParametroPadre());
		parametroPadrePOImpl.setGroupId(parametroPadrePO.getGroupId());
		parametroPadrePOImpl.setCompanyId(parametroPadrePO.getCompanyId());
		parametroPadrePOImpl.setUserId(parametroPadrePO.getUserId());
		parametroPadrePOImpl.setUserName(parametroPadrePO.getUserName());
		parametroPadrePOImpl.setCreateDate(parametroPadrePO.getCreateDate());
		parametroPadrePOImpl.setModifiedDate(parametroPadrePO.getModifiedDate());
		parametroPadrePOImpl.setCodigoPadre(parametroPadrePO.getCodigoPadre());
		parametroPadrePOImpl.setNombre(parametroPadrePO.getNombre());
		parametroPadrePOImpl.setDescripcion(parametroPadrePO.getDescripcion());
		parametroPadrePOImpl.setEstado(parametroPadrePO.isEstado());
		parametroPadrePOImpl.setJson(parametroPadrePO.getJson());

		return parametroPadrePOImpl;
	}

	/**
	 * Returns the parametro padre p o with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the parametro padre p o
	 * @return the parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByPrimaryKey(Serializable primaryKey)
		throws NoSuchParametroPadrePOException, SystemException {
		ParametroPadrePO parametroPadrePO = fetchByPrimaryKey(primaryKey);

		if (parametroPadrePO == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchParametroPadrePOException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return parametroPadrePO;
	}

	/**
	 * Returns the parametro padre p o with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePOException} if it could not be found.
	 *
	 * @param idParametroPadre the primary key of the parametro padre p o
	 * @return the parametro padre p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO findByPrimaryKey(long idParametroPadre)
		throws NoSuchParametroPadrePOException, SystemException {
		return findByPrimaryKey((Serializable)idParametroPadre);
	}

	/**
	 * Returns the parametro padre p o with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the parametro padre p o
	 * @return the parametro padre p o, or <code>null</code> if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ParametroPadrePO parametroPadrePO = (ParametroPadrePO)EntityCacheUtil.getResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
				ParametroPadrePOImpl.class, primaryKey);

		if (parametroPadrePO == _nullParametroPadrePO) {
			return null;
		}

		if (parametroPadrePO == null) {
			Session session = null;

			try {
				session = openSession();

				parametroPadrePO = (ParametroPadrePO)session.get(ParametroPadrePOImpl.class,
						primaryKey);

				if (parametroPadrePO != null) {
					cacheResult(parametroPadrePO);
				}
				else {
					EntityCacheUtil.putResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
						ParametroPadrePOImpl.class, primaryKey,
						_nullParametroPadrePO);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ParametroPadrePOModelImpl.ENTITY_CACHE_ENABLED,
					ParametroPadrePOImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return parametroPadrePO;
	}

	/**
	 * Returns the parametro padre p o with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idParametroPadre the primary key of the parametro padre p o
	 * @return the parametro padre p o, or <code>null</code> if a parametro padre p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroPadrePO fetchByPrimaryKey(long idParametroPadre)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idParametroPadre);
	}

	/**
	 * Returns all the parametro padre p os.
	 *
	 * @return the parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro padre p os.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of parametro padre p os
	 * @param end the upper bound of the range of parametro padre p os (not inclusive)
	 * @return the range of parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro padre p os.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of parametro padre p os
	 * @param end the upper bound of the range of parametro padre p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroPadrePO> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ParametroPadrePO> list = (List<ParametroPadrePO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PARAMETROPADREPO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PARAMETROPADREPO;

				if (pagination) {
					sql = sql.concat(ParametroPadrePOModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ParametroPadrePO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroPadrePO>(list);
				}
				else {
					list = (List<ParametroPadrePO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the parametro padre p os from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ParametroPadrePO parametroPadrePO : findAll()) {
			remove(parametroPadrePO);
		}
	}

	/**
	 * Returns the number of parametro padre p os.
	 *
	 * @return the number of parametro padre p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PARAMETROPADREPO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the parametro padre p o persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.ParametroPadrePO")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ParametroPadrePO>> listenersList = new ArrayList<ModelListener<ParametroPadrePO>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ParametroPadrePO>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ParametroPadrePOImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PARAMETROPADREPO = "SELECT parametroPadrePO FROM ParametroPadrePO parametroPadrePO";
	private static final String _SQL_SELECT_PARAMETROPADREPO_WHERE = "SELECT parametroPadrePO FROM ParametroPadrePO parametroPadrePO WHERE ";
	private static final String _SQL_COUNT_PARAMETROPADREPO = "SELECT COUNT(parametroPadrePO) FROM ParametroPadrePO parametroPadrePO";
	private static final String _SQL_COUNT_PARAMETROPADREPO_WHERE = "SELECT COUNT(parametroPadrePO) FROM ParametroPadrePO parametroPadrePO WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "parametroPadrePO.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ParametroPadrePO exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ParametroPadrePO exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ParametroPadrePOPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idParametroPadre", "groupId", "companyId", "userId", "userName",
				"createDate", "modifiedDate", "codigoPadre", "nombre",
				"descripcion", "estado", "json"
			});
	private static ParametroPadrePO _nullParametroPadrePO = new ParametroPadrePOImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ParametroPadrePO> toCacheModel() {
				return _nullParametroPadrePOCacheModel;
			}
		};

	private static CacheModel<ParametroPadrePO> _nullParametroPadrePOCacheModel = new CacheModel<ParametroPadrePO>() {
			@Override
			public ParametroPadrePO toEntityModel() {
				return _nullParametroPadrePO;
			}
		};
}