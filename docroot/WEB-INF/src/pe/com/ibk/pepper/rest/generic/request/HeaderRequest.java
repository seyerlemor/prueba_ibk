package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HeaderRequest {

	private Request request;
	private Identity identity;
	
	@JsonCreator
	public HeaderRequest(Request request, Identity identity) {
		super();
		this.request = request;
		this.identity = identity;
	}

	@JsonProperty("request")
	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	@JsonProperty("identity")
	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	@Override
	public String toString() {
		return "HeaderRequest [request=" + request + ", identity="
				+ identity + "]";
	}

	
}
