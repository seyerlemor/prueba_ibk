/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.AuditoriaUsuario;

import java.util.List;

/**
 * The persistence utility for the auditoria usuario service. This utility wraps {@link AuditoriaUsuarioPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see AuditoriaUsuarioPersistence
 * @see AuditoriaUsuarioPersistenceImpl
 * @generated
 */
public class AuditoriaUsuarioUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AuditoriaUsuario auditoriaUsuario) {
		getPersistence().clearCache(auditoriaUsuario);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AuditoriaUsuario> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AuditoriaUsuario> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AuditoriaUsuario> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static AuditoriaUsuario update(AuditoriaUsuario auditoriaUsuario)
		throws SystemException {
		return getPersistence().update(auditoriaUsuario);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static AuditoriaUsuario update(AuditoriaUsuario auditoriaUsuario,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(auditoriaUsuario, serviceContext);
	}

	/**
	* Returns the auditoria usuario where idAuditoriaUsuario = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException} if it could not be found.
	*
	* @param idAuditoriaUsuario the id auditoria usuario
	* @return the matching auditoria usuario
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException if a matching auditoria usuario could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario findByAU_ID(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException {
		return getPersistence().findByAU_ID(idAuditoriaUsuario);
	}

	/**
	* Returns the auditoria usuario where idAuditoriaUsuario = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idAuditoriaUsuario the id auditoria usuario
	* @return the matching auditoria usuario, or <code>null</code> if a matching auditoria usuario could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario fetchByAU_ID(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByAU_ID(idAuditoriaUsuario);
	}

	/**
	* Returns the auditoria usuario where idAuditoriaUsuario = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idAuditoriaUsuario the id auditoria usuario
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching auditoria usuario, or <code>null</code> if a matching auditoria usuario could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario fetchByAU_ID(
		long idAuditoriaUsuario, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAU_ID(idAuditoriaUsuario, retrieveFromCache);
	}

	/**
	* Removes the auditoria usuario where idAuditoriaUsuario = &#63; from the database.
	*
	* @param idAuditoriaUsuario the id auditoria usuario
	* @return the auditoria usuario that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario removeByAU_ID(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException {
		return getPersistence().removeByAU_ID(idAuditoriaUsuario);
	}

	/**
	* Returns the number of auditoria usuarios where idAuditoriaUsuario = &#63;.
	*
	* @param idAuditoriaUsuario the id auditoria usuario
	* @return the number of matching auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAU_ID(long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByAU_ID(idAuditoriaUsuario);
	}

	/**
	* Caches the auditoria usuario in the entity cache if it is enabled.
	*
	* @param auditoriaUsuario the auditoria usuario
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario) {
		getPersistence().cacheResult(auditoriaUsuario);
	}

	/**
	* Caches the auditoria usuarios in the entity cache if it is enabled.
	*
	* @param auditoriaUsuarios the auditoria usuarios
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.AuditoriaUsuario> auditoriaUsuarios) {
		getPersistence().cacheResult(auditoriaUsuarios);
	}

	/**
	* Creates a new auditoria usuario with the primary key. Does not add the auditoria usuario to the database.
	*
	* @param idAuditoriaUsuario the primary key for the new auditoria usuario
	* @return the new auditoria usuario
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario create(
		long idAuditoriaUsuario) {
		return getPersistence().create(idAuditoriaUsuario);
	}

	/**
	* Removes the auditoria usuario with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idAuditoriaUsuario the primary key of the auditoria usuario
	* @return the auditoria usuario that was removed
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException if a auditoria usuario with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario remove(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException {
		return getPersistence().remove(idAuditoriaUsuario);
	}

	public static pe.com.ibk.pepper.model.AuditoriaUsuario updateImpl(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(auditoriaUsuario);
	}

	/**
	* Returns the auditoria usuario with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException} if it could not be found.
	*
	* @param idAuditoriaUsuario the primary key of the auditoria usuario
	* @return the auditoria usuario
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException if a auditoria usuario with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario findByPrimaryKey(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException {
		return getPersistence().findByPrimaryKey(idAuditoriaUsuario);
	}

	/**
	* Returns the auditoria usuario with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idAuditoriaUsuario the primary key of the auditoria usuario
	* @return the auditoria usuario, or <code>null</code> if a auditoria usuario with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaUsuario fetchByPrimaryKey(
		long idAuditoriaUsuario)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idAuditoriaUsuario);
	}

	/**
	* Returns all the auditoria usuarios.
	*
	* @return the auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.AuditoriaUsuario> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the auditoria usuarios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaUsuarioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria usuarios
	* @param end the upper bound of the range of auditoria usuarios (not inclusive)
	* @return the range of auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.AuditoriaUsuario> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the auditoria usuarios.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaUsuarioModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria usuarios
	* @param end the upper bound of the range of auditoria usuarios (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.AuditoriaUsuario> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the auditoria usuarios from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of auditoria usuarios.
	*
	* @return the number of auditoria usuarios
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AuditoriaUsuarioPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AuditoriaUsuarioPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					AuditoriaUsuarioPersistence.class.getName());

			ReferenceRegistry.registerReference(AuditoriaUsuarioUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(AuditoriaUsuarioPersistence persistence) {
	}

	private static AuditoriaUsuarioPersistence _persistence;
}