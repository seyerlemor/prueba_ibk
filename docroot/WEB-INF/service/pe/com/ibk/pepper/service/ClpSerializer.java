/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import pe.com.ibk.pepper.model.AuditoriaClienteClp;
import pe.com.ibk.pepper.model.AuditoriaUsuarioClp;
import pe.com.ibk.pepper.model.CampaniaClp;
import pe.com.ibk.pepper.model.ClienteClp;
import pe.com.ibk.pepper.model.ClienteTransaccionClp;
import pe.com.ibk.pepper.model.DirecEstandarClp;
import pe.com.ibk.pepper.model.DireccionClienteClp;
import pe.com.ibk.pepper.model.ExpedienteClp;
import pe.com.ibk.pepper.model.LogQRadarClp;
import pe.com.ibk.pepper.model.ParametroHijoPOClp;
import pe.com.ibk.pepper.model.ParametroPadrePOClp;
import pe.com.ibk.pepper.model.PreguntasEquifaxClp;
import pe.com.ibk.pepper.model.ProductoClp;
import pe.com.ibk.pepper.model.ServiciosClp;
import pe.com.ibk.pepper.model.TblCodigoPromocionClp;
import pe.com.ibk.pepper.model.TblMaestroClp;
import pe.com.ibk.pepper.model.UbigeoClp;
import pe.com.ibk.pepper.model.UsuarioSessionClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Interbank
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"pepper-plugin-v2-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"pepper-plugin-v2-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "pepper-plugin-v2-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AuditoriaClienteClp.class.getName())) {
			return translateInputAuditoriaCliente(oldModel);
		}

		if (oldModelClassName.equals(AuditoriaUsuarioClp.class.getName())) {
			return translateInputAuditoriaUsuario(oldModel);
		}

		if (oldModelClassName.equals(CampaniaClp.class.getName())) {
			return translateInputCampania(oldModel);
		}

		if (oldModelClassName.equals(ClienteClp.class.getName())) {
			return translateInputCliente(oldModel);
		}

		if (oldModelClassName.equals(ClienteTransaccionClp.class.getName())) {
			return translateInputClienteTransaccion(oldModel);
		}

		if (oldModelClassName.equals(DireccionClienteClp.class.getName())) {
			return translateInputDireccionCliente(oldModel);
		}

		if (oldModelClassName.equals(DirecEstandarClp.class.getName())) {
			return translateInputDirecEstandar(oldModel);
		}

		if (oldModelClassName.equals(ExpedienteClp.class.getName())) {
			return translateInputExpediente(oldModel);
		}

		if (oldModelClassName.equals(LogQRadarClp.class.getName())) {
			return translateInputLogQRadar(oldModel);
		}

		if (oldModelClassName.equals(ParametroHijoPOClp.class.getName())) {
			return translateInputParametroHijoPO(oldModel);
		}

		if (oldModelClassName.equals(ParametroPadrePOClp.class.getName())) {
			return translateInputParametroPadrePO(oldModel);
		}

		if (oldModelClassName.equals(PreguntasEquifaxClp.class.getName())) {
			return translateInputPreguntasEquifax(oldModel);
		}

		if (oldModelClassName.equals(ProductoClp.class.getName())) {
			return translateInputProducto(oldModel);
		}

		if (oldModelClassName.equals(ServiciosClp.class.getName())) {
			return translateInputServicios(oldModel);
		}

		if (oldModelClassName.equals(TblCodigoPromocionClp.class.getName())) {
			return translateInputTblCodigoPromocion(oldModel);
		}

		if (oldModelClassName.equals(TblMaestroClp.class.getName())) {
			return translateInputTblMaestro(oldModel);
		}

		if (oldModelClassName.equals(UbigeoClp.class.getName())) {
			return translateInputUbigeo(oldModel);
		}

		if (oldModelClassName.equals(UsuarioSessionClp.class.getName())) {
			return translateInputUsuarioSession(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAuditoriaCliente(BaseModel<?> oldModel) {
		AuditoriaClienteClp oldClpModel = (AuditoriaClienteClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAuditoriaClienteRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAuditoriaUsuario(BaseModel<?> oldModel) {
		AuditoriaUsuarioClp oldClpModel = (AuditoriaUsuarioClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAuditoriaUsuarioRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCampania(BaseModel<?> oldModel) {
		CampaniaClp oldClpModel = (CampaniaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCampaniaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCliente(BaseModel<?> oldModel) {
		ClienteClp oldClpModel = (ClienteClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getClienteRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputClienteTransaccion(BaseModel<?> oldModel) {
		ClienteTransaccionClp oldClpModel = (ClienteTransaccionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getClienteTransaccionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDireccionCliente(BaseModel<?> oldModel) {
		DireccionClienteClp oldClpModel = (DireccionClienteClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDireccionClienteRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDirecEstandar(BaseModel<?> oldModel) {
		DirecEstandarClp oldClpModel = (DirecEstandarClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDirecEstandarRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputExpediente(BaseModel<?> oldModel) {
		ExpedienteClp oldClpModel = (ExpedienteClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getExpedienteRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLogQRadar(BaseModel<?> oldModel) {
		LogQRadarClp oldClpModel = (LogQRadarClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLogQRadarRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputParametroHijoPO(BaseModel<?> oldModel) {
		ParametroHijoPOClp oldClpModel = (ParametroHijoPOClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getParametroHijoPORemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputParametroPadrePO(BaseModel<?> oldModel) {
		ParametroPadrePOClp oldClpModel = (ParametroPadrePOClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getParametroPadrePORemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPreguntasEquifax(BaseModel<?> oldModel) {
		PreguntasEquifaxClp oldClpModel = (PreguntasEquifaxClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPreguntasEquifaxRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputProducto(BaseModel<?> oldModel) {
		ProductoClp oldClpModel = (ProductoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getProductoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputServicios(BaseModel<?> oldModel) {
		ServiciosClp oldClpModel = (ServiciosClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getServiciosRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTblCodigoPromocion(BaseModel<?> oldModel) {
		TblCodigoPromocionClp oldClpModel = (TblCodigoPromocionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTblCodigoPromocionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTblMaestro(BaseModel<?> oldModel) {
		TblMaestroClp oldClpModel = (TblMaestroClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTblMaestroRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUbigeo(BaseModel<?> oldModel) {
		UbigeoClp oldClpModel = (UbigeoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUbigeoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUsuarioSession(BaseModel<?> oldModel) {
		UsuarioSessionClp oldClpModel = (UsuarioSessionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUsuarioSessionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.AuditoriaClienteImpl")) {
			return translateOutputAuditoriaCliente(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.AuditoriaUsuarioImpl")) {
			return translateOutputAuditoriaUsuario(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.CampaniaImpl")) {
			return translateOutputCampania(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("pe.com.ibk.pepper.model.impl.ClienteImpl")) {
			return translateOutputCliente(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.ClienteTransaccionImpl")) {
			return translateOutputClienteTransaccion(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.DireccionClienteImpl")) {
			return translateOutputDireccionCliente(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.DirecEstandarImpl")) {
			return translateOutputDirecEstandar(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.ExpedienteImpl")) {
			return translateOutputExpediente(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.LogQRadarImpl")) {
			return translateOutputLogQRadar(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.ParametroHijoPOImpl")) {
			return translateOutputParametroHijoPO(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.ParametroPadrePOImpl")) {
			return translateOutputParametroPadrePO(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.PreguntasEquifaxImpl")) {
			return translateOutputPreguntasEquifax(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.ProductoImpl")) {
			return translateOutputProducto(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.ServiciosImpl")) {
			return translateOutputServicios(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.TblCodigoPromocionImpl")) {
			return translateOutputTblCodigoPromocion(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.TblMaestroImpl")) {
			return translateOutputTblMaestro(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("pe.com.ibk.pepper.model.impl.UbigeoImpl")) {
			return translateOutputUbigeo(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"pe.com.ibk.pepper.model.impl.UsuarioSessionImpl")) {
			return translateOutputUsuarioSession(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.DuplicateParametroPadreCodigoException")) {
			return new pe.com.ibk.pepper.DuplicateParametroPadreCodigoException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchAuditoriaClienteException")) {
			return new pe.com.ibk.pepper.NoSuchAuditoriaClienteException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException")) {
			return new pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchCampaniaException")) {
			return new pe.com.ibk.pepper.NoSuchCampaniaException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchClienteException")) {
			return new pe.com.ibk.pepper.NoSuchClienteException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchClienteTransaccionException")) {
			return new pe.com.ibk.pepper.NoSuchClienteTransaccionException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchDireccionClienteException")) {
			return new pe.com.ibk.pepper.NoSuchDireccionClienteException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchDirecEstandarException")) {
			return new pe.com.ibk.pepper.NoSuchDirecEstandarException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchExpedienteException")) {
			return new pe.com.ibk.pepper.NoSuchExpedienteException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchLogQRadarException")) {
			return new pe.com.ibk.pepper.NoSuchLogQRadarException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchParametroHijoPOException")) {
			return new pe.com.ibk.pepper.NoSuchParametroHijoPOException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchParametroPadrePOException")) {
			return new pe.com.ibk.pepper.NoSuchParametroPadrePOException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchPreguntasEquifaxException")) {
			return new pe.com.ibk.pepper.NoSuchPreguntasEquifaxException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchProductoException")) {
			return new pe.com.ibk.pepper.NoSuchProductoException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchServiciosException")) {
			return new pe.com.ibk.pepper.NoSuchServiciosException();
		}

		if (className.equals(
					"pe.com.ibk.pepper.NoSuchTblCodigoPromocionException")) {
			return new pe.com.ibk.pepper.NoSuchTblCodigoPromocionException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchTblMaestroException")) {
			return new pe.com.ibk.pepper.NoSuchTblMaestroException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchUbigeoException")) {
			return new pe.com.ibk.pepper.NoSuchUbigeoException();
		}

		if (className.equals("pe.com.ibk.pepper.NoSuchUsuarioSessionException")) {
			return new pe.com.ibk.pepper.NoSuchUsuarioSessionException();
		}

		return throwable;
	}

	public static Object translateOutputAuditoriaCliente(BaseModel<?> oldModel) {
		AuditoriaClienteClp newModel = new AuditoriaClienteClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAuditoriaClienteRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAuditoriaUsuario(BaseModel<?> oldModel) {
		AuditoriaUsuarioClp newModel = new AuditoriaUsuarioClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAuditoriaUsuarioRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCampania(BaseModel<?> oldModel) {
		CampaniaClp newModel = new CampaniaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCampaniaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCliente(BaseModel<?> oldModel) {
		ClienteClp newModel = new ClienteClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setClienteRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputClienteTransaccion(
		BaseModel<?> oldModel) {
		ClienteTransaccionClp newModel = new ClienteTransaccionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setClienteTransaccionRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDireccionCliente(BaseModel<?> oldModel) {
		DireccionClienteClp newModel = new DireccionClienteClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDireccionClienteRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDirecEstandar(BaseModel<?> oldModel) {
		DirecEstandarClp newModel = new DirecEstandarClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDirecEstandarRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputExpediente(BaseModel<?> oldModel) {
		ExpedienteClp newModel = new ExpedienteClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setExpedienteRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLogQRadar(BaseModel<?> oldModel) {
		LogQRadarClp newModel = new LogQRadarClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLogQRadarRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputParametroHijoPO(BaseModel<?> oldModel) {
		ParametroHijoPOClp newModel = new ParametroHijoPOClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setParametroHijoPORemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputParametroPadrePO(BaseModel<?> oldModel) {
		ParametroPadrePOClp newModel = new ParametroPadrePOClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setParametroPadrePORemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPreguntasEquifax(BaseModel<?> oldModel) {
		PreguntasEquifaxClp newModel = new PreguntasEquifaxClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPreguntasEquifaxRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputProducto(BaseModel<?> oldModel) {
		ProductoClp newModel = new ProductoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setProductoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputServicios(BaseModel<?> oldModel) {
		ServiciosClp newModel = new ServiciosClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setServiciosRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTblCodigoPromocion(
		BaseModel<?> oldModel) {
		TblCodigoPromocionClp newModel = new TblCodigoPromocionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTblCodigoPromocionRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTblMaestro(BaseModel<?> oldModel) {
		TblMaestroClp newModel = new TblMaestroClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTblMaestroRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUbigeo(BaseModel<?> oldModel) {
		UbigeoClp newModel = new UbigeoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUbigeoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUsuarioSession(BaseModel<?> oldModel) {
		UsuarioSessionClp newModel = new UsuarioSessionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUsuarioSessionRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}