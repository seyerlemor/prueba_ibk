/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author Interbank
 */
public class UbigeoFinderUtil {
	public static pe.com.ibk.pepper.model.Ubigeo obtenerUbigeoByNombre(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, java.lang.String nombre, int tipo) {
		return getFinder()
				   .obtenerUbigeoByNombre(codDepartamento, codProvincia,
			codDistrito, nombre, tipo);
	}

	public static java.lang.String obtenerUbigeoByCodigo(
		java.lang.String codigo) {
		return getFinder().obtenerUbigeoByCodigo(codigo);
	}

	public static UbigeoFinder getFinder() {
		if (_finder == null) {
			_finder = (UbigeoFinder)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					UbigeoFinder.class.getName());

			ReferenceRegistry.registerReference(UbigeoFinderUtil.class,
				"_finder");
		}

		return _finder;
	}

	public void setFinder(UbigeoFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(UbigeoFinderUtil.class, "_finder");
	}

	private static UbigeoFinder _finder;
}