/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchPreguntasEquifaxException;
import pe.com.ibk.pepper.model.PreguntasEquifax;
import pe.com.ibk.pepper.model.impl.PreguntasEquifaxImpl;
import pe.com.ibk.pepper.model.impl.PreguntasEquifaxModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the preguntas equifax service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see PreguntasEquifaxPersistence
 * @see PreguntasEquifaxUtil
 * @generated
 */
public class PreguntasEquifaxPersistenceImpl extends BasePersistenceImpl<PreguntasEquifax>
	implements PreguntasEquifaxPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PreguntasEquifaxUtil} to access the preguntas equifax persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PreguntasEquifaxImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxModelImpl.FINDER_CACHE_ENABLED,
			PreguntasEquifaxImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxModelImpl.FINDER_CACHE_ENABLED,
			PreguntasEquifaxImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_PE_ID = new FinderPath(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxModelImpl.FINDER_CACHE_ENABLED,
			PreguntasEquifaxImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByPE_ID", new String[] { Long.class.getName() },
			PreguntasEquifaxModelImpl.IDPREGUNTA_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PE_ID = new FinderPath(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPE_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the preguntas equifax where idPregunta = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchPreguntasEquifaxException} if it could not be found.
	 *
	 * @param idPregunta the id pregunta
	 * @return the matching preguntas equifax
	 * @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a matching preguntas equifax could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax findByPE_ID(long idPregunta)
		throws NoSuchPreguntasEquifaxException, SystemException {
		PreguntasEquifax preguntasEquifax = fetchByPE_ID(idPregunta);

		if (preguntasEquifax == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idPregunta=");
			msg.append(idPregunta);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPreguntasEquifaxException(msg.toString());
		}

		return preguntasEquifax;
	}

	/**
	 * Returns the preguntas equifax where idPregunta = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idPregunta the id pregunta
	 * @return the matching preguntas equifax, or <code>null</code> if a matching preguntas equifax could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax fetchByPE_ID(long idPregunta)
		throws SystemException {
		return fetchByPE_ID(idPregunta, true);
	}

	/**
	 * Returns the preguntas equifax where idPregunta = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idPregunta the id pregunta
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching preguntas equifax, or <code>null</code> if a matching preguntas equifax could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax fetchByPE_ID(long idPregunta,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idPregunta };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PE_ID,
					finderArgs, this);
		}

		if (result instanceof PreguntasEquifax) {
			PreguntasEquifax preguntasEquifax = (PreguntasEquifax)result;

			if ((idPregunta != preguntasEquifax.getIdPregunta())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PREGUNTASEQUIFAX_WHERE);

			query.append(_FINDER_COLUMN_PE_ID_IDPREGUNTA_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idPregunta);

				List<PreguntasEquifax> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PE_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PreguntasEquifaxPersistenceImpl.fetchByPE_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PreguntasEquifax preguntasEquifax = list.get(0);

					result = preguntasEquifax;

					cacheResult(preguntasEquifax);

					if ((preguntasEquifax.getIdPregunta() != idPregunta)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PE_ID,
							finderArgs, preguntasEquifax);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PE_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PreguntasEquifax)result;
		}
	}

	/**
	 * Removes the preguntas equifax where idPregunta = &#63; from the database.
	 *
	 * @param idPregunta the id pregunta
	 * @return the preguntas equifax that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax removeByPE_ID(long idPregunta)
		throws NoSuchPreguntasEquifaxException, SystemException {
		PreguntasEquifax preguntasEquifax = findByPE_ID(idPregunta);

		return remove(preguntasEquifax);
	}

	/**
	 * Returns the number of preguntas equifaxs where idPregunta = &#63;.
	 *
	 * @param idPregunta the id pregunta
	 * @return the number of matching preguntas equifaxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPE_ID(long idPregunta) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PE_ID;

		Object[] finderArgs = new Object[] { idPregunta };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PREGUNTASEQUIFAX_WHERE);

			query.append(_FINDER_COLUMN_PE_ID_IDPREGUNTA_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idPregunta);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PE_ID_IDPREGUNTA_2 = "preguntasEquifax.idPregunta = ?";

	public PreguntasEquifaxPersistenceImpl() {
		setModelClass(PreguntasEquifax.class);
	}

	/**
	 * Caches the preguntas equifax in the entity cache if it is enabled.
	 *
	 * @param preguntasEquifax the preguntas equifax
	 */
	@Override
	public void cacheResult(PreguntasEquifax preguntasEquifax) {
		EntityCacheUtil.putResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxImpl.class, preguntasEquifax.getPrimaryKey(),
			preguntasEquifax);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PE_ID,
			new Object[] { preguntasEquifax.getIdPregunta() }, preguntasEquifax);

		preguntasEquifax.resetOriginalValues();
	}

	/**
	 * Caches the preguntas equifaxs in the entity cache if it is enabled.
	 *
	 * @param preguntasEquifaxs the preguntas equifaxs
	 */
	@Override
	public void cacheResult(List<PreguntasEquifax> preguntasEquifaxs) {
		for (PreguntasEquifax preguntasEquifax : preguntasEquifaxs) {
			if (EntityCacheUtil.getResult(
						PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
						PreguntasEquifaxImpl.class,
						preguntasEquifax.getPrimaryKey()) == null) {
				cacheResult(preguntasEquifax);
			}
			else {
				preguntasEquifax.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all preguntas equifaxs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PreguntasEquifaxImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PreguntasEquifaxImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the preguntas equifax.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PreguntasEquifax preguntasEquifax) {
		EntityCacheUtil.removeResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxImpl.class, preguntasEquifax.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(preguntasEquifax);
	}

	@Override
	public void clearCache(List<PreguntasEquifax> preguntasEquifaxs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PreguntasEquifax preguntasEquifax : preguntasEquifaxs) {
			EntityCacheUtil.removeResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
				PreguntasEquifaxImpl.class, preguntasEquifax.getPrimaryKey());

			clearUniqueFindersCache(preguntasEquifax);
		}
	}

	protected void cacheUniqueFindersCache(PreguntasEquifax preguntasEquifax) {
		if (preguntasEquifax.isNew()) {
			Object[] args = new Object[] { preguntasEquifax.getIdPregunta() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PE_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PE_ID, args,
				preguntasEquifax);
		}
		else {
			PreguntasEquifaxModelImpl preguntasEquifaxModelImpl = (PreguntasEquifaxModelImpl)preguntasEquifax;

			if ((preguntasEquifaxModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PE_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { preguntasEquifax.getIdPregunta() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PE_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PE_ID, args,
					preguntasEquifax);
			}
		}
	}

	protected void clearUniqueFindersCache(PreguntasEquifax preguntasEquifax) {
		PreguntasEquifaxModelImpl preguntasEquifaxModelImpl = (PreguntasEquifaxModelImpl)preguntasEquifax;

		Object[] args = new Object[] { preguntasEquifax.getIdPregunta() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PE_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PE_ID, args);

		if ((preguntasEquifaxModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PE_ID.getColumnBitmask()) != 0) {
			args = new Object[] {
					preguntasEquifaxModelImpl.getOriginalIdPregunta()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PE_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PE_ID, args);
		}
	}

	/**
	 * Creates a new preguntas equifax with the primary key. Does not add the preguntas equifax to the database.
	 *
	 * @param idPregunta the primary key for the new preguntas equifax
	 * @return the new preguntas equifax
	 */
	@Override
	public PreguntasEquifax create(long idPregunta) {
		PreguntasEquifax preguntasEquifax = new PreguntasEquifaxImpl();

		preguntasEquifax.setNew(true);
		preguntasEquifax.setPrimaryKey(idPregunta);

		return preguntasEquifax;
	}

	/**
	 * Removes the preguntas equifax with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idPregunta the primary key of the preguntas equifax
	 * @return the preguntas equifax that was removed
	 * @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a preguntas equifax with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax remove(long idPregunta)
		throws NoSuchPreguntasEquifaxException, SystemException {
		return remove((Serializable)idPregunta);
	}

	/**
	 * Removes the preguntas equifax with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the preguntas equifax
	 * @return the preguntas equifax that was removed
	 * @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a preguntas equifax with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax remove(Serializable primaryKey)
		throws NoSuchPreguntasEquifaxException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PreguntasEquifax preguntasEquifax = (PreguntasEquifax)session.get(PreguntasEquifaxImpl.class,
					primaryKey);

			if (preguntasEquifax == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPreguntasEquifaxException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(preguntasEquifax);
		}
		catch (NoSuchPreguntasEquifaxException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PreguntasEquifax removeImpl(PreguntasEquifax preguntasEquifax)
		throws SystemException {
		preguntasEquifax = toUnwrappedModel(preguntasEquifax);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(preguntasEquifax)) {
				preguntasEquifax = (PreguntasEquifax)session.get(PreguntasEquifaxImpl.class,
						preguntasEquifax.getPrimaryKeyObj());
			}

			if (preguntasEquifax != null) {
				session.delete(preguntasEquifax);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (preguntasEquifax != null) {
			clearCache(preguntasEquifax);
		}

		return preguntasEquifax;
	}

	@Override
	public PreguntasEquifax updateImpl(
		pe.com.ibk.pepper.model.PreguntasEquifax preguntasEquifax)
		throws SystemException {
		preguntasEquifax = toUnwrappedModel(preguntasEquifax);

		boolean isNew = preguntasEquifax.isNew();

		Session session = null;

		try {
			session = openSession();

			if (preguntasEquifax.isNew()) {
				session.save(preguntasEquifax);

				preguntasEquifax.setNew(false);
			}
			else {
				session.merge(preguntasEquifax);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PreguntasEquifaxModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
			PreguntasEquifaxImpl.class, preguntasEquifax.getPrimaryKey(),
			preguntasEquifax);

		clearUniqueFindersCache(preguntasEquifax);
		cacheUniqueFindersCache(preguntasEquifax);

		return preguntasEquifax;
	}

	protected PreguntasEquifax toUnwrappedModel(
		PreguntasEquifax preguntasEquifax) {
		if (preguntasEquifax instanceof PreguntasEquifaxImpl) {
			return preguntasEquifax;
		}

		PreguntasEquifaxImpl preguntasEquifaxImpl = new PreguntasEquifaxImpl();

		preguntasEquifaxImpl.setNew(preguntasEquifax.isNew());
		preguntasEquifaxImpl.setPrimaryKey(preguntasEquifax.getPrimaryKey());

		preguntasEquifaxImpl.setIdPregunta(preguntasEquifax.getIdPregunta());
		preguntasEquifaxImpl.setIdCabecera(preguntasEquifax.getIdCabecera());
		preguntasEquifaxImpl.setCodigoPregunta(preguntasEquifax.getCodigoPregunta());
		preguntasEquifaxImpl.setDescripPregunta(preguntasEquifax.getDescripPregunta());
		preguntasEquifaxImpl.setCodigoRespuesta(preguntasEquifax.getCodigoRespuesta());
		preguntasEquifaxImpl.setDescripRespuesta(preguntasEquifax.getDescripRespuesta());
		preguntasEquifaxImpl.setFechaRegistro(preguntasEquifax.getFechaRegistro());

		return preguntasEquifaxImpl;
	}

	/**
	 * Returns the preguntas equifax with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the preguntas equifax
	 * @return the preguntas equifax
	 * @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a preguntas equifax with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPreguntasEquifaxException, SystemException {
		PreguntasEquifax preguntasEquifax = fetchByPrimaryKey(primaryKey);

		if (preguntasEquifax == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPreguntasEquifaxException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return preguntasEquifax;
	}

	/**
	 * Returns the preguntas equifax with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchPreguntasEquifaxException} if it could not be found.
	 *
	 * @param idPregunta the primary key of the preguntas equifax
	 * @return the preguntas equifax
	 * @throws pe.com.ibk.pepper.NoSuchPreguntasEquifaxException if a preguntas equifax with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax findByPrimaryKey(long idPregunta)
		throws NoSuchPreguntasEquifaxException, SystemException {
		return findByPrimaryKey((Serializable)idPregunta);
	}

	/**
	 * Returns the preguntas equifax with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the preguntas equifax
	 * @return the preguntas equifax, or <code>null</code> if a preguntas equifax with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PreguntasEquifax preguntasEquifax = (PreguntasEquifax)EntityCacheUtil.getResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
				PreguntasEquifaxImpl.class, primaryKey);

		if (preguntasEquifax == _nullPreguntasEquifax) {
			return null;
		}

		if (preguntasEquifax == null) {
			Session session = null;

			try {
				session = openSession();

				preguntasEquifax = (PreguntasEquifax)session.get(PreguntasEquifaxImpl.class,
						primaryKey);

				if (preguntasEquifax != null) {
					cacheResult(preguntasEquifax);
				}
				else {
					EntityCacheUtil.putResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
						PreguntasEquifaxImpl.class, primaryKey,
						_nullPreguntasEquifax);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PreguntasEquifaxModelImpl.ENTITY_CACHE_ENABLED,
					PreguntasEquifaxImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return preguntasEquifax;
	}

	/**
	 * Returns the preguntas equifax with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idPregunta the primary key of the preguntas equifax
	 * @return the preguntas equifax, or <code>null</code> if a preguntas equifax with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PreguntasEquifax fetchByPrimaryKey(long idPregunta)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idPregunta);
	}

	/**
	 * Returns all the preguntas equifaxs.
	 *
	 * @return the preguntas equifaxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PreguntasEquifax> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the preguntas equifaxs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.PreguntasEquifaxModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of preguntas equifaxs
	 * @param end the upper bound of the range of preguntas equifaxs (not inclusive)
	 * @return the range of preguntas equifaxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PreguntasEquifax> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the preguntas equifaxs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.PreguntasEquifaxModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of preguntas equifaxs
	 * @param end the upper bound of the range of preguntas equifaxs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of preguntas equifaxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PreguntasEquifax> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PreguntasEquifax> list = (List<PreguntasEquifax>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PREGUNTASEQUIFAX);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PREGUNTASEQUIFAX;

				if (pagination) {
					sql = sql.concat(PreguntasEquifaxModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PreguntasEquifax>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PreguntasEquifax>(list);
				}
				else {
					list = (List<PreguntasEquifax>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the preguntas equifaxs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PreguntasEquifax preguntasEquifax : findAll()) {
			remove(preguntasEquifax);
		}
	}

	/**
	 * Returns the number of preguntas equifaxs.
	 *
	 * @return the number of preguntas equifaxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PREGUNTASEQUIFAX);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the preguntas equifax persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.PreguntasEquifax")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PreguntasEquifax>> listenersList = new ArrayList<ModelListener<PreguntasEquifax>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PreguntasEquifax>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PreguntasEquifaxImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PREGUNTASEQUIFAX = "SELECT preguntasEquifax FROM PreguntasEquifax preguntasEquifax";
	private static final String _SQL_SELECT_PREGUNTASEQUIFAX_WHERE = "SELECT preguntasEquifax FROM PreguntasEquifax preguntasEquifax WHERE ";
	private static final String _SQL_COUNT_PREGUNTASEQUIFAX = "SELECT COUNT(preguntasEquifax) FROM PreguntasEquifax preguntasEquifax";
	private static final String _SQL_COUNT_PREGUNTASEQUIFAX_WHERE = "SELECT COUNT(preguntasEquifax) FROM PreguntasEquifax preguntasEquifax WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "preguntasEquifax.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PreguntasEquifax exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PreguntasEquifax exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PreguntasEquifaxPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idPregunta", "idCabecera", "codigoPregunta", "descripPregunta",
				"codigoRespuesta", "descripRespuesta", "fechaRegistro"
			});
	private static PreguntasEquifax _nullPreguntasEquifax = new PreguntasEquifaxImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PreguntasEquifax> toCacheModel() {
				return _nullPreguntasEquifaxCacheModel;
			}
		};

	private static CacheModel<PreguntasEquifax> _nullPreguntasEquifaxCacheModel = new CacheModel<PreguntasEquifax>() {
			@Override
			public PreguntasEquifax toEntityModel() {
				return _nullPreguntasEquifax;
			}
		};
}