
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Evaluation {

    private List<Customer> customers;
    private Product product;
    
    @JsonCreator
	public Evaluation(List<Customer> customers, Product product) {
		super();
		this.customers = customers;
		this.product = product;
	}

    @JsonProperty("customers")
	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	@JsonProperty("product")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
