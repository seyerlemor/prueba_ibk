/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class DireccionClienteSoap implements Serializable {
	public static DireccionClienteSoap toSoapModel(DireccionCliente model) {
		DireccionClienteSoap soapModel = new DireccionClienteSoap();

		soapModel.setIdDireccion(model.getIdDireccion());
		soapModel.setIdDatoCliente(model.getIdDatoCliente());
		soapModel.setTipoDireccion(model.getTipoDireccion());
		soapModel.setCodigoUso(model.getCodigoUso());
		soapModel.setFlagDireccionEstandar(model.getFlagDireccionEstandar());
		soapModel.setVia(model.getVia());
		soapModel.setDepartamento(model.getDepartamento());
		soapModel.setProvincia(model.getProvincia());
		soapModel.setDistrito(model.getDistrito());
		soapModel.setPais(model.getPais());
		soapModel.setUbigeo(model.getUbigeo());
		soapModel.setCodigoPostal(model.getCodigoPostal());
		soapModel.setEstado(model.getEstado());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static DireccionClienteSoap[] toSoapModels(DireccionCliente[] models) {
		DireccionClienteSoap[] soapModels = new DireccionClienteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DireccionClienteSoap[][] toSoapModels(
		DireccionCliente[][] models) {
		DireccionClienteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DireccionClienteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DireccionClienteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DireccionClienteSoap[] toSoapModels(
		List<DireccionCliente> models) {
		List<DireccionClienteSoap> soapModels = new ArrayList<DireccionClienteSoap>(models.size());

		for (DireccionCliente model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DireccionClienteSoap[soapModels.size()]);
	}

	public DireccionClienteSoap() {
	}

	public long getPrimaryKey() {
		return _idDireccion;
	}

	public void setPrimaryKey(long pk) {
		setIdDireccion(pk);
	}

	public long getIdDireccion() {
		return _idDireccion;
	}

	public void setIdDireccion(long idDireccion) {
		_idDireccion = idDireccion;
	}

	public long getIdDatoCliente() {
		return _idDatoCliente;
	}

	public void setIdDatoCliente(long idDatoCliente) {
		_idDatoCliente = idDatoCliente;
	}

	public String getTipoDireccion() {
		return _tipoDireccion;
	}

	public void setTipoDireccion(String tipoDireccion) {
		_tipoDireccion = tipoDireccion;
	}

	public String getCodigoUso() {
		return _codigoUso;
	}

	public void setCodigoUso(String codigoUso) {
		_codigoUso = codigoUso;
	}

	public String getFlagDireccionEstandar() {
		return _flagDireccionEstandar;
	}

	public void setFlagDireccionEstandar(String flagDireccionEstandar) {
		_flagDireccionEstandar = flagDireccionEstandar;
	}

	public String getVia() {
		return _via;
	}

	public void setVia(String via) {
		_via = via;
	}

	public String getDepartamento() {
		return _departamento;
	}

	public void setDepartamento(String departamento) {
		_departamento = departamento;
	}

	public String getProvincia() {
		return _provincia;
	}

	public void setProvincia(String provincia) {
		_provincia = provincia;
	}

	public String getDistrito() {
		return _distrito;
	}

	public void setDistrito(String distrito) {
		_distrito = distrito;
	}

	public String getPais() {
		return _pais;
	}

	public void setPais(String pais) {
		_pais = pais;
	}

	public String getUbigeo() {
		return _ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		_ubigeo = ubigeo;
	}

	public String getCodigoPostal() {
		return _codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		_codigoPostal = codigoPostal;
	}

	public String getEstado() {
		return _estado;
	}

	public void setEstado(String estado) {
		_estado = estado;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private long _idDireccion;
	private long _idDatoCliente;
	private String _tipoDireccion;
	private String _codigoUso;
	private String _flagDireccionEstandar;
	private String _via;
	private String _departamento;
	private String _provincia;
	private String _distrito;
	private String _pais;
	private String _ubigeo;
	private String _codigoPostal;
	private String _estado;
	private Date _fechaRegistro;
}