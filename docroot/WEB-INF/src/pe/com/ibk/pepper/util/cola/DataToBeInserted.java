	package pe.com.ibk.pepper.util.cola;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.gestion.GestorRegistros;
import pe.com.ibk.pepper.util.Constantes;

public class DataToBeInserted implements EnqueueableData {

	private Integer tabla;
	private SolicitudBean pasoBean;
	GestorRegistros gestorRegistros;
	private static final Log _log = LogFactoryUtil.getLog(DataToBeInserted.class);
	
	public DataToBeInserted(Integer tabla, SolicitudBean pasoBean) {
		super();
		this.tabla = tabla;
		this.pasoBean = pasoBean;
		
	}

	/**
	 * @return the field_1
	 */
	public Integer getTabla() {
		return tabla;
	}

	/**
	 * @param tabla
	 *            the tabla to set
	 */
	public void setTabla(Integer tabla) {
		this.tabla = tabla;
	}

	/**
	 * @return the PasoBean
	 */
	public SolicitudBean getPasoBean() {
		return pasoBean;
	}

	/**
	 * @param SolicitudBean
	 *            the PasoBean to set
	 */
	public void setPasoBean(SolicitudBean pasoBean) {
		this.pasoBean = pasoBean;
	}

	@Override
	public void execute() {
		if(Constantes.T_SERVICIOS==tabla)
			getGestorRegistros().registrarServicios(pasoBean);
		_log.debug("Guardo en tabla: "+ tabla);
	}

	public GestorRegistros getGestorRegistros() {
		if(gestorRegistros == null){
			gestorRegistros = new GestorRegistros();
		}
		return gestorRegistros;
	}

	public void setGestorRegistros(GestorRegistros gestorRegistros) {
		this.gestorRegistros = gestorRegistros;
	}
	
	

}
