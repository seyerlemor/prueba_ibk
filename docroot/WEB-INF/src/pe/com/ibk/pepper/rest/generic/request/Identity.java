package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Identity {

    private String netId; 
    private String userId;   
    private String supervisorId;  
    private String deviceId;
    private String serverId; 
    private String branchCode;
        
    @JsonCreator
    public Identity() {
		super();
	}

	@JsonCreator
	public Identity(String netId, String userId, String supervisorId,
			String deviceId, String serverId, String branchCode) {
		super();
		this.netId = netId;
		this.userId = userId;
		this.supervisorId = supervisorId;
		this.deviceId = deviceId;
		this.serverId = serverId;
		this.branchCode = branchCode;
	}

    @JsonProperty("netId")
	public String getNetId() {
        return netId;
    }

    public void setNetId(String netId) {
        this.netId = netId;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("supervisorId")
    public String getSupervisorId() {
        return supervisorId;
    }
   
    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }
   
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("serverId")
    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    @JsonProperty("branchCode")
    public String getBranchCode() {
        return branchCode;
    }

  
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

	@Override
	public String toString() {
		return "Identity [netId=" + netId + ", userId=" + userId
				+ ", supervisorId=" + supervisorId + ", deviceId=" + deviceId
				+ ", serverId=" + serverId + ", branchCode=" + branchCode + "]";
	}

}
