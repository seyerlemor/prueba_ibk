/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ServiciosLocalService}.
 *
 * @author Interbank
 * @see ServiciosLocalService
 * @generated
 */
public class ServiciosLocalServiceWrapper implements ServiciosLocalService,
	ServiceWrapper<ServiciosLocalService> {
	public ServiciosLocalServiceWrapper(
		ServiciosLocalService serviciosLocalService) {
		_serviciosLocalService = serviciosLocalService;
	}

	/**
	* Adds the servicios to the database. Also notifies the appropriate model listeners.
	*
	* @param servicios the servicios
	* @return the servicios that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Servicios addServicios(
		pe.com.ibk.pepper.model.Servicios servicios)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.addServicios(servicios);
	}

	/**
	* Creates a new servicios with the primary key. Does not add the servicios to the database.
	*
	* @param idServicio the primary key for the new servicios
	* @return the new servicios
	*/
	@Override
	public pe.com.ibk.pepper.model.Servicios createServicios(long idServicio) {
		return _serviciosLocalService.createServicios(idServicio);
	}

	/**
	* Deletes the servicios with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios that was removed
	* @throws PortalException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Servicios deleteServicios(long idServicio)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.deleteServicios(idServicio);
	}

	/**
	* Deletes the servicios from the database. Also notifies the appropriate model listeners.
	*
	* @param servicios the servicios
	* @return the servicios that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Servicios deleteServicios(
		pe.com.ibk.pepper.model.Servicios servicios)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.deleteServicios(servicios);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _serviciosLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public pe.com.ibk.pepper.model.Servicios fetchServicios(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.fetchServicios(idServicio);
	}

	/**
	* Returns the servicios with the primary key.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios
	* @throws PortalException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Servicios getServicios(long idServicio)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.getServicios(idServicio);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the servicioses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @return the range of servicioses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.Servicios> getServicioses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.getServicioses(start, end);
	}

	/**
	* Returns the number of servicioses.
	*
	* @return the number of servicioses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getServiciosesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.getServiciosesCount();
	}

	/**
	* Updates the servicios in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param servicios the servicios
	* @return the servicios that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Servicios updateServicios(
		pe.com.ibk.pepper.model.Servicios servicios)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.updateServicios(servicios);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _serviciosLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_serviciosLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _serviciosLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public pe.com.ibk.pepper.model.Servicios registrarServicios(
		pe.com.ibk.pepper.model.Servicios servicios)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.registrarServicios(servicios);
	}

	@Override
	public pe.com.ibk.pepper.model.Servicios buscarServiciosporId(
		long idServicios) {
		return _serviciosLocalService.buscarServiciosporId(idServicios);
	}

	@Override
	public java.util.List<pe.com.ibk.pepper.model.Servicios> getServiciosByExpediente(
		long idExpediente, java.lang.String tipoFlujo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _serviciosLocalService.getServiciosByExpediente(idExpediente,
			tipoFlujo);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ServiciosLocalService getWrappedServiciosLocalService() {
		return _serviciosLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedServiciosLocalService(
		ServiciosLocalService serviciosLocalService) {
		_serviciosLocalService = serviciosLocalService;
	}

	@Override
	public ServiciosLocalService getWrappedService() {
		return _serviciosLocalService;
	}

	@Override
	public void setWrappedService(ServiciosLocalService serviciosLocalService) {
		_serviciosLocalService = serviciosLocalService;
	}

	private ServiciosLocalService _serviciosLocalService;
}