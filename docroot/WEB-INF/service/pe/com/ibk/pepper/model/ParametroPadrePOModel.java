/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.GroupedModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the ParametroPadrePO service. Represents a row in the &quot;T_PARAMETRO_PRINCIPAL&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOImpl}.
 * </p>
 *
 * @author Interbank
 * @see ParametroPadrePO
 * @see pe.com.ibk.pepper.model.impl.ParametroPadrePOImpl
 * @see pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl
 * @generated
 */
public interface ParametroPadrePOModel extends BaseModel<ParametroPadrePO>,
	GroupedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a parametro padre p o model instance should use the {@link ParametroPadrePO} interface instead.
	 */

	/**
	 * Returns the primary key of this parametro padre p o.
	 *
	 * @return the primary key of this parametro padre p o
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this parametro padre p o.
	 *
	 * @param primaryKey the primary key of this parametro padre p o
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the id parametro padre of this parametro padre p o.
	 *
	 * @return the id parametro padre of this parametro padre p o
	 */
	public long getIdParametroPadre();

	/**
	 * Sets the id parametro padre of this parametro padre p o.
	 *
	 * @param idParametroPadre the id parametro padre of this parametro padre p o
	 */
	public void setIdParametroPadre(long idParametroPadre);

	/**
	 * Returns the group ID of this parametro padre p o.
	 *
	 * @return the group ID of this parametro padre p o
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this parametro padre p o.
	 *
	 * @param groupId the group ID of this parametro padre p o
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this parametro padre p o.
	 *
	 * @return the company ID of this parametro padre p o
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this parametro padre p o.
	 *
	 * @param companyId the company ID of this parametro padre p o
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this parametro padre p o.
	 *
	 * @return the user ID of this parametro padre p o
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this parametro padre p o.
	 *
	 * @param userId the user ID of this parametro padre p o
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this parametro padre p o.
	 *
	 * @return the user uuid of this parametro padre p o
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this parametro padre p o.
	 *
	 * @param userUuid the user uuid of this parametro padre p o
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this parametro padre p o.
	 *
	 * @return the user name of this parametro padre p o
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this parametro padre p o.
	 *
	 * @param userName the user name of this parametro padre p o
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this parametro padre p o.
	 *
	 * @return the create date of this parametro padre p o
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this parametro padre p o.
	 *
	 * @param createDate the create date of this parametro padre p o
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this parametro padre p o.
	 *
	 * @return the modified date of this parametro padre p o
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this parametro padre p o.
	 *
	 * @param modifiedDate the modified date of this parametro padre p o
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the codigo padre of this parametro padre p o.
	 *
	 * @return the codigo padre of this parametro padre p o
	 */
	@AutoEscape
	public String getCodigoPadre();

	/**
	 * Sets the codigo padre of this parametro padre p o.
	 *
	 * @param codigoPadre the codigo padre of this parametro padre p o
	 */
	public void setCodigoPadre(String codigoPadre);

	/**
	 * Returns the nombre of this parametro padre p o.
	 *
	 * @return the nombre of this parametro padre p o
	 */
	@AutoEscape
	public String getNombre();

	/**
	 * Sets the nombre of this parametro padre p o.
	 *
	 * @param nombre the nombre of this parametro padre p o
	 */
	public void setNombre(String nombre);

	/**
	 * Returns the descripcion of this parametro padre p o.
	 *
	 * @return the descripcion of this parametro padre p o
	 */
	@AutoEscape
	public String getDescripcion();

	/**
	 * Sets the descripcion of this parametro padre p o.
	 *
	 * @param descripcion the descripcion of this parametro padre p o
	 */
	public void setDescripcion(String descripcion);

	/**
	 * Returns the estado of this parametro padre p o.
	 *
	 * @return the estado of this parametro padre p o
	 */
	public boolean getEstado();

	/**
	 * Returns <code>true</code> if this parametro padre p o is estado.
	 *
	 * @return <code>true</code> if this parametro padre p o is estado; <code>false</code> otherwise
	 */
	public boolean isEstado();

	/**
	 * Sets whether this parametro padre p o is estado.
	 *
	 * @param estado the estado of this parametro padre p o
	 */
	public void setEstado(boolean estado);

	/**
	 * Returns the json of this parametro padre p o.
	 *
	 * @return the json of this parametro padre p o
	 */
	@AutoEscape
	public String getJson();

	/**
	 * Sets the json of this parametro padre p o.
	 *
	 * @param json the json of this parametro padre p o
	 */
	public void setJson(String json);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO);

	@Override
	public int hashCode();

	@Override
	public CacheModel<pe.com.ibk.pepper.model.ParametroPadrePO> toCacheModel();

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO toEscapedModel();

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}