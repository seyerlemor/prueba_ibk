/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class DirecEstandarSoap implements Serializable {
	public static DirecEstandarSoap toSoapModel(DirecEstandar model) {
		DirecEstandarSoap soapModel = new DirecEstandarSoap();

		soapModel.setIdDetalleDireccion(model.getIdDetalleDireccion());
		soapModel.setIdDireccion(model.getIdDireccion());
		soapModel.setTipoVia(model.getTipoVia());
		soapModel.setNombreVia(model.getNombreVia());
		soapModel.setNumero(model.getNumero());
		soapModel.setManzana(model.getManzana());
		soapModel.setPisoLote(model.getPisoLote());
		soapModel.setInterior(model.getInterior());
		soapModel.setUrbanizacion(model.getUrbanizacion());
		soapModel.setReferencia(model.getReferencia());

		return soapModel;
	}

	public static DirecEstandarSoap[] toSoapModels(DirecEstandar[] models) {
		DirecEstandarSoap[] soapModels = new DirecEstandarSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DirecEstandarSoap[][] toSoapModels(DirecEstandar[][] models) {
		DirecEstandarSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DirecEstandarSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DirecEstandarSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DirecEstandarSoap[] toSoapModels(List<DirecEstandar> models) {
		List<DirecEstandarSoap> soapModels = new ArrayList<DirecEstandarSoap>(models.size());

		for (DirecEstandar model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DirecEstandarSoap[soapModels.size()]);
	}

	public DirecEstandarSoap() {
	}

	public long getPrimaryKey() {
		return _idDetalleDireccion;
	}

	public void setPrimaryKey(long pk) {
		setIdDetalleDireccion(pk);
	}

	public long getIdDetalleDireccion() {
		return _idDetalleDireccion;
	}

	public void setIdDetalleDireccion(long idDetalleDireccion) {
		_idDetalleDireccion = idDetalleDireccion;
	}

	public long getIdDireccion() {
		return _idDireccion;
	}

	public void setIdDireccion(long idDireccion) {
		_idDireccion = idDireccion;
	}

	public String getTipoVia() {
		return _tipoVia;
	}

	public void setTipoVia(String tipoVia) {
		_tipoVia = tipoVia;
	}

	public String getNombreVia() {
		return _nombreVia;
	}

	public void setNombreVia(String nombreVia) {
		_nombreVia = nombreVia;
	}

	public String getNumero() {
		return _numero;
	}

	public void setNumero(String numero) {
		_numero = numero;
	}

	public String getManzana() {
		return _manzana;
	}

	public void setManzana(String manzana) {
		_manzana = manzana;
	}

	public String getPisoLote() {
		return _pisoLote;
	}

	public void setPisoLote(String pisoLote) {
		_pisoLote = pisoLote;
	}

	public String getInterior() {
		return _interior;
	}

	public void setInterior(String interior) {
		_interior = interior;
	}

	public String getUrbanizacion() {
		return _urbanizacion;
	}

	public void setUrbanizacion(String urbanizacion) {
		_urbanizacion = urbanizacion;
	}

	public String getReferencia() {
		return _referencia;
	}

	public void setReferencia(String referencia) {
		_referencia = referencia;
	}

	private long _idDetalleDireccion;
	private long _idDireccion;
	private String _tipoVia;
	private String _nombreVia;
	private String _numero;
	private String _manzana;
	private String _pisoLote;
	private String _interior;
	private String _urbanizacion;
	private String _referencia;
}