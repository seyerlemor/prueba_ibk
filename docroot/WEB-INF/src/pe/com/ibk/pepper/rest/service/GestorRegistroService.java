package pe.com.ibk.pepper.rest.service;

import java.text.ParseException;

import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.bean.UsuarioSessionBean;
import pe.com.ibk.pepper.model.Cliente;
import pe.com.ibk.pepper.model.Expediente;
import pe.com.ibk.pepper.model.Producto;
import pe.com.ibk.pepper.model.UsuarioSession;

public interface GestorRegistroService {

		public long registrarExpediente(SolicitudBean pasoBean);	

		public long registrarUsuarioSession(UsuarioSessionBean usuarioSessionBean);
		
		public long registrarCliente(SolicitudBean pasoBean);
		
		public long registrarProducto(SolicitudBean pasoBean);

		public Expediente setPasoBeanToExpediente(SolicitudBean pasoBean, Expediente expediente);	

		public UsuarioSession setUsuarioSessionBeanToUsuarioSession(UsuarioSessionBean usuarioSession1, UsuarioSession usuarioSession2);

		public Cliente setPasoBeanToCliente(SolicitudBean pasoBean, Cliente cliente2);		
		
		public Producto setPasoBeanToProducto(SolicitudBean pasoBean, Producto producto2) throws ParseException;	
}
