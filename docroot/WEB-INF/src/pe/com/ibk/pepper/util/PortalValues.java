package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.util.portlet.PortletProps;

public abstract class PortalValues {
	
	public static final String SERVICIOS_RESPUESTA_SUCCESS="0";
	public static final String SERVICIOS_RESPUESTA_ERROR="2";
	public static final String SERVICIOS_RESPUESTA_ADVERTENCIA="1";
	public static final String SERVICIOS_RESPUESTA_ERROR_TOKEN="4";
	public static final String SERVICIOS_RESPUESTA_NO_EXISTE="5";
	public static final String SERVICIOS_RESPUESTA_EMAIL_INDECOPI="01";
	
	public static final String URL_FTARJETA_GENERICA="tarjeta-credito";
	public static final String CODIGO_CANAL_FLUJO_WEB="WEB";
	public static final String CODIGO_CANAL_FLUJO_BPI="BPI";
	
	public static final int  WS_RENIEC_RSPTA_NO_BD=2;
	public static final int  WS_RENIEC_RSPTA_HIT=1;
	public static final int  WS_RENIEC_RSPTA_NO_HIT=0;
	
	public static final String WS_REQUEST_TIMEOUT = PortletProps.get(PortalKeys.WS_REQUEST_TIMEOUT);
	public static final String WS_CONNECT_TIMEOUT = PortletProps.get(PortalKeys.WS_CONNECT_TIMEOUT);

	public static final String REST_CONNECT_TIMEOUT = PortletProps.get(PortalKeys.REST_CONNECT_TIMEOUT);
	public static final String REST_READ_TIMEOUT = PortletProps.get(PortalKeys.REST_READ_TIMEOUT);

	public static final String REST_UBICACION_JKS = PortletProps.get(PortalKeys.REST_UBICACION_JKS);

	public static final String REST_API_JKS_NAME = PortletProps.get(PortalKeys.REST_API_JKS_NAME);
	public static final String REST_API_JKS_PASSW = PortletProps.get(PortalKeys.REST_API_JKS_PASSW);

	public static final String REST_BUS_JKS_NAME = PortletProps.get(PortalKeys.REST_BUS_JKS_NAME);
	public static final String REST_BUS_JKS_PASSW = PortletProps.get(PortalKeys.REST_BUS_JKS_PASSW);

	public static final String REST_R1_ENDPOINT = PortletProps.get(PortalKeys.REST_R1_ENDPOINT);
	public static final String REST_R1_RESOURCE = PortletProps.get(PortalKeys.REST_R1_RESOURCE);
	public static final String REST_R1_USUARIO = PortletProps.get(PortalKeys.REST_R1_USUARIO);
	public static final String REST_R1_PASSW = PortletProps.get(PortalKeys.REST_R1_PASSW);

	public static final String REST_CAMPANIA_ENDPOINT = PortletProps.get(PortalKeys.REST_CAMPANIA_ENDPOINT);
	public static final String REST_CAMPANIA_RESOURCE = PortletProps.get(PortalKeys.REST_CAMPANIA_RESOURCE);
	public static final String REST_CAMPANIA_USUARIO = PortletProps.get(PortalKeys.REST_CAMPANIA_USUARIO);
	public static final String REST_CAMPANIA_PASSW = PortletProps.get(PortalKeys.REST_CAMPANIA_PASSW);

	public static final String WS_RENIEC_ENDPOINT = PortletProps.get(PortalKeys.WS_RENIEC_ENDPOINT);
	public static final String WS_RENIEC_USUARIO = PortletProps.get(PortalKeys.WS_RENIEC_USUARIO);
	public static final String WS_RENIEC_PASSW = PortletProps.get(PortalKeys.WS_RENIEC_PASSW);
	public static final String WS_RENIEC_RUTA_FOTO = PortletProps.get(PortalKeys.WS_RENIEC_RUTA_FOTO);
	public static final String WS_RENIEC_INTENTOS = PortletProps.get(PortalKeys.WS_RENIEC_INTENTOS);

	public static final String REST_EQUIFAX_ENDPOINT = PortletProps.get(PortalKeys.REST_EQUIFAX_ENDPOINT);
	public static final String REST_EQUIFAX_CONSULTAR_RESOURCE = PortletProps.get(PortalKeys.REST_EQUIFAX_CONSULTAR_RESOURCE);
	public static final String REST_EQUIFAX_VALIDAR_RESOURCE = PortletProps.get(PortalKeys.REST_EQUIFAX_VALIDAR_RESOURCE);
	public static final String REST_EQUIFAX_USUARIO = PortletProps.get(PortalKeys.REST_EQUIFAX_USUARIO);
	public static final String REST_EQUIFAX_PASSW = PortletProps.get(PortalKeys.REST_EQUIFAX_PASSW);
	public static final String WS_EQUIFAX_INTENTOS = PortletProps.get(PortalKeys.WS_EQUIFAX_INTENTOS);

	public static final String REST_TIPIFICAR_ENDPOINT = PortletProps.get(PortalKeys.REST_TIPIFICAR_ENDPOINT);
	public static final String REST_TIPIFICAR_RESOURCE = PortletProps.get(PortalKeys.REST_TIPIFICAR_RESOURCE);
	public static final String REST_TIPIFICAR_USUARIO = PortletProps.get(PortalKeys.REST_TIPIFICAR_USUARIO);
	public static final String REST_TIPIFICAR_PASSW = PortletProps.get(PortalKeys.REST_TIPIFICAR_PASSW);

	public static final String REST_ACT_RESP_ENDPOINT = PortletProps.get(PortalKeys.REST_ACT_RESP_ENDPOINT);
	public static final String REST_ACT_RESP_RESOURCE = PortletProps.get(PortalKeys.REST_ACT_RESP_RESOURCE);
	public static final String REST_ACT_RESP_USUARIO = PortletProps.get(PortalKeys.REST_ACT_RESP_USUARIO);
	public static final String REST_ACT_RESP_PASSW = PortletProps.get(PortalKeys.REST_ACT_RESP_PASSW);

	public static final String REST_OBT_INF_ENDPOINT = PortletProps.get(PortalKeys.REST_OBT_INF_ENDPOINT);
	public static final String REST_OBT_INF_RESOURCE = PortletProps.get(PortalKeys.REST_OBT_INF_RESOURCE);
	public static final String REST_OBT_INF_USUARIO = PortletProps.get(PortalKeys.REST_OBT_INF_USUARIO);
	public static final String REST_OBT_INF_PASSW = PortletProps.get(PortalKeys.REST_OBT_INF_PASSW);

	public static final String REST_CALIF_CDA_ENDPOINT = PortletProps.get(PortalKeys.REST_CALIF_CDA_ENDPOINT);
	public static final String REST_CALIF_CDA_RESOURCE = PortletProps.get(PortalKeys.REST_CALIF_CDA_RESOURCE);
	public static final String REST_CALIF_CDA_USUARIO = PortletProps.get(PortalKeys.REST_CALIF_CDA_USUARIO);
	public static final String REST_CALIF_CDA_PASSW = PortletProps.get(PortalKeys.REST_CALIF_CDA_PASSW);

	public static final String REST_AUT_TOKEN_ENDPOINT = PortletProps.get(PortalKeys.REST_AUT_TOKEN_ENDPOINT);
	public static final String REST_AUT_TOKEN_RESOURCE = PortletProps.get(PortalKeys.REST_AUT_TOKEN_RESOURCE);
	public static final String REST_AUT_TOKEN_USUARIO = PortletProps.get(PortalKeys.REST_AUT_TOKEN_USUARIO);
	public static final String REST_AUT_TOKEN_PASSW = PortletProps.get(PortalKeys.REST_AUT_TOKEN_PASSW);
	
	public static final String REST_CONS_AFILIACION_ENDPOINT = PortletProps.get(PortalKeys.REST_CONS_AFILIACION_ENDPOINT);
	public static final String REST_CONS_AFILIACION_RESOURCE = PortletProps.get(PortalKeys.REST_CONS_AFILIACION_RESOURCE);
	public static final String REST_CONS_AFILIACION_USUARIO = PortletProps.get(PortalKeys.REST_CONS_AFILIACION_USUARIO);
	public static final String REST_CONS_AFILIACION_PASSW = PortletProps.get(PortalKeys.REST_CONS_AFILIACION_PASSW);

	//Venta TC
	public static final String REST_VENTA_TC_PROVISIONAL_ENDPOINT = PortletProps.get(PortalKeys.REST_VENTA_TC_PROVISIONAL_ENDPOINT);
	public static final String REST_VENTA_TC_PROVISIONAL_RESOURCE = PortletProps.get(PortalKeys.REST_VENTA_TC_PROVISIONAL_RESOURCE);
	public static final String REST_VENTA_TC_PROVISIONAL_USUARIO = PortletProps.get(PortalKeys.REST_VENTA_TC_PROVISIONAL_USUARIO);
	public static final String REST_VENTA_TC_PROVISIONAL_PASSW = PortletProps.get(PortalKeys.REST_VENTA_TC_PROVISIONAL_PASSW);
	//Consultar POTC
	public static final String REST_CONSULTAR_POTC_ENDPOINT = PortletProps.get(PortalKeys.REST_CONSULTAR_POTC_ENDPOINT);
	public static final String REST_CONSULTAR_POTC_RESOURCE = PortletProps.get(PortalKeys.REST_CONSULTAR_POTC_RESOURCE);
	public static final String REST_CONSULTAR_POTC_USUARIO = PortletProps.get(PortalKeys.REST_CONSULTAR_POTC_USUARIO);
	public static final String REST_CONSULTAR_POTC_PASSW = PortletProps.get(PortalKeys.REST_CONSULTAR_POTC_PASSW);	
	//Generar POTC
	public static final String REST_GENERAR_POTC_ENDPOINT = PortletProps.get(PortalKeys.REST_GENERAR_POTC_ENDPOINT);
	public static final String REST_GENERAR_POTC_RESOURCE = PortletProps.get(PortalKeys.REST_GENERAR_POTC_RESOURCE);
	public static final String REST_GENERAR_POTC_USUARIO = PortletProps.get(PortalKeys.REST_GENERAR_POTC_USUARIO);
	public static final String REST_GENERAR_POTC_PASSW = PortletProps.get(PortalKeys.REST_GENERAR_POTC_PASSW);
	//Validacio Token
	public static final String REST_VALID_TOKEN_ENDPOINT = PortletProps.get(PortalKeys.REST_VALID_TOKEN_ENDPOINT);
	public static final String REST_VALID_TOKEN_RESOURCE = PortletProps.get(PortalKeys.REST_VALID_TOKEN_RESOURCE);
	public static final String REST_VALID_TOKEN_USUARIO = PortletProps.get(PortalKeys.REST_VALID_TOKEN_USUARIO);
	public static final String REST_VALID_TOKEN_PASSW = PortletProps.get(PortalKeys.REST_VALID_TOKEN_PASSW);
	
	public static final String REST_CAMPANIA_CODIGO_PRODUCTO = PortletProps.get(PortalKeys.REST_CAMPANIA_CODIGO_PRODUCTO);
	public static final String REST_CAMPANIA_CODIGO_PRESTAMO = PortletProps.get(PortalKeys.REST_CAMPANIA_CODIGO_PRESTAMO);
	public static final String REST_CAMPANIA_CODIGO_SEGURO = PortletProps.get(PortalKeys.REST_CAMPANIA_CODIGO_SEGURO);
	public static final String REST_CAMPANIA_CODIGO_BAIS = PortletProps.get(PortalKeys.REST_CAMPANIA_CODIGO_BAIS);

	public static final String REST_EQUIFAX_MODELO1 = PortletProps.get(PortalKeys.REST_EQUIFAX_MODELO1);
	public static final String REST_EQUIFAX_MODELO2 = PortletProps.get(PortalKeys.REST_EQUIFAX_MODELO2);
	
	public static final String REST_SERVER_FIX = PortletProps.get(PortalKeys.REST_SERVER_FIX);
	public static final String REST_HOST_NAME = PortletProps.get(PortalKeys.REST_HOST_NAME);

	public static final String WEB_RECAPTCHA_KEY = PropsUtil.get(PortalKeys.WEB_RECAPTCHA_KEY);
	
	//Consultar POTC
	public static final String REST_CONSULTAR_PROV_ENDPOINT = PortletProps.get(PortalKeys.REST_CONSULTAR_PROV_ENDPOINT);
	public static final String REST_CONSULTAR_PROV_RESOURCE = PortletProps.get(PortalKeys.REST_CONSULTAR_PROV_RESOURCE);
	public static final String REST_CONSULTAR_PROV_USUARIO = PortletProps.get(PortalKeys.REST_CONSULTAR_PROV_USUARIO);
	public static final String REST_CONSULTAR_PROV_PASSW = PortletProps.get(PortalKeys.REST_CONSULTAR_PROV_PASSW);
	
	//Venta TC
	public static final String REST_OBTENER_TARJETA_ENDPOINT = PortletProps.get(PortalKeys.REST_OBTENER_TARJETA_ENDPOINT);
	public static final String REST_OBTENER_TARJETA_RESOURCE = PortletProps.get(PortalKeys.REST_OBTENER_TARJETA_RESOURCE);
	public static final String REST_OBTENER_TARJETA_USUARIO = PortletProps.get(PortalKeys.REST_OBTENER_TARJETA_USUARIO);
	public static final String REST_OBTENER_TARJETA_PASSW = PortletProps.get(PortalKeys.REST_OBTENER_TARJETA_PASSW);	
	
	public static final String REST_CONSULTAR_TARJETA_ENDPOINT = PortletProps.get(PortalKeys.REST_CONSULTAR_TARJETA_ENDPOINT);
	public static final String REST_CONSULTAR_TARJETA_RESOURCE = PortletProps.get(PortalKeys.REST_CONSULTAR_TARJETA_RESOURCE);
	public static final String REST_CONSULTAR_TARJETA_USUARIO = PortletProps.get(PortalKeys.REST_CONSULTAR_TARJETA_USUARIO);
	public static final String REST_CONSULTAR_TARJETA_PASSW = PortletProps.get(PortalKeys.REST_CONSULTAR_TARJETA_PASSW);
	public static final String REST_VENTA_TC_PROVISIONAL_FECHA_PAGO = PortletProps.get(PortalKeys.REST_VENTA_TC_PROVISIONAL_FECHA_PAGO); 
	
	public static final String REST_ENVIO_CORREO_ENDPOINT = PortletProps.get(PortalKeys.REST_ENVIO_CORREO_ENDPOINT);
	public static final String REST_ENVIO_CORREO_RESOURCE = PortletProps.get(PortalKeys.REST_ENVIO_CORREO_RESOURCE);
	public static final String REST_ENVIO_CORREO_USUARIO = PortletProps.get(PortalKeys.REST_ENVIO_CORREO_USUARIO);
	public static final String REST_ENVIO_CORREO_PASSW = PortletProps.get(PortalKeys.REST_ENVIO_CORREO_PASSW);
	
	public static final String MONTO_INICIAL_MEGALINEA = PortletProps.get(PortalKeys.MONTO_INICIAL_MEGALINEA);
	public static final String HABILITAR_PAS2 = PortletProps.get(PortalKeys.HABILITAR_PAS2);
	public static final String HABILITAR_V2CDA = PortletProps.get(PortalKeys.HABILITAR_V2CDA);
	
	//Logica Reingreso
	public static final String REST_LOGICA_REINGRESO_ENDPOINT = PortletProps.get(PortalKeys.REST_LOGICA_REINGRESO_ENDPOINT);
	public static final String REST_LOGICA_REINGRESO_RESOURCE = PortletProps.get(PortalKeys.REST_LOGICA_REINGRESO_RESOURCE);
	public static final String REST_LOGICA_REINGRESO_USUARIO = PortletProps.get(PortalKeys.REST_LOGICA_REINGRESO_USUARIO);
	public static final String REST_LOGICA_REINGRESO_PASSW = PortletProps.get(PortalKeys.REST_LOGICA_REINGRESO_PASSW);
	
	public static final String REST_CONS_SCORE_EFL_ENDPOINT = PortletProps.get(PortalKeys.REST_CONS_SCORE_EFL_ENDPOINT);
	public static final String REST_CONS_SCORE_EFL_RESOURCE = PortletProps.get(PortalKeys.REST_CONS_SCORE_EFL_RESOURCE);
	public static final String REST_CONS_SCORE_EFL_USUARIO = PortletProps.get(PortalKeys.REST_CONS_SCORE_EFL_USUARIO);
	public static final String REST_CONS_SCORE_EFL_PASSW = PortletProps.get(PortalKeys.REST_CONS_SCORE_EFL_PASSW);
	
	public static final String REST_DEV_AMB = PortletProps.get(PortalKeys.REST_DEV_AMB);
	public static final String REST_DEV_JKS_NAME = PortletProps.get(PortalKeys.REST_DEV_JKS_NAME);
	public static final String REST_DEV_JKS_PASSW = PortletProps.get(PortalKeys.REST_DEV_JKS_PASSW);
	
	public static final String HABILITAR_R1 = PortletProps.get(PortalKeys.HABILITAR_R1);
	
	public static final String REST_ENVIO_AUDITORIA_ENDPOINT = PortletProps.get(PortalKeys.REST_ENVIO_AUDITORIA_ENDPOINT);
	public static final String REST_ENVIO_AUDITORIA_RESOURCE = PortletProps.get(PortalKeys.REST_ENVIO_AUDITORIA_RESOURCE);
	public static final String REST_ENVIO_AUDITORIA_USUARIO = PortletProps.get(PortalKeys.REST_ENVIO_AUDITORIA_USUARIO);
	public static final String REST_ENVIO_AUDITORIA_PASSW = PortletProps.get(PortalKeys.REST_ENVIO_AUDITORIA_PASSW);
	
	public static final String REST_VAL_RUC_ENDPOINT = PortletProps.get(PortalKeys.REST_VAL_RUC_ENDPOINT);
	public static final String REST_VAL_RUC_RESOURCE = PortletProps.get(PortalKeys.REST_VAL_RUC_RESOURCE);
	public static final String REST_VAL_RUC_USUARIO = PortletProps.get(PortalKeys.REST_VAL_RUC_USUARIO);
	public static final String REST_VAL_RUC_PASSW = PortletProps.get(PortalKeys.REST_VAL_RUC_PASSW);
	
	public static final String TIPO_AMBIENTE = PortletProps.get(PortalKeys.TIPO_AMBIENTE);

	public static final String REST_VAL_RECAPTCHA_ENDPOINT = PortletProps.get(PortalKeys.REST_VAL_RECAPTCHA_ENDPOINT);
	public static final String REST_VAL_RECAPTCHA_KEY = PortletProps.get(PortalKeys.REST_VAL_RECAPTCHA_KEY);
	public static final String REST_VAL_RECAPTCHA_SECRET = PortletProps.get(PortalKeys.REST_VAL_RECAPTCHA_SECRET);
	
	public static final String REST_FHETREGA_ENDPOINT = PortletProps.get(PortalKeys.REST_FHETREGA_ENDPOINT);
	public static final String REST_FHETREGA_RESOURCE = PortletProps.get(PortalKeys.REST_FHETREGA_RESOURCE);
	public static final String REST_FHETREGA_USUARIO = PortletProps.get(PortalKeys.REST_FHETREGA_USUARIO);
	public static final String REST_FHETREGA_PASSW = PortletProps.get(PortalKeys.REST_FHETREGA_PASSW);
	
	public static final String FLAG_CAMPANIA_V2 = PortletProps.get(PortalKeys.FLAG_CAMPANIA_V2);
	
	private PortalValues() {
		
	}
}
