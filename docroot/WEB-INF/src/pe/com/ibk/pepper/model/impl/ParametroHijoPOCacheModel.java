/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.ParametroHijoPO;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ParametroHijoPO in entity cache.
 *
 * @author Interbank
 * @see ParametroHijoPO
 * @generated
 */
public class ParametroHijoPOCacheModel implements CacheModel<ParametroHijoPO>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(43);

		sb.append("{idParametroHijo=");
		sb.append(idParametroHijo);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", codigoPadre=");
		sb.append(codigoPadre);
		sb.append(", codigo=");
		sb.append(codigo);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", orden=");
		sb.append(orden);
		sb.append(", dato1=");
		sb.append(dato1);
		sb.append(", dato2=");
		sb.append(dato2);
		sb.append(", dato3=");
		sb.append(dato3);
		sb.append(", dato4=");
		sb.append(dato4);
		sb.append(", dato5=");
		sb.append(dato5);
		sb.append(", dato6=");
		sb.append(dato6);
		sb.append(", dato7=");
		sb.append(dato7);
		sb.append(", dato8=");
		sb.append(dato8);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ParametroHijoPO toEntityModel() {
		ParametroHijoPOImpl parametroHijoPOImpl = new ParametroHijoPOImpl();

		parametroHijoPOImpl.setIdParametroHijo(idParametroHijo);
		parametroHijoPOImpl.setGroupId(groupId);
		parametroHijoPOImpl.setCompanyId(companyId);
		parametroHijoPOImpl.setUserId(userId);

		if (userName == null) {
			parametroHijoPOImpl.setUserName(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			parametroHijoPOImpl.setCreateDate(null);
		}
		else {
			parametroHijoPOImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			parametroHijoPOImpl.setModifiedDate(null);
		}
		else {
			parametroHijoPOImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (codigoPadre == null) {
			parametroHijoPOImpl.setCodigoPadre(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setCodigoPadre(codigoPadre);
		}

		if (codigo == null) {
			parametroHijoPOImpl.setCodigo(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setCodigo(codigo);
		}

		if (nombre == null) {
			parametroHijoPOImpl.setNombre(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setNombre(nombre);
		}

		if (descripcion == null) {
			parametroHijoPOImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDescripcion(descripcion);
		}

		parametroHijoPOImpl.setEstado(estado);
		parametroHijoPOImpl.setOrden(orden);

		if (dato1 == null) {
			parametroHijoPOImpl.setDato1(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato1(dato1);
		}

		if (dato2 == null) {
			parametroHijoPOImpl.setDato2(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato2(dato2);
		}

		if (dato3 == null) {
			parametroHijoPOImpl.setDato3(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato3(dato3);
		}

		if (dato4 == null) {
			parametroHijoPOImpl.setDato4(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato4(dato4);
		}

		if (dato5 == null) {
			parametroHijoPOImpl.setDato5(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato5(dato5);
		}

		if (dato6 == null) {
			parametroHijoPOImpl.setDato6(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato6(dato6);
		}

		if (dato7 == null) {
			parametroHijoPOImpl.setDato7(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato7(dato7);
		}

		if (dato8 == null) {
			parametroHijoPOImpl.setDato8(StringPool.BLANK);
		}
		else {
			parametroHijoPOImpl.setDato8(dato8);
		}

		parametroHijoPOImpl.resetOriginalValues();

		return parametroHijoPOImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idParametroHijo = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		codigoPadre = objectInput.readUTF();
		codigo = objectInput.readUTF();
		nombre = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		estado = objectInput.readBoolean();
		orden = objectInput.readInt();
		dato1 = objectInput.readUTF();
		dato2 = objectInput.readUTF();
		dato3 = objectInput.readUTF();
		dato4 = objectInput.readUTF();
		dato5 = objectInput.readUTF();
		dato6 = objectInput.readUTF();
		dato7 = objectInput.readUTF();
		dato8 = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idParametroHijo);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (codigoPadre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoPadre);
		}

		if (codigo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigo);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		objectOutput.writeBoolean(estado);
		objectOutput.writeInt(orden);

		if (dato1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato1);
		}

		if (dato2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato2);
		}

		if (dato3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato3);
		}

		if (dato4 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato4);
		}

		if (dato5 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato5);
		}

		if (dato6 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato6);
		}

		if (dato7 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato7);
		}

		if (dato8 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dato8);
		}
	}

	public long idParametroHijo;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String codigoPadre;
	public String codigo;
	public String nombre;
	public String descripcion;
	public boolean estado;
	public int orden;
	public String dato1;
	public String dato2;
	public String dato3;
	public String dato4;
	public String dato5;
	public String dato6;
	public String dato7;
	public String dato8;
}