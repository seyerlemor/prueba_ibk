jQuery(document).ready(function($) {
	
	    function detectarDispositivoyNavegador(){
	    		var dispositivo = navigator.userAgent.toLowerCase();
	    	    var isMobile = {
	    	    	    iOS: function() {
	    	    	        return navigator.userAgent.match(/iPhone|iPod/i);
	    	    	    },
	    	    	    iOSTablet: function() {
	    	    	        return navigator.userAgent.match(/iPad/i);
	    	    	    },
	    	    	    Android: function() {
	    	    	        return navigator.userAgent.match(/Android/i);
	    	    	    },
	    	    	    BlackBerry: function() {
	    	    	        return navigator.userAgent.match(/BlackBerry/i);
	    	    	    },
	    	    	    Opera: function() {
	    	    	        return navigator.userAgent.match(/Opera Mini/i);
	    	    	    },
	    	    	    Windows: function() {
	    	    	        return navigator.userAgent.match(/IEMobile/i);
	    	    	    },
	    	    	    any: function() {
	    	    	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    	    	    }
	    	    	};
	    	  
	    	 if(isMobile.any()) {
	    		 $('#dispositivo').val('MOBILE');
	    	 }else if(isMobile.iOSTablet()) {
	    		 $('#dispositivo').val('IPAD');	 	 
	    	 }else{
	    	     $('#dispositivo').val('PC');
	    	 }
	    	 detectarNavegador();
	    }
	    	    
	    
		function detectarNavegador(){
			        var pgwBrowser = {};
			        pgwBrowser.userAgent = navigator.userAgent;
			        pgwBrowser.browser = {};
			        resizeEvent = null;

			        var browserData = [
			            { name: 'Chromium',          group: 'Chrome',   identifier: 'Chromium/([0-9\.]*)'       },
			            { name: 'Chrome Mobile',     group: 'Chrome',   identifier: 'Chrome/([0-9\.]*) Mobile', versionIdentifier: 'Chrome/([0-9\.]*)'},
			            { name: 'Chrome',            group: 'Chrome',   identifier: 'Chrome/([0-9\.]*)'         },
			            { name: 'Chrome for iOS',    group: 'Chrome',   identifier: 'CriOS/([0-9\.]*)'          },
			            { name: 'Android Browser',   group: 'Chrome',   identifier: 'CrMo/([0-9\.]*)'           },
			            { name: 'Firefox',           group: 'Firefox',  identifier: 'Firefox/([0-9\.]*)'        },
			            { name: 'Opera Mini',        group: 'Opera',    identifier: 'Opera Mini/([0-9\.]*)'     },
			            { name: 'Opera',             group: 'Opera',    identifier: 'Opera ([0-9\.]*)'          },
			            { name: 'Opera',             group: 'Opera',    identifier: 'Opera/([0-9\.]*)',         versionIdentifier: 'Version/([0-9\.]*)' },
			            { name: 'IEMobile',          group: 'Explorer', identifier: 'IEMobile/([0-9\.]*)'       },
			            { name: 'Internet Explorer', group: 'Explorer', identifier: 'MSIE ([a-zA-Z0-9\.]*)'     },
			            { name: 'Internet Explorer', group: 'Explorer', identifier: 'Trident/([0-9\.]*)',       versionIdentifier: 'rv:([0-9\.]*)' },
			            { name: 'Spartan',           group: 'Spartan',  identifier: 'Edge/([0-9\.]*)',          versionIdentifier: 'Edge/([0-9\.]*)' },
			            { name: 'Safari',            group: 'Safari',   identifier: 'Safari/([0-9\.]*)',        versionIdentifier: 'Version/([0-9\.]*)' }
			        ];

			        var setBrowserData = function() {
			            var userAgent = pgwBrowser.userAgent.toLowerCase();

			            for (i in browserData) {
			                var browserRegExp = new RegExp(browserData[i].identifier.toLowerCase());
			                var browserRegExpResult = browserRegExp.exec(userAgent);

			                if (browserRegExpResult != null && browserRegExpResult[1]) {
			                    pgwBrowser.browser.name = browserData[i].name;
			                    pgwBrowser.browser.group = browserData[i].group;
			                    $('#navegador').val('');
			        			$('#navegador').val(pgwBrowser.browser.group);
			                    
			                    
			                    if (browserData[i].versionIdentifier) {
			                        var versionRegExp = new RegExp(browserData[i].versionIdentifier.toLowerCase());
			                        var versionRegExpResult = versionRegExp.exec(userAgent);

			                        if (versionRegExpResult != null && versionRegExpResult[1]) {
			                            setBrowserVersion(versionRegExpResult[1]);
			                        }

			                    } else {
			                        setBrowserVersion(browserRegExpResult[1]);
			                    }

			                    break;
			                }
			            }
			            return true;
			        };

			        var setBrowserVersion = function(version) {
			            var splitVersion = version.split('.', 2);
			            pgwBrowser.browser.fullVersion = version;

			            if (splitVersion[0]) {
			                pgwBrowser.browser.majorVersion = parseInt(splitVersion[0]);
			            }

			            if (splitVersion[1]) {
			                pgwBrowser.browser.minorVersion = parseInt(splitVersion[1]);
			            }

			            return true;
			        };

			        if (typeof window.pgwJsUserAgentTester != 'undefined') {
			            pgwBrowser.userAgent = window.pgwJsUserAgentTester;
			        }

			        // Initialization
			        setBrowserData();
		}
			  $('.lh-range__track').on('mouseup touchstart', function () {
					 var creditLineAux = $("#creditLineAux").val().replace("S/ ", "").replace(",", "");
					 $('#creditLine').val(creditLineAux);
		      });
			  
			  $(".lh-range__track").mouseup(function(){
				  	var creditLineAux = $("#creditLineAux").val().replace("S/ ", "").replace(",", "");
				  	$('#creditLine').val(creditLineAux);
			  });
			  
			  $('#creditLineAux').on('mouseup touchstart', function () {
					 var creditLineAux = $("#creditLineAux").val().replace("S/ ", "").replace(",", "");
					 $('#creditLine').val(creditLineAux);
		      });
			  $('#creditLineAux').mouseup(function(){
				  	var creditLineAux = $("#creditLineAux").val().replace("S/ ", "").replace(",", "");
				  	$('#creditLine').val(creditLineAux);
			  });
			  
			  $(".lh-btn-config").click(function(e) {
				  var creditLineAux = $("#creditLineAux").val().replace("S/ ", "").replace(",", "");
				  	$('#creditLine').val(creditLineAux);
				});
			  
});		
