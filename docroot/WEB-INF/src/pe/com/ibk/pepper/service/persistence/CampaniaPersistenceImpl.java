/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchCampaniaException;
import pe.com.ibk.pepper.model.Campania;
import pe.com.ibk.pepper.model.impl.CampaniaImpl;
import pe.com.ibk.pepper.model.impl.CampaniaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the campania service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see CampaniaPersistence
 * @see CampaniaUtil
 * @generated
 */
public class CampaniaPersistenceImpl extends BasePersistenceImpl<Campania>
	implements CampaniaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CampaniaUtil} to access the campania persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CampaniaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, CampaniaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, CampaniaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_C_ID = new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, CampaniaImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByC_ID",
			new String[] { Long.class.getName() },
			CampaniaModelImpl.IDCAMPANIA_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_ID = new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the campania where idCampania = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchCampaniaException} if it could not be found.
	 *
	 * @param idCampania the id campania
	 * @return the matching campania
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania findByC_ID(long idCampania)
		throws NoSuchCampaniaException, SystemException {
		Campania campania = fetchByC_ID(idCampania);

		if (campania == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idCampania=");
			msg.append(idCampania);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCampaniaException(msg.toString());
		}

		return campania;
	}

	/**
	 * Returns the campania where idCampania = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idCampania the id campania
	 * @return the matching campania, or <code>null</code> if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania fetchByC_ID(long idCampania) throws SystemException {
		return fetchByC_ID(idCampania, true);
	}

	/**
	 * Returns the campania where idCampania = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idCampania the id campania
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching campania, or <code>null</code> if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania fetchByC_ID(long idCampania, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idCampania };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_ID,
					finderArgs, this);
		}

		if (result instanceof Campania) {
			Campania campania = (Campania)result;

			if ((idCampania != campania.getIdCampania())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CAMPANIA_WHERE);

			query.append(_FINDER_COLUMN_C_ID_IDCAMPANIA_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idCampania);

				List<Campania> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"CampaniaPersistenceImpl.fetchByC_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Campania campania = list.get(0);

					result = campania;

					cacheResult(campania);

					if ((campania.getIdCampania() != idCampania)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_ID,
							finderArgs, campania);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Campania)result;
		}
	}

	/**
	 * Removes the campania where idCampania = &#63; from the database.
	 *
	 * @param idCampania the id campania
	 * @return the campania that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania removeByC_ID(long idCampania)
		throws NoSuchCampaniaException, SystemException {
		Campania campania = findByC_ID(idCampania);

		return remove(campania);
	}

	/**
	 * Returns the number of campanias where idCampania = &#63;.
	 *
	 * @param idCampania the id campania
	 * @return the number of matching campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_ID(long idCampania) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_ID;

		Object[] finderArgs = new Object[] { idCampania };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CAMPANIA_WHERE);

			query.append(_FINDER_COLUMN_C_ID_IDCAMPANIA_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idCampania);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_ID_IDCAMPANIA_2 = "campania.idCampania = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEXPEDIENTE =
		new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, CampaniaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdExpediente",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE =
		new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, CampaniaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdExpediente",
			new String[] { Long.class.getName() },
			CampaniaModelImpl.IDEXPEDIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEXPEDIENTE = new FinderPath(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdExpediente",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the campanias where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Campania> findByIdExpediente(long idExpediente)
		throws SystemException {
		return findByIdExpediente(idExpediente, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the campanias where idExpediente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idExpediente the id expediente
	 * @param start the lower bound of the range of campanias
	 * @param end the upper bound of the range of campanias (not inclusive)
	 * @return the range of matching campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Campania> findByIdExpediente(long idExpediente, int start,
		int end) throws SystemException {
		return findByIdExpediente(idExpediente, start, end, null);
	}

	/**
	 * Returns an ordered range of all the campanias where idExpediente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idExpediente the id expediente
	 * @param start the lower bound of the range of campanias
	 * @param end the upper bound of the range of campanias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Campania> findByIdExpediente(long idExpediente, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE;
			finderArgs = new Object[] { idExpediente };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEXPEDIENTE;
			finderArgs = new Object[] {
					idExpediente,
					
					start, end, orderByComparator
				};
		}

		List<Campania> list = (List<Campania>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Campania campania : list) {
				if ((idExpediente != campania.getIdExpediente())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CAMPANIA_WHERE);

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CampaniaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				if (!pagination) {
					list = (List<Campania>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Campania>(list);
				}
				else {
					list = (List<Campania>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first campania in the ordered set where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching campania
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania findByIdExpediente_First(long idExpediente,
		OrderByComparator orderByComparator)
		throws NoSuchCampaniaException, SystemException {
		Campania campania = fetchByIdExpediente_First(idExpediente,
				orderByComparator);

		if (campania != null) {
			return campania;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idExpediente=");
		msg.append(idExpediente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCampaniaException(msg.toString());
	}

	/**
	 * Returns the first campania in the ordered set where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching campania, or <code>null</code> if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania fetchByIdExpediente_First(long idExpediente,
		OrderByComparator orderByComparator) throws SystemException {
		List<Campania> list = findByIdExpediente(idExpediente, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last campania in the ordered set where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching campania
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania findByIdExpediente_Last(long idExpediente,
		OrderByComparator orderByComparator)
		throws NoSuchCampaniaException, SystemException {
		Campania campania = fetchByIdExpediente_Last(idExpediente,
				orderByComparator);

		if (campania != null) {
			return campania;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idExpediente=");
		msg.append(idExpediente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCampaniaException(msg.toString());
	}

	/**
	 * Returns the last campania in the ordered set where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching campania, or <code>null</code> if a matching campania could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania fetchByIdExpediente_Last(long idExpediente,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdExpediente(idExpediente);

		if (count == 0) {
			return null;
		}

		List<Campania> list = findByIdExpediente(idExpediente, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the campanias before and after the current campania in the ordered set where idExpediente = &#63;.
	 *
	 * @param idCampania the primary key of the current campania
	 * @param idExpediente the id expediente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next campania
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania[] findByIdExpediente_PrevAndNext(long idCampania,
		long idExpediente, OrderByComparator orderByComparator)
		throws NoSuchCampaniaException, SystemException {
		Campania campania = findByPrimaryKey(idCampania);

		Session session = null;

		try {
			session = openSession();

			Campania[] array = new CampaniaImpl[3];

			array[0] = getByIdExpediente_PrevAndNext(session, campania,
					idExpediente, orderByComparator, true);

			array[1] = campania;

			array[2] = getByIdExpediente_PrevAndNext(session, campania,
					idExpediente, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Campania getByIdExpediente_PrevAndNext(Session session,
		Campania campania, long idExpediente,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CAMPANIA_WHERE);

		query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CampaniaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(idExpediente);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(campania);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Campania> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the campanias where idExpediente = &#63; from the database.
	 *
	 * @param idExpediente the id expediente
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdExpediente(long idExpediente)
		throws SystemException {
		for (Campania campania : findByIdExpediente(idExpediente,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(campania);
		}
	}

	/**
	 * Returns the number of campanias where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @return the number of matching campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdExpediente(long idExpediente) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEXPEDIENTE;

		Object[] finderArgs = new Object[] { idExpediente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CAMPANIA_WHERE);

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2 = "campania.idExpediente = ?";

	public CampaniaPersistenceImpl() {
		setModelClass(Campania.class);
	}

	/**
	 * Caches the campania in the entity cache if it is enabled.
	 *
	 * @param campania the campania
	 */
	@Override
	public void cacheResult(Campania campania) {
		EntityCacheUtil.putResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaImpl.class, campania.getPrimaryKey(), campania);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_ID,
			new Object[] { campania.getIdCampania() }, campania);

		campania.resetOriginalValues();
	}

	/**
	 * Caches the campanias in the entity cache if it is enabled.
	 *
	 * @param campanias the campanias
	 */
	@Override
	public void cacheResult(List<Campania> campanias) {
		for (Campania campania : campanias) {
			if (EntityCacheUtil.getResult(
						CampaniaModelImpl.ENTITY_CACHE_ENABLED,
						CampaniaImpl.class, campania.getPrimaryKey()) == null) {
				cacheResult(campania);
			}
			else {
				campania.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all campanias.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CampaniaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CampaniaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the campania.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Campania campania) {
		EntityCacheUtil.removeResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaImpl.class, campania.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(campania);
	}

	@Override
	public void clearCache(List<Campania> campanias) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Campania campania : campanias) {
			EntityCacheUtil.removeResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
				CampaniaImpl.class, campania.getPrimaryKey());

			clearUniqueFindersCache(campania);
		}
	}

	protected void cacheUniqueFindersCache(Campania campania) {
		if (campania.isNew()) {
			Object[] args = new Object[] { campania.getIdCampania() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_ID, args, campania);
		}
		else {
			CampaniaModelImpl campaniaModelImpl = (CampaniaModelImpl)campania;

			if ((campaniaModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { campania.getIdCampania() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_ID, args,
					campania);
			}
		}
	}

	protected void clearUniqueFindersCache(Campania campania) {
		CampaniaModelImpl campaniaModelImpl = (CampaniaModelImpl)campania;

		Object[] args = new Object[] { campania.getIdCampania() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_ID, args);

		if ((campaniaModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_ID.getColumnBitmask()) != 0) {
			args = new Object[] { campaniaModelImpl.getOriginalIdCampania() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_ID, args);
		}
	}

	/**
	 * Creates a new campania with the primary key. Does not add the campania to the database.
	 *
	 * @param idCampania the primary key for the new campania
	 * @return the new campania
	 */
	@Override
	public Campania create(long idCampania) {
		Campania campania = new CampaniaImpl();

		campania.setNew(true);
		campania.setPrimaryKey(idCampania);

		return campania;
	}

	/**
	 * Removes the campania with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idCampania the primary key of the campania
	 * @return the campania that was removed
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania remove(long idCampania)
		throws NoSuchCampaniaException, SystemException {
		return remove((Serializable)idCampania);
	}

	/**
	 * Removes the campania with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the campania
	 * @return the campania that was removed
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania remove(Serializable primaryKey)
		throws NoSuchCampaniaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Campania campania = (Campania)session.get(CampaniaImpl.class,
					primaryKey);

			if (campania == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCampaniaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(campania);
		}
		catch (NoSuchCampaniaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Campania removeImpl(Campania campania) throws SystemException {
		campania = toUnwrappedModel(campania);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(campania)) {
				campania = (Campania)session.get(CampaniaImpl.class,
						campania.getPrimaryKeyObj());
			}

			if (campania != null) {
				session.delete(campania);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (campania != null) {
			clearCache(campania);
		}

		return campania;
	}

	@Override
	public Campania updateImpl(pe.com.ibk.pepper.model.Campania campania)
		throws SystemException {
		campania = toUnwrappedModel(campania);

		boolean isNew = campania.isNew();

		CampaniaModelImpl campaniaModelImpl = (CampaniaModelImpl)campania;

		Session session = null;

		try {
			session = openSession();

			if (campania.isNew()) {
				session.save(campania);

				campania.setNew(false);
			}
			else {
				session.merge(campania);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CampaniaModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((campaniaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						campaniaModelImpl.getOriginalIdExpediente()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEXPEDIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE,
					args);

				args = new Object[] { campaniaModelImpl.getIdExpediente() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEXPEDIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE,
					args);
			}
		}

		EntityCacheUtil.putResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
			CampaniaImpl.class, campania.getPrimaryKey(), campania);

		clearUniqueFindersCache(campania);
		cacheUniqueFindersCache(campania);

		return campania;
	}

	protected Campania toUnwrappedModel(Campania campania) {
		if (campania instanceof CampaniaImpl) {
			return campania;
		}

		CampaniaImpl campaniaImpl = new CampaniaImpl();

		campaniaImpl.setNew(campania.isNew());
		campaniaImpl.setPrimaryKey(campania.getPrimaryKey());

		campaniaImpl.setIdCampania(campania.getIdCampania());
		campaniaImpl.setIdExpediente(campania.getIdExpediente());
		campaniaImpl.setCodigoCampania(campania.getCodigoCampania());
		campaniaImpl.setNombreCampania(campania.getNombreCampania());
		campaniaImpl.setCodigoOferta(campania.getCodigoOferta());
		campaniaImpl.setNombreOferta(campania.getNombreOferta());
		campaniaImpl.setCodigoTratamiento(campania.getCodigoTratamiento());
		campaniaImpl.setNombreTratamiento(campania.getNombreTratamiento());
		campaniaImpl.setCodigoUnico(campania.getCodigoUnico());
		campaniaImpl.setCodigoProducto(campania.getCodigoProducto());
		campaniaImpl.setCore(campania.getCore());
		campaniaImpl.setNombreProducto(campania.getNombreProducto());
		campaniaImpl.setTipoCampania(campania.getTipoCampania());
		campaniaImpl.setFechaRegistro(campania.getFechaRegistro());

		return campaniaImpl;
	}

	/**
	 * Returns the campania with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the campania
	 * @return the campania
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCampaniaException, SystemException {
		Campania campania = fetchByPrimaryKey(primaryKey);

		if (campania == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCampaniaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return campania;
	}

	/**
	 * Returns the campania with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchCampaniaException} if it could not be found.
	 *
	 * @param idCampania the primary key of the campania
	 * @return the campania
	 * @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania findByPrimaryKey(long idCampania)
		throws NoSuchCampaniaException, SystemException {
		return findByPrimaryKey((Serializable)idCampania);
	}

	/**
	 * Returns the campania with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the campania
	 * @return the campania, or <code>null</code> if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Campania campania = (Campania)EntityCacheUtil.getResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
				CampaniaImpl.class, primaryKey);

		if (campania == _nullCampania) {
			return null;
		}

		if (campania == null) {
			Session session = null;

			try {
				session = openSession();

				campania = (Campania)session.get(CampaniaImpl.class, primaryKey);

				if (campania != null) {
					cacheResult(campania);
				}
				else {
					EntityCacheUtil.putResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
						CampaniaImpl.class, primaryKey, _nullCampania);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CampaniaModelImpl.ENTITY_CACHE_ENABLED,
					CampaniaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return campania;
	}

	/**
	 * Returns the campania with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idCampania the primary key of the campania
	 * @return the campania, or <code>null</code> if a campania with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Campania fetchByPrimaryKey(long idCampania)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idCampania);
	}

	/**
	 * Returns all the campanias.
	 *
	 * @return the campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Campania> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the campanias.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of campanias
	 * @param end the upper bound of the range of campanias (not inclusive)
	 * @return the range of campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Campania> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the campanias.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of campanias
	 * @param end the upper bound of the range of campanias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Campania> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Campania> list = (List<Campania>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CAMPANIA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CAMPANIA;

				if (pagination) {
					sql = sql.concat(CampaniaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Campania>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Campania>(list);
				}
				else {
					list = (List<Campania>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the campanias from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Campania campania : findAll()) {
			remove(campania);
		}
	}

	/**
	 * Returns the number of campanias.
	 *
	 * @return the number of campanias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CAMPANIA);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the campania persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.Campania")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Campania>> listenersList = new ArrayList<ModelListener<Campania>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Campania>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CampaniaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CAMPANIA = "SELECT campania FROM Campania campania";
	private static final String _SQL_SELECT_CAMPANIA_WHERE = "SELECT campania FROM Campania campania WHERE ";
	private static final String _SQL_COUNT_CAMPANIA = "SELECT COUNT(campania) FROM Campania campania";
	private static final String _SQL_COUNT_CAMPANIA_WHERE = "SELECT COUNT(campania) FROM Campania campania WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "campania.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Campania exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Campania exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CampaniaPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idCampania", "idExpediente", "codigoCampania", "nombreCampania",
				"codigoOferta", "nombreOferta", "codigoTratamiento",
				"nombreTratamiento", "codigoUnico", "codigoProducto", "core",
				"nombreProducto", "tipoCampania", "fechaRegistro"
			});
	private static Campania _nullCampania = new CampaniaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Campania> toCacheModel() {
				return _nullCampaniaCacheModel;
			}
		};

	private static CacheModel<Campania> _nullCampaniaCacheModel = new CacheModel<Campania>() {
			@Override
			public Campania toEntityModel() {
				return _nullCampania;
			}
		};
}