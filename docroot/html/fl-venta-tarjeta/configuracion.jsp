<%@ include file="/html/init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="altaTc" var="altaTc" />

<portlet:resourceURL var="getListUbigeo" id="getUbigeo">
</portlet:resourceURL>

<portlet:resourceURL var="addAddressTC" id="addAddress">
</portlet:resourceURL>

    
<div class="lq-container">
        <div class="lh-ff">
          <div class="lh-ff-block lh-ff-block--active">
            <div class="lq-row">
                    ${commerceText.v501001}
              <div class="lh-ff-content-col">
              	<form:form   action="${altaTc}" autocomplete="off" commandName="configurarTarjeta" class="lh-ff-form" novalidate="novalidate" >
              	<div class="lh-ff-section">
                    <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 text-center">${commerceText.v501002}</h3>
                    <picture>
                      <source media="(min-width: 961px)" srcset="/pepper-v2-theme/images/tc/normal/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg">
                      <source media="(min-width: 961px) 2x" srcset="/pepper-v2-theme/images/tc/normal/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg"><img class="lh-form-tc__img" src="/pepper-v2-theme/images/tc/normal/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg" alt="#">
                    </picture>
                    
                    <c:choose>
  					<c:when test="${requestScope.validateLine}">
  						<div class="text-center lh-typo__commontitle lh-typo__commontitle--1">
	                    	${commerceText.v501009} <br/>
	                    	S/${formatCreditLine}
	                    </div>
  					</c:when>
  					<c:otherwise>
  						<div class="text-center lh-typo__commontitle lh-typo__commontitle--1">${commerceText.v501003}</div>
	                    <div class="lh-form-tc__range">
	                            <div class="lh-range">
	                              <input class="lh-range__input" type="hidden" autocomplete="off" value="0" data-range-min="${pasoBean.productoBean.lineaCreditoMinima}" data-range-max="${pasoBean.productoBean.lineaCredito}" data-range-step="${renderRequest.preferences.getValue('configuracionRangeStepLine','')}">
	                              <div class="lh-range__track">
	                                <div class="lh-range__distance">
	                                  <div class="lh-range__handle"></div>
	                                </div>
	                              </div>
	                              <output class="lh-range__indicator" name="creditLineAux" id="creditLineAux"><small class="lh-typo__label lh-range__value nowrap" data-prefix="S/ " data-suffix="" data-number-decimal="false"></small></output>
	                            </div>
	                      <div class="lh-form-tc__range__amounts"><span class="lh-typo__label">S/ ${pasoBean.productoBean.lineaCreditoMinima}</span><span class="lh-typo__label">S/ ${pasoBean.productoBean.lineaCredito}</span></div>
	                      <input id="creditLine" type="hidden" name="creditLine" value=""/>
	                    </div>
  					</c:otherwise>
  					</c:choose>
                  </div>
<!--                   <div class="lh-ff-section lh-ff-delivery" data-lightbox-target="#address__lb" data-address-request="3"> -->
                  <div class="lh-ff-section lh-ff-delivery" data-address-request="3">
                    <h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">${renderRequest.preferences.getValue('configuracionDatosEntrega001','')}</h4>
                          <div class="lh-ff-group">
                                  <div class="lh-ff-group__item lh-ff-delivery__address" id="lh-ff-delivery-address">
                                    <label class="lh-ff__label">${renderRequest.preferences.getValue('configuracionText002','')}</label>
                                    <div class="lh-ff-group__field">
                                      <c:forEach items="${pasoBean.clienteBean.direcciones.direccion}" var="lstDirecciones" varStatus="direcciones">
										   <div class="lh-form__radio">
		                                        	<input id="fct_deliveryaddress" type="radio" name="deliveryaddress" value="${direcciones.index}" data-related="#lh-ff-delivery-date" <c:if test="${lstDirecciones.codigoUso == 'PRINCI'}"> checked </c:if>>
		                                        	<label for="fct_deliveryaddress"><span>${lstDirecciones.addressDetail}</span></label>
		                                      	</div>
					                    </c:forEach> 
                                      <div class="lh-form__radio lh-ff-delivery__address-new">
                                        <input id="fct_deliveryaddress1" type="radio" name="deliveryaddress" value="" data-related="#lh-ff-delivery-add-address" data-disabled="">
                                        <label for="fct_deliveryaddress1"><span>${renderRequest.preferences.getValue('configuracionDatosEntrega002','')}</span></label>
                                      </div>
                                    </div>
                                  </div>
                          </div>
                          <div class="lh-ff-related-content lh-ff-related-content--nested lh-ff-delivery__add-address" id="lh-ff-delivery-add-address" data-remote-action="${addAddressTC}">
                                  <div class="lh-ff-group lh-ff-group__ubigeo">
                                                <div class="lh-ff-group__item">
                                                  <label class="lh-ff__label" for="fct-addressDepartamento">Departamento
                                                  </label>
                                                              <div class="lh-ff-group__field">
                                                                      <div class="lh__select lh__select--checked">
                                                                        <select class="ff_selectDepartamento" id="fct-addressDepartamento" name="addressDepartamento" data-validate-required data-validate-required-message="Selecciona un departamento">
                                                                          <option value disabled selected>Seleccione</option>
                                                                          	<c:forEach items="${lstDepartamentos}" var="depar">
																		    	<option value="${depar.key}" >${depar.value}</option>
																	  		</c:forEach>
                                                                        </select>
                                                                      </div>
                                                              </div>
                                                </div>
                                                <div class="lh-ff-group__item">
                                                  <label class="lh-ff__label" for="fct-addressProvincia">Provincia
                                                  </label>
                                                              <div class="lh-ff-group__field">
                                                                      <div class="lh__select lh__select--checked lh__select__disabled">
                                                                        <select class="ff_selectProvincia" id="fct-addressProvincia" name="addressProvincia" disabled data-validate-required data-validate-required-message="Selecciona una Provincia" data-remote="${getListUbigeo}&tipo=1&parent=" data-remote-bind="#fct-addressDepartamento">
                                                                          <option value disabled selected>Seleccione</option>
                                                                        </select>
                                                                      </div>
                                                              </div>
                                                </div>
                                                <div class="lh-ff-group__item">
                                                  <label class="lh-ff__label" for="fct-addressDistrito">Distrito
                                                  </label>
                                                              <div class="lh-ff-group__field">
                                                                      <div class="lh__select lh__select--checked lh__select__disabled">
                                                                        <select class="ff_selectDistrito" id="fct-addressDistrito" name="addressDistrito" disabled data-validate-required data-validate-required-message="Selecciona un Distrito" data-remote="${getListUbigeo}&tipo=2&parent=" data-remote-bind="#fct-addressProvincia">
                                                                          <option value disabled selected>Seleccione</option>
                                                                        </select>
                                                                      </div>
                                                              </div>
                                                </div>
                                  </div>
                                        <div class="lh-ff-group lh-ff-group--custom">
                                                      <div class="lh-ff-group__item lh-ff-group__item--xs">
                                                        <label class="lh-ff__label" for="fct-addressType">Tipo de vía
                                                        </label>
                                                                    <div class="lh-ff-group__field">
                                                                            <div class="lh__select lh__select--checked">
                                                                              <select id="fct-addressType" name="addressType" data-validate-required data-validate-required-message="Seleccione un tipo de vía">
                                                                                <option value disabled selected>Seleccione</option>
                                                                                <c:forEach items="${requestScope['lstTipoVia']}" var="via">
										                                   			<option value="${via.key}" >${via.value}</option>
										                           				</c:forEach>
                                                                              </select>
                                                                            </div>
                                                                    </div>
                                                      </div>
                                                      <div class="lh-ff-group__item">
                                                        <label class="lh-ff__label" for="fct-addressName">Nombre de la vía
                                                        </label>
                                                                    <div class="lh-ff-group__field">
                                                                            <input class="lh-input" id="fct-addressName" maxlength="37" name="addressName" data-allowed-keys="letter" data-allowed-keys-add="0-9 " data-validate-required data-validate-required-message="Ingrese el nombre de la vía" type="text">
                                                                    </div>
                                                      </div>
                                        </div>
                                  <div class="lh-ff-group-wrapper lh-ff-address-info">
                                          <div class="lh-ff-group lh-ff-group--small-fields">
                                                        <div class="lh-ff-group__item">
                                                          <label class="lh-ff__label" for="fct-addressNro">Número
                                                          </label>
                                                                      <div class="lh-ff-group__field">
                                                                              <input class="lh-input lh-ff-address-info__numero" id="fct-addressNro" maxlength="5" name="addressNro" type="tel" inputmode="numeric" pattern="[0-9]*" data-allowed-keys="integer" data-validate>
                                                                      </div>
                                                        </div>
                                                        <div class="lh-ff-group__item">
                                                          <label class="lh-ff__label" for="fct-addressMz">Manzana
                                                          </label>
                                                                      <div class="lh-ff-group__field">
                                                                              <input class="lh-input lh-ff-address-info__mnza" id="fct-addressMz" maxlength="5" name="addressMz" data-allowed-keys="alphanumeric" data-allowed-keys-add="ñÑ" data-validate type="text">
                                                                      </div>
                                                        </div>
                                                        <div class="lh-ff-group__item">
                                                          <label class="lh-ff__label" for="fct-addressLt">Lote
                                                          </label>
                                                                      <div class="lh-ff-group__field">
                                                                              <input class="lh-input lh-ff-address-info__lote" id="fct-addressLt" maxlength="5" name="addressLt" data-allowed-keys="alphanumeric" data-allowed-keys-add="ñÑ" data-validate type="text">
                                                                      </div>
                                                        </div>
                                                        <div class="lh-ff-group__item">
                                                          <label class="lh-ff__label" for="fct-addressInt">Interior
                                                          </label>
                                                                      <div class="lh-ff-group__field">
                                                                              <input class="lh-input lh-ff-address-info__interior" id="fct-addressInt" maxlength="5" name="addressInt" data-allowed-keys="alphanumeric" data-allowed-keys-add="ñÑ" data-validate type="text">
                                                                      </div>
                                                        </div>
                                          </div>
                                    <input type="hidden" data-validate="{&quot;errorContainer&quot;:&quot;.lh-ff-address-info&quot;}" data-validate-custom-address-info data-validate-custom-address-info-message="Debe ingresar una dirección completa">
                                  </div>
                                  <div class="lh-ff-group">
                                    <div class="lh-ff-group__item lh-ff-group__item--buttons">
                                      <button class="lh-btn lh-btn--secondary lh-btn--medium lh-btn--fill lh-btn--fill--blue lh-btn--block-xxs" type="button"><span>Guardar dirección</span></button>
                                    </div>
                                  </div>
                            <div class="lh-ff-delivery__error">
                              <div class="lh-typo__obs lh-ff-delivery__error-message" data-message="Hubo un problema en el servidor. Por favor, inténtalo nuevamente."></div>
                            </div>
                          </div>
                          <c:if test="${requestScope.code==1}">
                          		<div class="lh-ff-related-content" id="lh-ff-delivery-date">
                            		<div class="lh-ff-delivery__date lh-ff-related-content--active">
	                                    <div class="lh-ff-group">
	                                                  <div class="lh-ff-group__item lh-ff-group__item--md">
	                                                    <label class="lh-ff__label" for="fct_deliverydate">Fecha de entrega
	                                                    </label>
	                                                                <div class="lh-ff-group__field">
	                                                                        <div class="lh__select lh__select--checked">
	                                                                          <select class="lh-ff-delivery__date-dates" id="fct_deliverydate" name="deliverydate" data-validate-required data-validate-required-message="Selecciona un día de entrega">
	                                                                          		${requestScope.dates}
	                                                                          </select>
	                                                                        </div>
	                                                                </div>
	                                                  </div>
	                                    </div>
	                                    <div class="lh-ff-group">
	                                            <div class="lh-ff-group__item lh-ff-delivery__date-schedule">
	                                              <label class="lh-ff__label">Horario de entrega</label>
	                                              <div class="lh-ff-group__field">
	                                              		<input id="fct_hiddenDeliveryTime" type="hidden" name="deliveryTime" data-validate-required="radio" data-validate-required-message="Seleccione su horario de entrega"/>
	                                                      ${requestScope.schedule}
	                                              </div>
	                                            </div>
	                                    </div>
                            		</div>
                          		</div>
		                    </c:if>
		                    <template id="delivery-date-block">
		                            <div class="lh-ff-related-content">
		                              <div class="lh-ff-delivery__date lh-ff-related-content--active">
		                                      <div class="lh-ff-group">
		                                                    <div class="lh-ff-group__item lh-ff-group__item--md">
		                                                      <label class="lh-ff__label" for="fct_deliverydate_x">Fecha de entrega
		                                                      </label>
		                                                                  <div class="lh-ff-group__field">
		                                                                          <div class="lh__select lh__select--checked">
		                                                                            <select class="lh-ff-delivery__date-dates"  id="fct_deliverydate_x" name="deliverydate" data-validate-required data-validate-required-message="Selecciona un día de entrega">
		                                                                            </select>
		                                                                          </div>
		                                                                  </div>
		                                                    </div>
		                                      </div>
		                                      <div class="lh-ff-group">
		                                              <div class="lh-ff-group__item lh-ff-delivery__date-schedule">
		                                                <label class="lh-ff__label">Horario de entrega</label>
		                                                <div class="lh-ff-group__field">
		                                                	<input id="fct_hiddenDeliveryTime_x" type="hidden" name="deliveryTime" data-validate-required="radio" data-validate-required-message="Seleccione su horario de entrega"/>
		                                                </div>
		                                              </div>
		                                      </div>
		                              </div>
		                            </div>
		                    </template>
                          <div class="lh-ff-group">
                                        <div class="lh-ff-group__item lh-ff-group__item--fixed lh-ff-group__item--lg lh-ff-group__item--wtooltip">
                                          <label class="lh-ff__label" for="fct_emailAddress">${renderRequest.preferences.getValue('configuracionText003','')}
                                          </label>
                                          <div class="lh-ff__field-wtooltip">
                                                        <div class="lh-ff-group__field">
                                                                <input class="lh-input" id="fct_emailAddress" maxlength="60" name="emailStateAccount" type="email" data-no-paste data-allowed-keys="email" data-validate-required data-validate-required-message="${renderRequest.preferences.getValue('inicioEmailAddressRequiredMessage','')}" data-validate-email data-validate-email-message="${renderRequest.preferences.getValue('inicioEmailAddressValidMessage','')}" value="${pasoBean.clienteBean.emailAddress}">
                                                        </div>
                                                  <div class="lh-tooltip-info__container">
                                                    <div class="lh-tooltip-info lh-tooltip-info--right2"><span class="lh-tooltip-info__button"><i class="lh-icon-i"></i></span>
                                                      <div class="lh-tooltip-info__content"><span class="lh-box">${commerceText.v501004}</span></div>
                                                    </div>
                                                  </div>
                                          </div>
                                        </div>
                          </div>
                    <div class="lh-ff-group">
                      <div class="lh-ff-group__item lh-ff-group__item--terms">
                      <span>
                      	${commerceText.v501005}
                      </span></div>
                    </div>
                          <div class="lh-ff-group">
                            <div class="lh-ff-group__item lh-ff-group__item--buttons">
                              	<button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit lh-btn-config">
	                              	<span>
										${commerceText.v501006}
									</span>
								</button>
                            </div>
                          </div>
                          <div class="lh-form__error">
                            <div class="lh-typo__p2 lh-form__error-message" data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
                          </div>
                  </div>
                </form:form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
			<div class="lh-lightbox" id="lb-terms-conditions-lf">
	              ${commerceText.v501007}
            </div>
		<div class="lh-lightbox lh-lightbox--modal" id="address__lb">
	        ${commerceText.v501008}
      	</div>