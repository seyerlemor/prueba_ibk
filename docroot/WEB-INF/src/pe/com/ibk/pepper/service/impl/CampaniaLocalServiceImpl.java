/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;
import java.util.List;

import pe.com.ibk.pepper.NoSuchCampaniaException;
import pe.com.ibk.pepper.model.Campania;
import pe.com.ibk.pepper.service.base.CampaniaLocalServiceBaseImpl;

/**
 * The implementation of the campania local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.CampaniaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.CampaniaLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.CampaniaLocalServiceUtil
 */
public class CampaniaLocalServiceImpl extends CampaniaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.CampaniaLocalServiceUtil} to access the campania local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(CampaniaLocalServiceImpl.class);
	@Override
	public Campania registrarCampania(Campania campania) throws SystemException{
		
		Date now = DateUtil.newDate();
		if (campania.isNew()) {
			campania.setFechaRegistro(now);
		}
		campaniaPersistence.update(campania);
		
		return campania;
	}
	
	@Override
	public Campania buscarCampaniaporId(long idCampania){
		Campania campania = null;
		try {
			campania = campaniaPersistence.findByC_ID(idCampania);
		} catch (NoSuchCampaniaException e) {
			_log.error("Error en clase ExpedienteLocalServiceImpl metodo buscarExpedienteporId:: NO EXISTE EL EXPEDIENTE");
		} catch (SystemException e) {
			_log.error("Error en clase ExpedienteLocalServiceImpl metodo buscarExpedienteporId::", e );
		}
		return campania;
	}
	
	public List<Campania> getCampaniasByExpediente(long idExpediente) throws SystemException{
		return campaniaPersistence.findByIdExpediente(idExpediente);
	}
}