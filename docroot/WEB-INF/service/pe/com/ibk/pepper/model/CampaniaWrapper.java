/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Campania}.
 * </p>
 *
 * @author Interbank
 * @see Campania
 * @generated
 */
public class CampaniaWrapper implements Campania, ModelWrapper<Campania> {
	public CampaniaWrapper(Campania campania) {
		_campania = campania;
	}

	@Override
	public Class<?> getModelClass() {
		return Campania.class;
	}

	@Override
	public String getModelClassName() {
		return Campania.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCampania", getIdCampania());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("codigoCampania", getCodigoCampania());
		attributes.put("nombreCampania", getNombreCampania());
		attributes.put("codigoOferta", getCodigoOferta());
		attributes.put("nombreOferta", getNombreOferta());
		attributes.put("codigoTratamiento", getCodigoTratamiento());
		attributes.put("nombreTratamiento", getNombreTratamiento());
		attributes.put("codigoUnico", getCodigoUnico());
		attributes.put("codigoProducto", getCodigoProducto());
		attributes.put("core", getCore());
		attributes.put("nombreProducto", getNombreProducto());
		attributes.put("tipoCampania", getTipoCampania());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idCampania = (Long)attributes.get("idCampania");

		if (idCampania != null) {
			setIdCampania(idCampania);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String codigoCampania = (String)attributes.get("codigoCampania");

		if (codigoCampania != null) {
			setCodigoCampania(codigoCampania);
		}

		String nombreCampania = (String)attributes.get("nombreCampania");

		if (nombreCampania != null) {
			setNombreCampania(nombreCampania);
		}

		String codigoOferta = (String)attributes.get("codigoOferta");

		if (codigoOferta != null) {
			setCodigoOferta(codigoOferta);
		}

		String nombreOferta = (String)attributes.get("nombreOferta");

		if (nombreOferta != null) {
			setNombreOferta(nombreOferta);
		}

		String codigoTratamiento = (String)attributes.get("codigoTratamiento");

		if (codigoTratamiento != null) {
			setCodigoTratamiento(codigoTratamiento);
		}

		String nombreTratamiento = (String)attributes.get("nombreTratamiento");

		if (nombreTratamiento != null) {
			setNombreTratamiento(nombreTratamiento);
		}

		String codigoUnico = (String)attributes.get("codigoUnico");

		if (codigoUnico != null) {
			setCodigoUnico(codigoUnico);
		}

		String codigoProducto = (String)attributes.get("codigoProducto");

		if (codigoProducto != null) {
			setCodigoProducto(codigoProducto);
		}

		String core = (String)attributes.get("core");

		if (core != null) {
			setCore(core);
		}

		String nombreProducto = (String)attributes.get("nombreProducto");

		if (nombreProducto != null) {
			setNombreProducto(nombreProducto);
		}

		String tipoCampania = (String)attributes.get("tipoCampania");

		if (tipoCampania != null) {
			setTipoCampania(tipoCampania);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this campania.
	*
	* @return the primary key of this campania
	*/
	@Override
	public long getPrimaryKey() {
		return _campania.getPrimaryKey();
	}

	/**
	* Sets the primary key of this campania.
	*
	* @param primaryKey the primary key of this campania
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_campania.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id campania of this campania.
	*
	* @return the id campania of this campania
	*/
	@Override
	public long getIdCampania() {
		return _campania.getIdCampania();
	}

	/**
	* Sets the id campania of this campania.
	*
	* @param idCampania the id campania of this campania
	*/
	@Override
	public void setIdCampania(long idCampania) {
		_campania.setIdCampania(idCampania);
	}

	/**
	* Returns the id expediente of this campania.
	*
	* @return the id expediente of this campania
	*/
	@Override
	public long getIdExpediente() {
		return _campania.getIdExpediente();
	}

	/**
	* Sets the id expediente of this campania.
	*
	* @param idExpediente the id expediente of this campania
	*/
	@Override
	public void setIdExpediente(long idExpediente) {
		_campania.setIdExpediente(idExpediente);
	}

	/**
	* Returns the codigo campania of this campania.
	*
	* @return the codigo campania of this campania
	*/
	@Override
	public java.lang.String getCodigoCampania() {
		return _campania.getCodigoCampania();
	}

	/**
	* Sets the codigo campania of this campania.
	*
	* @param codigoCampania the codigo campania of this campania
	*/
	@Override
	public void setCodigoCampania(java.lang.String codigoCampania) {
		_campania.setCodigoCampania(codigoCampania);
	}

	/**
	* Returns the nombre campania of this campania.
	*
	* @return the nombre campania of this campania
	*/
	@Override
	public java.lang.String getNombreCampania() {
		return _campania.getNombreCampania();
	}

	/**
	* Sets the nombre campania of this campania.
	*
	* @param nombreCampania the nombre campania of this campania
	*/
	@Override
	public void setNombreCampania(java.lang.String nombreCampania) {
		_campania.setNombreCampania(nombreCampania);
	}

	/**
	* Returns the codigo oferta of this campania.
	*
	* @return the codigo oferta of this campania
	*/
	@Override
	public java.lang.String getCodigoOferta() {
		return _campania.getCodigoOferta();
	}

	/**
	* Sets the codigo oferta of this campania.
	*
	* @param codigoOferta the codigo oferta of this campania
	*/
	@Override
	public void setCodigoOferta(java.lang.String codigoOferta) {
		_campania.setCodigoOferta(codigoOferta);
	}

	/**
	* Returns the nombre oferta of this campania.
	*
	* @return the nombre oferta of this campania
	*/
	@Override
	public java.lang.String getNombreOferta() {
		return _campania.getNombreOferta();
	}

	/**
	* Sets the nombre oferta of this campania.
	*
	* @param nombreOferta the nombre oferta of this campania
	*/
	@Override
	public void setNombreOferta(java.lang.String nombreOferta) {
		_campania.setNombreOferta(nombreOferta);
	}

	/**
	* Returns the codigo tratamiento of this campania.
	*
	* @return the codigo tratamiento of this campania
	*/
	@Override
	public java.lang.String getCodigoTratamiento() {
		return _campania.getCodigoTratamiento();
	}

	/**
	* Sets the codigo tratamiento of this campania.
	*
	* @param codigoTratamiento the codigo tratamiento of this campania
	*/
	@Override
	public void setCodigoTratamiento(java.lang.String codigoTratamiento) {
		_campania.setCodigoTratamiento(codigoTratamiento);
	}

	/**
	* Returns the nombre tratamiento of this campania.
	*
	* @return the nombre tratamiento of this campania
	*/
	@Override
	public java.lang.String getNombreTratamiento() {
		return _campania.getNombreTratamiento();
	}

	/**
	* Sets the nombre tratamiento of this campania.
	*
	* @param nombreTratamiento the nombre tratamiento of this campania
	*/
	@Override
	public void setNombreTratamiento(java.lang.String nombreTratamiento) {
		_campania.setNombreTratamiento(nombreTratamiento);
	}

	/**
	* Returns the codigo unico of this campania.
	*
	* @return the codigo unico of this campania
	*/
	@Override
	public java.lang.String getCodigoUnico() {
		return _campania.getCodigoUnico();
	}

	/**
	* Sets the codigo unico of this campania.
	*
	* @param codigoUnico the codigo unico of this campania
	*/
	@Override
	public void setCodigoUnico(java.lang.String codigoUnico) {
		_campania.setCodigoUnico(codigoUnico);
	}

	/**
	* Returns the codigo producto of this campania.
	*
	* @return the codigo producto of this campania
	*/
	@Override
	public java.lang.String getCodigoProducto() {
		return _campania.getCodigoProducto();
	}

	/**
	* Sets the codigo producto of this campania.
	*
	* @param codigoProducto the codigo producto of this campania
	*/
	@Override
	public void setCodigoProducto(java.lang.String codigoProducto) {
		_campania.setCodigoProducto(codigoProducto);
	}

	/**
	* Returns the core of this campania.
	*
	* @return the core of this campania
	*/
	@Override
	public java.lang.String getCore() {
		return _campania.getCore();
	}

	/**
	* Sets the core of this campania.
	*
	* @param core the core of this campania
	*/
	@Override
	public void setCore(java.lang.String core) {
		_campania.setCore(core);
	}

	/**
	* Returns the nombre producto of this campania.
	*
	* @return the nombre producto of this campania
	*/
	@Override
	public java.lang.String getNombreProducto() {
		return _campania.getNombreProducto();
	}

	/**
	* Sets the nombre producto of this campania.
	*
	* @param nombreProducto the nombre producto of this campania
	*/
	@Override
	public void setNombreProducto(java.lang.String nombreProducto) {
		_campania.setNombreProducto(nombreProducto);
	}

	/**
	* Returns the tipo campania of this campania.
	*
	* @return the tipo campania of this campania
	*/
	@Override
	public java.lang.String getTipoCampania() {
		return _campania.getTipoCampania();
	}

	/**
	* Sets the tipo campania of this campania.
	*
	* @param tipoCampania the tipo campania of this campania
	*/
	@Override
	public void setTipoCampania(java.lang.String tipoCampania) {
		_campania.setTipoCampania(tipoCampania);
	}

	/**
	* Returns the fecha registro of this campania.
	*
	* @return the fecha registro of this campania
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _campania.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this campania.
	*
	* @param fechaRegistro the fecha registro of this campania
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_campania.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _campania.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_campania.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _campania.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_campania.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _campania.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _campania.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_campania.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _campania.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_campania.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_campania.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_campania.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CampaniaWrapper((Campania)_campania.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.Campania campania) {
		return _campania.compareTo(campania);
	}

	@Override
	public int hashCode() {
		return _campania.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.Campania> toCacheModel() {
		return _campania.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.Campania toEscapedModel() {
		return new CampaniaWrapper(_campania.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.Campania toUnescapedModel() {
		return new CampaniaWrapper(_campania.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _campania.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _campania.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_campania.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CampaniaWrapper)) {
			return false;
		}

		CampaniaWrapper campaniaWrapper = (CampaniaWrapper)obj;

		if (Validator.equals(_campania, campaniaWrapper._campania)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Campania getWrappedCampania() {
		return _campania;
	}

	@Override
	public Campania getWrappedModel() {
		return _campania;
	}

	@Override
	public void resetOriginalValues() {
		_campania.resetOriginalValues();
	}

	private Campania _campania;
}