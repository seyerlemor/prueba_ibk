/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.DireccionCliente;

/**
 * The persistence interface for the direccion cliente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see DireccionClientePersistenceImpl
 * @see DireccionClienteUtil
 * @generated
 */
public interface DireccionClientePersistence extends BasePersistence<DireccionCliente> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DireccionClienteUtil} to access the direccion cliente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the direccion cliente where idDireccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	*
	* @param idDireccion the id direccion
	* @return the matching direccion cliente
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente findByDC_ID(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException;

	/**
	* Returns the direccion cliente where idDireccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idDireccion the id direccion
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente fetchByDC_ID(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the direccion cliente where idDireccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idDireccion the id direccion
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente fetchByDC_ID(
		long idDireccion, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the direccion cliente where idDireccion = &#63; from the database.
	*
	* @param idDireccion the id direccion
	* @return the direccion cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente removeByDC_ID(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException;

	/**
	* Returns the number of direccion clientes where idDireccion = &#63;.
	*
	* @param idDireccion the id direccion
	* @return the number of matching direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countByDC_ID(long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the direccion cliente where codigoUso = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	*
	* @param codigoUso the codigo uso
	* @return the matching direccion cliente
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente findByDC_CU(
		java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException;

	/**
	* Returns the direccion cliente where codigoUso = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoUso the codigo uso
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente fetchByDC_CU(
		java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the direccion cliente where codigoUso = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoUso the codigo uso
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente fetchByDC_CU(
		java.lang.String codigoUso, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the direccion cliente where codigoUso = &#63; from the database.
	*
	* @param codigoUso the codigo uso
	* @return the direccion cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente removeByDC_CU(
		java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException;

	/**
	* Returns the number of direccion clientes where codigoUso = &#63;.
	*
	* @param codigoUso the codigo uso
	* @return the number of matching direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countByDC_CU(java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the direccion cliente in the entity cache if it is enabled.
	*
	* @param direccionCliente the direccion cliente
	*/
	public void cacheResult(
		pe.com.ibk.pepper.model.DireccionCliente direccionCliente);

	/**
	* Caches the direccion clientes in the entity cache if it is enabled.
	*
	* @param direccionClientes the direccion clientes
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.DireccionCliente> direccionClientes);

	/**
	* Creates a new direccion cliente with the primary key. Does not add the direccion cliente to the database.
	*
	* @param idDireccion the primary key for the new direccion cliente
	* @return the new direccion cliente
	*/
	public pe.com.ibk.pepper.model.DireccionCliente create(long idDireccion);

	/**
	* Removes the direccion cliente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDireccion the primary key of the direccion cliente
	* @return the direccion cliente that was removed
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente remove(long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException;

	public pe.com.ibk.pepper.model.DireccionCliente updateImpl(
		pe.com.ibk.pepper.model.DireccionCliente direccionCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the direccion cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	*
	* @param idDireccion the primary key of the direccion cliente
	* @return the direccion cliente
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente findByPrimaryKey(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException;

	/**
	* Returns the direccion cliente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDireccion the primary key of the direccion cliente
	* @return the direccion cliente, or <code>null</code> if a direccion cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DireccionCliente fetchByPrimaryKey(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the direccion clientes.
	*
	* @return the direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.DireccionCliente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the direccion clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of direccion clientes
	* @param end the upper bound of the range of direccion clientes (not inclusive)
	* @return the range of direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.DireccionCliente> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the direccion clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of direccion clientes
	* @param end the upper bound of the range of direccion clientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.DireccionCliente> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the direccion clientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of direccion clientes.
	*
	* @return the number of direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}