package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.util.CookieKeys;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpServletRequest;

public abstract class SessionKeys {
	
	public static final int SESSION_PORTLET = PortletSession.PORTLET_SCOPE;
	public static final int SESSION_PORTAL = PortletSession.APPLICATION_SCOPE;
	public static final String SOLICITUD_BEAN = "LIFERAY_SHARED_SOLICITUD_BEAN";
	public static final String URL_PRODUCTO = "LIFERAY_SHARED_URL_PRODUCTO";
	public static final String FLAG_RESTAURACION = "LIFERAY_SHARED_RESTAURACION";
	public static final String PASO_BEAN = "LIFERAY_SHARED_PASO_BEAN";
	public static final String USUARIO_BEAN = "LIFERAY_SHARED_USUARIO_BEAN";
	public static final String TIPO_FLUJO = "LIFERAY_SHARED_TIPO_FLUJO";
	public static final String TIPO_FLUJO_SELECCIONADO = "LIFERAY_SHARED_TIPO_FLUJO_SELECCIONADO";

	public static final String LIST_TIPO_ESTADO = "LIFERAY_SHARED_LIST_TIPO_ESTADO";
	public static final String LIST_REPORTE_BACKEND = "LIFERAY_SHARED_LIST_REPORTE_BACKEND";
	
	public static final String FLAG_PASO_2 = "LIFERAY_SHARED_FLAG_PASO_2";
	public static final String FLAG_REST_VTC = "LIFERAY_SHARED_FLAG_REST_VTC";
	public static final String FLAG_TE_AYUDAMOS = "LIFERAY_SHARED_FLAG_TE_AYUDAMOS";
	
	public static final String URL_ESTABLECIMIENTO = "LIFERAY_SHARED_URL_ESTABLECIMIENTO";
	public static final String REGISTRO_LOGIN = "LIFERAY_SHARED_REGISTRO_LOGIN";
	public static final String IP = "LIFERAY_SHARED_IP";
	
	public static final String COMERCIO_BEAN = "LIFERAY_SHARED_COMERCIO_BEAN";
	public static final String FLAG_PAN = "LIFERAY_SHARED_FLAG_PAN";
	
	public static final String MESSAGE_ID = "LIFERAY_SHARED_MESSAGE_ID";
	
	private SessionKeys() {
	}
	
	public static void addSessionPortal(PortletRequest portletRequest,String llave,Object valor){
		addSession(portletRequest, llave, valor, SESSION_PORTAL);
	}
	
	public static Object getSessionPortal(PortletRequest portletRequest,String llave){
		return getSession(portletRequest, llave, SESSION_PORTAL);
	}
	
	public static void addSessionPortlet(PortletRequest portletRequest,String llave,Object valor){
		addSession(portletRequest, llave, valor, SESSION_PORTLET);
	}
	
	public static Object getSessionPortlet(PortletRequest portletRequest,String llave){
		return getSession(portletRequest, llave, SESSION_PORTLET);
	}
	
	public static void addSession(PortletRequest portletRequest,String llave,Object valor, int tipo){
		PortletSession portletSession =  portletRequest.getPortletSession();
		portletSession.setAttribute(llave, valor, tipo);
	}
	
	public static Object getSession(PortletRequest portletRequest,String llave, int tipo){
		PortletSession portletSession =  portletRequest.getPortletSession();
		return portletSession.getAttribute(llave , tipo);
	}
	
	public static void removeSessionPortlet(PortletRequest portletRequest,String llave){
		  removeSession(portletRequest, llave, SESSION_PORTLET);
	}
	
	public static void removeSessionPortal(PortletRequest portletRequest,String llave){
		 removeSession(portletRequest, llave, SESSION_PORTAL);
	}
	
	public static void removeSession(PortletRequest portletRequest,String llave, int tipo){
		PortletSession portletSession =  portletRequest.getPortletSession();
		portletSession.removeAttribute(llave, tipo);
	}
	
	public static String getSessionId(HttpServletRequest request){
		return CookieKeys.getCookie(request, CookieKeys.JSESSIONID);
	}
	

}
