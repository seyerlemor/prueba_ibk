
package pe.com.ibk.pepper.rest.equifax.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Candidato {

    private String tipoDocumento;
    private String numeroDocumento;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String inicioValidacion;
    private String codigoInterno;
    private String codigoInterno1;
    private String codigoInterno2;
    
    @JsonCreator
    public Candidato(String tipoDocumento, String numeroDocumento,
			String nombres, String apellidoPaterno, String apellidoMaterno,
			String inicioValidacion, String codigoInterno,
			String codigoInterno1, String codigoInterno2) {
		super();
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.nombres = nombres;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.inicioValidacion = inicioValidacion;
		this.codigoInterno = codigoInterno;
		this.codigoInterno1 = codigoInterno1;
		this.codigoInterno2 = codigoInterno2;
	}

    @JsonProperty("tipoDocumento")
	public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @JsonProperty("numeroDocumento")
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @JsonProperty("nombres")
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @JsonProperty("apellidoPaterno")
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    @JsonProperty("apellidoMaterno")
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    @JsonProperty("inicioValidacion")
    public String getInicioValidacion() {
        return inicioValidacion;
    }

    public void setInicioValidacion(String inicioValidacion) {
        this.inicioValidacion = inicioValidacion;
    }

    @JsonProperty("codigoInterno")
    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    @JsonProperty("codigoInterno2")
    public String getCodigoInterno1() {
        return codigoInterno1;
    }

    public void setCodigoInterno1(String codigoInterno1) {
        this.codigoInterno1 = codigoInterno1;
    }

    @JsonProperty("codigoInterno2")
    public String getCodigoInterno2() {
        return codigoInterno2;
    }

    public void setCodigoInterno2(String codigoInterno2) {
        this.codigoInterno2 = codigoInterno2;
    }

}
