
package pe.com.ibk.pepper.rest.campania.response;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Treatment implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("name")
    @Expose
    private String name;
    private final static long serialVersionUID = 6020080962621754900L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Treatment() {
    }

    /**
     * 
     * @param number
     * @param name
     * @param id
     */
    public Treatment(String id, String number, String name) {
        super();
        this.id = id;
        this.number = number;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Treatment withId(String id) {
        this.id = id;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Treatment withNumber(String number) {
        this.number = number;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Treatment withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("number", number).append("name", name).toString();
    }

}
