package pe.com.ibk.pepper.rest.fechahoraentrega.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ConsultarFechaHoraEntregaResponse implements Serializable{
	
	private static final long serialVersionUID = -1284580319495070536L;

	@SerializedName(value="codigoCourier")
	private String codigoCourier;
	
	@SerializedName(value="nombreCourier")
	private String nombreCourier;
	
	@SerializedName(value="flagDisponibilidad")
	private String flagDisponibilidad;
	
	@SerializedName(value="horarioInicio")
	private String horarioInicio;
	
	@SerializedName(value="horarioFin")
	private String horarioFin;
	
	@SerializedName(value="diasEntrega")
	private DiasEntrega diasEntrega;
	
	@SerializedName(value="fechasDisponibles")
	private FechasDisponibles fechasDisponibles;
	
	@SerializedName(value="horariosDisponibles")
	private HorariosDisponibles horariosDisponibles;
	
	@SerializedName("courier")
	private Courier courier;
	
	@SerializedName("startTime")
    private String startTime;
	
	@SerializedName("endTime")
    private String endTime;
	
	@SerializedName("weekdays")
    private Weekdays weekdays;
	
	@SerializedName("dates")
    private List<String> dates;
	
	@SerializedName("shifts")
    private List<Shift> shifts;

	public String getCodigoCourier() {
		return codigoCourier;
	}

	public void setCodigoCourier(String codigoCourier) {
		this.codigoCourier = codigoCourier;
	}

	public String getNombreCourier() {
		return nombreCourier;
	}

	public void setNombreCourier(String nombreCourier) {
		this.nombreCourier = nombreCourier;
	}

	public String getFlagDisponibilidad() {
		return flagDisponibilidad;
	}

	public void setFlagDisponibilidad(String flagDisponibilidad) {
		this.flagDisponibilidad = flagDisponibilidad;
	}

	public String getHorarioInicio() {
		return horarioInicio;
	}

	public void setHorarioInicio(String horarioInicio) {
		this.horarioInicio = horarioInicio;
	}

	public String getHorarioFin() {
		return horarioFin;
	}

	public void setHorarioFin(String horarioFin) {
		this.horarioFin = horarioFin;
	}

	public DiasEntrega getDiasEntrega() {
		return diasEntrega;
	}

	public void setDiasEntrega(DiasEntrega diasEntrega) {
		this.diasEntrega = diasEntrega;
	}

	public FechasDisponibles getFechasDisponibles() {
		return fechasDisponibles;
	}

	public void setFechasDisponibles(FechasDisponibles fechasDisponibles) {
		this.fechasDisponibles = fechasDisponibles;
	}

	public HorariosDisponibles getHorariosDisponibles() {
		return horariosDisponibles;
	}

	public void setHorariosDisponibles(HorariosDisponibles horariosDisponibles) {
		this.horariosDisponibles = horariosDisponibles;
	}

	public Courier getCourier() {
		return courier;
	}

	public void setCourier(Courier courier) {
		this.courier = courier;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Weekdays getWeekdays() {
		return weekdays;
	}

	public void setWeekdays(Weekdays weekdays) {
		this.weekdays = weekdays;
	}

	public List<String> getDates() {
		return dates;
	}

	public void setDates(List<String> dates) {
		this.dates = dates;
	}

	public List<Shift> getShifts() {
		return shifts;
	}

	public void setShifts(List<Shift> shifts) {
		this.shifts = shifts;
	}
	
}
