
package pe.com.ibk.pepper.rest.potc.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Productos implements Serializable{

	private static final long serialVersionUID = 7229965779208039531L;
	
	@SerializedName(value="producto")
	private Producto producto;

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}
