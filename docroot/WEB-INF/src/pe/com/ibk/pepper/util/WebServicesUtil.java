package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class WebServicesUtil {
	
	private static final Log logger = LogFactoryUtil.getLog(WebServicesUtil.class);

	private WebServicesUtil(){}
	
	 public static String toXMLGregorianCalendar(Date date){
		 TimeZone timeZone1 = TimeZone.getTimeZone("America/Lima");
		   GregorianCalendar gCalendar = new GregorianCalendar(timeZone1);
	        gCalendar.setTime(date);
	        XMLGregorianCalendar xmlCalendar = null;
	        try {
	            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
	            return xmlCalendar.toString();
	        } catch (DatatypeConfigurationException ex) {
	        	logger.error(ex);
	        	return null;
	        }
	        
	}
	 
}
