<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL id="enviarCodigoOtp" var="enviarCodigoOtp" />
<portlet:resourceURL var="accessStep2" id="accessStep2"></portlet:resourceURL>

	<div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    <liferay-ui:journal-article articleId="${contentStep.articleId}" groupId="${contentStep.groupId}" />
                    <div class="lh-ff-content-col">
                      <div class="lh-ff-section lh-ff-section--notification">
                        <div class="lh-ff-section__wrapper">
                          <div class="lh-ff-section__text">
                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 lh-ff-section__text__title">${renderRequest.preferences.getValue('otpPartTitle','')} ${pasoBean.clienteBean.firstname},</h3>
                            <!-- TARJETA DE CRÉDITO-->
                            <h4 class="lh-typo__commontitle lh-typo__commontitle--1">${renderRequest.preferences.getValue('otpOptionsPartSubTitle001','')}</h4>
                            <p class="lh-typo__p1">${renderRequest.preferences.getValue('otpOptionsPartSubTitle002','')}</p>
                            <div class="lh-gap-15"></div>
                            <form:form   action="${enviarCodigoOtp}" autocomplete="off" commandName="clienteBean" class="lh-ff-form" novalidate="novalidate" >
                              <div class="lh-ff-group">
                              			<div class="lh-ff-group__item lh-ff-group__item--checkbox">
											<div class="lh-ff-group__field">
												<div class="lh-form__checkbox lh-form__checkbox--light">
													<input id="fct_checkSendEmail" type="checkbox" name="checkboxSendEmail" checked> 
													${renderRequest.preferences.getValue('otpOptionsText001','')} ${requestScope.formatEmailAddress}
												</div>
											</div>
										</div>
                                      <div class="lh-ff-group__item">
                                        <div class="lh-ff-group__field">
                                          <input id="optOption" type="hidden" name="radioOTP" data-validate-required="radio">
                                                <div class="lh-form__radio">
                                                  <input id="otpOptions2" type="radio" name="otpOption" value="1" checked="checked" data-related="#ff-operador">
                                                  <label for="otpOptions2"><span>${renderRequest.preferences.getValue('otpOptionsText002','')} ${requestScope.formatCellNumber}</span></label>
                                                </div>
                                                <div class="lh-ff-related-content lh-ff-related-content--nested lh-ff-related-content--active" id="ff-operador">
                                                  <div class="lh-ff-group">
                                                                <div class="lh-ff-group__item lh-ff-group__item--md">
                                                                              <div class="lh-ff-group__field">
                                                                                      <div class="lh__select lh__select--checked">
                                                                                        <select id="ff_operador" name="cellPhoneOperator" data-validate-required data-validate-required-message="${renderRequest.preferences.getValue('otpOperatorRequiredMessage','')}">
                                                                                          <option value="" selected>Seleccionar un operador</option>
                                                                                          <option value="C">CLARO</option>
					                                                                      <option value="E">ENTEL</option>
					                                                                      <option value="M">MOVISTAR</option>
					                                                                      <option value="B">BITEL</option>
                                                                                        </select>
                                                                                      </div>
                                                                              </div>
                                                                </div>
                                                  </div>
                                                </div>
                                        </div>
                                      </div>
                              </div>
                              <div class="lh-ff-group">
                                <div class="lh-ff-group__item lh-ff-group__item--buttons">
                                  <button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit"><span>${renderRequest.preferences.getValue('otpBtnSigamos','')}</span></button>
                                </div>
                              </div>
                                    <div class="lh-form__error">
                                      <div class="lh-typo__p2 lh-form__error-message" data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
                                    </div>
                            </form:form>
                          </div>
                        </div>
                      </div>
                      <div class="lh-ff-section">
                        <div class="lh-ff-section__cols">
                          <div class="lh-ff-section__col">
                            <h4 class="lh-typo__commontitle lh-typo__commontitle--1">${renderRequest.preferences.getValue('otpRedirectText001','')}</h4>
                            <p class="lh-typo__p1">${renderRequest.preferences.getValue('otpRedirectText002','')}</p>
                            <div class="lh-gap-15"></div>
                            <div class="lh-ff-group">
                              <div class="lh-ff-group__item lh-ff-group__item--buttons">
                                <button class="lh-btn lh-btn-group lh-btn--secondary lh-btn--fill lh-btn--fill--blue" id="btnGoToUrl" data-action="${accessStep2}"><span>${renderRequest.preferences.getValue('otpRedirectBtn','')}</span></button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>