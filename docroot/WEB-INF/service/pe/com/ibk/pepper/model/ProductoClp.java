/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.ProductoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ProductoClp extends BaseModelImpl<Producto> implements Producto {
	public ProductoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Producto.class;
	}

	@Override
	public String getModelClassName() {
		return Producto.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idProducto;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdProducto(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idProducto;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idProducto", getIdProducto());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("tipoProducto", getTipoProducto());
		attributes.put("codigoProducto", getCodigoProducto());
		attributes.put("moneda", getMoneda());
		attributes.put("marcaProducto", getMarcaProducto());
		attributes.put("lineaCredito", getLineaCredito());
		attributes.put("diaPago", getDiaPago());
		attributes.put("tasaProducto", getTasaProducto());
		attributes.put("nombreTitularProducto", getNombreTitularProducto());
		attributes.put("tipoSeguro", getTipoSeguro());
		attributes.put("costoSeguro", getCostoSeguro());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("flagEnvioEECCFisico", getFlagEnvioEECCFisico());
		attributes.put("flagEnvioEECCEmail", getFlagEnvioEECCEmail());
		attributes.put("flagTerminos", getFlagTerminos());
		attributes.put("flagCargoCompra", getFlagCargoCompra());
		attributes.put("claveventa", getClaveventa());
		attributes.put("codigoOperacion", getCodigoOperacion());
		attributes.put("fechaHoraOperacion", getFechaHoraOperacion());
		attributes.put("codigoUnico", getCodigoUnico());
		attributes.put("numTarjetaTitular", getNumTarjetaTitular());
		attributes.put("numCuenta", getNumCuenta());
		attributes.put("fechaAltaTitular", getFechaAltaTitular());
		attributes.put("fechaVencTitular", getFechaVencTitular());
		attributes.put("numTarjetaProvisional", getNumTarjetaProvisional());
		attributes.put("fechaAltaProvisional", getFechaAltaProvisional());
		attributes.put("fechaVencProvisional", getFechaVencProvisional());
		attributes.put("RazonNoUsoTarjeta", getRazonNoUsoTarjeta());
		attributes.put("Motivo", getMotivo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idProducto = (Long)attributes.get("idProducto");

		if (idProducto != null) {
			setIdProducto(idProducto);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String tipoProducto = (String)attributes.get("tipoProducto");

		if (tipoProducto != null) {
			setTipoProducto(tipoProducto);
		}

		String codigoProducto = (String)attributes.get("codigoProducto");

		if (codigoProducto != null) {
			setCodigoProducto(codigoProducto);
		}

		String moneda = (String)attributes.get("moneda");

		if (moneda != null) {
			setMoneda(moneda);
		}

		String marcaProducto = (String)attributes.get("marcaProducto");

		if (marcaProducto != null) {
			setMarcaProducto(marcaProducto);
		}

		String lineaCredito = (String)attributes.get("lineaCredito");

		if (lineaCredito != null) {
			setLineaCredito(lineaCredito);
		}

		String diaPago = (String)attributes.get("diaPago");

		if (diaPago != null) {
			setDiaPago(diaPago);
		}

		String tasaProducto = (String)attributes.get("tasaProducto");

		if (tasaProducto != null) {
			setTasaProducto(tasaProducto);
		}

		String nombreTitularProducto = (String)attributes.get(
				"nombreTitularProducto");

		if (nombreTitularProducto != null) {
			setNombreTitularProducto(nombreTitularProducto);
		}

		String tipoSeguro = (String)attributes.get("tipoSeguro");

		if (tipoSeguro != null) {
			setTipoSeguro(tipoSeguro);
		}

		String costoSeguro = (String)attributes.get("costoSeguro");

		if (costoSeguro != null) {
			setCostoSeguro(costoSeguro);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String flagEnvioEECCFisico = (String)attributes.get(
				"flagEnvioEECCFisico");

		if (flagEnvioEECCFisico != null) {
			setFlagEnvioEECCFisico(flagEnvioEECCFisico);
		}

		String flagEnvioEECCEmail = (String)attributes.get("flagEnvioEECCEmail");

		if (flagEnvioEECCEmail != null) {
			setFlagEnvioEECCEmail(flagEnvioEECCEmail);
		}

		String flagTerminos = (String)attributes.get("flagTerminos");

		if (flagTerminos != null) {
			setFlagTerminos(flagTerminos);
		}

		String flagCargoCompra = (String)attributes.get("flagCargoCompra");

		if (flagCargoCompra != null) {
			setFlagCargoCompra(flagCargoCompra);
		}

		String claveventa = (String)attributes.get("claveventa");

		if (claveventa != null) {
			setClaveventa(claveventa);
		}

		String codigoOperacion = (String)attributes.get("codigoOperacion");

		if (codigoOperacion != null) {
			setCodigoOperacion(codigoOperacion);
		}

		Date fechaHoraOperacion = (Date)attributes.get("fechaHoraOperacion");

		if (fechaHoraOperacion != null) {
			setFechaHoraOperacion(fechaHoraOperacion);
		}

		String codigoUnico = (String)attributes.get("codigoUnico");

		if (codigoUnico != null) {
			setCodigoUnico(codigoUnico);
		}

		String numTarjetaTitular = (String)attributes.get("numTarjetaTitular");

		if (numTarjetaTitular != null) {
			setNumTarjetaTitular(numTarjetaTitular);
		}

		String numCuenta = (String)attributes.get("numCuenta");

		if (numCuenta != null) {
			setNumCuenta(numCuenta);
		}

		Date fechaAltaTitular = (Date)attributes.get("fechaAltaTitular");

		if (fechaAltaTitular != null) {
			setFechaAltaTitular(fechaAltaTitular);
		}

		Date fechaVencTitular = (Date)attributes.get("fechaVencTitular");

		if (fechaVencTitular != null) {
			setFechaVencTitular(fechaVencTitular);
		}

		String numTarjetaProvisional = (String)attributes.get(
				"numTarjetaProvisional");

		if (numTarjetaProvisional != null) {
			setNumTarjetaProvisional(numTarjetaProvisional);
		}

		Date fechaAltaProvisional = (Date)attributes.get("fechaAltaProvisional");

		if (fechaAltaProvisional != null) {
			setFechaAltaProvisional(fechaAltaProvisional);
		}

		Date fechaVencProvisional = (Date)attributes.get("fechaVencProvisional");

		if (fechaVencProvisional != null) {
			setFechaVencProvisional(fechaVencProvisional);
		}

		String RazonNoUsoTarjeta = (String)attributes.get("RazonNoUsoTarjeta");

		if (RazonNoUsoTarjeta != null) {
			setRazonNoUsoTarjeta(RazonNoUsoTarjeta);
		}

		String Motivo = (String)attributes.get("Motivo");

		if (Motivo != null) {
			setMotivo(Motivo);
		}
	}

	@Override
	public long getIdProducto() {
		return _idProducto;
	}

	@Override
	public void setIdProducto(long idProducto) {
		_idProducto = idProducto;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdProducto", long.class);

				method.invoke(_productoRemoteModel, idProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdExpediente", long.class);

				method.invoke(_productoRemoteModel, idExpediente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoProducto() {
		return _tipoProducto;
	}

	@Override
	public void setTipoProducto(String tipoProducto) {
		_tipoProducto = tipoProducto;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoProducto", String.class);

				method.invoke(_productoRemoteModel, tipoProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoProducto() {
		return _codigoProducto;
	}

	@Override
	public void setCodigoProducto(String codigoProducto) {
		_codigoProducto = codigoProducto;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoProducto",
						String.class);

				method.invoke(_productoRemoteModel, codigoProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMoneda() {
		return _moneda;
	}

	@Override
	public void setMoneda(String moneda) {
		_moneda = moneda;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setMoneda", String.class);

				method.invoke(_productoRemoteModel, moneda);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMarcaProducto() {
		return _marcaProducto;
	}

	@Override
	public void setMarcaProducto(String marcaProducto) {
		_marcaProducto = marcaProducto;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setMarcaProducto", String.class);

				method.invoke(_productoRemoteModel, marcaProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLineaCredito() {
		return _lineaCredito;
	}

	@Override
	public void setLineaCredito(String lineaCredito) {
		_lineaCredito = lineaCredito;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setLineaCredito", String.class);

				method.invoke(_productoRemoteModel, lineaCredito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDiaPago() {
		return _diaPago;
	}

	@Override
	public void setDiaPago(String diaPago) {
		_diaPago = diaPago;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setDiaPago", String.class);

				method.invoke(_productoRemoteModel, diaPago);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTasaProducto() {
		return _tasaProducto;
	}

	@Override
	public void setTasaProducto(String tasaProducto) {
		_tasaProducto = tasaProducto;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setTasaProducto", String.class);

				method.invoke(_productoRemoteModel, tasaProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreTitularProducto() {
		return _nombreTitularProducto;
	}

	@Override
	public void setNombreTitularProducto(String nombreTitularProducto) {
		_nombreTitularProducto = nombreTitularProducto;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreTitularProducto",
						String.class);

				method.invoke(_productoRemoteModel, nombreTitularProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoSeguro() {
		return _tipoSeguro;
	}

	@Override
	public void setTipoSeguro(String tipoSeguro) {
		_tipoSeguro = tipoSeguro;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoSeguro", String.class);

				method.invoke(_productoRemoteModel, tipoSeguro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCostoSeguro() {
		return _costoSeguro;
	}

	@Override
	public void setCostoSeguro(String costoSeguro) {
		_costoSeguro = costoSeguro;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setCostoSeguro", String.class);

				method.invoke(_productoRemoteModel, costoSeguro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_productoRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagEnvioEECCFisico() {
		return _flagEnvioEECCFisico;
	}

	@Override
	public void setFlagEnvioEECCFisico(String flagEnvioEECCFisico) {
		_flagEnvioEECCFisico = flagEnvioEECCFisico;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagEnvioEECCFisico",
						String.class);

				method.invoke(_productoRemoteModel, flagEnvioEECCFisico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagEnvioEECCEmail() {
		return _flagEnvioEECCEmail;
	}

	@Override
	public void setFlagEnvioEECCEmail(String flagEnvioEECCEmail) {
		_flagEnvioEECCEmail = flagEnvioEECCEmail;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagEnvioEECCEmail",
						String.class);

				method.invoke(_productoRemoteModel, flagEnvioEECCEmail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagTerminos() {
		return _flagTerminos;
	}

	@Override
	public void setFlagTerminos(String flagTerminos) {
		_flagTerminos = flagTerminos;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagTerminos", String.class);

				method.invoke(_productoRemoteModel, flagTerminos);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagCargoCompra() {
		return _flagCargoCompra;
	}

	@Override
	public void setFlagCargoCompra(String flagCargoCompra) {
		_flagCargoCompra = flagCargoCompra;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagCargoCompra",
						String.class);

				method.invoke(_productoRemoteModel, flagCargoCompra);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getClaveventa() {
		return _claveventa;
	}

	@Override
	public void setClaveventa(String claveventa) {
		_claveventa = claveventa;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setClaveventa", String.class);

				method.invoke(_productoRemoteModel, claveventa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoOperacion() {
		return _codigoOperacion;
	}

	@Override
	public void setCodigoOperacion(String codigoOperacion) {
		_codigoOperacion = codigoOperacion;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoOperacion",
						String.class);

				method.invoke(_productoRemoteModel, codigoOperacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaHoraOperacion() {
		return _fechaHoraOperacion;
	}

	@Override
	public void setFechaHoraOperacion(Date fechaHoraOperacion) {
		_fechaHoraOperacion = fechaHoraOperacion;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaHoraOperacion",
						Date.class);

				method.invoke(_productoRemoteModel, fechaHoraOperacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoUnico() {
		return _codigoUnico;
	}

	@Override
	public void setCodigoUnico(String codigoUnico) {
		_codigoUnico = codigoUnico;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoUnico", String.class);

				method.invoke(_productoRemoteModel, codigoUnico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNumTarjetaTitular() {
		return _numTarjetaTitular;
	}

	@Override
	public void setNumTarjetaTitular(String numTarjetaTitular) {
		_numTarjetaTitular = numTarjetaTitular;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumTarjetaTitular",
						String.class);

				method.invoke(_productoRemoteModel, numTarjetaTitular);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNumCuenta() {
		return _numCuenta;
	}

	@Override
	public void setNumCuenta(String numCuenta) {
		_numCuenta = numCuenta;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumCuenta", String.class);

				method.invoke(_productoRemoteModel, numCuenta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaAltaTitular() {
		return _fechaAltaTitular;
	}

	@Override
	public void setFechaAltaTitular(Date fechaAltaTitular) {
		_fechaAltaTitular = fechaAltaTitular;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaAltaTitular",
						Date.class);

				method.invoke(_productoRemoteModel, fechaAltaTitular);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaVencTitular() {
		return _fechaVencTitular;
	}

	@Override
	public void setFechaVencTitular(Date fechaVencTitular) {
		_fechaVencTitular = fechaVencTitular;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaVencTitular",
						Date.class);

				method.invoke(_productoRemoteModel, fechaVencTitular);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNumTarjetaProvisional() {
		return _numTarjetaProvisional;
	}

	@Override
	public void setNumTarjetaProvisional(String numTarjetaProvisional) {
		_numTarjetaProvisional = numTarjetaProvisional;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setNumTarjetaProvisional",
						String.class);

				method.invoke(_productoRemoteModel, numTarjetaProvisional);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaAltaProvisional() {
		return _fechaAltaProvisional;
	}

	@Override
	public void setFechaAltaProvisional(Date fechaAltaProvisional) {
		_fechaAltaProvisional = fechaAltaProvisional;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaAltaProvisional",
						Date.class);

				method.invoke(_productoRemoteModel, fechaAltaProvisional);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaVencProvisional() {
		return _fechaVencProvisional;
	}

	@Override
	public void setFechaVencProvisional(Date fechaVencProvisional) {
		_fechaVencProvisional = fechaVencProvisional;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaVencProvisional",
						Date.class);

				method.invoke(_productoRemoteModel, fechaVencProvisional);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRazonNoUsoTarjeta() {
		return _RazonNoUsoTarjeta;
	}

	@Override
	public void setRazonNoUsoTarjeta(String RazonNoUsoTarjeta) {
		_RazonNoUsoTarjeta = RazonNoUsoTarjeta;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setRazonNoUsoTarjeta",
						String.class);

				method.invoke(_productoRemoteModel, RazonNoUsoTarjeta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMotivo() {
		return _Motivo;
	}

	@Override
	public void setMotivo(String Motivo) {
		_Motivo = Motivo;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setMotivo", String.class);

				method.invoke(_productoRemoteModel, Motivo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getProductoRemoteModel() {
		return _productoRemoteModel;
	}

	public void setProductoRemoteModel(BaseModel<?> productoRemoteModel) {
		_productoRemoteModel = productoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _productoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_productoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ProductoLocalServiceUtil.addProducto(this);
		}
		else {
			ProductoLocalServiceUtil.updateProducto(this);
		}
	}

	@Override
	public Producto toEscapedModel() {
		return (Producto)ProxyUtil.newProxyInstance(Producto.class.getClassLoader(),
			new Class[] { Producto.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ProductoClp clone = new ProductoClp();

		clone.setIdProducto(getIdProducto());
		clone.setIdExpediente(getIdExpediente());
		clone.setTipoProducto(getTipoProducto());
		clone.setCodigoProducto(getCodigoProducto());
		clone.setMoneda(getMoneda());
		clone.setMarcaProducto(getMarcaProducto());
		clone.setLineaCredito(getLineaCredito());
		clone.setDiaPago(getDiaPago());
		clone.setTasaProducto(getTasaProducto());
		clone.setNombreTitularProducto(getNombreTitularProducto());
		clone.setTipoSeguro(getTipoSeguro());
		clone.setCostoSeguro(getCostoSeguro());
		clone.setFechaRegistro(getFechaRegistro());
		clone.setFlagEnvioEECCFisico(getFlagEnvioEECCFisico());
		clone.setFlagEnvioEECCEmail(getFlagEnvioEECCEmail());
		clone.setFlagTerminos(getFlagTerminos());
		clone.setFlagCargoCompra(getFlagCargoCompra());
		clone.setClaveventa(getClaveventa());
		clone.setCodigoOperacion(getCodigoOperacion());
		clone.setFechaHoraOperacion(getFechaHoraOperacion());
		clone.setCodigoUnico(getCodigoUnico());
		clone.setNumTarjetaTitular(getNumTarjetaTitular());
		clone.setNumCuenta(getNumCuenta());
		clone.setFechaAltaTitular(getFechaAltaTitular());
		clone.setFechaVencTitular(getFechaVencTitular());
		clone.setNumTarjetaProvisional(getNumTarjetaProvisional());
		clone.setFechaAltaProvisional(getFechaAltaProvisional());
		clone.setFechaVencProvisional(getFechaVencProvisional());
		clone.setRazonNoUsoTarjeta(getRazonNoUsoTarjeta());
		clone.setMotivo(getMotivo());

		return clone;
	}

	@Override
	public int compareTo(Producto producto) {
		long primaryKey = producto.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductoClp)) {
			return false;
		}

		ProductoClp producto = (ProductoClp)obj;

		long primaryKey = producto.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(61);

		sb.append("{idProducto=");
		sb.append(getIdProducto());
		sb.append(", idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", tipoProducto=");
		sb.append(getTipoProducto());
		sb.append(", codigoProducto=");
		sb.append(getCodigoProducto());
		sb.append(", moneda=");
		sb.append(getMoneda());
		sb.append(", marcaProducto=");
		sb.append(getMarcaProducto());
		sb.append(", lineaCredito=");
		sb.append(getLineaCredito());
		sb.append(", diaPago=");
		sb.append(getDiaPago());
		sb.append(", tasaProducto=");
		sb.append(getTasaProducto());
		sb.append(", nombreTitularProducto=");
		sb.append(getNombreTitularProducto());
		sb.append(", tipoSeguro=");
		sb.append(getTipoSeguro());
		sb.append(", costoSeguro=");
		sb.append(getCostoSeguro());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append(", flagEnvioEECCFisico=");
		sb.append(getFlagEnvioEECCFisico());
		sb.append(", flagEnvioEECCEmail=");
		sb.append(getFlagEnvioEECCEmail());
		sb.append(", flagTerminos=");
		sb.append(getFlagTerminos());
		sb.append(", flagCargoCompra=");
		sb.append(getFlagCargoCompra());
		sb.append(", claveventa=");
		sb.append(getClaveventa());
		sb.append(", codigoOperacion=");
		sb.append(getCodigoOperacion());
		sb.append(", fechaHoraOperacion=");
		sb.append(getFechaHoraOperacion());
		sb.append(", codigoUnico=");
		sb.append(getCodigoUnico());
		sb.append(", numTarjetaTitular=");
		sb.append(getNumTarjetaTitular());
		sb.append(", numCuenta=");
		sb.append(getNumCuenta());
		sb.append(", fechaAltaTitular=");
		sb.append(getFechaAltaTitular());
		sb.append(", fechaVencTitular=");
		sb.append(getFechaVencTitular());
		sb.append(", numTarjetaProvisional=");
		sb.append(getNumTarjetaProvisional());
		sb.append(", fechaAltaProvisional=");
		sb.append(getFechaAltaProvisional());
		sb.append(", fechaVencProvisional=");
		sb.append(getFechaVencProvisional());
		sb.append(", RazonNoUsoTarjeta=");
		sb.append(getRazonNoUsoTarjeta());
		sb.append(", Motivo=");
		sb.append(getMotivo());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(94);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Producto");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idProducto</column-name><column-value><![CDATA[");
		sb.append(getIdProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoProducto</column-name><column-value><![CDATA[");
		sb.append(getTipoProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoProducto</column-name><column-value><![CDATA[");
		sb.append(getCodigoProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moneda</column-name><column-value><![CDATA[");
		sb.append(getMoneda());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>marcaProducto</column-name><column-value><![CDATA[");
		sb.append(getMarcaProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lineaCredito</column-name><column-value><![CDATA[");
		sb.append(getLineaCredito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>diaPago</column-name><column-value><![CDATA[");
		sb.append(getDiaPago());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tasaProducto</column-name><column-value><![CDATA[");
		sb.append(getTasaProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreTitularProducto</column-name><column-value><![CDATA[");
		sb.append(getNombreTitularProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSeguro</column-name><column-value><![CDATA[");
		sb.append(getTipoSeguro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>costoSeguro</column-name><column-value><![CDATA[");
		sb.append(getCostoSeguro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagEnvioEECCFisico</column-name><column-value><![CDATA[");
		sb.append(getFlagEnvioEECCFisico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagEnvioEECCEmail</column-name><column-value><![CDATA[");
		sb.append(getFlagEnvioEECCEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagTerminos</column-name><column-value><![CDATA[");
		sb.append(getFlagTerminos());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagCargoCompra</column-name><column-value><![CDATA[");
		sb.append(getFlagCargoCompra());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>claveventa</column-name><column-value><![CDATA[");
		sb.append(getClaveventa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoOperacion</column-name><column-value><![CDATA[");
		sb.append(getCodigoOperacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaHoraOperacion</column-name><column-value><![CDATA[");
		sb.append(getFechaHoraOperacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoUnico</column-name><column-value><![CDATA[");
		sb.append(getCodigoUnico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numTarjetaTitular</column-name><column-value><![CDATA[");
		sb.append(getNumTarjetaTitular());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numCuenta</column-name><column-value><![CDATA[");
		sb.append(getNumCuenta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaAltaTitular</column-name><column-value><![CDATA[");
		sb.append(getFechaAltaTitular());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaVencTitular</column-name><column-value><![CDATA[");
		sb.append(getFechaVencTitular());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numTarjetaProvisional</column-name><column-value><![CDATA[");
		sb.append(getNumTarjetaProvisional());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaAltaProvisional</column-name><column-value><![CDATA[");
		sb.append(getFechaAltaProvisional());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaVencProvisional</column-name><column-value><![CDATA[");
		sb.append(getFechaVencProvisional());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>RazonNoUsoTarjeta</column-name><column-value><![CDATA[");
		sb.append(getRazonNoUsoTarjeta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>Motivo</column-name><column-value><![CDATA[");
		sb.append(getMotivo());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idProducto;
	private long _idExpediente;
	private String _tipoProducto;
	private String _codigoProducto;
	private String _moneda;
	private String _marcaProducto;
	private String _lineaCredito;
	private String _diaPago;
	private String _tasaProducto;
	private String _nombreTitularProducto;
	private String _tipoSeguro;
	private String _costoSeguro;
	private Date _fechaRegistro;
	private String _flagEnvioEECCFisico;
	private String _flagEnvioEECCEmail;
	private String _flagTerminos;
	private String _flagCargoCompra;
	private String _claveventa;
	private String _codigoOperacion;
	private Date _fechaHoraOperacion;
	private String _codigoUnico;
	private String _numTarjetaTitular;
	private String _numCuenta;
	private Date _fechaAltaTitular;
	private Date _fechaVencTitular;
	private String _numTarjetaProvisional;
	private Date _fechaAltaProvisional;
	private Date _fechaVencProvisional;
	private String _RazonNoUsoTarjeta;
	private String _Motivo;
	private BaseModel<?> _productoRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}