
package pe.com.ibk.pepper.rest.listanegra.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Telefono {

    private String tipoTelefono;
    private String codigoTelediscado;
    private String numeroTelefono;

    @JsonCreator
    public Telefono(String tipoTelefono, String codigoTelediscado, String numeroTelefono) {
        super();
        this.tipoTelefono = tipoTelefono;
        this.codigoTelediscado = codigoTelediscado;
        this.numeroTelefono = numeroTelefono;
    }

    @JsonProperty("tipoTelefono")
    public String getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(String tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }

    @JsonProperty("codigoTelediscado")
    public String getCodigoTelediscado() {
        return codigoTelediscado;
    }

    public void setCodigoTelediscado(String codigoTelediscado) {
        this.codigoTelediscado = codigoTelediscado;
    }

    @JsonProperty("numeroTelefono")
    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tipoTelefono", tipoTelefono).append("codigoTelediscado", codigoTelediscado).append("numeroTelefono", numeroTelefono).toString();
    }

}
