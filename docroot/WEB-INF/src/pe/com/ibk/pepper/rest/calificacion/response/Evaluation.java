
package pe.com.ibk.pepper.rest.calificacion.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Evaluation implements Serializable{

	private static final long serialVersionUID = 6665610699177759419L;
	
	@SerializedName(value="resultCod")
	private String resultCod;
	
	@SerializedName(value="record")
    private Record record;

	public String getResultCod() {
		return resultCod;
	}

	public void setResultCod(String resultCod) {
		this.resultCod = resultCod;
	}

	public Record getRecord() {
		return record;
	}

	public void setRecord(Record record) {
		this.record = record;
	}

}
