/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author Interbank
 */
public class ExpedienteFinderUtil {
	public static java.util.List<java.lang.Object[]> obtenerBusqueda(
		java.lang.Integer filas, java.lang.Integer pagina,
		java.lang.String fechaInicio, java.lang.String fechaFinal,
		java.lang.String estado) {
		return getFinder()
				   .obtenerBusqueda(filas, pagina, fechaInicio, fechaFinal,
			estado);
	}

	public static pe.com.ibk.pepper.model.Expediente obtenerExpedienteDNIFecha(
		java.lang.String documento, java.lang.String mesAnio) {
		return getFinder().obtenerExpedienteDNIFecha(documento, mesAnio);
	}

	public static ExpedienteFinder getFinder() {
		if (_finder == null) {
			_finder = (ExpedienteFinder)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ExpedienteFinder.class.getName());

			ReferenceRegistry.registerReference(ExpedienteFinderUtil.class,
				"_finder");
		}

		return _finder;
	}

	public void setFinder(ExpedienteFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(ExpedienteFinderUtil.class,
			"_finder");
	}

	private static ExpedienteFinder _finder;
}