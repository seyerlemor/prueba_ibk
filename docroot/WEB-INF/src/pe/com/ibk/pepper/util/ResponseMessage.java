package pe.com.ibk.pepper.util;


public enum ResponseMessage {

	SUCCESS(1),
	ERROR_VALIDATION(2),
	ERROR_SERVER(3),
	ERROR_CUSTOM(4);
	
	private int code;
	
	private ResponseMessage(int code){
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}

}
