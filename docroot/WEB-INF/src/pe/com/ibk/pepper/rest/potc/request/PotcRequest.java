package pe.com.ibk.pepper.rest.potc.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.generic.request.HttpHeadersV2;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PotcRequest {

	private HttpHeadersV2 httpHeader;
    private Generacion generacion;

    @JsonCreator
    public PotcRequest() {
		super();
	}

	public PotcRequest(HttpHeadersV2 httpHeader, Generacion generacion) {
		super();
		this.httpHeader = httpHeader;
		this.generacion = generacion;
	}

	@JsonProperty("HttpHeader")
	public HttpHeadersV2 getHttpHeader() {
		return httpHeader;
	}

	public void setHttpHeader(HttpHeadersV2 httpHeader) {
		this.httpHeader = httpHeader;
	}

	@JsonProperty("generacion")
	public Generacion getGeneracion() {
		return generacion;
	}

	public void setGeneracion(Generacion generacion) {
		this.generacion = generacion;
	}

}
