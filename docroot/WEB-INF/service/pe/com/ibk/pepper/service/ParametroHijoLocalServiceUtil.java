/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for ParametroHijo. This utility wraps
 * {@link pe.com.ibk.pepper.service.impl.ParametroHijoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Interbank
 * @see ParametroHijoLocalService
 * @see pe.com.ibk.pepper.service.base.ParametroHijoLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.impl.ParametroHijoLocalServiceImpl
 * @generated
 */
public class ParametroHijoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link pe.com.ibk.pepper.service.impl.ParametroHijoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the parametro hijo to the database. Also notifies the appropriate model listeners.
	*
	* @param parametroHijo the parametro hijo
	* @return the parametro hijo that was added
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijo addParametroHijo(
		pe.com.ibk.pepper.model.ParametroHijo parametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addParametroHijo(parametroHijo);
	}

	/**
	* Creates a new parametro hijo with the primary key. Does not add the parametro hijo to the database.
	*
	* @param idParametroHijo the primary key for the new parametro hijo
	* @return the new parametro hijo
	*/
	public static pe.com.ibk.pepper.model.ParametroHijo createParametroHijo(
		long idParametroHijo) {
		return getService().createParametroHijo(idParametroHijo);
	}

	/**
	* Deletes the parametro hijo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idParametroHijo the primary key of the parametro hijo
	* @return the parametro hijo that was removed
	* @throws PortalException if a parametro hijo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijo deleteParametroHijo(
		long idParametroHijo)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteParametroHijo(idParametroHijo);
	}

	/**
	* Deletes the parametro hijo from the database. Also notifies the appropriate model listeners.
	*
	* @param parametroHijo the parametro hijo
	* @return the parametro hijo that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijo deleteParametroHijo(
		pe.com.ibk.pepper.model.ParametroHijo parametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteParametroHijo(parametroHijo);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static pe.com.ibk.pepper.model.ParametroHijo fetchParametroHijo(
		long idParametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchParametroHijo(idParametroHijo);
	}

	/**
	* Returns the parametro hijo with the primary key.
	*
	* @param idParametroHijo the primary key of the parametro hijo
	* @return the parametro hijo
	* @throws PortalException if a parametro hijo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijo getParametroHijo(
		long idParametroHijo)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getParametroHijo(idParametroHijo);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the parametro hijos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro hijos
	* @param end the upper bound of the range of parametro hijos (not inclusive)
	* @return the range of parametro hijos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijo> getParametroHijos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getParametroHijos(start, end);
	}

	/**
	* Returns the number of parametro hijos.
	*
	* @return the number of parametro hijos
	* @throws SystemException if a system exception occurred
	*/
	public static int getParametroHijosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getParametroHijosCount();
	}

	/**
	* Updates the parametro hijo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param parametroHijo the parametro hijo
	* @return the parametro hijo that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroHijo updateParametroHijo(
		pe.com.ibk.pepper.model.ParametroHijo parametroHijo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateParametroHijo(parametroHijo);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijo> getParametroHijosByCodigoPadre(
		java.lang.String codigoPadre, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getParametroHijosByCodigoPadre(codigoPadre, start, end);
	}

	public static pe.com.ibk.pepper.model.ParametroHijo getParametroHijoByCodigoAndCodigoPadre(
		java.lang.String codigo, java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getParametroHijoByCodigoAndCodigoPadre(codigo, codigoPadre);
	}

	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijo> getParametroHijoByCodigoAndCodigoPadreAndDato(
		java.lang.String dato, java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getParametroHijoByCodigoAndCodigoPadreAndDato(dato,
			codigoPadre);
	}

	public static java.util.List<pe.com.ibk.pepper.model.ParametroHijo> getParametroHijoByCodigoAndCodigoPadreAndDato1AndDato2(
		java.lang.String dato1, java.lang.String dato2,
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getParametroHijoByCodigoAndCodigoPadreAndDato1AndDato2(dato1,
			dato2, codigoPadre);
	}

	public static pe.com.ibk.pepper.model.ParametroHijo getParametroHijoByCodigoAndCodigoPadreAndDato2(
		java.lang.String dato2, java.lang.String codigo,
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getParametroHijoByCodigoAndCodigoPadreAndDato2(dato2,
			codigo, codigoPadre);
	}

	public static void clearService() {
		_service = null;
	}

	public static ParametroHijoLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ParametroHijoLocalService.class.getName());

			if (invokableLocalService instanceof ParametroHijoLocalService) {
				_service = (ParametroHijoLocalService)invokableLocalService;
			}
			else {
				_service = new ParametroHijoLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ParametroHijoLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ParametroHijoLocalService service) {
	}

	private static ParametroHijoLocalService _service;
}