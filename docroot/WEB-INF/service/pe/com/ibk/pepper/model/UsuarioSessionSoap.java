/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class UsuarioSessionSoap implements Serializable {
	public static UsuarioSessionSoap toSoapModel(UsuarioSession model) {
		UsuarioSessionSoap soapModel = new UsuarioSessionSoap();

		soapModel.setIdUsuarioSession(model.getIdUsuarioSession());
		soapModel.setIdSession(model.getIdSession());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setTienda(model.getTienda());
		soapModel.setEstado(model.getEstado());
		soapModel.setUserId(model.getUserId());
		soapModel.setTiendaId(model.getTiendaId());
		soapModel.setEstablecimiento(model.getEstablecimiento());
		soapModel.setNombreVendedor(model.getNombreVendedor());

		return soapModel;
	}

	public static UsuarioSessionSoap[] toSoapModels(UsuarioSession[] models) {
		UsuarioSessionSoap[] soapModels = new UsuarioSessionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UsuarioSessionSoap[][] toSoapModels(UsuarioSession[][] models) {
		UsuarioSessionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UsuarioSessionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UsuarioSessionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UsuarioSessionSoap[] toSoapModels(List<UsuarioSession> models) {
		List<UsuarioSessionSoap> soapModels = new ArrayList<UsuarioSessionSoap>(models.size());

		for (UsuarioSession model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UsuarioSessionSoap[soapModels.size()]);
	}

	public UsuarioSessionSoap() {
	}

	public long getPrimaryKey() {
		return _idUsuarioSession;
	}

	public void setPrimaryKey(long pk) {
		setIdUsuarioSession(pk);
	}

	public long getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	public void setIdUsuarioSession(long idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;
	}

	public String getIdSession() {
		return _idSession;
	}

	public void setIdSession(String idSession) {
		_idSession = idSession;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getTienda() {
		return _tienda;
	}

	public void setTienda(String tienda) {
		_tienda = tienda;
	}

	public String getEstado() {
		return _estado;
	}

	public void setEstado(String estado) {
		_estado = estado;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getTiendaId() {
		return _tiendaId;
	}

	public void setTiendaId(long tiendaId) {
		_tiendaId = tiendaId;
	}

	public String getEstablecimiento() {
		return _establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		_establecimiento = establecimiento;
	}

	public String getNombreVendedor() {
		return _nombreVendedor;
	}

	public void setNombreVendedor(String nombreVendedor) {
		_nombreVendedor = nombreVendedor;
	}

	private long _idUsuarioSession;
	private String _idSession;
	private Date _fechaRegistro;
	private String _tienda;
	private String _estado;
	private long _userId;
	private long _tiendaId;
	private String _establecimiento;
	private String _nombreVendedor;
}