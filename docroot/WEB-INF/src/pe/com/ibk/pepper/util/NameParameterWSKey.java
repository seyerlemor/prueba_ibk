package pe.com.ibk.pepper.util;


public class NameParameterWSKey
{

	public static final String H_SERVICE_ID = "H_SERVICE_ID";
	public static final String H_CONSUMER_ID = "H_CONSUMER_ID";
	public static final String H_MODULE_ID = "H_MODULE_ID";
	public static final String H_CHANNEL_COD = "H_CHANNEL_COD";
	public static final String H_COUNTRY_COD = "H_COUNTRY_COD";
	public static final String H_GROUP_MEMB = "H_GROUP_MEMB";
	public static final String H_REFERENCE_NUMB = "H_REFERENCE_NUMB";
	public static final String H_NET_ID = "H_NET_ID";
	public static final String H_USER_ID = "H_USER_ID";
	public static final String H_SUPERV_ID = "H_SUPERV_ID";
	
	public static final String H_DEVICE_ID = "H_DEVICE_ID";
	public static final String H_SERVER_ID = "H_SERVER_ID";
	
	public static final String H_BRANCH_CODE = "H_BRANCH_CODE";
	public static final String H_BRANCH_ID = "H_BRANCH_ID";
	public static final String H_CARD_ID_TYPE = "H_CARD_ID_TYPE";

	/*BODY WS CONSULTA CAMPANIA*/
	public static final String B_TIPO_BUSQUEDA = "B_TIPO_BUSQUEDA";
	public static final String B_FLG_DATOS_OFERTA = "B_FLG_DATOS_OFERTA";
	public static final String B_FLG_NUEVO_BUS = "B_FLG_NUEVO_BUS";
	public static final String B_FLG_CONSULT_DIR = "B_FLG_CONSULT_DIR";
	
	/*BODY WS CONSULTA PREGUNTAS EQUIFAX*/
	public static final String B_INICIO_VALIDA = "B_INICIO_VALIDA";
	
	public static final String H_MESSAGEID = "MESSAGEID";
	public static final String H_CLIENT_ID = "H_CLIENT_ID";
	
}
