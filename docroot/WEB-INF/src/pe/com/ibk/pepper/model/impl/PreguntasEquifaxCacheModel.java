/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.PreguntasEquifax;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PreguntasEquifax in entity cache.
 *
 * @author Interbank
 * @see PreguntasEquifax
 * @generated
 */
public class PreguntasEquifaxCacheModel implements CacheModel<PreguntasEquifax>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{idPregunta=");
		sb.append(idPregunta);
		sb.append(", idCabecera=");
		sb.append(idCabecera);
		sb.append(", codigoPregunta=");
		sb.append(codigoPregunta);
		sb.append(", descripPregunta=");
		sb.append(descripPregunta);
		sb.append(", codigoRespuesta=");
		sb.append(codigoRespuesta);
		sb.append(", descripRespuesta=");
		sb.append(descripRespuesta);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PreguntasEquifax toEntityModel() {
		PreguntasEquifaxImpl preguntasEquifaxImpl = new PreguntasEquifaxImpl();

		preguntasEquifaxImpl.setIdPregunta(idPregunta);
		preguntasEquifaxImpl.setIdCabecera(idCabecera);

		if (codigoPregunta == null) {
			preguntasEquifaxImpl.setCodigoPregunta(StringPool.BLANK);
		}
		else {
			preguntasEquifaxImpl.setCodigoPregunta(codigoPregunta);
		}

		if (descripPregunta == null) {
			preguntasEquifaxImpl.setDescripPregunta(StringPool.BLANK);
		}
		else {
			preguntasEquifaxImpl.setDescripPregunta(descripPregunta);
		}

		if (codigoRespuesta == null) {
			preguntasEquifaxImpl.setCodigoRespuesta(StringPool.BLANK);
		}
		else {
			preguntasEquifaxImpl.setCodigoRespuesta(codigoRespuesta);
		}

		if (descripRespuesta == null) {
			preguntasEquifaxImpl.setDescripRespuesta(StringPool.BLANK);
		}
		else {
			preguntasEquifaxImpl.setDescripRespuesta(descripRespuesta);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			preguntasEquifaxImpl.setFechaRegistro(null);
		}
		else {
			preguntasEquifaxImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		preguntasEquifaxImpl.resetOriginalValues();

		return preguntasEquifaxImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idPregunta = objectInput.readLong();
		idCabecera = objectInput.readLong();
		codigoPregunta = objectInput.readUTF();
		descripPregunta = objectInput.readUTF();
		codigoRespuesta = objectInput.readUTF();
		descripRespuesta = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idPregunta);
		objectOutput.writeLong(idCabecera);

		if (codigoPregunta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoPregunta);
		}

		if (descripPregunta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripPregunta);
		}

		if (codigoRespuesta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoRespuesta);
		}

		if (descripRespuesta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripRespuesta);
		}

		objectOutput.writeLong(fechaRegistro);
	}

	public long idPregunta;
	public long idCabecera;
	public String codigoPregunta;
	public String descripPregunta;
	public String codigoRespuesta;
	public String descripRespuesta;
	public long fechaRegistro;
}