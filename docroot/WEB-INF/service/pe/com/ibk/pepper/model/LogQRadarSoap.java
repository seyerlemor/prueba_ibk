/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class LogQRadarSoap implements Serializable {
	public static LogQRadarSoap toSoapModel(LogQRadar model) {
		LogQRadarSoap soapModel = new LogQRadarSoap();

		soapModel.setIdLogQRadar(model.getIdLogQRadar());
		soapModel.setPrioridad(model.getPrioridad());
		soapModel.setSeveridad(model.getSeveridad());
		soapModel.setRecurso(model.getRecurso());
		soapModel.setUtc(model.getUtc());
		soapModel.setLogCreador(model.getLogCreador());
		soapModel.setTipoEvento(model.getTipoEvento());
		soapModel.setRegistroUsuario(model.getRegistroUsuario());
		soapModel.setOrigenEvento(model.getOrigenEvento());
		soapModel.setRespuesta(model.getRespuesta());
		soapModel.setAmbiente(model.getAmbiente());
		soapModel.setProducto(model.getProducto());
		soapModel.setIdProducto(model.getIdProducto());
		soapModel.setFechaHora(model.getFechaHora());

		return soapModel;
	}

	public static LogQRadarSoap[] toSoapModels(LogQRadar[] models) {
		LogQRadarSoap[] soapModels = new LogQRadarSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LogQRadarSoap[][] toSoapModels(LogQRadar[][] models) {
		LogQRadarSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LogQRadarSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LogQRadarSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LogQRadarSoap[] toSoapModels(List<LogQRadar> models) {
		List<LogQRadarSoap> soapModels = new ArrayList<LogQRadarSoap>(models.size());

		for (LogQRadar model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LogQRadarSoap[soapModels.size()]);
	}

	public LogQRadarSoap() {
	}

	public int getPrimaryKey() {
		return _idLogQRadar;
	}

	public void setPrimaryKey(int pk) {
		setIdLogQRadar(pk);
	}

	public int getIdLogQRadar() {
		return _idLogQRadar;
	}

	public void setIdLogQRadar(int idLogQRadar) {
		_idLogQRadar = idLogQRadar;
	}

	public int getPrioridad() {
		return _prioridad;
	}

	public void setPrioridad(int prioridad) {
		_prioridad = prioridad;
	}

	public int getSeveridad() {
		return _severidad;
	}

	public void setSeveridad(int severidad) {
		_severidad = severidad;
	}

	public int getRecurso() {
		return _recurso;
	}

	public void setRecurso(int recurso) {
		_recurso = recurso;
	}

	public Date getUtc() {
		return _utc;
	}

	public void setUtc(Date utc) {
		_utc = utc;
	}

	public String getLogCreador() {
		return _logCreador;
	}

	public void setLogCreador(String logCreador) {
		_logCreador = logCreador;
	}

	public String getTipoEvento() {
		return _tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		_tipoEvento = tipoEvento;
	}

	public String getRegistroUsuario() {
		return _registroUsuario;
	}

	public void setRegistroUsuario(String registroUsuario) {
		_registroUsuario = registroUsuario;
	}

	public String getOrigenEvento() {
		return _origenEvento;
	}

	public void setOrigenEvento(String origenEvento) {
		_origenEvento = origenEvento;
	}

	public String getRespuesta() {
		return _respuesta;
	}

	public void setRespuesta(String respuesta) {
		_respuesta = respuesta;
	}

	public String getAmbiente() {
		return _ambiente;
	}

	public void setAmbiente(String ambiente) {
		_ambiente = ambiente;
	}

	public String getProducto() {
		return _producto;
	}

	public void setProducto(String producto) {
		_producto = producto;
	}

	public int getIdProducto() {
		return _idProducto;
	}

	public void setIdProducto(int idProducto) {
		_idProducto = idProducto;
	}

	public Date getFechaHora() {
		return _fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		_fechaHora = fechaHora;
	}

	private int _idLogQRadar;
	private int _prioridad;
	private int _severidad;
	private int _recurso;
	private Date _utc;
	private String _logCreador;
	private String _tipoEvento;
	private String _registroUsuario;
	private String _origenEvento;
	private String _respuesta;
	private String _ambiente;
	private String _producto;
	private int _idProducto;
	private Date _fechaHora;
}