package pe.com.ibk.pepper.util.cola;

public interface EnqueueableData {

	/*
	* Metodo a ser implementado por los objetos
	* que implementen esta interfaz.
	*/
	public void execute();
}
