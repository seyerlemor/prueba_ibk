package pe.com.ibk.pepper.util.cola;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class AsyncDataWriter extends Thread {

	private BlockingQueue<EnqueueableData> queue = new LinkedBlockingQueue<>();

	/**
	 * Envia un nuevo elemento request a la cola.
	 * 
	 * @param request
	 */
	public void accept(EnqueueableData request) {
		queue.add(request);
	}

	/**
	 * Metodo que obtiene los elementos de la cola y los procesa.
	 */
	public void run() {
		while (true)
			try {
				execute(queue.take());
			} catch (InterruptedException e) {
				// Report to log4j
			}
	}

	/**
	 * Metodo que se encarga de procesar cada elemento obtenido de la cola.
	 * 
	 * @param request
	 */
	private void execute(final EnqueueableData request) {
		request.execute();
	}
}