/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class TblCodigoPromocionSoap implements Serializable {
	public static TblCodigoPromocionSoap toSoapModel(TblCodigoPromocion model) {
		TblCodigoPromocionSoap soapModel = new TblCodigoPromocionSoap();

		soapModel.setIdCodigo(model.getIdCodigo());
		soapModel.setCodigo(model.getCodigo());
		soapModel.setComercio(model.getComercio());
		soapModel.setEstado(model.getEstado());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static TblCodigoPromocionSoap[] toSoapModels(
		TblCodigoPromocion[] models) {
		TblCodigoPromocionSoap[] soapModels = new TblCodigoPromocionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TblCodigoPromocionSoap[][] toSoapModels(
		TblCodigoPromocion[][] models) {
		TblCodigoPromocionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TblCodigoPromocionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TblCodigoPromocionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TblCodigoPromocionSoap[] toSoapModels(
		List<TblCodigoPromocion> models) {
		List<TblCodigoPromocionSoap> soapModels = new ArrayList<TblCodigoPromocionSoap>(models.size());

		for (TblCodigoPromocion model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TblCodigoPromocionSoap[soapModels.size()]);
	}

	public TblCodigoPromocionSoap() {
	}

	public int getPrimaryKey() {
		return _idCodigo;
	}

	public void setPrimaryKey(int pk) {
		setIdCodigo(pk);
	}

	public int getIdCodigo() {
		return _idCodigo;
	}

	public void setIdCodigo(int idCodigo) {
		_idCodigo = idCodigo;
	}

	public String getCodigo() {
		return _codigo;
	}

	public void setCodigo(String codigo) {
		_codigo = codigo;
	}

	public String getComercio() {
		return _comercio;
	}

	public void setComercio(String comercio) {
		_comercio = comercio;
	}

	public String getEstado() {
		return _estado;
	}

	public void setEstado(String estado) {
		_estado = estado;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private int _idCodigo;
	private String _codigo;
	private String _comercio;
	private String _estado;
	private Date _fechaRegistro;
}