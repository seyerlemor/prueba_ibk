/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.AuditoriaClienteLocalServiceUtil;
import pe.com.ibk.pepper.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class AuditoriaClienteClp extends BaseModelImpl<AuditoriaCliente>
	implements AuditoriaCliente {
	public AuditoriaClienteClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return AuditoriaCliente.class;
	}

	@Override
	public String getModelClassName() {
		return AuditoriaCliente.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idAuditoriaCliente;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdAuditoriaCliente(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idAuditoriaCliente;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idAuditoriaCliente", getIdAuditoriaCliente());
		attributes.put("idClienteTransaccion", getIdClienteTransaccion());
		attributes.put("paso", getPaso());
		attributes.put("pagina", getPagina());
		attributes.put("tipoFlujo", getTipoFlujo());
		attributes.put("strJson", getStrJson());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idAuditoriaCliente = (Long)attributes.get("idAuditoriaCliente");

		if (idAuditoriaCliente != null) {
			setIdAuditoriaCliente(idAuditoriaCliente);
		}

		Long idClienteTransaccion = (Long)attributes.get("idClienteTransaccion");

		if (idClienteTransaccion != null) {
			setIdClienteTransaccion(idClienteTransaccion);
		}

		String paso = (String)attributes.get("paso");

		if (paso != null) {
			setPaso(paso);
		}

		String pagina = (String)attributes.get("pagina");

		if (pagina != null) {
			setPagina(pagina);
		}

		String tipoFlujo = (String)attributes.get("tipoFlujo");

		if (tipoFlujo != null) {
			setTipoFlujo(tipoFlujo);
		}

		String strJson = (String)attributes.get("strJson");

		if (strJson != null) {
			setStrJson(strJson);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public long getIdAuditoriaCliente() {
		return _idAuditoriaCliente;
	}

	@Override
	public void setIdAuditoriaCliente(long idAuditoriaCliente) {
		_idAuditoriaCliente = idAuditoriaCliente;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdAuditoriaCliente",
						long.class);

				method.invoke(_auditoriaClienteRemoteModel, idAuditoriaCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdClienteTransaccion() {
		return _idClienteTransaccion;
	}

	@Override
	public void setIdClienteTransaccion(long idClienteTransaccion) {
		_idClienteTransaccion = idClienteTransaccion;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdClienteTransaccion",
						long.class);

				method.invoke(_auditoriaClienteRemoteModel, idClienteTransaccion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPaso() {
		return _paso;
	}

	@Override
	public void setPaso(String paso) {
		_paso = paso;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPaso", String.class);

				method.invoke(_auditoriaClienteRemoteModel, paso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPagina() {
		return _pagina;
	}

	@Override
	public void setPagina(String pagina) {
		_pagina = pagina;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPagina", String.class);

				method.invoke(_auditoriaClienteRemoteModel, pagina);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoFlujo() {
		return _tipoFlujo;
	}

	@Override
	public void setTipoFlujo(String tipoFlujo) {
		_tipoFlujo = tipoFlujo;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoFlujo", String.class);

				method.invoke(_auditoriaClienteRemoteModel, tipoFlujo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStrJson() {
		return _strJson;
	}

	@Override
	public void setStrJson(String strJson) {
		_strJson = strJson;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setStrJson", String.class);

				method.invoke(_auditoriaClienteRemoteModel, strJson);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_auditoriaClienteRemoteModel != null) {
			try {
				Class<?> clazz = _auditoriaClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_auditoriaClienteRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAuditoriaClienteRemoteModel() {
		return _auditoriaClienteRemoteModel;
	}

	public void setAuditoriaClienteRemoteModel(
		BaseModel<?> auditoriaClienteRemoteModel) {
		_auditoriaClienteRemoteModel = auditoriaClienteRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _auditoriaClienteRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_auditoriaClienteRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AuditoriaClienteLocalServiceUtil.addAuditoriaCliente(this);
		}
		else {
			AuditoriaClienteLocalServiceUtil.updateAuditoriaCliente(this);
		}
	}

	@Override
	public AuditoriaCliente toEscapedModel() {
		return (AuditoriaCliente)ProxyUtil.newProxyInstance(AuditoriaCliente.class.getClassLoader(),
			new Class[] { AuditoriaCliente.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AuditoriaClienteClp clone = new AuditoriaClienteClp();

		clone.setIdAuditoriaCliente(getIdAuditoriaCliente());
		clone.setIdClienteTransaccion(getIdClienteTransaccion());
		clone.setPaso(getPaso());
		clone.setPagina(getPagina());
		clone.setTipoFlujo(getTipoFlujo());
		clone.setStrJson(getStrJson());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(AuditoriaCliente auditoriaCliente) {
		long primaryKey = auditoriaCliente.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditoriaClienteClp)) {
			return false;
		}

		AuditoriaClienteClp auditoriaCliente = (AuditoriaClienteClp)obj;

		long primaryKey = auditoriaCliente.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{idAuditoriaCliente=");
		sb.append(getIdAuditoriaCliente());
		sb.append(", idClienteTransaccion=");
		sb.append(getIdClienteTransaccion());
		sb.append(", paso=");
		sb.append(getPaso());
		sb.append(", pagina=");
		sb.append(getPagina());
		sb.append(", tipoFlujo=");
		sb.append(getTipoFlujo());
		sb.append(", strJson=");
		sb.append(getStrJson());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.AuditoriaCliente");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idAuditoriaCliente</column-name><column-value><![CDATA[");
		sb.append(getIdAuditoriaCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idClienteTransaccion</column-name><column-value><![CDATA[");
		sb.append(getIdClienteTransaccion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>paso</column-name><column-value><![CDATA[");
		sb.append(getPaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pagina</column-name><column-value><![CDATA[");
		sb.append(getPagina());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoFlujo</column-name><column-value><![CDATA[");
		sb.append(getTipoFlujo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>strJson</column-name><column-value><![CDATA[");
		sb.append(getStrJson());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idAuditoriaCliente;
	private long _idClienteTransaccion;
	private String _paso;
	private String _pagina;
	private String _tipoFlujo;
	private String _strJson;
	private Date _fechaRegistro;
	private BaseModel<?> _auditoriaClienteRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}