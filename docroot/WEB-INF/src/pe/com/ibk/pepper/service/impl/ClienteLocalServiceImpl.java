/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;
import java.util.List;

import pe.com.ibk.pepper.NoSuchClienteException;
import pe.com.ibk.pepper.model.Cliente;
import pe.com.ibk.pepper.service.base.ClienteLocalServiceBaseImpl;

/**
 * The implementation of the cliente local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.ClienteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.ClienteLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.ClienteLocalServiceUtil
 */
public class ClienteLocalServiceImpl extends ClienteLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.ClienteLocalServiceUtil} to access the cliente local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(ClienteLocalServiceImpl.class);
	
	@Override
	public Cliente registrarCliente(Cliente cliente) throws SystemException{
		
		Date now = DateUtil.newDate();
		if (cliente.isNew()) {
			cliente.setFechaRegistro(now);
		}
		clientePersistence.update(cliente);
		
		return cliente;
	}
	
	@Override
	public Cliente buscarPorExpediente(long idExpediente){
		Cliente cliente = null;
		try {
			cliente = clientePersistence.findByC_E(idExpediente); 
		} catch (NoSuchClienteException e) {
			_log.error("NO EXISTE EL CLIENTE");
		}catch (SystemException e) {
			_log.error("Error en clase ClienteLocalServiceImpl metodo buscarPorExpediente:: NO EXISTE EL CLIENTE", e );
		}	
		return cliente;
	}
	
	@Override
	public Cliente buscarClienteporId(long idCliente){
		Cliente cliente = null;
		try {
			cliente = clientePersistence.findByDC_ID(idCliente); 
		} catch (NoSuchClienteException e) {
			_log.error("Error en clase ClienteLocalServiceImpl metodo buscarPorCliente:: NO EXISTE EN CLIENTE ", e);
		}catch (SystemException e) {
			_log.error("Error en clase ClienteLocalServiceImpl metodo buscarPorCliente:: ", e);
		}	
		return cliente;
	}
	
	public List<Cliente> obtenerDatosCliente(String fechaDesde, String fechaHasta, String nombres, 
			String apellidos, String DNI, int start, int end) throws SystemException {
		return clienteFinder.findByF_F_N_A_D(fechaDesde, fechaHasta, nombres, apellidos, DNI, start, end);
	}
	
	public int obtenerDatosClienteCount(String fechaDesde, String fechaHasta, String nombres, 
			String apellidos, String DNI) throws SystemException {
		return clienteFinder.countByF_F_N_A_D(fechaDesde, fechaHasta, nombres, apellidos, DNI);
	}

	
}