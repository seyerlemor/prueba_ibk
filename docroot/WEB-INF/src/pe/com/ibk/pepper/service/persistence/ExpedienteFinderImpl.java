package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

import java.util.ArrayList;
import java.util.List;

import pe.com.ibk.pepper.model.Expediente;
import pe.com.ibk.pepper.model.impl.ExpedienteImpl;


/**
 * @author Interbank
 */
public class ExpedienteFinderImpl extends BasePersistenceImpl<Expediente> implements ExpedienteFinder{

	private static final Log _log = LogFactoryUtil.getLog(ExpedienteFinderImpl.class);
	
	public static final String FINDER_EXPEDIENTE_DNI_FECHA = "Expediente.obtenerExpedienteDNIFecha";

	public static final String FINDER_RESULTADO_BUSQUEDA = "ResultadoPepper.obtenerResultadoPepper";	

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> obtenerBusqueda(Integer filas , Integer pagina , String fechaInicio, String fechaFinal, String estado) {
		Session session = null;
		List<Object[]> out = null;
		try {		
			session = openSession(); 
			String sql = CustomSQLUtil.get(FINDER_RESULTADO_BUSQUEDA);
			
			sql = CustomSQLUtil.replaceAndOperator(sql, Boolean.TRUE);
			String typeJoin = "LEFT JOIN";
			String whereQuery = StringPool.BLANK;
				
			sql = StringUtil.replace(sql, new String[]{"[$TYPE_JOIN$]","[$WHERE_QUERY$]"}, new String[]{typeJoin, whereQuery});
			SQLQuery q = session.createSQLQuery(sql);
			
			if (filas > 0 && pagina > 0) {
				q.setFirstResult(filas * pagina - filas);
	            q.setMaxResults(filas);
			}			

			q.addScalar("xpdt_IdExpediente", Type.LONG);
			q.addScalar("ApellidosNombres", Type.STRING);
			q.addScalar("dtcl_Documento", Type.STRING);
			q.addScalar("xpdt_FechaRegistro", Type.STRING);
			q.addScalar("xpdt_Estado", Type.STRING);
			q.addScalar("xpdt_FlagR1", Type.STRING);
			q.addScalar("xpdt_FlagCampania", Type.STRING);
			q.addScalar("xpdt_FlagObtenerInformacion", Type.STRING);
			q.addScalar("xpdt_FlagEquifax", Type.STRING);
			q.addScalar("xpdt_FlagCalificacion", Type.STRING);
			q.addScalar("xpdt_FlagVentaTC", Type.STRING);	
			q.addScalar("prcl_LineaCredito", Type.STRING);
			q.addScalar("dtcl_Correo", Type.STRING);
			q.addScalar("dtcl_Telefono", Type.STRING);	
			q.addScalar("dtcl_Operador", Type.STRING);
			q.addScalar("dtcl_EstadoCivil", Type.STRING);
			q.addScalar("prcl_MarcaProducto", Type.STRING);
			q.addScalar("prcl_TipoProducto", Type.STRING);
			q.addScalar("xpdt_StrJson", Type.STRING);
			q.addScalar("xpdt_FormHtml", Type.STRING);
			
			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(fechaInicio.concat(" 00:00:00"));
			qPos.add(fechaFinal.concat(" 23:59:59"));			
			qPos.add(estado);
			qPos.add(estado);
			qPos.add(estado);
			
			out = new ArrayList<>();
			out = q.list();
			
		} catch (Exception e) {
	        _log.debug(e);
		} finally {
			closeSession(session);
		}
		return out;
	}		
	
	@SuppressWarnings("unchecked")
	@Override
	public Expediente obtenerExpedienteDNIFecha(String documento, String mesAnio) {
		Session session = null;
		List<Expediente> out = new ArrayList<Expediente>();
		try {
			session = openSession();
			String sql = CustomSQLUtil.get(FINDER_EXPEDIENTE_DNI_FECHA);
			SQLQuery q = session.createSQLQuery(sql);
			q.addEntity("Expediente", ExpedienteImpl.class);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(documento);
			qPos.add(mesAnio);
			out = q.list();
		} catch (Exception e) {
	        _log.debug(e);
		} finally {
			closeSession(session);
		}
		return out.get(0);
	}	
	
}