package pe.com.ibk.pepper.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;

import pe.com.ibk.pepper.bean.ServiceBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.rest.generic.request.Header;
import pe.com.ibk.pepper.rest.generic.request.HeaderRequest;
import pe.com.ibk.pepper.rest.generic.request.Identity;
import pe.com.ibk.pepper.rest.generic.request.Request;

public class ServicioUtil {

	private static final Log _log = LogFactoryUtil.getLog(ServicioUtil.class);
	
	private ServicioUtil(){}
	
	public static Builder obtenerDatosParametroServicio(Client client, ServiceBean serviceBean)
 	{
		 WebTarget target = client.target(serviceBean.getEndPoint()).path(serviceBean.getPath());
		try {
			JsonNode requestNode = new ObjectMapper().readTree(serviceBean.getRequestString());
			Map<String, String> queryParameters = ServicioUtil.obtenerMapParametro(requestNode, RequestParam.QUERY_PARAMS);
			if(Validator.isNotNull(queryParameters))
				for (Entry<String, String> entry : queryParameters.entrySet()) 
					target = target.queryParam(entry.getKey(), entry.getValue()); 
			Map<String, String> templatesParameters = ServicioUtil.obtenerMapParametro(requestNode, RequestParam.TEMPLATE_PARAMS);
			if(Validator.isNotNull(templatesParameters))
				for (Entry<String, String> entry : templatesParameters.entrySet()) 
					target = target.resolveTemplate(entry.getKey(), entry.getValue());
		} catch (IOException e) {
			_log.error(e);
		}
		return target.request(Constantes.APPLICATION_JSON_TYPE_UTF8);
 	}
 	
	public static Map<String, String> obtenerMapParametro(JsonNode requestNode, String param)
 	{
 		Map<String, String> parameter = new HashMap<>();
		try {
			ObjectMapper mapper = new ObjectMapper();
	 		JsonNode parameterNode = requestNode.get(param);
	 		if(Validator.isNotNull(parameterNode))
	 			parameter =  mapper.readValue(parameterNode.toString(), new TypeReference<Map<String, String>>() {});
		} catch (IOException e) {
			_log.error(e);
		}
		return parameter;
 	}
	
 	public static Builder obtenerDatosParametroCabecera(Builder buildClient, String requestString){
 			try {
 				ObjectMapper mapper = new ObjectMapper();
 				JsonNode requestNode = mapper.readTree(requestString);
 				JsonNode headersNode = requestNode.get(RequestParam.HTTP_HEADER);
 				if(Validator.isNotNull(headersNode)){
 					Map<String, Object> mapHeaders = mapper.readValue(headersNode.toString(), new TypeReference<Map<String, Object>>() {});
 	 				MultivaluedHashMap<String, Object> headers = new MultivaluedHashMap<>(mapHeaders);
 	 				buildClient.headers(headers);
 				}
 				buildClient.acceptEncoding(Constantes.APPLICATION_JSON_TYPE_UTF8);
 			} catch (IOException e) {
 				_log.error(e);
 			}
 			return buildClient;
 	 	}
 	
 	
 	public static Response invocarServicio(Builder buildClient, ServiceBean serviceBean) throws WebServicesException{
 		Response response;
 		if(Constantes.HTTP_GET.equals(serviceBean.getMetodoHttp()))
			response = buildClient.get();
		else if(Constantes.HTTP_PUT.equals(serviceBean.getMetodoHttp())) 
			response = buildClient.put(Entity.entity(serviceBean.getRequestString(), Constantes.APPLICATION_JSON_TYPE_UTF8));
		else
			response = buildClient.post(Entity.entity(serviceBean.getRequestString(), Constantes.APPLICATION_JSON_TYPE_UTF8));
		return response;
 	}
 	
 	public static String generarRequestString(Object httpRequest){
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(httpRequest);
		} catch (JsonProcessingException e) {
			_log.error(e);
			return null;
		}
 	}
 	
 	public static ServiceBean removerNodo(ServiceBean serviceBean){
 		try{
 			if(Constantes.HTTP_POST.equals(serviceBean.getMetodoHttp())){
	 			ObjectMapper mapper = new ObjectMapper();
	 			JsonNode requestNode = mapper.readTree(serviceBean.getRequestString());
				((ObjectNode) requestNode).remove(RequestParam.HTTP_HEADER);
				((ObjectNode) requestNode).remove(RequestParam.QUERY_PARAMS);
				((ObjectNode) requestNode).remove(RequestParam.TEMPLATE_PARAMS);
				serviceBean.setRequestString(requestNode.toString());
 			}
 		} catch (IOException e) {
			_log.error(e);
		}
 		return serviceBean;
 	}
 	
 	public static Header obtenerHeaderRequest(Map<String, ParametroHijoPO> nombreMap) {
		 Request request = new Request(); 
		 request.setServiceId(nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1()); 
		 request.setConsumerId(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1());
		 request.setModuleId(nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1());
		 request.setChannelCode(nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1());
		 Date today = new Date();
		 request.setMessageId(nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1());
        request.setTimestamp(WebServicesUtil.toXMLGregorianCalendar(today));
		 request.setCountryCode(nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1());
		 request.setGroupMember(nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1());
		 request.setReferenceNumber(nombreMap.get(NameParameterWSKey.H_REFERENCE_NUMB).getDato1());
		 
		 Identity identityType = new Identity();
		 identityType.setNetId(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1());
		 identityType.setUserId(nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1());
		 identityType.setSupervisorId(nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1());
		 identityType.setDeviceId(nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1());
		 identityType.setServerId(nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1());
		 identityType.setBranchCode(nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1()); 
		 HeaderRequest headerRequest = new HeaderRequest(request, identityType);
		 return new Header(headerRequest);
	 }
}
