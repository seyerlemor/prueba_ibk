
package pe.com.ibk.pepper.rest.potc.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ConsultarProvisional {

    private String codigoCliente;

    @JsonCreator
	public ConsultarProvisional(String codigoCliente) {
		super();
		this.codigoCliente = codigoCliente;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

}
