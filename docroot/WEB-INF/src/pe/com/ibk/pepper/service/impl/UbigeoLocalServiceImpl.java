/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import java.util.List;

import pe.com.ibk.pepper.model.Ubigeo;
import pe.com.ibk.pepper.service.base.UbigeoLocalServiceBaseImpl;
import pe.com.ibk.pepper.util.Validador;

/**
 * The implementation of the ubigeo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.UbigeoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.UbigeoLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.UbigeoLocalServiceUtil
 */
public class UbigeoLocalServiceImpl extends UbigeoLocalServiceBaseImpl {
	
	private static final Log _log = LogFactoryUtil.getLog(UbigeoLocalServiceImpl.class);
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.UbigeoLocalServiceUtil} to access the ubigeo local service.
	 */
	public static final String CODIGO_DEAFULT="00";
	
	public List<Ubigeo> getUbigeoByCodProvincia(String CodProvincia) throws SystemException{
		return ubigeoPersistence.findByProv(CodProvincia);
	}
	
	public List<Ubigeo> getUbigeoByD_N_P_D(String codDepartamento, String codProvincia, String codDistrito) throws SystemException{
		return ubigeoPersistence.findByD_N_P_D(codDepartamento, codProvincia, codDistrito);
	}
	
	public List<Ubigeo> getUbigeoByD_P_N_D(String codDepartamento, String codProvincia, String codDistrito) throws SystemException{
		return ubigeoPersistence.findByD_P_N_D(codDepartamento, codProvincia, codDistrito);
	}
	
	public Ubigeo getUbigeoByNombre(String nomDepartamento, String nomProvincia,String nomDistrito){
		Ubigeo departamento = ubigeoFinder.obtenerUbigeoByNombre(CODIGO_DEAFULT,CODIGO_DEAFULT,CODIGO_DEAFULT,nomDepartamento,1);
		if(Validador.isNotNull(departamento)){
			Ubigeo provincia = ubigeoFinder.obtenerUbigeoByNombre(departamento.getCodDepartamento(), CODIGO_DEAFULT, CODIGO_DEAFULT, nomProvincia,2);
			if(Validador.isNotNull(provincia)){
				Ubigeo distrito = ubigeoFinder.obtenerUbigeoByNombre(departamento.getCodDepartamento(), provincia.getCodProvincia(),CODIGO_DEAFULT, nomDistrito,3);
				return distrito;
			}
		}
		return null;
	}
	
	@Override
	public String getUbigeoByCodigo(String codigo){
		_log.debug(codigo);
		try {
			return ubigeoFinder.obtenerUbigeoByCodigo(codigo);
		} catch (Exception e) {
			return StringPool.BLANK;
		}
	}
	
	@Override
	public String getNombreUbigeoByCodigo(String codigo){
		_log.debug(codigo);
		try {
			return ubigeoPersistence.fetchByN_C_U(codigo).getNombre();
		} catch (SystemException e) {
			return StringPool.BLANK;
		}
	}
	
}