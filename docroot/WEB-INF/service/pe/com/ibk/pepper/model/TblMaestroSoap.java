/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class TblMaestroSoap implements Serializable {
	public static TblMaestroSoap toSoapModel(TblMaestro model) {
		TblMaestroSoap soapModel = new TblMaestroSoap();

		soapModel.setIdMaestro(model.getIdMaestro());
		soapModel.setNombreComercio(model.getNombreComercio());
		soapModel.setIdHash(model.getIdHash());
		soapModel.setTipoTarjeta(model.getTipoTarjeta());
		soapModel.setUrlImagen(model.getUrlImagen());
		soapModel.setTopeOtp(model.getTopeOtp());
		soapModel.setTopeEquifax(model.getTopeEquifax());
		soapModel.setCodigoSeguridad(model.getCodigoSeguridad());

		return soapModel;
	}

	public static TblMaestroSoap[] toSoapModels(TblMaestro[] models) {
		TblMaestroSoap[] soapModels = new TblMaestroSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TblMaestroSoap[][] toSoapModels(TblMaestro[][] models) {
		TblMaestroSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TblMaestroSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TblMaestroSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TblMaestroSoap[] toSoapModels(List<TblMaestro> models) {
		List<TblMaestroSoap> soapModels = new ArrayList<TblMaestroSoap>(models.size());

		for (TblMaestro model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TblMaestroSoap[soapModels.size()]);
	}

	public TblMaestroSoap() {
	}

	public int getPrimaryKey() {
		return _idMaestro;
	}

	public void setPrimaryKey(int pk) {
		setIdMaestro(pk);
	}

	public int getIdMaestro() {
		return _idMaestro;
	}

	public void setIdMaestro(int idMaestro) {
		_idMaestro = idMaestro;
	}

	public String getNombreComercio() {
		return _nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		_nombreComercio = nombreComercio;
	}

	public String getIdHash() {
		return _idHash;
	}

	public void setIdHash(String idHash) {
		_idHash = idHash;
	}

	public String getTipoTarjeta() {
		return _tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		_tipoTarjeta = tipoTarjeta;
	}

	public Date getUrlImagen() {
		return _urlImagen;
	}

	public void setUrlImagen(Date urlImagen) {
		_urlImagen = urlImagen;
	}

	public String getTopeOtp() {
		return _topeOtp;
	}

	public void setTopeOtp(String topeOtp) {
		_topeOtp = topeOtp;
	}

	public String getTopeEquifax() {
		return _topeEquifax;
	}

	public void setTopeEquifax(String topeEquifax) {
		_topeEquifax = topeEquifax;
	}

	public String getCodigoSeguridad() {
		return _codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		_codigoSeguridad = codigoSeguridad;
	}

	private int _idMaestro;
	private String _nombreComercio;
	private String _idHash;
	private String _tipoTarjeta;
	private Date _urlImagen;
	private String _topeOtp;
	private String _topeEquifax;
	private String _codigoSeguridad;
}