/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.AuditoriaCliente;

/**
 * The persistence interface for the auditoria cliente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see AuditoriaClientePersistenceImpl
 * @see AuditoriaClienteUtil
 * @generated
 */
public interface AuditoriaClientePersistence extends BasePersistence<AuditoriaCliente> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AuditoriaClienteUtil} to access the auditoria cliente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the auditoria cliente where idAuditoriaCliente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaClienteException} if it could not be found.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the matching auditoria cliente
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a matching auditoria cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente findByAC_ID(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException;

	/**
	* Returns the auditoria cliente where idAuditoriaCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the matching auditoria cliente, or <code>null</code> if a matching auditoria cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente fetchByAC_ID(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the auditoria cliente where idAuditoriaCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching auditoria cliente, or <code>null</code> if a matching auditoria cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente fetchByAC_ID(
		long idAuditoriaCliente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the auditoria cliente where idAuditoriaCliente = &#63; from the database.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the auditoria cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente removeByAC_ID(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException;

	/**
	* Returns the number of auditoria clientes where idAuditoriaCliente = &#63;.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the number of matching auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countByAC_ID(long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the auditoria cliente in the entity cache if it is enabled.
	*
	* @param auditoriaCliente the auditoria cliente
	*/
	public void cacheResult(
		pe.com.ibk.pepper.model.AuditoriaCliente auditoriaCliente);

	/**
	* Caches the auditoria clientes in the entity cache if it is enabled.
	*
	* @param auditoriaClientes the auditoria clientes
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> auditoriaClientes);

	/**
	* Creates a new auditoria cliente with the primary key. Does not add the auditoria cliente to the database.
	*
	* @param idAuditoriaCliente the primary key for the new auditoria cliente
	* @return the new auditoria cliente
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente create(
		long idAuditoriaCliente);

	/**
	* Removes the auditoria cliente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idAuditoriaCliente the primary key of the auditoria cliente
	* @return the auditoria cliente that was removed
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente remove(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException;

	public pe.com.ibk.pepper.model.AuditoriaCliente updateImpl(
		pe.com.ibk.pepper.model.AuditoriaCliente auditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the auditoria cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaClienteException} if it could not be found.
	*
	* @param idAuditoriaCliente the primary key of the auditoria cliente
	* @return the auditoria cliente
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente findByPrimaryKey(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException;

	/**
	* Returns the auditoria cliente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idAuditoriaCliente the primary key of the auditoria cliente
	* @return the auditoria cliente, or <code>null</code> if a auditoria cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.AuditoriaCliente fetchByPrimaryKey(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the auditoria clientes.
	*
	* @return the auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the auditoria clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria clientes
	* @param end the upper bound of the range of auditoria clientes (not inclusive)
	* @return the range of auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the auditoria clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria clientes
	* @param end the upper bound of the range of auditoria clientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the auditoria clientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of auditoria clientes.
	*
	* @return the number of auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}