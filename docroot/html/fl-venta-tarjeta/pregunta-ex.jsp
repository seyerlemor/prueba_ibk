<%@ include file="/html/init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="validateEquifax" var="validateEquifax" />
<portlet:resourceURL id="validateEquifaxRE" var="validateEquifaxRE" />
       
<div class="lq-container">
        <div class="lh-ff">
          <div class="lh-ff-block lh-ff-block--active">
            <div class="lq-row">
                    <liferay-ui:journal-article articleId="${contentStep.articleId}" groupId="${contentStep.groupId}" />
              <div class="lh-ff-content-col">
                <form:form action="${validateEquifax}" data-captcha="false" autocomplete="off" class="lh-ff-form" novalidate="novalidate" >
                  <div class="lh-ff-section">
                    <div class="lh-ff-section__wrapper">
                      <div class="lh-ff-section__text">
	                        <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1">${renderRequest.preferences.getValue('equifaxPartTitle','')} ${pasoBean.clienteBean.firstname},</h3>
	                        <div class="lh-ff-hint"><span class="lh-typo__p1">${renderRequest.preferences.getValue('equifaxText001','')} ${pasoBean.clienteBean.dniNumber}</span>
	                          <div><a class="lh-link lh-link--cta4" href="#">${renderRequest.preferences.getValue('equifaxText002','')}</a></div>
	                        </div>
                        <p class="lh-typo__p1">${renderRequest.preferences.getValue('equifaxText003','')}</p>
                      </div>
                    </div>
                    	<c:forEach var="lstPreguntas" items="${lstPreguntaBean}" varStatus="lp">
                          <div class="lh-ff-group">
                                  <div class="lh-ff-group__item">
                                    <label class="lh-ff__label">${lstPreguntas.descripcionPregunta}</label>
                                    <div class="lh-ff-group__field">
                                      <input id="fpa_hiddenEfx${lp.index+1}" type="hidden" name="selectedOptionEfx${lp.index+1}" data-validate-required="radio" data-validate-required-message="Seleccione una opción">
                                            <c:forEach var="lstPreguntasOpciones" items="${lstPreguntas.opciones.opcion}" varStatus="lpo"> 
                                            <div class="lh-form__radio">
                                              <input id="fpa_efx${lp.index+1}-${lstPreguntasOpciones.numeroOpcion}" type="radio" name="efx${lp.index+1}" value="${lstPreguntasOpciones.numeroOpcion}.${lstPreguntas.numeroPregunta}.${lstPreguntas.categoriaPregunta}">
                                              <label for="fpa_efx${lp.index+1}-${lstPreguntasOpciones.numeroOpcion}"><span>${lstPreguntasOpciones.descripcionOpcion}</span></label>
                                            </div>
                                            </c:forEach>
                                    </div>
                                  </div>
                          </div>
                       </c:forEach>
                       <input type="hidden" id="cantidadPreguntas" name="cantidadPreguntas" value="${lstPreguntaBean.size()}"/>
                          <div class="lh-ff-group">
                            <div class="lh-ff-group__item lh-ff-group__item--buttons">
                              <button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit"><span>${renderRequest.preferences.getValue('equifaxBtn','')}</span></button>
                            </div>
                          </div>
                          <div class="lh-form__error">
                            <div class="lh-typo__p2 lh-form__error-message" data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
                          </div>
                  </div>
                </form:form>
              </div>
            </div>
          </div>
        </div>
      </div>          
          
          
          
          
          
          