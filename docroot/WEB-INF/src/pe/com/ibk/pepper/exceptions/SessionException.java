package pe.com.ibk.pepper.exceptions;


/**/

/**
 * The Class WebServicesException.
 * Excepcion para control de errores en todo el flujo 
 */
public class SessionException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -892747895166253824L;

	private String nombre;
	private String trazaMessage;
	
	public SessionException() {
		super();
	} 
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getTrazaMessage() {
		return trazaMessage;
	}

	public void setTrazaMessage(String trazaMessage) {
		this.trazaMessage = trazaMessage;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public SessionException(String nombre, String trazaMessage){
		this.nombre = nombre;
		this.trazaMessage = trazaMessage;
	}
	/**
	 * Instantiates a new web services exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public SessionException(String message, Throwable cause){
		super(message,cause);
	}
	
	/**
	 * Instantiates a new web services exception.
	 *
	 * @param message the message
	 */
	public SessionException(String message){	
		super(message);
	}
		
	/**
	 * Instantiates a new web services exception.
	 *
	 * @param cause the cause
	 */
	public SessionException(Throwable cause){
		super(cause);
	}
	

}
