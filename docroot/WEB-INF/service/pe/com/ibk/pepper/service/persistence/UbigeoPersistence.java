/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.Ubigeo;

/**
 * The persistence interface for the ubigeo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see UbigeoPersistenceImpl
 * @see UbigeoUtil
 * @generated
 */
public interface UbigeoPersistence extends BasePersistence<Ubigeo> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UbigeoUtil} to access the ubigeo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the ubigeos where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @return the matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByProv(
		java.lang.String codProvincia)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ubigeos where codProvincia = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codProvincia the cod provincia
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByProv(
		java.lang.String codProvincia, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ubigeos where codProvincia = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codProvincia the cod provincia
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByProv(
		java.lang.String codProvincia, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByProv_First(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the first ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByProv_First(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByProv_Last(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the last ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByProv_Last(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ubigeos before and after the current ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param idUbigeo the primary key of the current ubigeo
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo[] findByProv_PrevAndNext(
		long idUbigeo, java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Removes all the ubigeos where codProvincia = &#63; from the database.
	*
	* @param codProvincia the cod provincia
	* @throws SystemException if a system exception occurred
	*/
	public void removeByProv(java.lang.String codProvincia)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ubigeos where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public int countByProv(java.lang.String codProvincia)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByD_N_P_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByD_N_P_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByD_N_P_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByD_N_P_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ubigeos before and after the current ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param idUbigeo the primary key of the current ubigeo
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo[] findByD_N_P_D_PrevAndNext(
		long idUbigeo, java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Removes all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63; from the database.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @throws SystemException if a system exception occurred
	*/
	public void removeByD_N_P_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public int countByD_N_P_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByD_P_N_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByD_P_N_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByD_P_N_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByD_P_N_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ubigeos before and after the current ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param idUbigeo the primary key of the current ubigeo
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo[] findByD_P_N_D_PrevAndNext(
		long idUbigeo, java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Removes all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63; from the database.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @throws SystemException if a system exception occurred
	*/
	public void removeByD_P_N_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public int countByD_P_N_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ubigeo where codigo = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUbigeoException} if it could not be found.
	*
	* @param codigo the codigo
	* @return the matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByN_C_U(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the ubigeo where codigo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigo the codigo
	* @return the matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByN_C_U(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ubigeo where codigo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigo the codigo
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByN_C_U(
		java.lang.String codigo, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the ubigeo where codigo = &#63; from the database.
	*
	* @param codigo the codigo
	* @return the ubigeo that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo removeByN_C_U(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the number of ubigeos where codigo = &#63;.
	*
	* @param codigo the codigo
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public int countByN_C_U(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the ubigeo in the entity cache if it is enabled.
	*
	* @param ubigeo the ubigeo
	*/
	public void cacheResult(pe.com.ibk.pepper.model.Ubigeo ubigeo);

	/**
	* Caches the ubigeos in the entity cache if it is enabled.
	*
	* @param ubigeos the ubigeos
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Ubigeo> ubigeos);

	/**
	* Creates a new ubigeo with the primary key. Does not add the ubigeo to the database.
	*
	* @param idUbigeo the primary key for the new ubigeo
	* @return the new ubigeo
	*/
	public pe.com.ibk.pepper.model.Ubigeo create(long idUbigeo);

	/**
	* Removes the ubigeo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo that was removed
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo remove(long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	public pe.com.ibk.pepper.model.Ubigeo updateImpl(
		pe.com.ibk.pepper.model.Ubigeo ubigeo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the ubigeo with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchUbigeoException} if it could not be found.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo findByPrimaryKey(long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException;

	/**
	* Returns the ubigeo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo, or <code>null</code> if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Ubigeo fetchByPrimaryKey(long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the ubigeos.
	*
	* @return the ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the ubigeos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the ubigeos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the ubigeos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of ubigeos.
	*
	* @return the number of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}