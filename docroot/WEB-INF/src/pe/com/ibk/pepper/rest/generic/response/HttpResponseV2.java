
package pe.com.ibk.pepper.rest.generic.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import pe.com.ibk.pepper.rest.bancasms.response.EnrollmentInfo;
import pe.com.ibk.pepper.rest.campania.response.Lead;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Address;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Email;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Telephone;

public class HttpResponseV2 implements Serializable{
	
	private static final long serialVersionUID = 6464836328734057934L;

	/*ANTIFRAUDE*/
	@SerializedName(value="valid")
    private Boolean valid;
    
	/*BANCA SMS*/
    @SerializedName(value="enrollmentInfo")
    private List<EnrollmentInfo> enrollmentInfo;
    
    /*OBTENER INFO PERSONA*/
    @SerializedName(value="isCustomer")
    private String isCustomer;
    
    @SerializedName(value="customerId")
	private String customerId;
    
    @SerializedName(value="lastName")
	private String lastName;
    
    @SerializedName(value="secondLastName")
	private String secondLastName;
    
    @SerializedName(value="firstName")
	private String firstName;
    
    @SerializedName(value="secondName")
	private String secondName;
    
    @SerializedName(value="birthdate")
	private String birthdate;
    
    @SerializedName(value="genderType")
	private String genderType;
    
    @SerializedName(value="maritalStatusId")
	private String maritalStatusId;
    
    @SerializedName(value="educationLevel")
	private String educationLevel;
    
    @SerializedName(value="telephones")
	private List<Telephone> telephones;
    
    @SerializedName(value="emails")
	private List<Email> emails;
    
    @SerializedName(value="address")
	private List<Address> address;
    
    /*API/DOCUMENT LEAD*/
    @SerializedName(value="leads")
    private List<Lead> leads;
    
    /*LOGICA DE REINGRESO*/
    @SerializedName(value="ProductHost")
    private String productHost;
    
    @SerializedName(value="SubProductHost")
	private String subProductHost;
    
    @SerializedName(value="FileId")
	private String fileId;
    
    @SerializedName(value="registerDate")
	private String registerDate;
    
    @SerializedName(value="status")
	private String status;
    
    @SerializedName(value="ErrorCode")
	private String errorCode;
    
    @SerializedName(value="ErrorMessage")
	private String errorMessage;
    
    /*BD FRAUDE*/
	@SerializedName(value="isFraud")
	private String isFraud;
    
	 /*GENERAR TOKEN*/
    @SerializedName(value="captcha")
	private String captcha;
    
    @SerializedName(value="send_key")
	private Boolean sendkey;
	
    /*VALIDAR TOKEN*/
	@SerializedName(value="validated")
    private Boolean validated;

	/*FECHA HORA DE ENTREGA*/
	@SerializedName(value="MessageResponse")
	MessageResponse messageResponse;
	
	/*ALTA TC*/
	@SerializedName(value="cvv2")
	private String cvv2;
	
	@SerializedName(value="codigo4csc")
    private String codigo4csc;
	
	@SerializedName(value="numeroTarjeta")
    private String numeroTarjeta;
	
	@SerializedName(value="fechaVencimiento")
    private String fechaVencimiento;
	
	@SerializedName("msResponseMessage")
    private String msResponseMessage;
	
	@SerializedName("httpCode")
    private String httpCode;
	
	@SerializedName("potc")
    private String potc;
	
	@SerializedName("aceptado")
    private Boolean aceptado;
	
	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public List<EnrollmentInfo> getEnrollmentInfo() {
		return enrollmentInfo;
	}

	public void setEnrollmentInfo(List<EnrollmentInfo> enrollmentInfo) {
		this.enrollmentInfo = enrollmentInfo;
	}

	public String getIsCustomer() {
		return isCustomer;
	}

	public void setIsCustomer(String isCustomer) {
		this.isCustomer = isCustomer;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getGenderType() {
		return genderType;
	}

	public void setGenderType(String genderType) {
		this.genderType = genderType;
	}

	public String getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusId(String maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public List<Telephone> getTelephones() {
		return telephones;
	}

	public void setTelephones(List<Telephone> telephones) {
		this.telephones = telephones;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}
	
	public List<Lead> getLeads() {
        return leads;
    }

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
    }

	public String getProductHost() {
		return productHost;
	}

	public void setProductHost(String productHost) {
		this.productHost = productHost;
	}

	public String getSubProductHost() {
		return subProductHost;
	}

	public void setSubProductHost(String subProductHost) {
		this.subProductHost = subProductHost;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public Boolean getSendkey() {
		return sendkey;
	}

	public void setSendkey(Boolean sendkey) {
		this.sendkey = sendkey;
	}

	public Boolean getValidated() {
		return validated;
	}

	public void setValidated(Boolean validated) {
		this.validated = validated;
	}
    
	public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getCodigo4csc() {
        return codigo4csc;
    }

    public void setCodigo4csc(String codigo4csc) {
        this.codigo4csc = codigo4csc;
    }

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getMsResponseMessage() {
		return msResponseMessage;
	}

	public void setMsResponseMessage(String msResponseMessage) {
		this.msResponseMessage = msResponseMessage;
	}

	public String getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	public MessageResponse getMessageResponse() {
		return messageResponse;
	}

	public void setMessageResponse(MessageResponse messageResponse) {
		this.messageResponse = messageResponse;
	}

	public String getIsFraud() {
		return isFraud;
	}

	public void setIsFraud(String isFraud) {
		this.isFraud = isFraud;
	}

	public String getPotc() {
		return potc;
	}

	public void setPotc(String potc) {
		this.potc = potc;
	}

	public Boolean getAceptado() {
		return aceptado;
	}

	public void setAceptado(Boolean aceptado) {
		this.aceptado = aceptado;
	}

}
