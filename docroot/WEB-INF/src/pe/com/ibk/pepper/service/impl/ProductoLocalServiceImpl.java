/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;

import pe.com.ibk.pepper.NoSuchProductoException;
import pe.com.ibk.pepper.model.Producto;
import pe.com.ibk.pepper.service.base.ProductoLocalServiceBaseImpl;

/**
 * The implementation of the producto local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.ProductoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.ProductoLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.ProductoLocalServiceUtil
 */
public class ProductoLocalServiceImpl extends ProductoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.ProductoLocalServiceUtil} to access the producto local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(ProductoLocalServiceImpl.class);
	
	@Override
	public Producto registrarProducto(Producto producto) throws SystemException{
		Date now = DateUtil.newDate();
		if (producto.isNew()) {
			producto.setFechaRegistro(now);
		}
		productoPersistence.update(producto);
		return producto;
	}	
	
	@Override
	public Producto buscarProductoClienteporId(long idProducto){
		Producto producto = null;
		try {
			producto = productoPersistence.findByPC_ID(idProducto);
		} catch (NoSuchProductoException e) {
			_log.error("Error en clase ProductoLocalServiceImpl metodo buscarProductoporId:: NO EXISTE EN PRODUCTO ", e );
		} catch (SystemException e) {
			_log.error("Error en clase ProductoLocalServiceImpl metodo buscarProductoporId:: ", e );
		}
		return producto;
	}		
	
	@Override
	public Producto buscarPorExpediente(long idExpediente){
		Producto producto = null;
		try {
			producto = productoPersistence.findByPC_E(idExpediente); 
		} catch (NoSuchProductoException e) {
			_log.error("NO EXISTE EL CLIENTE");
		}catch (SystemException e) {
			_log.error("Error en clase ClienteLocalServiceImpl metodo buscarPorExpediente:: NO EXISTE EL CLIENTE", e );
		}	
		return producto;
	}
}