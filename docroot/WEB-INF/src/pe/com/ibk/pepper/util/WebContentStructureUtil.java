package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.Node;
import java.util.ArrayList;
import java.util.List;

public class WebContentStructureUtil {

	private WebContentStructureUtil() {	}
	
	public static String getText(com.liferay.portal.kernel.xml.Document document, String fieldname){
		Node node = document.selectSingleNode(getDynamicContentWithRoot(fieldname));
		if (node != null) {
			return node.getText();
		}
		return StringPool.BLANK;
	}
	
	public static String getDynamicContentWithRoot(String fieldname){
		return String.format("/root/dynamic-element[@name='%s']/dynamic-content", fieldname);
	}
	
	public static String getDynamicElementWithRoot(String fieldname){
		return String.format("/root/dynamic-element[@name='%s']", fieldname);
	}
	
	public static String getDynamicContent(String fieldname){
		return String.format("dynamic-element[@name='%s']/dynamic-content", fieldname);
	}
	
	public static String getOnlyDynamicElement(){
		return "dynamic-content";
	}
	
	public static List<Entity> getEntitesByName(com.liferay.portal.kernel.xml.Document document, String fieldName){
		
		List<Entity> enties = new ArrayList<Entity>();
		List<Node> nodes = getNodes(document, fieldName);
		nodesToEnties(nodes, enties);
		
		return enties; 
	}
	
	public static Entity getEntityByName(com.liferay.portal.kernel.xml.Document document, String fieldName){
				
		List<Entity> entites = getEntitesByName(document, fieldName);
		
		return  entites.get(0); 
	}
	
	private static void nodesToEnties(List<Node> nodes, List<Entity> childs){

		for (Node node : nodes) {
				 
			Entity entity = new Entity();
			Element element = (Element) node;
			entity.setName(element.attributeValue("name"));
			entity.setText(element.selectSingleNode("dynamic-content").getText());	 
			childs.add(entity);

			List<Node> nodesElements =  element.selectNodes("dynamic-element");
			
			if (nodesElements != null && !nodesElements.isEmpty()) {
				entity.setChilds(new ArrayList<Entity>());
				nodesToEnties(nodesElements, entity.getChilds());
			} 
		}
	}
	public static List<Node> getNodes(com.liferay.portal.kernel.xml.Document document, String fieldname){
		return document.selectNodes(getDynamicElementWithRoot(fieldname));
	}
}
