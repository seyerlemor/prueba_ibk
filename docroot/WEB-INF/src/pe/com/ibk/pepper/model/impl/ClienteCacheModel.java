/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.Cliente;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Cliente in entity cache.
 *
 * @author Interbank
 * @see Cliente
 * @generated
 */
public class ClienteCacheModel implements CacheModel<Cliente>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(55);

		sb.append("{idDatoCliente=");
		sb.append(idDatoCliente);
		sb.append(", idExpediente=");
		sb.append(idExpediente);
		sb.append(", primerNombre=");
		sb.append(primerNombre);
		sb.append(", segundoNombre=");
		sb.append(segundoNombre);
		sb.append(", apellidoPaterno=");
		sb.append(apellidoPaterno);
		sb.append(", apellidoMaterno=");
		sb.append(apellidoMaterno);
		sb.append(", sexo=");
		sb.append(sexo);
		sb.append(", correo=");
		sb.append(correo);
		sb.append(", telefono=");
		sb.append(telefono);
		sb.append(", estadoCivil=");
		sb.append(estadoCivil);
		sb.append(", situacionLaboral=");
		sb.append(situacionLaboral);
		sb.append(", rucEmpresaTrabajo=");
		sb.append(rucEmpresaTrabajo);
		sb.append(", nivelEducacion=");
		sb.append(nivelEducacion);
		sb.append(", ocupacion=");
		sb.append(ocupacion);
		sb.append(", cargoActual=");
		sb.append(cargoActual);
		sb.append(", actividadNegocio=");
		sb.append(actividadNegocio);
		sb.append(", antiguedadLaboral=");
		sb.append(antiguedadLaboral);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", porcentajeParticipacion=");
		sb.append(porcentajeParticipacion);
		sb.append(", ingresoMensualFijo=");
		sb.append(ingresoMensualFijo);
		sb.append(", ingresoMensualVariable=");
		sb.append(ingresoMensualVariable);
		sb.append(", documento=");
		sb.append(documento);
		sb.append(", terminosCondiciones=");
		sb.append(terminosCondiciones);
		sb.append(", tipoDocumento=");
		sb.append(tipoDocumento);
		sb.append(", flagPEP=");
		sb.append(flagPEP);
		sb.append(", fechaNacimiento=");
		sb.append(fechaNacimiento);
		sb.append(", operador=");
		sb.append(operador);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Cliente toEntityModel() {
		ClienteImpl clienteImpl = new ClienteImpl();

		clienteImpl.setIdDatoCliente(idDatoCliente);
		clienteImpl.setIdExpediente(idExpediente);

		if (primerNombre == null) {
			clienteImpl.setPrimerNombre(StringPool.BLANK);
		}
		else {
			clienteImpl.setPrimerNombre(primerNombre);
		}

		if (segundoNombre == null) {
			clienteImpl.setSegundoNombre(StringPool.BLANK);
		}
		else {
			clienteImpl.setSegundoNombre(segundoNombre);
		}

		if (apellidoPaterno == null) {
			clienteImpl.setApellidoPaterno(StringPool.BLANK);
		}
		else {
			clienteImpl.setApellidoPaterno(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			clienteImpl.setApellidoMaterno(StringPool.BLANK);
		}
		else {
			clienteImpl.setApellidoMaterno(apellidoMaterno);
		}

		if (sexo == null) {
			clienteImpl.setSexo(StringPool.BLANK);
		}
		else {
			clienteImpl.setSexo(sexo);
		}

		if (correo == null) {
			clienteImpl.setCorreo(StringPool.BLANK);
		}
		else {
			clienteImpl.setCorreo(correo);
		}

		if (telefono == null) {
			clienteImpl.setTelefono(StringPool.BLANK);
		}
		else {
			clienteImpl.setTelefono(telefono);
		}

		if (estadoCivil == null) {
			clienteImpl.setEstadoCivil(StringPool.BLANK);
		}
		else {
			clienteImpl.setEstadoCivil(estadoCivil);
		}

		if (situacionLaboral == null) {
			clienteImpl.setSituacionLaboral(StringPool.BLANK);
		}
		else {
			clienteImpl.setSituacionLaboral(situacionLaboral);
		}

		if (rucEmpresaTrabajo == null) {
			clienteImpl.setRucEmpresaTrabajo(StringPool.BLANK);
		}
		else {
			clienteImpl.setRucEmpresaTrabajo(rucEmpresaTrabajo);
		}

		if (nivelEducacion == null) {
			clienteImpl.setNivelEducacion(StringPool.BLANK);
		}
		else {
			clienteImpl.setNivelEducacion(nivelEducacion);
		}

		if (ocupacion == null) {
			clienteImpl.setOcupacion(StringPool.BLANK);
		}
		else {
			clienteImpl.setOcupacion(ocupacion);
		}

		if (cargoActual == null) {
			clienteImpl.setCargoActual(StringPool.BLANK);
		}
		else {
			clienteImpl.setCargoActual(cargoActual);
		}

		if (actividadNegocio == null) {
			clienteImpl.setActividadNegocio(StringPool.BLANK);
		}
		else {
			clienteImpl.setActividadNegocio(actividadNegocio);
		}

		if (antiguedadLaboral == null) {
			clienteImpl.setAntiguedadLaboral(StringPool.BLANK);
		}
		else {
			clienteImpl.setAntiguedadLaboral(antiguedadLaboral);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			clienteImpl.setFechaRegistro(null);
		}
		else {
			clienteImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (porcentajeParticipacion == null) {
			clienteImpl.setPorcentajeParticipacion(StringPool.BLANK);
		}
		else {
			clienteImpl.setPorcentajeParticipacion(porcentajeParticipacion);
		}

		if (ingresoMensualFijo == null) {
			clienteImpl.setIngresoMensualFijo(StringPool.BLANK);
		}
		else {
			clienteImpl.setIngresoMensualFijo(ingresoMensualFijo);
		}

		if (ingresoMensualVariable == null) {
			clienteImpl.setIngresoMensualVariable(StringPool.BLANK);
		}
		else {
			clienteImpl.setIngresoMensualVariable(ingresoMensualVariable);
		}

		if (documento == null) {
			clienteImpl.setDocumento(StringPool.BLANK);
		}
		else {
			clienteImpl.setDocumento(documento);
		}

		if (terminosCondiciones == null) {
			clienteImpl.setTerminosCondiciones(StringPool.BLANK);
		}
		else {
			clienteImpl.setTerminosCondiciones(terminosCondiciones);
		}

		if (tipoDocumento == null) {
			clienteImpl.setTipoDocumento(StringPool.BLANK);
		}
		else {
			clienteImpl.setTipoDocumento(tipoDocumento);
		}

		if (flagPEP == null) {
			clienteImpl.setFlagPEP(StringPool.BLANK);
		}
		else {
			clienteImpl.setFlagPEP(flagPEP);
		}

		if (fechaNacimiento == Long.MIN_VALUE) {
			clienteImpl.setFechaNacimiento(null);
		}
		else {
			clienteImpl.setFechaNacimiento(new Date(fechaNacimiento));
		}

		if (operador == null) {
			clienteImpl.setOperador(StringPool.BLANK);
		}
		else {
			clienteImpl.setOperador(operador);
		}

		clienteImpl.resetOriginalValues();

		return clienteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idDatoCliente = objectInput.readLong();
		idExpediente = objectInput.readLong();
		primerNombre = objectInput.readUTF();
		segundoNombre = objectInput.readUTF();
		apellidoPaterno = objectInput.readUTF();
		apellidoMaterno = objectInput.readUTF();
		sexo = objectInput.readUTF();
		correo = objectInput.readUTF();
		telefono = objectInput.readUTF();
		estadoCivil = objectInput.readUTF();
		situacionLaboral = objectInput.readUTF();
		rucEmpresaTrabajo = objectInput.readUTF();
		nivelEducacion = objectInput.readUTF();
		ocupacion = objectInput.readUTF();
		cargoActual = objectInput.readUTF();
		actividadNegocio = objectInput.readUTF();
		antiguedadLaboral = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
		porcentajeParticipacion = objectInput.readUTF();
		ingresoMensualFijo = objectInput.readUTF();
		ingresoMensualVariable = objectInput.readUTF();
		documento = objectInput.readUTF();
		terminosCondiciones = objectInput.readUTF();
		tipoDocumento = objectInput.readUTF();
		flagPEP = objectInput.readUTF();
		fechaNacimiento = objectInput.readLong();
		operador = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idDatoCliente);
		objectOutput.writeLong(idExpediente);

		if (primerNombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(primerNombre);
		}

		if (segundoNombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(segundoNombre);
		}

		if (apellidoPaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoPaterno);
		}

		if (apellidoMaterno == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidoMaterno);
		}

		if (sexo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sexo);
		}

		if (correo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(correo);
		}

		if (telefono == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telefono);
		}

		if (estadoCivil == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estadoCivil);
		}

		if (situacionLaboral == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(situacionLaboral);
		}

		if (rucEmpresaTrabajo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rucEmpresaTrabajo);
		}

		if (nivelEducacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nivelEducacion);
		}

		if (ocupacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ocupacion);
		}

		if (cargoActual == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cargoActual);
		}

		if (actividadNegocio == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(actividadNegocio);
		}

		if (antiguedadLaboral == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(antiguedadLaboral);
		}

		objectOutput.writeLong(fechaRegistro);

		if (porcentajeParticipacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(porcentajeParticipacion);
		}

		if (ingresoMensualFijo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ingresoMensualFijo);
		}

		if (ingresoMensualVariable == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ingresoMensualVariable);
		}

		if (documento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(documento);
		}

		if (terminosCondiciones == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(terminosCondiciones);
		}

		if (tipoDocumento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDocumento);
		}

		if (flagPEP == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagPEP);
		}

		objectOutput.writeLong(fechaNacimiento);

		if (operador == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(operador);
		}
	}

	public long idDatoCliente;
	public long idExpediente;
	public String primerNombre;
	public String segundoNombre;
	public String apellidoPaterno;
	public String apellidoMaterno;
	public String sexo;
	public String correo;
	public String telefono;
	public String estadoCivil;
	public String situacionLaboral;
	public String rucEmpresaTrabajo;
	public String nivelEducacion;
	public String ocupacion;
	public String cargoActual;
	public String actividadNegocio;
	public String antiguedadLaboral;
	public long fechaRegistro;
	public String porcentajeParticipacion;
	public String ingresoMensualFijo;
	public String ingresoMensualVariable;
	public String documento;
	public String terminosCondiciones;
	public String tipoDocumento;
	public String flagPEP;
	public long fechaNacimiento;
	public String operador;
}