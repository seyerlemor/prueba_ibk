
package pe.com.ibk.pepper.rest.equifax.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ValidacionPreguntasResponse implements Serializable{

	private static final long serialVersionUID = 2633392233237632915L;
	
	@SerializedName(value="numeroOperacion")
	private String numeroOperacion;
	
	@SerializedName(value="resultadoEvaluacion")
    private String resultadoEvaluacion;

	public String getNumeroOperacion() {
		return numeroOperacion;
	}

	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}

	public String getResultadoEvaluacion() {
		return resultadoEvaluacion;
	}

	public void setResultadoEvaluacion(String resultadoEvaluacion) {
		this.resultadoEvaluacion = resultadoEvaluacion;
	}

}
