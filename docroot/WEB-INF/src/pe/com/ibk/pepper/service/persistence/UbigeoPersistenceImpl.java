/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchUbigeoException;
import pe.com.ibk.pepper.model.Ubigeo;
import pe.com.ibk.pepper.model.impl.UbigeoImpl;
import pe.com.ibk.pepper.model.impl.UbigeoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the ubigeo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see UbigeoPersistence
 * @see UbigeoUtil
 * @generated
 */
public class UbigeoPersistenceImpl extends BasePersistenceImpl<Ubigeo>
	implements UbigeoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UbigeoUtil} to access the ubigeo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UbigeoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROV = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProv",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROV = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByProv",
			new String[] { String.class.getName() },
			UbigeoModelImpl.CODPROVINCIA_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROV = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProv",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ubigeos where codProvincia = &#63;.
	 *
	 * @param codProvincia the cod provincia
	 * @return the matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByProv(String codProvincia)
		throws SystemException {
		return findByProv(codProvincia, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the ubigeos where codProvincia = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codProvincia the cod provincia
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @return the range of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByProv(String codProvincia, int start, int end)
		throws SystemException {
		return findByProv(codProvincia, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ubigeos where codProvincia = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codProvincia the cod provincia
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByProv(String codProvincia, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROV;
			finderArgs = new Object[] { codProvincia };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROV;
			finderArgs = new Object[] {
					codProvincia,
					
					start, end, orderByComparator
				};
		}

		List<Ubigeo> list = (List<Ubigeo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Ubigeo ubigeo : list) {
				if (!Validator.equals(codProvincia, ubigeo.getCodProvincia())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_UBIGEO_WHERE);

			boolean bindCodProvincia = false;

			if (codProvincia == null) {
				query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_1);
			}
			else if (codProvincia.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_3);
			}
			else {
				bindCodProvincia = true;

				query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UbigeoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodProvincia) {
					qPos.add(codProvincia);
				}

				if (!pagination) {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Ubigeo>(list);
				}
				else {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ubigeo in the ordered set where codProvincia = &#63;.
	 *
	 * @param codProvincia the cod provincia
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByProv_First(String codProvincia,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByProv_First(codProvincia, orderByComparator);

		if (ubigeo != null) {
			return ubigeo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codProvincia=");
		msg.append(codProvincia);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUbigeoException(msg.toString());
	}

	/**
	 * Returns the first ubigeo in the ordered set where codProvincia = &#63;.
	 *
	 * @param codProvincia the cod provincia
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByProv_First(String codProvincia,
		OrderByComparator orderByComparator) throws SystemException {
		List<Ubigeo> list = findByProv(codProvincia, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ubigeo in the ordered set where codProvincia = &#63;.
	 *
	 * @param codProvincia the cod provincia
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByProv_Last(String codProvincia,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByProv_Last(codProvincia, orderByComparator);

		if (ubigeo != null) {
			return ubigeo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codProvincia=");
		msg.append(codProvincia);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUbigeoException(msg.toString());
	}

	/**
	 * Returns the last ubigeo in the ordered set where codProvincia = &#63;.
	 *
	 * @param codProvincia the cod provincia
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByProv_Last(String codProvincia,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByProv(codProvincia);

		if (count == 0) {
			return null;
		}

		List<Ubigeo> list = findByProv(codProvincia, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ubigeos before and after the current ubigeo in the ordered set where codProvincia = &#63;.
	 *
	 * @param idUbigeo the primary key of the current ubigeo
	 * @param codProvincia the cod provincia
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo[] findByProv_PrevAndNext(long idUbigeo, String codProvincia,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = findByPrimaryKey(idUbigeo);

		Session session = null;

		try {
			session = openSession();

			Ubigeo[] array = new UbigeoImpl[3];

			array[0] = getByProv_PrevAndNext(session, ubigeo, codProvincia,
					orderByComparator, true);

			array[1] = ubigeo;

			array[2] = getByProv_PrevAndNext(session, ubigeo, codProvincia,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Ubigeo getByProv_PrevAndNext(Session session, Ubigeo ubigeo,
		String codProvincia, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_UBIGEO_WHERE);

		boolean bindCodProvincia = false;

		if (codProvincia == null) {
			query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_1);
		}
		else if (codProvincia.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_3);
		}
		else {
			bindCodProvincia = true;

			query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UbigeoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodProvincia) {
			qPos.add(codProvincia);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ubigeo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Ubigeo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ubigeos where codProvincia = &#63; from the database.
	 *
	 * @param codProvincia the cod provincia
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByProv(String codProvincia) throws SystemException {
		for (Ubigeo ubigeo : findByProv(codProvincia, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(ubigeo);
		}
	}

	/**
	 * Returns the number of ubigeos where codProvincia = &#63;.
	 *
	 * @param codProvincia the cod provincia
	 * @return the number of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByProv(String codProvincia) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROV;

		Object[] finderArgs = new Object[] { codProvincia };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_UBIGEO_WHERE);

			boolean bindCodProvincia = false;

			if (codProvincia == null) {
				query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_1);
			}
			else if (codProvincia.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_3);
			}
			else {
				bindCodProvincia = true;

				query.append(_FINDER_COLUMN_PROV_CODPROVINCIA_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodProvincia) {
					qPos.add(codProvincia);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROV_CODPROVINCIA_1 = "ubigeo.codProvincia IS NULL";
	private static final String _FINDER_COLUMN_PROV_CODPROVINCIA_2 = "ubigeo.codProvincia = ?";
	private static final String _FINDER_COLUMN_PROV_CODPROVINCIA_3 = "(ubigeo.codProvincia IS NULL OR ubigeo.codProvincia = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_D_N_P_D = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByD_N_P_D",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_D_N_P_D = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByD_N_P_D",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @return the matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByD_N_P_D(String codDepartamento,
		String codProvincia, String codDistrito) throws SystemException {
		return findByD_N_P_D(codDepartamento, codProvincia, codDistrito,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @return the range of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByD_N_P_D(String codDepartamento,
		String codProvincia, String codDistrito, int start, int end)
		throws SystemException {
		return findByD_N_P_D(codDepartamento, codProvincia, codDistrito, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByD_N_P_D(String codDepartamento,
		String codProvincia, String codDistrito, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_D_N_P_D;
		finderArgs = new Object[] {
				codDepartamento, codProvincia, codDistrito,
				
				start, end, orderByComparator
			};

		List<Ubigeo> list = (List<Ubigeo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Ubigeo ubigeo : list) {
				if (!Validator.equals(codDepartamento,
							ubigeo.getCodDepartamento()) ||
						Validator.equals(codProvincia, ubigeo.getCodProvincia()) ||
						!Validator.equals(codDistrito, ubigeo.getCodDistrito())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_UBIGEO_WHERE);

			boolean bindCodDepartamento = false;

			if (codDepartamento == null) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_1);
			}
			else if (codDepartamento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_3);
			}
			else {
				bindCodDepartamento = true;

				query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_2);
			}

			boolean bindCodProvincia = false;

			if (codProvincia == null) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_1);
			}
			else if (codProvincia.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_3);
			}
			else {
				bindCodProvincia = true;

				query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_2);
			}

			boolean bindCodDistrito = false;

			if (codDistrito == null) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_1);
			}
			else if (codDistrito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_3);
			}
			else {
				bindCodDistrito = true;

				query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UbigeoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodDepartamento) {
					qPos.add(codDepartamento);
				}

				if (bindCodProvincia) {
					qPos.add(codProvincia);
				}

				if (bindCodDistrito) {
					qPos.add(codDistrito);
				}

				if (!pagination) {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Ubigeo>(list);
				}
				else {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByD_N_P_D_First(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByD_N_P_D_First(codDepartamento, codProvincia,
				codDistrito, orderByComparator);

		if (ubigeo != null) {
			return ubigeo;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codDepartamento=");
		msg.append(codDepartamento);

		msg.append(", codProvincia=");
		msg.append(codProvincia);

		msg.append(", codDistrito=");
		msg.append(codDistrito);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUbigeoException(msg.toString());
	}

	/**
	 * Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByD_N_P_D_First(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator) throws SystemException {
		List<Ubigeo> list = findByD_N_P_D(codDepartamento, codProvincia,
				codDistrito, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByD_N_P_D_Last(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByD_N_P_D_Last(codDepartamento, codProvincia,
				codDistrito, orderByComparator);

		if (ubigeo != null) {
			return ubigeo;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codDepartamento=");
		msg.append(codDepartamento);

		msg.append(", codProvincia=");
		msg.append(codProvincia);

		msg.append(", codDistrito=");
		msg.append(codDistrito);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUbigeoException(msg.toString());
	}

	/**
	 * Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByD_N_P_D_Last(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByD_N_P_D(codDepartamento, codProvincia, codDistrito);

		if (count == 0) {
			return null;
		}

		List<Ubigeo> list = findByD_N_P_D(codDepartamento, codProvincia,
				codDistrito, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ubigeos before and after the current ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param idUbigeo the primary key of the current ubigeo
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo[] findByD_N_P_D_PrevAndNext(long idUbigeo,
		String codDepartamento, String codProvincia, String codDistrito,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = findByPrimaryKey(idUbigeo);

		Session session = null;

		try {
			session = openSession();

			Ubigeo[] array = new UbigeoImpl[3];

			array[0] = getByD_N_P_D_PrevAndNext(session, ubigeo,
					codDepartamento, codProvincia, codDistrito,
					orderByComparator, true);

			array[1] = ubigeo;

			array[2] = getByD_N_P_D_PrevAndNext(session, ubigeo,
					codDepartamento, codProvincia, codDistrito,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Ubigeo getByD_N_P_D_PrevAndNext(Session session, Ubigeo ubigeo,
		String codDepartamento, String codProvincia, String codDistrito,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_UBIGEO_WHERE);

		boolean bindCodDepartamento = false;

		if (codDepartamento == null) {
			query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_1);
		}
		else if (codDepartamento.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_3);
		}
		else {
			bindCodDepartamento = true;

			query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_2);
		}

		boolean bindCodProvincia = false;

		if (codProvincia == null) {
			query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_1);
		}
		else if (codProvincia.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_3);
		}
		else {
			bindCodProvincia = true;

			query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_2);
		}

		boolean bindCodDistrito = false;

		if (codDistrito == null) {
			query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_1);
		}
		else if (codDistrito.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_3);
		}
		else {
			bindCodDistrito = true;

			query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UbigeoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodDepartamento) {
			qPos.add(codDepartamento);
		}

		if (bindCodProvincia) {
			qPos.add(codProvincia);
		}

		if (bindCodDistrito) {
			qPos.add(codDistrito);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ubigeo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Ubigeo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63; from the database.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByD_N_P_D(String codDepartamento, String codProvincia,
		String codDistrito) throws SystemException {
		for (Ubigeo ubigeo : findByD_N_P_D(codDepartamento, codProvincia,
				codDistrito, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ubigeo);
		}
	}

	/**
	 * Returns the number of ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @return the number of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByD_N_P_D(String codDepartamento, String codProvincia,
		String codDistrito) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_D_N_P_D;

		Object[] finderArgs = new Object[] {
				codDepartamento, codProvincia, codDistrito
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_UBIGEO_WHERE);

			boolean bindCodDepartamento = false;

			if (codDepartamento == null) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_1);
			}
			else if (codDepartamento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_3);
			}
			else {
				bindCodDepartamento = true;

				query.append(_FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_2);
			}

			boolean bindCodProvincia = false;

			if (codProvincia == null) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_1);
			}
			else if (codProvincia.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_3);
			}
			else {
				bindCodProvincia = true;

				query.append(_FINDER_COLUMN_D_N_P_D_CODPROVINCIA_2);
			}

			boolean bindCodDistrito = false;

			if (codDistrito == null) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_1);
			}
			else if (codDistrito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_3);
			}
			else {
				bindCodDistrito = true;

				query.append(_FINDER_COLUMN_D_N_P_D_CODDISTRITO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodDepartamento) {
					qPos.add(codDepartamento);
				}

				if (bindCodProvincia) {
					qPos.add(codProvincia);
				}

				if (bindCodDistrito) {
					qPos.add(codDistrito);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_1 = "ubigeo.codDepartamento IS NULL AND ";
	private static final String _FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_2 = "ubigeo.codDepartamento = ? AND ";
	private static final String _FINDER_COLUMN_D_N_P_D_CODDEPARTAMENTO_3 = "(ubigeo.codDepartamento IS NULL OR ubigeo.codDepartamento = '') AND ";
	private static final String _FINDER_COLUMN_D_N_P_D_CODPROVINCIA_1 = "ubigeo.codProvincia IS NOT NULL AND ";
	private static final String _FINDER_COLUMN_D_N_P_D_CODPROVINCIA_2 = "ubigeo.codProvincia != ? AND ";
	private static final String _FINDER_COLUMN_D_N_P_D_CODPROVINCIA_3 = "(ubigeo.codProvincia IS NULL OR ubigeo.codProvincia != '') AND ";
	private static final String _FINDER_COLUMN_D_N_P_D_CODDISTRITO_1 = "ubigeo.codDistrito IS NULL";
	private static final String _FINDER_COLUMN_D_N_P_D_CODDISTRITO_2 = "ubigeo.codDistrito = ?";
	private static final String _FINDER_COLUMN_D_N_P_D_CODDISTRITO_3 = "(ubigeo.codDistrito IS NULL OR ubigeo.codDistrito = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_D_P_N_D = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByD_P_N_D",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_D_P_N_D = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByD_P_N_D",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @return the matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByD_P_N_D(String codDepartamento,
		String codProvincia, String codDistrito) throws SystemException {
		return findByD_P_N_D(codDepartamento, codProvincia, codDistrito,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @return the range of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByD_P_N_D(String codDepartamento,
		String codProvincia, String codDistrito, int start, int end)
		throws SystemException {
		return findByD_P_N_D(codDepartamento, codProvincia, codDistrito, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findByD_P_N_D(String codDepartamento,
		String codProvincia, String codDistrito, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_D_P_N_D;
		finderArgs = new Object[] {
				codDepartamento, codProvincia, codDistrito,
				
				start, end, orderByComparator
			};

		List<Ubigeo> list = (List<Ubigeo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Ubigeo ubigeo : list) {
				if (!Validator.equals(codDepartamento,
							ubigeo.getCodDepartamento()) ||
						!Validator.equals(codProvincia, ubigeo.getCodProvincia()) ||
						Validator.equals(codDistrito, ubigeo.getCodDistrito())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_UBIGEO_WHERE);

			boolean bindCodDepartamento = false;

			if (codDepartamento == null) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_1);
			}
			else if (codDepartamento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_3);
			}
			else {
				bindCodDepartamento = true;

				query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_2);
			}

			boolean bindCodProvincia = false;

			if (codProvincia == null) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_1);
			}
			else if (codProvincia.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_3);
			}
			else {
				bindCodProvincia = true;

				query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_2);
			}

			boolean bindCodDistrito = false;

			if (codDistrito == null) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_1);
			}
			else if (codDistrito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_3);
			}
			else {
				bindCodDistrito = true;

				query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UbigeoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodDepartamento) {
					qPos.add(codDepartamento);
				}

				if (bindCodProvincia) {
					qPos.add(codProvincia);
				}

				if (bindCodDistrito) {
					qPos.add(codDistrito);
				}

				if (!pagination) {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Ubigeo>(list);
				}
				else {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByD_P_N_D_First(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByD_P_N_D_First(codDepartamento, codProvincia,
				codDistrito, orderByComparator);

		if (ubigeo != null) {
			return ubigeo;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codDepartamento=");
		msg.append(codDepartamento);

		msg.append(", codProvincia=");
		msg.append(codProvincia);

		msg.append(", codDistrito=");
		msg.append(codDistrito);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUbigeoException(msg.toString());
	}

	/**
	 * Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByD_P_N_D_First(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator) throws SystemException {
		List<Ubigeo> list = findByD_P_N_D(codDepartamento, codProvincia,
				codDistrito, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByD_P_N_D_Last(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByD_P_N_D_Last(codDepartamento, codProvincia,
				codDistrito, orderByComparator);

		if (ubigeo != null) {
			return ubigeo;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codDepartamento=");
		msg.append(codDepartamento);

		msg.append(", codProvincia=");
		msg.append(codProvincia);

		msg.append(", codDistrito=");
		msg.append(codDistrito);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUbigeoException(msg.toString());
	}

	/**
	 * Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByD_P_N_D_Last(String codDepartamento,
		String codProvincia, String codDistrito,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByD_P_N_D(codDepartamento, codProvincia, codDistrito);

		if (count == 0) {
			return null;
		}

		List<Ubigeo> list = findByD_P_N_D(codDepartamento, codProvincia,
				codDistrito, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ubigeos before and after the current ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param idUbigeo the primary key of the current ubigeo
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo[] findByD_P_N_D_PrevAndNext(long idUbigeo,
		String codDepartamento, String codProvincia, String codDistrito,
		OrderByComparator orderByComparator)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = findByPrimaryKey(idUbigeo);

		Session session = null;

		try {
			session = openSession();

			Ubigeo[] array = new UbigeoImpl[3];

			array[0] = getByD_P_N_D_PrevAndNext(session, ubigeo,
					codDepartamento, codProvincia, codDistrito,
					orderByComparator, true);

			array[1] = ubigeo;

			array[2] = getByD_P_N_D_PrevAndNext(session, ubigeo,
					codDepartamento, codProvincia, codDistrito,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Ubigeo getByD_P_N_D_PrevAndNext(Session session, Ubigeo ubigeo,
		String codDepartamento, String codProvincia, String codDistrito,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_UBIGEO_WHERE);

		boolean bindCodDepartamento = false;

		if (codDepartamento == null) {
			query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_1);
		}
		else if (codDepartamento.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_3);
		}
		else {
			bindCodDepartamento = true;

			query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_2);
		}

		boolean bindCodProvincia = false;

		if (codProvincia == null) {
			query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_1);
		}
		else if (codProvincia.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_3);
		}
		else {
			bindCodProvincia = true;

			query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_2);
		}

		boolean bindCodDistrito = false;

		if (codDistrito == null) {
			query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_1);
		}
		else if (codDistrito.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_3);
		}
		else {
			bindCodDistrito = true;

			query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UbigeoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodDepartamento) {
			qPos.add(codDepartamento);
		}

		if (bindCodProvincia) {
			qPos.add(codProvincia);
		}

		if (bindCodDistrito) {
			qPos.add(codDistrito);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ubigeo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Ubigeo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63; from the database.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByD_P_N_D(String codDepartamento, String codProvincia,
		String codDistrito) throws SystemException {
		for (Ubigeo ubigeo : findByD_P_N_D(codDepartamento, codProvincia,
				codDistrito, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ubigeo);
		}
	}

	/**
	 * Returns the number of ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	 *
	 * @param codDepartamento the cod departamento
	 * @param codProvincia the cod provincia
	 * @param codDistrito the cod distrito
	 * @return the number of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByD_P_N_D(String codDepartamento, String codProvincia,
		String codDistrito) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_D_P_N_D;

		Object[] finderArgs = new Object[] {
				codDepartamento, codProvincia, codDistrito
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_UBIGEO_WHERE);

			boolean bindCodDepartamento = false;

			if (codDepartamento == null) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_1);
			}
			else if (codDepartamento.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_3);
			}
			else {
				bindCodDepartamento = true;

				query.append(_FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_2);
			}

			boolean bindCodProvincia = false;

			if (codProvincia == null) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_1);
			}
			else if (codProvincia.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_3);
			}
			else {
				bindCodProvincia = true;

				query.append(_FINDER_COLUMN_D_P_N_D_CODPROVINCIA_2);
			}

			boolean bindCodDistrito = false;

			if (codDistrito == null) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_1);
			}
			else if (codDistrito.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_3);
			}
			else {
				bindCodDistrito = true;

				query.append(_FINDER_COLUMN_D_P_N_D_CODDISTRITO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodDepartamento) {
					qPos.add(codDepartamento);
				}

				if (bindCodProvincia) {
					qPos.add(codProvincia);
				}

				if (bindCodDistrito) {
					qPos.add(codDistrito);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_1 = "ubigeo.codDepartamento IS NULL AND ";
	private static final String _FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_2 = "ubigeo.codDepartamento = ? AND ";
	private static final String _FINDER_COLUMN_D_P_N_D_CODDEPARTAMENTO_3 = "(ubigeo.codDepartamento IS NULL OR ubigeo.codDepartamento = '') AND ";
	private static final String _FINDER_COLUMN_D_P_N_D_CODPROVINCIA_1 = "ubigeo.codProvincia IS NULL AND ";
	private static final String _FINDER_COLUMN_D_P_N_D_CODPROVINCIA_2 = "ubigeo.codProvincia = ? AND ";
	private static final String _FINDER_COLUMN_D_P_N_D_CODPROVINCIA_3 = "(ubigeo.codProvincia IS NULL OR ubigeo.codProvincia = '') AND ";
	private static final String _FINDER_COLUMN_D_P_N_D_CODDISTRITO_1 = "ubigeo.codDistrito IS NOT NULL";
	private static final String _FINDER_COLUMN_D_P_N_D_CODDISTRITO_2 = "ubigeo.codDistrito != ?";
	private static final String _FINDER_COLUMN_D_P_N_D_CODDISTRITO_3 = "(ubigeo.codDistrito IS NULL OR ubigeo.codDistrito != '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_N_C_U = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, UbigeoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByN_C_U",
			new String[] { String.class.getName() },
			UbigeoModelImpl.CODIGO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_N_C_U = new FinderPath(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByN_C_U",
			new String[] { String.class.getName() });

	/**
	 * Returns the ubigeo where codigo = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUbigeoException} if it could not be found.
	 *
	 * @param codigo the codigo
	 * @return the matching ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByN_C_U(String codigo)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByN_C_U(codigo);

		if (ubigeo == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigo=");
			msg.append(codigo);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchUbigeoException(msg.toString());
		}

		return ubigeo;
	}

	/**
	 * Returns the ubigeo where codigo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigo the codigo
	 * @return the matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByN_C_U(String codigo) throws SystemException {
		return fetchByN_C_U(codigo, true);
	}

	/**
	 * Returns the ubigeo where codigo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigo the codigo
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByN_C_U(String codigo, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { codigo };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_N_C_U,
					finderArgs, this);
		}

		if (result instanceof Ubigeo) {
			Ubigeo ubigeo = (Ubigeo)result;

			if (!Validator.equals(codigo, ubigeo.getCodigo())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_UBIGEO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_N_C_U_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_N_C_U_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_N_C_U_CODIGO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				List<Ubigeo> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_N_C_U,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"UbigeoPersistenceImpl.fetchByN_C_U(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Ubigeo ubigeo = list.get(0);

					result = ubigeo;

					cacheResult(ubigeo);

					if ((ubigeo.getCodigo() == null) ||
							!ubigeo.getCodigo().equals(codigo)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_N_C_U,
							finderArgs, ubigeo);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_N_C_U,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Ubigeo)result;
		}
	}

	/**
	 * Removes the ubigeo where codigo = &#63; from the database.
	 *
	 * @param codigo the codigo
	 * @return the ubigeo that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo removeByN_C_U(String codigo)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = findByN_C_U(codigo);

		return remove(ubigeo);
	}

	/**
	 * Returns the number of ubigeos where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @return the number of matching ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByN_C_U(String codigo) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_N_C_U;

		Object[] finderArgs = new Object[] { codigo };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_UBIGEO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_N_C_U_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_N_C_U_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_N_C_U_CODIGO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_N_C_U_CODIGO_1 = "ubigeo.codigo IS NULL";
	private static final String _FINDER_COLUMN_N_C_U_CODIGO_2 = "ubigeo.codigo = ?";
	private static final String _FINDER_COLUMN_N_C_U_CODIGO_3 = "(ubigeo.codigo IS NULL OR ubigeo.codigo = '')";

	public UbigeoPersistenceImpl() {
		setModelClass(Ubigeo.class);
	}

	/**
	 * Caches the ubigeo in the entity cache if it is enabled.
	 *
	 * @param ubigeo the ubigeo
	 */
	@Override
	public void cacheResult(Ubigeo ubigeo) {
		EntityCacheUtil.putResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoImpl.class, ubigeo.getPrimaryKey(), ubigeo);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_N_C_U,
			new Object[] { ubigeo.getCodigo() }, ubigeo);

		ubigeo.resetOriginalValues();
	}

	/**
	 * Caches the ubigeos in the entity cache if it is enabled.
	 *
	 * @param ubigeos the ubigeos
	 */
	@Override
	public void cacheResult(List<Ubigeo> ubigeos) {
		for (Ubigeo ubigeo : ubigeos) {
			if (EntityCacheUtil.getResult(
						UbigeoModelImpl.ENTITY_CACHE_ENABLED, UbigeoImpl.class,
						ubigeo.getPrimaryKey()) == null) {
				cacheResult(ubigeo);
			}
			else {
				ubigeo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ubigeos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UbigeoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UbigeoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ubigeo.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Ubigeo ubigeo) {
		EntityCacheUtil.removeResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoImpl.class, ubigeo.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(ubigeo);
	}

	@Override
	public void clearCache(List<Ubigeo> ubigeos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Ubigeo ubigeo : ubigeos) {
			EntityCacheUtil.removeResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
				UbigeoImpl.class, ubigeo.getPrimaryKey());

			clearUniqueFindersCache(ubigeo);
		}
	}

	protected void cacheUniqueFindersCache(Ubigeo ubigeo) {
		if (ubigeo.isNew()) {
			Object[] args = new Object[] { ubigeo.getCodigo() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_N_C_U, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_N_C_U, args, ubigeo);
		}
		else {
			UbigeoModelImpl ubigeoModelImpl = (UbigeoModelImpl)ubigeo;

			if ((ubigeoModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_N_C_U.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { ubigeo.getCodigo() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_N_C_U, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_N_C_U, args,
					ubigeo);
			}
		}
	}

	protected void clearUniqueFindersCache(Ubigeo ubigeo) {
		UbigeoModelImpl ubigeoModelImpl = (UbigeoModelImpl)ubigeo;

		Object[] args = new Object[] { ubigeo.getCodigo() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_N_C_U, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_N_C_U, args);

		if ((ubigeoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_N_C_U.getColumnBitmask()) != 0) {
			args = new Object[] { ubigeoModelImpl.getOriginalCodigo() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_N_C_U, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_N_C_U, args);
		}
	}

	/**
	 * Creates a new ubigeo with the primary key. Does not add the ubigeo to the database.
	 *
	 * @param idUbigeo the primary key for the new ubigeo
	 * @return the new ubigeo
	 */
	@Override
	public Ubigeo create(long idUbigeo) {
		Ubigeo ubigeo = new UbigeoImpl();

		ubigeo.setNew(true);
		ubigeo.setPrimaryKey(idUbigeo);

		return ubigeo;
	}

	/**
	 * Removes the ubigeo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idUbigeo the primary key of the ubigeo
	 * @return the ubigeo that was removed
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo remove(long idUbigeo)
		throws NoSuchUbigeoException, SystemException {
		return remove((Serializable)idUbigeo);
	}

	/**
	 * Removes the ubigeo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ubigeo
	 * @return the ubigeo that was removed
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo remove(Serializable primaryKey)
		throws NoSuchUbigeoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Ubigeo ubigeo = (Ubigeo)session.get(UbigeoImpl.class, primaryKey);

			if (ubigeo == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUbigeoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ubigeo);
		}
		catch (NoSuchUbigeoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Ubigeo removeImpl(Ubigeo ubigeo) throws SystemException {
		ubigeo = toUnwrappedModel(ubigeo);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ubigeo)) {
				ubigeo = (Ubigeo)session.get(UbigeoImpl.class,
						ubigeo.getPrimaryKeyObj());
			}

			if (ubigeo != null) {
				session.delete(ubigeo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ubigeo != null) {
			clearCache(ubigeo);
		}

		return ubigeo;
	}

	@Override
	public Ubigeo updateImpl(pe.com.ibk.pepper.model.Ubigeo ubigeo)
		throws SystemException {
		ubigeo = toUnwrappedModel(ubigeo);

		boolean isNew = ubigeo.isNew();

		UbigeoModelImpl ubigeoModelImpl = (UbigeoModelImpl)ubigeo;

		Session session = null;

		try {
			session = openSession();

			if (ubigeo.isNew()) {
				session.save(ubigeo);

				ubigeo.setNew(false);
			}
			else {
				session.merge(ubigeo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UbigeoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ubigeoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROV.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ubigeoModelImpl.getOriginalCodProvincia()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROV, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROV,
					args);

				args = new Object[] { ubigeoModelImpl.getCodProvincia() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROV, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROV,
					args);
			}
		}

		EntityCacheUtil.putResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
			UbigeoImpl.class, ubigeo.getPrimaryKey(), ubigeo);

		clearUniqueFindersCache(ubigeo);
		cacheUniqueFindersCache(ubigeo);

		return ubigeo;
	}

	protected Ubigeo toUnwrappedModel(Ubigeo ubigeo) {
		if (ubigeo instanceof UbigeoImpl) {
			return ubigeo;
		}

		UbigeoImpl ubigeoImpl = new UbigeoImpl();

		ubigeoImpl.setNew(ubigeo.isNew());
		ubigeoImpl.setPrimaryKey(ubigeo.getPrimaryKey());

		ubigeoImpl.setIdUbigeo(ubigeo.getIdUbigeo());
		ubigeoImpl.setCodigo(ubigeo.getCodigo());
		ubigeoImpl.setNombre(ubigeo.getNombre());
		ubigeoImpl.setCodDepartamento(ubigeo.getCodDepartamento());
		ubigeoImpl.setCodProvincia(ubigeo.getCodProvincia());
		ubigeoImpl.setCodDistrito(ubigeo.getCodDistrito());

		return ubigeoImpl;
	}

	/**
	 * Returns the ubigeo with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ubigeo
	 * @return the ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUbigeoException, SystemException {
		Ubigeo ubigeo = fetchByPrimaryKey(primaryKey);

		if (ubigeo == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUbigeoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ubigeo;
	}

	/**
	 * Returns the ubigeo with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchUbigeoException} if it could not be found.
	 *
	 * @param idUbigeo the primary key of the ubigeo
	 * @return the ubigeo
	 * @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo findByPrimaryKey(long idUbigeo)
		throws NoSuchUbigeoException, SystemException {
		return findByPrimaryKey((Serializable)idUbigeo);
	}

	/**
	 * Returns the ubigeo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ubigeo
	 * @return the ubigeo, or <code>null</code> if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Ubigeo ubigeo = (Ubigeo)EntityCacheUtil.getResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
				UbigeoImpl.class, primaryKey);

		if (ubigeo == _nullUbigeo) {
			return null;
		}

		if (ubigeo == null) {
			Session session = null;

			try {
				session = openSession();

				ubigeo = (Ubigeo)session.get(UbigeoImpl.class, primaryKey);

				if (ubigeo != null) {
					cacheResult(ubigeo);
				}
				else {
					EntityCacheUtil.putResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
						UbigeoImpl.class, primaryKey, _nullUbigeo);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UbigeoModelImpl.ENTITY_CACHE_ENABLED,
					UbigeoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ubigeo;
	}

	/**
	 * Returns the ubigeo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idUbigeo the primary key of the ubigeo
	 * @return the ubigeo, or <code>null</code> if a ubigeo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Ubigeo fetchByPrimaryKey(long idUbigeo) throws SystemException {
		return fetchByPrimaryKey((Serializable)idUbigeo);
	}

	/**
	 * Returns all the ubigeos.
	 *
	 * @return the ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ubigeos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @return the range of ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ubigeos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ubigeos
	 * @param end the upper bound of the range of ubigeos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Ubigeo> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Ubigeo> list = (List<Ubigeo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_UBIGEO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_UBIGEO;

				if (pagination) {
					sql = sql.concat(UbigeoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Ubigeo>(list);
				}
				else {
					list = (List<Ubigeo>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ubigeos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Ubigeo ubigeo : findAll()) {
			remove(ubigeo);
		}
	}

	/**
	 * Returns the number of ubigeos.
	 *
	 * @return the number of ubigeos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_UBIGEO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the ubigeo persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.Ubigeo")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Ubigeo>> listenersList = new ArrayList<ModelListener<Ubigeo>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Ubigeo>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UbigeoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_UBIGEO = "SELECT ubigeo FROM Ubigeo ubigeo";
	private static final String _SQL_SELECT_UBIGEO_WHERE = "SELECT ubigeo FROM Ubigeo ubigeo WHERE ";
	private static final String _SQL_COUNT_UBIGEO = "SELECT COUNT(ubigeo) FROM Ubigeo ubigeo";
	private static final String _SQL_COUNT_UBIGEO_WHERE = "SELECT COUNT(ubigeo) FROM Ubigeo ubigeo WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ubigeo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Ubigeo exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Ubigeo exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UbigeoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idUbigeo", "codigo", "nombre", "codDepartamento",
				"codProvincia", "codDistrito"
			});
	private static Ubigeo _nullUbigeo = new UbigeoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Ubigeo> toCacheModel() {
				return _nullUbigeoCacheModel;
			}
		};

	private static CacheModel<Ubigeo> _nullUbigeoCacheModel = new CacheModel<Ubigeo>() {
			@Override
			public Ubigeo toEntityModel() {
				return _nullUbigeo;
			}
		};
}