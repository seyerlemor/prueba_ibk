package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

import java.util.ArrayList;
import java.util.List;

import pe.com.ibk.pepper.NoSuchUbigeoException;
import pe.com.ibk.pepper.model.Ubigeo;
import pe.com.ibk.pepper.model.impl.UbigeoImpl;


/**
 * @author Interbank
 */
public class UbigeoFinderImpl extends BasePersistenceImpl<Ubigeo> implements UbigeoFinder{

	private static final Log _log = LogFactoryUtil.getLog(UbigeoFinderImpl.class);
	
	public static final String FINDER_OBTENER_UBIGEO_DEPARTAMENTO = "Ubigeo.obtenerUbigeoNombreDep";
	public static final String FINDER_OBTENER_UBIGEO_PROVINCIA = "Ubigeo.obtenerUbigeoNombreProv";
	public static final String FINDER_OBTENER_UBIGEO_DISTRITO = "Ubigeo.obtenerUbigeoNombreDist";
	public static final String FINDER_OBTENER_UBIGEO_CODIGO = "Ubigeo.obtenerUbigeoNombreCodigo";
	

		@SuppressWarnings("unchecked")
		@Override
		public Ubigeo obtenerUbigeoByNombre(String codDepartamento, String codProvincia, String codDistrito, String nombre, int tipo) {
			Session session = null;
			try {		
				session = openSession();
				String sql = "";
				if(tipo==1){
					sql=CustomSQLUtil.get(FINDER_OBTENER_UBIGEO_DEPARTAMENTO);
				}else if(tipo==2){
					sql=CustomSQLUtil.get(FINDER_OBTENER_UBIGEO_PROVINCIA);
				}else if(tipo==3){
					sql=CustomSQLUtil.get(FINDER_OBTENER_UBIGEO_DISTRITO);
				}				
				SQLQuery q = session.createSQLQuery(sql);
				q.addEntity(UbigeoImpl.TABLE_NAME, UbigeoImpl.class);
				QueryPos qPos = QueryPos.getInstance(q);
				qPos.add(codDepartamento);
				qPos.add(codProvincia);
				qPos.add(codDistrito);
				qPos.add(nombre);
				List<Ubigeo> ubigeo = q.list();
				if (!ubigeo.isEmpty() && ubigeo.size()==1) {
					return ubigeo.get(0);
				}else{
					new NoSuchUbigeoException("Busqueda diferente de 1 ");
				}
			} catch (Exception e) {
		        _log.debug(e);
			} finally {
				closeSession(session);
			}
			return null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public String obtenerUbigeoByCodigo(String codigo) {
			_log.debug(codigo);
			Session session = null;
			String nombre = StringPool.BLANK;
			List<Ubigeo> out = new ArrayList<>();
			try {		
				session = openSession();
				String sql = CustomSQLUtil.get(FINDER_OBTENER_UBIGEO_CODIGO);	
				SQLQuery q = session.createSQLQuery(sql);
				q.addEntity("Ubigeo", UbigeoImpl.class);
				QueryPos qPos = QueryPos.getInstance(q);
				qPos.add(codigo);
				out = q.list();
				nombre = out.get(0).getNombre();
			} catch (Exception e) {
		        _log.debug("Error en obtenerUbigeoByCodigo:"+codigo);
			} finally {
				closeSession(session);
			}
			return nombre;
		}
}
