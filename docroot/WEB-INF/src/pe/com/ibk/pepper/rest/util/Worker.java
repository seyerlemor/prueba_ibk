package pe.com.ibk.pepper.rest.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.concurrent.Future;

import javax.ws.rs.core.Response;

public class Worker implements Runnable {

	private static final Log _log = LogFactoryUtil.getLog(Worker.class);

	private Future<Response> futureResponse;

	public Worker(Future<Response> futureResponse) {
		this.futureResponse = futureResponse;
	}

	@Override
	public void run() {
		try {
			final Response response = futureResponse.get();
			if (response.getStatus() == 500) 
				_log.debug("error 500");
			else 
				response.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
