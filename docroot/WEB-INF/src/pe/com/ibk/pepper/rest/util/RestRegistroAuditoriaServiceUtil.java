
package pe.com.ibk.pepper.rest.util;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.client.AsyncInvoker;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.rest.config.SecurityRestClientGeneral;
import pe.com.ibk.pepper.rest.registroauditoria.request.RegistroAuditoriaRequest;
import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;

public class RestRegistroAuditoriaServiceUtil {
	private static final Log _log = LogFactoryUtil.getLog(RestRegistroAuditoriaServiceUtil.class);
	private static final  ExecutorService executorService = Executors.newFixedThreadPool(20);	
	
	private RestRegistroAuditoriaServiceUtil(){}
	
	public static void registroAudtoriaLog(RegistroAuditoriaRequest requestBean) throws WebServicesException {
		try {			 
			Gson gson = new Gson(); 
			String request = gson.toJson(requestBean);			
			Builder crearClienteClient = SecurityRestClientGeneral.getSecurityRestClient(
					PrefsPropsUtil.getString(PortalKeys.REST_ENVIO_AUDITORIA_ENDPOINT, PortalValues.REST_ENVIO_AUDITORIA_ENDPOINT),
					PrefsPropsUtil.getString(PortalKeys.REST_ENVIO_AUDITORIA_RESOURCE, PortalValues.REST_ENVIO_AUDITORIA_RESOURCE),
					Boolean.TRUE); 
			AsyncInvoker invoker = crearClienteClient.async();
			Future<Response> responseFuture =  invoker.post(Entity.entity(request, MediaType.APPLICATION_JSON));
			executorService.execute(new Worker(responseFuture));
		} catch (SystemException  e) {
			_log.error(e);
		}		
	}
	
}
