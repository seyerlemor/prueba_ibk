package pe.com.ibk.pepper.rest.listanegra.request;

public class Adrress {
	private String departmentId;
	private String provinceId;
	private String districtId;
	
	public Adrress() {
		super();
	}
	
	public Adrress(String departmentId, String provinceId, String districtId) {
		super();
		this.departmentId = departmentId;
		this.provinceId = provinceId;
		this.districtId = districtId;
	}
	
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	
}
