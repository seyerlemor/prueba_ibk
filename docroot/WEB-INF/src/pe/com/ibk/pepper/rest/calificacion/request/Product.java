
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Product {

    private GeneralProductData generalProductData;
    private String productType;
    private String requestType;
    private String productClass;
    private String productSub;
    
    @JsonCreator
	public Product(GeneralProductData generalProductData, String productType,
			String requestType, String productClass, String productSub) {
		super();
		this.generalProductData = generalProductData;
		this.productType = productType;
		this.requestType = requestType;
		this.productClass = productClass;
		this.productSub = productSub;
	}

    @JsonProperty("generalProductData")
	public GeneralProductData getGeneralProductData() {
		return generalProductData;
	}

	public void setGeneralProductData(GeneralProductData generalProductData) {
		this.generalProductData = generalProductData;
	}

	@JsonProperty("productType")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@JsonProperty("requestType")
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	@JsonProperty("productClass")
	public String getProductClass() {
		return productClass;
	}

	public void setProductClass(String productClass) {
		this.productClass = productClass;
	}

	@JsonProperty("productSub")
	public String getProductSub() {
		return productSub;
	}

	public void setProductSub(String productSub) {
		this.productSub = productSub;
	}

}
