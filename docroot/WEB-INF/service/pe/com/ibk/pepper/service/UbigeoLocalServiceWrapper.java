/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UbigeoLocalService}.
 *
 * @author Interbank
 * @see UbigeoLocalService
 * @generated
 */
public class UbigeoLocalServiceWrapper implements UbigeoLocalService,
	ServiceWrapper<UbigeoLocalService> {
	public UbigeoLocalServiceWrapper(UbigeoLocalService ubigeoLocalService) {
		_ubigeoLocalService = ubigeoLocalService;
	}

	/**
	* Adds the ubigeo to the database. Also notifies the appropriate model listeners.
	*
	* @param ubigeo the ubigeo
	* @return the ubigeo that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Ubigeo addUbigeo(
		pe.com.ibk.pepper.model.Ubigeo ubigeo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.addUbigeo(ubigeo);
	}

	/**
	* Creates a new ubigeo with the primary key. Does not add the ubigeo to the database.
	*
	* @param idUbigeo the primary key for the new ubigeo
	* @return the new ubigeo
	*/
	@Override
	public pe.com.ibk.pepper.model.Ubigeo createUbigeo(long idUbigeo) {
		return _ubigeoLocalService.createUbigeo(idUbigeo);
	}

	/**
	* Deletes the ubigeo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo that was removed
	* @throws PortalException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Ubigeo deleteUbigeo(long idUbigeo)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.deleteUbigeo(idUbigeo);
	}

	/**
	* Deletes the ubigeo from the database. Also notifies the appropriate model listeners.
	*
	* @param ubigeo the ubigeo
	* @return the ubigeo that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Ubigeo deleteUbigeo(
		pe.com.ibk.pepper.model.Ubigeo ubigeo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.deleteUbigeo(ubigeo);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ubigeoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public pe.com.ibk.pepper.model.Ubigeo fetchUbigeo(long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.fetchUbigeo(idUbigeo);
	}

	/**
	* Returns the ubigeo with the primary key.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo
	* @throws PortalException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Ubigeo getUbigeo(long idUbigeo)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getUbigeo(idUbigeo);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ubigeos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> getUbigeos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getUbigeos(start, end);
	}

	/**
	* Returns the number of ubigeos.
	*
	* @return the number of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getUbigeosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getUbigeosCount();
	}

	/**
	* Updates the ubigeo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ubigeo the ubigeo
	* @return the ubigeo that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Ubigeo updateUbigeo(
		pe.com.ibk.pepper.model.Ubigeo ubigeo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.updateUbigeo(ubigeo);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _ubigeoLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ubigeoLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ubigeoLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> getUbigeoByCodProvincia(
		java.lang.String CodProvincia)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getUbigeoByCodProvincia(CodProvincia);
	}

	@Override
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> getUbigeoByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getUbigeoByD_N_P_D(codDepartamento,
			codProvincia, codDistrito);
	}

	@Override
	public java.util.List<pe.com.ibk.pepper.model.Ubigeo> getUbigeoByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ubigeoLocalService.getUbigeoByD_P_N_D(codDepartamento,
			codProvincia, codDistrito);
	}

	@Override
	public pe.com.ibk.pepper.model.Ubigeo getUbigeoByNombre(
		java.lang.String nomDepartamento, java.lang.String nomProvincia,
		java.lang.String nomDistrito) {
		return _ubigeoLocalService.getUbigeoByNombre(nomDepartamento,
			nomProvincia, nomDistrito);
	}

	@Override
	public java.lang.String getUbigeoByCodigo(java.lang.String codigo) {
		return _ubigeoLocalService.getUbigeoByCodigo(codigo);
	}

	@Override
	public java.lang.String getNombreUbigeoByCodigo(java.lang.String codigo) {
		return _ubigeoLocalService.getNombreUbigeoByCodigo(codigo);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UbigeoLocalService getWrappedUbigeoLocalService() {
		return _ubigeoLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUbigeoLocalService(
		UbigeoLocalService ubigeoLocalService) {
		_ubigeoLocalService = ubigeoLocalService;
	}

	@Override
	public UbigeoLocalService getWrappedService() {
		return _ubigeoLocalService;
	}

	@Override
	public void setWrappedService(UbigeoLocalService ubigeoLocalService) {
		_ubigeoLocalService = ubigeoLocalService;
	}

	private UbigeoLocalService _ubigeoLocalService;
}