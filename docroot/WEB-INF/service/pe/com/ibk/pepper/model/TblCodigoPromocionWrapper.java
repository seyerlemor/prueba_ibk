/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TblCodigoPromocion}.
 * </p>
 *
 * @author Interbank
 * @see TblCodigoPromocion
 * @generated
 */
public class TblCodigoPromocionWrapper implements TblCodigoPromocion,
	ModelWrapper<TblCodigoPromocion> {
	public TblCodigoPromocionWrapper(TblCodigoPromocion tblCodigoPromocion) {
		_tblCodigoPromocion = tblCodigoPromocion;
	}

	@Override
	public Class<?> getModelClass() {
		return TblCodigoPromocion.class;
	}

	@Override
	public String getModelClassName() {
		return TblCodigoPromocion.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCodigo", getIdCodigo());
		attributes.put("codigo", getCodigo());
		attributes.put("comercio", getComercio());
		attributes.put("estado", getEstado());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idCodigo = (Integer)attributes.get("idCodigo");

		if (idCodigo != null) {
			setIdCodigo(idCodigo);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String comercio = (String)attributes.get("comercio");

		if (comercio != null) {
			setComercio(comercio);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this tbl codigo promocion.
	*
	* @return the primary key of this tbl codigo promocion
	*/
	@Override
	public int getPrimaryKey() {
		return _tblCodigoPromocion.getPrimaryKey();
	}

	/**
	* Sets the primary key of this tbl codigo promocion.
	*
	* @param primaryKey the primary key of this tbl codigo promocion
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_tblCodigoPromocion.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id codigo of this tbl codigo promocion.
	*
	* @return the id codigo of this tbl codigo promocion
	*/
	@Override
	public int getIdCodigo() {
		return _tblCodigoPromocion.getIdCodigo();
	}

	/**
	* Sets the id codigo of this tbl codigo promocion.
	*
	* @param idCodigo the id codigo of this tbl codigo promocion
	*/
	@Override
	public void setIdCodigo(int idCodigo) {
		_tblCodigoPromocion.setIdCodigo(idCodigo);
	}

	/**
	* Returns the codigo of this tbl codigo promocion.
	*
	* @return the codigo of this tbl codigo promocion
	*/
	@Override
	public java.lang.String getCodigo() {
		return _tblCodigoPromocion.getCodigo();
	}

	/**
	* Sets the codigo of this tbl codigo promocion.
	*
	* @param codigo the codigo of this tbl codigo promocion
	*/
	@Override
	public void setCodigo(java.lang.String codigo) {
		_tblCodigoPromocion.setCodigo(codigo);
	}

	/**
	* Returns the comercio of this tbl codigo promocion.
	*
	* @return the comercio of this tbl codigo promocion
	*/
	@Override
	public java.lang.String getComercio() {
		return _tblCodigoPromocion.getComercio();
	}

	/**
	* Sets the comercio of this tbl codigo promocion.
	*
	* @param comercio the comercio of this tbl codigo promocion
	*/
	@Override
	public void setComercio(java.lang.String comercio) {
		_tblCodigoPromocion.setComercio(comercio);
	}

	/**
	* Returns the estado of this tbl codigo promocion.
	*
	* @return the estado of this tbl codigo promocion
	*/
	@Override
	public java.lang.String getEstado() {
		return _tblCodigoPromocion.getEstado();
	}

	/**
	* Sets the estado of this tbl codigo promocion.
	*
	* @param estado the estado of this tbl codigo promocion
	*/
	@Override
	public void setEstado(java.lang.String estado) {
		_tblCodigoPromocion.setEstado(estado);
	}

	/**
	* Returns the fecha registro of this tbl codigo promocion.
	*
	* @return the fecha registro of this tbl codigo promocion
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _tblCodigoPromocion.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this tbl codigo promocion.
	*
	* @param fechaRegistro the fecha registro of this tbl codigo promocion
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_tblCodigoPromocion.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _tblCodigoPromocion.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_tblCodigoPromocion.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _tblCodigoPromocion.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_tblCodigoPromocion.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _tblCodigoPromocion.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _tblCodigoPromocion.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_tblCodigoPromocion.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _tblCodigoPromocion.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_tblCodigoPromocion.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_tblCodigoPromocion.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_tblCodigoPromocion.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TblCodigoPromocionWrapper((TblCodigoPromocion)_tblCodigoPromocion.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion) {
		return _tblCodigoPromocion.compareTo(tblCodigoPromocion);
	}

	@Override
	public int hashCode() {
		return _tblCodigoPromocion.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.TblCodigoPromocion> toCacheModel() {
		return _tblCodigoPromocion.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.TblCodigoPromocion toEscapedModel() {
		return new TblCodigoPromocionWrapper(_tblCodigoPromocion.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.TblCodigoPromocion toUnescapedModel() {
		return new TblCodigoPromocionWrapper(_tblCodigoPromocion.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _tblCodigoPromocion.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _tblCodigoPromocion.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_tblCodigoPromocion.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TblCodigoPromocionWrapper)) {
			return false;
		}

		TblCodigoPromocionWrapper tblCodigoPromocionWrapper = (TblCodigoPromocionWrapper)obj;

		if (Validator.equals(_tblCodigoPromocion,
					tblCodigoPromocionWrapper._tblCodigoPromocion)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public TblCodigoPromocion getWrappedTblCodigoPromocion() {
		return _tblCodigoPromocion;
	}

	@Override
	public TblCodigoPromocion getWrappedModel() {
		return _tblCodigoPromocion;
	}

	@Override
	public void resetOriginalValues() {
		_tblCodigoPromocion.resetOriginalValues();
	}

	private TblCodigoPromocion _tblCodigoPromocion;
}