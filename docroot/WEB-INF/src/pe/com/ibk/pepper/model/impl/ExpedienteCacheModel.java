/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.Expediente;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Expediente in entity cache.
 *
 * @author Interbank
 * @see Expediente
 * @generated
 */
public class ExpedienteCacheModel implements CacheModel<Expediente>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(69);

		sb.append("{idExpediente=");
		sb.append(idExpediente);
		sb.append(", documento=");
		sb.append(documento);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", paso=");
		sb.append(paso);
		sb.append(", pagina=");
		sb.append(pagina);
		sb.append(", tipoFlujo=");
		sb.append(tipoFlujo);
		sb.append(", flagR1=");
		sb.append(flagR1);
		sb.append(", flagCampania=");
		sb.append(flagCampania);
		sb.append(", flagReniec=");
		sb.append(flagReniec);
		sb.append(", flagEquifax=");
		sb.append(flagEquifax);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", tipoCampania=");
		sb.append(tipoCampania);
		sb.append(", flagCalificacion=");
		sb.append(flagCalificacion);
		sb.append(", flagCliente=");
		sb.append(flagCliente);
		sb.append(", idUsuarioSession=");
		sb.append(idUsuarioSession);
		sb.append(", strJson=");
		sb.append(strJson);
		sb.append(", formHtml=");
		sb.append(formHtml);
		sb.append(", codigoHtml=");
		sb.append(codigoHtml);
		sb.append(", periodoRegistro=");
		sb.append(periodoRegistro);
		sb.append(", modificacion=");
		sb.append(modificacion);
		sb.append(", flagTipificacion=");
		sb.append(flagTipificacion);
		sb.append(", flagActualizarRespuesta=");
		sb.append(flagActualizarRespuesta);
		sb.append(", flagObtenerInformacion=");
		sb.append(flagObtenerInformacion);
		sb.append(", flagConsultarAfiliacion=");
		sb.append(flagConsultarAfiliacion);
		sb.append(", flagGeneracionToken=");
		sb.append(flagGeneracionToken);
		sb.append(", flagVentaTC=");
		sb.append(flagVentaTC);
		sb.append(", flagValidacionToken=");
		sb.append(flagValidacionToken);
		sb.append(", flagRecompraGenPOTC=");
		sb.append(flagRecompraGenPOTC);
		sb.append(", flagRecompraConPOTC=");
		sb.append(flagRecompraConPOTC);
		sb.append(", flagExtornoGenPOTC=");
		sb.append(flagExtornoGenPOTC);
		sb.append(", flagExtornoConPOTC=");
		sb.append(flagExtornoConPOTC);
		sb.append(", ip=");
		sb.append(ip);
		sb.append(", navegador=");
		sb.append(navegador);
		sb.append(", pepperType=");
		sb.append(pepperType);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Expediente toEntityModel() {
		ExpedienteImpl expedienteImpl = new ExpedienteImpl();

		expedienteImpl.setIdExpediente(idExpediente);

		if (documento == null) {
			expedienteImpl.setDocumento(StringPool.BLANK);
		}
		else {
			expedienteImpl.setDocumento(documento);
		}

		if (estado == null) {
			expedienteImpl.setEstado(StringPool.BLANK);
		}
		else {
			expedienteImpl.setEstado(estado);
		}

		if (paso == null) {
			expedienteImpl.setPaso(StringPool.BLANK);
		}
		else {
			expedienteImpl.setPaso(paso);
		}

		if (pagina == null) {
			expedienteImpl.setPagina(StringPool.BLANK);
		}
		else {
			expedienteImpl.setPagina(pagina);
		}

		if (tipoFlujo == null) {
			expedienteImpl.setTipoFlujo(StringPool.BLANK);
		}
		else {
			expedienteImpl.setTipoFlujo(tipoFlujo);
		}

		if (flagR1 == null) {
			expedienteImpl.setFlagR1(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagR1(flagR1);
		}

		if (flagCampania == null) {
			expedienteImpl.setFlagCampania(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagCampania(flagCampania);
		}

		if (flagReniec == null) {
			expedienteImpl.setFlagReniec(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagReniec(flagReniec);
		}

		if (flagEquifax == null) {
			expedienteImpl.setFlagEquifax(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagEquifax(flagEquifax);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			expedienteImpl.setFechaRegistro(null);
		}
		else {
			expedienteImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (tipoCampania == null) {
			expedienteImpl.setTipoCampania(StringPool.BLANK);
		}
		else {
			expedienteImpl.setTipoCampania(tipoCampania);
		}

		if (flagCalificacion == null) {
			expedienteImpl.setFlagCalificacion(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagCalificacion(flagCalificacion);
		}

		if (flagCliente == null) {
			expedienteImpl.setFlagCliente(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagCliente(flagCliente);
		}

		expedienteImpl.setIdUsuarioSession(idUsuarioSession);

		if (strJson == null) {
			expedienteImpl.setStrJson(StringPool.BLANK);
		}
		else {
			expedienteImpl.setStrJson(strJson);
		}

		if (formHtml == null) {
			expedienteImpl.setFormHtml(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFormHtml(formHtml);
		}

		if (codigoHtml == null) {
			expedienteImpl.setCodigoHtml(StringPool.BLANK);
		}
		else {
			expedienteImpl.setCodigoHtml(codigoHtml);
		}

		if (periodoRegistro == null) {
			expedienteImpl.setPeriodoRegistro(StringPool.BLANK);
		}
		else {
			expedienteImpl.setPeriodoRegistro(periodoRegistro);
		}

		if (modificacion == Long.MIN_VALUE) {
			expedienteImpl.setModificacion(null);
		}
		else {
			expedienteImpl.setModificacion(new Date(modificacion));
		}

		if (flagTipificacion == null) {
			expedienteImpl.setFlagTipificacion(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagTipificacion(flagTipificacion);
		}

		if (flagActualizarRespuesta == null) {
			expedienteImpl.setFlagActualizarRespuesta(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagActualizarRespuesta(flagActualizarRespuesta);
		}

		if (flagObtenerInformacion == null) {
			expedienteImpl.setFlagObtenerInformacion(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagObtenerInformacion(flagObtenerInformacion);
		}

		if (flagConsultarAfiliacion == null) {
			expedienteImpl.setFlagConsultarAfiliacion(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagConsultarAfiliacion(flagConsultarAfiliacion);
		}

		if (flagGeneracionToken == null) {
			expedienteImpl.setFlagGeneracionToken(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagGeneracionToken(flagGeneracionToken);
		}

		if (flagVentaTC == null) {
			expedienteImpl.setFlagVentaTC(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagVentaTC(flagVentaTC);
		}

		if (flagValidacionToken == null) {
			expedienteImpl.setFlagValidacionToken(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagValidacionToken(flagValidacionToken);
		}

		if (flagRecompraGenPOTC == null) {
			expedienteImpl.setFlagRecompraGenPOTC(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagRecompraGenPOTC(flagRecompraGenPOTC);
		}

		if (flagRecompraConPOTC == null) {
			expedienteImpl.setFlagRecompraConPOTC(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagRecompraConPOTC(flagRecompraConPOTC);
		}

		if (flagExtornoGenPOTC == null) {
			expedienteImpl.setFlagExtornoGenPOTC(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagExtornoGenPOTC(flagExtornoGenPOTC);
		}

		if (flagExtornoConPOTC == null) {
			expedienteImpl.setFlagExtornoConPOTC(StringPool.BLANK);
		}
		else {
			expedienteImpl.setFlagExtornoConPOTC(flagExtornoConPOTC);
		}

		if (ip == null) {
			expedienteImpl.setIp(StringPool.BLANK);
		}
		else {
			expedienteImpl.setIp(ip);
		}

		if (navegador == null) {
			expedienteImpl.setNavegador(StringPool.BLANK);
		}
		else {
			expedienteImpl.setNavegador(navegador);
		}

		if (pepperType == null) {
			expedienteImpl.setPepperType(StringPool.BLANK);
		}
		else {
			expedienteImpl.setPepperType(pepperType);
		}

		expedienteImpl.resetOriginalValues();

		return expedienteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idExpediente = objectInput.readLong();
		documento = objectInput.readUTF();
		estado = objectInput.readUTF();
		paso = objectInput.readUTF();
		pagina = objectInput.readUTF();
		tipoFlujo = objectInput.readUTF();
		flagR1 = objectInput.readUTF();
		flagCampania = objectInput.readUTF();
		flagReniec = objectInput.readUTF();
		flagEquifax = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
		tipoCampania = objectInput.readUTF();
		flagCalificacion = objectInput.readUTF();
		flagCliente = objectInput.readUTF();
		idUsuarioSession = objectInput.readInt();
		strJson = objectInput.readUTF();
		formHtml = objectInput.readUTF();
		codigoHtml = objectInput.readUTF();
		periodoRegistro = objectInput.readUTF();
		modificacion = objectInput.readLong();
		flagTipificacion = objectInput.readUTF();
		flagActualizarRespuesta = objectInput.readUTF();
		flagObtenerInformacion = objectInput.readUTF();
		flagConsultarAfiliacion = objectInput.readUTF();
		flagGeneracionToken = objectInput.readUTF();
		flagVentaTC = objectInput.readUTF();
		flagValidacionToken = objectInput.readUTF();
		flagRecompraGenPOTC = objectInput.readUTF();
		flagRecompraConPOTC = objectInput.readUTF();
		flagExtornoGenPOTC = objectInput.readUTF();
		flagExtornoConPOTC = objectInput.readUTF();
		ip = objectInput.readUTF();
		navegador = objectInput.readUTF();
		pepperType = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idExpediente);

		if (documento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(documento);
		}

		if (estado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estado);
		}

		if (paso == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paso);
		}

		if (pagina == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pagina);
		}

		if (tipoFlujo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoFlujo);
		}

		if (flagR1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagR1);
		}

		if (flagCampania == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagCampania);
		}

		if (flagReniec == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagReniec);
		}

		if (flagEquifax == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagEquifax);
		}

		objectOutput.writeLong(fechaRegistro);

		if (tipoCampania == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoCampania);
		}

		if (flagCalificacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagCalificacion);
		}

		if (flagCliente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagCliente);
		}

		objectOutput.writeInt(idUsuarioSession);

		if (strJson == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(strJson);
		}

		if (formHtml == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(formHtml);
		}

		if (codigoHtml == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoHtml);
		}

		if (periodoRegistro == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(periodoRegistro);
		}

		objectOutput.writeLong(modificacion);

		if (flagTipificacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagTipificacion);
		}

		if (flagActualizarRespuesta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagActualizarRespuesta);
		}

		if (flagObtenerInformacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagObtenerInformacion);
		}

		if (flagConsultarAfiliacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagConsultarAfiliacion);
		}

		if (flagGeneracionToken == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagGeneracionToken);
		}

		if (flagVentaTC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagVentaTC);
		}

		if (flagValidacionToken == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagValidacionToken);
		}

		if (flagRecompraGenPOTC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagRecompraGenPOTC);
		}

		if (flagRecompraConPOTC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagRecompraConPOTC);
		}

		if (flagExtornoGenPOTC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagExtornoGenPOTC);
		}

		if (flagExtornoConPOTC == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagExtornoConPOTC);
		}

		if (ip == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ip);
		}

		if (navegador == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(navegador);
		}

		if (pepperType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pepperType);
		}
	}

	public long idExpediente;
	public String documento;
	public String estado;
	public String paso;
	public String pagina;
	public String tipoFlujo;
	public String flagR1;
	public String flagCampania;
	public String flagReniec;
	public String flagEquifax;
	public long fechaRegistro;
	public String tipoCampania;
	public String flagCalificacion;
	public String flagCliente;
	public int idUsuarioSession;
	public String strJson;
	public String formHtml;
	public String codigoHtml;
	public String periodoRegistro;
	public long modificacion;
	public String flagTipificacion;
	public String flagActualizarRespuesta;
	public String flagObtenerInformacion;
	public String flagConsultarAfiliacion;
	public String flagGeneracionToken;
	public String flagVentaTC;
	public String flagValidacionToken;
	public String flagRecompraGenPOTC;
	public String flagRecompraConPOTC;
	public String flagExtornoGenPOTC;
	public String flagExtornoConPOTC;
	public String ip;
	public String navegador;
	public String pepperType;
}