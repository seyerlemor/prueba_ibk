/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;
import java.util.List;

import pe.com.ibk.pepper.NoSuchClienteTransaccionException;
import pe.com.ibk.pepper.model.ClienteTransaccion;
import pe.com.ibk.pepper.service.base.ClienteTransaccionLocalServiceBaseImpl;

/**
 * The implementation of the cliente transaccion local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.ClienteTransaccionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.ClienteTransaccionLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.ClienteTransaccionLocalServiceUtil
 */
public class ClienteTransaccionLocalServiceImpl
	extends ClienteTransaccionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.ClienteTransaccionLocalServiceUtil} to access the cliente transaccion local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(ClienteTransaccionLocalServiceImpl.class);
	
	@Override
	public ClienteTransaccion registrarClienteTransaccion(ClienteTransaccion clienteTransaccion) throws SystemException{
		
		Date now = DateUtil.newDate();
		if (clienteTransaccion.isNew()) {
			clienteTransaccion.setFechaRegistro(now);
		}
		clienteTransaccionPersistence.update(clienteTransaccion);
		return clienteTransaccion;
	}		

	@Override
	public ClienteTransaccion buscarClienteTransaccionporId(long idClienteTransaccion){
		ClienteTransaccion clienteTransaccion = null;
		try {
			clienteTransaccion = clienteTransaccionPersistence.findByCT_ID(idClienteTransaccion);
		} catch (NoSuchClienteTransaccionException e) {
			_log.error("Error en clase CLienteTransaccionLocalServiceImpl metodo buscarClienteTransaccionporId:: NO EXISTE EL CLIENTETRANSACCION ", e );
		} catch (SystemException e) {
			_log.error("Error en clase ClienteTransaccionLocalServiceImpl metodo buscarClianteTransaccionporId:: ", e );
		}
		return clienteTransaccion;
	}
	
	public List<ClienteTransaccion> getClienteTransaccionesByExpediente(long idExpediente, String tipoFlujo) throws SystemException{
		return clienteTransaccionPersistence.findByIdExpediente(idExpediente, tipoFlujo);
	}
	
	public List<ClienteTransaccion> obtenerTransacciones(long idExpediente, String tipoFlujo) {
		return clienteTransaccionFinder.obtenerTransacciones(idExpediente, tipoFlujo);
	}
}