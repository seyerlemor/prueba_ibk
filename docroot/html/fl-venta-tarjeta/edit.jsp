<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page  import="com.liferay.portal.kernel.util.Constants"%>
<%@ page  import="com.liferay.portal.kernel.util.UnicodeFormatter"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<!-- JSP UTILIZADOS inicio, resultado-tc, pregunta-ex, otp-options, otp-email, otp-sms, otp-validacion-codigo, calificacion, configuracion, resumen -->

<liferay-portlet:actionURL portletConfiguration="true" var="editURL" />

<aui:form action="${editURL}" method="post" name="fm" >
<aui:input name="<%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />
<liferay-ui:tabs names="Inicio,Oferta Multiple,OTP,Equifax,Calificación,Configuración,Resumen, Recompra/Extorno, Configuración General, Configuración Urls" refresh="false" tabsValues="Inicio,Oferta Multiple,OTP,Equifax,Calificación,Configuración,Resumen, Recompra/Extorno, Configuración General, Configuración Urls">

	 <!--==-- Inicio --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para la página Inicio</H3>
			  <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioTitleText--" helpMessage="Ingresar el titulo de la página de inicio"
					value="${renderRequest.preferences.getValue('inicioTitleText','Empieza tu solicitud')}"
					label="Título" style="width:80%;"/>
				  </aui:col>
			   	 <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioDniNumberText--" helpMessage="Ingresar el texto para identificar DNI"
					value="${renderRequest.preferences.getValue('inicioDniNumberText','Número de DNI')}"
					label="DNI" style="width:80%;"/>
				  </aui:col>
				</aui:row>   
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioDniNumberRequiredMessage--" helpMessage="DNI requerido"
					value="${renderRequest.preferences.getValue('inicioDniNumberRequiredMessage','Ingrese su número de documento')}"
					label="DNI requerido" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioDniNumberLengthMessage--" helpMessage="DNI longitud requerida"
					value="${renderRequest.preferences.getValue('inicioDniNumberLengthMessage','El DNI debe contar con 8 caracteres')}"
					label="DNI longitud requerida" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioEmailAddressText--" helpMessage="Ingresar el texto para identificar el email"
					value="${renderRequest.preferences.getValue('inicioEmailAddressText','Email')}"
					label="Email" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioEmailAddressRequiredMessage--" helpMessage="Email requerido"
					value="${renderRequest.preferences.getValue('inicioEmailAddressRequiredMessage','Ingrese su email')}"
					label="Email requerido" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioEmailAddressValidMessage--" helpMessage="Email válido"
					value="${renderRequest.preferences.getValue('inicioEmailAddressValidMessage','Ingrese un email válido')}"
					label="Email válido" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioOperatorText--" helpMessage="Ingresar el texto para identificar el operador telefónico"
					value="${renderRequest.preferences.getValue('inicioOperatorText','Operador')}"
					label="Ingresar el texto para el operador telefónico" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioOperatorRequiredMessage--" helpMessage="Operador requerido"
					value="${renderRequest.preferences.getValue('inicioOperatorRequiredMessage','Seleccione un operador telef&oacute;nico')}"
					label="Operador requerido" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioCelularText--" helpMessage="Ingresar el texto para identificar el celular"
					value="${renderRequest.preferences.getValue('inicioCelularText','Celular')}"
					label="Ingresar el texto para el celular" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioCelularRequiredMessage--" helpMessage="Celular requerido"
					value="${renderRequest.preferences.getValue('inicioCelularRequiredMessage','Ingrese su número de celular')}"
					label="Celular requerido" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioCelularValidMessage--" helpMessage="Celular válido"
					value="${renderRequest.preferences.getValue('inicioCelularValidMessage','El celular ingresado no es válido')}"
					label="Celular válido" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioCelularLengthMessage--" helpMessage="Celular longitud requerida"
					value="${renderRequest.preferences.getValue('inicioCelularLengthMessage','El celular debe contar con 9 caracteres')}"
					label="Celular longitud requerida" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioRecaptchaSiteKey--" helpMessage="Ingresar siteKey Recaptcha"
					value="${renderRequest.preferences.getValue('inicioRecaptchaSiteKey','6LcJ-scSAAAAAPsTxaM-L8qEuTTOtUdwmvG7u2IY')}"
					label="SiteKey Recaptcha" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioRecaptchaRequiredMessage--" helpMessage="Recatpcha requerido"
					value="${renderRequest.preferences.getValue('inicioRecaptchaRequiredMessage','Debe marcar la casilla')}"
					label="Recatpcha requerido" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="textarea" name="preferences--inicioProteccionDatosText--" helpMessage="Ingresar texto Protección de Datos"
					value="${renderRequest.preferences.getValue('inicioProteccionDatosText','<span>He leído y acepto la <a class=\"lh-btn--link\" href=\"#\" data-lightbox=\"#fpa_lbLPDP\">Política de tratamiento de Protección de datos personales. </a></span>')}"
					label="Ingresar el texto para Protección de Datos" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioBtnSubmit--" helpMessage="Ingresar texto para identificar boton de inicio"
					value="${renderRequest.preferences.getValue('inicioBtnSubmit','Comencemos')}"
					label="Ingresar texto para boton de inicio" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="textarea" name="preferences--inicioFooterText--" helpMessage="Ingresar texto parte inferior de formulario"
					value="${renderRequest.preferences.getValue('inicioFooterText','')}"
					label="Texto parte inferior de formulario" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
				  <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioLightBoxTitle--" helpMessage="Ingresar título de LightBox"
					value="${renderRequest.preferences.getValue('inicioLightBoxTitle','Tratamiento de datos personales')}"
					label="título de LightBox" style="width:80%;"/>
				  </aui:col>
				  <aui:col width="45">
				     <aui:input type="textarea" name="preferences--inicioLightBoxText--" helpMessage="Ingresar texto LightBox"
					value="${renderRequest.preferences.getValue('inicioLightBoxText','
					<ol>
					<li><span>Se informa que los datos personales proporcionados a Interbank 
								quedan incorporados al banco de datos de clientes de Interbank 
								y que Interbank utilizará dicha información para efectos de la 
								gestión de los productos y/o servicios solicitados y/o contratados 
								(incluyendo evaluaciones financieras, procesamiento de datos, 
								formalizaciones contractuales, cobro de deudas, gestión de operaciones 
								financieras y remisión de correspondencia, entre otros), la misma que 
								podrá ser realizada a través de terceros.</span></li>
					<li><span>Interbank protege estos bancos de datos y sus tratamientos con todas las 
								medidas de índole técnica y organizativa necesarias para garantizar su 
								seguridad y evitar la alteración, pérdida, tratamiento o acceso no 
								autorizado.</span></li>
					<li><span>Mediante la suscripción del presente documento, usted:</span>
						<ol>
							<li><span>Autoriza a Interbank a utilizar sus datos personales 
										(incluyendo datos sensibles) proporcionados a Interbank, aquellos 
										que pudieran encontrarse en fuentes accesibles para el público o 
										los que hayan sido válidamente obtenidos de terceros; para tratamientos 
										que supongan desarrollo de acciones comerciales, realización de estudios 
										de mercado, elaboración de perfiles de compra y evaluaciones financieras, 
										la remisión, directa o por intermedio de terceros (vía medio físico, 
										electrónico o telefónico) de publicidad, información, obsequios, ofertas 
										y/o promociones (personalizadas o generales) de productos y/o servicios de 
										Interbank y/o de otras empresas del Grupo Intercorp y sus socios estratégicos, 
										entre las que se encuentran aquellas difundidas en el portal de la 
										Superintendencia del Mercado de Valores (www.smv.gob.pe), así como en el portal 
										www.intercorp.com.pe/es. Para tales efectos, el titular de los datos personales 
										autoriza a Interbank la cesión, transferencia y/o comunicación de sus datos personales, 
										a dichas empresas y entre ellas*.</span></li>
							<li><span>Declara conocer que la suscripción de la presente autorización es de carácter 
										libre y voluntaria y que, por tanto, no condiciona el otorgamiento y/o gestión 
										de ninguno de los productos o servicios financieros ofrecidos por Interbank.</span></li>
						</ol></li>
					<li><span>Interbank le informa que usted puede revocar la autorización a la que se refiere el 
								numeral 3 anterior en cualquier momento, así como ejercer los derechos de acceso, 
								rectificación, cancelación y oposición para el tratamiento de sus datos personales. 
								Para ejercer estos derechos, o cualquier otro previsto en las normas referidas a la 
								protección de sus datos personales, usted deberá presentar su solicitud en cualquiera 
								de las tiendas de Interbank.</span></li>
				</ol>
				<p>Algunas de las principales marcas, cuya titularidad pertenece a estas empresas son: Plaza Vea, Vivanda, 
					Mass, San Jorge, Economax, Oechsle, Tu Entrada, Promart, Cineplanet, Casa Andina, Papa John’s, Bembos, 
					China Wok, Dunkin Donuts, Popeye’s, Don Belisario, UTP, Innova Schools, IDAT, UCIK, IPAE, Inkafarma, 
					Salud Total, Sparza Club, Interfondos, Interseguro, entre otras. Esta lista es enunciativa, más no limitativa.</p>
					')}"
					label="Texto LightBox" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
			   <aui:row>
			     <aui:col width="45">
				     <aui:input type="text" name="preferences--inicioCommerceDefault--" helpMessage="Ingresar codigo de comercio por default"
					value="${renderRequest.preferences.getValue('inicioCommerceDefault','6b6a3ab3800a2876b8dd6a4742f00d6c442526ea2ff1af2f837059d335329cde')}"
					label="Ingresar codigo de comercio por default" style="width:80%;"/>
				  </aui:col>
			   </aui:row>
		  </aui:col>
		  <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- Oferta Múltiple --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para la página Oferta Múltiple(ResultadoTC)</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resultadotcIdContentLeft--" 
					value="${renderRequest.preferences.getValue('resultadotcIdContentLeft','80304')}"
					label="Id Contenido Izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resultadotcPartSubTitle001--" 
					value="${renderRequest.preferences.getValue('resultadotcPartSubTitle001','Tienes una tarjeta de crédito preaprobada.')}"
					label="Contenido de título parte izquierda" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resultadotcPartSubTitle002--" 
					value="${renderRequest.preferences.getValue('resultadotcPartSubTitle002','¡')}"
					label="Contenido de subtítulo parte izquierda" style="width:80%;"/>
			   </aui:col>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resultadotcPartSubTitle003--" 
					value="${renderRequest.preferences.getValue('resultadotcPartSubTitle003','escoge una tarjeta de crédito que vaya con tu estilo!')}"
					label="Contenido de subtítulo parte derecha" style="width:80%;"/>
			   </aui:col> 
			</aui:row> 
			<aui:row>
				<aui:col width="45">
				    <aui:input type="text" name="preferences--resultadotcBtnSubmit--" 
					value="${renderRequest.preferences.getValue('resultadotcBtnSubmit','¡Quiero esta tarjeta!')}"
					label="Texto del Botón" style="width:80%;"/>
			   </aui:col>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resultadotcDetailTCText--" 
					value="${renderRequest.preferences.getValue('resultadotcDetailTCText','[{&quot;idBrand&quot;:&quot;[$INDICE$]&quot;,&quot;title&quot;:&quot;Beneficios&quot;,&quot;detail&quot;:&quot;&lt;&sol;br&gt;&lt;strong&gt;Cuponera de descuentos&lt;&sol;strong&gt;&lt;&sol;br&gt;En cines, restaurantes y más.&lt;&sol;br&gt;&lt;&sol;br&gt;&lt;strong&gt;Hasta S/100 de devolución mensual&lt;&sol;strong&gt;&lt;&sol;br&gt;En supermercados.&quot;},{&quot;title&quot;:&quot;Principales seguros&quot;,&quot;detail&quot;:&quot;Compra protegida y garantía extendida para los productos que compres con la tarjeta.&quot;},{&quot;title&quot;:&quot;Membresía&quot;,&quot;detail&quot;:&quot;S/[$MEMBERSHIP$]&lt;&sol;br&gt;&lt;strong&gt;*Gratis&lt;&sol;strong&gt; si consumes todos los meses.&quot;},{&quot;title&quot;:&quot;Seguro de desgravamen&quot;,&quot;detail&quot;:&quot;S/[$SAFEDEDUCTION$]&lt;&sol;br&gt;Evita que tus familiares hereden la deuda de tu tarjeta.&quot;},{&quot;title&quot;:&quot;Tasa Efectiva Anual(TEA)&quot;,&quot;detail&quot;:&quot;[$TEA$]%&quot;}]')}"
					label="Contenido de detalle de tc" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		  </aui:col>
		  <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- OTP --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para la página OTP</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpIdContentLeft--" 
					value="${renderRequest.preferences.getValue('otpIdContentLeft','80314')}"
					label="Id Contenido Izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpPartTitle--" 
					value="${renderRequest.preferences.getValue('otpPartTitle','Hola')}"
					label="Contenido de título parte izquierda" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				     <aui:input type="text" name="preferences--otpOperatorRequiredMessage--" helpMessage="Operador requerido"
					value="${renderRequest.preferences.getValue('otpOperatorRequiredMessage','Selecciona un operador')}"
					label="Texto Operador requerido" style="width:80%;"/>
				  </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpBtnSigamos--" 
					value="${renderRequest.preferences.getValue('otpBtnSigamos','Sigamos')}"
					label="Texto del Botón" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpValidateCodeRequiredMessage--" 
					value="${renderRequest.preferences.getValue('otpValidateCodeRequiredMessage','Ingresa el código')}"
					label="Campo validateCode requerido" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpValidateCodeLengthMessage--" 
					value="${renderRequest.preferences.getValue('otpValidateCodeLengthMessage','El código debe contar con 8 dígitos')}"
					label="Campo validateCode longitud requerida" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpText001--" 
					value="${renderRequest.preferences.getValue('otpText001','Te hemos enviado un código de verificación al correo electrónico ')}"
					label="Contenido de subtítulo parte izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpText002--" 
					value="${renderRequest.preferences.getValue('otpText002','Te hemos enviado un código de verificación por sms al celular ')}"
					label="Contenido de subtítulo inferior" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpText003--" 
					value="${renderRequest.preferences.getValue('otpText003','¡Hay una Tarjeta Interbank esperando por ti!')}"
					label="Contenido de subtítulo parte izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpOptionsAlertText--" 
					value="${renderRequest.preferences.getValue('otpOptionsAlertText','Recuerda: No compartas información confidencial con otras personas, el banco nunca te va a llamar para solicitar este c&oacute;digo.')}"
					label="Contenido Alerta" style="width:80%;"/>
			   </aui:col>
			</aui:row> 
			<H3>OTP Options</H3>
			<aui:row>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpOptionsPartSubTitle002--" 
					value="${renderRequest.preferences.getValue('otpOptionsPartSubTitle002','Solo tienes que tener a la mano el código que enviaremos y seguir los pasos que aparecerán en pantalla. Elige cómo quieres recibir tu código de verificación:')}"
					label="Contenido de subtítulo inferior" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpOptionsText001--" 
					value="${renderRequest.preferences.getValue('otpOptionsText001','Enviar a ')}"
					label="Texto 01" style="width:80%;"/>
			   </aui:col>
			</aui:row> 
			<aui:row>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpOptionsText002--" 
					value="${renderRequest.preferences.getValue('otpOptionsText002','Enviar SMS a ')}"
					label="Texto 02" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpOptionsText003--" 
					value="${renderRequest.preferences.getValue('otpOptionsText003','Enviar a Ambos')}"
					label="Texto 03" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<H3>OTP SMS</H3>
			<aui:row>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpSmsPartSubTitle002--" 
					value="${renderRequest.preferences.getValue('otpSmsPartSubTitle002','Solo tienes que tener a la mano el código que enviaremos y seguir los pasos que aparecerán en pantalla. Selecciona el operador para tu número ')}"
					label="Contenido de subtítulo inferior" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<H3>OTP CONFIGURACION TOKEN</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenBtnListo--" 
					value="${renderRequest.preferences.getValue('otpTokenBtnListo','Listo')}"
					label="Contenido de subtítulo parte izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenLimiteEnvioText--" 
					value="${renderRequest.preferences.getValue('otpTokenLimiteEnvioText','Llegaste al límite de envíos por hoy.')}"
					label="Contenido de subtítulo inferior" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenText001--" 
					value="${renderRequest.preferences.getValue('otpTokenText001','¿No has recibido el código?')}"
					label="Contenido de subtítulo parte izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenLimiteEnvioNum--" 
					value="${renderRequest.preferences.getValue('otpTokenLimiteEnvioNum','3')}"
					label="Contenido de subtítulo inferior" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenText002--" 
					value="${renderRequest.preferences.getValue('otpTokenText002','Reenviar código')}"
					label="Contenido de subtítulo parte izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenText003--" 
					value="${renderRequest.preferences.getValue('otpTokenText003','Identifícate de otra manera')}"
					label="Contenido de subtítulo inferior" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenCounterNum--" 
					value="${renderRequest.preferences.getValue('otpTokenCounterNum','10')}"
					label="Tiempo de espera envio token" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenCounterText--" 
					value="${renderRequest.preferences.getValue('otpTokenCounterText','00:10')}"
					label="Texto Tiempo de espera envio token" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpTokenText004--" 
					value="${renderRequest.preferences.getValue('otpTokenText004','Solicita uno nuevo en ')}"
					label="Texto 004" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<H3>OTP REDIRECCION EQUIFAX</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpRedirectText001--" 
					value="${renderRequest.preferences.getValue('otpRedirectText001','¿No son tus datos?')}"
					label="Contenido de texto" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--otpRedirectText002--" 
					value="${renderRequest.preferences.getValue('otpRedirectText002','¡También puedes hacerlo así! Puedes continuar con la solicitud respondiendo a unas preguntas para confirmar que eres tú.')}"
					label="Contenido de texto" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--otpRedirectBtn--" 
					value="${renderRequest.preferences.getValue('otpRedirectBtn','Mejor de esta forma')}"
					label="Contenido de boton" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		  </aui:col>
		  <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- Paso 2 --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para la página Equifax</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--equifaxIdContentLeft--" 
					value="${renderRequest.preferences.getValue('equifaxIdContentLeft','80324')}"
					label="Id Contenido Izquierda" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--equifaxPartTitle--" 
					value="${renderRequest.preferences.getValue('equifaxPartTitle','Hola')}"
					label="Contenido de título parte izquierda" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--equifaxText001--" 
					value="${renderRequest.preferences.getValue('equifaxText001','DNI: ')}"
					label="Paso 2 - Texto boton enviar fotos" style="width:80%;"/>
			   </aui:col>	
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--equifaxText002--" 
					value="${renderRequest.preferences.getValue('equifaxText002','No es mi DNI')}"
					label="Paso 2 - Texto boton otra foto" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--equifaxText003--" 
					value="${renderRequest.preferences.getValue('equifaxText003','Antes de continuar con el proceso, necesitamos algunos datos. por favor responde las siguientes preguntas:')}"
					label="Paso 2 - Texto boton otra foto" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--equifaxBtn--" 
					value="${renderRequest.preferences.getValue('equifaxBtn','Listo')}"
					label="Paso 2 - Mensaje reintento" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		  </aui:col>	
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- Paso 3 --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para la página Calificación</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionIdContentLeft--" 
					value="${renderRequest.preferences.getValue('calificacionIdContentLeft','80334')}"
					label="Título del formulario" style="width:80%;"/>
			   </aui:col>	
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionPartTitle--" 
					value="${renderRequest.preferences.getValue('calificacionPartTitle','Usa tu Tarjeta Interbank al instante')}"
					label="Paso 3 - Título Datos Personales" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--calificacionText001--" 
					value="${renderRequest.preferences.getValue('calificacionText001','
					<ul class=\"lh-typo__list\">
										<li>Al terminar tu solicitud, podrás comprar tus
											entradas.</li>
										<li>No necesitas tener una tarjeta física. te daremos los
											datos.</li>
										<li>Obtén un descuentodel 50 % en Teleticket, Valido solo
											hoy.</li>
									</ul>
					')}"
					label="Texto Inferior" style="width:80%;"/>
			   </aui:col>	
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionDatosDomicilio--" 
					value="${renderRequest.preferences.getValue('calificacionDatosDomicilio','Datos de domicilio')}"
					label="Título Datos de Domicilio" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionText002--" 
					value="${renderRequest.preferences.getValue('calificacionText002','Verifica o modifica la dirección a la de tu residencia actual.')}"
					label="Texto verifica" style="width:80%;"/>
			   </aui:col>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionDatosLaborales--" 
					value="${renderRequest.preferences.getValue('calificacionDatosLaborales','Datos laborales')}"
					label="Título Datos Laborales" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row> 
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionDatosConyuge--" 
					value="${renderRequest.preferences.getValue('calificacionDatosConyuge','Datos del cónyugue')}"
					label="Título Datos del cónyugue" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionText003--" 
					value="${renderRequest.preferences.getValue('calificacionText003','Conoce las opciones')}"
					label="Texto Conoce las opciones" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>  
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionBtnEnviar--" 
					value="${renderRequest.preferences.getValue('calificacionBtnEnviar','Listo')}"
					label="Paso 3 - Texto boton Enviar" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--calificacionSituacionLaboral--" 
					value="${renderRequest.preferences.getValue('calificacionSituacionLaboral','Situación Laboral')}"
					label="Títulosituación laboral" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>  
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--calificacionText004--" 
					value="${renderRequest.preferences.getValue('calificacionText004','
					<div class=\"lq-container\">
          <div class=\"lh-lightbox__container\">
            <button class=\"lh-lightbox__close\" type=\"button\" aria-label=\"Close\"><i class=\"icon lh-icon-close\"></i></button>
            <div class=\"lh-lightbox__header\">
              <h3 class=\"lh-lightbox__title\">Situación Laboral</h3>
            </div>
            <div class=\"lh-lightbox__content lh-highlight--extended\">
              <div class=\"lh-highlight__item\">
                <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Rentista (1° y 2° Categoría)</h4>
                <div class=\"lh-highlight__content\">
                  <p class=\"lh-typo__p3\">Categoría: Rentas generadas por el alquiler, sub arrendamiento.</p>
                </div>
              </div>
              <div class=\"lh-highlight__item\">
                <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Microempresario (3° Categoría)</h4>
                <div class=\"lh-highlight__content\">
                  <p class=\"lh-typo__p3\">Categoría: Rentas generadas por el alquiler, sub arrendamiento.</p>
                </div>
              </div>
              <div class=\"lh-highlight__item\">
                <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Accionista (3° Categoría)</h4>
                <div class=\"lh-highlight__content\">
                  <p class=\"lh-typo__p3\">Persona que posee acciones en una empresa. Su responsabilidad y sus derechos están limitados al número de acciones que posea.</p>
                </div>
              </div>
              <div class=\"lh-highlight__item\">
                <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Profesional Independiente (4° Categoría)</h4>
                <div class=\"lh-highlight__content\">
                  <p class=\"lh-typo__p3\">Profesionales que desarrollan individualmente una profesión, y que son capaces de generar ingresos. Como consecuencia de la prestación de sus servicios.</p>
                </div>
              </div>
              <div class=\"lh-highlight__item\">
                <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Dependiente (5° Categoría)</h4>
                <div class=\"lh-highlight__content\">
                  <p class=\"lh-typo__p3\">Trabajador que tiene una relacion de dependencia con una empresa, mediante contratos determinados o indeterminados.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
					')}"
					label="Paso 3 - Texto LightBox" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		  </aui:col>
		  <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- Paso 5 --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para la página Configuración</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionIdContentLeft--" 
					value="${renderRequest.preferences.getValue('configuracionIdContentLeft','80344')}"
					label="Título del formulario" style="width:80%;"/>
			   </aui:col>	
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionPartTitle01--" 
					value="${renderRequest.preferences.getValue('configuracionPartTitle01','!tu tarjeta')}"
					label="Titulo Parte 1" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionPartTitle02--" 
					value="${renderRequest.preferences.getValue('configuracionPartTitle02',' ha sido aprobada!')}"
					label="Titulo Parte 2" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionText001--" 
					value="${renderRequest.preferences.getValue('configuracionText001','Ahora, escoge tu línea de crédito')}"
					label="Texto 01" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionDatosEntrega001--" 
					value="${renderRequest.preferences.getValue('configuracionDatosEntrega001','Datos de tu entrega')}"
					label="Texto Datos Entrega 01" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionText002--" 
					value="${renderRequest.preferences.getValue('configuracionText002','Elige la direcci&oacute;n de entrega')}"
					label="Texto 02" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionDatosEntrega002--" 
					value="${renderRequest.preferences.getValue('configuracionDatosEntrega002','Otra')}"
					label="Texto Datos Entrega 02" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionText003--" 
					value="${renderRequest.preferences.getValue('configuracionText003','E-mail para el envío de estado de cuenta')}"
					label="Texto 03" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--configuracionText004--" 
					value="${renderRequest.preferences.getValue('configuracionText004','Si deseas el envío de estado de cuenta a tu domicilio, puedes realizar el cambio a través de banca telefónica.')}"
					label="Texto 04" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--configuracionTitleTerminosCondiciones--" 
					value="${renderRequest.preferences.getValue('configuracionTitleTerminosCondiciones','
					Al darle click a \"Solicitar mi tarjeta\" declaro que he leído y acepto los 
					<a class=\"lh-link lh-link--underline lh-ff__link lh-link--bold\" href=\"#\" data-lightbox=\"#lb-terms-conditions-lf\">Términos y Condiciones</a> 
					de la Tarjeta de crédito, seguro de desgravamen y contrato de la tarjeta de crédito.
					')}"
					label="Texto 04" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--configuracionBtn001--" 
					value="${renderRequest.preferences.getValue('configuracionBtn001','Solicitar mi tarjeta')}"
					label="Botón 01" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--configuracionLightBox001--" 
					value="${renderRequest.preferences.getValue('configuracionLightBox001','
						<div class=\"lq-container\">
	                <div class=\"lh-lightbox__container\">
	                  <button class=\"lh-lightbox__close\" type=\"button\" aria-label=\"Close\"><i class=\"icon lh-icon-close\"></i></button>
	                  <div class=\"lh-lightbox__header\">
	                    <h3 class=\"lh-lightbox__title\">Términos y Condiciones</h3>
	                  </div>
	                  <div class=\"lh-lightbox__content lh-fb lh-highlight--extended\">
	                    <div class=\"lh-highlight__wrap\">
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Legalmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>1. El Cliente podrá adquirir y/o contratar diferentes productos y servicios financieros a través de la banca por internet de Interbank o la aplicación móvil de Interbank (Canales Digitales).</p>
	                        </div>
	                      </div>
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Familiarmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>1. Puedes contratar diferentes productos a través de la Banca por Internet o la aplicación de Interbank.</p>
	                        </div>
	                      </div>
	                    </div>
	                    <div class=\"lh-highlight__wrap\">
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Legalmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>2. El Cliente manifiesta su aceptación a través de un clic, la clave secreta, clave dinámica y/o cualquier otro medio de autenticación que complemente y/o reemplace dichas claves, así como de mecanismos que impliquen la manifestación de voluntad de aceptación del Cliente. La manifestación de voluntad de El Cliente puede realizarse a través de la firma manuscrita y/o electrónica, así como aquella brindada por medios electrónicos y/o virtuales como por ejemplo: ingreso de claves o contraseñas, clic, voz, datos biométricos (huella dactilar, identificación facial, etc.), entre otros.</p>
	                        </div>
	                      </div>
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Familiarmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>2. Puedes contratar diferentes productos a través de la Banca por Internet o la aplicación de Interbank.</p>
	                        </div>
	                      </div>
	                    </div>
	                    <div class=\"lh-highlight__wrap\">
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Legalmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>3. El Cliente podrá adquirir y/o contratar diferentes productos y servicios financieros a través de la banca por internet de Interbank o la aplicación móvil de Interbank (Canales Digitales).</p>
	                        </div>
	                      </div>
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Familiarmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>3. Puedes contratar diferentes productos a través de la Banca por Internet o la aplicación de Interbank.</p>
	                        </div>
	                      </div>
	                    </div>
	                    <div class=\"lh-highlight__wrap\">
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Legalmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>4. El Cliente manifiesta su aceptación a través de un clic, la clave secreta, clave dinámica y/o cualquier otro medio de autenticación que complemente y/o reemplace dichas claves, así como de mecanismos que impliquen la manifestación de voluntad de aceptación del Cliente. La manifestación de voluntad de El Cliente puede realizarse a través de la firma manuscrita y/o electrónica, así como aquella brindada por medios electrónicos y/o virtuales como por ejemplo: ingreso de claves o contraseñas, clic, voz, datos biométricos (huella dactilar, identificación facial, etc.), entre otros.</p>
	                        </div>
	                      </div>
	                      <div class=\"lh-highlight__item\">
	                        <h4 class=\"lh-typo__commontitle lh-typo__commontitle--1 lh-highlight__title\">Familiarmente</h4>
	                        <div class=\"lh-highlight__content\">
	                          <p>4. Puedes contratar diferentes productos a través de la Banca por Internet o la aplicación de Interbank.</p>
	                        </div>
	                      </div>
	                    </div>
	                  </div>
	                </div>
					')}"
					label="Texto Light Box 0" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--configuracionLightBox002--" 
					value="${renderRequest.preferences.getValue('configuracionLightBox002','
						<div class=\"lq-container\">
				          <div class=\"lh-lightbox__container\">
				            <button class=\"lh-lightbox__close lh-lightbox__action-close\" type=\"button\" aria-label=\"Close\"><i class=\"icon lh-icon-close\"></i></button>
				            <div class=\"lh-lightbox__content\">
				              <h3 class=\"lh-typo__thirdtitle lh-typo__thirdtitle--2\">La distancia nos separa</h3>
				              <p class=\"lh-typo__p2\">¡Lo sentimos! No estás en la zona de reparto. En 3 días te contactaremos para coordinar la entrega de tu tarjeta. Después de ese tiempo, también puedes llamarnos al 415-1805, de lunes a viernes de 9 a.m. a 6 p.m. y sábados de 9 a.m. a 12 p.m. Mientras tanto, continuemos con tu solicitud.</p>
				              <div class=\"lh-lightbox__buttons-container lh-lightbox__buttons-container--small\">
				                <button class=\"lh-btn lh-btn--secondary lh-btn--block-xxs lh-lightbox__action-close\" type=\"button\">Cambiar dirección</button>
				                <button class=\"lh-btn lh-btn--primary lh-btn--block-xxs lh-lightbox__action-close\" type=\"button\" data-btn-confirm-address>Continuar</button>
				              </div>
				            </div>
				          </div>
				        </div>
					')}"
					label="Texto Light Box 02" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--configuracionRangeStepLine--" 
					value="${renderRequest.preferences.getValue('configuracionRangeStepLine','100')}"
					label="Rango de pasos de linea de Crédito" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="45">
				    <aui:input type="textarea" name="preferences--configuracionText005--" 
					value="${renderRequest.preferences.getValue('configuracionText005','Línea de Crédito')}"
					label="Texto 04" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		  </aui:col>
		  <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- Paso 6 --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			 <H3>Parámetros de configuración para la página Resumen</H3>
			 <aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenIdContentLeft--" 
					value="${renderRequest.preferences.getValue('resumenIdContentLeft','80354')}"
					label="Título del formulario" style="width:80%;"/>
			   </aui:col>	
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenPartTitle01--" 
					value="${renderRequest.preferences.getValue('resumenPartTitle01','¿Estás listo para hacer tu primera compra?')}"
					label="Titulo Parte 01" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		    <aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText001--" 
					value="${renderRequest.preferences.getValue('resumenText001','En 5 minutos, los datos de tu tarjeta temporal desaparecerán. ¡Anótalos! Con ellos podrás hacer tu compra y recuerda que puedes realizar tu primera compra para un monto de hasta S/ ')}"
					label="Texto 01" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenTimer--" 
					value="${renderRequest.preferences.getValue('resumenTimer','300')}"
					label="Texto Timer en segundos" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenTimerText--" 
					value="${renderRequest.preferences.getValue('resumenTimerText','05 : 00')}"
					label="Texto Timer" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText002--" 
					value="${renderRequest.preferences.getValue('resumenText002','Disfruta tu compra')}"
					label="Texto 02" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText003--" 
					value="${renderRequest.preferences.getValue('resumenText003','En los próximos días te haremos llegar tu nueva tarjeta. Recuerda que al recibirla te pediremos firma y huella digital.')}"
					label="Texto 03" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenBtn001--" 
					value="${renderRequest.preferences.getValue('resumenBtn001','Ver datos')}"
					label="Texto 04" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText004--" 
					value="${renderRequest.preferences.getValue('resumenText004','Copiado')}"
					label="Texto 04" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText005--" 
					value="${renderRequest.preferences.getValue('resumenText005','vence:')}"
					label="Texto 05" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText006--" 
					value="${renderRequest.preferences.getValue('resumenText006','Cod. Seguridad:')}"
					label="Texto 06" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText007--" 
					value="${renderRequest.preferences.getValue('resumenText007','Datos de tu tarjeta')}"
					label="Texto 07" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText008--" 
					value="${renderRequest.preferences.getValue('resumenText008','Tarjeta a recibir')}"
					label="Texto 08" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText009--" 
					value="${renderRequest.preferences.getValue('resumenText009','Línea de crédito')}"
					label="Texto 09" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText010--" 
					value="${renderRequest.preferences.getValue('resumenText010','Seguro de desgravamen')}"
					label="Texto 10" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText011--" 
					value="${renderRequest.preferences.getValue('resumenText011','Tasa Efectiva Anual (TEA)')}"
					label="Texto 11" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText012--" 
					value="${renderRequest.preferences.getValue('resumenText012','Membresía')}"
					label="Texto 12" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText013--" 
					value="${renderRequest.preferences.getValue('resumenText013','<strong>(Gratis si consumes todos los meses)</strong>')}"
					label="Texto 13" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText014--" 
					value="${renderRequest.preferences.getValue('resumenText014','Día de pago')}"
					label="Texto 14" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText015--" 
					value="${renderRequest.preferences.getValue('resumenText015',' de cada mes')}"
					label="Texto 15" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText016--" 
					value="${renderRequest.preferences.getValue('resumenText016','E-mail para envío de constancia')}"
					label="Texto 16" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText017--" 
					value="${renderRequest.preferences.getValue('resumenText017','
						<div class=\"lh-ff-section__message text-center\">
                          <h3 class=\"lh-typo__commontitle lh-typo__commontitle--2\">Sobre el envío de tu tarjeta</h3>
                          <p class=\"lh-typo__list\">En tres días te contactaremos para coordinar la entrega de tu Tarjeta de crédito.
                          <br/>
                          <strong>Recuerda que la entrega es personal</strong>
                          </p>
                        </div>
					')}"
					label="Texto 17" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText018--" 
					value="${renderRequest.preferences.getValue('resumenText018','')}"
					label="Texto 18" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText019--" 
					value="${renderRequest.preferences.getValue('resumenText019','')}"
					label="Texto 4CSC" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--resumenText020--" 
					value="${renderRequest.preferences.getValue('resumenText020','')}"
					label="Texto 3CSC" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText021--" 
					value="${renderRequest.preferences.getValue('resumenText021','')}"
					label="Texto Tooltip" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="textarea" name="preferences--resumenText022--" 
					value="${renderRequest.preferences.getValue('resumenText022','')}"
					label="Texto Final Pepper Fisico" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		   </aui:col>
		   <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			 <H3>Parámetros de Recompra / Extorno</H3>
			 <aui:row>
			  		<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt001--" helpMessage="Ingresar Texto Configurable 001"
						value="${renderRequest.preferences.getValue('recExt001','Recibirás un SMS para poder realizar tu pago.')}"
						label="Ingresar Texto Configurable 001" style="width:80%;"/>
				  	</aui:col>
				  	<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt002--" helpMessage="Ingresar Texto Configurable 002"
						value="${renderRequest.preferences.getValue('recExt002','Recibirás un SMS para poder extornar tu pago')}"
						label="Ingresar Texto Configurable 002" style="width:80%;"/>
				  	</aui:col>
			</aui:row> 
			<aui:row>
			  		<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt003--" helpMessage="Ingresar Texto Configurable 003"
						value="${renderRequest.preferences.getValue('recExt003','Tus datos')}"
						label="Ingresar Texto Configurable 003" style="width:80%;"/>
				  	</aui:col>
				  	<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt004--" helpMessage="Ingresar Texto Configurable 004"
						value="${renderRequest.preferences.getValue('recExt004','Fecha y hora')}"
						label="Ingresar Texto Configurable 004" style="width:80%;"/>
				  	</aui:col>
			</aui:row>
			<aui:row>
			  		<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt005--" helpMessage="Ingresar Texto Configurable 005"
						value="${renderRequest.preferences.getValue('recExt005','N&uacute;mero de tel&eacute;fono')}"
						label="IIngresar Texto Configurable 005" style="width:80%;"/>
				  	</aui:col>
				  	<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt006--" helpMessage="Ingresar Texto Configurable 006"
						value="${renderRequest.preferences.getValue('recExt006','Correo electr&oacute;nico')}"
						label="Ingresar Texto Configurable 006" style="width:80%;"/>
				  	</aui:col>
			</aui:row>
			<aui:row>
			  		<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt007--" helpMessage="Ingresar Texto Configurable 007"
						value="${renderRequest.preferences.getValue('recExt007','')}"
						label="Ingresar Texto Configurable 007" style="width:80%;"/>
				  	</aui:col>
			</aui:row>
			<aui:row>
			  		<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt008--" helpMessage="Ingresar Inicio recompra"
						value="${renderRequest.preferences.getValue('recExt008','')}"
						label="Ingresar Inicio recompra" style="width:80%;"/>
				  	</aui:col>
				  	<aui:col width="45">
					     <aui:input type="textarea" name="preferences--recExt009--" helpMessage="Ingresar Inicio extorno"
						value="${renderRequest.preferences.getValue('recExt009','')}"
						label="Ingresar Inicio extorno" style="width:80%;"/>
				  	</aui:col>
			</aui:row>
		   </aui:col>
		   <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			 <H3>Parámetros de Configuración General</H3>
			 <aui:row>
			  		<aui:col width="45">
					     <aui:input type="textarea" name="preferences--generalPepperType--" helpMessage="PEPPER FISICO = FISICO | PEPPER ONLINE = ONLINE"
						value="${renderRequest.preferences.getValue('generalPepperType','')}"
						label="INGRESAR TIPO DE PEPPER: FISICO / ONLINE" style="width:80%;"/>
				  	</aui:col>
				  	<aui:col width="45">
					     <aui:input type="textarea" name="preferences--generalErrorMassage--" helpMessage="Ingresar mensaje de Error en servidor"
						value="${renderRequest.preferences.getValue('generalErrorMassage','Hubo un problema en el servidor. Por favor, inténtalo nuevamente.')}"
						label="Ingresar mensaje de Error en servidor" style="width:80%;"/>
				  	</aui:col>
			</aui:row>
			<aui:row>
			  		<aui:col width="45">
					     <aui:input type="text" name="preferences--generalTipoCampania--" helpMessage="API SIEBEL = API / CAMPANIA FW4 = FW4"
						value="${renderRequest.preferences.getValue('generalTipoCampania','FW4')}"
						label="TIPO CAMPANIA API SIEBEL = API / FW4 = FW4" style="width:80%;"/>
				  	</aui:col>
				  	<aui:col width="45">
					     <aui:input type="text" name="preferences--generalTipoFechaHora--" helpMessage="3G / 2G"
						value="${renderRequest.preferences.getValue('generalTipoFechaHora','2G')}"
						label="TIPO FECHA HORA ENTREGA 3G / 2G" style="width:80%;"/>
				  	</aui:col>
			</aui:row> 
		   </aui:col>
		   <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
	 <!--==-- Configuración Urls de Servicios --==-->
	 <liferay-ui:section>
	   <aui:row>
	      <aui:col span="2"></aui:col>
	      <aui:col span="10">
			<H3>Parámetros de configuración para urls de servicios</H3>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointListaNegra--" 
					value="${renderRequest.preferences.getValue('urlEndpointListaNegra','')}"
					label="EndPoint Lista Negra" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathListaNegra--" 
					value="${renderRequest.preferences.getValue('urlPathListaNegra','')}"
					label="Path Lista Negra" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointCampania--" 
					value="${renderRequest.preferences.getValue('urlEndpointCampania','')}"
					label="EndPoint Consultar Campaña" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathCampania--" 
					value="${renderRequest.preferences.getValue('urlPathCampania','')}"
					label="Path Consultar Campaña" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointObtenerInfoPersona--" 
					value="${renderRequest.preferences.getValue('urlEndpointObtenerInfoPersona','')}"
					label="EndPoint Obtener Información Persona" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathObtenerInfoPersona--" 
					value="${renderRequest.preferences.getValue('urlPathObtenerInfoPersona','')}"
					label="Path Obtener Información Persona" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointLogicaReingreso--" 
					value="${renderRequest.preferences.getValue('urlEndpointLogicaReingreso','')}"
					label="EndPoint Logica Reingreso" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathLogicaReingreso--" 
					value="${renderRequest.preferences.getValue('urlPathLogicaReingreso','')}"
					label="Path Logica Reingreso" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointObtenerTarjetas--" 
					value="${renderRequest.preferences.getValue('urlEndpointObtenerTarjetas','')}"
					label="EndPoint Obtener Tarjetas" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathObtenerTarjetas--" 
					value="${renderRequest.preferences.getValue('urlPathObtenerTarjetas','')}"
					label="Path Obtener Tarjetas" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointConsultarTarjeta--" 
					value="${renderRequest.preferences.getValue('urlEndpointConsultarTarjeta','')}"
					label="EndPoint Consultar Tarjeta" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathConsultarTarjeta--" 
					value="${renderRequest.preferences.getValue('urlPathConsultarTarjeta','')}"
					label="Path Consultar Tarjeta" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointConsultarPreguntas--" 
					value="${renderRequest.preferences.getValue('urlEndpointConsultarPreguntas','')}"
					label="EndPoint Consultar Preguntas" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathConsultarPreguntas--" 
					value="${renderRequest.preferences.getValue('urlPathConsultarPreguntas','')}"
					label="Path Consultar Preguntas" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointValidarPreguntas--" 
					value="${renderRequest.preferences.getValue('urlEndpointValidarPreguntas','')}"
					label="EndPoint Validar Preguntas" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathValidarPreguntas--" 
					value="${renderRequest.preferences.getValue('urlPathValidarPreguntas','')}"
					label="Path Validar Preguntas" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointGenerarToken--" 
					value="${renderRequest.preferences.getValue('urlEndpointGenerarToken','')}"
					label="EndPoint Generar Token" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathGenerarToken--" 
					value="${renderRequest.preferences.getValue('urlPathGenerarToken','')}"
					label="Path Generar Token" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointValidarToken--" 
					value="${renderRequest.preferences.getValue('urlEndpointValidarToken','')}"
					label="EndPoint Validar Token" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathValidarToken--" 
					value="${renderRequest.preferences.getValue('urlPathValidarToken','')}"
					label="Path Validar Token" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointCalificacion--" 
					value="${renderRequest.preferences.getValue('urlEndpointCalificacion','')}"
					label="EndPoint Calificacion CDA" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathCalificacion--" 
					value="${renderRequest.preferences.getValue('urlPathCalificacion','')}"
					label="Path Calificacion CDA" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointFechaHora--" 
					value="${renderRequest.preferences.getValue('urlEndpointFechaHora','')}"
					label="EndPoint Fecha y Hora" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathFechaHora--" 
					value="${renderRequest.preferences.getValue('urlPathFechaHora','')}"
					label="Path Fecha y Hora" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointAltaPepperOnline--" 
					value="${renderRequest.preferences.getValue('urlEndpointAltaPepperOnline','')}"
					label="EndPoint Alta Pepper Online" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathAltaPepperOnline--" 
					value="${renderRequest.preferences.getValue('urlPathAltaPepperOnline','')}"
					label="Path Alta Pepper Online" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointAuditoria--" 
					value="${renderRequest.preferences.getValue('urlEndpointAuditoria','')}"
					label="EndPoint Auditoria" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathAuditoria--" 
					value="${renderRequest.preferences.getValue('urlPathAuditoria','')}"
					label="Path Auditoria" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointAltaCardless--" 
					value="${renderRequest.preferences.getValue('urlEndpointAltaCardless','')}"
					label="EndPoint Alta Cardless" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathAltaCardless--" 
					value="${renderRequest.preferences.getValue('urlPathAltaCardless','')}"
					label="Path Alta Cardless" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointAltaFisico--" 
					value="${renderRequest.preferences.getValue('urlEndpointAltaFisico','')}"
					label="EndPoint Alta Pepper Fisico" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathAltaFisico--" 
					value="${renderRequest.preferences.getValue('urlPathAltaFisico','')}"
					label="Path Alta Pepper Fisico" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointDocumentLead--" 
					value="${renderRequest.preferences.getValue('urlEndpointDocumentLead','')}"
					label="EndPoint Document Lead" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathDocumentLead--" 
					value="${renderRequest.preferences.getValue('urlPathDocumentLead','')}"
					label="Path Alta Document Lead" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointApiLead--" 
					value="${renderRequest.preferences.getValue('urlEndpointApiLead','')}"
					label="EndPoint API Lead" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathApiLead--" 
					value="${renderRequest.preferences.getValue('urlPathApiLead','')}"
					label="Path API Lead" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointAntifraude--" 
					value="${renderRequest.preferences.getValue('urlEndpointAntifraude','')}"
					label="EndPoint Antifraude" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathAntifraude--" 
					value="${renderRequest.preferences.getValue('urlPathAntifraude','')}"
					label="Path Antifraude" style="width:80%;"/>
			   </aui:col>
			</aui:row>
			<aui:row>
			  <aui:col width="45">
				    <aui:input type="text" name="preferences--urlEndpointBancaSMS--" 
					value="${renderRequest.preferences.getValue('urlEndpointBancaSMS','')}"
					label="EndPoint Banca SMS" style="width:80%;"/>
			   </aui:col>
			   <aui:col width="45">
				    <aui:input type="text" name="preferences--urlPathBancaSMS--" 
					value="${renderRequest.preferences.getValue('urlPathBancaSMS','')}"
					label="Path Banca SMS" style="width:80%;"/>
			   </aui:col>
			</aui:row>
		  </aui:col>
		  <aui:col span="2"></aui:col>
	  	</aui:row>
	 </liferay-ui:section>
</liferay-ui:tabs>
	<aui:button-row> 
		<aui:button type="submit" value="Guardar" />
	</aui:button-row>
</aui:form>