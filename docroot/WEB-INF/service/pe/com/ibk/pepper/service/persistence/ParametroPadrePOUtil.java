/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.ParametroPadrePO;

import java.util.List;

/**
 * The persistence utility for the parametro padre p o service. This utility wraps {@link ParametroPadrePOPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ParametroPadrePOPersistence
 * @see ParametroPadrePOPersistenceImpl
 * @generated
 */
public class ParametroPadrePOUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ParametroPadrePO parametroPadrePO) {
		getPersistence().clearCache(parametroPadrePO);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ParametroPadrePO> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ParametroPadrePO> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ParametroPadrePO> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ParametroPadrePO update(ParametroPadrePO parametroPadrePO)
		throws SystemException {
		return getPersistence().update(parametroPadrePO);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ParametroPadrePO update(ParametroPadrePO parametroPadrePO,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(parametroPadrePO, serviceContext);
	}

	/**
	* Returns the parametro padre p o where codigoPadre = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePOException} if it could not be found.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByC_G_E(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByC_G_E(codigoPadre, estado);
	}

	/**
	* Returns the parametro padre p o where codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByC_G_E(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_G_E(codigoPadre, estado);
	}

	/**
	* Returns the parametro padre p o where codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByC_G_E(
		java.lang.String codigoPadre, boolean estado, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_E(codigoPadre, estado, retrieveFromCache);
	}

	/**
	* Removes the parametro padre p o where codigoPadre = &#63; and estado = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the parametro padre p o that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO removeByC_G_E(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().removeByC_G_E(codigoPadre, estado);
	}

	/**
	* Returns the number of parametro padre p os where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the number of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_G_E(java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_G_E(codigoPadre, estado);
	}

	/**
	* Returns the parametro padre p o where codigoPadre = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePOException} if it could not be found.
	*
	* @param codigoPadre the codigo padre
	* @return the matching parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByC_G(
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByC_G(codigoPadre);
	}

	/**
	* Returns the parametro padre p o where codigoPadre = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByC_G(
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_G(codigoPadre);
	}

	/**
	* Returns the parametro padre p o where codigoPadre = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByC_G(
		java.lang.String codigoPadre, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByC_G(codigoPadre, retrieveFromCache);
	}

	/**
	* Removes the parametro padre p o where codigoPadre = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @return the parametro padre p o that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO removeByC_G(
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().removeByC_G(codigoPadre);
	}

	/**
	* Returns the number of parametro padre p os where codigoPadre = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @return the number of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_G(java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_G(codigoPadre);
	}

	/**
	* Returns all the parametro padre p os where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @return the matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findByN_G(
		java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByN_G(nombre);
	}

	/**
	* Returns a range of all the parametro padre p os where nombre LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @return the range of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findByN_G(
		java.lang.String nombre, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByN_G(nombre, start, end);
	}

	/**
	* Returns an ordered range of all the parametro padre p os where nombre LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findByN_G(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByN_G(nombre, start, end, orderByComparator);
	}

	/**
	* Returns the first parametro padre p o in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByN_G_First(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByN_G_First(nombre, orderByComparator);
	}

	/**
	* Returns the first parametro padre p o in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByN_G_First(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByN_G_First(nombre, orderByComparator);
	}

	/**
	* Returns the last parametro padre p o in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByN_G_Last(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByN_G_Last(nombre, orderByComparator);
	}

	/**
	* Returns the last parametro padre p o in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByN_G_Last(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByN_G_Last(nombre, orderByComparator);
	}

	/**
	* Returns the parametro padre p os before and after the current parametro padre p o in the ordered set where nombre LIKE &#63;.
	*
	* @param idParametroPadre the primary key of the current parametro padre p o
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO[] findByN_G_PrevAndNext(
		long idParametroPadre, java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence()
				   .findByN_G_PrevAndNext(idParametroPadre, nombre,
			orderByComparator);
	}

	/**
	* Removes all the parametro padre p os where nombre LIKE &#63; from the database.
	*
	* @param nombre the nombre
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByN_G(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByN_G(nombre);
	}

	/**
	* Returns the number of parametro padre p os where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static int countByN_G(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByN_G(nombre);
	}

	/**
	* Returns all the parametro padre p os where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the parametro padre p os where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @return the range of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the parametro padre p os where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns the first parametro padre p o in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first parametro padre p o in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last parametro padre p o in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last parametro padre p o in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre p o, or <code>null</code> if a matching parametro padre p o could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the parametro padre p os before and after the current parametro padre p o in the ordered set where groupId = &#63;.
	*
	* @param idParametroPadre the primary key of the current parametro padre p o
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO[] findByGroupId_PrevAndNext(
		long idParametroPadre, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(idParametroPadre, groupId,
			orderByComparator);
	}

	/**
	* Removes all the parametro padre p os where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of parametro padre p os where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the parametro padre p o in the entity cache if it is enabled.
	*
	* @param parametroPadrePO the parametro padre p o
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO) {
		getPersistence().cacheResult(parametroPadrePO);
	}

	/**
	* Caches the parametro padre p os in the entity cache if it is enabled.
	*
	* @param parametroPadrePOs the parametro padre p os
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> parametroPadrePOs) {
		getPersistence().cacheResult(parametroPadrePOs);
	}

	/**
	* Creates a new parametro padre p o with the primary key. Does not add the parametro padre p o to the database.
	*
	* @param idParametroPadre the primary key for the new parametro padre p o
	* @return the new parametro padre p o
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO create(
		long idParametroPadre) {
		return getPersistence().create(idParametroPadre);
	}

	/**
	* Removes the parametro padre p o with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idParametroPadre the primary key of the parametro padre p o
	* @return the parametro padre p o that was removed
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO remove(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().remove(idParametroPadre);
	}

	public static pe.com.ibk.pepper.model.ParametroPadrePO updateImpl(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(parametroPadrePO);
	}

	/**
	* Returns the parametro padre p o with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePOException} if it could not be found.
	*
	* @param idParametroPadre the primary key of the parametro padre p o
	* @return the parametro padre p o
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePOException if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO findByPrimaryKey(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePOException {
		return getPersistence().findByPrimaryKey(idParametroPadre);
	}

	/**
	* Returns the parametro padre p o with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idParametroPadre the primary key of the parametro padre p o
	* @return the parametro padre p o, or <code>null</code> if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ParametroPadrePO fetchByPrimaryKey(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idParametroPadre);
	}

	/**
	* Returns all the parametro padre p os.
	*
	* @return the parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the parametro padre p os.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @return the range of parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the parametro padre p os.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the parametro padre p os from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of parametro padre p os.
	*
	* @return the number of parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ParametroPadrePOPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ParametroPadrePOPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ParametroPadrePOPersistence.class.getName());

			ReferenceRegistry.registerReference(ParametroPadrePOUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ParametroPadrePOPersistence persistence) {
	}

	private static ParametroPadrePOPersistence _persistence;
}