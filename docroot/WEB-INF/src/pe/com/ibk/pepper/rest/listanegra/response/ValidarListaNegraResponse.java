
package pe.com.ibk.pepper.rest.listanegra.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ValidarListaNegraResponse  implements Serializable{

	private static final long serialVersionUID = 5576874683522629340L;
	
	@SerializedName(value="flagValidarListaNegra")
	private String flagValidarListaNegra;
	
	@SerializedName(value="isFraud")
    private String isFraud;

	public String getFlagValidarListaNegra() {
		return flagValidarListaNegra;
	}

	public void setFlagValidarListaNegra(String flagValidarListaNegra) {
		this.flagValidarListaNegra = flagValidarListaNegra;
	}

	public String getIsFraud() {
		return isFraud;
	}

	public void setIsFraud(String isFraud) {
		this.isFraud = isFraud;
	}

}
