package pe.com.ibk.pepper.bean;

import java.util.List;

/**
 * @author yahir.rivas
 * @param <E>
 * */
public class ReporteBean<E> {

	private int fila;
	private int pagina;
	private int cantidad;
	private List<E> list;
	
	public int getFila() {
		return fila;
	}
	public void setFila(int fila) {
		this.fila = fila;
	}
	public int getPagina() {
		return pagina;
	}
	public void setPagina(int pagina) {
		this.pagina = pagina;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public List<E> getList() {
		return list;
	}
	public void setList(List<E> list) {
		this.list = list;
	}
	
}
