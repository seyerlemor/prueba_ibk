
package pe.com.ibk.pepper.rest.campania.response;


public class Campanium {

    private Adicionales adicionales;
    private Cabecera cabecera;
    private Detalle detalle;
    
	public Campanium() {
		super();
	}

	public Campanium(Adicionales adicionales, Cabecera cabecera, Detalle detalle) {
		super();
		this.adicionales = adicionales;
		this.cabecera = cabecera;
		this.detalle = detalle;
	}

	public Adicionales getAdicionales() {
        return adicionales;
    }

    public void setAdicionales(Adicionales adicionales) {
        this.adicionales = adicionales;
    }

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }

	@Override
	public String toString() {
		return "Campanium [adicionales=" + adicionales + ", cabecera="
				+ cabecera + ", detalle=" + detalle + "]";
	}

}
