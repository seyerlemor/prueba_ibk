package pe.com.ibk.pepper.rest.config;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;

import java.io.File;

import javax.net.ssl.HostnameVerifier;
//import javax.net.ssl.SNIHostName;
//import javax.net.ssl.SNIServerName;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;


public class SecurityRestClientAzure {
  private static final String SSL = "SSL";

  private static Log logger =  LogFactoryUtil.getLog(SecurityRestClient.class);

  private SecurityRestClientAzure() {
    throw new IllegalAccessError("Clase EstÃ¡tica");
  }
  
  /**
   * Inicializacion de SecurityRestClient
   * @param host
   * @param path
   * @param user
   * @param password
   * @return
   */
  public static Builder init(String host, String path, String user, String password, String tipoServicio) {
    try {
    	HostnameVerifier hostnameVerifier = new InsecureHostnameVerifier();
    	 Client client = null;
    	 
    	 if(tipoServicio.equals(ConstantesRest.SERVICIO_API)) {

    	        client = ClientBuilder.newBuilder().sslContext(getSSLContextCertified(tipoServicio))
    	  	          .hostnameVerifier(new HostnameVerifier() {
      	                @Override
      	                public boolean verify(String hostname, SSLSession session) {
      	                    return true;
      	                }
  					
      	            }).build();
    	 } else if(tipoServicio.equals(ConstantesRest.SERVICIO_BUS)) {
	    	 client = ClientBuilder.newBuilder().sslContext(getSSLContextCertified(tipoServicio))
	          .hostnameVerifier(hostnameVerifier).build();
    	  }
    	 
      HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(user, password);
      client.register(feature);
        
      WebTarget target = client.target(host).path(path);

      return target.request(MediaType.APPLICATION_JSON_TYPE);
      
    } catch (Exception e) {
      logger.error("Excepcion el la inicializacion de SecurityRestClient ", e);
    }
    return null;
  }

  /**
   * Retorna el protocolo SSL del contexto actual
   * 
   * @return SSLContext Contexto con protocolo SSL
   */
  public static SSLContext getSSLContextCertified(String tipoServicio) {
    SSLContext context = null;
	SslConfigurator sslConfig = null;
    try {
    	String resourceDir = PrefsPropsUtil.getString(PortalKeys.REST_UBICACION_JKS, PortalValues.REST_UBICACION_JKS);
    	logger.debug("resourceDir::>"+PrefsPropsUtil.getString(PortalKeys.REST_UBICACION_JKS, PortalValues.REST_UBICACION_JKS));
    	
    	 if(tipoServicio.equals(ConstantesRest.SERVICIO_API)){
    		
    		 	String keyStoreFile = PrefsPropsUtil.getString(PortalKeys.REST_API_JKS_NAME, PortalValues.REST_API_JKS_NAME);
    		 	String keyPassword = PrefsPropsUtil.getString(PortalKeys.REST_API_JKS_PASSW, PortalValues.REST_API_JKS_PASSW);
    		 	
    		       sslConfig = SslConfigurator.newInstance()
    		    		   .keyStoreFile(resourceDir + File.separator
    		    		              + keyStoreFile)
    		    		          .keyPassword(keyPassword)
    		    		          .keyStoreType("JKS")
    		    		          .trustStoreFile(resourceDir + File.separator
        		    		              + keyStoreFile)
        		    		          .trustStorePassword(keyPassword)
        		    		          .trustStoreType("JKS")
    		          .securityProtocol(SSL);
    		      context = sslConfig.createSSLContext();
   		      
    	 } else if(tipoServicio.equals(ConstantesRest.SERVICIO_BUS)){
    		
    		       sslConfig = SslConfigurator.newInstance()
    		          .keyStoreFile(resourceDir + File.separator
    		              + PrefsPropsUtil.getString(PortalKeys.REST_BUS_JKS_NAME, PortalValues.REST_BUS_JKS_NAME))
    		          .keyPassword(PrefsPropsUtil.getString(PortalKeys.REST_BUS_JKS_PASSW, PortalValues.REST_BUS_JKS_PASSW))
    		          .keyStoreType("JKS")
    		          .securityProtocol(SSL);
    		      context = sslConfig.createSSLContext();

     		
 		
    	 }
    } catch (Exception e) {
      logger.error("Excepcion en el metodo getSSLContextCertified", e);
    }

    return context;
  }
  
  public static SSLContext getTrustAllSSLContextCertified() {
      SSLContext context = null;
      try {
          context = SSLContext.getInstance("TLS");
          final TrustManager[] trustManagerArray = {new NullX509TrustManager()};
          context.init(null, trustManagerArray, null);
      }catch (Exception e) {
          logger.error("" + ": " + "" + e.getMessage(), e);
      }
      return context;
  }
  
//  public static Client restClientInitAzure(String user, String password) {
//		Client client = null;
//		try (PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager()) {
//			client = ClientBuilder.newBuilder().sslContext(getTrustAllSSLContextCertified()).build();
//			HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(user, password);
//			client.register(feature);
////			client.property(ClientProperties.CONNECT_TIMEOUT, TraceConfigConstantes.SERVICE_CONNECT_TIMEOUT);
////			client.property(ClientProperties.READ_TIMEOUT, TraceConfigConstantes.SERVICE_READ_TIMEOUT);
//			
//			connectionManager.setMaxTotal(Integer.parseInt(TraceConfigConstantes.CONNECTION_MAX_SIZE));
//			connectionManager.setDefaultMaxPerRoute(Integer.parseInt(TraceConfigConstantes.CONNECTION_MAX_PER_ROUTE));
//
//			client.property(ApacheClientProperties.CONNECTION_MANAGER, connectionManager);
//
//		} catch (Exception e) {
//			String message = TraceConfigConstantes.MICROSERVICES_CLIENT_ERROR;
//			logger.error(MessageEnum.MICROSERVICES_CLIENT_ERROR.code() + ": " + message + e.getMessage(), e);
//		}
//		return client;
//	}
}
