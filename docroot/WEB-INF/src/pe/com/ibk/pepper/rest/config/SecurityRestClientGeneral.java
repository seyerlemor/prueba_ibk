package pe.com.ibk.pepper.rest.config;

import java.util.Map;

import javax.ws.rs.client.Invocation.Builder;

public class SecurityRestClientGeneral {
	
	private SecurityRestClientGeneral(){}
	
	public static Builder getSecurityRestClient(String host, String path, Boolean flagAutenticacion){
		return SecurityRestClientFix.init(host, path, flagAutenticacion);
	}
	
	public static Builder initAzure(String host, String path, String user, String password, Map<String, String> queryParameters, Map<String, String> templates){
		return SecurityRestClientFix.initAzure(host, path, user, password, queryParameters, templates);
	}
}
