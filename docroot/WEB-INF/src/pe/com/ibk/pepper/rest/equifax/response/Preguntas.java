
package pe.com.ibk.pepper.rest.equifax.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Preguntas implements Serializable {

	private static final long serialVersionUID = -5230510543435078124L;
	
	@SerializedName("pregunta")
	private List<Preguntum> pregunta;

	public List<Preguntum> getPregunta() {
		return pregunta;
	}

	public void setPregunta(List<Preguntum> pregunta) {
		this.pregunta = pregunta;
	}
   
}
