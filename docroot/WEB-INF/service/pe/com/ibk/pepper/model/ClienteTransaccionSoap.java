/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class ClienteTransaccionSoap implements Serializable {
	public static ClienteTransaccionSoap toSoapModel(ClienteTransaccion model) {
		ClienteTransaccionSoap soapModel = new ClienteTransaccionSoap();

		soapModel.setIdClienteTransaccion(model.getIdClienteTransaccion());
		soapModel.setIdUsuarioSession(model.getIdUsuarioSession());
		soapModel.setIdExpediente(model.getIdExpediente());
		soapModel.setEstado(model.getEstado());
		soapModel.setPaso(model.getPaso());
		soapModel.setPagina(model.getPagina());
		soapModel.setTipoFlujo(model.getTipoFlujo());
		soapModel.setStrJson(model.getStrJson());
		soapModel.setRestauracion(model.getRestauracion());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static ClienteTransaccionSoap[] toSoapModels(
		ClienteTransaccion[] models) {
		ClienteTransaccionSoap[] soapModels = new ClienteTransaccionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ClienteTransaccionSoap[][] toSoapModels(
		ClienteTransaccion[][] models) {
		ClienteTransaccionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ClienteTransaccionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ClienteTransaccionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ClienteTransaccionSoap[] toSoapModels(
		List<ClienteTransaccion> models) {
		List<ClienteTransaccionSoap> soapModels = new ArrayList<ClienteTransaccionSoap>(models.size());

		for (ClienteTransaccion model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ClienteTransaccionSoap[soapModels.size()]);
	}

	public ClienteTransaccionSoap() {
	}

	public long getPrimaryKey() {
		return _idClienteTransaccion;
	}

	public void setPrimaryKey(long pk) {
		setIdClienteTransaccion(pk);
	}

	public long getIdClienteTransaccion() {
		return _idClienteTransaccion;
	}

	public void setIdClienteTransaccion(long idClienteTransaccion) {
		_idClienteTransaccion = idClienteTransaccion;
	}

	public long getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	public void setIdUsuarioSession(long idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;
	}

	public long getIdExpediente() {
		return _idExpediente;
	}

	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;
	}

	public String getEstado() {
		return _estado;
	}

	public void setEstado(String estado) {
		_estado = estado;
	}

	public String getPaso() {
		return _paso;
	}

	public void setPaso(String paso) {
		_paso = paso;
	}

	public String getPagina() {
		return _pagina;
	}

	public void setPagina(String pagina) {
		_pagina = pagina;
	}

	public String getTipoFlujo() {
		return _tipoFlujo;
	}

	public void setTipoFlujo(String tipoFlujo) {
		_tipoFlujo = tipoFlujo;
	}

	public String getStrJson() {
		return _strJson;
	}

	public void setStrJson(String strJson) {
		_strJson = strJson;
	}

	public String getRestauracion() {
		return _restauracion;
	}

	public void setRestauracion(String restauracion) {
		_restauracion = restauracion;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private long _idClienteTransaccion;
	private long _idUsuarioSession;
	private long _idExpediente;
	private String _estado;
	private String _paso;
	private String _pagina;
	private String _tipoFlujo;
	private String _strJson;
	private String _restauracion;
	private Date _fechaRegistro;
}