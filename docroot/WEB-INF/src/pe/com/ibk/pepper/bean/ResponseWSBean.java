package pe.com.ibk.pepper.bean;


import java.util.List;

public class ResponseWSBean {

private Boolean success;
private String challenge_ts;
private String hostname;
private List<String> errorCodes = null;

/**
* No args constructor for use in serialization
* 
*/
public ResponseWSBean() {
}

/**
*
* @param hostname
* @param success
* @param errorCodes
* @param challenge_ts
*/
public ResponseWSBean(Boolean success, List<String> errorCodes, String challenge_ts, String hostname) {
super();
this.success = success;
this.challenge_ts = challenge_ts;
this.hostname = hostname;
this.errorCodes = errorCodes;
}

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getHostname() {
return hostname;
}

public void setHostname(String hostname) {
this.hostname = hostname;
}

public List<String> getErrorCodes() {
return errorCodes;
}

public void setErrorCodes(List<String> errorCodes) {
this.errorCodes = errorCodes;
}

public String getChallenge_ts() {
	return challenge_ts;
}

public void setChallenge_ts(String challenge_ts) {
	this.challenge_ts = challenge_ts;
}



}
