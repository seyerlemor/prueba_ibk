/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author Interbank
 */
public class ClienteFinderUtil {
	public static java.util.List<pe.com.ibk.pepper.model.Cliente> findByF_F_N_A_D(
		java.lang.String fechaDesde, java.lang.String fechaHasta,
		java.lang.String nombres, java.lang.String apellidos,
		java.lang.String DNI, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder()
				   .findByF_F_N_A_D(fechaDesde, fechaHasta, nombres, apellidos,
			DNI, start, end);
	}

	public static int countByF_F_N_A_D(java.lang.String fechaDesde,
		java.lang.String fechaHasta, java.lang.String nombres,
		java.lang.String apellidos, java.lang.String DNI)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder()
				   .countByF_F_N_A_D(fechaDesde, fechaHasta, nombres,
			apellidos, DNI);
	}

	public static ClienteFinder getFinder() {
		if (_finder == null) {
			_finder = (ClienteFinder)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ClienteFinder.class.getName());

			ReferenceRegistry.registerReference(ClienteFinderUtil.class,
				"_finder");
		}

		return _finder;
	}

	public void setFinder(ClienteFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(ClienteFinderUtil.class, "_finder");
	}

	private static ClienteFinder _finder;
}