/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.LogQRadarLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class LogQRadarClp extends BaseModelImpl<LogQRadar> implements LogQRadar {
	public LogQRadarClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return LogQRadar.class;
	}

	@Override
	public String getModelClassName() {
		return LogQRadar.class.getName();
	}

	@Override
	public int getPrimaryKey() {
		return _idLogQRadar;
	}

	@Override
	public void setPrimaryKey(int primaryKey) {
		setIdLogQRadar(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idLogQRadar;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idLogQRadar", getIdLogQRadar());
		attributes.put("prioridad", getPrioridad());
		attributes.put("severidad", getSeveridad());
		attributes.put("recurso", getRecurso());
		attributes.put("utc", getUtc());
		attributes.put("logCreador", getLogCreador());
		attributes.put("tipoEvento", getTipoEvento());
		attributes.put("registroUsuario", getRegistroUsuario());
		attributes.put("origenEvento", getOrigenEvento());
		attributes.put("respuesta", getRespuesta());
		attributes.put("ambiente", getAmbiente());
		attributes.put("producto", getProducto());
		attributes.put("idProducto", getIdProducto());
		attributes.put("fechaHora", getFechaHora());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idLogQRadar = (Integer)attributes.get("idLogQRadar");

		if (idLogQRadar != null) {
			setIdLogQRadar(idLogQRadar);
		}

		Integer prioridad = (Integer)attributes.get("prioridad");

		if (prioridad != null) {
			setPrioridad(prioridad);
		}

		Integer severidad = (Integer)attributes.get("severidad");

		if (severidad != null) {
			setSeveridad(severidad);
		}

		Integer recurso = (Integer)attributes.get("recurso");

		if (recurso != null) {
			setRecurso(recurso);
		}

		Date utc = (Date)attributes.get("utc");

		if (utc != null) {
			setUtc(utc);
		}

		String logCreador = (String)attributes.get("logCreador");

		if (logCreador != null) {
			setLogCreador(logCreador);
		}

		String tipoEvento = (String)attributes.get("tipoEvento");

		if (tipoEvento != null) {
			setTipoEvento(tipoEvento);
		}

		String registroUsuario = (String)attributes.get("registroUsuario");

		if (registroUsuario != null) {
			setRegistroUsuario(registroUsuario);
		}

		String origenEvento = (String)attributes.get("origenEvento");

		if (origenEvento != null) {
			setOrigenEvento(origenEvento);
		}

		String respuesta = (String)attributes.get("respuesta");

		if (respuesta != null) {
			setRespuesta(respuesta);
		}

		String ambiente = (String)attributes.get("ambiente");

		if (ambiente != null) {
			setAmbiente(ambiente);
		}

		String producto = (String)attributes.get("producto");

		if (producto != null) {
			setProducto(producto);
		}

		Integer idProducto = (Integer)attributes.get("idProducto");

		if (idProducto != null) {
			setIdProducto(idProducto);
		}

		Date fechaHora = (Date)attributes.get("fechaHora");

		if (fechaHora != null) {
			setFechaHora(fechaHora);
		}
	}

	@Override
	public int getIdLogQRadar() {
		return _idLogQRadar;
	}

	@Override
	public void setIdLogQRadar(int idLogQRadar) {
		_idLogQRadar = idLogQRadar;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setIdLogQRadar", int.class);

				method.invoke(_logQRadarRemoteModel, idLogQRadar);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getPrioridad() {
		return _prioridad;
	}

	@Override
	public void setPrioridad(int prioridad) {
		_prioridad = prioridad;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setPrioridad", int.class);

				method.invoke(_logQRadarRemoteModel, prioridad);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSeveridad() {
		return _severidad;
	}

	@Override
	public void setSeveridad(int severidad) {
		_severidad = severidad;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setSeveridad", int.class);

				method.invoke(_logQRadarRemoteModel, severidad);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRecurso() {
		return _recurso;
	}

	@Override
	public void setRecurso(int recurso) {
		_recurso = recurso;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setRecurso", int.class);

				method.invoke(_logQRadarRemoteModel, recurso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getUtc() {
		return _utc;
	}

	@Override
	public void setUtc(Date utc) {
		_utc = utc;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setUtc", Date.class);

				method.invoke(_logQRadarRemoteModel, utc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLogCreador() {
		return _logCreador;
	}

	@Override
	public void setLogCreador(String logCreador) {
		_logCreador = logCreador;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setLogCreador", String.class);

				method.invoke(_logQRadarRemoteModel, logCreador);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoEvento() {
		return _tipoEvento;
	}

	@Override
	public void setTipoEvento(String tipoEvento) {
		_tipoEvento = tipoEvento;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoEvento", String.class);

				method.invoke(_logQRadarRemoteModel, tipoEvento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRegistroUsuario() {
		return _registroUsuario;
	}

	@Override
	public void setRegistroUsuario(String registroUsuario) {
		_registroUsuario = registroUsuario;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setRegistroUsuario",
						String.class);

				method.invoke(_logQRadarRemoteModel, registroUsuario);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getOrigenEvento() {
		return _origenEvento;
	}

	@Override
	public void setOrigenEvento(String origenEvento) {
		_origenEvento = origenEvento;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setOrigenEvento", String.class);

				method.invoke(_logQRadarRemoteModel, origenEvento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRespuesta() {
		return _respuesta;
	}

	@Override
	public void setRespuesta(String respuesta) {
		_respuesta = respuesta;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setRespuesta", String.class);

				method.invoke(_logQRadarRemoteModel, respuesta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAmbiente() {
		return _ambiente;
	}

	@Override
	public void setAmbiente(String ambiente) {
		_ambiente = ambiente;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setAmbiente", String.class);

				method.invoke(_logQRadarRemoteModel, ambiente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getProducto() {
		return _producto;
	}

	@Override
	public void setProducto(String producto) {
		_producto = producto;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setProducto", String.class);

				method.invoke(_logQRadarRemoteModel, producto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getIdProducto() {
		return _idProducto;
	}

	@Override
	public void setIdProducto(int idProducto) {
		_idProducto = idProducto;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setIdProducto", int.class);

				method.invoke(_logQRadarRemoteModel, idProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaHora() {
		return _fechaHora;
	}

	@Override
	public void setFechaHora(Date fechaHora) {
		_fechaHora = fechaHora;

		if (_logQRadarRemoteModel != null) {
			try {
				Class<?> clazz = _logQRadarRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaHora", Date.class);

				method.invoke(_logQRadarRemoteModel, fechaHora);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getLogQRadarRemoteModel() {
		return _logQRadarRemoteModel;
	}

	public void setLogQRadarRemoteModel(BaseModel<?> logQRadarRemoteModel) {
		_logQRadarRemoteModel = logQRadarRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _logQRadarRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_logQRadarRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			LogQRadarLocalServiceUtil.addLogQRadar(this);
		}
		else {
			LogQRadarLocalServiceUtil.updateLogQRadar(this);
		}
	}

	@Override
	public LogQRadar toEscapedModel() {
		return (LogQRadar)ProxyUtil.newProxyInstance(LogQRadar.class.getClassLoader(),
			new Class[] { LogQRadar.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LogQRadarClp clone = new LogQRadarClp();

		clone.setIdLogQRadar(getIdLogQRadar());
		clone.setPrioridad(getPrioridad());
		clone.setSeveridad(getSeveridad());
		clone.setRecurso(getRecurso());
		clone.setUtc(getUtc());
		clone.setLogCreador(getLogCreador());
		clone.setTipoEvento(getTipoEvento());
		clone.setRegistroUsuario(getRegistroUsuario());
		clone.setOrigenEvento(getOrigenEvento());
		clone.setRespuesta(getRespuesta());
		clone.setAmbiente(getAmbiente());
		clone.setProducto(getProducto());
		clone.setIdProducto(getIdProducto());
		clone.setFechaHora(getFechaHora());

		return clone;
	}

	@Override
	public int compareTo(LogQRadar logQRadar) {
		int primaryKey = logQRadar.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LogQRadarClp)) {
			return false;
		}

		LogQRadarClp logQRadar = (LogQRadarClp)obj;

		int primaryKey = logQRadar.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{idLogQRadar=");
		sb.append(getIdLogQRadar());
		sb.append(", prioridad=");
		sb.append(getPrioridad());
		sb.append(", severidad=");
		sb.append(getSeveridad());
		sb.append(", recurso=");
		sb.append(getRecurso());
		sb.append(", utc=");
		sb.append(getUtc());
		sb.append(", logCreador=");
		sb.append(getLogCreador());
		sb.append(", tipoEvento=");
		sb.append(getTipoEvento());
		sb.append(", registroUsuario=");
		sb.append(getRegistroUsuario());
		sb.append(", origenEvento=");
		sb.append(getOrigenEvento());
		sb.append(", respuesta=");
		sb.append(getRespuesta());
		sb.append(", ambiente=");
		sb.append(getAmbiente());
		sb.append(", producto=");
		sb.append(getProducto());
		sb.append(", idProducto=");
		sb.append(getIdProducto());
		sb.append(", fechaHora=");
		sb.append(getFechaHora());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.LogQRadar");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idLogQRadar</column-name><column-value><![CDATA[");
		sb.append(getIdLogQRadar());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prioridad</column-name><column-value><![CDATA[");
		sb.append(getPrioridad());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>severidad</column-name><column-value><![CDATA[");
		sb.append(getSeveridad());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>recurso</column-name><column-value><![CDATA[");
		sb.append(getRecurso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>utc</column-name><column-value><![CDATA[");
		sb.append(getUtc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>logCreador</column-name><column-value><![CDATA[");
		sb.append(getLogCreador());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoEvento</column-name><column-value><![CDATA[");
		sb.append(getTipoEvento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>registroUsuario</column-name><column-value><![CDATA[");
		sb.append(getRegistroUsuario());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>origenEvento</column-name><column-value><![CDATA[");
		sb.append(getOrigenEvento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>respuesta</column-name><column-value><![CDATA[");
		sb.append(getRespuesta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ambiente</column-name><column-value><![CDATA[");
		sb.append(getAmbiente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>producto</column-name><column-value><![CDATA[");
		sb.append(getProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idProducto</column-name><column-value><![CDATA[");
		sb.append(getIdProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaHora</column-name><column-value><![CDATA[");
		sb.append(getFechaHora());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _idLogQRadar;
	private int _prioridad;
	private int _severidad;
	private int _recurso;
	private Date _utc;
	private String _logCreador;
	private String _tipoEvento;
	private String _registroUsuario;
	private String _origenEvento;
	private String _respuesta;
	private String _ambiente;
	private String _producto;
	private int _idProducto;
	private Date _fechaHora;
	private BaseModel<?> _logQRadarRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}