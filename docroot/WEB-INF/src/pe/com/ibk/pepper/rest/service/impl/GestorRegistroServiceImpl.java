package pe.com.ibk.pepper.rest.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.text.ParseException;
import java.util.Date;

import org.springframework.stereotype.Service;

import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.bean.UsuarioSessionBean;
import pe.com.ibk.pepper.model.Cliente;
import pe.com.ibk.pepper.model.Expediente;
import pe.com.ibk.pepper.model.Producto;
import pe.com.ibk.pepper.model.UsuarioSession;
import pe.com.ibk.pepper.rest.service.GestorRegistroService;
import pe.com.ibk.pepper.service.ClienteLocalServiceUtil;
import pe.com.ibk.pepper.service.ExpedienteLocalServiceUtil;
import pe.com.ibk.pepper.service.ProductoLocalServiceUtil;
import pe.com.ibk.pepper.service.UsuarioSessionLocalServiceUtil;
import pe.com.ibk.pepper.util.DatesUtil;

@Service
public class GestorRegistroServiceImpl implements GestorRegistroService{

	private static final Log _log = LogFactoryUtil.getLog(GestorRegistroServiceImpl.class); 

	@Override
	public long registrarExpediente(SolicitudBean pasoBean) {
		long idExpediente = 0;
		try {
			Expediente expediente = ExpedienteLocalServiceUtil.createExpediente(-1);
			expediente = setPasoBeanToExpediente(pasoBean, expediente);
		    expediente = ExpedienteLocalServiceUtil.registrarExpediente(expediente);
		    idExpediente = expediente.getIdExpediente();
		} catch (SystemException e) {
			_log.error("Error en la clase GestorRegistros, metodo registrarExpediente:::>", e);
		}
		return idExpediente;
	}		

	@Override
	public long registrarUsuarioSession(UsuarioSessionBean usuarioSessionBean) {
		long idUsuarioSession = 0;
		try {
			UsuarioSession usuarioSession = UsuarioSessionLocalServiceUtil.createUsuarioSession(-1);
			usuarioSession.setIdSession(usuarioSessionBean.getIdSession());
			usuarioSession.setUserId(usuarioSessionBean.getUserId());
			usuarioSession.setEstado(usuarioSessionBean.getEstado());
			usuarioSession.setTienda(usuarioSessionBean.getTienda());
			usuarioSession.setEstablecimiento(usuarioSessionBean.getEstablecimiento());
			usuarioSession.setNombreVendedor(usuarioSessionBean.getVendedor());
			usuarioSession = UsuarioSessionLocalServiceUtil.registrarUsuarioSession(usuarioSession);
		    idUsuarioSession = usuarioSession.getIdUsuarioSession();
		} catch (Exception e) {
			_log.error("Error en la clase GestorRegistros, metodo registrarUsuarioSession:::>", e);
		}
		return idUsuarioSession;
	}
	
	@Override
	public long registrarCliente(SolicitudBean pasoBean) {	
		long idCliente = 0;				
		try {
			Cliente cliente = ClienteLocalServiceUtil.createCliente(-1);
			cliente = setPasoBeanToCliente(pasoBean, cliente);
			cliente = ClienteLocalServiceUtil.registrarCliente(cliente);
			idCliente = cliente.getIdDatoCliente();
		} catch (Exception e) {
			_log.error("Error en la clase GestorRegistros, metodo registrarCliente:::>", e);
		}
		return idCliente;
	}	
	
	@Override
	public long registrarProducto(SolicitudBean pasoBean) {	
		long idProducto = 0;
		try {
			Producto producto = ProductoLocalServiceUtil.createProducto(-1);
			producto = setPasoBeanToProducto(pasoBean, producto);
			producto = ProductoLocalServiceUtil.registrarProducto(producto);
			idProducto = producto.getIdProducto();		
		} catch (Exception e) {
			_log.error("Error en la clase GestorRegistros, metodo registrarProductoCliente:::>", e);
		}
		return idProducto;
	}	

	@Override
	public Expediente setPasoBeanToExpediente(SolicitudBean pasoBean, Expediente expediente){
		expediente.setDocumento(pasoBean.getExpedienteBean().getDniNumber());
		expediente.setEstado(pasoBean.getExpedienteBean().getEstado());
		expediente.setPaso(pasoBean.getExpedienteBean().getPaso());
		expediente.setTipoFlujo(pasoBean.getExpedienteBean().getTipoFlujo());	
		expediente.setFlagR1(Validator.isNull(pasoBean.getExpedienteBean().getFlagR1())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagR1()));
		expediente.setFlagCampania(Validator.isNull(pasoBean.getExpedienteBean().getFlagCampania())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagCampania()));
		expediente.setFlagEquifax(Validator.isNull(pasoBean.getExpedienteBean().getFlagValidarEquifax())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagValidarEquifax()));
		expediente.setTipoCampania(pasoBean.getExpedienteBean().getTipoCampania());
		expediente.setFlagCalificacion(Validator.isNull(pasoBean.getExpedienteBean().getFlagCalificacion())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagCalificacion()));
		expediente.setFlagCliente(Validator.isNull(pasoBean.getClienteBean().getFlagCliente())?StringPool.BLANK:String.valueOf(pasoBean.getClienteBean().getFlagCliente()));
		expediente.setIdUsuarioSession(pasoBean.getExpedienteBean().getIdUsuarioSession());
		expediente.setModificacion(new Date());
		expediente.setCodigoHtml(pasoBean.getExpedienteBean().getHtmlCode());
		expediente.setIp(pasoBean.getClienteBean().getIp());
		expediente.setNavegador(pasoBean.getExpedienteBean().getNavegador());
		expediente.setFlagObtenerInformacion(Validator.isNull(pasoBean.getExpedienteBean().getFlagObtenerInformacion())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagObtenerInformacion()));
		expediente.setFlagGeneracionToken(Validator.isNull(pasoBean.getExpedienteBean().getFlagGeneracionToken())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagGeneracionToken()));
		expediente.setFlagVentaTC(Validator.isNull(pasoBean.getExpedienteBean().getFlagVentaTC())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagVentaTC()));
		expediente.setFlagValidacionToken(Validator.isNull(pasoBean.getExpedienteBean().getFlagValidacionToken())?StringPool.BLANK:String.valueOf(pasoBean.getExpedienteBean().getFlagValidacionToken()));
		expediente.setPepperType(pasoBean.getExpedienteBean().getPepperType());
		expediente.setFormHtml(pasoBean.getExpedienteBean().getCommerceId());
		return expediente;
	}		

	@Override
	public UsuarioSession setUsuarioSessionBeanToUsuarioSession(UsuarioSessionBean usuarioSession1, UsuarioSession usuarioSession2){
		usuarioSession2.setIdSession(usuarioSession1.getIdSession());
		usuarioSession2.setTienda(usuarioSession1.getTienda());
		usuarioSession2.setEstado(usuarioSession1.getEstado());
		usuarioSession2.setUserId(usuarioSession1.getUserId());
		usuarioSession2.setTiendaId(usuarioSession1.getTiendaId());
		return usuarioSession2;
	}	

	@Override
	public Cliente setPasoBeanToCliente(SolicitudBean pasoBean, Cliente cliente2){
		cliente2.setIdExpediente(pasoBean.getExpedienteBean().getIdExpediente());
		cliente2.setPrimerNombre(pasoBean.getClienteBean().getFirstname());
		cliente2.setSegundoNombre(pasoBean.getClienteBean().getMiddlename());
		cliente2.setApellidoPaterno(pasoBean.getClienteBean().getLastname());
		cliente2.setApellidoMaterno(pasoBean.getClienteBean().getMotherslastname());
		cliente2.setSexo(pasoBean.getClienteBean().getGender());
		cliente2.setCorreo(pasoBean.getClienteBean().getEmailAddress());
		cliente2.setTelefono(pasoBean.getClienteBean().getCellPhoneNumber());
		cliente2.setEstadoCivil(pasoBean.getClienteBean().getCivilStatus());
		cliente2.setSituacionLaboral(pasoBean.getClienteBean().getEmploymentStatus());
		cliente2.setRucEmpresaTrabajo(pasoBean.getClienteBean().getRuc());
		cliente2.setNivelEducacion(pasoBean.getClienteBean().getNivelEducacion());
		cliente2.setOcupacion(pasoBean.getClienteBean().getOcupacion());
		cliente2.setCargoActual(pasoBean.getClienteBean().getCargoActual());
		cliente2.setActividadNegocio(pasoBean.getClienteBean().getActividadNegocio());
		cliente2.setDocumento(pasoBean.getClienteBean().getDniNumber());
		cliente2.setTerminosCondiciones(pasoBean.getProductoBean().getFlagLPDP());
		cliente2.setTipoDocumento(pasoBean.getClienteBean().getTipoDocumento());
		cliente2.setFlagPEP(pasoBean.getClienteBean().getPersonaExpuestaPublica());
		cliente2.setOperador(pasoBean.getClienteBean().getCellPhoneOperator());
		cliente2.setFechaNacimiento(Validator.isNotNull(pasoBean.getClienteBean().getBirthday())?DatesUtil.stringAdate2(pasoBean.getClienteBean().getBirthday()):null);
		return cliente2;
	}			
	
	@Override
	public Producto setPasoBeanToProducto(SolicitudBean pasoBean, Producto producto2) throws ParseException{
		producto2.setIdExpediente(pasoBean.getExpedienteBean().getIdExpediente());
		producto2.setTipoProducto(pasoBean.getProductoBean().getTipoProducto());
		producto2.setCodigoProducto(pasoBean.getProductoBean().getCodigoProducto());
		producto2.setMoneda(pasoBean.getProductoBean().getMoneda());
		producto2.setMarcaProducto(pasoBean.getProductoBean().getMarcaProducto());
		producto2.setLineaCredito(pasoBean.getProductoBean().getLineaCredito());
		producto2.setDiaPago(pasoBean.getProductoBean().getDiaDePago());
		producto2.setTasaProducto(pasoBean.getProductoBean().getTasaProducto());
		producto2.setNombreTitularProducto(pasoBean.getProductoBean().getNombreTitularProducto());
		producto2.setTipoSeguro(pasoBean.getProductoBean().getTipoSeguro());
		producto2.setCostoSeguro(pasoBean.getProductoBean().getCostoSeguro());
		producto2.setFlagEnvioEECCFisico(Boolean.parseBoolean(pasoBean.getProductoBean().getFlagEnvioEECCFisico())?"1":"0");
		producto2.setFlagEnvioEECCEmail(Boolean.parseBoolean(pasoBean.getProductoBean().getFlagEnvioEECCEmail())?"1":"0");
		producto2.setFlagTerminos(Boolean.parseBoolean(pasoBean.getProductoBean().getTerminos())?"1":"0");
		producto2.setFlagCargoCompra(Boolean.parseBoolean(pasoBean.getProductoBean().getPurchasePolicies())?"1":"0");
		producto2.setCodigoUnico(pasoBean.getProductoBean().getCodigoUnico());
		producto2.setRazonNoUsoTarjeta(pasoBean.getProductoBean().getRazonNoUsoTarjeta());
		producto2.setMotivo(pasoBean.getProductoBean().getMotivo());
		return producto2;
	}	
}
