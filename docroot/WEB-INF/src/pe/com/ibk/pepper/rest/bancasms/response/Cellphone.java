package pe.com.ibk.pepper.rest.bancasms.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cellphone  implements Serializable{

	private static final long serialVersionUID = 1L;

	@SerializedName("operatorCode")
	private String operatorCode;
	
	@SerializedName("number")
	private String number;

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "Cellphone [operatorCode=" + operatorCode + ", number=" + number
				+ "]";
	}
}
