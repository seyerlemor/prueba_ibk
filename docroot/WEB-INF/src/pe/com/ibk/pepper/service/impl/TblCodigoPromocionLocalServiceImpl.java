/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

import pe.com.ibk.pepper.model.TblCodigoPromocion;
import pe.com.ibk.pepper.service.base.TblCodigoPromocionLocalServiceBaseImpl;
import pe.com.ibk.pepper.util.Constantes;

/**
 * The implementation of the tbl codigo promocion local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.TblCodigoPromocionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.TblCodigoPromocionLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.TblCodigoPromocionLocalServiceUtil
 */
public class TblCodigoPromocionLocalServiceImpl
	extends TblCodigoPromocionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.TblCodigoPromocionLocalServiceUtil} to access the tbl codigo promocion local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(TblCodigoPromocionLocalServiceImpl.class);
	
	@Override
	public List<TblCodigoPromocion> listarCodigoPromocion(String estado){
		List<TblCodigoPromocion> tblCodigoPromocion = null;
		try {
			tblCodigoPromocion = tblCodigoPromocionPersistence.findByCP_E(estado);
		} catch (SystemException e) {
			_log.error("Error en clase TblCodigoPromocionLocalServiceImpl metodo listarCodigoPromocion:: ", e );
		}
		return tblCodigoPromocion;
	}
	
	@Override
	public TblCodigoPromocion obtenerCodigoPromocion(String estado){
		TblCodigoPromocion tblCodigoPromocion = null;
		try {
			List<TblCodigoPromocion> lstTblCodigo = tblCodigoPromocionPersistence.findByCP_E(estado);
			tblCodigoPromocion = lstTblCodigo.get(0);
		} catch (SystemException e) {
			_log.error("Error en clase TblCodigoPromocionLocalServiceImpl metodo obtenerCodigoPromocion:: " );
		}
		return tblCodigoPromocion;
	}
	
	@Override
	public TblCodigoPromocion obtenerCodigoPromocionByComercio(String estado, String comercio){
		TblCodigoPromocion tblCodigoPromocion = null ;
		try {
			List<TblCodigoPromocion> lstTblCodigo = tblCodigoPromocionPersistence.findByCP_C_E(comercio, estado);
			_log.error(lstTblCodigo.size());
			if(lstTblCodigo.size()>0){
				tblCodigoPromocion = lstTblCodigo.get(0);
				tblCodigoPromocion.setEstado(Constantes.ESTADO_INACTIVO);
				tblCodigoPromocionPersistence.update(tblCodigoPromocion);
			}
		} catch (SystemException e) {
			_log.error("Error en clase TblCodigoPromocionLocalServiceImpl metodo obtenerCodigoPromocionByComercio:: " );
		}
		return tblCodigoPromocion;
	}
}