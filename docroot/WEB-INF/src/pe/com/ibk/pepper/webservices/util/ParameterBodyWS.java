package pe.com.ibk.pepper.webservices.util;


public class ParameterBodyWS
{

	/*BODY WS CONSULTA CAMPANIA*/
	public static final String B_TIPO_BUSQUEDA = "B_TIPO_BUSQUEDA";
	public static final String B_FLG_DATOS_OFERTA = "B_FLG_DATOS_OFERTA";
	public static final String B_FLG_NUEVO_BUS = "B_FLG_NUEVO_BUS";
	public static final String B_FLG_CONSULT_DIR = "B_FLG_CONSULT_DIR";
	public static final String B_TIPO_DOCUMENTO = "B_TIPO_DOCUMENTO";
	public static final String B_COD_CANAL = "B_COD_CANAL";
	public static final String B_COD_PRODUCTO = "B_COD_PRODUCTO";

	/*BODY WS CONSULTA PREGUNTAS EQUIFAX*/
	public static final String B_COD_INT1 = "B_COD_INT1";
	public static final String B_COD_INT2 = "B_COD_INT2";
	public static final String B_COD_INT3 = "B_COD_INT3";
	public static final String B_INICIO_VALIDA = "B_INICIO_VALIDA";

	//
	public static final String B_AMBIENTE = "B_AMBIENTE";
	public static final String B_TIPO_GESTION = "B_TIPO_GESTION";
	public static final String B_COD_TIPO_FLUJO = "B_COD_TIPO_FLUJO";
	public static final String B_NUM_LLAMADA = "B_NUM_LLAMADA";
	public static final String B_USUARIO = "B_USUARIO";
	public static final String B_CLASE_PRODUCTO = "B_CLASE_PRODUCTO";
	public static final String B_COD_PTO_VENTA = "B_COD_PTO_VENTA";
	public static final String B_COD_TIENDA = "B_COD_TIENDA";
	public static final String B_COD_PROMOTOR = "B_COD_PROMOTOR";
	public static final String B_TIPO_TRAMITE = "B_TIPO_TRAMITE";
	public static final String B_TIPO_FLUJO = "B_TIPO_FLUJO";
	public static final String B_USUARIO_REG = "B_USUARIO_REG";
	public static final String B_COD_EVALUACION = "B_COD_EVALUACION";
	public static final String B_NUM_DEPENDIENTES = "B_NUM_DEPENDIENTES";
	public static final String B_TIPO_TELEFONO = "B_TIPO_TELEFONO";
	public static final String B_TIPO_VIVIENDA = "B_TIPO_VIVIENDA";
	public static final String B_IND_DOM_LABORAL = "B_IND_DOM_LABORAL"; 
	public static final String B_COD_TIPO_SOLIC = "B_COD_TIPO_SOLIC"; 
	public static final String B_COD_CLASE_PROD = "B_COD_CLASE_PROD";  
	public static final String B_NOMBRE_PROD = "B_NOMBRE_PROD";
	
	public static final String B_IND_AVAL = "B_IND_AVAL";
	public static final String B_PROD_PRINCIPAL = "B_PROD_PRINCIPAL";
	public static final String B_IND_AMP_LINEA = "B_IND_AMP_LINEA";
	public static final String B_IND_RED_LINEA = "B_IND_RED_LINEA";
	public static final String B_IND_ING_CONYUGAL = "B_IND_ING_CONYUGAL";
	public static final String B_MONEDA = "B_MONEDA";
	public static final String B_IND_DISP_EFECTIVO = "B_IND_DISP_EFECTIVO";
	public static final String B_VAL_DISP_EFECTIVO = "B_VAL_DISP_EFECTIVO";
	public static final String B_FECHA_PAGO = "B_FECHA_PAGO";
	
	//CALIFICACION CDA CONYUGE
	public static final String B_SITUACION_LABORAL = "B_SITUACION_LABORAL";
	public static final String B_IND_PEP = "B_IND_PEP";
	public static final String B_NIVEL_EDUCACION = "B_NIVEL_EDUCACION";
	public static final String B_OCUPACION = "B_OCUPACION";	
	public static final String B_CARGO = "B_CARGO";	
	public static final String B_GIRO_EMPRESA = "B_GIRO_EMPRESA";	
	public static final String B_FEC_ING_LABORAL = "B_FEC_ING_LABORAL";	
	public static final String B_ING_MENS_FIJO = "B_ING_MENS_FIJO";	
	public static final String B_ING_MENS_VAR = "B_ING_MENS_VAR";	
	public static final String B_DEP_LABORAL = "B_DEP_LABORAL";	
	public static final String B_PROV_LABORAL = "B_PROV_LABORAL";	
	public static final String B_DIS_LABORAL = "B_DIS_LABORAL";	
	
	public static final String B_NUM_INTEGRANTE = "B_NUM_INTEGRANTE";
	
	public static final String B_FLG_OBT_TARIF = "B_FLG_OBT_TARIF";
	public static final String B_FLG_OBT_DAT_CLI = "B_FLG_OBT_DAT_CLI";
	
	public static final String B_ID_TOKEN = "B_ID_TOKEN";
	public static final String B_SEND_METHOD = "B_SEND_METHOD";
	public static final String B_CHANNEL = "B_CHANNEL";
	
	public static final String B_TIPO_RESPUESTA = "B_TIPO_RESPUESTA";
	public static final String B_COD_FUENTE = "B_COD_FUENTE";
	public static final String B_FLG_OPERACION = "B_FLG_OPERACION";
	public static final String B_RESULTADO = "B_RESULTADO";
	public static final String B_CREADO_POR = "B_CREADO_POR";
	public static final String B_MODIF_POR = "B_MODIF_POR";
	
	public static final String B_KEY1 = "B_KEY1";
	public static final String B_KEY2 = "B_KEY2";
	
	public static final String B_VALUE1 = "B_VALUE1";
	public static final String B_VALUE2 = "B_VALUE2";
	
	//TIPIFICACION REGISTRO INTENTO
	public static final String B_TIPO_SOLIC = "B_TIPO_SOLIC";
	public static final String B_MODULO = "B_MODULO";
	public static final String B_ORIG_ERROR = "B_ORIG_ERROR";
	public static final String B_FIN_FLUJO = "B_FIN_FLUJO";
	
	//VENTA TC PROVISIONAL
	public static final String B_IND_PROCESO = "B_IND_PROCESO";
	public static final String B_COD_OFIC_CAPTADORA = "B_COD_OFIC_CAPTADORA";
	public static final String B_COD_OFIC_GESTORA = "B_COD_OFIC_GESTORA";
	public static final String B_DISENO_PLASTICO = "B_DISENO_PLASTICO";
	public static final String B_IND_DOBLE_MONEDA = "B_IND_DOBLE_MONEDA";
	public static final String B_COD_FORMA_PAGO = "B_COD_FORMA_PAGO";
	public static final String B_IND_VAL_COD_UNICO = "B_IND_VAL_COD_UNICO";
	public static final String B_COD_TIPO_GESTION_ALTA = "B_COD_TIPO_GESTION_ALTA"; //
	public static final String B_COD_CONVENIO = "B_COD_CONVENIO";
	public static final String B_IND_GARANTIA = "B_IND_GARANTIA";
	public static final String B_IND_DEVOL_CASHBACK = "B_IND_DEVOL_CASHBACK";
	public static final String B_COD_TIPO_INTEGR = "B_COD_TIPO_INTEGR";
	public static final String B_COD_NACIONALIDAD = "B_COD_NACIONALIDAD";
	public static final String B_COD_PAIS_NACIMIENTO = "B_COD_PAIS_NACIMIENTO";
	public static final String B_COD_PAIS_NACIONALIDAD = "B_COD_PAIS_NACIONALIDAD";	
	public static final String B_COD_PAIS_RESIDENCIA = "B_COD_PAIS_RESIDENCIA";
	public static final String B_TOTAL_OTROS_INGRESOS = "B_TOTAL_OTROS_INGRESOS";
	public static final String B_TIPO_RPTA = "B_TIPO_RPTA";
	public static final String B_FLAG_NUEVO_BUS = "B_FLAG_NUEVO_BUS";
	public static final String B_FLAG_OPERACION = "B_COD_PAIS_NACIONALIDAD";
	public static final String B_COD_PLAN = "B_COD_PLAN";
	public static final String B_COD_FREC_PAGO = "B_COD_FREC_PAGO";
	public static final String B_COD_MEDIO_PAGO = "B_COD_MEDIO_PAGO";
	public static final String B_TIPO_MEDIO_PAGO = "B_TIPO_MEDIO_PAGO";
	public static final String B_MONTO_PRIMA = "B_MONTO_PRIMA";
	public static final String B_MONEDA_PRIMA = "B_MONEDA_PRIMA";		
	public static final String B_FLAG_ACT_DIRECCION = "B_MONEDA_PRIMA";
	public static final String B_FLAG_CLIENTE = "B_FLAG_CLIENTE";
	public static final String B_FLAG_LPD = "B_FLAG_LPD";
	public static final String B_TIPO_CONSENT = "B_TIPO_CONSENT";	
	public static final String B_COD_TIPO_GESTION_ADQ = "B_COD_TIPO_GESTION_ADQ"; //
	public static final String B_COD_ESTADO_PROCESO = "B_COD_ESTADO_PROCESO";
	public static final String B_COD_PROCESO = "B_COD_PROCESO";	
	public static final String B_COD_SUB_PROCESO = "B_COD_SUB_PROCESO";
	public static final String B_COD_TIPO_TRAMITE = "B_COD_TIPO_TRAMITE";
	public static final String B_INDICADOR = "B_INDICADOR";	
	public static final String B_TIPO_DIRECCION = "B_TIPO_DIRECCION";
	public static final String B_TIPO_MENSAJE = "B_TIPO_MENSAJE";
	public static final String B_COD_TRANSACCION = "B_COD_TRANSACCION";//
	public static final String B_REVERSO = "B_REVERSO";
	public static final String B_CONDIC_PTO_VENTA = "B_CONDIC_PTO_VENTA";
	public static final String B_COD_RPTA = "B_COD_RPTA";	
	public static final String B_COD_SERVICIO = "B_COD_SERVICIO";	
	public static final String B_COD_PROCEDENCIA = "B_COD_PROCEDENCIA";
	public static final String B_CUENTA_ORIGEN = "B_CUENTA_ORIGEN";
	public static final String B_SUCUR_APERT_DEST = "B_SUCUR_APERT_DEST";
	public static final String B_STATUS_CUEN_PROD = "B_STATUS_CUEN_PROD";
	public static final String B_ACCION = "B_ACCION";	
	public static final String B_RAZON_RPTA = "B_RAZON_RPTA";
	public static final String B_NOM_COMERCIO = "B_NOM_COMERCIO";
	public static final String B_LOCAL_COMERCIO = "B_LOCAL_COMERCIO";
	public static final String B_PAIS_ORIGEN = "B_PAIS_ORIGEN";	
	public static final String B_COD_CIUDAD = "B_COD_CIUDAD";
	public static final String B_COD_GIRO = "B_COD_GIRO";
	public static final String B_PATRIMONIO = "B_PATRIMONIO";
	public static final String B_TOT_PATRIMONIO = "B_TOT_PATRIMONIO";
	public static final String B_COD_TIPO_CONTRATO = "B_COD_TIPO_CONTRATO";
	
	//CONSULTAR POTC
	public static final String B_DOMINIO = "B_DOMINIO";
	public static final String B_VERSION = "B_VERSION";
	
	//GENERAR POTC
	public static final String B_FLAG_NUEVO_CLIE = "B_FLAG_NUEVO_CLIE";
	public static final String B_FLAG_NUEVO_PROD = "B_FLAG_NUEVO_PROD";
	public static final String B_ID_PRODUCTO = "B_ID_PRODUCTO";
	public static final String B_ID_PROD_USUARIO = "B_ID_PROD_USUARIO";
	public static final String B_TIPO_POTC = "B_TIPO_POTC";
	
	
	public static final String B_IND_COMP_DEUDA = "B_IND_COMP_DEUDA";
	public static final String B_IND_PROD_PRIN = "B_IND_PROD_PRIN";
	public static final String B_IND_AMA_CASA = "B_IND_AMA_CASA";
	public static final String B_IND_MI_TAXI = "B_IND_MI_TAXI";
	public static final String B_IND_AMP_PLAZO = "B_IND_AMP_PLAZO";
	public static final String B_IND_RED_PLAZO = "B_IND_RED_PLAZO";
	public static final String B_IND_NOVIOS = "B_IND_NOVIOS";
	public static final String B_IND_AHOR_CASA = "B_IND_AHOR_CASA";
	public static final String B_IND_SIN_FIRMA = "B_IND_SIN_FIRMA";
	public static final String B_IND_LIC = "B_IND_LIC";
	public static final String B_LIB_CAMP2 = "B_LIB_CAMP2";
	public static final String B_LIB_CAMP3 = "B_LIB_CAMP3";
	public static final String B_LIB_CAMP4 = "B_LIB_CAMP4";
	public static final String B_LIB_CAMP5 = "B_LIB_CAMP5";
	public static final String B_IND_CAMP = "B_IND_CAMP";
	public static final String B_MONTO_PROD = "B_MONTO_PROD";
	public static final String B_TI_RES_PER_ANIO = "B_TI_RES_PER_ANIO";
	public static final String B_TI_RES_PER_MES = "B_TI_RES_PER_MES";
	public static final String B_COD_PAIS = "B_COD_PAIS";
	public static final String B_MONEDA_CAMP = "B_MONEDA_CAMP"; 
	
	public static final String B_TIPO_EMAIL ="B_TIPO_EMAIL";
	public static final String B_ESTADO_EMAIL = "B_ESTADO_EMAIL";
	public static final String B_CODUSO_EMAIL = "B_CODUSO_EMAIL";
	public static final String B_CODUSO_DIREC = "B_CODUSO_DIREC";
	public static final String B_ESTADO_DIREC = "B_ESTADO_DIREC";
	public static final String B_TIPO_DIREC = "B_TIPO_DIREC";
	public static final String B_EQUIPO_CLIENTE = "B_EQUIPO_CLIENTE";
	public static final String B_NIV_CONF_USU = "B_NIV_CONF_USU";
	public static final String B_COD_EMISOR_PROD = "B_COD_EMISOR_PROD";
	public static final String B_TIPO_PROD = "B_TIPO_PROD";
	public static final String B_NUM_TRANSACCION = "B_NUM_TRANSACCION";
	public static final String B_DETALLE_PROD = "B_DETALLE_PROD";
	public static final String B_NOM_EMISOR_PROD = "B_NOM_EMISOR_PROD";
	
	public static final String B_NUM_CUENTA = "B_NUM_CUENTA";
	
	/*BODY WS ENVIAR NOTIFICACION*/
	public static final String B_ID = "B_ID";
	public static final String B_TYPE = "B_TYPE";
	public static final String B_APPSOURCE = "B_APPSOURCE";
	public static final String B_IDENTITY = "B_IDENTITY";
	public static final String B_CREDENTIAL = "B_CREDENTIAL";
	public static final String B_TEMPLATE = "B_TEMPLATE"; 
}
