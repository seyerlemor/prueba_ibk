package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pe.com.ibk.pepper.model.Cliente;
import pe.com.ibk.pepper.service.ClienteLocalServiceUtil;
import pe.com.ibk.pepper.util.DatesUtil;

public class ClienteFinderImpl extends BasePersistenceImpl<Cliente> implements ClienteFinder{
	
	
	public static final String FIND_F_F_N_A_D = "ResultadoDatosCliente.obtenerDatosCliente";
	public static final String COUNT_F_F_N_A_D = "ResultadoDatosCliente.countObtenerDatosCliente";
	
	private static final Log _log = LogFactoryUtil.getLog(ClienteFinderImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> findByF_F_N_A_D(String fechaDesde, String fechaHasta, String nombres, 
			String apellidos, String DNI, int start, int end) throws SystemException {

		Session session = null;
		List<Object[]> out = null;
		List<Cliente> lstClientes = null;
		try {
			_log.debug("::ClienteFinderImpl::>"+fechaDesde+"////"+fechaHasta);
			session = openSession();

			String sql = CustomSQLUtil.get(FIND_F_F_N_A_D);
			
			sql = CustomSQLUtil.replaceAndOperator(sql, Boolean.TRUE);
			
			if (Validator.isNotNull(nombres)) {
				sql = StringUtil.replace(sql, "[$AND_OR_NULL_CHECK_NOMBRE$]", StringPool.BLANK);
			}
			
			if (Validator.isNotNull(apellidos)) {
				sql = StringUtil.replace(sql, "[$AND_OR_NULL_CHECK_APPELIDO$]", StringPool.BLANK);
			}
			
			if (Validator.isNotNull(DNI)) {
				sql = StringUtil.replace(sql, "[$AND_OR_NULL_CHECK_DOCUMENTO$]", StringPool.BLANK);
			}
			
			sql = StringUtil.replace(sql, new String[]{"[$AND_OR_NULL_CHECK_NOMBRE$]", "[$AND_OR_NULL_CHECK_APPELIDO$]", "[$AND_OR_NULL_CHECK_DOCUMENTO$]"}, 
					new String[]{"or ? IS NOT NULL","or ? IS NOT NULL","or ? IS NOT NULL"});
			
			SQLQuery q = session.createSQLQuery(sql);
			q.addScalar("xpdt_IdExpediente", Type.STRING);
			q.addScalar("dtcl_IdDatoCliente", Type.STRING);
			q.addScalar("dtcl_TipoDocumento", Type.STRING);
			q.addScalar("dtcl_PrimerNombre", Type.STRING);
			q.addScalar("dtcl_SegundoNombre", Type.STRING);
			q.addScalar("dtcl_ApellidoPaterno", Type.STRING);
			q.addScalar("dtcl_ApellidoMaterno", Type.STRING);
			q.addScalar("dtcl_Sexo", Type.STRING);
			q.addScalar("dtcl_Correo", Type.STRING);
			q.addScalar("dtcl_Telefono", Type.STRING);
			q.addScalar("dtcl_EstadoCivil", Type.STRING);
			q.addScalar("dtcl_SituacionLaboral", Type.STRING);
			q.addScalar("dtcl_RucEmpresaTrabajo", Type.STRING);
			q.addScalar("dtcl_NivelEducacion", Type.STRING);
			q.addScalar("dtcl_Ocupacion", Type.STRING);
			q.addScalar("dtcl_CargoActual", Type.STRING);
			q.addScalar("dtcl_ActividadNegocio", Type.STRING);
			q.addScalar("dtcl_AntiguedadLaboral", Type.STRING);
			q.addScalar("dtcl_PorcentajeParticipacion", Type.STRING);
			q.addScalar("dtcl_IngresoMensualFijo", Type.STRING);
			q.addScalar("dtcl_IngresoMensualVariable", Type.STRING);
			q.addScalar("dtcl_Documento", Type.STRING);
			q.addScalar("dtcl_Terminos", Type.STRING);
			q.addScalar("dtcl_FlagPEP", Type.STRING);
			q.addScalar("dtcl_Operador", Type.STRING);
			q.addScalar("dtcl_FechaRegistro", Type.STRING);
			q.addScalar("dtcl_FechaNacimiento", Type.STRING);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add("%"+nombres+"%");
			if (Validator.isNull(nombres)) {
				qPos.add(nombres);
			}
			
			qPos.add("%"+apellidos+"%");
			if (Validator.isNull(apellidos)) {
				qPos.add(apellidos);
			}
			qPos.add(DNI+"%");
			if (Validator.isNull(DNI)) {
				qPos.add(DNI);
			}

//			Calendar calendar = Calendar.getInstance();
//			calendar.setTime(fechaHasta); 
//			calendar.add(Calendar.DAY_OF_YEAR, 1);			
//			fechaHasta = calendar.getTime();
//
//			qPos.add(fechaDesde);
//			qPos.add(fechaHasta);
			
			qPos.add(fechaDesde.concat(" 00:00:00"));
			qPos.add(fechaHasta.concat(" 23:59:59"));
			
			/**/
			qPos.add("%"+nombres+"%");
			if (Validator.isNull(nombres)) {
				qPos.add(nombres);
			}
			
			qPos.add("%"+apellidos+"%");
			if (Validator.isNull(apellidos)) {
				qPos.add(apellidos);
			}
			qPos.add(DNI+"%");
			if (Validator.isNull(DNI)) {
				qPos.add(DNI);
			}

//			calendar.setTime(fechaHasta); 
//			calendar.add(Calendar.DAY_OF_YEAR, 1);			
//			fechaHasta = calendar.getTime();
//
//			qPos.add(fechaDesde);
//			qPos.add(fechaHasta);
			
			qPos.add(fechaDesde.concat(" 00:00:00"));
			qPos.add(fechaHasta.concat(" 23:59:59"));
			/**/
			out = new ArrayList<>();
			out = (List<Object[]>)QueryUtil.list(q, getDialect(), start, end);
			lstClientes = new ArrayList<>();
			
			for(Object[] lista:out){
				Cliente cliente  = ClienteLocalServiceUtil.createCliente(-1);
				cliente.setIdExpediente(Long.parseLong((String)lista[0]));
				cliente.setIdDatoCliente(Long.parseLong((String)lista[1]));
				cliente.setTipoDocumento((String)lista[2]);
				cliente.setPrimerNombre((String)lista[3]);
				cliente.setSegundoNombre((String)lista[4]);
				cliente.setApellidoPaterno((String)lista[5]);
				cliente.setApellidoMaterno((String)lista[6]);
				cliente.setSexo((String)lista[7]);
				cliente.setCorreo((String)lista[8]);
				cliente.setTelefono((String)lista[9]);
				cliente.setEstadoCivil((String)lista[10]);
				cliente.setSituacionLaboral((String)lista[11]);
				cliente.setRucEmpresaTrabajo((String)lista[12]);
				cliente.setNivelEducacion((String)lista[13]);
				cliente.setOcupacion((String)lista[14]);
				cliente.setCargoActual((String)lista[15]);
				cliente.setActividadNegocio((String)lista[16]);
				cliente.setAntiguedadLaboral((String)lista[17]);
				cliente.setPorcentajeParticipacion((String)lista[18]);
				cliente.setIngresoMensualFijo((String)lista[19]);
				cliente.setIngresoMensualVariable((String)lista[20]);
				cliente.setDocumento((String)lista[21]);
				cliente.setTerminosCondiciones((String)lista[22]);
				cliente.setFlagPEP((String)lista[23]);
				cliente.setOperador((String)lista[24]);
				cliente.setFechaRegistro(Validator.isNotNull((String)lista[25])?DatesUtil.convertSringToDateReport((String)lista[25]):null);
				cliente.setFechaNacimiento(Validator.isNotNull((String)lista[26])?DatesUtil.convertSringToDateReport((String)lista[26]):null);
				lstClientes.add(cliente);
			}
		}
		catch (Exception e) {
			_log.error("Error en findByF_F_N_A_D::", e);
		}
		finally {
			closeSession(session);
		}
		return lstClientes;
	}
	
	public int countByF_F_N_A_D(String fechaDesde, String fechaHasta, String nombres, 
			String apellidos, String DNI) throws SystemException {

		Session session = null;

		try {
			session = openSession();

			String sql = CustomSQLUtil.get(COUNT_F_F_N_A_D);
			
			sql = CustomSQLUtil.replaceAndOperator(sql, Boolean.TRUE);
			
			if (Validator.isNotNull(nombres)) {
				sql = StringUtil.replace(sql, "[$AND_OR_NULL_CHECK_NOMBRE$]", StringPool.BLANK);
			}
			
			if (Validator.isNotNull(apellidos)) {
				sql = StringUtil.replace(sql, "[$AND_OR_NULL_CHECK_APPELIDO$]", StringPool.BLANK);
			}
			
			if (Validator.isNotNull(DNI)) {
				sql = StringUtil.replace(sql, "[$AND_OR_NULL_CHECK_DOCUMENTO$]", StringPool.BLANK);
			}
			
			sql = StringUtil.replace(sql, new String[]{"[$AND_OR_NULL_CHECK_NOMBRE$]", "[$AND_OR_NULL_CHECK_APPELIDO$]", "[$AND_OR_NULL_CHECK_DOCUMENTO$]"}, 
					new String[]{"or ? IS NOT NULL","or ? IS NOT NULL","or ? IS NOT NULL"});
			
			SQLQuery q = session.createSQLQuery(sql);
			q.addScalar(COUNT_COLUMN_NAME, Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add("%"+nombres+"%");
			if (Validator.isNull(nombres)) {
				qPos.add(nombres);
			}
			
			qPos.add("%"+apellidos+"%");
			if (Validator.isNull(apellidos)) {
				qPos.add(apellidos);
			}
			qPos.add(DNI+"%");
			if (Validator.isNull(DNI)) {
				qPos.add(DNI);
			}
			qPos.add(fechaDesde.concat(" 00:00:00"));
			qPos.add(fechaHasta.concat(" 23:59:59"));
			/**/
			qPos.add("%"+nombres+"%");
			if (Validator.isNull(nombres)) {
				qPos.add(nombres);
			}
			
			qPos.add("%"+apellidos+"%");
			if (Validator.isNull(apellidos)) {
				qPos.add(apellidos);
			}
			qPos.add(DNI+"%");
			if (Validator.isNull(DNI)) {
				qPos.add(DNI);
			}
			qPos.add(fechaDesde.concat(" 00:00:00"));
			qPos.add(fechaHasta.concat(" 23:59:59"));
			/**/
			
			Iterator<Long> itr = q.iterate();

			if (itr.hasNext()) {
				Long count = itr.next();

				if (count != null) {
					return count.intValue();
				}
			}
			return 0;
		}
		catch (Exception e) {
			_log.error("Error en countByF_F_N_A_D::", e);
			return 0;
		}
		finally {
			closeSession(session);
		}
	}
	
	
}
