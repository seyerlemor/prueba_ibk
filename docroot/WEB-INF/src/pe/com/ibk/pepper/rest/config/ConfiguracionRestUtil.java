/**
 * 
 */
package pe.com.ibk.pepper.rest.config;

import com.liferay.portal.kernel.util.StringPool;

/**
 * @author XT6581
 *
 */
public class ConfiguracionRestUtil {

	
	public static String limpiarJsonResponse(String json) {
		
		String jsonResponse = "";
		
		jsonResponse = json.replaceAll(ConstantesRest.PATH_MSG_MESSAGE_RESPONSE, ConstantesRest.PATH_MSG_MESSAGE_RESPONSE_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_MSG_HEADER, ConstantesRest.PATH_MSG_HEADER_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_MSG_HEADER_RESPONSE, ConstantesRest.PATH_MSG_HEADER_RESPONSE_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.MSG_TYPE_BODY, ConstantesRest.MSG_TYPE_BODY_NEW);
		
		//Para el servicio REST Consulta Campania
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_CONSULTARLEADRESPONSE, ConstantesRest.PATH_BODY_CONSULTARLEADRESPONSE_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_DETAIL_CEM, ConstantesRest.PATH_BODY_DETAIL_CEM_NEW);
		//Lista Negra R1
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_VALIDAR_LISTA_NEGRA, ConstantesRest.PATH_BODY_RES_VALIDAR_LISTA_NEGRA_NEW);
		//Consultar Equifax
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_CONSULTA_PREGUNTAS_EQUIFAX, ConstantesRest.PATH_BODY_RES_CONSULTA_PREGUNTAS_EQUIFAX_NEW);
		//Validar Equifax
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_VALIDAR_PREGUNTAS_EQUIFAX, ConstantesRest.PATH_BODY_RES_VALIDAR_PREGUNTAS_EQUIFAX_NEW);
		//Obtener Informacion Persona
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBTENER_INFORMACION_PERSONA, ConstantesRest.PATH_BODY_RES_OBTENER_INFORMACION_PERSONA_NEW);
		
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_ACTUALIZAR_RESPUESTA, ConstantesRest.PATH_BODY_RES_ACTUALIZAR_RESPUESTA_NEW);
		
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_CONSULTAR_AFILIACION, ConstantesRest.PATH_BODY_RES_CONSULTAR_AFILIACION_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBTENER_TARJETA, ConstantesRest.PATH_BODY_RES_OBTENER_TARJETA_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_TARJETAS, ConstantesRest.PATH_BODY_RES_OBT_TARJ_TARJETAS_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_TARJETA, ConstantesRest.PATH_BODY_RES_OBT_TARJ_TARJETA_NEW);
		
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_TIPO, ConstantesRest.PATH_BODY_RES_OBT_TARJ_TIPO_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_NUMERO, ConstantesRest.PATH_BODY_RES_OBT_TARJ_NUMERO_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_INDTIPO, ConstantesRest.PATH_BODY_RES_OBT_TARJ_INDTIPO_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_ESTADO, ConstantesRest.PATH_BODY_RES_OBT_TARJ_ESTADO_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_FACTIV, ConstantesRest.PATH_BODY_RES_OBT_TARJ_FACTIV_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_FBLOQ, ConstantesRest.PATH_BODY_RES_OBT_TARJ_FBLOQ_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_FCANCEL, ConstantesRest.PATH_BODY_RES_OBT_TARJ_FCANCEL_NEW);
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_RES_OBT_TARJ_CALPART, ConstantesRest.PATH_BODY_RES_OBT_TARJ_CALPART_NEW);
		
		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_CONTENT_TYPE, ConstantesRest.PATH_BODY_CONTENT_TYPE_NEW);
//		jsonResponse = jsonResponse.replaceAll(ConstantesRest.PATH_BODY_DATA, ConstantesRest.PATH_BODY_DATA_NEW);
		jsonResponse = jsonResponse.replaceAll("_  ", "");
		return jsonResponse;
	}
	
	public static String limpiarJsonRequest(String json) {
		
		String jsonRequest = "";
		
		//De Minusculas a Mayusculas 
		/**
		 */
		jsonRequest = json.replaceAll(ConstantesRest.PATH_MSG_MESSAGE_REQUEST_NEW, ConstantesRest.PATH_MSG_MESSAGE_REQUEST);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_MSG_HEADER_NEW, ConstantesRest.PATH_MSG_HEADER);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_MSG_HEADER_REQUEST_NEW, ConstantesRest.PATH_MSG_HEADER_REQUEST);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.MSG_TYPE_BODY_NEW, ConstantesRest.MSG_TYPE_BODY);
		
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_CONSULTA_CAMPANIA_NEW, ConstantesRest.PATH_BODY_CONSULTA_CAMPANIA);
		jsonRequest = jsonRequest.replaceAll(",\"direccion\":null", StringPool.BLANK);
		//Consultar Equifax
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_CONSULTA_PREGUNTAS_EQUIFAX_NEW, ConstantesRest.PATH_BODY_CONSULTA_PREGUNTAS_EQUIFAX);
		
		//Validar Equifax
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_VALIDAR_PREGUNTAS_EQUIFAX_NEW, ConstantesRest.PATH_BODY_VALIDAR_PREGUNTAS_EQUIFAX);
		
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_1, ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_NEW);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_2, ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_NEW);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_3, ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_NEW);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_4, ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_NEW);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_5, ConstantesRest.PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_NEW);
		
		//Obtener Informacion Persona
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_OBTENER_INFORMACION_PERSONA_NEW, ConstantesRest.PATH_BODY_OBTENER_INFORMACION_PERSONA);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_CONSULTAR_AFILIACION_NEW, ConstantesRest.PATH_BODY_CONSULTAR_AFILIACION);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_ACTUALIZAR_RESPUESTA_NEW, ConstantesRest.PATH_BODY_ACTUALIZAR_RESPUESTA);
		
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_VENTA_TC_NULO_NEW, ConstantesRest.PATH_BODY_VENTA_TC_NULO);	
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_OBTENER_TARJETA_NEW, ConstantesRest.PATH_BODY_OBTENER_TARJETA);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_ESTADO_TARJETA_NEW, ConstantesRest.PATH_BODY_ESTADO_TARJETA);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_COD_UNICO_NEW, ConstantesRest.PATH_BODY_COD_UNICO);
		
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_HTTP, ConstantesRest.PATH_BODY_HTTP);
		jsonRequest = jsonRequest.replaceAll(ConstantesRest.PATH_BODY_DATA_NEW, ConstantesRest.PATH_BODY_DATA);
		return jsonRequest;
	}
		
	
}
