package pe.com.ibk.pepper.rest.bancasms.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EnrollmentInfo  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@SerializedName("phone")
	private Cellphone phone;

	public Cellphone getPhone() {
		return phone;
	}

	public void setPhone(Cellphone phone) {
		this.phone = phone;
	}

}
