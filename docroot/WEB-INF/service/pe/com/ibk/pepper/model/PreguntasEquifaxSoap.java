/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class PreguntasEquifaxSoap implements Serializable {
	public static PreguntasEquifaxSoap toSoapModel(PreguntasEquifax model) {
		PreguntasEquifaxSoap soapModel = new PreguntasEquifaxSoap();

		soapModel.setIdPregunta(model.getIdPregunta());
		soapModel.setIdCabecera(model.getIdCabecera());
		soapModel.setCodigoPregunta(model.getCodigoPregunta());
		soapModel.setDescripPregunta(model.getDescripPregunta());
		soapModel.setCodigoRespuesta(model.getCodigoRespuesta());
		soapModel.setDescripRespuesta(model.getDescripRespuesta());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static PreguntasEquifaxSoap[] toSoapModels(PreguntasEquifax[] models) {
		PreguntasEquifaxSoap[] soapModels = new PreguntasEquifaxSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PreguntasEquifaxSoap[][] toSoapModels(
		PreguntasEquifax[][] models) {
		PreguntasEquifaxSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PreguntasEquifaxSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PreguntasEquifaxSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PreguntasEquifaxSoap[] toSoapModels(
		List<PreguntasEquifax> models) {
		List<PreguntasEquifaxSoap> soapModels = new ArrayList<PreguntasEquifaxSoap>(models.size());

		for (PreguntasEquifax model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PreguntasEquifaxSoap[soapModels.size()]);
	}

	public PreguntasEquifaxSoap() {
	}

	public long getPrimaryKey() {
		return _idPregunta;
	}

	public void setPrimaryKey(long pk) {
		setIdPregunta(pk);
	}

	public long getIdPregunta() {
		return _idPregunta;
	}

	public void setIdPregunta(long idPregunta) {
		_idPregunta = idPregunta;
	}

	public long getIdCabecera() {
		return _idCabecera;
	}

	public void setIdCabecera(long idCabecera) {
		_idCabecera = idCabecera;
	}

	public String getCodigoPregunta() {
		return _codigoPregunta;
	}

	public void setCodigoPregunta(String codigoPregunta) {
		_codigoPregunta = codigoPregunta;
	}

	public String getDescripPregunta() {
		return _descripPregunta;
	}

	public void setDescripPregunta(String descripPregunta) {
		_descripPregunta = descripPregunta;
	}

	public String getCodigoRespuesta() {
		return _codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		_codigoRespuesta = codigoRespuesta;
	}

	public String getDescripRespuesta() {
		return _descripRespuesta;
	}

	public void setDescripRespuesta(String descripRespuesta) {
		_descripRespuesta = descripRespuesta;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private long _idPregunta;
	private long _idCabecera;
	private String _codigoPregunta;
	private String _descripPregunta;
	private String _codigoRespuesta;
	private String _descripRespuesta;
	private Date _fechaRegistro;
}