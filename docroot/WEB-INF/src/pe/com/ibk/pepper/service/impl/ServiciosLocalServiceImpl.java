/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;
import java.util.List;

import pe.com.ibk.pepper.NoSuchServiciosException;
import pe.com.ibk.pepper.model.Servicios;
import pe.com.ibk.pepper.service.base.ServiciosLocalServiceBaseImpl;

/**
 * The implementation of the servicios local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.ServiciosLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.ServiciosLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.ServiciosLocalServiceUtil
 */
public class ServiciosLocalServiceImpl extends ServiciosLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.ServiciosLocalServiceUtil} to access the servicios local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(ServiciosLocalServiceImpl.class);
	
	@Override
	public Servicios registrarServicios(Servicios servicios) throws SystemException{
		Date now = DateUtil.newDate();
		if (servicios.isNew()) {
			servicios.setFechaRegistro(now);
		}
		serviciosPersistence.update(servicios);
		return servicios;
	}	

	@Override
	public Servicios buscarServiciosporId(long idServicios){
		Servicios servicios = null;
		try {
			servicios = serviciosPersistence.findByS_ID(idServicios);
		} catch (NoSuchServiciosException e) {
			_log.error("Error en clase ServicioLocalServiceImpl metodo buscarServicioporId:: NO EXISTE EN SERVICIOS ", e );
		} catch (SystemException e) {
			_log.error("Error en clase ServicioLocalServiceImpl metodo buscarServicioporId:: ", e );
		}
		return servicios;
	}		
	
	public List<Servicios> getServiciosByExpediente(long idExpediente, String tipoFlujo) throws SystemException{
		return serviciosPersistence.findByIdExpediente(idExpediente, tipoFlujo);
	}
	
}