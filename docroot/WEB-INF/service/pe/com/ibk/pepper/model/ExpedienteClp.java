/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.ExpedienteLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ExpedienteClp extends BaseModelImpl<Expediente>
	implements Expediente {
	public ExpedienteClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Expediente.class;
	}

	@Override
	public String getModelClassName() {
		return Expediente.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idExpediente;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdExpediente(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idExpediente;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idExpediente", getIdExpediente());
		attributes.put("documento", getDocumento());
		attributes.put("estado", getEstado());
		attributes.put("paso", getPaso());
		attributes.put("pagina", getPagina());
		attributes.put("tipoFlujo", getTipoFlujo());
		attributes.put("flagR1", getFlagR1());
		attributes.put("flagCampania", getFlagCampania());
		attributes.put("flagReniec", getFlagReniec());
		attributes.put("flagEquifax", getFlagEquifax());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("tipoCampania", getTipoCampania());
		attributes.put("flagCalificacion", getFlagCalificacion());
		attributes.put("flagCliente", getFlagCliente());
		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("strJson", getStrJson());
		attributes.put("formHtml", getFormHtml());
		attributes.put("codigoHtml", getCodigoHtml());
		attributes.put("periodoRegistro", getPeriodoRegistro());
		attributes.put("modificacion", getModificacion());
		attributes.put("flagTipificacion", getFlagTipificacion());
		attributes.put("flagActualizarRespuesta", getFlagActualizarRespuesta());
		attributes.put("flagObtenerInformacion", getFlagObtenerInformacion());
		attributes.put("flagConsultarAfiliacion", getFlagConsultarAfiliacion());
		attributes.put("flagGeneracionToken", getFlagGeneracionToken());
		attributes.put("flagVentaTC", getFlagVentaTC());
		attributes.put("flagValidacionToken", getFlagValidacionToken());
		attributes.put("flagRecompraGenPOTC", getFlagRecompraGenPOTC());
		attributes.put("flagRecompraConPOTC", getFlagRecompraConPOTC());
		attributes.put("flagExtornoGenPOTC", getFlagExtornoGenPOTC());
		attributes.put("flagExtornoConPOTC", getFlagExtornoConPOTC());
		attributes.put("ip", getIp());
		attributes.put("navegador", getNavegador());
		attributes.put("pepperType", getPepperType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String documento = (String)attributes.get("documento");

		if (documento != null) {
			setDocumento(documento);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String paso = (String)attributes.get("paso");

		if (paso != null) {
			setPaso(paso);
		}

		String pagina = (String)attributes.get("pagina");

		if (pagina != null) {
			setPagina(pagina);
		}

		String tipoFlujo = (String)attributes.get("tipoFlujo");

		if (tipoFlujo != null) {
			setTipoFlujo(tipoFlujo);
		}

		String flagR1 = (String)attributes.get("flagR1");

		if (flagR1 != null) {
			setFlagR1(flagR1);
		}

		String flagCampania = (String)attributes.get("flagCampania");

		if (flagCampania != null) {
			setFlagCampania(flagCampania);
		}

		String flagReniec = (String)attributes.get("flagReniec");

		if (flagReniec != null) {
			setFlagReniec(flagReniec);
		}

		String flagEquifax = (String)attributes.get("flagEquifax");

		if (flagEquifax != null) {
			setFlagEquifax(flagEquifax);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String tipoCampania = (String)attributes.get("tipoCampania");

		if (tipoCampania != null) {
			setTipoCampania(tipoCampania);
		}

		String flagCalificacion = (String)attributes.get("flagCalificacion");

		if (flagCalificacion != null) {
			setFlagCalificacion(flagCalificacion);
		}

		String flagCliente = (String)attributes.get("flagCliente");

		if (flagCliente != null) {
			setFlagCliente(flagCliente);
		}

		Integer idUsuarioSession = (Integer)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		String strJson = (String)attributes.get("strJson");

		if (strJson != null) {
			setStrJson(strJson);
		}

		String formHtml = (String)attributes.get("formHtml");

		if (formHtml != null) {
			setFormHtml(formHtml);
		}

		String codigoHtml = (String)attributes.get("codigoHtml");

		if (codigoHtml != null) {
			setCodigoHtml(codigoHtml);
		}

		String periodoRegistro = (String)attributes.get("periodoRegistro");

		if (periodoRegistro != null) {
			setPeriodoRegistro(periodoRegistro);
		}

		Date modificacion = (Date)attributes.get("modificacion");

		if (modificacion != null) {
			setModificacion(modificacion);
		}

		String flagTipificacion = (String)attributes.get("flagTipificacion");

		if (flagTipificacion != null) {
			setFlagTipificacion(flagTipificacion);
		}

		String flagActualizarRespuesta = (String)attributes.get(
				"flagActualizarRespuesta");

		if (flagActualizarRespuesta != null) {
			setFlagActualizarRespuesta(flagActualizarRespuesta);
		}

		String flagObtenerInformacion = (String)attributes.get(
				"flagObtenerInformacion");

		if (flagObtenerInformacion != null) {
			setFlagObtenerInformacion(flagObtenerInformacion);
		}

		String flagConsultarAfiliacion = (String)attributes.get(
				"flagConsultarAfiliacion");

		if (flagConsultarAfiliacion != null) {
			setFlagConsultarAfiliacion(flagConsultarAfiliacion);
		}

		String flagGeneracionToken = (String)attributes.get(
				"flagGeneracionToken");

		if (flagGeneracionToken != null) {
			setFlagGeneracionToken(flagGeneracionToken);
		}

		String flagVentaTC = (String)attributes.get("flagVentaTC");

		if (flagVentaTC != null) {
			setFlagVentaTC(flagVentaTC);
		}

		String flagValidacionToken = (String)attributes.get(
				"flagValidacionToken");

		if (flagValidacionToken != null) {
			setFlagValidacionToken(flagValidacionToken);
		}

		String flagRecompraGenPOTC = (String)attributes.get(
				"flagRecompraGenPOTC");

		if (flagRecompraGenPOTC != null) {
			setFlagRecompraGenPOTC(flagRecompraGenPOTC);
		}

		String flagRecompraConPOTC = (String)attributes.get(
				"flagRecompraConPOTC");

		if (flagRecompraConPOTC != null) {
			setFlagRecompraConPOTC(flagRecompraConPOTC);
		}

		String flagExtornoGenPOTC = (String)attributes.get("flagExtornoGenPOTC");

		if (flagExtornoGenPOTC != null) {
			setFlagExtornoGenPOTC(flagExtornoGenPOTC);
		}

		String flagExtornoConPOTC = (String)attributes.get("flagExtornoConPOTC");

		if (flagExtornoConPOTC != null) {
			setFlagExtornoConPOTC(flagExtornoConPOTC);
		}

		String ip = (String)attributes.get("ip");

		if (ip != null) {
			setIp(ip);
		}

		String navegador = (String)attributes.get("navegador");

		if (navegador != null) {
			setNavegador(navegador);
		}

		String pepperType = (String)attributes.get("pepperType");

		if (pepperType != null) {
			setPepperType(pepperType);
		}
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdExpediente", long.class);

				method.invoke(_expedienteRemoteModel, idExpediente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDocumento() {
		return _documento;
	}

	@Override
	public void setDocumento(String documento) {
		_documento = documento;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setDocumento", String.class);

				method.invoke(_expedienteRemoteModel, documento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstado() {
		return _estado;
	}

	@Override
	public void setEstado(String estado) {
		_estado = estado;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", String.class);

				method.invoke(_expedienteRemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPaso() {
		return _paso;
	}

	@Override
	public void setPaso(String paso) {
		_paso = paso;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPaso", String.class);

				method.invoke(_expedienteRemoteModel, paso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPagina() {
		return _pagina;
	}

	@Override
	public void setPagina(String pagina) {
		_pagina = pagina;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPagina", String.class);

				method.invoke(_expedienteRemoteModel, pagina);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoFlujo() {
		return _tipoFlujo;
	}

	@Override
	public void setTipoFlujo(String tipoFlujo) {
		_tipoFlujo = tipoFlujo;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoFlujo", String.class);

				method.invoke(_expedienteRemoteModel, tipoFlujo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagR1() {
		return _flagR1;
	}

	@Override
	public void setFlagR1(String flagR1) {
		_flagR1 = flagR1;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagR1", String.class);

				method.invoke(_expedienteRemoteModel, flagR1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagCampania() {
		return _flagCampania;
	}

	@Override
	public void setFlagCampania(String flagCampania) {
		_flagCampania = flagCampania;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagCampania", String.class);

				method.invoke(_expedienteRemoteModel, flagCampania);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagReniec() {
		return _flagReniec;
	}

	@Override
	public void setFlagReniec(String flagReniec) {
		_flagReniec = flagReniec;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagReniec", String.class);

				method.invoke(_expedienteRemoteModel, flagReniec);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagEquifax() {
		return _flagEquifax;
	}

	@Override
	public void setFlagEquifax(String flagEquifax) {
		_flagEquifax = flagEquifax;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagEquifax", String.class);

				method.invoke(_expedienteRemoteModel, flagEquifax);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_expedienteRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoCampania() {
		return _tipoCampania;
	}

	@Override
	public void setTipoCampania(String tipoCampania) {
		_tipoCampania = tipoCampania;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoCampania", String.class);

				method.invoke(_expedienteRemoteModel, tipoCampania);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagCalificacion() {
		return _flagCalificacion;
	}

	@Override
	public void setFlagCalificacion(String flagCalificacion) {
		_flagCalificacion = flagCalificacion;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagCalificacion",
						String.class);

				method.invoke(_expedienteRemoteModel, flagCalificacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagCliente() {
		return _flagCliente;
	}

	@Override
	public void setFlagCliente(String flagCliente) {
		_flagCliente = flagCliente;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagCliente", String.class);

				method.invoke(_expedienteRemoteModel, flagCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	@Override
	public void setIdUsuarioSession(int idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdUsuarioSession", int.class);

				method.invoke(_expedienteRemoteModel, idUsuarioSession);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStrJson() {
		return _strJson;
	}

	@Override
	public void setStrJson(String strJson) {
		_strJson = strJson;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setStrJson", String.class);

				method.invoke(_expedienteRemoteModel, strJson);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFormHtml() {
		return _formHtml;
	}

	@Override
	public void setFormHtml(String formHtml) {
		_formHtml = formHtml;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFormHtml", String.class);

				method.invoke(_expedienteRemoteModel, formHtml);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoHtml() {
		return _codigoHtml;
	}

	@Override
	public void setCodigoHtml(String codigoHtml) {
		_codigoHtml = codigoHtml;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoHtml", String.class);

				method.invoke(_expedienteRemoteModel, codigoHtml);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPeriodoRegistro() {
		return _periodoRegistro;
	}

	@Override
	public void setPeriodoRegistro(String periodoRegistro) {
		_periodoRegistro = periodoRegistro;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPeriodoRegistro",
						String.class);

				method.invoke(_expedienteRemoteModel, periodoRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getModificacion() {
		return _modificacion;
	}

	@Override
	public void setModificacion(Date modificacion) {
		_modificacion = modificacion;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setModificacion", Date.class);

				method.invoke(_expedienteRemoteModel, modificacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagTipificacion() {
		return _flagTipificacion;
	}

	@Override
	public void setFlagTipificacion(String flagTipificacion) {
		_flagTipificacion = flagTipificacion;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagTipificacion",
						String.class);

				method.invoke(_expedienteRemoteModel, flagTipificacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagActualizarRespuesta() {
		return _flagActualizarRespuesta;
	}

	@Override
	public void setFlagActualizarRespuesta(String flagActualizarRespuesta) {
		_flagActualizarRespuesta = flagActualizarRespuesta;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagActualizarRespuesta",
						String.class);

				method.invoke(_expedienteRemoteModel, flagActualizarRespuesta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagObtenerInformacion() {
		return _flagObtenerInformacion;
	}

	@Override
	public void setFlagObtenerInformacion(String flagObtenerInformacion) {
		_flagObtenerInformacion = flagObtenerInformacion;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagObtenerInformacion",
						String.class);

				method.invoke(_expedienteRemoteModel, flagObtenerInformacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagConsultarAfiliacion() {
		return _flagConsultarAfiliacion;
	}

	@Override
	public void setFlagConsultarAfiliacion(String flagConsultarAfiliacion) {
		_flagConsultarAfiliacion = flagConsultarAfiliacion;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagConsultarAfiliacion",
						String.class);

				method.invoke(_expedienteRemoteModel, flagConsultarAfiliacion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagGeneracionToken() {
		return _flagGeneracionToken;
	}

	@Override
	public void setFlagGeneracionToken(String flagGeneracionToken) {
		_flagGeneracionToken = flagGeneracionToken;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagGeneracionToken",
						String.class);

				method.invoke(_expedienteRemoteModel, flagGeneracionToken);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagVentaTC() {
		return _flagVentaTC;
	}

	@Override
	public void setFlagVentaTC(String flagVentaTC) {
		_flagVentaTC = flagVentaTC;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagVentaTC", String.class);

				method.invoke(_expedienteRemoteModel, flagVentaTC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagValidacionToken() {
		return _flagValidacionToken;
	}

	@Override
	public void setFlagValidacionToken(String flagValidacionToken) {
		_flagValidacionToken = flagValidacionToken;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagValidacionToken",
						String.class);

				method.invoke(_expedienteRemoteModel, flagValidacionToken);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagRecompraGenPOTC() {
		return _flagRecompraGenPOTC;
	}

	@Override
	public void setFlagRecompraGenPOTC(String flagRecompraGenPOTC) {
		_flagRecompraGenPOTC = flagRecompraGenPOTC;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagRecompraGenPOTC",
						String.class);

				method.invoke(_expedienteRemoteModel, flagRecompraGenPOTC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagRecompraConPOTC() {
		return _flagRecompraConPOTC;
	}

	@Override
	public void setFlagRecompraConPOTC(String flagRecompraConPOTC) {
		_flagRecompraConPOTC = flagRecompraConPOTC;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagRecompraConPOTC",
						String.class);

				method.invoke(_expedienteRemoteModel, flagRecompraConPOTC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagExtornoGenPOTC() {
		return _flagExtornoGenPOTC;
	}

	@Override
	public void setFlagExtornoGenPOTC(String flagExtornoGenPOTC) {
		_flagExtornoGenPOTC = flagExtornoGenPOTC;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagExtornoGenPOTC",
						String.class);

				method.invoke(_expedienteRemoteModel, flagExtornoGenPOTC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagExtornoConPOTC() {
		return _flagExtornoConPOTC;
	}

	@Override
	public void setFlagExtornoConPOTC(String flagExtornoConPOTC) {
		_flagExtornoConPOTC = flagExtornoConPOTC;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagExtornoConPOTC",
						String.class);

				method.invoke(_expedienteRemoteModel, flagExtornoConPOTC);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIp() {
		return _ip;
	}

	@Override
	public void setIp(String ip) {
		_ip = ip;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIp", String.class);

				method.invoke(_expedienteRemoteModel, ip);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNavegador() {
		return _navegador;
	}

	@Override
	public void setNavegador(String navegador) {
		_navegador = navegador;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setNavegador", String.class);

				method.invoke(_expedienteRemoteModel, navegador);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPepperType() {
		return _pepperType;
	}

	@Override
	public void setPepperType(String pepperType) {
		_pepperType = pepperType;

		if (_expedienteRemoteModel != null) {
			try {
				Class<?> clazz = _expedienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPepperType", String.class);

				method.invoke(_expedienteRemoteModel, pepperType);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getExpedienteRemoteModel() {
		return _expedienteRemoteModel;
	}

	public void setExpedienteRemoteModel(BaseModel<?> expedienteRemoteModel) {
		_expedienteRemoteModel = expedienteRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _expedienteRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_expedienteRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ExpedienteLocalServiceUtil.addExpediente(this);
		}
		else {
			ExpedienteLocalServiceUtil.updateExpediente(this);
		}
	}

	@Override
	public Expediente toEscapedModel() {
		return (Expediente)ProxyUtil.newProxyInstance(Expediente.class.getClassLoader(),
			new Class[] { Expediente.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ExpedienteClp clone = new ExpedienteClp();

		clone.setIdExpediente(getIdExpediente());
		clone.setDocumento(getDocumento());
		clone.setEstado(getEstado());
		clone.setPaso(getPaso());
		clone.setPagina(getPagina());
		clone.setTipoFlujo(getTipoFlujo());
		clone.setFlagR1(getFlagR1());
		clone.setFlagCampania(getFlagCampania());
		clone.setFlagReniec(getFlagReniec());
		clone.setFlagEquifax(getFlagEquifax());
		clone.setFechaRegistro(getFechaRegistro());
		clone.setTipoCampania(getTipoCampania());
		clone.setFlagCalificacion(getFlagCalificacion());
		clone.setFlagCliente(getFlagCliente());
		clone.setIdUsuarioSession(getIdUsuarioSession());
		clone.setStrJson(getStrJson());
		clone.setFormHtml(getFormHtml());
		clone.setCodigoHtml(getCodigoHtml());
		clone.setPeriodoRegistro(getPeriodoRegistro());
		clone.setModificacion(getModificacion());
		clone.setFlagTipificacion(getFlagTipificacion());
		clone.setFlagActualizarRespuesta(getFlagActualizarRespuesta());
		clone.setFlagObtenerInformacion(getFlagObtenerInformacion());
		clone.setFlagConsultarAfiliacion(getFlagConsultarAfiliacion());
		clone.setFlagGeneracionToken(getFlagGeneracionToken());
		clone.setFlagVentaTC(getFlagVentaTC());
		clone.setFlagValidacionToken(getFlagValidacionToken());
		clone.setFlagRecompraGenPOTC(getFlagRecompraGenPOTC());
		clone.setFlagRecompraConPOTC(getFlagRecompraConPOTC());
		clone.setFlagExtornoGenPOTC(getFlagExtornoGenPOTC());
		clone.setFlagExtornoConPOTC(getFlagExtornoConPOTC());
		clone.setIp(getIp());
		clone.setNavegador(getNavegador());
		clone.setPepperType(getPepperType());

		return clone;
	}

	@Override
	public int compareTo(Expediente expediente) {
		long primaryKey = expediente.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ExpedienteClp)) {
			return false;
		}

		ExpedienteClp expediente = (ExpedienteClp)obj;

		long primaryKey = expediente.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(69);

		sb.append("{idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", documento=");
		sb.append(getDocumento());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", paso=");
		sb.append(getPaso());
		sb.append(", pagina=");
		sb.append(getPagina());
		sb.append(", tipoFlujo=");
		sb.append(getTipoFlujo());
		sb.append(", flagR1=");
		sb.append(getFlagR1());
		sb.append(", flagCampania=");
		sb.append(getFlagCampania());
		sb.append(", flagReniec=");
		sb.append(getFlagReniec());
		sb.append(", flagEquifax=");
		sb.append(getFlagEquifax());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append(", tipoCampania=");
		sb.append(getTipoCampania());
		sb.append(", flagCalificacion=");
		sb.append(getFlagCalificacion());
		sb.append(", flagCliente=");
		sb.append(getFlagCliente());
		sb.append(", idUsuarioSession=");
		sb.append(getIdUsuarioSession());
		sb.append(", strJson=");
		sb.append(getStrJson());
		sb.append(", formHtml=");
		sb.append(getFormHtml());
		sb.append(", codigoHtml=");
		sb.append(getCodigoHtml());
		sb.append(", periodoRegistro=");
		sb.append(getPeriodoRegistro());
		sb.append(", modificacion=");
		sb.append(getModificacion());
		sb.append(", flagTipificacion=");
		sb.append(getFlagTipificacion());
		sb.append(", flagActualizarRespuesta=");
		sb.append(getFlagActualizarRespuesta());
		sb.append(", flagObtenerInformacion=");
		sb.append(getFlagObtenerInformacion());
		sb.append(", flagConsultarAfiliacion=");
		sb.append(getFlagConsultarAfiliacion());
		sb.append(", flagGeneracionToken=");
		sb.append(getFlagGeneracionToken());
		sb.append(", flagVentaTC=");
		sb.append(getFlagVentaTC());
		sb.append(", flagValidacionToken=");
		sb.append(getFlagValidacionToken());
		sb.append(", flagRecompraGenPOTC=");
		sb.append(getFlagRecompraGenPOTC());
		sb.append(", flagRecompraConPOTC=");
		sb.append(getFlagRecompraConPOTC());
		sb.append(", flagExtornoGenPOTC=");
		sb.append(getFlagExtornoGenPOTC());
		sb.append(", flagExtornoConPOTC=");
		sb.append(getFlagExtornoConPOTC());
		sb.append(", ip=");
		sb.append(getIp());
		sb.append(", navegador=");
		sb.append(getNavegador());
		sb.append(", pepperType=");
		sb.append(getPepperType());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(106);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Expediente");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>documento</column-name><column-value><![CDATA[");
		sb.append(getDocumento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>paso</column-name><column-value><![CDATA[");
		sb.append(getPaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pagina</column-name><column-value><![CDATA[");
		sb.append(getPagina());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoFlujo</column-name><column-value><![CDATA[");
		sb.append(getTipoFlujo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagR1</column-name><column-value><![CDATA[");
		sb.append(getFlagR1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagCampania</column-name><column-value><![CDATA[");
		sb.append(getFlagCampania());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagReniec</column-name><column-value><![CDATA[");
		sb.append(getFlagReniec());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagEquifax</column-name><column-value><![CDATA[");
		sb.append(getFlagEquifax());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoCampania</column-name><column-value><![CDATA[");
		sb.append(getTipoCampania());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagCalificacion</column-name><column-value><![CDATA[");
		sb.append(getFlagCalificacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagCliente</column-name><column-value><![CDATA[");
		sb.append(getFlagCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idUsuarioSession</column-name><column-value><![CDATA[");
		sb.append(getIdUsuarioSession());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>strJson</column-name><column-value><![CDATA[");
		sb.append(getStrJson());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>formHtml</column-name><column-value><![CDATA[");
		sb.append(getFormHtml());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoHtml</column-name><column-value><![CDATA[");
		sb.append(getCodigoHtml());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>periodoRegistro</column-name><column-value><![CDATA[");
		sb.append(getPeriodoRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modificacion</column-name><column-value><![CDATA[");
		sb.append(getModificacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagTipificacion</column-name><column-value><![CDATA[");
		sb.append(getFlagTipificacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagActualizarRespuesta</column-name><column-value><![CDATA[");
		sb.append(getFlagActualizarRespuesta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagObtenerInformacion</column-name><column-value><![CDATA[");
		sb.append(getFlagObtenerInformacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagConsultarAfiliacion</column-name><column-value><![CDATA[");
		sb.append(getFlagConsultarAfiliacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagGeneracionToken</column-name><column-value><![CDATA[");
		sb.append(getFlagGeneracionToken());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagVentaTC</column-name><column-value><![CDATA[");
		sb.append(getFlagVentaTC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagValidacionToken</column-name><column-value><![CDATA[");
		sb.append(getFlagValidacionToken());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagRecompraGenPOTC</column-name><column-value><![CDATA[");
		sb.append(getFlagRecompraGenPOTC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagRecompraConPOTC</column-name><column-value><![CDATA[");
		sb.append(getFlagRecompraConPOTC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagExtornoGenPOTC</column-name><column-value><![CDATA[");
		sb.append(getFlagExtornoGenPOTC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagExtornoConPOTC</column-name><column-value><![CDATA[");
		sb.append(getFlagExtornoConPOTC());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ip</column-name><column-value><![CDATA[");
		sb.append(getIp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>navegador</column-name><column-value><![CDATA[");
		sb.append(getNavegador());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pepperType</column-name><column-value><![CDATA[");
		sb.append(getPepperType());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idExpediente;
	private String _documento;
	private String _estado;
	private String _paso;
	private String _pagina;
	private String _tipoFlujo;
	private String _flagR1;
	private String _flagCampania;
	private String _flagReniec;
	private String _flagEquifax;
	private Date _fechaRegistro;
	private String _tipoCampania;
	private String _flagCalificacion;
	private String _flagCliente;
	private int _idUsuarioSession;
	private String _strJson;
	private String _formHtml;
	private String _codigoHtml;
	private String _periodoRegistro;
	private Date _modificacion;
	private String _flagTipificacion;
	private String _flagActualizarRespuesta;
	private String _flagObtenerInformacion;
	private String _flagConsultarAfiliacion;
	private String _flagGeneracionToken;
	private String _flagVentaTC;
	private String _flagValidacionToken;
	private String _flagRecompraGenPOTC;
	private String _flagRecompraConPOTC;
	private String _flagExtornoGenPOTC;
	private String _flagExtornoConPOTC;
	private String _ip;
	private String _navegador;
	private String _pepperType;
	private BaseModel<?> _expedienteRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}