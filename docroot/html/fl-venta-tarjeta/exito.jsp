<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
            <div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    ${commerceText.v601001}
                    <div class="lh-ff-content-col">
                     	<div class="lh-ff-section lh-ff-section--notification">
	                        <div class="lh-ff-section__wrapper lh-ff-section__wrapper--image-to-left">
	                          <div class="lh-ff-section__image" style="background-image:url(${themeDisplay.pathThemeImages}/form-framework/ff-result-check.png)"></div>
	                          <div class="lh-ff-section__text">
	                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 lh-ff-section__text__title">${pasoBean.clienteBean.firstname}</h3>
								${renderRequest.preferences.getValue('resumenText022','')}
	                          </div>
	                        </div>
	                      </div>
                      <div class="lh-ff-section">
                        <h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">${renderRequest.preferences.getValue('resumenText007','')}</h4>
                        <div class="lh-ff-info">
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText008','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDescripcionProducto">${pasoBean.productoBean.descripcionTipoProducto}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText009','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoLineaCredito"><span class="nowrap">S/ ${formatCreditLine}</span></div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText010','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoSeguroDesgravamen"><span class="nowrap">S/ ${pasoBean.productoBean.seguroDesgravamen}</span></div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText011','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoTea">${pasoBean.productoBean.tea}%</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText012','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoSeguro">S/${pasoBean.productoBean.cobroMembresia}${renderRequest.preferences.getValue('resumenText013','')}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText014','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDiaPago">${pasoBean.productoBean.diaDePago} ${renderRequest.preferences.getValue('resumenText015','')}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText016','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoEnvioEstadoCuenta">${pasoBean.productoBean.emailStateAccount}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>