/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ParametroHijoPOClp extends BaseModelImpl<ParametroHijoPO>
	implements ParametroHijoPO {
	public ParametroHijoPOClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroHijoPO.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroHijoPO.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idParametroHijo;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdParametroHijo(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idParametroHijo;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroHijo", getIdParametroHijo());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("codigo", getCodigo());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("orden", getOrden());
		attributes.put("dato1", getDato1());
		attributes.put("dato2", getDato2());
		attributes.put("dato3", getDato3());
		attributes.put("dato4", getDato4());
		attributes.put("dato5", getDato5());
		attributes.put("dato6", getDato6());
		attributes.put("dato7", getDato7());
		attributes.put("dato8", getDato8());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroHijo = (Long)attributes.get("idParametroHijo");

		if (idParametroHijo != null) {
			setIdParametroHijo(idParametroHijo);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Integer orden = (Integer)attributes.get("orden");

		if (orden != null) {
			setOrden(orden);
		}

		String dato1 = (String)attributes.get("dato1");

		if (dato1 != null) {
			setDato1(dato1);
		}

		String dato2 = (String)attributes.get("dato2");

		if (dato2 != null) {
			setDato2(dato2);
		}

		String dato3 = (String)attributes.get("dato3");

		if (dato3 != null) {
			setDato3(dato3);
		}

		String dato4 = (String)attributes.get("dato4");

		if (dato4 != null) {
			setDato4(dato4);
		}

		String dato5 = (String)attributes.get("dato5");

		if (dato5 != null) {
			setDato5(dato5);
		}

		String dato6 = (String)attributes.get("dato6");

		if (dato6 != null) {
			setDato6(dato6);
		}

		String dato7 = (String)attributes.get("dato7");

		if (dato7 != null) {
			setDato7(dato7);
		}

		String dato8 = (String)attributes.get("dato8");

		if (dato8 != null) {
			setDato8(dato8);
		}
	}

	@Override
	public long getIdParametroHijo() {
		return _idParametroHijo;
	}

	@Override
	public void setIdParametroHijo(long idParametroHijo) {
		_idParametroHijo = idParametroHijo;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setIdParametroHijo", long.class);

				method.invoke(_parametroHijoPORemoteModel, idParametroHijo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_parametroHijoPORemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_parametroHijoPORemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_parametroHijoPORemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getUserName() {
		return _userName;
	}

	@Override
	public void setUserName(String userName) {
		_userName = userName;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setUserName", String.class);

				method.invoke(_parametroHijoPORemoteModel, userName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setCreateDate", Date.class);

				method.invoke(_parametroHijoPORemoteModel, createDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setModifiedDate", Date.class);

				method.invoke(_parametroHijoPORemoteModel, modifiedDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoPadre() {
		return _codigoPadre;
	}

	@Override
	public void setCodigoPadre(String codigoPadre) {
		_codigoPadre = codigoPadre;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoPadre", String.class);

				method.invoke(_parametroHijoPORemoteModel, codigoPadre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigo() {
		return _codigo;
	}

	@Override
	public void setCodigo(String codigo) {
		_codigo = codigo;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setCodigo", String.class);

				method.invoke(_parametroHijoPORemoteModel, codigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombre() {
		return _nombre;
	}

	@Override
	public void setNombre(String nombre) {
		_nombre = nombre;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setNombre", String.class);

				method.invoke(_parametroHijoPORemoteModel, nombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescripcion() {
		return _descripcion;
	}

	@Override
	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDescripcion", String.class);

				method.invoke(_parametroHijoPORemoteModel, descripcion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getEstado() {
		return _estado;
	}

	@Override
	public boolean isEstado() {
		return _estado;
	}

	@Override
	public void setEstado(boolean estado) {
		_estado = estado;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", boolean.class);

				method.invoke(_parametroHijoPORemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getOrden() {
		return _orden;
	}

	@Override
	public void setOrden(int orden) {
		_orden = orden;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setOrden", int.class);

				method.invoke(_parametroHijoPORemoteModel, orden);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato1() {
		return _dato1;
	}

	@Override
	public void setDato1(String dato1) {
		_dato1 = dato1;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato1", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato2() {
		return _dato2;
	}

	@Override
	public void setDato2(String dato2) {
		_dato2 = dato2;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato2", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato3() {
		return _dato3;
	}

	@Override
	public void setDato3(String dato3) {
		_dato3 = dato3;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato3", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato3);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato4() {
		return _dato4;
	}

	@Override
	public void setDato4(String dato4) {
		_dato4 = dato4;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato4", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato5() {
		return _dato5;
	}

	@Override
	public void setDato5(String dato5) {
		_dato5 = dato5;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato5", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato5);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato6() {
		return _dato6;
	}

	@Override
	public void setDato6(String dato6) {
		_dato6 = dato6;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato6", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato6);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato7() {
		return _dato7;
	}

	@Override
	public void setDato7(String dato7) {
		_dato7 = dato7;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato7", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato7);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDato8() {
		return _dato8;
	}

	@Override
	public void setDato8(String dato8) {
		_dato8 = dato8;

		if (_parametroHijoPORemoteModel != null) {
			try {
				Class<?> clazz = _parametroHijoPORemoteModel.getClass();

				Method method = clazz.getMethod("setDato8", String.class);

				method.invoke(_parametroHijoPORemoteModel, dato8);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getParametroHijoPORemoteModel() {
		return _parametroHijoPORemoteModel;
	}

	public void setParametroHijoPORemoteModel(
		BaseModel<?> parametroHijoPORemoteModel) {
		_parametroHijoPORemoteModel = parametroHijoPORemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _parametroHijoPORemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_parametroHijoPORemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ParametroHijoPOLocalServiceUtil.addParametroHijoPO(this);
		}
		else {
			ParametroHijoPOLocalServiceUtil.updateParametroHijoPO(this);
		}
	}

	@Override
	public ParametroHijoPO toEscapedModel() {
		return (ParametroHijoPO)ProxyUtil.newProxyInstance(ParametroHijoPO.class.getClassLoader(),
			new Class[] { ParametroHijoPO.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ParametroHijoPOClp clone = new ParametroHijoPOClp();

		clone.setIdParametroHijo(getIdParametroHijo());
		clone.setGroupId(getGroupId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setCodigoPadre(getCodigoPadre());
		clone.setCodigo(getCodigo());
		clone.setNombre(getNombre());
		clone.setDescripcion(getDescripcion());
		clone.setEstado(getEstado());
		clone.setOrden(getOrden());
		clone.setDato1(getDato1());
		clone.setDato2(getDato2());
		clone.setDato3(getDato3());
		clone.setDato4(getDato4());
		clone.setDato5(getDato5());
		clone.setDato6(getDato6());
		clone.setDato7(getDato7());
		clone.setDato8(getDato8());

		return clone;
	}

	@Override
	public int compareTo(ParametroHijoPO parametroHijoPO) {
		int value = 0;

		if (getOrden() < parametroHijoPO.getOrden()) {
			value = -1;
		}
		else if (getOrden() > parametroHijoPO.getOrden()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroHijoPOClp)) {
			return false;
		}

		ParametroHijoPOClp parametroHijoPO = (ParametroHijoPOClp)obj;

		long primaryKey = parametroHijoPO.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(43);

		sb.append("{idParametroHijo=");
		sb.append(getIdParametroHijo());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", codigoPadre=");
		sb.append(getCodigoPadre());
		sb.append(", codigo=");
		sb.append(getCodigo());
		sb.append(", nombre=");
		sb.append(getNombre());
		sb.append(", descripcion=");
		sb.append(getDescripcion());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", orden=");
		sb.append(getOrden());
		sb.append(", dato1=");
		sb.append(getDato1());
		sb.append(", dato2=");
		sb.append(getDato2());
		sb.append(", dato3=");
		sb.append(getDato3());
		sb.append(", dato4=");
		sb.append(getDato4());
		sb.append(", dato5=");
		sb.append(getDato5());
		sb.append(", dato6=");
		sb.append(getDato6());
		sb.append(", dato7=");
		sb.append(getDato7());
		sb.append(", dato8=");
		sb.append(getDato8());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(67);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.ParametroHijoPO");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idParametroHijo</column-name><column-value><![CDATA[");
		sb.append(getIdParametroHijo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoPadre</column-name><column-value><![CDATA[");
		sb.append(getCodigoPadre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigo</column-name><column-value><![CDATA[");
		sb.append(getCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombre</column-name><column-value><![CDATA[");
		sb.append(getNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripcion</column-name><column-value><![CDATA[");
		sb.append(getDescripcion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>orden</column-name><column-value><![CDATA[");
		sb.append(getOrden());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato1</column-name><column-value><![CDATA[");
		sb.append(getDato1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato2</column-name><column-value><![CDATA[");
		sb.append(getDato2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato3</column-name><column-value><![CDATA[");
		sb.append(getDato3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato4</column-name><column-value><![CDATA[");
		sb.append(getDato4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato5</column-name><column-value><![CDATA[");
		sb.append(getDato5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato6</column-name><column-value><![CDATA[");
		sb.append(getDato6());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato7</column-name><column-value><![CDATA[");
		sb.append(getDato7());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato8</column-name><column-value><![CDATA[");
		sb.append(getDato8());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idParametroHijo;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _codigoPadre;
	private String _codigo;
	private String _nombre;
	private String _descripcion;
	private boolean _estado;
	private int _orden;
	private String _dato1;
	private String _dato2;
	private String _dato3;
	private String _dato4;
	private String _dato5;
	private String _dato6;
	private String _dato7;
	private String _dato8;
	private BaseModel<?> _parametroHijoPORemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}