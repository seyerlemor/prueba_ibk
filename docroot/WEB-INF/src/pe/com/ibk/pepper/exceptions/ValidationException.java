package pe.com.ibk.pepper.exceptions;

public class ValidationException extends Exception {

	private static final long serialVersionUID = -3246131613320839666L;
	
	public ValidationException() {
		super();
	}
	
	public ValidationException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public ValidationException(String message) {
		super(message);
	}
	
	public ValidationException(Throwable throwable) {
		super(throwable);
	}
}
