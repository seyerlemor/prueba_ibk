/**
 * 
 */
package pe.com.ibk.pepper.gestion;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.model.Servicios;
import pe.com.ibk.pepper.service.ServiciosLocalServiceUtil;

/**
 * @author Interbank
 *
 */
public class GestorRegistros {
	
	private static final Log _log = LogFactoryUtil.getLog(GestorRegistros.class);
	
	public long registrarServicios(SolicitudBean pasoBean) {
		long idServicios = 0;
		try {
			Servicios servicios = ServiciosLocalServiceUtil.createServicios(-1);
			servicios = setServiciosBeanToServicios(pasoBean, servicios);
			servicios = ServiciosLocalServiceUtil.registrarServicios(servicios);
		    idServicios = servicios.getIdServicio();
		} catch (Exception e) {
			_log.error("Error en la clase GestorRegistros, metodo registrarServicios:::>", e);
		}
		return idServicios;
	}	
	
	public Servicios setServiciosBeanToServicios(SolicitudBean pasoBean, Servicios servicios){
		servicios.setIdExpediente(pasoBean.getExpedienteBean().getIdExpediente());
		servicios.setNombre(pasoBean.getServiciosBean().getNombre());
		servicios.setCodigo(pasoBean.getServiciosBean().getCodigo());
		String descripcion = pasoBean.getServiciosBean().getDescripcion();
		descripcion = Validator.isNotNull(descripcion)?descripcion:StringPool.BLANK;
		String descripcionNew = (descripcion.length()>300)?descripcion.substring(0, 300):descripcion;
		servicios.setDescripcion(descripcionNew);
		servicios.setResponse(pasoBean.getServiciosBean().getResponse());
		servicios.setMessageBus(pasoBean.getServiciosBean().getMessageBus());
		servicios.setTipo(pasoBean.getServiciosBean().getTipo());
		servicios.setRespuestaInterna(pasoBean.getServiciosBean().getRespuestaInterna());
		return servicios;
	
	}
}
