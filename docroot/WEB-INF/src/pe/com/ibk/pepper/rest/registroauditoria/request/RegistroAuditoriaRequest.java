
package pe.com.ibk.pepper.rest.registroauditoria.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RegistroAuditoriaRequest {

    private String messageId;
    private String serviceId;
    private String destinationId;
    private String messageType;
    private String message;

    public RegistroAuditoriaRequest(String messageId, String serviceId, String destinationId, String messageType, String message) {
        super();
        this.messageId = messageId;
        this.serviceId = serviceId;
        this.destinationId = destinationId;
        this.messageType = messageType;
        this.message = message;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
