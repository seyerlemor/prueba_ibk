/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Expediente}.
 * </p>
 *
 * @author Interbank
 * @see Expediente
 * @generated
 */
public class ExpedienteWrapper implements Expediente, ModelWrapper<Expediente> {
	public ExpedienteWrapper(Expediente expediente) {
		_expediente = expediente;
	}

	@Override
	public Class<?> getModelClass() {
		return Expediente.class;
	}

	@Override
	public String getModelClassName() {
		return Expediente.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idExpediente", getIdExpediente());
		attributes.put("documento", getDocumento());
		attributes.put("estado", getEstado());
		attributes.put("paso", getPaso());
		attributes.put("pagina", getPagina());
		attributes.put("tipoFlujo", getTipoFlujo());
		attributes.put("flagR1", getFlagR1());
		attributes.put("flagCampania", getFlagCampania());
		attributes.put("flagReniec", getFlagReniec());
		attributes.put("flagEquifax", getFlagEquifax());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("tipoCampania", getTipoCampania());
		attributes.put("flagCalificacion", getFlagCalificacion());
		attributes.put("flagCliente", getFlagCliente());
		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("strJson", getStrJson());
		attributes.put("formHtml", getFormHtml());
		attributes.put("codigoHtml", getCodigoHtml());
		attributes.put("periodoRegistro", getPeriodoRegistro());
		attributes.put("modificacion", getModificacion());
		attributes.put("flagTipificacion", getFlagTipificacion());
		attributes.put("flagActualizarRespuesta", getFlagActualizarRespuesta());
		attributes.put("flagObtenerInformacion", getFlagObtenerInformacion());
		attributes.put("flagConsultarAfiliacion", getFlagConsultarAfiliacion());
		attributes.put("flagGeneracionToken", getFlagGeneracionToken());
		attributes.put("flagVentaTC", getFlagVentaTC());
		attributes.put("flagValidacionToken", getFlagValidacionToken());
		attributes.put("flagRecompraGenPOTC", getFlagRecompraGenPOTC());
		attributes.put("flagRecompraConPOTC", getFlagRecompraConPOTC());
		attributes.put("flagExtornoGenPOTC", getFlagExtornoGenPOTC());
		attributes.put("flagExtornoConPOTC", getFlagExtornoConPOTC());
		attributes.put("ip", getIp());
		attributes.put("navegador", getNavegador());
		attributes.put("pepperType", getPepperType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String documento = (String)attributes.get("documento");

		if (documento != null) {
			setDocumento(documento);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String paso = (String)attributes.get("paso");

		if (paso != null) {
			setPaso(paso);
		}

		String pagina = (String)attributes.get("pagina");

		if (pagina != null) {
			setPagina(pagina);
		}

		String tipoFlujo = (String)attributes.get("tipoFlujo");

		if (tipoFlujo != null) {
			setTipoFlujo(tipoFlujo);
		}

		String flagR1 = (String)attributes.get("flagR1");

		if (flagR1 != null) {
			setFlagR1(flagR1);
		}

		String flagCampania = (String)attributes.get("flagCampania");

		if (flagCampania != null) {
			setFlagCampania(flagCampania);
		}

		String flagReniec = (String)attributes.get("flagReniec");

		if (flagReniec != null) {
			setFlagReniec(flagReniec);
		}

		String flagEquifax = (String)attributes.get("flagEquifax");

		if (flagEquifax != null) {
			setFlagEquifax(flagEquifax);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String tipoCampania = (String)attributes.get("tipoCampania");

		if (tipoCampania != null) {
			setTipoCampania(tipoCampania);
		}

		String flagCalificacion = (String)attributes.get("flagCalificacion");

		if (flagCalificacion != null) {
			setFlagCalificacion(flagCalificacion);
		}

		String flagCliente = (String)attributes.get("flagCliente");

		if (flagCliente != null) {
			setFlagCliente(flagCliente);
		}

		Integer idUsuarioSession = (Integer)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		String strJson = (String)attributes.get("strJson");

		if (strJson != null) {
			setStrJson(strJson);
		}

		String formHtml = (String)attributes.get("formHtml");

		if (formHtml != null) {
			setFormHtml(formHtml);
		}

		String codigoHtml = (String)attributes.get("codigoHtml");

		if (codigoHtml != null) {
			setCodigoHtml(codigoHtml);
		}

		String periodoRegistro = (String)attributes.get("periodoRegistro");

		if (periodoRegistro != null) {
			setPeriodoRegistro(periodoRegistro);
		}

		Date modificacion = (Date)attributes.get("modificacion");

		if (modificacion != null) {
			setModificacion(modificacion);
		}

		String flagTipificacion = (String)attributes.get("flagTipificacion");

		if (flagTipificacion != null) {
			setFlagTipificacion(flagTipificacion);
		}

		String flagActualizarRespuesta = (String)attributes.get(
				"flagActualizarRespuesta");

		if (flagActualizarRespuesta != null) {
			setFlagActualizarRespuesta(flagActualizarRespuesta);
		}

		String flagObtenerInformacion = (String)attributes.get(
				"flagObtenerInformacion");

		if (flagObtenerInformacion != null) {
			setFlagObtenerInformacion(flagObtenerInformacion);
		}

		String flagConsultarAfiliacion = (String)attributes.get(
				"flagConsultarAfiliacion");

		if (flagConsultarAfiliacion != null) {
			setFlagConsultarAfiliacion(flagConsultarAfiliacion);
		}

		String flagGeneracionToken = (String)attributes.get(
				"flagGeneracionToken");

		if (flagGeneracionToken != null) {
			setFlagGeneracionToken(flagGeneracionToken);
		}

		String flagVentaTC = (String)attributes.get("flagVentaTC");

		if (flagVentaTC != null) {
			setFlagVentaTC(flagVentaTC);
		}

		String flagValidacionToken = (String)attributes.get(
				"flagValidacionToken");

		if (flagValidacionToken != null) {
			setFlagValidacionToken(flagValidacionToken);
		}

		String flagRecompraGenPOTC = (String)attributes.get(
				"flagRecompraGenPOTC");

		if (flagRecompraGenPOTC != null) {
			setFlagRecompraGenPOTC(flagRecompraGenPOTC);
		}

		String flagRecompraConPOTC = (String)attributes.get(
				"flagRecompraConPOTC");

		if (flagRecompraConPOTC != null) {
			setFlagRecompraConPOTC(flagRecompraConPOTC);
		}

		String flagExtornoGenPOTC = (String)attributes.get("flagExtornoGenPOTC");

		if (flagExtornoGenPOTC != null) {
			setFlagExtornoGenPOTC(flagExtornoGenPOTC);
		}

		String flagExtornoConPOTC = (String)attributes.get("flagExtornoConPOTC");

		if (flagExtornoConPOTC != null) {
			setFlagExtornoConPOTC(flagExtornoConPOTC);
		}

		String ip = (String)attributes.get("ip");

		if (ip != null) {
			setIp(ip);
		}

		String navegador = (String)attributes.get("navegador");

		if (navegador != null) {
			setNavegador(navegador);
		}

		String pepperType = (String)attributes.get("pepperType");

		if (pepperType != null) {
			setPepperType(pepperType);
		}
	}

	/**
	* Returns the primary key of this expediente.
	*
	* @return the primary key of this expediente
	*/
	@Override
	public long getPrimaryKey() {
		return _expediente.getPrimaryKey();
	}

	/**
	* Sets the primary key of this expediente.
	*
	* @param primaryKey the primary key of this expediente
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_expediente.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id expediente of this expediente.
	*
	* @return the id expediente of this expediente
	*/
	@Override
	public long getIdExpediente() {
		return _expediente.getIdExpediente();
	}

	/**
	* Sets the id expediente of this expediente.
	*
	* @param idExpediente the id expediente of this expediente
	*/
	@Override
	public void setIdExpediente(long idExpediente) {
		_expediente.setIdExpediente(idExpediente);
	}

	/**
	* Returns the documento of this expediente.
	*
	* @return the documento of this expediente
	*/
	@Override
	public java.lang.String getDocumento() {
		return _expediente.getDocumento();
	}

	/**
	* Sets the documento of this expediente.
	*
	* @param documento the documento of this expediente
	*/
	@Override
	public void setDocumento(java.lang.String documento) {
		_expediente.setDocumento(documento);
	}

	/**
	* Returns the estado of this expediente.
	*
	* @return the estado of this expediente
	*/
	@Override
	public java.lang.String getEstado() {
		return _expediente.getEstado();
	}

	/**
	* Sets the estado of this expediente.
	*
	* @param estado the estado of this expediente
	*/
	@Override
	public void setEstado(java.lang.String estado) {
		_expediente.setEstado(estado);
	}

	/**
	* Returns the paso of this expediente.
	*
	* @return the paso of this expediente
	*/
	@Override
	public java.lang.String getPaso() {
		return _expediente.getPaso();
	}

	/**
	* Sets the paso of this expediente.
	*
	* @param paso the paso of this expediente
	*/
	@Override
	public void setPaso(java.lang.String paso) {
		_expediente.setPaso(paso);
	}

	/**
	* Returns the pagina of this expediente.
	*
	* @return the pagina of this expediente
	*/
	@Override
	public java.lang.String getPagina() {
		return _expediente.getPagina();
	}

	/**
	* Sets the pagina of this expediente.
	*
	* @param pagina the pagina of this expediente
	*/
	@Override
	public void setPagina(java.lang.String pagina) {
		_expediente.setPagina(pagina);
	}

	/**
	* Returns the tipo flujo of this expediente.
	*
	* @return the tipo flujo of this expediente
	*/
	@Override
	public java.lang.String getTipoFlujo() {
		return _expediente.getTipoFlujo();
	}

	/**
	* Sets the tipo flujo of this expediente.
	*
	* @param tipoFlujo the tipo flujo of this expediente
	*/
	@Override
	public void setTipoFlujo(java.lang.String tipoFlujo) {
		_expediente.setTipoFlujo(tipoFlujo);
	}

	/**
	* Returns the flag r1 of this expediente.
	*
	* @return the flag r1 of this expediente
	*/
	@Override
	public java.lang.String getFlagR1() {
		return _expediente.getFlagR1();
	}

	/**
	* Sets the flag r1 of this expediente.
	*
	* @param flagR1 the flag r1 of this expediente
	*/
	@Override
	public void setFlagR1(java.lang.String flagR1) {
		_expediente.setFlagR1(flagR1);
	}

	/**
	* Returns the flag campania of this expediente.
	*
	* @return the flag campania of this expediente
	*/
	@Override
	public java.lang.String getFlagCampania() {
		return _expediente.getFlagCampania();
	}

	/**
	* Sets the flag campania of this expediente.
	*
	* @param flagCampania the flag campania of this expediente
	*/
	@Override
	public void setFlagCampania(java.lang.String flagCampania) {
		_expediente.setFlagCampania(flagCampania);
	}

	/**
	* Returns the flag reniec of this expediente.
	*
	* @return the flag reniec of this expediente
	*/
	@Override
	public java.lang.String getFlagReniec() {
		return _expediente.getFlagReniec();
	}

	/**
	* Sets the flag reniec of this expediente.
	*
	* @param flagReniec the flag reniec of this expediente
	*/
	@Override
	public void setFlagReniec(java.lang.String flagReniec) {
		_expediente.setFlagReniec(flagReniec);
	}

	/**
	* Returns the flag equifax of this expediente.
	*
	* @return the flag equifax of this expediente
	*/
	@Override
	public java.lang.String getFlagEquifax() {
		return _expediente.getFlagEquifax();
	}

	/**
	* Sets the flag equifax of this expediente.
	*
	* @param flagEquifax the flag equifax of this expediente
	*/
	@Override
	public void setFlagEquifax(java.lang.String flagEquifax) {
		_expediente.setFlagEquifax(flagEquifax);
	}

	/**
	* Returns the fecha registro of this expediente.
	*
	* @return the fecha registro of this expediente
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _expediente.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this expediente.
	*
	* @param fechaRegistro the fecha registro of this expediente
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_expediente.setFechaRegistro(fechaRegistro);
	}

	/**
	* Returns the tipo campania of this expediente.
	*
	* @return the tipo campania of this expediente
	*/
	@Override
	public java.lang.String getTipoCampania() {
		return _expediente.getTipoCampania();
	}

	/**
	* Sets the tipo campania of this expediente.
	*
	* @param tipoCampania the tipo campania of this expediente
	*/
	@Override
	public void setTipoCampania(java.lang.String tipoCampania) {
		_expediente.setTipoCampania(tipoCampania);
	}

	/**
	* Returns the flag calificacion of this expediente.
	*
	* @return the flag calificacion of this expediente
	*/
	@Override
	public java.lang.String getFlagCalificacion() {
		return _expediente.getFlagCalificacion();
	}

	/**
	* Sets the flag calificacion of this expediente.
	*
	* @param flagCalificacion the flag calificacion of this expediente
	*/
	@Override
	public void setFlagCalificacion(java.lang.String flagCalificacion) {
		_expediente.setFlagCalificacion(flagCalificacion);
	}

	/**
	* Returns the flag cliente of this expediente.
	*
	* @return the flag cliente of this expediente
	*/
	@Override
	public java.lang.String getFlagCliente() {
		return _expediente.getFlagCliente();
	}

	/**
	* Sets the flag cliente of this expediente.
	*
	* @param flagCliente the flag cliente of this expediente
	*/
	@Override
	public void setFlagCliente(java.lang.String flagCliente) {
		_expediente.setFlagCliente(flagCliente);
	}

	/**
	* Returns the id usuario session of this expediente.
	*
	* @return the id usuario session of this expediente
	*/
	@Override
	public int getIdUsuarioSession() {
		return _expediente.getIdUsuarioSession();
	}

	/**
	* Sets the id usuario session of this expediente.
	*
	* @param idUsuarioSession the id usuario session of this expediente
	*/
	@Override
	public void setIdUsuarioSession(int idUsuarioSession) {
		_expediente.setIdUsuarioSession(idUsuarioSession);
	}

	/**
	* Returns the str json of this expediente.
	*
	* @return the str json of this expediente
	*/
	@Override
	public java.lang.String getStrJson() {
		return _expediente.getStrJson();
	}

	/**
	* Sets the str json of this expediente.
	*
	* @param strJson the str json of this expediente
	*/
	@Override
	public void setStrJson(java.lang.String strJson) {
		_expediente.setStrJson(strJson);
	}

	/**
	* Returns the form html of this expediente.
	*
	* @return the form html of this expediente
	*/
	@Override
	public java.lang.String getFormHtml() {
		return _expediente.getFormHtml();
	}

	/**
	* Sets the form html of this expediente.
	*
	* @param formHtml the form html of this expediente
	*/
	@Override
	public void setFormHtml(java.lang.String formHtml) {
		_expediente.setFormHtml(formHtml);
	}

	/**
	* Returns the codigo html of this expediente.
	*
	* @return the codigo html of this expediente
	*/
	@Override
	public java.lang.String getCodigoHtml() {
		return _expediente.getCodigoHtml();
	}

	/**
	* Sets the codigo html of this expediente.
	*
	* @param codigoHtml the codigo html of this expediente
	*/
	@Override
	public void setCodigoHtml(java.lang.String codigoHtml) {
		_expediente.setCodigoHtml(codigoHtml);
	}

	/**
	* Returns the periodo registro of this expediente.
	*
	* @return the periodo registro of this expediente
	*/
	@Override
	public java.lang.String getPeriodoRegistro() {
		return _expediente.getPeriodoRegistro();
	}

	/**
	* Sets the periodo registro of this expediente.
	*
	* @param periodoRegistro the periodo registro of this expediente
	*/
	@Override
	public void setPeriodoRegistro(java.lang.String periodoRegistro) {
		_expediente.setPeriodoRegistro(periodoRegistro);
	}

	/**
	* Returns the modificacion of this expediente.
	*
	* @return the modificacion of this expediente
	*/
	@Override
	public java.util.Date getModificacion() {
		return _expediente.getModificacion();
	}

	/**
	* Sets the modificacion of this expediente.
	*
	* @param modificacion the modificacion of this expediente
	*/
	@Override
	public void setModificacion(java.util.Date modificacion) {
		_expediente.setModificacion(modificacion);
	}

	/**
	* Returns the flag tipificacion of this expediente.
	*
	* @return the flag tipificacion of this expediente
	*/
	@Override
	public java.lang.String getFlagTipificacion() {
		return _expediente.getFlagTipificacion();
	}

	/**
	* Sets the flag tipificacion of this expediente.
	*
	* @param flagTipificacion the flag tipificacion of this expediente
	*/
	@Override
	public void setFlagTipificacion(java.lang.String flagTipificacion) {
		_expediente.setFlagTipificacion(flagTipificacion);
	}

	/**
	* Returns the flag actualizar respuesta of this expediente.
	*
	* @return the flag actualizar respuesta of this expediente
	*/
	@Override
	public java.lang.String getFlagActualizarRespuesta() {
		return _expediente.getFlagActualizarRespuesta();
	}

	/**
	* Sets the flag actualizar respuesta of this expediente.
	*
	* @param flagActualizarRespuesta the flag actualizar respuesta of this expediente
	*/
	@Override
	public void setFlagActualizarRespuesta(
		java.lang.String flagActualizarRespuesta) {
		_expediente.setFlagActualizarRespuesta(flagActualizarRespuesta);
	}

	/**
	* Returns the flag obtener informacion of this expediente.
	*
	* @return the flag obtener informacion of this expediente
	*/
	@Override
	public java.lang.String getFlagObtenerInformacion() {
		return _expediente.getFlagObtenerInformacion();
	}

	/**
	* Sets the flag obtener informacion of this expediente.
	*
	* @param flagObtenerInformacion the flag obtener informacion of this expediente
	*/
	@Override
	public void setFlagObtenerInformacion(
		java.lang.String flagObtenerInformacion) {
		_expediente.setFlagObtenerInformacion(flagObtenerInformacion);
	}

	/**
	* Returns the flag consultar afiliacion of this expediente.
	*
	* @return the flag consultar afiliacion of this expediente
	*/
	@Override
	public java.lang.String getFlagConsultarAfiliacion() {
		return _expediente.getFlagConsultarAfiliacion();
	}

	/**
	* Sets the flag consultar afiliacion of this expediente.
	*
	* @param flagConsultarAfiliacion the flag consultar afiliacion of this expediente
	*/
	@Override
	public void setFlagConsultarAfiliacion(
		java.lang.String flagConsultarAfiliacion) {
		_expediente.setFlagConsultarAfiliacion(flagConsultarAfiliacion);
	}

	/**
	* Returns the flag generacion token of this expediente.
	*
	* @return the flag generacion token of this expediente
	*/
	@Override
	public java.lang.String getFlagGeneracionToken() {
		return _expediente.getFlagGeneracionToken();
	}

	/**
	* Sets the flag generacion token of this expediente.
	*
	* @param flagGeneracionToken the flag generacion token of this expediente
	*/
	@Override
	public void setFlagGeneracionToken(java.lang.String flagGeneracionToken) {
		_expediente.setFlagGeneracionToken(flagGeneracionToken);
	}

	/**
	* Returns the flag venta t c of this expediente.
	*
	* @return the flag venta t c of this expediente
	*/
	@Override
	public java.lang.String getFlagVentaTC() {
		return _expediente.getFlagVentaTC();
	}

	/**
	* Sets the flag venta t c of this expediente.
	*
	* @param flagVentaTC the flag venta t c of this expediente
	*/
	@Override
	public void setFlagVentaTC(java.lang.String flagVentaTC) {
		_expediente.setFlagVentaTC(flagVentaTC);
	}

	/**
	* Returns the flag validacion token of this expediente.
	*
	* @return the flag validacion token of this expediente
	*/
	@Override
	public java.lang.String getFlagValidacionToken() {
		return _expediente.getFlagValidacionToken();
	}

	/**
	* Sets the flag validacion token of this expediente.
	*
	* @param flagValidacionToken the flag validacion token of this expediente
	*/
	@Override
	public void setFlagValidacionToken(java.lang.String flagValidacionToken) {
		_expediente.setFlagValidacionToken(flagValidacionToken);
	}

	/**
	* Returns the flag recompra gen p o t c of this expediente.
	*
	* @return the flag recompra gen p o t c of this expediente
	*/
	@Override
	public java.lang.String getFlagRecompraGenPOTC() {
		return _expediente.getFlagRecompraGenPOTC();
	}

	/**
	* Sets the flag recompra gen p o t c of this expediente.
	*
	* @param flagRecompraGenPOTC the flag recompra gen p o t c of this expediente
	*/
	@Override
	public void setFlagRecompraGenPOTC(java.lang.String flagRecompraGenPOTC) {
		_expediente.setFlagRecompraGenPOTC(flagRecompraGenPOTC);
	}

	/**
	* Returns the flag recompra con p o t c of this expediente.
	*
	* @return the flag recompra con p o t c of this expediente
	*/
	@Override
	public java.lang.String getFlagRecompraConPOTC() {
		return _expediente.getFlagRecompraConPOTC();
	}

	/**
	* Sets the flag recompra con p o t c of this expediente.
	*
	* @param flagRecompraConPOTC the flag recompra con p o t c of this expediente
	*/
	@Override
	public void setFlagRecompraConPOTC(java.lang.String flagRecompraConPOTC) {
		_expediente.setFlagRecompraConPOTC(flagRecompraConPOTC);
	}

	/**
	* Returns the flag extorno gen p o t c of this expediente.
	*
	* @return the flag extorno gen p o t c of this expediente
	*/
	@Override
	public java.lang.String getFlagExtornoGenPOTC() {
		return _expediente.getFlagExtornoGenPOTC();
	}

	/**
	* Sets the flag extorno gen p o t c of this expediente.
	*
	* @param flagExtornoGenPOTC the flag extorno gen p o t c of this expediente
	*/
	@Override
	public void setFlagExtornoGenPOTC(java.lang.String flagExtornoGenPOTC) {
		_expediente.setFlagExtornoGenPOTC(flagExtornoGenPOTC);
	}

	/**
	* Returns the flag extorno con p o t c of this expediente.
	*
	* @return the flag extorno con p o t c of this expediente
	*/
	@Override
	public java.lang.String getFlagExtornoConPOTC() {
		return _expediente.getFlagExtornoConPOTC();
	}

	/**
	* Sets the flag extorno con p o t c of this expediente.
	*
	* @param flagExtornoConPOTC the flag extorno con p o t c of this expediente
	*/
	@Override
	public void setFlagExtornoConPOTC(java.lang.String flagExtornoConPOTC) {
		_expediente.setFlagExtornoConPOTC(flagExtornoConPOTC);
	}

	/**
	* Returns the ip of this expediente.
	*
	* @return the ip of this expediente
	*/
	@Override
	public java.lang.String getIp() {
		return _expediente.getIp();
	}

	/**
	* Sets the ip of this expediente.
	*
	* @param ip the ip of this expediente
	*/
	@Override
	public void setIp(java.lang.String ip) {
		_expediente.setIp(ip);
	}

	/**
	* Returns the navegador of this expediente.
	*
	* @return the navegador of this expediente
	*/
	@Override
	public java.lang.String getNavegador() {
		return _expediente.getNavegador();
	}

	/**
	* Sets the navegador of this expediente.
	*
	* @param navegador the navegador of this expediente
	*/
	@Override
	public void setNavegador(java.lang.String navegador) {
		_expediente.setNavegador(navegador);
	}

	/**
	* Returns the pepper type of this expediente.
	*
	* @return the pepper type of this expediente
	*/
	@Override
	public java.lang.String getPepperType() {
		return _expediente.getPepperType();
	}

	/**
	* Sets the pepper type of this expediente.
	*
	* @param pepperType the pepper type of this expediente
	*/
	@Override
	public void setPepperType(java.lang.String pepperType) {
		_expediente.setPepperType(pepperType);
	}

	@Override
	public boolean isNew() {
		return _expediente.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_expediente.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _expediente.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_expediente.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _expediente.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _expediente.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_expediente.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _expediente.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_expediente.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_expediente.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_expediente.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ExpedienteWrapper((Expediente)_expediente.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.Expediente expediente) {
		return _expediente.compareTo(expediente);
	}

	@Override
	public int hashCode() {
		return _expediente.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.Expediente> toCacheModel() {
		return _expediente.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente toEscapedModel() {
		return new ExpedienteWrapper(_expediente.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente toUnescapedModel() {
		return new ExpedienteWrapper(_expediente.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _expediente.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _expediente.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_expediente.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ExpedienteWrapper)) {
			return false;
		}

		ExpedienteWrapper expedienteWrapper = (ExpedienteWrapper)obj;

		if (Validator.equals(_expediente, expedienteWrapper._expediente)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Expediente getWrappedExpediente() {
		return _expediente;
	}

	@Override
	public Expediente getWrappedModel() {
		return _expediente;
	}

	@Override
	public void resetOriginalValues() {
		_expediente.resetOriginalValues();
	}

	private Expediente _expediente;
}