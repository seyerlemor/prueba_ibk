package pe.com.ibk.pepper.rest.obtenerinformacion.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Address implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@SerializedName("subType")
	private String subType;
	
	@SerializedName("useCode")
	private String useCode;
	
	@SerializedName("secuenceId")
	private String secuenceId;
	
	@SerializedName("isStandard")
	private String isStandard;
	
	@SerializedName("standardAddress")
	private StandardAddress standardAddr;
	
	@SerializedName("roadTypeName")
	private String roadTypeName;
	
	@SerializedName("country")
	private String country;
	
	@SerializedName("department")
	private String department;
	
	@SerializedName("province")
	private String province;
	
	@SerializedName("district")
	private String district;
	
	@SerializedName("ubigeoCode")
	private String ubigeoCode;
	
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getUseCode() {
		return useCode;
	}

	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}

	public String getSecuenceId() {
		return secuenceId;
	}

	public void setSecuenceId(String secuenceId) {
		this.secuenceId = secuenceId;
	}

	public String getIsStandard() {
		return isStandard;
	}

	public void setIsStandard(String isStandard) {
		this.isStandard = isStandard;
	}

	public StandardAddress getStandardAddress() {
		return standardAddr;
	}

	public void setStandardAddress(StandardAddress standardAddress) {
		this.standardAddr = standardAddress;
	}

	public String getRoadTypeName() {
		return roadTypeName;
	}

	public void setRoadTypeName(String roadTypeName) {
		this.roadTypeName = roadTypeName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUbigeoCode() {
		return ubigeoCode;
	}

	public void setUbigeoCode(String ubigeoCode) {
		this.ubigeoCode = ubigeoCode;
	}
}
