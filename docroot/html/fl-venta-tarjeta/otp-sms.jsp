<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="enviarCodigoOtp" var="enviarCodigoOtp" />

<portlet:resourceURL var="accessStep2" id="accessStep2"></portlet:resourceURL>
	
	<div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    <liferay-ui:journal-article articleId="${contentStep.articleId}" groupId="${contentStep.groupId}" />
                    <div class="lh-ff-content-col">
                      <div class="lh-ff-section lh-ff-section--notification">
                        <div class="lh-ff-section__wrapper">
                          <div class="lh-ff-section__text">
                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 lh-ff-section__text__title">${renderRequest.preferences.getValue('otpPartTitle','')} ${pasoBean.clienteBean.firstname},</h3>
                            <!-- TARJETA DE CRÉDITO-->
                            <h4 class="lh-typo__commontitle lh-typo__commontitle--1">${renderRequest.preferences.getValue('otpText003','')}</h4>
                            <!-- CUENTA 100% DIGITAL-->
                            <p class="lh-typo__p1">${renderRequest.preferences.getValue('otpSmsPartSubTitle002','')} ${requestScope.formatCellNumber}.</p>
                            <div class="lh-gap-15"></div>
                            <form:form   action="${enviarCodigoOtp}" autocomplete="off" commandName="clienteBean" class="lh-ff-form" novalidate="novalidate" >
                              <div class="lh-ff-group">
                                            <div class="lh-ff-group__item lh-ff-group__item--sm">
                                              <label class="lh-ff__label" for="ff_operador">Operador
                                              </label>
                                                          <div class="lh-ff-group__field">
                                                                  <div class="lh__select lh__select--checked">
                                                                    <select id="ff_operador" name="cellPhoneOperator" data-validate-required data-validate-required-message="${renderRequest.preferences.getValue('otpOperatorRequiredMessage','')}">
                                                                      <option value selected>Seleccionar</option>
                                                                      <option value="C">CLARO</option>
                                                                      <option value="E">ENTEL</option>
                                                                      <option value="M">MOVISTAR</option>
                                                                      <option value="B">BITEL</option>
                                                                    </select>
                                                                  </div>
                                                          </div>
                                            </div>
                              <input type="hidden" id="radioOTP" value="1" name="radioOTP" />              
                              </div>
                              <div class="lh-ff-group">
                                <div class="lh-ff-group__item lh-ff-group__item--buttons">
                                  <button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit"><span>${renderRequest.preferences.getValue('otpBtnSigamos','')}</span></button>
                                </div>
                              </div>
                                    <div class="lh-form__error">
                                      <div class="lh-typo__p2 lh-form__error-message" data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
                                    </div>
                            </form:form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>
	
