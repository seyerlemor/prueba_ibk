
package pe.com.ibk.pepper.rest.campania.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Lead implements Serializable{
	
	private static final long serialVersionUID = -2292735018746941351L;
	
	@SerializedName("id")
    private String id;
	
    @SerializedName("customerType")

    private String customerType;
    @SerializedName("type")
    
    private String type;
    @SerializedName("cardBrand")
    
    private CardBrand cardBrand;
    @SerializedName("cardType")
    
    private CardType cardType;
    @SerializedName("cardId")
    
    private String cardId;
    @SerializedName("accountHost")
    
    private String accountHost;
    @SerializedName("accountAmount")
    
    private String accountAmount;
    @SerializedName("currency")
    
    private Currency currency;
    @SerializedName("rent")
    
    private String rent;
    @SerializedName("amount")
    
    private String amount;
    @SerializedName("amount1")
    
    private String amount1;
    @SerializedName("amount2")
    
    private String amount2;
    @SerializedName("rate1")
    
    private String rate1;
    @SerializedName("rate2")
    
    private String rate2;
    @SerializedName("CEM")
    
    private String cEM;
    @SerializedName("incomeFlow")
    
    private String incomeFlow;
    @SerializedName("deadline")
    
    private String deadline;
    @SerializedName("quota")
    
    private Quota quota;
    @SerializedName("channelPriority")
    
    private String channelPriority;
    @SerializedName("priority")
    
    private String priority;
    @SerializedName("merchandisingId")
    
    private String merchandisingId;
    @SerializedName("channelId")
    
    private String channelId;
    @SerializedName("additionals")
    
    private List<Additional> additionals = null;
    @SerializedName("campaigns")
    
    private List<Campaign> campaigns = null;
    @SerializedName("offers")
    
    private List<Offer> offers = null;
    @SerializedName("treatments")
    
    private List<Treatment> treatments = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Lead withId(String id) {
        this.id = id;
        return this;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public Lead withCustomerType(String customerType) {
        this.customerType = customerType;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Lead withType(String type) {
        this.type = type;
        return this;
    }

    public CardBrand getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(CardBrand cardBrand) {
        this.cardBrand = cardBrand;
    }

    public Lead withCardBrand(CardBrand cardBrand) {
        this.cardBrand = cardBrand;
        return this;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Lead withCardType(CardType cardType) {
        this.cardType = cardType;
        return this;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Lead withCardId(String cardId) {
        this.cardId = cardId;
        return this;
    }

    public String getAccountHost() {
        return accountHost;
    }

    public void setAccountHost(String accountHost) {
        this.accountHost = accountHost;
    }

    public Lead withAccountHost(String accountHost) {
        this.accountHost = accountHost;
        return this;
    }

    public String getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(String accountAmount) {
        this.accountAmount = accountAmount;
    }

    public Lead withAccountAmount(String accountAmount) {
        this.accountAmount = accountAmount;
        return this;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Lead withCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public Lead withRent(String rent) {
        this.rent = rent;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Lead withAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public String getAmount1() {
        return amount1;
    }

    public void setAmount1(String amount1) {
        this.amount1 = amount1;
    }

    public Lead withAmount1(String amount1) {
        this.amount1 = amount1;
        return this;
    }

    public String getAmount2() {
        return amount2;
    }

    public void setAmount2(String amount2) {
        this.amount2 = amount2;
    }

    public Lead withAmount2(String amount2) {
        this.amount2 = amount2;
        return this;
    }

    public String getRate1() {
        return rate1;
    }

    public void setRate1(String rate1) {
        this.rate1 = rate1;
    }

    public Lead withRate1(String rate1) {
        this.rate1 = rate1;
        return this;
    }

    public String getRate2() {
        return rate2;
    }

    public void setRate2(String rate2) {
        this.rate2 = rate2;
    }

    public Lead withRate2(String rate2) {
        this.rate2 = rate2;
        return this;
    }

    public String getCEM() {
        return cEM;
    }

    public void setCEM(String cEM) {
        this.cEM = cEM;
    }

    public Lead withCEM(String cEM) {
        this.cEM = cEM;
        return this;
    }

    public String getIncomeFlow() {
        return incomeFlow;
    }

    public void setIncomeFlow(String incomeFlow) {
        this.incomeFlow = incomeFlow;
    }

    public Lead withIncomeFlow(String incomeFlow) {
        this.incomeFlow = incomeFlow;
        return this;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Lead withDeadline(String deadline) {
        this.deadline = deadline;
        return this;
    }

    public Quota getQuota() {
        return quota;
    }

    public void setQuota(Quota quota) {
        this.quota = quota;
    }

    public Lead withQuota(Quota quota) {
        this.quota = quota;
        return this;
    }

    public String getChannelPriority() {
        return channelPriority;
    }

    public void setChannelPriority(String channelPriority) {
        this.channelPriority = channelPriority;
    }

    public Lead withChannelPriority(String channelPriority) {
        this.channelPriority = channelPriority;
        return this;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Lead withPriority(String priority) {
        this.priority = priority;
        return this;
    }

    public String getMerchandisingId() {
        return merchandisingId;
    }

    public void setMerchandisingId(String merchandisingId) {
        this.merchandisingId = merchandisingId;
    }

    public Lead withMerchandisingId(String merchandisingId) {
        this.merchandisingId = merchandisingId;
        return this;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Lead withChannelId(String channelId) {
        this.channelId = channelId;
        return this;
    }

    public List<Additional> getAdditionals() {
        return additionals;
    }

    public void setAdditionals(List<Additional> additionals) {
        this.additionals = additionals;
    }

    public Lead withAdditionals(List<Additional> additionals) {
        this.additionals = additionals;
        return this;
    }

    public List<Campaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Campaign> campaigns) {
        this.campaigns = campaigns;
    }

    public Lead withCampaigns(List<Campaign> campaigns) {
        this.campaigns = campaigns;
        return this;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public Lead withOffers(List<Offer> offers) {
        this.offers = offers;
        return this;
    }

    public List<Treatment> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<Treatment> treatments) {
        this.treatments = treatments;
    }

    public Lead withTreatments(List<Treatment> treatments) {
        this.treatments = treatments;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("customerType", customerType).append("type", type).append("cardBrand", cardBrand).append("cardType", cardType).append("cardId", cardId).append("accountHost", accountHost).append("accountAmount", accountAmount).append("currency", currency).append("rent", rent).append("amount", amount).append("amount1", amount1).append("amount2", amount2).append("rate1", rate1).append("rate2", rate2).append("cEM", cEM).append("incomeFlow", incomeFlow).append("deadline", deadline).append("quota", quota).append("channelPriority", channelPriority).append("priority", priority).append("merchandisingId", merchandisingId).append("channelId", channelId).append("additionals", additionals).append("campaigns", campaigns).append("offers", offers).append("treatments", treatments).toString();
    }

}
