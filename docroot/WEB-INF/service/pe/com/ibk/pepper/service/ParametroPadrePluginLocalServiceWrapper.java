/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ParametroPadrePluginLocalService}.
 *
 * @author Interbank
 * @see ParametroPadrePluginLocalService
 * @generated
 */
public class ParametroPadrePluginLocalServiceWrapper
	implements ParametroPadrePluginLocalService,
		ServiceWrapper<ParametroPadrePluginLocalService> {
	public ParametroPadrePluginLocalServiceWrapper(
		ParametroPadrePluginLocalService parametroPadrePluginLocalService) {
		_parametroPadrePluginLocalService = parametroPadrePluginLocalService;
	}

	/**
	* Adds the parametro padre plugin to the database. Also notifies the appropriate model listeners.
	*
	* @param parametroPadrePlugin the parametro padre plugin
	* @return the parametro padre plugin that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin addParametroPadrePlugin(
		pe.com.ibk.pepper.model.ParametroPadrePlugin parametroPadrePlugin)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.addParametroPadrePlugin(parametroPadrePlugin);
	}

	/**
	* Creates a new parametro padre plugin with the primary key. Does not add the parametro padre plugin to the database.
	*
	* @param idParametroPadre the primary key for the new parametro padre plugin
	* @return the new parametro padre plugin
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin createParametroPadrePlugin(
		long idParametroPadre) {
		return _parametroPadrePluginLocalService.createParametroPadrePlugin(idParametroPadre);
	}

	/**
	* Deletes the parametro padre plugin with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idParametroPadre the primary key of the parametro padre plugin
	* @return the parametro padre plugin that was removed
	* @throws PortalException if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin deleteParametroPadrePlugin(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.deleteParametroPadrePlugin(idParametroPadre);
	}

	/**
	* Deletes the parametro padre plugin from the database. Also notifies the appropriate model listeners.
	*
	* @param parametroPadrePlugin the parametro padre plugin
	* @return the parametro padre plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin deleteParametroPadrePlugin(
		pe.com.ibk.pepper.model.ParametroPadrePlugin parametroPadrePlugin)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.deleteParametroPadrePlugin(parametroPadrePlugin);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _parametroPadrePluginLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchParametroPadrePlugin(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.fetchParametroPadrePlugin(idParametroPadre);
	}

	/**
	* Returns the parametro padre plugin with the primary key.
	*
	* @param idParametroPadre the primary key of the parametro padre plugin
	* @return the parametro padre plugin
	* @throws PortalException if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin getParametroPadrePlugin(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.getParametroPadrePlugin(idParametroPadre);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the parametro padre plugins.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @return the range of parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> getParametroPadrePlugins(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.getParametroPadrePlugins(start,
			end);
	}

	/**
	* Returns the number of parametro padre plugins.
	*
	* @return the number of parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getParametroPadrePluginsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.getParametroPadrePluginsCount();
	}

	/**
	* Updates the parametro padre plugin in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param parametroPadrePlugin the parametro padre plugin
	* @return the parametro padre plugin that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin updateParametroPadrePlugin(
		pe.com.ibk.pepper.model.ParametroPadrePlugin parametroPadrePlugin)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePluginLocalService.updateParametroPadrePlugin(parametroPadrePlugin);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _parametroPadrePluginLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_parametroPadrePluginLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _parametroPadrePluginLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ParametroPadrePluginLocalService getWrappedParametroPadrePluginLocalService() {
		return _parametroPadrePluginLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedParametroPadrePluginLocalService(
		ParametroPadrePluginLocalService parametroPadrePluginLocalService) {
		_parametroPadrePluginLocalService = parametroPadrePluginLocalService;
	}

	@Override
	public ParametroPadrePluginLocalService getWrappedService() {
		return _parametroPadrePluginLocalService;
	}

	@Override
	public void setWrappedService(
		ParametroPadrePluginLocalService parametroPadrePluginLocalService) {
		_parametroPadrePluginLocalService = parametroPadrePluginLocalService;
	}

	private ParametroPadrePluginLocalService _parametroPadrePluginLocalService;
}