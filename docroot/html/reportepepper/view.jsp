<%@ include file="/html/init.jsp" %>
<script type="text/javascript" src="<c:url value="/js/jquery-1.8.3.js" ></c:url>"></script>
<script type="text/javascript" src="<c:url value="/js/util.js" ></c:url>"></script>

<input type="hidden" id="portletNameSpace" value="<portlet:namespace />">
<portlet:resourceURL id="action" var="action"></portlet:resourceURL>
<input type="hidden" id="idAction" value="${action}">

<style type="text/css">
<!--
label.font-control {  font-weight: bold; }	

.paginator a {
  text-decoration: underline;
}

.paginator a.active { 
    color: #999999;
    cursor: default;
}

.ml{
	margin: 5%;
}
-->
</style>

<div class="ml">
   <aui:form action="asd" method="get" id="formSubmit" name="formSubmit">
           <input type="hidden" id="idMethod" name="method">
           <input type="hidden" id="idCantidadRowsExport" value="${CANTIDAD_ROWS}">
	     <div class="row">
	     	 	   <div class="span2">
		             	<label for="cboPromocion" class="font-control">Estado</label>
		           </div>
		           <div class="span4">
		           	    <select id="cboPromocion" name="cboProducto" style="width: auto;">
							<option value="0">Todos</option>
							<c:forEach items="${TIPO_ESTADO}" var="p" >
								<option value="${p.codigo}" >${p.nombre}</option>
							</c:forEach>
						</select>
		           </div>
		   </div>
		   <div class="row">		   
		           <div class="span2">
		             <label for="Desde" class="font-control">Desde</label>
		           </div>
		           <div class="span4">
		           	  <liferay-ui:input-date dayValue="${DIA_ACTUAL}" 
		           	   	monthValue="${MES_ACTUAL}" 
		           	   	yearValue="${ANNO_ACTUAL}" 
		           	   	dayParam="inicioDia" 
		           	   	monthParam="inicioMes" 
		           	   	yearParam="incioAnno" />
						<input type="hidden" value="${DIA_ACTUAL}-${MES_ACTUAL}-${ANNO_ACTUAL}" id="txtFechaInicio" name="<portlet:namespace />txtFechaInicio" >
		           </div>
		           <div class="span2">
		           		<label for="" class="font-control">Hasta</label>
		           </div>
		           <div class="span3">
		          	    <liferay-ui:input-date dayValue="${DIA_ACTUAL}" 
		          	    monthValue="${MES_ACTUAL}" 
		          	    yearValue="${ANNO_ACTUAL}"  
		          	    dayParam="finDia" 
		          	    monthParam="finMes" 
		          	    yearParam="finAnno"  />		          	    
						<input type="hidden" value="${DIA_ACTUAL}-${MES_ACTUAL}-${ANNO_ACTUAL}" id="txtFechaFin" name="<portlet:namespace />txtFechaFin" >
		           </div>
		   </div>
		    <!-- Buttons -->
		     <div class="row">
		    
		           <div class="span2">
		             &nbsp;
		           </div>
		           <div class="span4">
		               <input id="btnExportCVS" type="button" value="Exportar a CSV" class="btn">
		           </div>
		           <div class="span2">
		           	&nbsp;
		           </div>
		           <div class="span3">
		          	  <input id="btnBuscar" type="button" value="Buscar" class="btn">
		           </div>

			</div>
			<!-- Message Response -->
			<div class="row" style="padding-top: 15px;">
					<input type="hidden" id="MENSAJE_ERROR_FORMATO_FECHA_DESDE" value="<liferay-ui:message key="mensaje.error.fecha.fechaDesde"/>">
					<input type="hidden" id="MENSAJE_ERROR_FORMATO_FECHA_HASTA" value="<liferay-ui:message key="mensaje.error.fecha.fechaHasta" />">
					<input type="hidden" id="MENSAJE_ERROR_FECHAS_DESDE_MAYOR" value="<liferay-ui:message key="mensaje.error.fecha.fechaDesdeMayor" />">
					<input type="hidden" id="MENSAJE_ERROR_FECHAS_HASTA_MENOR_DESDE" value="<liferay-ui:message key="mensaje.error.fecha.fechaHastaMenorDesde"/>">
					<input type="hidden" id="MENSAJE_ERROR_FECHAS_HASTA_MAYOR_FECHA_ACTUAL" value="<liferay-ui:message key="mensaje.error.fecha.fechaHastaMayorFechaActual" />">
					<input type="hidden" id="MENSAJE_ERROR_NO_HAY_REGISTROS" value="<liferay-ui:message key="mensaje.info.busqueda.noHayRegistros" />">
					<input type="hidden" id="MENSAJE_INFO_ROWS_EXPORT_CSV" value="<liferay-ui:message key="mensaje.info.exportacion" />">		
					
					<p id="msjRespuesta"></p>								
			</div>  
	</aui:form>	 
	
	<!-- Display Table  -->
	<div class="row" style="margin-left: 1px;padding-top: 10px;">
			<div style="clear: both;"></div>
	</div>
	   
</div>