/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.TblCodigoPromocion;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TblCodigoPromocion in entity cache.
 *
 * @author Interbank
 * @see TblCodigoPromocion
 * @generated
 */
public class TblCodigoPromocionCacheModel implements CacheModel<TblCodigoPromocion>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{idCodigo=");
		sb.append(idCodigo);
		sb.append(", codigo=");
		sb.append(codigo);
		sb.append(", comercio=");
		sb.append(comercio);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TblCodigoPromocion toEntityModel() {
		TblCodigoPromocionImpl tblCodigoPromocionImpl = new TblCodigoPromocionImpl();

		tblCodigoPromocionImpl.setIdCodigo(idCodigo);

		if (codigo == null) {
			tblCodigoPromocionImpl.setCodigo(StringPool.BLANK);
		}
		else {
			tblCodigoPromocionImpl.setCodigo(codigo);
		}

		if (comercio == null) {
			tblCodigoPromocionImpl.setComercio(StringPool.BLANK);
		}
		else {
			tblCodigoPromocionImpl.setComercio(comercio);
		}

		if (estado == null) {
			tblCodigoPromocionImpl.setEstado(StringPool.BLANK);
		}
		else {
			tblCodigoPromocionImpl.setEstado(estado);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			tblCodigoPromocionImpl.setFechaRegistro(null);
		}
		else {
			tblCodigoPromocionImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		tblCodigoPromocionImpl.resetOriginalValues();

		return tblCodigoPromocionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idCodigo = objectInput.readInt();
		codigo = objectInput.readUTF();
		comercio = objectInput.readUTF();
		estado = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idCodigo);

		if (codigo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigo);
		}

		if (comercio == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comercio);
		}

		if (estado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estado);
		}

		objectOutput.writeLong(fechaRegistro);
	}

	public int idCodigo;
	public String codigo;
	public String comercio;
	public String estado;
	public long fechaRegistro;
}