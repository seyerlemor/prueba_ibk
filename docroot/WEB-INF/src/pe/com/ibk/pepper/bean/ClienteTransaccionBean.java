package pe.com.ibk.pepper.bean;

import java.io.Serializable;
import java.util.Date;

public class ClienteTransaccionBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private long idClienteTransaccion;
	private long idUsuarioSession;
	private long idExpediente;
	private String estado;
	private String paso;
	private String pagina;
	private String tipoFlujo;
	private String strJson;
	private String restauracion;
	private Date fechaRegistro;
	public long getIdClienteTransaccion() {
		return idClienteTransaccion;
	}
	public void setIdClienteTransaccion(long idClienteTransaccion) {
		this.idClienteTransaccion = idClienteTransaccion;
	}
	public long getIdUsuarioSession() {
		return idUsuarioSession;
	}
	public void setIdUsuarioSession(long idUsuarioSession) {
		this.idUsuarioSession = idUsuarioSession;
	}
	public long getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(long idExpediente) {
		this.idExpediente = idExpediente;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPaso() {
		return paso;
	}
	public void setPaso(String paso) {
		this.paso = paso;
	}
	public String getPagina() {
		return pagina;
	}
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	public String getTipoFlujo() {
		return tipoFlujo;
	}
	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}
	public String getStrJson() {
		return strJson;
	}
	public void setStrJson(String strJson) {
		this.strJson = strJson;
	}
	public String getRestauracion() {
		return restauracion;
	}
	public void setRestauracion(String restauracion) {
		this.restauracion = restauracion;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	@Override
	public String toString() {
		return "ClienteTransaccionBean [idClienteTransaccion="
				+ idClienteTransaccion + ", idUsuarioSession="
				+ idUsuarioSession + ", idExpediente=" + idExpediente
				+ ", estado=" + estado + ", paso=" + paso + ", pagina="
				+ pagina + ", tipoFlujo=" + tipoFlujo + ", strJson=" + strJson
				+ ", restauracion=" + restauracion + ", fechaRegistro="
				+ fechaRegistro + "]";
	}
	
	
	
	
	
}
