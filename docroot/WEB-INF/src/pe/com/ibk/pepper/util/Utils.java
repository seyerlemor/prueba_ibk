package pe.com.ibk.pepper.util;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import pe.com.ibk.pepper.bean.ClienteBean;
import pe.com.ibk.pepper.bean.ComercioBean;
import pe.com.ibk.pepper.bean.ConfigurarTarjeta;
import pe.com.ibk.pepper.bean.DireccionClienteBean;
import pe.com.ibk.pepper.bean.DynamicTextBean;
import pe.com.ibk.pepper.bean.ExpedienteBean;
import pe.com.ibk.pepper.bean.FormConyugeBean;
import pe.com.ibk.pepper.bean.OfertaBean;
import pe.com.ibk.pepper.bean.OfertasBean;
import pe.com.ibk.pepper.bean.ProductoBean;
import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.bean.TarjetaContent;
import pe.com.ibk.pepper.bean.UsuarioSessionBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.model.TblCodigoPromocion;
import pe.com.ibk.pepper.rest.campania.response.Campanium;
import pe.com.ibk.pepper.rest.campania.response.Lead;
import pe.com.ibk.pepper.rest.config.ConfiguracionRestUtil;
import pe.com.ibk.pepper.rest.fechahoraentrega.response.Shift;
import pe.com.ibk.pepper.rest.fechahoraentrega.response.Turno;
import pe.com.ibk.pepper.rest.generic.response.HttpResponseV2;
import pe.com.ibk.pepper.rest.listanegra.request.Adrress;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Address;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Direccion;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.DireccionEstandar;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Email;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.StandardAddress;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Telephone;
import pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil;
import pe.com.ibk.pepper.service.TblCodigoPromocionLocalServiceUtil;

public class Utils {

	private static final Log logger = LogFactoryUtil.getLog(Utils.class);
	public static final String REST_FWK4_HOST = loadProperties(PortalKeys.REST_FWK4_HOST);
	public static final String REST_API_HOST = loadProperties(PortalKeys.REST_API_HOST);
	public static final String REST_USUARIO = loadProperties(PortalKeys.REST_USUARIO);
	public static final String REST_PASSWORD = loadProperties(PortalKeys.REST_PSSWRD);

	private Utils() {}
	
	public static Boolean validarVacioNulo(String valor){
		Boolean flag = false;
		if(Validator.isNull(valor) || StringPool.BLANK.equals(valor))
			flag = true;
		return flag;
	}
	
	public static String[] reemplazarYSplit(String cadena){
		String campoAdicional = cadena.replace('*', '#');
		return campoAdicional.split("#");
	}
	
	public static String[] reemplazarYSplit2(String cadena){
		return cadena.split(";");
	}
	
	public static String reemplazarTildes(String cadena){
		String newValue = cadena;
		if(validarVacioNulo(cadena))
			newValue = StringPool.BLANK;
		String[] oldSubs = {"á","é","í","ó","ú","Á","É","Í","Ó","Ú"};
		String[] newSubs = {"a","e","i","o","u","A","E","I","O","U"};
		return StringUtil.replace(newValue, oldSubs, newSubs);
	}
	
	public static String limpiarMontos(String cadena){
		String[] oldSubs = {"S/", StringPool.PERCENT, StringPool.SPACE, StringPool.COMMA, StringPool.PERIOD};
		String[] newSubs = {StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK};
		return StringUtil.replace(cadena.trim(), oldSubs, newSubs);
	}

	public static String replaceCadenaX(String cadena, String tipo){
		String resultado = StringPool.BLANK;
		int longitud = cadena.length();
		String nCaracter = "X";
		switch (tipo) {
		case Constantes.TELEPHONE_FIELD_TYPE:
			for (int x=0;x<(longitud-3);x++)
				resultado +=nCaracter;
			resultado += cadena.substring(longitud-3);
			break;
		case Constantes.EMAIL_FIELD_TYPE:
			resultado = StringUtils.overlay(cadena,"XXXXX", 0, cadena.indexOf(StringPool.AT)-3);
			break;

		default:
			break;
		}
		return resultado;
	}

	public static String getHeaderError(String requestString, String responseString ,String nameWS) {
		
		StringBuilder stringBuilder = new StringBuilder("-- ".concat(nameWS).concat(" -- :"));	
		stringBuilder.append("REQUEST:");
		stringBuilder.append(requestString);
		stringBuilder.append(" ,RESPONSE:");
		stringBuilder.append(responseString);
		String mensajeError = stringBuilder.toString();
		return Constantes.WS_EXCEPCION_DES_TRAZA.concat(mensajeError).concat("] ");
	}
	
	public static String verificarSegundoNombre(String cadena){
		String newValue = cadena;
		if(validarVacioNulo(newValue))
			newValue = StringPool.BLANK;
		newValue = newValue.trim();
		String[] oldSubs = {"á","é","í","ó","ú","Á","É","Í","Ó","Ú","."};
		String[] newSubs = {"a","e","i","o","u","A","E","I","O","U",""};
		newValue = StringUtil.replace(newValue, oldSubs, newSubs);
		return newValue;
	}
	
	public static SolicitudBean getRedirectPageLogicaReingreso(SolicitudBean pasoBean){
		String pageError = StringPool.BLANK;
		Boolean flagLogicaReingreso = Boolean.FALSE;
		String contentHtml = StringPool.BLANK;
		String codigoError = pasoBean.getExpedienteBean().getCodigoLogicaReingreso();
		switch (codigoError) {
		case "0":
			flagLogicaReingreso = Boolean.TRUE;
			contentHtml = Constantes.HTML_CONTINUAR;
			break;
		case "1":
		case "14":
			pageError = Constantes.HTML_ERROR_NO_VIA;
			break;
		case "2":
		case "3":
		case "6":
		case "15":
			pageError = Constantes.HTML_ERROR_TRAMTE_PROCESO;
			break;
		default:
			pageError = Constantes.HTML_ERROR;
		}
		pasoBean.getExpedienteBean().setHtmlCode(contentHtml);
		pasoBean.getExpedienteBean().setCodigoServicioError(pageError);
		pasoBean.getExpedienteBean().setFlagLogicaReingreso(flagLogicaReingreso);	
		return pasoBean;
	}
	
	public static SolicitudBean getRedirectPageRetryLogic(SolicitudBean pasoBean){
		String pageError = StringPool.BLANK;
		Boolean flagRetryLogic = Boolean.FALSE;
		String contentHtml = StringPool.BLANK;
		String codigoError = pasoBean.getExpedienteBean().getCodigoLogicaReingreso();
		switch (codigoError) {
		case "0":
			flagRetryLogic = Boolean.TRUE;
			contentHtml = Constantes.HTML_CONTINUAR;
			break;
		case "1":
			flagRetryLogic = Boolean.TRUE;
			contentHtml = Constantes.HTML_CONTINUAR;
			pasoBean.setOfertasBean(loadCardsByRetryLogic(pasoBean,pasoBean.getProductoBean().getMarcaProducto(), pasoBean.getProductoBean().getTipoProducto()));
			break;
		case "E7010-111":
		case "E7010-07":
		case "E7010-06":
		case "E7010-02":
			pageError = Constantes.HTML_ERROR_NO_VIA;
			break;
		case "":
		case "E7011":
			pageError = Constantes.HTML_ERROR_TRAMTE_PROCESO;
			break;
		case "E1605":
			pageError = Constantes.HTML_NO_CALIFICA;
			pasoBean.getExpedienteBean().setFlagCalificacion(Boolean.FALSE);
			break;
		default:
			pageError = Constantes.HTML_ERROR_LOGICA_REINGRESO;
		}
		pasoBean.getExpedienteBean().setHtmlCode(contentHtml);
		pasoBean.getExpedienteBean().setCodigoServicioError(pageError);
		pasoBean.getExpedienteBean().setFlagLogicaReingreso(flagRetryLogic);
		return pasoBean;
	}
	
	public static OfertasBean loadCardsByRetryLogic(SolicitudBean pasoBean, String brandProduct, String typeProduct){
		List<OfertaBean> lstOferta = new ArrayList<>();
		String brandProductFormat = brandProduct.length()>1?brandProduct:Constantes.VALOR_CERO.concat(brandProduct);
		String typeProductFormat = typeProduct.length()>1?typeProduct:Constantes.VALOR_CERO.concat(typeProduct);
		for (OfertaBean oferta : pasoBean.getOfertasBean().getOfertaBean()) 
			if(brandProductFormat.equalsIgnoreCase(oferta.getBrandProduct()) && typeProductFormat.equalsIgnoreCase(oferta.getTypeProduct()))
				lstOferta.add(oferta);
		OfertasBean ofertasBean = new OfertasBean();
		ofertasBean.setOfertaBean(lstOferta);
		return ofertasBean;
	}
    
    public static Boolean validateBlankAndNull(String strText){
    	return Validator.isNotNull(strText)&&!StringPool.BLANK.equals(strText);
    }
    
    public static String getUrlPath(SolicitudBean pasoBean){
    	  String urlBase = Constantes.URL_TARJETA;
		  return urlBase.concat(pasoBean.getExpedienteBean().getHtmlCode());
	}
    
    public static boolean buscarEnCadena(String[] listaCadena,String codBusqueda)
	{
    	Set<String> companias = new HashSet<>(Arrays.asList(listaCadena)); 
		if (Validator.isNull(codBusqueda)) {
			return false;
		}
		return companias.contains(codBusqueda);
	}
    
    public static String obtenerSiguientePaso(SolicitudBean pasoBean){
		return String.valueOf(((Integer.parseInt(pasoBean.getExpedienteBean().getPaso())+1)/10)+1);
	}
	
    public String concatenarPasoPagina(String tipoFlujo, String paso, String pagina){
		return tipoFlujo+paso+pagina;
	}
    
    public static OfertaBean loadOfferApi(Lead lead, String[] detalleAdicionalCampania, String[] detalleAdicional24810){
    	OfertaBean ofertaBean = new OfertaBean();
    	if(Validator.isNotNull(lead)){
    		ofertaBean.setBrandProduct(lead.getCardBrand().getId());
        	ofertaBean.setTypeProduct(lead.getCardType().getId());
        	ofertaBean.setCreditLine(lead.getAmount());
        	ofertaBean.setFormatCreditLine(currencyFormat(lead.getAmount()));
        	ofertaBean.setProductRate(lead.getRate2());
        	ofertaBean.setCustomerType(lead.getCustomerType());
        	ofertaBean.setBrandDescription(lead.getCardBrand().getName());
        	ofertaBean.setDescriptionType(lead.getCardType().getName());
    	}else{
    		ofertaBean.setBrandProduct(detalleAdicionalCampania[0]);
        	ofertaBean.setTypeProduct(detalleAdicionalCampania[1]);
        	ofertaBean.setCreditLine(detalleAdicionalCampania[2]);
        	ofertaBean.setFormatCreditLine(currencyFormat(detalleAdicionalCampania[2]));
        	ofertaBean.setProductRate(detalleAdicionalCampania[3]);
        	ofertaBean.setCustomerType(detalleAdicionalCampania[4]);
        	ofertaBean.setBrandDescription(detalleAdicionalCampania[5]);
        	ofertaBean.setDescriptionType(detalleAdicionalCampania[6]);
    	}
    	ofertaBean.setMemberShip(detalleAdicional24810[0]);
    	ofertaBean.setTea(detalleAdicional24810[1]);
    	ofertaBean.setSafeDeduction(detalleAdicional24810[2]);
    	ofertaBean.setMinimumCreditLine(detalleAdicional24810[3]);
    	return ofertaBean;
    }
    
    public static OfertaBean loadOffer(Campanium lead, String[] detalleAdicionalCampania, String[] detalleAdicional24810){
    	OfertaBean ofertaBean = new OfertaBean();
    	if(Validator.isNotNull(lead)){
    		ofertaBean.setBrandProduct(lead.getDetalle().getMarcaTarjeta());
        	ofertaBean.setTypeProduct(lead.getDetalle().getTipoTarjeta());
        	ofertaBean.setCreditLine(lead.getDetalle().getMonto());
        	ofertaBean.setFormatCreditLine(currencyFormat(lead.getDetalle().getMonto()));
        	ofertaBean.setProductRate(lead.getDetalle().getTasa2());
        	ofertaBean.setCustomerType(lead.getDetalle().getTipoCliente());
        	ofertaBean.setBrandDescription(lead.getAdicionales().getDescripcionMarcaTarjeta());
        	ofertaBean.setDescriptionType(lead.getAdicionales().getDescripcionTipoTarjeta());
    	}else{
    		ofertaBean.setBrandProduct(detalleAdicionalCampania[0]);
        	ofertaBean.setTypeProduct(detalleAdicionalCampania[1]);
        	ofertaBean.setCreditLine(detalleAdicionalCampania[2]);
        	ofertaBean.setFormatCreditLine(currencyFormat(detalleAdicionalCampania[2]));
        	ofertaBean.setProductRate(detalleAdicionalCampania[3]);
        	ofertaBean.setCustomerType(detalleAdicionalCampania[4]);
        	ofertaBean.setBrandDescription(detalleAdicionalCampania[5]);
        	ofertaBean.setDescriptionType(detalleAdicionalCampania[6]);
    	}
    	ofertaBean.setMemberShip(detalleAdicional24810[0]);
    	ofertaBean.setTea(detalleAdicional24810[1]);
    	ofertaBean.setSafeDeduction(detalleAdicional24810[2]);
    	ofertaBean.setMinimumCreditLine(detalleAdicional24810[3]);
    	return ofertaBean;
    }
    
    public static TarjetaContent loadMinimDetailCard(OfertaBean ofertaBean, PortletRequest request){
		TarjetaContent tc = new TarjetaContent();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String estabCompare = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.URL_ESTABLECIMIENTO));
			estabCompare = Validator.isNotNull(estabCompare)&&!Constantes.ESTABLECIMIENTO_ISHOP.equalsIgnoreCase(estabCompare)?estabCompare:StringPool.BLANK;
			String codigo = ofertaBean.getBrandProduct().concat(ofertaBean.getTypeProduct());
			ParametroHijoPO ph = ParametroHijoPOLocalServiceUtil.getParametroHijoByCodigoAndCodigoPadreAndDato2(estabCompare, codigo, Constantes.SOLICITUD_TARJETA);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(themeDisplay.getScopeGroupId(), ph.getDato1());
			com.liferay.portal.kernel.xml.Document documentArticle = SAXReaderUtil.read(journalArticle.getContent());
			tc.setTituloTarjeta(WebContentStructureUtil.getText(documentArticle, RequestParam.TITLE_CARD));
			tc.setImagenTarjeta(WebContentStructureUtil.getText(documentArticle, RequestParam.IMAGE_CARD));
		} catch (SystemException | PortalException | DocumentException  e) {
			logger.error("Error en SystemException | PortalException | DocumentException : ", e);
		}
		return tc;
	}
    
    public static TarjetaContent loadDetailCard(SolicitudBean pasoBean, PortletRequest request){
		TarjetaContent tc = new TarjetaContent();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String estabCompare = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.URL_ESTABLECIMIENTO));
			estabCompare = Validator.isNotNull(estabCompare)&&!Constantes.ESTABLECIMIENTO_ISHOP.equalsIgnoreCase(estabCompare)?estabCompare:StringPool.BLANK;
			String codigo = pasoBean.getProductoBean().getMarcaProducto()+pasoBean.getProductoBean().getTipoProducto();
			ParametroHijoPO ph = ParametroHijoPOLocalServiceUtil.getParametroHijoByCodigoAndCodigoPadreAndDato2(estabCompare, codigo, Constantes.SOLICITUD_TARJETA);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(themeDisplay.getScopeGroupId(), ph.getDato1());
			com.liferay.portal.kernel.xml.Document documentArticle = SAXReaderUtil.read(journalArticle.getContent());
			tc.setTitulo(WebContentStructureUtil.getText(documentArticle, RequestParam.TITLE));
			pasoBean.getProductoBean().setDescripcionContenido(WebContentStructureUtil.getText(documentArticle, RequestParam.TITLE_CARD));
			tc.setTituloTarjeta(WebContentStructureUtil.getText(documentArticle, RequestParam.TITLE_CARD));
			String tipoMoneda = pasoBean.getProductoBean().getIdMoneda();
			String monto = pasoBean.getProductoBean().getLineaCredito();
			String symbolWithMonto = getSymbolMonto(tipoMoneda, monto);
			String desc = StringUtil.replace(WebContentStructureUtil.getText(documentArticle, "subTituloCab"), 
									new String[]{"[$MONTO$]"}, new String[]{symbolWithMonto});
			tc.setDescripcion(desc);
			tc.setImagenTarjeta(WebContentStructureUtil.getText(documentArticle, RequestParam.IMAGE_CARD));
			tc.setPregunta(WebContentStructureUtil.getText(documentArticle, "pregunta"));
			tc.setTextoBotonIzquierdo(WebContentStructureUtil.getText(documentArticle, "textoBotonIzquierdo"));
			tc.setTextoBotonDerecho(WebContentStructureUtil.getText(documentArticle, "textoBotonDerecho"));
			tc.setIcons(WebContentStructureUtil.getEntitesByName(documentArticle, "imagenIcon"));
			tc.setHtml(WebContentStructureUtil.getText(documentArticle, "htmlPopupCab"));
			tc.setHtmlBeneficios(WebContentStructureUtil.getText(documentArticle, "htmlBeneficios"));
		} catch (SystemException | PortalException | DocumentException  e) {
			logger.error("Error en SystemException | PortalException | DocumentException : ", e);
		}
		return tc;
	}
    
    public static SolicitudBean loadOffertoStep(SolicitudBean pasoBean, String idBrand){
    	int id = Integer.parseInt(idBrand)-1;
    	OfertaBean ofertaBean = pasoBean.getOfertasBean().getOfertaBean().get(id);
    	pasoBean.getProductoBean().setMarcaProducto(ofertaBean.getBrandProduct());
		pasoBean.getProductoBean().setTipoProducto(ofertaBean.getTypeProduct());
		pasoBean.getProductoBean().setLineaCredito(ofertaBean.getCreditLine());
		pasoBean.getProductoBean().setTasaProducto(ofertaBean.getProductRate());
		pasoBean.getProductoBean().setTipoCliente(ofertaBean.getCustomerType());
		pasoBean.getProductoBean().setDescripcionMarca(ofertaBean.getBrandDescription());
		pasoBean.getProductoBean().setDescripcionTipoProducto(ofertaBean.getDescriptionType());						
		pasoBean.getProductoBean().setCobroMembresia(ofertaBean.getMemberShip());
		pasoBean.getProductoBean().setTea(ofertaBean.getTea());
		pasoBean.getProductoBean().setSeguroDesgravamen(ofertaBean.getSafeDeduction());
		pasoBean.getProductoBean().setLineaCreditoMinima(ofertaBean.getMinimumCreditLine());
		
		return pasoBean;
    }
    
    public static String formatExpirationDate(String fechaVencimiento){
    	return fechaVencimiento.substring(4,6).concat(StringPool.SLASH).concat(fechaVencimiento.substring(2,4));
    }
    
    public static JSONObject loadDeliveryAddress(SolicitudBean pasoBeanSession, Boolean flagFechaHora3G, Direccion direccionEntrega){
    	JSONObject json;
    	String version = flagFechaHora3G?Constantes.FECHA_HORA_3G:Constantes.FECHA_HORA_2G;
    	if(Constantes.FECHA_HORA_2G.equalsIgnoreCase(version))
			json = loadDeliveryAddress2G(pasoBeanSession, direccionEntrega);
		else
			json = loadDeliveryAddress3G(pasoBeanSession, direccionEntrega);
    	return json;
    }
    
    public static JSONObject loadDeliveryAddress3G(SolicitudBean pasoBeanSession, Direccion direccionEntrega){
    	JSONObject json = JSONFactoryUtil.createJSONObject();
    	try {
    		Boolean flagDisponibilidad = pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getCourier().getIsAvailable();
    		String horarioInicio = pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getStartTime();
			String horarioFin = pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getEndTime();
			String htmlOption = StringPool.BLANK;
			String turnoDisponible = StringPool.BLANK;
			if(flagDisponibilidad){
				for (Object item : pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getDates()) {
					String valorFH = "{}".equalsIgnoreCase(item.toString())?StringPool.BLANK:item.toString();
					if(!StringPool.BLANK.equalsIgnoreCase(valorFH)){
						htmlOption = Constantes.HTML_SELECT_VALUE_0;
						Date fechaDisponible = new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD).parse(item.toString());
						SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, YYYY");
						htmlOption+="<option value=\""+item.toString().concat(StringPool.AT).concat(horarioInicio).concat(StringPool.AT).concat(horarioFin)+"\">"+dateFormat.format(fechaDisponible)+"</option>";
					}
				}
				for (Shift turno : pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getShifts()) {
					turnoDisponible+=Constantes.OPEN_DIV_OPTION.concat("<input type=\"radio\" id=\"deliveryTime_"+turno.getId()).concat("\" name=\"deliveryTime\" value=\"").concat(turno.getHour()).concat("\"><label for=\"deliveryTime_").concat(String.valueOf(turno.getId())).concat("\"><span>").concat(turno.getDescription()).concat(Constantes.CLOSE_SPAN_LABEL_DIV);
				}
			}
			json.put(RequestParam.DELIVERY, flagDisponibilidad);
			json.put(RequestParam.CODE, pasoBeanSession.getExpedienteBean().getFlagConsultarFechaHora()?ResponseMessage.SUCCESS.getCode():ResponseMessage.ERROR_SERVER.getCode());
			json.put(RequestParam.DATES, htmlOption);
			json.put(RequestParam.SCHEDULE, turnoDisponible);
			if(Validator.isNotNull(direccionEntrega))
				json.put(RequestParam.RESULT, addHtmlAddress(direccionEntrega.getAddressDetail(), pasoBeanSession.getClienteBean().getDirecciones().getDireccion().size()));
	    } catch (ParseException e) {
			logger.error(e);
		}
			return json;
    }
    public static JSONObject loadDeliveryAddress2G(SolicitudBean pasoBeanSession, Direccion direccionEntrega){
    	JSONObject json = JSONFactoryUtil.createJSONObject();
    	try {
    		Boolean flagDisponibilidad = Boolean.parseBoolean(pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getFlagDisponibilidad());
    		String horarioInicio = pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getHorarioInicio();
			String horarioFin = pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getHorarioFin();
			String htmlOption = StringPool.BLANK;
			String turnoDisponible = StringPool.BLANK;
			if(flagDisponibilidad){
				htmlOption = Constantes.HTML_SELECT_VALUE_0;
				for (Object item : pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getFechasDisponibles().getItem()) {
					String valorFH = "{}".equalsIgnoreCase(item.toString())?StringPool.BLANK:item.toString();
					if(!StringPool.BLANK.equalsIgnoreCase(valorFH)){
						Date fechaDisponible;
						fechaDisponible = new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD).parse(item.toString());
						SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, YYYY");
						htmlOption+="<option value=\""+item.toString().concat(StringPool.AT).concat(horarioInicio).concat(StringPool.AT).concat(horarioFin)+"\">"+dateFormat.format(fechaDisponible)+"</option>";
					}
				}
				for (Turno turno : pasoBeanSession.getFhEntregaBean().getHttpResponseV2().getMessageResponse().getBody().getConsultarFechaHoraEntregaResponse().getHorariosDisponibles().getTurno()) {
					turnoDisponible+=Constantes.OPEN_DIV_OPTION.concat("<input type=\"radio\" id=\"deliveryTime_").concat(turno.getId()).concat("\" name=\"deliveryTime\" value=\"").concat(turno.getHora()).concat("\"><label for=\"deliveryTime_").concat(turno.getId()).concat("\"><span>").concat(turno.getDescripcion()).concat(Constantes.CLOSE_SPAN_LABEL_DIV);
				}
			}
			json.put(RequestParam.DELIVERY, flagDisponibilidad);
			json.put(RequestParam.CODE, pasoBeanSession.getExpedienteBean().getFlagConsultarFechaHora()?ResponseMessage.SUCCESS.getCode():ResponseMessage.ERROR_SERVER.getCode());
			json.put(RequestParam.DATES, htmlOption);
			json.put(RequestParam.SCHEDULE, turnoDisponible);
			json.put(RequestParam.RESULT, addHtmlAddress(direccionEntrega.getAddressDetail(), pasoBeanSession.getClienteBean().getDirecciones().getDireccion().size()));
    } catch (ParseException e) {
		logger.error(e);
	}
		return json;
    }
    
    public static String getSymbolMonto(String tipoMoneda, String monto){
		String symbol;
		switch (Validator.isNull(tipoMoneda) ? 0 : Integer.valueOf(tipoMoneda)) {
		case 1:
			symbol = "S/ ";
			break;
		case 2:
			symbol = "US$ ";
			break;
		default:
			symbol = StringPool.SPACE;
			break;
		}
		
	   return symbol.concat(monto);
	}
    public static boolean validarNumerico(String cadena) {
	      return cadena.matches("[0-9]*")?true:false; 
	   }
    
    public static String getCvvType(SolicitudBean pasoBean, String lstSecurityCode){
    	String typeCvvCode = StringPool.BLANK;
    	String[] list = reemplazarYSplit(lstSecurityCode);
    	switch (pasoBean.getProductoBean().getMarcaProducto()) {
			case Constantes.TARJETA_MARCA_VISA:
				typeCvvCode = list[0];
				break;
			case Constantes.TARJETA_MARCA_MASTERCARD:
				typeCvvCode = list[1];
				break;
			case Constantes.TARJETA_MARCA_AMEX:
				typeCvvCode = list[2];
				break;
			default:
				break;
		}
    	return  Constantes.TIPO_CODIGO_CVV2.equalsIgnoreCase(typeCvvCode)?pasoBean.getProductoBean().getCodigo4csc():pasoBean.getProductoBean().getCvv();
    }
    
    public static Boolean validateBoolean(Boolean flagRequest){
    	Boolean response = Boolean.FALSE;
    	if(Validator.isNotNull(flagRequest))
    		response = flagRequest;
    	return response;
    }
    
    public static List<OfertaBean> getTcDetails(PortletRequest request, List<OfertaBean> lstOfertaBean){
    	String templateDetail = request.getPreferences().getValue("resultadotcDetailTCText", StringPool.BLANK);
    	int counter = 1;
    	List<OfertaBean> lstOfferBean = new ArrayList<>();
    	for (OfertaBean ofertaBean : lstOfertaBean) {
    		String detail = StringUtil.replace(templateDetail
        			, new String[]{"[$INDICE$]", "[$MEMBERSHIP$]", "[$SAFEDEDUCTION$]", "[$TEA$]"}
        			, new String[]{String.valueOf(counter), ofertaBean.getMemberShip(), ofertaBean.getSafeDeduction(), ofertaBean.getTea()}
    				);
    		ofertaBean.setTcDetail(detail);
    		lstOfferBean.add(ofertaBean);
    		counter++;
		}
    	return lstOfferBean;
    }
    
    public static String currencyFormat(String linea){
    	int lineaNum = new BigDecimal(linea).intValue();
		DecimalFormatSymbols simbolo=new DecimalFormatSymbols();
	    simbolo.setDecimalSeparator('.');
	    simbolo.setGroupingSeparator(',');
		DecimalFormat formateador = new DecimalFormat("###,###.00",simbolo);
		return formateador.format(lineaNum);
    }
    public static String getMessageId(String pepperType){
    	DateFormat idMensaje = new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYYMMDDHHMMSS);
		Date today = new Date();
		return Constantes.PEPPER_TYPE_TAG.concat(StringPool.UNDERLINE).concat(pepperType).concat(StringPool.UNDERLINE).concat(idMensaje.format(today));
    }
    
	protected static String loadProperties(String key) {
		return PropertiesCache.getInstance().getProperty(key);
	}  
	  
	public static String getDevice(ResourceRequest request){
		HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
		 return servletRequest.getHeader("User-Agent").indexOf("Mobile") != -1?Constantes.DISPOSITIVO_MOBILE:Constantes.DISPOSITIVO_PC;
	}
	
	public static String formatCard(String numeroTarjeta){
		return numeroTarjeta.substring(0,4).concat(StringPool.SPACE).concat(numeroTarjeta.substring(4,8)).concat(StringPool.SPACE).concat(numeroTarjeta.substring(8,12)).concat(StringPool.SPACE).concat(numeroTarjeta.substring(12,numeroTarjeta.length()));
	}
	
	public static Boolean validateLine(SolicitudBean pasoBean){
		Boolean flagValidate = Boolean.FALSE;
		int lineaCreditoMinima = new BigDecimal(pasoBean.getProductoBean().getLineaCreditoMinima()).intValue();
		int lineaCredito = new BigDecimal(pasoBean.getProductoBean().getLineaCredito()).intValue();
		if(lineaCreditoMinima == lineaCredito)
			flagValidate = Boolean.TRUE;
		return flagValidate;
	}
	
	public static String validateLineaPepper(SolicitudBean pasoBean){
		int lineaCredito = new BigDecimal(pasoBean.getProductoBean().getLineaCredito()).intValue();
		int lineaPepper = new BigDecimal(pasoBean.getProductoBean().getProvisionalLinePepper()).intValue();
		return String.valueOf(lineaPepper>lineaCredito?lineaCredito:lineaPepper);
	}
	
	public static DynamicTextBean loadCommerceContent(String commerceId, ThemeDisplay themeDisplay){
		DynamicTextBean txtBean = new DynamicTextBean();
		try {
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(themeDisplay.getScopeGroupId(), commerceId);
			com.liferay.portal.kernel.xml.Document documentArticle = SAXReaderUtil.read(journalArticle.getContent());
			txtBean.setV101001(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V101001));
			txtBean.setV101002(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V101002));
			txtBean.setV101003(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V101003));
			txtBean.setV101004(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V101004));
			txtBean.setV101005(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V101005));
			txtBean.setV101006(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V101006));
			
			txtBean.setV201001(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V201001));
			txtBean.setV201002(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V201002));
			txtBean.setV201003(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V201003));
			txtBean.setV201004(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V201004));
			
			txtBean.setV401001(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V401001));
			txtBean.setV401002(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V401002));
			txtBean.setV401003(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V401003));
			txtBean.setV401004(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V401004));
			txtBean.setV401005(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V401005));
			
			txtBean.setV501001(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501001));
			txtBean.setV501002(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501002));
			txtBean.setV501003(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501003));
			txtBean.setV501004(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501004));
			txtBean.setV501005(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501005));
			txtBean.setV501006(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501006));
			txtBean.setV501007(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501007));
			txtBean.setV501008(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501008));
			txtBean.setV501009(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V501009));
			
			txtBean.setV601001(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601001));
			txtBean.setV601002(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601002));
			txtBean.setV601003(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601003));
			txtBean.setV601004(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601004));
			txtBean.setV601005(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601005));
			txtBean.setV601006(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601006));
			txtBean.setV601007(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601007));
			txtBean.setV601008(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601008));
			txtBean.setV601009(WebContentStructureUtil.getText(documentArticle, RequestParam.DYNAMIC_TXT_V601009));
			
		} catch (PortalException | SystemException | DocumentException e) {
			logger.error("Error al cargar contenido ", e);
		} 
		return txtBean;
	}
	
	public static Direccion addDeliveryAddress(ConfigurarTarjeta configurarTarjeta){
		Direccion direccionEntrega = new Direccion();
		try {
			DireccionEstandar direccionEstandarEntrega = new DireccionEstandar();
			direccionEntrega.setTipoDireccion(Constantes.CODIGO_USO_CORRES);
			DireccionClienteBean direAux;
			direAux = UbigeoUtil.getUbigeoByCod(configurarTarjeta.getAddressDistrito());
			direccionEntrega.setDepartamento(direAux.getDepartamento());
			direccionEntrega.setDistrito(direAux.getDistrito());
			direccionEntrega.setProvincia(direAux.getProvincia());
			direccionEntrega.setUbigeo(configurarTarjeta.getAddressDistrito());
			direccionEstandarEntrega.setTipoVia(configurarTarjeta.getAddressType());
			direccionEstandarEntrega.setDescripcionTipoVia(configurarTarjeta.getDireccionEntregaDescripcionTipovia());
			direccionEstandarEntrega.setNombreVia(reemplazarTildes(configurarTarjeta.getAddressName()));
			direccionEstandarEntrega.setNumero(reemplazarTildes(configurarTarjeta.getAddressNro()));
			direccionEstandarEntrega.setManzana(reemplazarTildes(configurarTarjeta.getAddressMz()));
			direccionEstandarEntrega.setPisoLote(reemplazarTildes(configurarTarjeta.getAddressLt()));
			direccionEstandarEntrega.setInterior(reemplazarTildes(configurarTarjeta.getAddressInt()));
			direccionEntrega.setDireccionEstandar(direccionEstandarEntrega);
			direccionEntrega.setCodigoUso(Constantes.CODIGO_USO_CORRES);
			direccionEntrega.setAddressDetail(StringsUtil.formatAddressPepper(direccionEstandarEntrega));
		} catch (WebServicesException e) {
			logger.error(e);
		}
		return direccionEntrega;
	}
	
	public static String addHtmlAddress(String address, int position){
		return Constantes.OPEN_DIV_OPTION.concat("<input type=\"radio\" name=\"deliveryaddress\" value=\"").concat(String.valueOf(position)).concat("\"><label><span>").concat(address).concat(Constantes.CLOSE_SPAN_LABEL_DIV);
	}
	
	public static List<Direccion> addStandardAddress(ClienteBean clienteBean){
		List<Direccion> direcciones = new ArrayList<>();
		Direccion direccionNueva = new Direccion();
		DireccionEstandar direccionEstandar = new DireccionEstandar();
		direccionNueva.setCodigoUso(Constantes.CODIGO_USO_PRINCI);
		direccionNueva.setDepartamento(clienteBean.getAddressDepartamento());
		direccionNueva.setProvincia(clienteBean.getAddressProvincia());
		direccionNueva.setDistrito(clienteBean.getAddressDistrito());
		direccionNueva.setUbigeo(clienteBean.getAddressDistrito());
		direccionEstandar.setTipoVia(clienteBean.getAddressType());
		direccionEstandar.setDescripcionTipoVia(clienteBean.getDescripcionTipoVia());
		direccionEstandar.setNombreVia(reemplazarTildes(clienteBean.getAddressName().trim()));
		direccionEstandar.setNumero(reemplazarTildes(clienteBean.getAddressNro().trim()));
		direccionEstandar.setManzana(reemplazarTildes(clienteBean.getAddressMz().trim()));
		direccionEstandar.setPisoLote(reemplazarTildes(clienteBean.getAddressLt().trim()));
		direccionEstandar.setInterior(reemplazarTildes(clienteBean.getAddressInt().trim()));
		direccionNueva.setDireccionEstandar(direccionEstandar);
		direccionNueva.setAddressDetail(StringsUtil.formatAddressPepper(direccionEstandar));
		direcciones.add(direccionNueva);
		return direcciones;
	}
	
	public static FormConyugeBean setSpouseData(SolicitudBean pasoBean){
		FormConyugeBean formConyuge	= new FormConyugeBean();
		if (Validator.isNotNull(pasoBean.getClienteBean().getFirstname())) 
			formConyuge.setFirstname(pasoBean.getClienteBean().getFirstname());
		if (Validator.isNotNull(pasoBean.getClienteBean().getMiddlename())) 
			formConyuge.setSecondname(pasoBean.getClienteBean().getMiddlename());
		if (Validator.isNotNull(pasoBean.getClienteBean().getLastname())) 
			formConyuge.setPaternalsurname(pasoBean.getClienteBean().getLastname());
		if (Validator.isNotNull(pasoBean.getClienteBean().getMotherslastname())) 
			formConyuge.setMaternalsurname(pasoBean.getClienteBean().getMotherslastname());
		if (Validator.isNotNull(pasoBean.getClienteBean().getBirthday())) 
			formConyuge.setBirthdate(pasoBean.getClienteBean().getBirthday());
		return formConyuge;
	}
	
	public static void addSessionUser(SolicitudBean pasoBeanSession, PortletRequest request){	
		Gson gson = new Gson();
		String request123 = gson.toJson(pasoBeanSession);
		String requestString = ConfiguracionRestUtil.limpiarJsonRequest(request123);
		SessionKeys.addSessionPortal(request, SessionKeys.PASO_BEAN, requestString);
	}
	
	public static SolicitudBean setServicePersonDataToPasoBean(HttpResponseV2 httpResponse, SolicitudBean pasoBean){
		String primerNombre = reemplazarTildes(httpResponse.getFirstName());
		String segundoNombre = reemplazarTildes(httpResponse.getSecondName());
		pasoBean.getClienteBean().setFlagCliente(httpResponse.getIsCustomer());
		pasoBean.getClienteBean().setLastname(reemplazarTildes(httpResponse.getLastName()));
		pasoBean.getClienteBean().setMotherslastname(reemplazarTildes(httpResponse.getSecondLastName()));
		pasoBean.getClienteBean().setFirstname(reemplazarTildes(httpResponse.getFirstName()));						
		pasoBean.getClienteBean().setCodigoUnico(httpResponse.getCustomerId());
		pasoBean.getClienteBean().setMiddlename(Validator.isNotNull(httpResponse.getSecondName()) && !StringPool.BLANK.equals(httpResponse.getSecondName())?verificarSegundoNombre(httpResponse.getSecondName()):null);
		pasoBean.getClienteBean().setBirthday(Validator.isNotNull(httpResponse.getBirthdate())?DatesUtil.formatoFechaNacimiento(httpResponse.getBirthdate()):null);
		pasoBean.getClienteBean().setGender(httpResponse.getGenderType());
		String civilStatus = Validator.isNotNull(httpResponse.getMaritalStatusId())&&!StringPool.BLANK.equalsIgnoreCase(httpResponse.getMaritalStatusId())?httpResponse.getMaritalStatusId():Constantes.ESTADO_TIPO_SOLTERO;
		pasoBean.getClienteBean().setCivilStatus(civilStatus);
		pasoBean.getClienteBean().setNivelEducacion(httpResponse.getEducationLevel());
		String flagCliente = httpResponse.getIsCustomer();			
		pasoBean.getClienteBean().setNombres(primerNombre.concat(segundoNombre));
		pasoBean.getProductoBean().setNombreTitularProducto(pasoBean.getClienteBean().getFirstname().toUpperCase().concat(StringPool.STAR).concat(pasoBean.getClienteBean().getLastname().toUpperCase()));
		if(Constantes.CLIENTE_SI.equals(flagCliente)){
			pasoBean.getClienteBean().getDirecciones().setDireccion(changeAddressToDireccion(httpResponse.getAddress()));					
			pasoBean.getClienteBean().setEmailAddressOtp(getEmailApiPerson(httpResponse.getEmails()));
			pasoBean.getClienteBean().setCellPhoneNumberOtp(getTelephoneApiPerson(httpResponse.getTelephones()));
		}
		return pasoBean;
	}
	
	public static List<Direccion> changeAddressToDireccion(List<Address> listAddress){
		 List<Direccion> listDireccion = new ArrayList<>();
		 if(Validator.isNotNull(listAddress))
			 for (Address address : listAddress) {
				 if(Constantes.FLAG_DIRECCION_ESTANDAR.equals(address.getIsStandard()) && Constantes.CODIGO_USO_PRINCI.equalsIgnoreCase(address.getUseCode())){
					 Direccion direccion = new Direccion();
					 direccion.setTipoDireccion(address.getSubType());
					 direccion.setCodigoUso(address.getUseCode());
					 direccion.setIdSecuencia(address.getSecuenceId());
					 direccion.setFlagDireccionEstandar(address.getIsStandard());
					 direccion.setDireccionEstandar(updatedDireccionEstandar(address.getStandardAddress()));
					 direccion.setVia(address.getRoadTypeName());
					 direccion.setDepartamento(address.getDepartment());
					 direccion.setProvincia(address.getProvince());
					 direccion.setDistrito(address.getDistrict());
					 direccion.setPais(address.getCountry());
					 direccion.setUbigeo(address.getUbigeoCode());
					 direccion.setAddressDetail(StringsUtil.formatAddressPepper(updatedDireccionEstandar(address.getStandardAddress())));
					 listDireccion.add(direccion);
				 }
			}
		 return listDireccion;
	}
	
	public static DireccionEstandar updatedDireccionEstandar(StandardAddress standardAddress){
		DireccionEstandar direccionEstandar = new DireccionEstandar();
		if(Validator.isNotNull(standardAddress)){
			direccionEstandar.setTipoVia(standardAddress.getRoadType().trim());
			direccionEstandar.setNombreVia(standardAddress.getRoadName().trim());
			direccionEstandar.setNumero(standardAddress.getRoadNumber().trim());
			direccionEstandar.setPisoLote(standardAddress.getApartment().trim());
			direccionEstandar.setInterior(standardAddress.getInside().trim());
			direccionEstandar.setReferencia(standardAddress.getReference().trim());
			direccionEstandar.setUrbanizacion(standardAddress.getLocation().trim());
			direccionEstandar.setManzana(standardAddress.getBlock().trim());
		}
		return direccionEstandar;
	}
	
	public static String getEmailApiPerson(List<Email> listEmail){
		String correo = null;
		for (Email email : listEmail) 
			if(Constantes.CODIGO_CORRESP_EMAPER.equalsIgnoreCase(email.getSubtype()))
				correo = email.getEmail();
		return correo;
	}
	
	public static String getTelephoneApiPerson(List<Telephone> telephone){
		String telf = null;
		for(Telephone lst:telephone)
			if(Constantes.TELEFONO_TIPO_CELULAR.equalsIgnoreCase(lst.getType()))
				telf = lst.getNumber();
		return telf;
	}
	
	public static Adrress agregarDireccionBdFraude(SolicitudBean pasoBean){
		Adrress adrress = null;
		String ubigeo = StringUtils.EMPTY;
		List<Direccion> listaDireccion = pasoBean.getClienteBean().getDirecciones().getDireccion();
		if(!listaDireccion.isEmpty() && Constantes.CODIGO_USO_PRINCI.equals(listaDireccion.get(0).getCodigoUso()) && Constantes.FLAG_DIRECCION_ESTANDAR.equals(listaDireccion.get(0).getFlagDireccionEstandar()))
			ubigeo = listaDireccion.get(0).getUbigeo();
		if(!ubigeo.isEmpty()){
			String codDepartamento = ubigeo.substring(0, 2);
			String codProvincia = ubigeo.substring(2, 4);
			String codDistrito = ubigeo.substring(4);
			adrress = new Adrress(codDepartamento, codProvincia, codDistrito);
		}
		return adrress;
	}
	public static ProductoBean setCampaignGeneralData(Boolean flagCampania, Campanium lead, ProductoBean productoBean){
		if(flagCampania){
			String[] detalleAdicional7 = Utils.reemplazarYSplit2(lead.getAdicionales().getCampoAdicional7());
			productoBean.setCodigoProducto(lead.getCabecera().getIdProductoPrincipal());						
			productoBean.setIdMoneda(lead.getDetalle().getIdMoneda());
			productoBean.setDiaDePago(detalleAdicional7[0]);
			productoBean.setFlujoCampana(Integer.parseInt(lead.getDetalle().getFlujoIngreso()));
			productoBean.setCodigoCampana(lead.getCabecera().getId());
			productoBean.setTipoClienteCampana(lead.getDetalle().getTipoCliente());
		}
		return productoBean;
	}
	
	public static ProductoBean setCampaignGeneralDataApi(Boolean flagCampania, Lead lead, ProductoBean productoBean){
		if(flagCampania){
			String[] detalleAdicional7 = Utils.reemplazarYSplit2(lead.getAdditionals().get(0).getItem7());
			productoBean.setIdMoneda(lead.getCurrency().getId());
			productoBean.setDiaDePago(detalleAdicional7[0]);
			productoBean.setFlujoCampana(Integer.parseInt(lead.getIncomeFlow()));
			productoBean.setCodigoCampana(lead.getCampaigns().get(0).getNumber());
			productoBean.setTipoClienteCampana(lead.getCustomerType());
		}
		return productoBean;
	}
	
	public static SolicitudBean setClientData(SolicitudBean pasoSession, ClienteBean clienteBean){
		if(Validator.isNotNull(clienteBean.getCompanyName()))
			pasoSession.getClienteBean().setCompanyName(Utils.reemplazarTildes(clienteBean.getCompanyName().trim()));
		if (Validator.isNotNull(clienteBean.getAddressDepartamento()) || Validator.isNotNull(clienteBean.getAddressProvincia()) || Validator.isNotNull(clienteBean.getAddressDistrito()))
			pasoSession.getClienteBean().getDirecciones().setDireccion(Utils.addStandardAddress(clienteBean));
		pasoSession.getClienteBean().setEmploymentStatus(Validator.isNotNull(clienteBean.getEmploymentStatus())?clienteBean.getEmploymentStatus():StringPool.BLANK);
		if(Validator.isNotNull(clienteBean.getOcupacion()))
			pasoSession.getClienteBean().setOcupacion(clienteBean.getOcupacion());
		if(Validator.isNotNull(clienteBean.getCivilStatus()))
				pasoSession.getClienteBean().setCivilStatus(clienteBean.getCivilStatus().substring(0, 1));
		List<Direccion> direcciones = pasoSession.getClienteBean().getDirecciones().getDireccion();
		if(!pasoSession.getClienteBean().getDirecciones().getDireccion().isEmpty()){
			pasoSession.getClienteBean().setAddressDepartamento(direcciones.get(0).getDepartamento().trim());
			pasoSession.getClienteBean().setAddressProvincia(direcciones.get(0).getProvincia().trim());
			pasoSession.getClienteBean().setAddressDistrito(direcciones.get(0).getDistrito().trim());
		}
		return pasoSession;
	}
	
	public static String setTextColor(SolicitudBean pasoBean){
		String brandType = pasoBean.getProductoBean().getMarcaProducto().concat(pasoBean.getProductoBean().getTipoProducto());
		String cssStyle = Constantes.CSS_WHITE_TEXT;
		switch (brandType) {
		case Constantes.VISA_CLASICA:
		case Constantes.VISA_PREMIA:
		case Constantes.VISA_ACCESS:
		case Constantes.AMERICAN_EXPRESS:
		case Constantes.THE_PLATINUM_CARD:
		case Constantes.AMERICAN_EXPRESS_BLUE:
		case Constantes.AMERICAN_EXPRESS_BLACK:
		case Constantes.MASTERCARD_CLASICA:
			cssStyle = Constantes.CSS_BLACK_TEXT;
			break;
		default:
			break;
		}
		return cssStyle;
	}
	
	public static String getTemplateContent(PortletRequest request, String idStep){
		String templateContent = StringPool.BLANK;
		switch (idStep) {
		case Constantes.URL_OTP_SMS:
		case Constantes.URL_OTP_EMAIL:
		case Constantes.URL_OTP_MULT:
		case Constantes.URL_OTP_VALIDACION:
		case Constantes.URL_OTP_BANCA_SMS:
			templateContent = request.getPreferences().getValue(RequestParam.OPT_CONTENT_LEFT, StringPool.BLANK);
			break;
		case Constantes.URL_RESULTADO_TC:
			templateContent = request.getPreferences().getValue(RequestParam.RESULTADO_TC_CONTENT_LEFT, StringPool.BLANK);
			break;
		case Constantes.URL_EQUIFAX:
			templateContent = request.getPreferences().getValue(RequestParam.EQUIFAX_CONTENT_LEFT, StringPool.BLANK);
			break;
		case Constantes.URL_CALIFICACION:
			templateContent = request.getPreferences().getValue(RequestParam.CALIFICACION_CONTENT_LEFT, StringPool.BLANK);
			break;
		case Constantes.URL_CONF_TC:
			templateContent = request.getPreferences().getValue(RequestParam.CONFIG_CONTENT_LEFT, StringPool.BLANK);
			break;
			/**/
		case Constantes.URL_FINAL:
			templateContent = request.getPreferences().getValue(RequestParam.RESUMEN_CONTENT_LEFT, StringPool.BLANK);
			break;
		default:
			break;
		}
		return templateContent;
	}
	
	public static Boolean validateCampaignType(String[] lstMarca, String campoAdicional, String[] detalleAdicional){
		return !StringPool.BLANK.equals(campoAdicional) && (detalleAdicional[0].equals(lstMarca[0]) || detalleAdicional[0].equals(lstMarca[1]) || detalleAdicional[0].equals(lstMarca[2]));
	}
	public static List<OfertaBean> addOfferListApiFirst(List<OfertaBean> lstOferta, String allowedCards, String marcaTarjeta, Lead lead){
		String[] lstMarca = Utils.reemplazarYSplit(allowedCards);
		if(marcaTarjeta.equals(lstMarca[0]) || marcaTarjeta.equals(lstMarca[1]) || marcaTarjeta.equals(lstMarca[2]))
			lstOferta.add(Utils.loadOfferApi(lead, null, Utils.reemplazarYSplit(lead.getAdditionals().get(0).getItem8())));
		return lstOferta;
	}
	
	public static List<OfertaBean> addOfferListApi(List<OfertaBean> lstOferta, String allowedCards, String campoAdicional, String campoAdicional24810){
		String[] lstMarca = Utils.reemplazarYSplit(allowedCards);
		String[] detalleAdicional = reemplazarYSplit(campoAdicional);
		String marcaTarjeta = detalleAdicional[0];
		if(!StringPool.BLANK.equals(campoAdicional) && (marcaTarjeta.equals(lstMarca[0]) || marcaTarjeta.equals(lstMarca[1]) || marcaTarjeta.equals(lstMarca[2])))
			lstOferta.add(Utils.loadOffer(null, Utils.reemplazarYSplit(campoAdicional), Utils.reemplazarYSplit(campoAdicional24810)));
		return lstOferta;
	}
	public static String getClientIp(HttpServletRequest request) {
        String remoteAddr = StringPool.BLANK;
        if (Validator.isNotNull(request)) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (Validator.isNull(remoteAddr) || StringPool.BLANK.equals(remoteAddr)) 
                remoteAddr = request.getRemoteAddr();
        }
        return remoteAddr;
    }
	
	public static ExpedienteBean obtenerContenidoComercio(PortletRequest request){
		String codigoServicioError = Constantes.HTML_ERROR_URL;
		String code = Constantes.REDIRECT_URL_BLANK;
		ExpedienteBean expedienteBean = new ExpedienteBean();
		ComercioBean comercioBean = new ComercioBean();
		try{
			removePortalSession(request);
			String urlActual = PortalUtil.getCurrentURL(request);
			String pasoSolicitud = HttpUtil.getParameter(urlActual, RequestParam.PASO, Boolean.FALSE);
			String hTienda = StringPool.BLANK;
			String hSede = StringPool.BLANK;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			if(themeDisplay.isSignedIn()){
				UsuarioSessionBean usuarioSessionBean = obtenerEstablecimientoTienda(request);
				hTienda = HttpUtil.getParameter(urlActual, RequestParam.HTIENDA, Boolean.FALSE).equalsIgnoreCase(replaceUrlCharacters(usuarioSessionBean.getEstablecimiento()))?usuarioSessionBean.getEstablecimiento():StringPool.BLANK;
				hSede = hTienda;
			}else if(validateBlankAndNull(HttpUtil.getParameter(urlActual, RequestParam.HTIENDA, Boolean.FALSE)) && validateBlankAndNull(HttpUtil.getParameter(urlActual, RequestParam.HSEDE, Boolean.FALSE))){
				hTienda = HttpUtil.getParameter(urlActual, RequestParam.HTIENDA, Boolean.FALSE);
				hSede = HttpUtil.getParameter(urlActual, RequestParam.HSEDE, Boolean.FALSE);
			}
			if(validateBlankAndNull(hTienda)&&validateBlankAndNull(hSede)){
				ParametroHijoPO ph = ParametroHijoPOLocalServiceUtil.getParametroHijoByCodigoAndCodigoPadreAndDato2(hSede, hTienda, Constantes.TBL_MAESTRO);
				if(Validator.isNotNull(ph)){
					comercioBean = loadCommerceData(ph, request);
					code = pasoSolicitud;
				}
			}
		}catch (SystemException e) {
			logger.error(e);
		}finally{
			expedienteBean.setHtmlCode(code);
			expedienteBean.setCodigoServicioError(codigoServicioError);
			expedienteBean.setComercioBean(comercioBean);
		}
		return expedienteBean;
	}
	
	public static SolicitudBean verDatosTarjeta(SolicitudBean pasoBeanSession, ComercioBean comercioBean, ResourceRequest request){
		String cvvCode = Utils.getCvvType(pasoBeanSession, comercioBean.getSecurityCode());
		if (Validator.isNotNull(cvvCode) && Validator.isNotNull(pasoBeanSession.getProductoBean().getNumTarjetaProvisional()) && Validator.isNotNull(pasoBeanSession.getProductoBean().getFechaVencProvisional())) {
			if(!pasoBeanSession.getProductoBean().getMarcaProducto().equals(Constantes.TARJETA_MARCA_AMEX))
				pasoBeanSession.getProductoBean().setCvvCode(cvvCode);
			TblCodigoPromocion tblCodigo = TblCodigoPromocionLocalServiceUtil.obtenerCodigoPromocionByComercio(Constantes.ESTADO_ACTIVO, comercioBean.getCommerceName().toUpperCase().trim());
			String promotionalCode = Validator.isNotNull(tblCodigo)?tblCodigo.getCodigo():comercioBean.getUniquePromotionalCode().trim();
			pasoBeanSession.getProductoBean().setUniquePromotionalCode(Constantes.CODIGO_PROMOCIONAL_NO_VALIDO.equalsIgnoreCase(promotionalCode)?StringPool.BLANK:promotionalCode);
			pasoBeanSession.getExpedienteBean().setEstado(Constantes.ESTADO_FINALIZADO);
			pasoBeanSession.getProductoBean().setEnvioEECC(Constantes.ENVIO_EECC_EMAIL);
			Boolean flagPAN = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.FLAG_PAN))?Boolean.FALSE:Boolean.TRUE;
			SessionKeys.addSessionPortal(request, SessionKeys.FLAG_PAN, flagPAN);
		}
		return pasoBeanSession;
	}
	
	public static ComercioBean loadCommerceData(ParametroHijoPO parametroHijoPlugin, PortletRequest request){
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		ComercioBean comercioBean = null;
		if(Validator.isNotNull(parametroHijoPlugin)){
			comercioBean = new ComercioBean(); 
			comercioBean.setCommerceName(parametroHijoPlugin.getDato1());
			comercioBean.setCommerceHash(parametroHijoPlugin.getDato2());
			comercioBean.setAllowedCards(parametroHijoPlugin.getDato3());
			comercioBean.setContentId(parametroHijoPlugin.getDato4());
			comercioBean.setOptMax(parametroHijoPlugin.getDato5());
			comercioBean.setEquifaxMax(parametroHijoPlugin.getDato6());
			comercioBean.setSecurityCode(parametroHijoPlugin.getDato7());
			DynamicTextBean textBean = loadCommerceContent(parametroHijoPlugin.getDato4(), themeDisplay);
			comercioBean.setDynamicTextBean(textBean);
			comercioBean.setUniquePromotionalCode(comercioBean.getDynamicTextBean().getV601009());
			comercioBean.setPepperOnlineType(parametroHijoPlugin.getDato8());
			addSessionCommerce(comercioBean, request);
		}
		return comercioBean;
	}
	
	public static void addSessionCommerce(ComercioBean comercioBean, PortletRequest request){	
		Gson gson = new Gson();
		String requestComercio = gson.toJson(comercioBean);
		String requestString = ConfiguracionRestUtil.limpiarJsonRequest(requestComercio);
		SessionKeys.addSessionPortal(request, SessionKeys.COMERCIO_BEAN, requestString);
	}
	
	/*ENCRIPTAR PASSWORD*/
	public static String encryptPassword(String passwordToHash) {
		StringBuilder builder=new StringBuilder(passwordToHash);
        String sCadenaInvertida=builder.reverse().toString();
		return getSecurePassword(sCadenaInvertida);
    }
	
	private static String getSecurePassword(String passwordToHash)
    {
    	String outHash = StringPool.BLANK;
        try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
	        md.update(passwordToHash.getBytes());
	        outHash = bytesToHex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
		}
        return outHash;
    }
	
	public static String bytesToHex(byte[] bytes) {
		StringBuilder result = new StringBuilder();
        for (byte byt : bytes) 
        	result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }
	
	public static String getTradeName(String stablishment, String store){
		String tradeName = StringPool.BLANK;
		if(!StringPool.BLANK.equalsIgnoreCase(stablishment) && !StringPool.BLANK.equalsIgnoreCase(store))
			tradeName = stablishment.concat(StringPool.SPACE).concat(StringPool.DASH).concat(StringPool.SPACE).concat(store);
		return tradeName;
	}
	
	public static void removePortalSession(PortletRequest request){
		SessionKeys.removeSessionPortlet(request, SessionKeys.COMERCIO_BEAN);
		SessionKeys.removeSessionPortlet(request, SessionKeys.PASO_BEAN);
		SessionKeys.removeSessionPortal(request, SessionKeys.FLAG_PAN);
	}
	
	public static UsuarioSessionBean obtenerEstablecimientoTienda(PortletRequest request){
		UsuarioSessionBean usuarioSessionBean = new UsuarioSessionBean();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		if(themeDisplay.isSignedIn()){
			String[] establecimiento = (String[])themeDisplay.getUser().getExpandoBridge().getAttribute(RequestParam.NAME_STABLISHMENT);
			String[] tienda = (String[])themeDisplay.getUser().getExpandoBridge().getAttribute(RequestParam.NAME_STORE);
			usuarioSessionBean.setEstablecimiento(establecimiento[0]);
			usuarioSessionBean.setTienda(tienda[0]);
			usuarioSessionBean.setVendedor(themeDisplay.getUser().getFullName());
			usuarioSessionBean.setUserId(themeDisplay.getUserId());
		}
		return usuarioSessionBean;
	}
	
	public static String getWebserviceExpectionError(WebServicesException e){
		return MsgError.MSJ_ERROR_P1.replace("##",e.getNombreWS()).concat(MsgError.MSJ_ERROR_P2).concat(e.getTrazaMessage());
	}
	
	public static Boolean validateOtpType(String dataType, SolicitudBean pasoBean){
		return dataType.equals(pasoBean.getProductoBean().getTipoSeguro()) || Constantes.TYPE_OTP_MULTIPLE.equals(pasoBean.getProductoBean().getTipoSeguro());
	}
	
	public static String getTradeUrl(UsuarioSessionBean usuarioSessionBean){
		return StringPool.BLANK.concat("?").concat(RequestParam.HTIENDA).concat("=").concat(replaceUrlCharacters(usuarioSessionBean.getEstablecimiento())).concat("&").concat(RequestParam.HSEDE).concat("=").concat(replaceUrlCharacters(usuarioSessionBean.getTienda()));
	}
	
	public static String replaceUrlCharacters(String cadena){
		String newValue = cadena.toLowerCase();
		if(validarVacioNulo(cadena))
			newValue = StringPool.BLANK;
		String[] oldSubs = {"á","é","í","ó","ú","ñ"," "};
		String[] newSubs = {"a","e","i","o","u","n","_"};
		return StringUtil.replace(newValue, oldSubs, newSubs);
	}
	
	public static String validateErrorType(String step){
		String messageResponse;
		switch (step) {
			case Constantes.URL_TIENE_TC:
			case Constantes.URL_NO_CAMPANIA:
			case Constantes.URL_BLACK_LIST:
			case Constantes.HTML_ERROR_NO_VIA:
			case Constantes.HTML_ERROR_TRAMTE_PROCESO:
			case Constantes.HTML_ERROR_LOGICA_REINGRESO:
			case Constantes.HTML_EQUIFAX_DESAPROBADO:
			case Constantes.HTML_TOKEN_INVALIDO:
			case Constantes.HTML_NO_CALIFICA:
				messageResponse = Constantes.APPLICATION_VALIDATION;
				break;
			case Constantes.HTML_ERROR_VALIDACION:
			case Constantes.HTML_ERROR_URL:
			case Constantes.HTML_ERROR:
			case Constantes.HTML_ERROR_REDIRECCION:
				messageResponse = Constantes.NO_AUTHORIZATION;
				break;
			default:
				messageResponse = Constantes.SERVICE_RESPONSE;
				break;
		}
		return messageResponse;
	}
	
	public static List<OfertaBean> addOfferListFirst(List<OfertaBean> lstOferta, String allowedCards, String marcaTarjeta, Campanium lead){
		String[] lstMarca = Utils.reemplazarYSplit(allowedCards);
		if(marcaTarjeta.equals(lstMarca[0]) || marcaTarjeta.equals(lstMarca[1]) || marcaTarjeta.equals(lstMarca[2]))
			lstOferta.add(loadOffer(lead, null, Utils.reemplazarYSplit(lead.getAdicionales().getCampoAdicional8())));
		return lstOferta;
	}
	
	public static List<OfertaBean> addOfferList(List<OfertaBean> lstOferta, String allowedCards, String campoAdicional, String campoAdicional24810){
		String[] lstMarca = Utils.reemplazarYSplit(allowedCards);
		String[] detalleAdicional = reemplazarYSplit(campoAdicional);
		String marcaTarjeta = detalleAdicional[0];
		if(!StringPool.BLANK.equals(campoAdicional) 
				&& (marcaTarjeta.equals(lstMarca[0]) || marcaTarjeta.equals(lstMarca[1]) || marcaTarjeta.equals(lstMarca[2])))
			lstOferta.add(loadOffer(null, Utils.reemplazarYSplit(campoAdicional), Utils.reemplazarYSplit(campoAdicional24810)));
		return lstOferta;
	}
}
