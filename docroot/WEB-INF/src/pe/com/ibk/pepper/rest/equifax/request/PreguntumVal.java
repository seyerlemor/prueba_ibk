
package pe.com.ibk.pepper.rest.equifax.request;


public class PreguntumVal {

    private String categoriaPregunta;
    private String numeroOpcion;
    private String numeroPregunta;

    public String getCategoriaPregunta() {
        return categoriaPregunta;
    }

    public void setCategoriaPregunta(String categoriaPregunta) {
        this.categoriaPregunta = categoriaPregunta;
    }

    public String getNumeroOpcion() {
        return numeroOpcion;
    }

    public void setNumeroOpcion(String numeroOpcion) {
        this.numeroOpcion = numeroOpcion;
    }

    public String getNumeroPregunta() {
        return numeroPregunta;
    }

    public void setNumeroPregunta(String numeroPregunta) {
        this.numeroPregunta = numeroPregunta;
    }
    

}
