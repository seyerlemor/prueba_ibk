package pe.com.ibk.pepper.util;


public final class RequestParam {
	
	public static final String ACTION = "action";
	public static final String ACT_CLAVE = "actClave";
	public static final String CAMPANIAS = "campanias";
	public static final String CODE = "code";
	public static final String CELL_PHONE_NUMBER = "cellPhoneNumber";
	public static final String CELL_PHONE_OPERATOR = "cellPhoneOperator";
	public static final String CEFL_URL = "ceflUrl";
	public static final String CLIENTE_EXPEDIENTE_BEAN = "clienteExpedienteBean";
	public static final String DATA = "data";
	public static final String DATES = "dates";
	public static final String DATOS_CLIENTE_BEAN = "datosClienteBean";
	public static final String DELIVERY = "delivery";
	public static final String DNI_NUMBER = "dniNumber";
	public static final String DOCUMENTO = "documento";
	public static final String ERRORS = "errors";
	public static final String EDIT_RUC= "editRuc";
	public static final String EMAIL = "email";
	public static final String FLAG_DATA_PROTECTION = "flagDataProtection";
	public static final String GO_TO_URL = "gotourl";
	public static final String IS_ACTIVE_EFL = "isActiveEFL";
	public static final String LIST_OPERADORES = "LIST_OPERADORES";
	public static final String NUMBER_CARD = "numberCard";
	public static final String MESSAGE = "message";
	public static final String NAME = "name";
	public static final String OPERATORS_LIST = "operatorsList";
	public static final String PASO = "paso";
	public static final String PARENT = "parent";
	public static final String PHOTO = "photo";
	public static final String STEP = "step";
	public static final String SCHEDULE = "schedule";
	public static final String TIPO = "tipo";
	public static final String REDIRECCIONAR_URL = "redireccionarURL";
	public static final String RESULTADO_EQUIFAX = "resultadoEquifax";
	public static final String RESULT = "result";
	public static final String VALIDATION_CODE = "validationCode";
	public static final String UNLOCK = "unlock";
	
	public static final String STC_VA_S_NOMBRE_FORM="nombreFormError";
	public static final String STC_VA_S_NOMBRE_FORM_DISABLE="nombreFormDisabled";
	public static final String CEFL_VA_S_NOMBRE_FORM="nameFormError";
	public static final String CEFL_VA_S_NOMBRE_FORM_FINAL="nameFormFinal";
	
	public static final String UTM_CAMPAIGN = "utm_campaign";
	public static final String UTM_CONTENT = "utm_content";
	public static final String UTM_SOURCE = "utm_source";
	public static final String UTM_MEDIUM = "utm_medium";
	
	public static final String CEFL_URL_PARAM_EXTERNAL_KEY = "externalKey";
	public static final String CEFL_URL_PARAM_BIRTHDAY = "birthday";
	public static final String CEFL_URL_PARAM_GENDER = "gender";
	public static final String CEFL_URL_PARAM_EXPIRED_URL = "expiredURL";
	public static final String CEFL_URL_PARAM_REDIRECT_URL = "redirectURL";
	
	public static final String TITLE = "titulo";
	public static final String TITLE_CARD = "tituloTarjeta";
	public static final String IMAGE_CARD = "imagenTarjeta";
	public static final String HEADER_DESCRIPT = "descripcionCab";
	public static final String FORMAT_CELL_NUMBRER = "formatCellNumber";
	public static final String FORMAT_EMAIL_NUMBRER = "formatEmailAddress";
	public static final String INTENTOS_EQUIFAX = "wsEquifaxIntentos";
	public static final String NCID = "ncid";
	public static final String HCID = "hcid";
	public static final String HTIENDA = "htienda";
	public static final String HSEDE = "hsede";
	public static final String COMMERCE_CONTENT = "contentComerce";
	public static final String ID_BRAND = "idBrand";
	
	public static final String RECATPCHA_RESPONSE = "g-recaptcha-response";
	public static final String STEP_CONTENT = "contentStep";
	public static final String ID_COMMERCE_DEFAULT = "inicioCommerceDefault";
	
	public static final String RADIO_OTP = "radioOTP";
	public static final String CHK_SEND_EMAIL = "chkSendEmail";
	public static final String LIST_DEPT_CACHE = "myListDepts-Cache";
	public static final String LIST_DEPT = "listDepts";
	public static final String R_CACHE = "-Cache";
	
	public static final String ENDPOINT_LISTA_NEGRA = "urlEndpointListaNegra";
	public static final String PATH_LISTA_NEGRA = "urlPathListaNegra";
	public static final String ENDPOINT_CAMPANIA = "urlEndpointCampania";
	public static final String PATH_CAMPANIA = "urlPathCampania";
	public static final String ENDPOINT_OBT_INFO_PERSONA = "urlEndpointObtenerInfoPersona";
	public static final String PATH_OBT_INFO_PERSONA = "urlPathObtenerInfoPersona";
	public static final String ENDPOINT_LOGICA_REINGRESO = "urlEndpointLogicaReingreso";
	public static final String PATH_LOGICA_REINGRESO = "urlPathLogicaReingreso";
	public static final String ENDPOINT_OBT_TARJETAS = "urlEndpointObtenerTarjetas";
	public static final String PATH_OBT_TARJETAS = "urlPathObtenerTarjetas";
	public static final String ENDPOINT_CONSULTAR_TARJETA = "urlEndpointConsultarTarjeta";
	public static final String PATH_CONSULTAR_TARJETA = "urlPathConsultarTarjeta";
	public static final String ENDPOINT_CONSULTAR_PREGUNTAS = "urlEndpointConsultarPreguntas";
	public static final String PATH_CONSULTAR_PREGUNTAS = "urlPathConsultarPreguntas";
	public static final String ENDPOINT_VALIDAR_PREGUNTAS = "urlEndpointValidarPreguntas";
	public static final String PATH_VALIDAR_PREGUNTAS = "urlPathValidarPreguntas";
	public static final String ENDPOINT_GENERAR_TOKEN = "urlEndpointGenerarToken";
	public static final String PATH_GENERAR_TOKEN = "urlPathGenerarToken";
	public static final String ENDPOINT_VALIDAR_TOKEN = "urlEndpointValidarToken";
	public static final String PATH_VALIDAR_TOKEN = "urlPathValidarToken";
	public static final String ENDPOINT_CALIFICACION = "urlEndpointCalificacion";
	public static final String PATH_CALIFICACION = "urlPathCalificacion";
	public static final String ENDPOINT_FECHA_HORA = "urlEndpointFechaHora";
	public static final String PATH_FECHA_HORA = "urlPathFechaHora";
	public static final String ENDPOINT_PEPPER_ONLINE_PROV = "urlEndpointAltaPepperOnline";
	public static final String PATH_PEPPER_ONLINE_PROV = "urlPathAltaPepperOnline";
	public static final String ENDPOINT_AUDITORIA = "urlEndpointAuditoria";
	public static final String PATH_AUDITORIA = "urlPathAuditoria";
	public static final String ENDPOINT_PEPPER_ONLINE_CARDLESS = "urlEndpointAltaCardless";
	public static final String PATH_PEPPER_ONLINE_CARDLESS = "urlPathAltaCardless";
	public static final String ENDPOINT_PEPPER_ONLINE_MATERIAL = "urlEndpointAltaFisico";
	public static final String PATH_PEPPER_ONLINE_MATERIAL = "urlPathAltaFisico";
	public static final String ENDPOINT_DOCUMENT_LEAD = "urlEndpointDocumentLead";
	public static final String PATH_DOCUMENT_LEAD = "urlPathDocumentLead";
	public static final String ENDPOINT_API_LEAD = "urlEndpointApiLead";
	public static final String PATH_API_LEAD = "urlPathApiLead";
	public static final String ENDPOINT_ANTIFRAUDE = "urlEndpointAntifraude";
	public static final String PATH_ANTIFRAUDE = "urlPathAntifraude";
	public static final String ENDPOINT_BANCA_SMS = "urlEndpointBancaSMS";
	public static final String PATH_BANCA_SMS = "urlPathBancaSMS";
	public static final String CHK_PROTECCION_DATOS = "checkboxProteccionDatos";
	
	public static final String OPT_CONTENT_LEFT="otpIdContentLeft";
	public static final String RESULTADO_TC_CONTENT_LEFT="resultadotcIdContentLeft";
	public static final String EQUIFAX_CONTENT_LEFT="equifaxIdContentLeft";
	public static final String CONFIG_CONTENT_LEFT="configuracionIdContentLeft";
	public static final String CALIFICACION_CONTENT_LEFT="resumenIdContentLeft";
	public static final String RESUMEN_CONTENT_LEFT = "calificacionIdContentLeft";
	
	public static final String LEAD_CAMPANIA = "leadCampania";
	public static final String REST_VENTA_TC_FECHA_PAGO = "restVentaTcFechaPago";
	public static final String FORMAT_CREDIT_LINE = "formatCreditLine";
	public static final String FECHA_OPERACION = "fechaOperacion";
	public static final String TIPO_MENSAJE = "tipoMensaje";
	public static final String TIPO_CAMPANIA = "generalTipoCampania";
	public static final String TIPO_FECHA_HORA = "generalTipoFechaHora";
	
	/*VARIABLES CONTENIDO TEXTOS DINAMICOS*/
	public static final String COMMERCE_TEXT = "commerceText";
	
	public static final String DYNAMIC_TXT_V101001 = "v1010_01";
	public static final String DYNAMIC_TXT_V101002 = "v1010_02";
	public static final String DYNAMIC_TXT_V101003 = "v1010_03";
	public static final String DYNAMIC_TXT_V101004 = "v1010_04";
	public static final String DYNAMIC_TXT_V101005 = "v1010_05";
	public static final String DYNAMIC_TXT_V101006 = "v1010_06";
	
	public static final String DYNAMIC_TXT_V201001 = "v2010_01";
	public static final String DYNAMIC_TXT_V201002 = "v2010_02";
	public static final String DYNAMIC_TXT_V201003 = "v2010_03";
	public static final String DYNAMIC_TXT_V201004 = "v2010_04";
	
	public static final String DYNAMIC_TXT_V401001 = "v4010_01";
	public static final String DYNAMIC_TXT_V401002 = "v4010_02";
	public static final String DYNAMIC_TXT_V401003 = "v4010_03";
	public static final String DYNAMIC_TXT_V401004 = "v4010_04";
	public static final String DYNAMIC_TXT_V401005 = "v4010_05";
	
	public static final String DYNAMIC_TXT_V501001 = "v5010_01";
	public static final String DYNAMIC_TXT_V501002 = "v5010_02";
	public static final String DYNAMIC_TXT_V501003 = "v5010_03";
	public static final String DYNAMIC_TXT_V501004 = "v5010_04";
	public static final String DYNAMIC_TXT_V501005 = "v5010_05";
	public static final String DYNAMIC_TXT_V501006 = "v5010_06";
	public static final String DYNAMIC_TXT_V501007 = "v5010_07";
	public static final String DYNAMIC_TXT_V501008 = "v5010_08";
	public static final String DYNAMIC_TXT_V501009 = "v5010_09";
	
	public static final String DYNAMIC_TXT_V601001 = "v6010_01";
	public static final String DYNAMIC_TXT_V601002 = "v6010_02";
	public static final String DYNAMIC_TXT_V601003 = "v6010_03";
	public static final String DYNAMIC_TXT_V601004 = "v6010_04";
	public static final String DYNAMIC_TXT_V601005 = "v6010_05";
	public static final String DYNAMIC_TXT_V601006 = "v6010_06";
	public static final String DYNAMIC_TXT_V601007 = "v6010_07";
	public static final String DYNAMIC_TXT_V601008 = "v6010_08";
	public static final String DYNAMIC_TXT_V601009 = "v6010_09";
	
	/**/
	public static final String NAME_STORE = "tienda";
	public static final String NAME_STABLISHMENT = "establecimiento";
	public static final String TRADE_NAME = "tradeName";
	public static final String TRADE_URL = "tradeUrl";
	public static final String PEPPER_TYPE = "generalPepperType";
	public static final String VALIDATE_PEPPER_TYPE = "validatePepperType";
	public static final String VALIDATE_LINE = "validateLine";
	public static final String FORMAT_EXPIRATION_DATE = "formatExpirationDate";
	public static final String CSS_TEXT_COLOR = "cssTextColor";
	public static final String PASO_BEAN = "pasoBean";
	public static final String CAMPAIGNS_OFFERS = "offers";
	
	/*Parametros de Servicios*/
	public static final String HTTP_HEADER = "HttpHeader";
	public static final String QUERY_PARAMS= "QueryParams";
	public static final String TEMPLATE_PARAMS= "TemplateParams";
	
	private RequestParam(){}
}
