/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.Ubigeo;

import java.util.List;

/**
 * The persistence utility for the ubigeo service. This utility wraps {@link UbigeoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see UbigeoPersistence
 * @see UbigeoPersistenceImpl
 * @generated
 */
public class UbigeoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Ubigeo ubigeo) {
		getPersistence().clearCache(ubigeo);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Ubigeo> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Ubigeo> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Ubigeo> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Ubigeo update(Ubigeo ubigeo) throws SystemException {
		return getPersistence().update(ubigeo);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Ubigeo update(Ubigeo ubigeo, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(ubigeo, serviceContext);
	}

	/**
	* Returns all the ubigeos where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @return the matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByProv(
		java.lang.String codProvincia)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByProv(codProvincia);
	}

	/**
	* Returns a range of all the ubigeos where codProvincia = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codProvincia the cod provincia
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByProv(
		java.lang.String codProvincia, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByProv(codProvincia, start, end);
	}

	/**
	* Returns an ordered range of all the ubigeos where codProvincia = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codProvincia the cod provincia
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByProv(
		java.lang.String codProvincia, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByProv(codProvincia, start, end, orderByComparator);
	}

	/**
	* Returns the first ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByProv_First(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence().findByProv_First(codProvincia, orderByComparator);
	}

	/**
	* Returns the first ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByProv_First(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByProv_First(codProvincia, orderByComparator);
	}

	/**
	* Returns the last ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByProv_Last(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence().findByProv_Last(codProvincia, orderByComparator);
	}

	/**
	* Returns the last ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByProv_Last(
		java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByProv_Last(codProvincia, orderByComparator);
	}

	/**
	* Returns the ubigeos before and after the current ubigeo in the ordered set where codProvincia = &#63;.
	*
	* @param idUbigeo the primary key of the current ubigeo
	* @param codProvincia the cod provincia
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo[] findByProv_PrevAndNext(
		long idUbigeo, java.lang.String codProvincia,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByProv_PrevAndNext(idUbigeo, codProvincia,
			orderByComparator);
	}

	/**
	* Removes all the ubigeos where codProvincia = &#63; from the database.
	*
	* @param codProvincia the cod provincia
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByProv(java.lang.String codProvincia)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByProv(codProvincia);
	}

	/**
	* Returns the number of ubigeos where codProvincia = &#63;.
	*
	* @param codProvincia the cod provincia
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByProv(java.lang.String codProvincia)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByProv(codProvincia);
	}

	/**
	* Returns all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_N_P_D(codDepartamento, codProvincia, codDistrito);
	}

	/**
	* Returns a range of all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_N_P_D(codDepartamento, codProvincia, codDistrito,
			start, end);
	}

	/**
	* Returns an ordered range of all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_N_P_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_N_P_D(codDepartamento, codProvincia, codDistrito,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByD_N_P_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByD_N_P_D_First(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByD_N_P_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByD_N_P_D_First(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByD_N_P_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByD_N_P_D_Last(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByD_N_P_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByD_N_P_D_Last(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the ubigeos before and after the current ubigeo in the ordered set where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param idUbigeo the primary key of the current ubigeo
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo[] findByD_N_P_D_PrevAndNext(
		long idUbigeo, java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByD_N_P_D_PrevAndNext(idUbigeo, codDepartamento,
			codProvincia, codDistrito, orderByComparator);
	}

	/**
	* Removes all the ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63; from the database.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByD_N_P_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByD_N_P_D(codDepartamento, codProvincia, codDistrito);
	}

	/**
	* Returns the number of ubigeos where codDepartamento = &#63; and codProvincia &ne; &#63; and codDistrito = &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByD_N_P_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByD_N_P_D(codDepartamento, codProvincia, codDistrito);
	}

	/**
	* Returns all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_P_N_D(codDepartamento, codProvincia, codDistrito);
	}

	/**
	* Returns a range of all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_P_N_D(codDepartamento, codProvincia, codDistrito,
			start, end);
	}

	/**
	* Returns an ordered range of all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findByD_P_N_D(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_P_N_D(codDepartamento, codProvincia, codDistrito,
			start, end, orderByComparator);
	}

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByD_P_N_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByD_P_N_D_First(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the first ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByD_P_N_D_First(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByD_P_N_D_First(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByD_P_N_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByD_P_N_D_Last(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the last ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByD_P_N_D_Last(
		java.lang.String codDepartamento, java.lang.String codProvincia,
		java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByD_P_N_D_Last(codDepartamento, codProvincia,
			codDistrito, orderByComparator);
	}

	/**
	* Returns the ubigeos before and after the current ubigeo in the ordered set where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param idUbigeo the primary key of the current ubigeo
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo[] findByD_P_N_D_PrevAndNext(
		long idUbigeo, java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence()
				   .findByD_P_N_D_PrevAndNext(idUbigeo, codDepartamento,
			codProvincia, codDistrito, orderByComparator);
	}

	/**
	* Removes all the ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63; from the database.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByD_P_N_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByD_P_N_D(codDepartamento, codProvincia, codDistrito);
	}

	/**
	* Returns the number of ubigeos where codDepartamento = &#63; and codProvincia = &#63; and codDistrito &ne; &#63;.
	*
	* @param codDepartamento the cod departamento
	* @param codProvincia the cod provincia
	* @param codDistrito the cod distrito
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByD_P_N_D(java.lang.String codDepartamento,
		java.lang.String codProvincia, java.lang.String codDistrito)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByD_P_N_D(codDepartamento, codProvincia, codDistrito);
	}

	/**
	* Returns the ubigeo where codigo = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUbigeoException} if it could not be found.
	*
	* @param codigo the codigo
	* @return the matching ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByN_C_U(
		java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence().findByN_C_U(codigo);
	}

	/**
	* Returns the ubigeo where codigo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigo the codigo
	* @return the matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByN_C_U(
		java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByN_C_U(codigo);
	}

	/**
	* Returns the ubigeo where codigo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigo the codigo
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching ubigeo, or <code>null</code> if a matching ubigeo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByN_C_U(
		java.lang.String codigo, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByN_C_U(codigo, retrieveFromCache);
	}

	/**
	* Removes the ubigeo where codigo = &#63; from the database.
	*
	* @param codigo the codigo
	* @return the ubigeo that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo removeByN_C_U(
		java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence().removeByN_C_U(codigo);
	}

	/**
	* Returns the number of ubigeos where codigo = &#63;.
	*
	* @param codigo the codigo
	* @return the number of matching ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByN_C_U(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByN_C_U(codigo);
	}

	/**
	* Caches the ubigeo in the entity cache if it is enabled.
	*
	* @param ubigeo the ubigeo
	*/
	public static void cacheResult(pe.com.ibk.pepper.model.Ubigeo ubigeo) {
		getPersistence().cacheResult(ubigeo);
	}

	/**
	* Caches the ubigeos in the entity cache if it is enabled.
	*
	* @param ubigeos the ubigeos
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Ubigeo> ubigeos) {
		getPersistence().cacheResult(ubigeos);
	}

	/**
	* Creates a new ubigeo with the primary key. Does not add the ubigeo to the database.
	*
	* @param idUbigeo the primary key for the new ubigeo
	* @return the new ubigeo
	*/
	public static pe.com.ibk.pepper.model.Ubigeo create(long idUbigeo) {
		return getPersistence().create(idUbigeo);
	}

	/**
	* Removes the ubigeo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo that was removed
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo remove(long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence().remove(idUbigeo);
	}

	public static pe.com.ibk.pepper.model.Ubigeo updateImpl(
		pe.com.ibk.pepper.model.Ubigeo ubigeo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(ubigeo);
	}

	/**
	* Returns the ubigeo with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchUbigeoException} if it could not be found.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo
	* @throws pe.com.ibk.pepper.NoSuchUbigeoException if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo findByPrimaryKey(long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchUbigeoException {
		return getPersistence().findByPrimaryKey(idUbigeo);
	}

	/**
	* Returns the ubigeo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idUbigeo the primary key of the ubigeo
	* @return the ubigeo, or <code>null</code> if a ubigeo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Ubigeo fetchByPrimaryKey(
		long idUbigeo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idUbigeo);
	}

	/**
	* Returns all the ubigeos.
	*
	* @return the ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the ubigeos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @return the range of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the ubigeos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UbigeoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ubigeos
	* @param end the upper bound of the range of ubigeos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Ubigeo> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the ubigeos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of ubigeos.
	*
	* @return the number of ubigeos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static UbigeoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (UbigeoPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					UbigeoPersistence.class.getName());

			ReferenceRegistry.registerReference(UbigeoUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(UbigeoPersistence persistence) {
	}

	private static UbigeoPersistence _persistence;
}