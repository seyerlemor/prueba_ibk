/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.TblMaestroLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class TblMaestroClp extends BaseModelImpl<TblMaestro>
	implements TblMaestro {
	public TblMaestroClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return TblMaestro.class;
	}

	@Override
	public String getModelClassName() {
		return TblMaestro.class.getName();
	}

	@Override
	public int getPrimaryKey() {
		return _idMaestro;
	}

	@Override
	public void setPrimaryKey(int primaryKey) {
		setIdMaestro(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idMaestro;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idMaestro", getIdMaestro());
		attributes.put("nombreComercio", getNombreComercio());
		attributes.put("idHash", getIdHash());
		attributes.put("tipoTarjeta", getTipoTarjeta());
		attributes.put("urlImagen", getUrlImagen());
		attributes.put("topeOtp", getTopeOtp());
		attributes.put("topeEquifax", getTopeEquifax());
		attributes.put("codigoSeguridad", getCodigoSeguridad());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idMaestro = (Integer)attributes.get("idMaestro");

		if (idMaestro != null) {
			setIdMaestro(idMaestro);
		}

		String nombreComercio = (String)attributes.get("nombreComercio");

		if (nombreComercio != null) {
			setNombreComercio(nombreComercio);
		}

		String idHash = (String)attributes.get("idHash");

		if (idHash != null) {
			setIdHash(idHash);
		}

		String tipoTarjeta = (String)attributes.get("tipoTarjeta");

		if (tipoTarjeta != null) {
			setTipoTarjeta(tipoTarjeta);
		}

		Date urlImagen = (Date)attributes.get("urlImagen");

		if (urlImagen != null) {
			setUrlImagen(urlImagen);
		}

		String topeOtp = (String)attributes.get("topeOtp");

		if (topeOtp != null) {
			setTopeOtp(topeOtp);
		}

		String topeEquifax = (String)attributes.get("topeEquifax");

		if (topeEquifax != null) {
			setTopeEquifax(topeEquifax);
		}

		String codigoSeguridad = (String)attributes.get("codigoSeguridad");

		if (codigoSeguridad != null) {
			setCodigoSeguridad(codigoSeguridad);
		}
	}

	@Override
	public int getIdMaestro() {
		return _idMaestro;
	}

	@Override
	public void setIdMaestro(int idMaestro) {
		_idMaestro = idMaestro;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setIdMaestro", int.class);

				method.invoke(_tblMaestroRemoteModel, idMaestro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreComercio() {
		return _nombreComercio;
	}

	@Override
	public void setNombreComercio(String nombreComercio) {
		_nombreComercio = nombreComercio;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreComercio",
						String.class);

				method.invoke(_tblMaestroRemoteModel, nombreComercio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdHash() {
		return _idHash;
	}

	@Override
	public void setIdHash(String idHash) {
		_idHash = idHash;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setIdHash", String.class);

				method.invoke(_tblMaestroRemoteModel, idHash);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoTarjeta() {
		return _tipoTarjeta;
	}

	@Override
	public void setTipoTarjeta(String tipoTarjeta) {
		_tipoTarjeta = tipoTarjeta;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoTarjeta", String.class);

				method.invoke(_tblMaestroRemoteModel, tipoTarjeta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getUrlImagen() {
		return _urlImagen;
	}

	@Override
	public void setUrlImagen(Date urlImagen) {
		_urlImagen = urlImagen;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setUrlImagen", Date.class);

				method.invoke(_tblMaestroRemoteModel, urlImagen);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTopeOtp() {
		return _topeOtp;
	}

	@Override
	public void setTopeOtp(String topeOtp) {
		_topeOtp = topeOtp;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setTopeOtp", String.class);

				method.invoke(_tblMaestroRemoteModel, topeOtp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTopeEquifax() {
		return _topeEquifax;
	}

	@Override
	public void setTopeEquifax(String topeEquifax) {
		_topeEquifax = topeEquifax;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setTopeEquifax", String.class);

				method.invoke(_tblMaestroRemoteModel, topeEquifax);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoSeguridad() {
		return _codigoSeguridad;
	}

	@Override
	public void setCodigoSeguridad(String codigoSeguridad) {
		_codigoSeguridad = codigoSeguridad;

		if (_tblMaestroRemoteModel != null) {
			try {
				Class<?> clazz = _tblMaestroRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoSeguridad",
						String.class);

				method.invoke(_tblMaestroRemoteModel, codigoSeguridad);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTblMaestroRemoteModel() {
		return _tblMaestroRemoteModel;
	}

	public void setTblMaestroRemoteModel(BaseModel<?> tblMaestroRemoteModel) {
		_tblMaestroRemoteModel = tblMaestroRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _tblMaestroRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_tblMaestroRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TblMaestroLocalServiceUtil.addTblMaestro(this);
		}
		else {
			TblMaestroLocalServiceUtil.updateTblMaestro(this);
		}
	}

	@Override
	public TblMaestro toEscapedModel() {
		return (TblMaestro)ProxyUtil.newProxyInstance(TblMaestro.class.getClassLoader(),
			new Class[] { TblMaestro.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TblMaestroClp clone = new TblMaestroClp();

		clone.setIdMaestro(getIdMaestro());
		clone.setNombreComercio(getNombreComercio());
		clone.setIdHash(getIdHash());
		clone.setTipoTarjeta(getTipoTarjeta());
		clone.setUrlImagen(getUrlImagen());
		clone.setTopeOtp(getTopeOtp());
		clone.setTopeEquifax(getTopeEquifax());
		clone.setCodigoSeguridad(getCodigoSeguridad());

		return clone;
	}

	@Override
	public int compareTo(TblMaestro tblMaestro) {
		int primaryKey = tblMaestro.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TblMaestroClp)) {
			return false;
		}

		TblMaestroClp tblMaestro = (TblMaestroClp)obj;

		int primaryKey = tblMaestro.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{idMaestro=");
		sb.append(getIdMaestro());
		sb.append(", nombreComercio=");
		sb.append(getNombreComercio());
		sb.append(", idHash=");
		sb.append(getIdHash());
		sb.append(", tipoTarjeta=");
		sb.append(getTipoTarjeta());
		sb.append(", urlImagen=");
		sb.append(getUrlImagen());
		sb.append(", topeOtp=");
		sb.append(getTopeOtp());
		sb.append(", topeEquifax=");
		sb.append(getTopeEquifax());
		sb.append(", codigoSeguridad=");
		sb.append(getCodigoSeguridad());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(28);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.TblMaestro");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idMaestro</column-name><column-value><![CDATA[");
		sb.append(getIdMaestro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreComercio</column-name><column-value><![CDATA[");
		sb.append(getNombreComercio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idHash</column-name><column-value><![CDATA[");
		sb.append(getIdHash());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoTarjeta</column-name><column-value><![CDATA[");
		sb.append(getTipoTarjeta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>urlImagen</column-name><column-value><![CDATA[");
		sb.append(getUrlImagen());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>topeOtp</column-name><column-value><![CDATA[");
		sb.append(getTopeOtp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>topeEquifax</column-name><column-value><![CDATA[");
		sb.append(getTopeEquifax());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoSeguridad</column-name><column-value><![CDATA[");
		sb.append(getCodigoSeguridad());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _idMaestro;
	private String _nombreComercio;
	private String _idHash;
	private String _tipoTarjeta;
	private Date _urlImagen;
	private String _topeOtp;
	private String _topeEquifax;
	private String _codigoSeguridad;
	private BaseModel<?> _tblMaestroRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}