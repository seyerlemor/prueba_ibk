/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.Expediente;

import java.util.List;

/**
 * The persistence utility for the expediente service. This utility wraps {@link ExpedientePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ExpedientePersistence
 * @see ExpedientePersistenceImpl
 * @generated
 */
public class ExpedienteUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Expediente expediente) {
		getPersistence().clearCache(expediente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Expediente> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Expediente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Expediente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Expediente update(Expediente expediente)
		throws SystemException {
		return getPersistence().update(expediente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Expediente update(Expediente expediente,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(expediente, serviceContext);
	}

	/**
	* Returns the expediente where documento = &#63; and fechaRegistro = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	*
	* @param documento the documento
	* @param fechaRegistro the fecha registro
	* @return the matching expediente
	* @throws pe.com.ibk.pepper.NoSuchExpedienteException if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente findByE_D_F(
		java.lang.String documento, java.util.Date fechaRegistro)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().findByE_D_F(documento, fechaRegistro);
	}

	/**
	* Returns the expediente where documento = &#63; and fechaRegistro = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param documento the documento
	* @param fechaRegistro the fecha registro
	* @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByE_D_F(
		java.lang.String documento, java.util.Date fechaRegistro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByE_D_F(documento, fechaRegistro);
	}

	/**
	* Returns the expediente where documento = &#63; and fechaRegistro = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param documento the documento
	* @param fechaRegistro the fecha registro
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByE_D_F(
		java.lang.String documento, java.util.Date fechaRegistro,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByE_D_F(documento, fechaRegistro, retrieveFromCache);
	}

	/**
	* Removes the expediente where documento = &#63; and fechaRegistro = &#63; from the database.
	*
	* @param documento the documento
	* @param fechaRegistro the fecha registro
	* @return the expediente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente removeByE_D_F(
		java.lang.String documento, java.util.Date fechaRegistro)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().removeByE_D_F(documento, fechaRegistro);
	}

	/**
	* Returns the number of expedientes where documento = &#63; and fechaRegistro = &#63;.
	*
	* @param documento the documento
	* @param fechaRegistro the fecha registro
	* @return the number of matching expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByE_D_F(java.lang.String documento,
		java.util.Date fechaRegistro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByE_D_F(documento, fechaRegistro);
	}

	/**
	* Returns the expediente where documento = &#63; and periodoRegistro = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	*
	* @param documento the documento
	* @param periodoRegistro the periodo registro
	* @return the matching expediente
	* @throws pe.com.ibk.pepper.NoSuchExpedienteException if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente findByE_D_P(
		java.lang.String documento, java.lang.String periodoRegistro)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().findByE_D_P(documento, periodoRegistro);
	}

	/**
	* Returns the expediente where documento = &#63; and periodoRegistro = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param documento the documento
	* @param periodoRegistro the periodo registro
	* @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByE_D_P(
		java.lang.String documento, java.lang.String periodoRegistro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByE_D_P(documento, periodoRegistro);
	}

	/**
	* Returns the expediente where documento = &#63; and periodoRegistro = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param documento the documento
	* @param periodoRegistro the periodo registro
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByE_D_P(
		java.lang.String documento, java.lang.String periodoRegistro,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByE_D_P(documento, periodoRegistro, retrieveFromCache);
	}

	/**
	* Removes the expediente where documento = &#63; and periodoRegistro = &#63; from the database.
	*
	* @param documento the documento
	* @param periodoRegistro the periodo registro
	* @return the expediente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente removeByE_D_P(
		java.lang.String documento, java.lang.String periodoRegistro)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().removeByE_D_P(documento, periodoRegistro);
	}

	/**
	* Returns the number of expedientes where documento = &#63; and periodoRegistro = &#63;.
	*
	* @param documento the documento
	* @param periodoRegistro the periodo registro
	* @return the number of matching expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByE_D_P(java.lang.String documento,
		java.lang.String periodoRegistro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByE_D_P(documento, periodoRegistro);
	}

	/**
	* Returns the expediente where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	*
	* @param idExpediente the id expediente
	* @return the matching expediente
	* @throws pe.com.ibk.pepper.NoSuchExpedienteException if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente findByE_ID(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().findByE_ID(idExpediente);
	}

	/**
	* Returns the expediente where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idExpediente the id expediente
	* @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByE_ID(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByE_ID(idExpediente);
	}

	/**
	* Returns the expediente where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idExpediente the id expediente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching expediente, or <code>null</code> if a matching expediente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByE_ID(
		long idExpediente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByE_ID(idExpediente, retrieveFromCache);
	}

	/**
	* Removes the expediente where idExpediente = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @return the expediente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente removeByE_ID(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().removeByE_ID(idExpediente);
	}

	/**
	* Returns the number of expedientes where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the number of matching expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByE_ID(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByE_ID(idExpediente);
	}

	/**
	* Caches the expediente in the entity cache if it is enabled.
	*
	* @param expediente the expediente
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.Expediente expediente) {
		getPersistence().cacheResult(expediente);
	}

	/**
	* Caches the expedientes in the entity cache if it is enabled.
	*
	* @param expedientes the expedientes
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Expediente> expedientes) {
		getPersistence().cacheResult(expedientes);
	}

	/**
	* Creates a new expediente with the primary key. Does not add the expediente to the database.
	*
	* @param idExpediente the primary key for the new expediente
	* @return the new expediente
	*/
	public static pe.com.ibk.pepper.model.Expediente create(long idExpediente) {
		return getPersistence().create(idExpediente);
	}

	/**
	* Removes the expediente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idExpediente the primary key of the expediente
	* @return the expediente that was removed
	* @throws pe.com.ibk.pepper.NoSuchExpedienteException if a expediente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente remove(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().remove(idExpediente);
	}

	public static pe.com.ibk.pepper.model.Expediente updateImpl(
		pe.com.ibk.pepper.model.Expediente expediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(expediente);
	}

	/**
	* Returns the expediente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchExpedienteException} if it could not be found.
	*
	* @param idExpediente the primary key of the expediente
	* @return the expediente
	* @throws pe.com.ibk.pepper.NoSuchExpedienteException if a expediente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente findByPrimaryKey(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchExpedienteException {
		return getPersistence().findByPrimaryKey(idExpediente);
	}

	/**
	* Returns the expediente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idExpediente the primary key of the expediente
	* @return the expediente, or <code>null</code> if a expediente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Expediente fetchByPrimaryKey(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idExpediente);
	}

	/**
	* Returns all the expedientes.
	*
	* @return the expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Expediente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the expedientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of expedientes
	* @param end the upper bound of the range of expedientes (not inclusive)
	* @return the range of expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Expediente> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the expedientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of expedientes
	* @param end the upper bound of the range of expedientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Expediente> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the expedientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of expedientes.
	*
	* @return the number of expedientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ExpedientePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ExpedientePersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ExpedientePersistence.class.getName());

			ReferenceRegistry.registerReference(ExpedienteUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ExpedientePersistence persistence) {
	}

	private static ExpedientePersistence _persistence;
}