/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Producto}.
 * </p>
 *
 * @author Interbank
 * @see Producto
 * @generated
 */
public class ProductoWrapper implements Producto, ModelWrapper<Producto> {
	public ProductoWrapper(Producto producto) {
		_producto = producto;
	}

	@Override
	public Class<?> getModelClass() {
		return Producto.class;
	}

	@Override
	public String getModelClassName() {
		return Producto.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idProducto", getIdProducto());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("tipoProducto", getTipoProducto());
		attributes.put("codigoProducto", getCodigoProducto());
		attributes.put("moneda", getMoneda());
		attributes.put("marcaProducto", getMarcaProducto());
		attributes.put("lineaCredito", getLineaCredito());
		attributes.put("diaPago", getDiaPago());
		attributes.put("tasaProducto", getTasaProducto());
		attributes.put("nombreTitularProducto", getNombreTitularProducto());
		attributes.put("tipoSeguro", getTipoSeguro());
		attributes.put("costoSeguro", getCostoSeguro());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("flagEnvioEECCFisico", getFlagEnvioEECCFisico());
		attributes.put("flagEnvioEECCEmail", getFlagEnvioEECCEmail());
		attributes.put("flagTerminos", getFlagTerminos());
		attributes.put("flagCargoCompra", getFlagCargoCompra());
		attributes.put("claveventa", getClaveventa());
		attributes.put("codigoOperacion", getCodigoOperacion());
		attributes.put("fechaHoraOperacion", getFechaHoraOperacion());
		attributes.put("codigoUnico", getCodigoUnico());
		attributes.put("numTarjetaTitular", getNumTarjetaTitular());
		attributes.put("numCuenta", getNumCuenta());
		attributes.put("fechaAltaTitular", getFechaAltaTitular());
		attributes.put("fechaVencTitular", getFechaVencTitular());
		attributes.put("numTarjetaProvisional", getNumTarjetaProvisional());
		attributes.put("fechaAltaProvisional", getFechaAltaProvisional());
		attributes.put("fechaVencProvisional", getFechaVencProvisional());
		attributes.put("RazonNoUsoTarjeta", getRazonNoUsoTarjeta());
		attributes.put("Motivo", getMotivo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idProducto = (Long)attributes.get("idProducto");

		if (idProducto != null) {
			setIdProducto(idProducto);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String tipoProducto = (String)attributes.get("tipoProducto");

		if (tipoProducto != null) {
			setTipoProducto(tipoProducto);
		}

		String codigoProducto = (String)attributes.get("codigoProducto");

		if (codigoProducto != null) {
			setCodigoProducto(codigoProducto);
		}

		String moneda = (String)attributes.get("moneda");

		if (moneda != null) {
			setMoneda(moneda);
		}

		String marcaProducto = (String)attributes.get("marcaProducto");

		if (marcaProducto != null) {
			setMarcaProducto(marcaProducto);
		}

		String lineaCredito = (String)attributes.get("lineaCredito");

		if (lineaCredito != null) {
			setLineaCredito(lineaCredito);
		}

		String diaPago = (String)attributes.get("diaPago");

		if (diaPago != null) {
			setDiaPago(diaPago);
		}

		String tasaProducto = (String)attributes.get("tasaProducto");

		if (tasaProducto != null) {
			setTasaProducto(tasaProducto);
		}

		String nombreTitularProducto = (String)attributes.get(
				"nombreTitularProducto");

		if (nombreTitularProducto != null) {
			setNombreTitularProducto(nombreTitularProducto);
		}

		String tipoSeguro = (String)attributes.get("tipoSeguro");

		if (tipoSeguro != null) {
			setTipoSeguro(tipoSeguro);
		}

		String costoSeguro = (String)attributes.get("costoSeguro");

		if (costoSeguro != null) {
			setCostoSeguro(costoSeguro);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String flagEnvioEECCFisico = (String)attributes.get(
				"flagEnvioEECCFisico");

		if (flagEnvioEECCFisico != null) {
			setFlagEnvioEECCFisico(flagEnvioEECCFisico);
		}

		String flagEnvioEECCEmail = (String)attributes.get("flagEnvioEECCEmail");

		if (flagEnvioEECCEmail != null) {
			setFlagEnvioEECCEmail(flagEnvioEECCEmail);
		}

		String flagTerminos = (String)attributes.get("flagTerminos");

		if (flagTerminos != null) {
			setFlagTerminos(flagTerminos);
		}

		String flagCargoCompra = (String)attributes.get("flagCargoCompra");

		if (flagCargoCompra != null) {
			setFlagCargoCompra(flagCargoCompra);
		}

		String claveventa = (String)attributes.get("claveventa");

		if (claveventa != null) {
			setClaveventa(claveventa);
		}

		String codigoOperacion = (String)attributes.get("codigoOperacion");

		if (codigoOperacion != null) {
			setCodigoOperacion(codigoOperacion);
		}

		Date fechaHoraOperacion = (Date)attributes.get("fechaHoraOperacion");

		if (fechaHoraOperacion != null) {
			setFechaHoraOperacion(fechaHoraOperacion);
		}

		String codigoUnico = (String)attributes.get("codigoUnico");

		if (codigoUnico != null) {
			setCodigoUnico(codigoUnico);
		}

		String numTarjetaTitular = (String)attributes.get("numTarjetaTitular");

		if (numTarjetaTitular != null) {
			setNumTarjetaTitular(numTarjetaTitular);
		}

		String numCuenta = (String)attributes.get("numCuenta");

		if (numCuenta != null) {
			setNumCuenta(numCuenta);
		}

		Date fechaAltaTitular = (Date)attributes.get("fechaAltaTitular");

		if (fechaAltaTitular != null) {
			setFechaAltaTitular(fechaAltaTitular);
		}

		Date fechaVencTitular = (Date)attributes.get("fechaVencTitular");

		if (fechaVencTitular != null) {
			setFechaVencTitular(fechaVencTitular);
		}

		String numTarjetaProvisional = (String)attributes.get(
				"numTarjetaProvisional");

		if (numTarjetaProvisional != null) {
			setNumTarjetaProvisional(numTarjetaProvisional);
		}

		Date fechaAltaProvisional = (Date)attributes.get("fechaAltaProvisional");

		if (fechaAltaProvisional != null) {
			setFechaAltaProvisional(fechaAltaProvisional);
		}

		Date fechaVencProvisional = (Date)attributes.get("fechaVencProvisional");

		if (fechaVencProvisional != null) {
			setFechaVencProvisional(fechaVencProvisional);
		}

		String RazonNoUsoTarjeta = (String)attributes.get("RazonNoUsoTarjeta");

		if (RazonNoUsoTarjeta != null) {
			setRazonNoUsoTarjeta(RazonNoUsoTarjeta);
		}

		String Motivo = (String)attributes.get("Motivo");

		if (Motivo != null) {
			setMotivo(Motivo);
		}
	}

	/**
	* Returns the primary key of this producto.
	*
	* @return the primary key of this producto
	*/
	@Override
	public long getPrimaryKey() {
		return _producto.getPrimaryKey();
	}

	/**
	* Sets the primary key of this producto.
	*
	* @param primaryKey the primary key of this producto
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_producto.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id producto of this producto.
	*
	* @return the id producto of this producto
	*/
	@Override
	public long getIdProducto() {
		return _producto.getIdProducto();
	}

	/**
	* Sets the id producto of this producto.
	*
	* @param idProducto the id producto of this producto
	*/
	@Override
	public void setIdProducto(long idProducto) {
		_producto.setIdProducto(idProducto);
	}

	/**
	* Returns the id expediente of this producto.
	*
	* @return the id expediente of this producto
	*/
	@Override
	public long getIdExpediente() {
		return _producto.getIdExpediente();
	}

	/**
	* Sets the id expediente of this producto.
	*
	* @param idExpediente the id expediente of this producto
	*/
	@Override
	public void setIdExpediente(long idExpediente) {
		_producto.setIdExpediente(idExpediente);
	}

	/**
	* Returns the tipo producto of this producto.
	*
	* @return the tipo producto of this producto
	*/
	@Override
	public java.lang.String getTipoProducto() {
		return _producto.getTipoProducto();
	}

	/**
	* Sets the tipo producto of this producto.
	*
	* @param tipoProducto the tipo producto of this producto
	*/
	@Override
	public void setTipoProducto(java.lang.String tipoProducto) {
		_producto.setTipoProducto(tipoProducto);
	}

	/**
	* Returns the codigo producto of this producto.
	*
	* @return the codigo producto of this producto
	*/
	@Override
	public java.lang.String getCodigoProducto() {
		return _producto.getCodigoProducto();
	}

	/**
	* Sets the codigo producto of this producto.
	*
	* @param codigoProducto the codigo producto of this producto
	*/
	@Override
	public void setCodigoProducto(java.lang.String codigoProducto) {
		_producto.setCodigoProducto(codigoProducto);
	}

	/**
	* Returns the moneda of this producto.
	*
	* @return the moneda of this producto
	*/
	@Override
	public java.lang.String getMoneda() {
		return _producto.getMoneda();
	}

	/**
	* Sets the moneda of this producto.
	*
	* @param moneda the moneda of this producto
	*/
	@Override
	public void setMoneda(java.lang.String moneda) {
		_producto.setMoneda(moneda);
	}

	/**
	* Returns the marca producto of this producto.
	*
	* @return the marca producto of this producto
	*/
	@Override
	public java.lang.String getMarcaProducto() {
		return _producto.getMarcaProducto();
	}

	/**
	* Sets the marca producto of this producto.
	*
	* @param marcaProducto the marca producto of this producto
	*/
	@Override
	public void setMarcaProducto(java.lang.String marcaProducto) {
		_producto.setMarcaProducto(marcaProducto);
	}

	/**
	* Returns the linea credito of this producto.
	*
	* @return the linea credito of this producto
	*/
	@Override
	public java.lang.String getLineaCredito() {
		return _producto.getLineaCredito();
	}

	/**
	* Sets the linea credito of this producto.
	*
	* @param lineaCredito the linea credito of this producto
	*/
	@Override
	public void setLineaCredito(java.lang.String lineaCredito) {
		_producto.setLineaCredito(lineaCredito);
	}

	/**
	* Returns the dia pago of this producto.
	*
	* @return the dia pago of this producto
	*/
	@Override
	public java.lang.String getDiaPago() {
		return _producto.getDiaPago();
	}

	/**
	* Sets the dia pago of this producto.
	*
	* @param diaPago the dia pago of this producto
	*/
	@Override
	public void setDiaPago(java.lang.String diaPago) {
		_producto.setDiaPago(diaPago);
	}

	/**
	* Returns the tasa producto of this producto.
	*
	* @return the tasa producto of this producto
	*/
	@Override
	public java.lang.String getTasaProducto() {
		return _producto.getTasaProducto();
	}

	/**
	* Sets the tasa producto of this producto.
	*
	* @param tasaProducto the tasa producto of this producto
	*/
	@Override
	public void setTasaProducto(java.lang.String tasaProducto) {
		_producto.setTasaProducto(tasaProducto);
	}

	/**
	* Returns the nombre titular producto of this producto.
	*
	* @return the nombre titular producto of this producto
	*/
	@Override
	public java.lang.String getNombreTitularProducto() {
		return _producto.getNombreTitularProducto();
	}

	/**
	* Sets the nombre titular producto of this producto.
	*
	* @param nombreTitularProducto the nombre titular producto of this producto
	*/
	@Override
	public void setNombreTitularProducto(java.lang.String nombreTitularProducto) {
		_producto.setNombreTitularProducto(nombreTitularProducto);
	}

	/**
	* Returns the tipo seguro of this producto.
	*
	* @return the tipo seguro of this producto
	*/
	@Override
	public java.lang.String getTipoSeguro() {
		return _producto.getTipoSeguro();
	}

	/**
	* Sets the tipo seguro of this producto.
	*
	* @param tipoSeguro the tipo seguro of this producto
	*/
	@Override
	public void setTipoSeguro(java.lang.String tipoSeguro) {
		_producto.setTipoSeguro(tipoSeguro);
	}

	/**
	* Returns the costo seguro of this producto.
	*
	* @return the costo seguro of this producto
	*/
	@Override
	public java.lang.String getCostoSeguro() {
		return _producto.getCostoSeguro();
	}

	/**
	* Sets the costo seguro of this producto.
	*
	* @param costoSeguro the costo seguro of this producto
	*/
	@Override
	public void setCostoSeguro(java.lang.String costoSeguro) {
		_producto.setCostoSeguro(costoSeguro);
	}

	/**
	* Returns the fecha registro of this producto.
	*
	* @return the fecha registro of this producto
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _producto.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this producto.
	*
	* @param fechaRegistro the fecha registro of this producto
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_producto.setFechaRegistro(fechaRegistro);
	}

	/**
	* Returns the flag envio e e c c fisico of this producto.
	*
	* @return the flag envio e e c c fisico of this producto
	*/
	@Override
	public java.lang.String getFlagEnvioEECCFisico() {
		return _producto.getFlagEnvioEECCFisico();
	}

	/**
	* Sets the flag envio e e c c fisico of this producto.
	*
	* @param flagEnvioEECCFisico the flag envio e e c c fisico of this producto
	*/
	@Override
	public void setFlagEnvioEECCFisico(java.lang.String flagEnvioEECCFisico) {
		_producto.setFlagEnvioEECCFisico(flagEnvioEECCFisico);
	}

	/**
	* Returns the flag envio e e c c email of this producto.
	*
	* @return the flag envio e e c c email of this producto
	*/
	@Override
	public java.lang.String getFlagEnvioEECCEmail() {
		return _producto.getFlagEnvioEECCEmail();
	}

	/**
	* Sets the flag envio e e c c email of this producto.
	*
	* @param flagEnvioEECCEmail the flag envio e e c c email of this producto
	*/
	@Override
	public void setFlagEnvioEECCEmail(java.lang.String flagEnvioEECCEmail) {
		_producto.setFlagEnvioEECCEmail(flagEnvioEECCEmail);
	}

	/**
	* Returns the flag terminos of this producto.
	*
	* @return the flag terminos of this producto
	*/
	@Override
	public java.lang.String getFlagTerminos() {
		return _producto.getFlagTerminos();
	}

	/**
	* Sets the flag terminos of this producto.
	*
	* @param flagTerminos the flag terminos of this producto
	*/
	@Override
	public void setFlagTerminos(java.lang.String flagTerminos) {
		_producto.setFlagTerminos(flagTerminos);
	}

	/**
	* Returns the flag cargo compra of this producto.
	*
	* @return the flag cargo compra of this producto
	*/
	@Override
	public java.lang.String getFlagCargoCompra() {
		return _producto.getFlagCargoCompra();
	}

	/**
	* Sets the flag cargo compra of this producto.
	*
	* @param flagCargoCompra the flag cargo compra of this producto
	*/
	@Override
	public void setFlagCargoCompra(java.lang.String flagCargoCompra) {
		_producto.setFlagCargoCompra(flagCargoCompra);
	}

	/**
	* Returns the claveventa of this producto.
	*
	* @return the claveventa of this producto
	*/
	@Override
	public java.lang.String getClaveventa() {
		return _producto.getClaveventa();
	}

	/**
	* Sets the claveventa of this producto.
	*
	* @param claveventa the claveventa of this producto
	*/
	@Override
	public void setClaveventa(java.lang.String claveventa) {
		_producto.setClaveventa(claveventa);
	}

	/**
	* Returns the codigo operacion of this producto.
	*
	* @return the codigo operacion of this producto
	*/
	@Override
	public java.lang.String getCodigoOperacion() {
		return _producto.getCodigoOperacion();
	}

	/**
	* Sets the codigo operacion of this producto.
	*
	* @param codigoOperacion the codigo operacion of this producto
	*/
	@Override
	public void setCodigoOperacion(java.lang.String codigoOperacion) {
		_producto.setCodigoOperacion(codigoOperacion);
	}

	/**
	* Returns the fecha hora operacion of this producto.
	*
	* @return the fecha hora operacion of this producto
	*/
	@Override
	public java.util.Date getFechaHoraOperacion() {
		return _producto.getFechaHoraOperacion();
	}

	/**
	* Sets the fecha hora operacion of this producto.
	*
	* @param fechaHoraOperacion the fecha hora operacion of this producto
	*/
	@Override
	public void setFechaHoraOperacion(java.util.Date fechaHoraOperacion) {
		_producto.setFechaHoraOperacion(fechaHoraOperacion);
	}

	/**
	* Returns the codigo unico of this producto.
	*
	* @return the codigo unico of this producto
	*/
	@Override
	public java.lang.String getCodigoUnico() {
		return _producto.getCodigoUnico();
	}

	/**
	* Sets the codigo unico of this producto.
	*
	* @param codigoUnico the codigo unico of this producto
	*/
	@Override
	public void setCodigoUnico(java.lang.String codigoUnico) {
		_producto.setCodigoUnico(codigoUnico);
	}

	/**
	* Returns the num tarjeta titular of this producto.
	*
	* @return the num tarjeta titular of this producto
	*/
	@Override
	public java.lang.String getNumTarjetaTitular() {
		return _producto.getNumTarjetaTitular();
	}

	/**
	* Sets the num tarjeta titular of this producto.
	*
	* @param numTarjetaTitular the num tarjeta titular of this producto
	*/
	@Override
	public void setNumTarjetaTitular(java.lang.String numTarjetaTitular) {
		_producto.setNumTarjetaTitular(numTarjetaTitular);
	}

	/**
	* Returns the num cuenta of this producto.
	*
	* @return the num cuenta of this producto
	*/
	@Override
	public java.lang.String getNumCuenta() {
		return _producto.getNumCuenta();
	}

	/**
	* Sets the num cuenta of this producto.
	*
	* @param numCuenta the num cuenta of this producto
	*/
	@Override
	public void setNumCuenta(java.lang.String numCuenta) {
		_producto.setNumCuenta(numCuenta);
	}

	/**
	* Returns the fecha alta titular of this producto.
	*
	* @return the fecha alta titular of this producto
	*/
	@Override
	public java.util.Date getFechaAltaTitular() {
		return _producto.getFechaAltaTitular();
	}

	/**
	* Sets the fecha alta titular of this producto.
	*
	* @param fechaAltaTitular the fecha alta titular of this producto
	*/
	@Override
	public void setFechaAltaTitular(java.util.Date fechaAltaTitular) {
		_producto.setFechaAltaTitular(fechaAltaTitular);
	}

	/**
	* Returns the fecha venc titular of this producto.
	*
	* @return the fecha venc titular of this producto
	*/
	@Override
	public java.util.Date getFechaVencTitular() {
		return _producto.getFechaVencTitular();
	}

	/**
	* Sets the fecha venc titular of this producto.
	*
	* @param fechaVencTitular the fecha venc titular of this producto
	*/
	@Override
	public void setFechaVencTitular(java.util.Date fechaVencTitular) {
		_producto.setFechaVencTitular(fechaVencTitular);
	}

	/**
	* Returns the num tarjeta provisional of this producto.
	*
	* @return the num tarjeta provisional of this producto
	*/
	@Override
	public java.lang.String getNumTarjetaProvisional() {
		return _producto.getNumTarjetaProvisional();
	}

	/**
	* Sets the num tarjeta provisional of this producto.
	*
	* @param numTarjetaProvisional the num tarjeta provisional of this producto
	*/
	@Override
	public void setNumTarjetaProvisional(java.lang.String numTarjetaProvisional) {
		_producto.setNumTarjetaProvisional(numTarjetaProvisional);
	}

	/**
	* Returns the fecha alta provisional of this producto.
	*
	* @return the fecha alta provisional of this producto
	*/
	@Override
	public java.util.Date getFechaAltaProvisional() {
		return _producto.getFechaAltaProvisional();
	}

	/**
	* Sets the fecha alta provisional of this producto.
	*
	* @param fechaAltaProvisional the fecha alta provisional of this producto
	*/
	@Override
	public void setFechaAltaProvisional(java.util.Date fechaAltaProvisional) {
		_producto.setFechaAltaProvisional(fechaAltaProvisional);
	}

	/**
	* Returns the fecha venc provisional of this producto.
	*
	* @return the fecha venc provisional of this producto
	*/
	@Override
	public java.util.Date getFechaVencProvisional() {
		return _producto.getFechaVencProvisional();
	}

	/**
	* Sets the fecha venc provisional of this producto.
	*
	* @param fechaVencProvisional the fecha venc provisional of this producto
	*/
	@Override
	public void setFechaVencProvisional(java.util.Date fechaVencProvisional) {
		_producto.setFechaVencProvisional(fechaVencProvisional);
	}

	/**
	* Returns the razon no uso tarjeta of this producto.
	*
	* @return the razon no uso tarjeta of this producto
	*/
	@Override
	public java.lang.String getRazonNoUsoTarjeta() {
		return _producto.getRazonNoUsoTarjeta();
	}

	/**
	* Sets the razon no uso tarjeta of this producto.
	*
	* @param RazonNoUsoTarjeta the razon no uso tarjeta of this producto
	*/
	@Override
	public void setRazonNoUsoTarjeta(java.lang.String RazonNoUsoTarjeta) {
		_producto.setRazonNoUsoTarjeta(RazonNoUsoTarjeta);
	}

	/**
	* Returns the motivo of this producto.
	*
	* @return the motivo of this producto
	*/
	@Override
	public java.lang.String getMotivo() {
		return _producto.getMotivo();
	}

	/**
	* Sets the motivo of this producto.
	*
	* @param Motivo the motivo of this producto
	*/
	@Override
	public void setMotivo(java.lang.String Motivo) {
		_producto.setMotivo(Motivo);
	}

	@Override
	public boolean isNew() {
		return _producto.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_producto.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _producto.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_producto.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _producto.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _producto.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_producto.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _producto.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_producto.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_producto.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_producto.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProductoWrapper((Producto)_producto.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.Producto producto) {
		return _producto.compareTo(producto);
	}

	@Override
	public int hashCode() {
		return _producto.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.Producto> toCacheModel() {
		return _producto.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.Producto toEscapedModel() {
		return new ProductoWrapper(_producto.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.Producto toUnescapedModel() {
		return new ProductoWrapper(_producto.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _producto.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _producto.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_producto.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductoWrapper)) {
			return false;
		}

		ProductoWrapper productoWrapper = (ProductoWrapper)obj;

		if (Validator.equals(_producto, productoWrapper._producto)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Producto getWrappedProducto() {
		return _producto;
	}

	@Override
	public Producto getWrappedModel() {
		return _producto;
	}

	@Override
	public void resetOriginalValues() {
		_producto.resetOriginalValues();
	}

	private Producto _producto;
}