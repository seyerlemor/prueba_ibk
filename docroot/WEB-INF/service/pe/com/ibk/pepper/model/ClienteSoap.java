/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class ClienteSoap implements Serializable {
	public static ClienteSoap toSoapModel(Cliente model) {
		ClienteSoap soapModel = new ClienteSoap();

		soapModel.setIdDatoCliente(model.getIdDatoCliente());
		soapModel.setIdExpediente(model.getIdExpediente());
		soapModel.setPrimerNombre(model.getPrimerNombre());
		soapModel.setSegundoNombre(model.getSegundoNombre());
		soapModel.setApellidoPaterno(model.getApellidoPaterno());
		soapModel.setApellidoMaterno(model.getApellidoMaterno());
		soapModel.setSexo(model.getSexo());
		soapModel.setCorreo(model.getCorreo());
		soapModel.setTelefono(model.getTelefono());
		soapModel.setEstadoCivil(model.getEstadoCivil());
		soapModel.setSituacionLaboral(model.getSituacionLaboral());
		soapModel.setRucEmpresaTrabajo(model.getRucEmpresaTrabajo());
		soapModel.setNivelEducacion(model.getNivelEducacion());
		soapModel.setOcupacion(model.getOcupacion());
		soapModel.setCargoActual(model.getCargoActual());
		soapModel.setActividadNegocio(model.getActividadNegocio());
		soapModel.setAntiguedadLaboral(model.getAntiguedadLaboral());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setPorcentajeParticipacion(model.getPorcentajeParticipacion());
		soapModel.setIngresoMensualFijo(model.getIngresoMensualFijo());
		soapModel.setIngresoMensualVariable(model.getIngresoMensualVariable());
		soapModel.setDocumento(model.getDocumento());
		soapModel.setTerminosCondiciones(model.getTerminosCondiciones());
		soapModel.setTipoDocumento(model.getTipoDocumento());
		soapModel.setFlagPEP(model.getFlagPEP());
		soapModel.setFechaNacimiento(model.getFechaNacimiento());
		soapModel.setOperador(model.getOperador());

		return soapModel;
	}

	public static ClienteSoap[] toSoapModels(Cliente[] models) {
		ClienteSoap[] soapModels = new ClienteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ClienteSoap[][] toSoapModels(Cliente[][] models) {
		ClienteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ClienteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ClienteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ClienteSoap[] toSoapModels(List<Cliente> models) {
		List<ClienteSoap> soapModels = new ArrayList<ClienteSoap>(models.size());

		for (Cliente model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ClienteSoap[soapModels.size()]);
	}

	public ClienteSoap() {
	}

	public long getPrimaryKey() {
		return _idDatoCliente;
	}

	public void setPrimaryKey(long pk) {
		setIdDatoCliente(pk);
	}

	public long getIdDatoCliente() {
		return _idDatoCliente;
	}

	public void setIdDatoCliente(long idDatoCliente) {
		_idDatoCliente = idDatoCliente;
	}

	public long getIdExpediente() {
		return _idExpediente;
	}

	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;
	}

	public String getPrimerNombre() {
		return _primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		_primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return _segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		_segundoNombre = segundoNombre;
	}

	public String getApellidoPaterno() {
		return _apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		_apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return _apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		_apellidoMaterno = apellidoMaterno;
	}

	public String getSexo() {
		return _sexo;
	}

	public void setSexo(String sexo) {
		_sexo = sexo;
	}

	public String getCorreo() {
		return _correo;
	}

	public void setCorreo(String correo) {
		_correo = correo;
	}

	public String getTelefono() {
		return _telefono;
	}

	public void setTelefono(String telefono) {
		_telefono = telefono;
	}

	public String getEstadoCivil() {
		return _estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		_estadoCivil = estadoCivil;
	}

	public String getSituacionLaboral() {
		return _situacionLaboral;
	}

	public void setSituacionLaboral(String situacionLaboral) {
		_situacionLaboral = situacionLaboral;
	}

	public String getRucEmpresaTrabajo() {
		return _rucEmpresaTrabajo;
	}

	public void setRucEmpresaTrabajo(String rucEmpresaTrabajo) {
		_rucEmpresaTrabajo = rucEmpresaTrabajo;
	}

	public String getNivelEducacion() {
		return _nivelEducacion;
	}

	public void setNivelEducacion(String nivelEducacion) {
		_nivelEducacion = nivelEducacion;
	}

	public String getOcupacion() {
		return _ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		_ocupacion = ocupacion;
	}

	public String getCargoActual() {
		return _cargoActual;
	}

	public void setCargoActual(String cargoActual) {
		_cargoActual = cargoActual;
	}

	public String getActividadNegocio() {
		return _actividadNegocio;
	}

	public void setActividadNegocio(String actividadNegocio) {
		_actividadNegocio = actividadNegocio;
	}

	public String getAntiguedadLaboral() {
		return _antiguedadLaboral;
	}

	public void setAntiguedadLaboral(String antiguedadLaboral) {
		_antiguedadLaboral = antiguedadLaboral;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getPorcentajeParticipacion() {
		return _porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		_porcentajeParticipacion = porcentajeParticipacion;
	}

	public String getIngresoMensualFijo() {
		return _ingresoMensualFijo;
	}

	public void setIngresoMensualFijo(String ingresoMensualFijo) {
		_ingresoMensualFijo = ingresoMensualFijo;
	}

	public String getIngresoMensualVariable() {
		return _ingresoMensualVariable;
	}

	public void setIngresoMensualVariable(String ingresoMensualVariable) {
		_ingresoMensualVariable = ingresoMensualVariable;
	}

	public String getDocumento() {
		return _documento;
	}

	public void setDocumento(String documento) {
		_documento = documento;
	}

	public String getTerminosCondiciones() {
		return _terminosCondiciones;
	}

	public void setTerminosCondiciones(String terminosCondiciones) {
		_terminosCondiciones = terminosCondiciones;
	}

	public String getTipoDocumento() {
		return _tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		_tipoDocumento = tipoDocumento;
	}

	public String getFlagPEP() {
		return _flagPEP;
	}

	public void setFlagPEP(String flagPEP) {
		_flagPEP = flagPEP;
	}

	public Date getFechaNacimiento() {
		return _fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		_fechaNacimiento = fechaNacimiento;
	}

	public String getOperador() {
		return _operador;
	}

	public void setOperador(String operador) {
		_operador = operador;
	}

	private long _idDatoCliente;
	private long _idExpediente;
	private String _primerNombre;
	private String _segundoNombre;
	private String _apellidoPaterno;
	private String _apellidoMaterno;
	private String _sexo;
	private String _correo;
	private String _telefono;
	private String _estadoCivil;
	private String _situacionLaboral;
	private String _rucEmpresaTrabajo;
	private String _nivelEducacion;
	private String _ocupacion;
	private String _cargoActual;
	private String _actividadNegocio;
	private String _antiguedadLaboral;
	private Date _fechaRegistro;
	private String _porcentajeParticipacion;
	private String _ingresoMensualFijo;
	private String _ingresoMensualVariable;
	private String _documento;
	private String _terminosCondiciones;
	private String _tipoDocumento;
	private String _flagPEP;
	private Date _fechaNacimiento;
	private String _operador;
}