/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.GroupedModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the ParametroHijoPO service. Represents a row in the &quot;T_PARAMETRO_DETALLE&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOImpl}.
 * </p>
 *
 * @author Interbank
 * @see ParametroHijoPO
 * @see pe.com.ibk.pepper.model.impl.ParametroHijoPOImpl
 * @see pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl
 * @generated
 */
public interface ParametroHijoPOModel extends BaseModel<ParametroHijoPO>,
	GroupedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a parametro hijo p o model instance should use the {@link ParametroHijoPO} interface instead.
	 */

	/**
	 * Returns the primary key of this parametro hijo p o.
	 *
	 * @return the primary key of this parametro hijo p o
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this parametro hijo p o.
	 *
	 * @param primaryKey the primary key of this parametro hijo p o
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the id parametro hijo of this parametro hijo p o.
	 *
	 * @return the id parametro hijo of this parametro hijo p o
	 */
	public long getIdParametroHijo();

	/**
	 * Sets the id parametro hijo of this parametro hijo p o.
	 *
	 * @param idParametroHijo the id parametro hijo of this parametro hijo p o
	 */
	public void setIdParametroHijo(long idParametroHijo);

	/**
	 * Returns the group ID of this parametro hijo p o.
	 *
	 * @return the group ID of this parametro hijo p o
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this parametro hijo p o.
	 *
	 * @param groupId the group ID of this parametro hijo p o
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this parametro hijo p o.
	 *
	 * @return the company ID of this parametro hijo p o
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this parametro hijo p o.
	 *
	 * @param companyId the company ID of this parametro hijo p o
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this parametro hijo p o.
	 *
	 * @return the user ID of this parametro hijo p o
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this parametro hijo p o.
	 *
	 * @param userId the user ID of this parametro hijo p o
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this parametro hijo p o.
	 *
	 * @return the user uuid of this parametro hijo p o
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this parametro hijo p o.
	 *
	 * @param userUuid the user uuid of this parametro hijo p o
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this parametro hijo p o.
	 *
	 * @return the user name of this parametro hijo p o
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this parametro hijo p o.
	 *
	 * @param userName the user name of this parametro hijo p o
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this parametro hijo p o.
	 *
	 * @return the create date of this parametro hijo p o
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this parametro hijo p o.
	 *
	 * @param createDate the create date of this parametro hijo p o
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this parametro hijo p o.
	 *
	 * @return the modified date of this parametro hijo p o
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this parametro hijo p o.
	 *
	 * @param modifiedDate the modified date of this parametro hijo p o
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the codigo padre of this parametro hijo p o.
	 *
	 * @return the codigo padre of this parametro hijo p o
	 */
	@AutoEscape
	public String getCodigoPadre();

	/**
	 * Sets the codigo padre of this parametro hijo p o.
	 *
	 * @param codigoPadre the codigo padre of this parametro hijo p o
	 */
	public void setCodigoPadre(String codigoPadre);

	/**
	 * Returns the codigo of this parametro hijo p o.
	 *
	 * @return the codigo of this parametro hijo p o
	 */
	@AutoEscape
	public String getCodigo();

	/**
	 * Sets the codigo of this parametro hijo p o.
	 *
	 * @param codigo the codigo of this parametro hijo p o
	 */
	public void setCodigo(String codigo);

	/**
	 * Returns the nombre of this parametro hijo p o.
	 *
	 * @return the nombre of this parametro hijo p o
	 */
	@AutoEscape
	public String getNombre();

	/**
	 * Sets the nombre of this parametro hijo p o.
	 *
	 * @param nombre the nombre of this parametro hijo p o
	 */
	public void setNombre(String nombre);

	/**
	 * Returns the descripcion of this parametro hijo p o.
	 *
	 * @return the descripcion of this parametro hijo p o
	 */
	@AutoEscape
	public String getDescripcion();

	/**
	 * Sets the descripcion of this parametro hijo p o.
	 *
	 * @param descripcion the descripcion of this parametro hijo p o
	 */
	public void setDescripcion(String descripcion);

	/**
	 * Returns the estado of this parametro hijo p o.
	 *
	 * @return the estado of this parametro hijo p o
	 */
	public boolean getEstado();

	/**
	 * Returns <code>true</code> if this parametro hijo p o is estado.
	 *
	 * @return <code>true</code> if this parametro hijo p o is estado; <code>false</code> otherwise
	 */
	public boolean isEstado();

	/**
	 * Sets whether this parametro hijo p o is estado.
	 *
	 * @param estado the estado of this parametro hijo p o
	 */
	public void setEstado(boolean estado);

	/**
	 * Returns the orden of this parametro hijo p o.
	 *
	 * @return the orden of this parametro hijo p o
	 */
	public int getOrden();

	/**
	 * Sets the orden of this parametro hijo p o.
	 *
	 * @param orden the orden of this parametro hijo p o
	 */
	public void setOrden(int orden);

	/**
	 * Returns the dato1 of this parametro hijo p o.
	 *
	 * @return the dato1 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato1();

	/**
	 * Sets the dato1 of this parametro hijo p o.
	 *
	 * @param dato1 the dato1 of this parametro hijo p o
	 */
	public void setDato1(String dato1);

	/**
	 * Returns the dato2 of this parametro hijo p o.
	 *
	 * @return the dato2 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato2();

	/**
	 * Sets the dato2 of this parametro hijo p o.
	 *
	 * @param dato2 the dato2 of this parametro hijo p o
	 */
	public void setDato2(String dato2);

	/**
	 * Returns the dato3 of this parametro hijo p o.
	 *
	 * @return the dato3 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato3();

	/**
	 * Sets the dato3 of this parametro hijo p o.
	 *
	 * @param dato3 the dato3 of this parametro hijo p o
	 */
	public void setDato3(String dato3);

	/**
	 * Returns the dato4 of this parametro hijo p o.
	 *
	 * @return the dato4 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato4();

	/**
	 * Sets the dato4 of this parametro hijo p o.
	 *
	 * @param dato4 the dato4 of this parametro hijo p o
	 */
	public void setDato4(String dato4);

	/**
	 * Returns the dato5 of this parametro hijo p o.
	 *
	 * @return the dato5 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato5();

	/**
	 * Sets the dato5 of this parametro hijo p o.
	 *
	 * @param dato5 the dato5 of this parametro hijo p o
	 */
	public void setDato5(String dato5);

	/**
	 * Returns the dato6 of this parametro hijo p o.
	 *
	 * @return the dato6 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato6();

	/**
	 * Sets the dato6 of this parametro hijo p o.
	 *
	 * @param dato6 the dato6 of this parametro hijo p o
	 */
	public void setDato6(String dato6);

	/**
	 * Returns the dato7 of this parametro hijo p o.
	 *
	 * @return the dato7 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato7();

	/**
	 * Sets the dato7 of this parametro hijo p o.
	 *
	 * @param dato7 the dato7 of this parametro hijo p o
	 */
	public void setDato7(String dato7);

	/**
	 * Returns the dato8 of this parametro hijo p o.
	 *
	 * @return the dato8 of this parametro hijo p o
	 */
	@AutoEscape
	public String getDato8();

	/**
	 * Sets the dato8 of this parametro hijo p o.
	 *
	 * @param dato8 the dato8 of this parametro hijo p o
	 */
	public void setDato8(String dato8);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ParametroHijoPO parametroHijoPO);

	@Override
	public int hashCode();

	@Override
	public CacheModel<pe.com.ibk.pepper.model.ParametroHijoPO> toCacheModel();

	@Override
	public pe.com.ibk.pepper.model.ParametroHijoPO toEscapedModel();

	@Override
	public pe.com.ibk.pepper.model.ParametroHijoPO toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}