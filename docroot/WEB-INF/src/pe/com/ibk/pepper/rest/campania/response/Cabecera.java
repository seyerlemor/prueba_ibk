
package pe.com.ibk.pepper.rest.campania.response;


public class Cabecera {

	private String id;
    private String idProductoPrincipal;
    
	public String getIdProductoPrincipal() {
		return idProductoPrincipal;
	}
	public void setIdProductoPrincipal(String idProductoPrincipal) {
		this.idProductoPrincipal = idProductoPrincipal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
}
