/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ParametroHijoPO}.
 * </p>
 *
 * @author Interbank
 * @see ParametroHijoPO
 * @generated
 */
public class ParametroHijoPOWrapper implements ParametroHijoPO,
	ModelWrapper<ParametroHijoPO> {
	public ParametroHijoPOWrapper(ParametroHijoPO parametroHijoPO) {
		_parametroHijoPO = parametroHijoPO;
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroHijoPO.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroHijoPO.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroHijo", getIdParametroHijo());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("codigo", getCodigo());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("orden", getOrden());
		attributes.put("dato1", getDato1());
		attributes.put("dato2", getDato2());
		attributes.put("dato3", getDato3());
		attributes.put("dato4", getDato4());
		attributes.put("dato5", getDato5());
		attributes.put("dato6", getDato6());
		attributes.put("dato7", getDato7());
		attributes.put("dato8", getDato8());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroHijo = (Long)attributes.get("idParametroHijo");

		if (idParametroHijo != null) {
			setIdParametroHijo(idParametroHijo);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Integer orden = (Integer)attributes.get("orden");

		if (orden != null) {
			setOrden(orden);
		}

		String dato1 = (String)attributes.get("dato1");

		if (dato1 != null) {
			setDato1(dato1);
		}

		String dato2 = (String)attributes.get("dato2");

		if (dato2 != null) {
			setDato2(dato2);
		}

		String dato3 = (String)attributes.get("dato3");

		if (dato3 != null) {
			setDato3(dato3);
		}

		String dato4 = (String)attributes.get("dato4");

		if (dato4 != null) {
			setDato4(dato4);
		}

		String dato5 = (String)attributes.get("dato5");

		if (dato5 != null) {
			setDato5(dato5);
		}

		String dato6 = (String)attributes.get("dato6");

		if (dato6 != null) {
			setDato6(dato6);
		}

		String dato7 = (String)attributes.get("dato7");

		if (dato7 != null) {
			setDato7(dato7);
		}

		String dato8 = (String)attributes.get("dato8");

		if (dato8 != null) {
			setDato8(dato8);
		}
	}

	/**
	* Returns the primary key of this parametro hijo p o.
	*
	* @return the primary key of this parametro hijo p o
	*/
	@Override
	public long getPrimaryKey() {
		return _parametroHijoPO.getPrimaryKey();
	}

	/**
	* Sets the primary key of this parametro hijo p o.
	*
	* @param primaryKey the primary key of this parametro hijo p o
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_parametroHijoPO.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id parametro hijo of this parametro hijo p o.
	*
	* @return the id parametro hijo of this parametro hijo p o
	*/
	@Override
	public long getIdParametroHijo() {
		return _parametroHijoPO.getIdParametroHijo();
	}

	/**
	* Sets the id parametro hijo of this parametro hijo p o.
	*
	* @param idParametroHijo the id parametro hijo of this parametro hijo p o
	*/
	@Override
	public void setIdParametroHijo(long idParametroHijo) {
		_parametroHijoPO.setIdParametroHijo(idParametroHijo);
	}

	/**
	* Returns the group ID of this parametro hijo p o.
	*
	* @return the group ID of this parametro hijo p o
	*/
	@Override
	public long getGroupId() {
		return _parametroHijoPO.getGroupId();
	}

	/**
	* Sets the group ID of this parametro hijo p o.
	*
	* @param groupId the group ID of this parametro hijo p o
	*/
	@Override
	public void setGroupId(long groupId) {
		_parametroHijoPO.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this parametro hijo p o.
	*
	* @return the company ID of this parametro hijo p o
	*/
	@Override
	public long getCompanyId() {
		return _parametroHijoPO.getCompanyId();
	}

	/**
	* Sets the company ID of this parametro hijo p o.
	*
	* @param companyId the company ID of this parametro hijo p o
	*/
	@Override
	public void setCompanyId(long companyId) {
		_parametroHijoPO.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this parametro hijo p o.
	*
	* @return the user ID of this parametro hijo p o
	*/
	@Override
	public long getUserId() {
		return _parametroHijoPO.getUserId();
	}

	/**
	* Sets the user ID of this parametro hijo p o.
	*
	* @param userId the user ID of this parametro hijo p o
	*/
	@Override
	public void setUserId(long userId) {
		_parametroHijoPO.setUserId(userId);
	}

	/**
	* Returns the user uuid of this parametro hijo p o.
	*
	* @return the user uuid of this parametro hijo p o
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroHijoPO.getUserUuid();
	}

	/**
	* Sets the user uuid of this parametro hijo p o.
	*
	* @param userUuid the user uuid of this parametro hijo p o
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_parametroHijoPO.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this parametro hijo p o.
	*
	* @return the user name of this parametro hijo p o
	*/
	@Override
	public java.lang.String getUserName() {
		return _parametroHijoPO.getUserName();
	}

	/**
	* Sets the user name of this parametro hijo p o.
	*
	* @param userName the user name of this parametro hijo p o
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_parametroHijoPO.setUserName(userName);
	}

	/**
	* Returns the create date of this parametro hijo p o.
	*
	* @return the create date of this parametro hijo p o
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _parametroHijoPO.getCreateDate();
	}

	/**
	* Sets the create date of this parametro hijo p o.
	*
	* @param createDate the create date of this parametro hijo p o
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_parametroHijoPO.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this parametro hijo p o.
	*
	* @return the modified date of this parametro hijo p o
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _parametroHijoPO.getModifiedDate();
	}

	/**
	* Sets the modified date of this parametro hijo p o.
	*
	* @param modifiedDate the modified date of this parametro hijo p o
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_parametroHijoPO.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the codigo padre of this parametro hijo p o.
	*
	* @return the codigo padre of this parametro hijo p o
	*/
	@Override
	public java.lang.String getCodigoPadre() {
		return _parametroHijoPO.getCodigoPadre();
	}

	/**
	* Sets the codigo padre of this parametro hijo p o.
	*
	* @param codigoPadre the codigo padre of this parametro hijo p o
	*/
	@Override
	public void setCodigoPadre(java.lang.String codigoPadre) {
		_parametroHijoPO.setCodigoPadre(codigoPadre);
	}

	/**
	* Returns the codigo of this parametro hijo p o.
	*
	* @return the codigo of this parametro hijo p o
	*/
	@Override
	public java.lang.String getCodigo() {
		return _parametroHijoPO.getCodigo();
	}

	/**
	* Sets the codigo of this parametro hijo p o.
	*
	* @param codigo the codigo of this parametro hijo p o
	*/
	@Override
	public void setCodigo(java.lang.String codigo) {
		_parametroHijoPO.setCodigo(codigo);
	}

	/**
	* Returns the nombre of this parametro hijo p o.
	*
	* @return the nombre of this parametro hijo p o
	*/
	@Override
	public java.lang.String getNombre() {
		return _parametroHijoPO.getNombre();
	}

	/**
	* Sets the nombre of this parametro hijo p o.
	*
	* @param nombre the nombre of this parametro hijo p o
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_parametroHijoPO.setNombre(nombre);
	}

	/**
	* Returns the descripcion of this parametro hijo p o.
	*
	* @return the descripcion of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _parametroHijoPO.getDescripcion();
	}

	/**
	* Sets the descripcion of this parametro hijo p o.
	*
	* @param descripcion the descripcion of this parametro hijo p o
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_parametroHijoPO.setDescripcion(descripcion);
	}

	/**
	* Returns the estado of this parametro hijo p o.
	*
	* @return the estado of this parametro hijo p o
	*/
	@Override
	public boolean getEstado() {
		return _parametroHijoPO.getEstado();
	}

	/**
	* Returns <code>true</code> if this parametro hijo p o is estado.
	*
	* @return <code>true</code> if this parametro hijo p o is estado; <code>false</code> otherwise
	*/
	@Override
	public boolean isEstado() {
		return _parametroHijoPO.isEstado();
	}

	/**
	* Sets whether this parametro hijo p o is estado.
	*
	* @param estado the estado of this parametro hijo p o
	*/
	@Override
	public void setEstado(boolean estado) {
		_parametroHijoPO.setEstado(estado);
	}

	/**
	* Returns the orden of this parametro hijo p o.
	*
	* @return the orden of this parametro hijo p o
	*/
	@Override
	public int getOrden() {
		return _parametroHijoPO.getOrden();
	}

	/**
	* Sets the orden of this parametro hijo p o.
	*
	* @param orden the orden of this parametro hijo p o
	*/
	@Override
	public void setOrden(int orden) {
		_parametroHijoPO.setOrden(orden);
	}

	/**
	* Returns the dato1 of this parametro hijo p o.
	*
	* @return the dato1 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato1() {
		return _parametroHijoPO.getDato1();
	}

	/**
	* Sets the dato1 of this parametro hijo p o.
	*
	* @param dato1 the dato1 of this parametro hijo p o
	*/
	@Override
	public void setDato1(java.lang.String dato1) {
		_parametroHijoPO.setDato1(dato1);
	}

	/**
	* Returns the dato2 of this parametro hijo p o.
	*
	* @return the dato2 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato2() {
		return _parametroHijoPO.getDato2();
	}

	/**
	* Sets the dato2 of this parametro hijo p o.
	*
	* @param dato2 the dato2 of this parametro hijo p o
	*/
	@Override
	public void setDato2(java.lang.String dato2) {
		_parametroHijoPO.setDato2(dato2);
	}

	/**
	* Returns the dato3 of this parametro hijo p o.
	*
	* @return the dato3 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato3() {
		return _parametroHijoPO.getDato3();
	}

	/**
	* Sets the dato3 of this parametro hijo p o.
	*
	* @param dato3 the dato3 of this parametro hijo p o
	*/
	@Override
	public void setDato3(java.lang.String dato3) {
		_parametroHijoPO.setDato3(dato3);
	}

	/**
	* Returns the dato4 of this parametro hijo p o.
	*
	* @return the dato4 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato4() {
		return _parametroHijoPO.getDato4();
	}

	/**
	* Sets the dato4 of this parametro hijo p o.
	*
	* @param dato4 the dato4 of this parametro hijo p o
	*/
	@Override
	public void setDato4(java.lang.String dato4) {
		_parametroHijoPO.setDato4(dato4);
	}

	/**
	* Returns the dato5 of this parametro hijo p o.
	*
	* @return the dato5 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato5() {
		return _parametroHijoPO.getDato5();
	}

	/**
	* Sets the dato5 of this parametro hijo p o.
	*
	* @param dato5 the dato5 of this parametro hijo p o
	*/
	@Override
	public void setDato5(java.lang.String dato5) {
		_parametroHijoPO.setDato5(dato5);
	}

	/**
	* Returns the dato6 of this parametro hijo p o.
	*
	* @return the dato6 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato6() {
		return _parametroHijoPO.getDato6();
	}

	/**
	* Sets the dato6 of this parametro hijo p o.
	*
	* @param dato6 the dato6 of this parametro hijo p o
	*/
	@Override
	public void setDato6(java.lang.String dato6) {
		_parametroHijoPO.setDato6(dato6);
	}

	/**
	* Returns the dato7 of this parametro hijo p o.
	*
	* @return the dato7 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato7() {
		return _parametroHijoPO.getDato7();
	}

	/**
	* Sets the dato7 of this parametro hijo p o.
	*
	* @param dato7 the dato7 of this parametro hijo p o
	*/
	@Override
	public void setDato7(java.lang.String dato7) {
		_parametroHijoPO.setDato7(dato7);
	}

	/**
	* Returns the dato8 of this parametro hijo p o.
	*
	* @return the dato8 of this parametro hijo p o
	*/
	@Override
	public java.lang.String getDato8() {
		return _parametroHijoPO.getDato8();
	}

	/**
	* Sets the dato8 of this parametro hijo p o.
	*
	* @param dato8 the dato8 of this parametro hijo p o
	*/
	@Override
	public void setDato8(java.lang.String dato8) {
		_parametroHijoPO.setDato8(dato8);
	}

	@Override
	public boolean isNew() {
		return _parametroHijoPO.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_parametroHijoPO.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _parametroHijoPO.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_parametroHijoPO.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _parametroHijoPO.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _parametroHijoPO.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_parametroHijoPO.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _parametroHijoPO.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_parametroHijoPO.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_parametroHijoPO.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_parametroHijoPO.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ParametroHijoPOWrapper((ParametroHijoPO)_parametroHijoPO.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ParametroHijoPO parametroHijoPO) {
		return _parametroHijoPO.compareTo(parametroHijoPO);
	}

	@Override
	public int hashCode() {
		return _parametroHijoPO.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.ParametroHijoPO> toCacheModel() {
		return _parametroHijoPO.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroHijoPO toEscapedModel() {
		return new ParametroHijoPOWrapper(_parametroHijoPO.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroHijoPO toUnescapedModel() {
		return new ParametroHijoPOWrapper(_parametroHijoPO.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _parametroHijoPO.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _parametroHijoPO.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_parametroHijoPO.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroHijoPOWrapper)) {
			return false;
		}

		ParametroHijoPOWrapper parametroHijoPOWrapper = (ParametroHijoPOWrapper)obj;

		if (Validator.equals(_parametroHijoPO,
					parametroHijoPOWrapper._parametroHijoPO)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ParametroHijoPO getWrappedParametroHijoPO() {
		return _parametroHijoPO;
	}

	@Override
	public ParametroHijoPO getWrappedModel() {
		return _parametroHijoPO;
	}

	@Override
	public void resetOriginalValues() {
		_parametroHijoPO.resetOriginalValues();
	}

	private ParametroHijoPO _parametroHijoPO;
}