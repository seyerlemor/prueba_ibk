/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchParametroHijoPOException;
import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.model.impl.ParametroHijoPOImpl;
import pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the parametro hijo p o service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ParametroHijoPOPersistence
 * @see ParametroHijoPOUtil
 * @generated
 */
public class ParametroHijoPOPersistenceImpl extends BasePersistenceImpl<ParametroHijoPO>
	implements ParametroHijoPOPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ParametroHijoPOUtil} to access the parametro hijo p o persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ParametroHijoPOImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_G = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByC_G",
			new String[] {
				String.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_G",
			new String[] { String.class.getName(), Boolean.class.getName() },
			ParametroHijoPOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ORDEN_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_G = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_G",
			new String[] { String.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the parametro hijo p os where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_G(String codigoPadre, boolean estado)
		throws SystemException {
		return findByC_G(codigoPadre, estado, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro hijo p os where codigoPadre = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @return the range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_G(String codigoPadre, boolean estado,
		int start, int end) throws SystemException {
		return findByC_G(codigoPadre, estado, start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro hijo p os where codigoPadre = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_G(String codigoPadre, boolean estado,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G;
			finderArgs = new Object[] { codigoPadre, estado };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_G;
			finderArgs = new Object[] {
					codigoPadre, estado,
					
					start, end, orderByComparator
				};
		}

		List<ParametroHijoPO> list = (List<ParametroHijoPO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ParametroHijoPO parametroHijoPO : list) {
				if (!Validator.equals(codigoPadre,
							parametroHijoPO.getCodigoPadre()) ||
						(estado != parametroHijoPO.getEstado())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_2);
			}

			query.append(_FINDER_COLUMN_C_G_ESTADO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				qPos.add(estado);

				if (!pagination) {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroHijoPO>(list);
				}
				else {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_G_First(String codigoPadre, boolean estado,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_G_First(codigoPadre, estado,
				orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_G_First(String codigoPadre, boolean estado,
		OrderByComparator orderByComparator) throws SystemException {
		List<ParametroHijoPO> list = findByC_G(codigoPadre, estado, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_G_Last(String codigoPadre, boolean estado,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_G_Last(codigoPadre, estado,
				orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_G_Last(String codigoPadre, boolean estado,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_G(codigoPadre, estado);

		if (count == 0) {
			return null;
		}

		List<ParametroHijoPO> list = findByC_G(codigoPadre, estado, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the parametro hijo p os before and after the current parametro hijo p o in the ordered set where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param idParametroHijo the primary key of the current parametro hijo p o
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO[] findByC_G_PrevAndNext(long idParametroHijo,
		String codigoPadre, boolean estado, OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByPrimaryKey(idParametroHijo);

		Session session = null;

		try {
			session = openSession();

			ParametroHijoPO[] array = new ParametroHijoPOImpl[3];

			array[0] = getByC_G_PrevAndNext(session, parametroHijoPO,
					codigoPadre, estado, orderByComparator, true);

			array[1] = parametroHijoPO;

			array[2] = getByC_G_PrevAndNext(session, parametroHijoPO,
					codigoPadre, estado, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ParametroHijoPO getByC_G_PrevAndNext(Session session,
		ParametroHijoPO parametroHijoPO, String codigoPadre, boolean estado,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

		boolean bindCodigoPadre = false;

		if (codigoPadre == null) {
			query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_1);
		}
		else if (codigoPadre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_3);
		}
		else {
			bindCodigoPadre = true;

			query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_2);
		}

		query.append(_FINDER_COLUMN_C_G_ESTADO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodigoPadre) {
			qPos.add(codigoPadre);
		}

		qPos.add(estado);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(parametroHijoPO);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ParametroHijoPO> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the parametro hijo p os where codigoPadre = &#63; and estado = &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_G(String codigoPadre, boolean estado)
		throws SystemException {
		for (ParametroHijoPO parametroHijoPO : findByC_G(codigoPadre, estado,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(parametroHijoPO);
		}
	}

	/**
	 * Returns the number of parametro hijo p os where codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_G(String codigoPadre, boolean estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_G;

		Object[] finderArgs = new Object[] { codigoPadre, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_G_CODIGOPADRE_2);
			}

			query.append(_FINDER_COLUMN_C_G_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_G_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_G_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_G_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_G_ESTADO_2 = "parametroHijoPO.estado = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_N_D_G = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByC_N_D_G",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_C_N_D_G = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByC_N_D_G",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the parametro hijo p os where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @return the matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_N_D_G(String codigoPadre,
		String nombre, String descripcion) throws SystemException {
		return findByC_N_D_G(codigoPadre, nombre, descripcion,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro hijo p os where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @return the range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_N_D_G(String codigoPadre,
		String nombre, String descripcion, int start, int end)
		throws SystemException {
		return findByC_N_D_G(codigoPadre, nombre, descripcion, start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro hijo p os where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_N_D_G(String codigoPadre,
		String nombre, String descripcion, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_N_D_G;
		finderArgs = new Object[] {
				codigoPadre, nombre, descripcion,
				
				start, end, orderByComparator
			};

		List<ParametroHijoPO> list = (List<ParametroHijoPO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ParametroHijoPO parametroHijoPO : list) {
				if (!Validator.equals(codigoPadre,
							parametroHijoPO.getCodigoPadre()) ||
						!StringUtil.wildcardMatches(
							parametroHijoPO.getNombre(), nombre,
							CharPool.UNDERLINE, CharPool.PERCENT,
							CharPool.BACK_SLASH, true) ||
						!StringUtil.wildcardMatches(
							parametroHijoPO.getDescripcion(), descripcion,
							CharPool.UNDERLINE, CharPool.PERCENT,
							CharPool.BACK_SLASH, true)) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_2);
			}

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_2);
			}

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				if (!pagination) {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroHijoPO>(list);
				}
				else {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_N_D_G_First(String codigoPadre,
		String nombre, String descripcion, OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_N_D_G_First(codigoPadre,
				nombre, descripcion, orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", nombre=");
		msg.append(nombre);

		msg.append(", descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_N_D_G_First(String codigoPadre,
		String nombre, String descripcion, OrderByComparator orderByComparator)
		throws SystemException {
		List<ParametroHijoPO> list = findByC_N_D_G(codigoPadre, nombre,
				descripcion, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_N_D_G_Last(String codigoPadre,
		String nombre, String descripcion, OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_N_D_G_Last(codigoPadre,
				nombre, descripcion, orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", nombre=");
		msg.append(nombre);

		msg.append(", descripcion=");
		msg.append(descripcion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_N_D_G_Last(String codigoPadre,
		String nombre, String descripcion, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByC_N_D_G(codigoPadre, nombre, descripcion);

		if (count == 0) {
			return null;
		}

		List<ParametroHijoPO> list = findByC_N_D_G(codigoPadre, nombre,
				descripcion, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the parametro hijo p os before and after the current parametro hijo p o in the ordered set where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param idParametroHijo the primary key of the current parametro hijo p o
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO[] findByC_N_D_G_PrevAndNext(long idParametroHijo,
		String codigoPadre, String nombre, String descripcion,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByPrimaryKey(idParametroHijo);

		Session session = null;

		try {
			session = openSession();

			ParametroHijoPO[] array = new ParametroHijoPOImpl[3];

			array[0] = getByC_N_D_G_PrevAndNext(session, parametroHijoPO,
					codigoPadre, nombre, descripcion, orderByComparator, true);

			array[1] = parametroHijoPO;

			array[2] = getByC_N_D_G_PrevAndNext(session, parametroHijoPO,
					codigoPadre, nombre, descripcion, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ParametroHijoPO getByC_N_D_G_PrevAndNext(Session session,
		ParametroHijoPO parametroHijoPO, String codigoPadre, String nombre,
		String descripcion, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

		boolean bindCodigoPadre = false;

		if (codigoPadre == null) {
			query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_1);
		}
		else if (codigoPadre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_3);
		}
		else {
			bindCodigoPadre = true;

			query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_2);
		}

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_2);
		}

		boolean bindDescripcion = false;

		if (descripcion == null) {
			query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_1);
		}
		else if (descripcion.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_3);
		}
		else {
			bindDescripcion = true;

			query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodigoPadre) {
			qPos.add(codigoPadre);
		}

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (bindDescripcion) {
			qPos.add(descripcion);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(parametroHijoPO);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ParametroHijoPO> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the parametro hijo p os where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_N_D_G(String codigoPadre, String nombre,
		String descripcion) throws SystemException {
		for (ParametroHijoPO parametroHijoPO : findByC_N_D_G(codigoPadre,
				nombre, descripcion, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(parametroHijoPO);
		}
	}

	/**
	 * Returns the number of parametro hijo p os where codigoPadre = &#63; and nombre LIKE &#63; and descripcion LIKE &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param nombre the nombre
	 * @param descripcion the descripcion
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_N_D_G(String codigoPadre, String nombre,
		String descripcion) throws SystemException {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_C_N_D_G;

		Object[] finderArgs = new Object[] { codigoPadre, nombre, descripcion };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_N_D_G_CODIGOPADRE_2);
			}

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_C_N_D_G_NOMBRE_2);
			}

			boolean bindDescripcion = false;

			if (descripcion == null) {
				query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_1);
			}
			else if (descripcion.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_3);
			}
			else {
				bindDescripcion = true;

				query.append(_FINDER_COLUMN_C_N_D_G_DESCRIPCION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (bindDescripcion) {
					qPos.add(descripcion);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_N_D_G_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_N_D_G_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_N_D_G_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_N_D_G_NOMBRE_1 = "parametroHijoPO.nombre LIKE NULL AND ";
	private static final String _FINDER_COLUMN_C_N_D_G_NOMBRE_2 = "parametroHijoPO.nombre LIKE ? AND ";
	private static final String _FINDER_COLUMN_C_N_D_G_NOMBRE_3 = "(parametroHijoPO.nombre IS NULL OR parametroHijoPO.nombre LIKE '') AND ";
	private static final String _FINDER_COLUMN_C_N_D_G_DESCRIPCION_1 = "parametroHijoPO.descripcion LIKE NULL";
	private static final String _FINDER_COLUMN_C_N_D_G_DESCRIPCION_2 = "parametroHijoPO.descripcion LIKE ?";
	private static final String _FINDER_COLUMN_C_N_D_G_DESCRIPCION_3 = "(parametroHijoPO.descripcion IS NULL OR parametroHijoPO.descripcion LIKE '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_C_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByC_G_E",
			new String[] { String.class.getName(), Boolean.class.getName() },
			ParametroHijoPOModelImpl.CODIGO_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_G_E",
			new String[] { String.class.getName(), Boolean.class.getName() });

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPOException} if it could not be found.
	 *
	 * @param codigo the codigo
	 * @param estado the estado
	 * @return the matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_G_E(String codigo, boolean estado)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_G_E(codigo, estado);

		if (parametroHijoPO == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigo=");
			msg.append(codigo);

			msg.append(", estado=");
			msg.append(estado);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchParametroHijoPOException(msg.toString());
		}

		return parametroHijoPO;
	}

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigo the codigo
	 * @param estado the estado
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_G_E(String codigo, boolean estado)
		throws SystemException {
		return fetchByC_G_E(codigo, estado, true);
	}

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigo the codigo
	 * @param estado the estado
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_G_E(String codigo, boolean estado,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { codigo, estado };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_G_E,
					finderArgs, this);
		}

		if (result instanceof ParametroHijoPO) {
			ParametroHijoPO parametroHijoPO = (ParametroHijoPO)result;

			if (!Validator.equals(codigo, parametroHijoPO.getCodigo()) ||
					(estado != parametroHijoPO.getEstado())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_C_G_E_CODIGO_2);
			}

			query.append(_FINDER_COLUMN_C_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				qPos.add(estado);

				List<ParametroHijoPO> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ParametroHijoPOPersistenceImpl.fetchByC_G_E(String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ParametroHijoPO parametroHijoPO = list.get(0);

					result = parametroHijoPO;

					cacheResult(parametroHijoPO);

					if ((parametroHijoPO.getCodigo() == null) ||
							!parametroHijoPO.getCodigo().equals(codigo) ||
							(parametroHijoPO.getEstado() != estado)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E,
							finderArgs, parametroHijoPO);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ParametroHijoPO)result;
		}
	}

	/**
	 * Removes the parametro hijo p o where codigo = &#63; and estado = &#63; from the database.
	 *
	 * @param codigo the codigo
	 * @param estado the estado
	 * @return the parametro hijo p o that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO removeByC_G_E(String codigo, boolean estado)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByC_G_E(codigo, estado);

		return remove(parametroHijoPO);
	}

	/**
	 * Returns the number of parametro hijo p os where codigo = &#63; and estado = &#63;.
	 *
	 * @param codigo the codigo
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_G_E(String codigo, boolean estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_G_E;

		Object[] finderArgs = new Object[] { codigo, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_G_E_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_C_G_E_CODIGO_2);
			}

			query.append(_FINDER_COLUMN_C_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_G_E_CODIGO_1 = "parametroHijoPO.codigo IS NULL AND ";
	private static final String _FINDER_COLUMN_C_G_E_CODIGO_2 = "parametroHijoPO.codigo = ? AND ";
	private static final String _FINDER_COLUMN_C_G_E_CODIGO_3 = "(parametroHijoPO.codigo IS NULL OR parametroHijoPO.codigo = '') AND ";
	private static final String _FINDER_COLUMN_C_G_E_ESTADO_2 = "parametroHijoPO.estado = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_C_C_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByC_C_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName()
			},
			ParametroHijoPOModelImpl.CODIGO_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_C_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_C_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName()
			});

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPOException} if it could not be found.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_C_G_E(String codigo, String codigoPadre,
		boolean estado) throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_C_G_E(codigo, codigoPadre,
				estado);

		if (parametroHijoPO == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigo=");
			msg.append(codigo);

			msg.append(", codigoPadre=");
			msg.append(codigoPadre);

			msg.append(", estado=");
			msg.append(estado);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchParametroHijoPOException(msg.toString());
		}

		return parametroHijoPO;
	}

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_C_G_E(String codigo, String codigoPadre,
		boolean estado) throws SystemException {
		return fetchByC_C_G_E(codigo, codigoPadre, estado, true);
	}

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_C_G_E(String codigo, String codigoPadre,
		boolean estado, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { codigo, codigoPadre, estado };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_C_G_E,
					finderArgs, this);
		}

		if (result instanceof ParametroHijoPO) {
			ParametroHijoPO parametroHijoPO = (ParametroHijoPO)result;

			if (!Validator.equals(codigo, parametroHijoPO.getCodigo()) ||
					!Validator.equals(codigoPadre,
						parametroHijoPO.getCodigoPadre()) ||
					(estado != parametroHijoPO.getEstado())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_C_C_G_E_CODIGO_2);
			}

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_C_G_E_CODIGOPADRE_2);
			}

			query.append(_FINDER_COLUMN_C_C_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				qPos.add(estado);

				List<ParametroHijoPO> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_G_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ParametroHijoPOPersistenceImpl.fetchByC_C_G_E(String, String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ParametroHijoPO parametroHijoPO = list.get(0);

					result = parametroHijoPO;

					cacheResult(parametroHijoPO);

					if ((parametroHijoPO.getCodigo() == null) ||
							!parametroHijoPO.getCodigo().equals(codigo) ||
							(parametroHijoPO.getCodigoPadre() == null) ||
							!parametroHijoPO.getCodigoPadre().equals(codigoPadre) ||
							(parametroHijoPO.getEstado() != estado)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_G_E,
							finderArgs, parametroHijoPO);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_C_G_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ParametroHijoPO)result;
		}
	}

	/**
	 * Removes the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and estado = &#63; from the database.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the parametro hijo p o that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO removeByC_C_G_E(String codigo, String codigoPadre,
		boolean estado) throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByC_C_G_E(codigo, codigoPadre,
				estado);

		return remove(parametroHijoPO);
	}

	/**
	 * Returns the number of parametro hijo p os where codigo = &#63; and codigoPadre = &#63; and estado = &#63;.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_C_G_E(String codigo, String codigoPadre, boolean estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_C_G_E;

		Object[] finderArgs = new Object[] { codigo, codigoPadre, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_C_C_G_E_CODIGO_2);
			}

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_C_G_E_CODIGOPADRE_2);
			}

			query.append(_FINDER_COLUMN_C_C_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_C_G_E_CODIGO_1 = "parametroHijoPO.codigo IS NULL AND ";
	private static final String _FINDER_COLUMN_C_C_G_E_CODIGO_2 = "parametroHijoPO.codigo = ? AND ";
	private static final String _FINDER_COLUMN_C_C_G_E_CODIGO_3 = "(parametroHijoPO.codigo IS NULL OR parametroHijoPO.codigo = '') AND ";
	private static final String _FINDER_COLUMN_C_C_G_E_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_C_G_E_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_C_G_E_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_C_G_E_ESTADO_2 = "parametroHijoPO.estado = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_D_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByC_D_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_G_E =
		new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_D_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName()
			},
			ParametroHijoPOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.DATO1_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ORDEN_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_D_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_D_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName()
			});

	/**
	 * Returns all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @return the matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_D_G_E(String codigoPadre,
		String dato1, boolean estado) throws SystemException {
		return findByC_D_G_E(codigoPadre, dato1, estado, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @return the range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_D_G_E(String codigoPadre,
		String dato1, boolean estado, int start, int end)
		throws SystemException {
		return findByC_D_G_E(codigoPadre, dato1, estado, start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_D_G_E(String codigoPadre,
		String dato1, boolean estado, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_G_E;
			finderArgs = new Object[] { codigoPadre, dato1, estado };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_D_G_E;
			finderArgs = new Object[] {
					codigoPadre, dato1, estado,
					
					start, end, orderByComparator
				};
		}

		List<ParametroHijoPO> list = (List<ParametroHijoPO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ParametroHijoPO parametroHijoPO : list) {
				if (!Validator.equals(codigoPadre,
							parametroHijoPO.getCodigoPadre()) ||
						!Validator.equals(dato1, parametroHijoPO.getDato1()) ||
						(estado != parametroHijoPO.getEstado())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_2);
			}

			boolean bindDato1 = false;

			if (dato1 == null) {
				query.append(_FINDER_COLUMN_C_D_G_E_DATO1_1);
			}
			else if (dato1.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_G_E_DATO1_3);
			}
			else {
				bindDato1 = true;

				query.append(_FINDER_COLUMN_C_D_G_E_DATO1_2);
			}

			query.append(_FINDER_COLUMN_C_D_G_E_ESTADO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato1) {
					qPos.add(dato1);
				}

				qPos.add(estado);

				if (!pagination) {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroHijoPO>(list);
				}
				else {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_D_G_E_First(String codigoPadre,
		String dato1, boolean estado, OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_D_G_E_First(codigoPadre,
				dato1, estado, orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", dato1=");
		msg.append(dato1);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_D_G_E_First(String codigoPadre,
		String dato1, boolean estado, OrderByComparator orderByComparator)
		throws SystemException {
		List<ParametroHijoPO> list = findByC_D_G_E(codigoPadre, dato1, estado,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_D_G_E_Last(String codigoPadre, String dato1,
		boolean estado, OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_D_G_E_Last(codigoPadre,
				dato1, estado, orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", dato1=");
		msg.append(dato1);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_D_G_E_Last(String codigoPadre,
		String dato1, boolean estado, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByC_D_G_E(codigoPadre, dato1, estado);

		if (count == 0) {
			return null;
		}

		List<ParametroHijoPO> list = findByC_D_G_E(codigoPadre, dato1, estado,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the parametro hijo p os before and after the current parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param idParametroHijo the primary key of the current parametro hijo p o
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO[] findByC_D_G_E_PrevAndNext(long idParametroHijo,
		String codigoPadre, String dato1, boolean estado,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByPrimaryKey(idParametroHijo);

		Session session = null;

		try {
			session = openSession();

			ParametroHijoPO[] array = new ParametroHijoPOImpl[3];

			array[0] = getByC_D_G_E_PrevAndNext(session, parametroHijoPO,
					codigoPadre, dato1, estado, orderByComparator, true);

			array[1] = parametroHijoPO;

			array[2] = getByC_D_G_E_PrevAndNext(session, parametroHijoPO,
					codigoPadre, dato1, estado, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ParametroHijoPO getByC_D_G_E_PrevAndNext(Session session,
		ParametroHijoPO parametroHijoPO, String codigoPadre, String dato1,
		boolean estado, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

		boolean bindCodigoPadre = false;

		if (codigoPadre == null) {
			query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_1);
		}
		else if (codigoPadre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_3);
		}
		else {
			bindCodigoPadre = true;

			query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_2);
		}

		boolean bindDato1 = false;

		if (dato1 == null) {
			query.append(_FINDER_COLUMN_C_D_G_E_DATO1_1);
		}
		else if (dato1.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_D_G_E_DATO1_3);
		}
		else {
			bindDato1 = true;

			query.append(_FINDER_COLUMN_C_D_G_E_DATO1_2);
		}

		query.append(_FINDER_COLUMN_C_D_G_E_ESTADO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodigoPadre) {
			qPos.add(codigoPadre);
		}

		if (bindDato1) {
			qPos.add(dato1);
		}

		qPos.add(estado);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(parametroHijoPO);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ParametroHijoPO> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and estado = &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_D_G_E(String codigoPadre, String dato1, boolean estado)
		throws SystemException {
		for (ParametroHijoPO parametroHijoPO : findByC_D_G_E(codigoPadre,
				dato1, estado, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(parametroHijoPO);
		}
	}

	/**
	 * Returns the number of parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_D_G_E(String codigoPadre, String dato1, boolean estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_D_G_E;

		Object[] finderArgs = new Object[] { codigoPadre, dato1, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_D_G_E_CODIGOPADRE_2);
			}

			boolean bindDato1 = false;

			if (dato1 == null) {
				query.append(_FINDER_COLUMN_C_D_G_E_DATO1_1);
			}
			else if (dato1.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_G_E_DATO1_3);
			}
			else {
				bindDato1 = true;

				query.append(_FINDER_COLUMN_C_D_G_E_DATO1_2);
			}

			query.append(_FINDER_COLUMN_C_D_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato1) {
					qPos.add(dato1);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_D_G_E_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D_G_E_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_D_G_E_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_D_G_E_DATO1_1 = "parametroHijoPO.dato1 IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D_G_E_DATO1_2 = "parametroHijoPO.dato1 = ? AND ";
	private static final String _FINDER_COLUMN_C_D_G_E_DATO1_3 = "(parametroHijoPO.dato1 IS NULL OR parametroHijoPO.dato1 = '') AND ";
	private static final String _FINDER_COLUMN_C_D_G_E_ESTADO_2 = "parametroHijoPO.estado = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_D_D_G_E =
		new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByC_D_D_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_D_G_E =
		new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_D_D_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Boolean.class.getName()
			},
			ParametroHijoPOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.DATO1_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.DATO2_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ORDEN_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_D_D_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_D_D_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Boolean.class.getName()
			});

	/**
	 * Returns all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_D_D_G_E(String codigoPadre,
		String dato1, String dato2, boolean estado) throws SystemException {
		return findByC_D_D_G_E(codigoPadre, dato1, dato2, estado,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @return the range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_D_D_G_E(String codigoPadre,
		String dato1, String dato2, boolean estado, int start, int end)
		throws SystemException {
		return findByC_D_D_G_E(codigoPadre, dato1, dato2, estado, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findByC_D_D_G_E(String codigoPadre,
		String dato1, String dato2, boolean estado, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_D_G_E;
			finderArgs = new Object[] { codigoPadre, dato1, dato2, estado };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_D_D_G_E;
			finderArgs = new Object[] {
					codigoPadre, dato1, dato2, estado,
					
					start, end, orderByComparator
				};
		}

		List<ParametroHijoPO> list = (List<ParametroHijoPO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ParametroHijoPO parametroHijoPO : list) {
				if (!Validator.equals(codigoPadre,
							parametroHijoPO.getCodigoPadre()) ||
						!Validator.equals(dato1, parametroHijoPO.getDato1()) ||
						!Validator.equals(dato2, parametroHijoPO.getDato2()) ||
						(estado != parametroHijoPO.getEstado())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(6 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(6);
			}

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_2);
			}

			boolean bindDato1 = false;

			if (dato1 == null) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_1);
			}
			else if (dato1.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_3);
			}
			else {
				bindDato1 = true;

				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_2);
			}

			boolean bindDato2 = false;

			if (dato2 == null) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_1);
			}
			else if (dato2.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_3);
			}
			else {
				bindDato2 = true;

				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_2);
			}

			query.append(_FINDER_COLUMN_C_D_D_G_E_ESTADO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato1) {
					qPos.add(dato1);
				}

				if (bindDato2) {
					qPos.add(dato2);
				}

				qPos.add(estado);

				if (!pagination) {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroHijoPO>(list);
				}
				else {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_D_D_G_E_First(String codigoPadre,
		String dato1, String dato2, boolean estado,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_D_D_G_E_First(codigoPadre,
				dato1, dato2, estado, orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", dato1=");
		msg.append(dato1);

		msg.append(", dato2=");
		msg.append(dato2);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the first parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_D_D_G_E_First(String codigoPadre,
		String dato1, String dato2, boolean estado,
		OrderByComparator orderByComparator) throws SystemException {
		List<ParametroHijoPO> list = findByC_D_D_G_E(codigoPadre, dato1, dato2,
				estado, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_D_D_G_E_Last(String codigoPadre,
		String dato1, String dato2, boolean estado,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_D_D_G_E_Last(codigoPadre,
				dato1, dato2, estado, orderByComparator);

		if (parametroHijoPO != null) {
			return parametroHijoPO;
		}

		StringBundler msg = new StringBundler(10);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigoPadre=");
		msg.append(codigoPadre);

		msg.append(", dato1=");
		msg.append(dato1);

		msg.append(", dato2=");
		msg.append(dato2);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchParametroHijoPOException(msg.toString());
	}

	/**
	 * Returns the last parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_D_D_G_E_Last(String codigoPadre,
		String dato1, String dato2, boolean estado,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_D_D_G_E(codigoPadre, dato1, dato2, estado);

		if (count == 0) {
			return null;
		}

		List<ParametroHijoPO> list = findByC_D_D_G_E(codigoPadre, dato1, dato2,
				estado, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the parametro hijo p os before and after the current parametro hijo p o in the ordered set where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param idParametroHijo the primary key of the current parametro hijo p o
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO[] findByC_D_D_G_E_PrevAndNext(long idParametroHijo,
		String codigoPadre, String dato1, String dato2, boolean estado,
		OrderByComparator orderByComparator)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByPrimaryKey(idParametroHijo);

		Session session = null;

		try {
			session = openSession();

			ParametroHijoPO[] array = new ParametroHijoPOImpl[3];

			array[0] = getByC_D_D_G_E_PrevAndNext(session, parametroHijoPO,
					codigoPadre, dato1, dato2, estado, orderByComparator, true);

			array[1] = parametroHijoPO;

			array[2] = getByC_D_D_G_E_PrevAndNext(session, parametroHijoPO,
					codigoPadre, dato1, dato2, estado, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ParametroHijoPO getByC_D_D_G_E_PrevAndNext(Session session,
		ParametroHijoPO parametroHijoPO, String codigoPadre, String dato1,
		String dato2, boolean estado, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

		boolean bindCodigoPadre = false;

		if (codigoPadre == null) {
			query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_1);
		}
		else if (codigoPadre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_3);
		}
		else {
			bindCodigoPadre = true;

			query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_2);
		}

		boolean bindDato1 = false;

		if (dato1 == null) {
			query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_1);
		}
		else if (dato1.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_3);
		}
		else {
			bindDato1 = true;

			query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_2);
		}

		boolean bindDato2 = false;

		if (dato2 == null) {
			query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_1);
		}
		else if (dato2.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_3);
		}
		else {
			bindDato2 = true;

			query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_2);
		}

		query.append(_FINDER_COLUMN_C_D_D_G_E_ESTADO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCodigoPadre) {
			qPos.add(codigoPadre);
		}

		if (bindDato1) {
			qPos.add(dato1);
		}

		if (bindDato2) {
			qPos.add(dato2);
		}

		qPos.add(estado);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(parametroHijoPO);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ParametroHijoPO> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_D_D_G_E(String codigoPadre, String dato1,
		String dato2, boolean estado) throws SystemException {
		for (ParametroHijoPO parametroHijoPO : findByC_D_D_G_E(codigoPadre,
				dato1, dato2, estado, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(parametroHijoPO);
		}
	}

	/**
	 * Returns the number of parametro hijo p os where codigoPadre = &#63; and dato1 = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato1 the dato1
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_D_D_G_E(String codigoPadre, String dato1, String dato2,
		boolean estado) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_D_D_G_E;

		Object[] finderArgs = new Object[] { codigoPadre, dato1, dato2, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_2);
			}

			boolean bindDato1 = false;

			if (dato1 == null) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_1);
			}
			else if (dato1.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_3);
			}
			else {
				bindDato1 = true;

				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO1_2);
			}

			boolean bindDato2 = false;

			if (dato2 == null) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_1);
			}
			else if (dato2.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_3);
			}
			else {
				bindDato2 = true;

				query.append(_FINDER_COLUMN_C_D_D_G_E_DATO2_2);
			}

			query.append(_FINDER_COLUMN_C_D_D_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato1) {
					qPos.add(dato1);
				}

				if (bindDato2) {
					qPos.add(dato2);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_DATO1_1 = "parametroHijoPO.dato1 IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_DATO1_2 = "parametroHijoPO.dato1 = ? AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_DATO1_3 = "(parametroHijoPO.dato1 IS NULL OR parametroHijoPO.dato1 = '') AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_DATO2_1 = "parametroHijoPO.dato2 IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_DATO2_2 = "parametroHijoPO.dato2 = ? AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_DATO2_3 = "(parametroHijoPO.dato2 IS NULL OR parametroHijoPO.dato2 = '') AND ";
	private static final String _FINDER_COLUMN_C_D_D_G_E_ESTADO_2 = "parametroHijoPO.estado = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_C_D2_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByC_D2_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName()
			},
			ParametroHijoPOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.DATO2_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_D2_G_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_D2_G_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				Boolean.class.getName()
			});

	/**
	 * Returns the parametro hijo p o where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPOException} if it could not be found.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_D2_G_E(String codigoPadre, String dato2,
		boolean estado) throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_D2_G_E(codigoPadre, dato2,
				estado);

		if (parametroHijoPO == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigoPadre=");
			msg.append(codigoPadre);

			msg.append(", dato2=");
			msg.append(dato2);

			msg.append(", estado=");
			msg.append(estado);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchParametroHijoPOException(msg.toString());
		}

		return parametroHijoPO;
	}

	/**
	 * Returns the parametro hijo p o where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_D2_G_E(String codigoPadre, String dato2,
		boolean estado) throws SystemException {
		return fetchByC_D2_G_E(codigoPadre, dato2, estado, true);
	}

	/**
	 * Returns the parametro hijo p o where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_D2_G_E(String codigoPadre, String dato2,
		boolean estado, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { codigoPadre, dato2, estado };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_D2_G_E,
					finderArgs, this);
		}

		if (result instanceof ParametroHijoPO) {
			ParametroHijoPO parametroHijoPO = (ParametroHijoPO)result;

			if (!Validator.equals(codigoPadre, parametroHijoPO.getCodigoPadre()) ||
					!Validator.equals(dato2, parametroHijoPO.getDato2()) ||
					(estado != parametroHijoPO.getEstado())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_2);
			}

			boolean bindDato2 = false;

			if (dato2 == null) {
				query.append(_FINDER_COLUMN_C_D2_G_E_DATO2_1);
			}
			else if (dato2.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D2_G_E_DATO2_3);
			}
			else {
				bindDato2 = true;

				query.append(_FINDER_COLUMN_C_D2_G_E_DATO2_2);
			}

			query.append(_FINDER_COLUMN_C_D2_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato2) {
					qPos.add(dato2);
				}

				qPos.add(estado);

				List<ParametroHijoPO> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_D2_G_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ParametroHijoPOPersistenceImpl.fetchByC_D2_G_E(String, String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ParametroHijoPO parametroHijoPO = list.get(0);

					result = parametroHijoPO;

					cacheResult(parametroHijoPO);

					if ((parametroHijoPO.getCodigoPadre() == null) ||
							!parametroHijoPO.getCodigoPadre().equals(codigoPadre) ||
							(parametroHijoPO.getDato2() == null) ||
							!parametroHijoPO.getDato2().equals(dato2) ||
							(parametroHijoPO.getEstado() != estado)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_D2_G_E,
							finderArgs, parametroHijoPO);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_D2_G_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ParametroHijoPO)result;
		}
	}

	/**
	 * Removes the parametro hijo p o where codigoPadre = &#63; and dato2 = &#63; and estado = &#63; from the database.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the parametro hijo p o that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO removeByC_D2_G_E(String codigoPadre, String dato2,
		boolean estado) throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByC_D2_G_E(codigoPadre, dato2,
				estado);

		return remove(parametroHijoPO);
	}

	/**
	 * Returns the number of parametro hijo p os where codigoPadre = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_D2_G_E(String codigoPadre, String dato2, boolean estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_D2_G_E;

		Object[] finderArgs = new Object[] { codigoPadre, dato2, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_2);
			}

			boolean bindDato2 = false;

			if (dato2 == null) {
				query.append(_FINDER_COLUMN_C_D2_G_E_DATO2_1);
			}
			else if (dato2.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_D2_G_E_DATO2_3);
			}
			else {
				bindDato2 = true;

				query.append(_FINDER_COLUMN_C_D2_G_E_DATO2_2);
			}

			query.append(_FINDER_COLUMN_C_D2_G_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato2) {
					qPos.add(dato2);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_D2_G_E_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_D2_G_E_DATO2_1 = "parametroHijoPO.dato2 IS NULL AND ";
	private static final String _FINDER_COLUMN_C_D2_G_E_DATO2_2 = "parametroHijoPO.dato2 = ? AND ";
	private static final String _FINDER_COLUMN_C_D2_G_E_DATO2_3 = "(parametroHijoPO.dato2 IS NULL OR parametroHijoPO.dato2 = '') AND ";
	private static final String _FINDER_COLUMN_C_D2_G_E_ESTADO_2 = "parametroHijoPO.estado = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_C_C_D2_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED,
			ParametroHijoPOImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByC_C_D2_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Boolean.class.getName()
			},
			ParametroHijoPOModelImpl.CODIGO_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.CODIGOPADRE_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.DATO2_COLUMN_BITMASK |
			ParametroHijoPOModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_C_D2_E = new FinderPath(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_C_D2_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(), Boolean.class.getName()
			});

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPOException} if it could not be found.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the matching parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByC_C_D2_E(String codigo, String codigoPadre,
		String dato2, boolean estado)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByC_C_D2_E(codigo, codigoPadre,
				dato2, estado);

		if (parametroHijoPO == null) {
			StringBundler msg = new StringBundler(10);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigo=");
			msg.append(codigo);

			msg.append(", codigoPadre=");
			msg.append(codigoPadre);

			msg.append(", dato2=");
			msg.append(dato2);

			msg.append(", estado=");
			msg.append(estado);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchParametroHijoPOException(msg.toString());
		}

		return parametroHijoPO;
	}

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_C_D2_E(String codigo, String codigoPadre,
		String dato2, boolean estado) throws SystemException {
		return fetchByC_C_D2_E(codigo, codigoPadre, dato2, estado, true);
	}

	/**
	 * Returns the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching parametro hijo p o, or <code>null</code> if a matching parametro hijo p o could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByC_C_D2_E(String codigo, String codigoPadre,
		String dato2, boolean estado, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { codigo, codigoPadre, dato2, estado };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_C_D2_E,
					finderArgs, this);
		}

		if (result instanceof ParametroHijoPO) {
			ParametroHijoPO parametroHijoPO = (ParametroHijoPO)result;

			if (!Validator.equals(codigo, parametroHijoPO.getCodigo()) ||
					!Validator.equals(codigoPadre,
						parametroHijoPO.getCodigoPadre()) ||
					!Validator.equals(dato2, parametroHijoPO.getDato2()) ||
					(estado != parametroHijoPO.getEstado())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(6);

			query.append(_SQL_SELECT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGO_2);
			}

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_2);
			}

			boolean bindDato2 = false;

			if (dato2 == null) {
				query.append(_FINDER_COLUMN_C_C_D2_E_DATO2_1);
			}
			else if (dato2.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_D2_E_DATO2_3);
			}
			else {
				bindDato2 = true;

				query.append(_FINDER_COLUMN_C_C_D2_E_DATO2_2);
			}

			query.append(_FINDER_COLUMN_C_C_D2_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato2) {
					qPos.add(dato2);
				}

				qPos.add(estado);

				List<ParametroHijoPO> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_D2_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ParametroHijoPOPersistenceImpl.fetchByC_C_D2_E(String, String, String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ParametroHijoPO parametroHijoPO = list.get(0);

					result = parametroHijoPO;

					cacheResult(parametroHijoPO);

					if ((parametroHijoPO.getCodigo() == null) ||
							!parametroHijoPO.getCodigo().equals(codigo) ||
							(parametroHijoPO.getCodigoPadre() == null) ||
							!parametroHijoPO.getCodigoPadre().equals(codigoPadre) ||
							(parametroHijoPO.getDato2() == null) ||
							!parametroHijoPO.getDato2().equals(dato2) ||
							(parametroHijoPO.getEstado() != estado)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_D2_E,
							finderArgs, parametroHijoPO);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_C_D2_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ParametroHijoPO)result;
		}
	}

	/**
	 * Removes the parametro hijo p o where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63; from the database.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the parametro hijo p o that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO removeByC_C_D2_E(String codigo, String codigoPadre,
		String dato2, boolean estado)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = findByC_C_D2_E(codigo, codigoPadre,
				dato2, estado);

		return remove(parametroHijoPO);
	}

	/**
	 * Returns the number of parametro hijo p os where codigo = &#63; and codigoPadre = &#63; and dato2 = &#63; and estado = &#63;.
	 *
	 * @param codigo the codigo
	 * @param codigoPadre the codigo padre
	 * @param dato2 the dato2
	 * @param estado the estado
	 * @return the number of matching parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_C_D2_E(String codigo, String codigoPadre, String dato2,
		boolean estado) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_C_D2_E;

		Object[] finderArgs = new Object[] { codigo, codigoPadre, dato2, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_COUNT_PARAMETROHIJOPO_WHERE);

			boolean bindCodigo = false;

			if (codigo == null) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGO_1);
			}
			else if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGO_3);
			}
			else {
				bindCodigo = true;

				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGO_2);
			}

			boolean bindCodigoPadre = false;

			if (codigoPadre == null) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_1);
			}
			else if (codigoPadre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_3);
			}
			else {
				bindCodigoPadre = true;

				query.append(_FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_2);
			}

			boolean bindDato2 = false;

			if (dato2 == null) {
				query.append(_FINDER_COLUMN_C_C_D2_E_DATO2_1);
			}
			else if (dato2.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_C_D2_E_DATO2_3);
			}
			else {
				bindDato2 = true;

				query.append(_FINDER_COLUMN_C_C_D2_E_DATO2_2);
			}

			query.append(_FINDER_COLUMN_C_C_D2_E_ESTADO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigo) {
					qPos.add(codigo);
				}

				if (bindCodigoPadre) {
					qPos.add(codigoPadre);
				}

				if (bindDato2) {
					qPos.add(dato2);
				}

				qPos.add(estado);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_C_D2_E_CODIGO_1 = "parametroHijoPO.codigo IS NULL AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_CODIGO_2 = "parametroHijoPO.codigo = ? AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_CODIGO_3 = "(parametroHijoPO.codigo IS NULL OR parametroHijoPO.codigo = '') AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_1 = "parametroHijoPO.codigoPadre IS NULL AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_2 = "parametroHijoPO.codigoPadre = ? AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_CODIGOPADRE_3 = "(parametroHijoPO.codigoPadre IS NULL OR parametroHijoPO.codigoPadre = '') AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_DATO2_1 = "parametroHijoPO.dato2 IS NULL AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_DATO2_2 = "parametroHijoPO.dato2 = ? AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_DATO2_3 = "(parametroHijoPO.dato2 IS NULL OR parametroHijoPO.dato2 = '') AND ";
	private static final String _FINDER_COLUMN_C_C_D2_E_ESTADO_2 = "parametroHijoPO.estado = ?";

	public ParametroHijoPOPersistenceImpl() {
		setModelClass(ParametroHijoPO.class);
	}

	/**
	 * Caches the parametro hijo p o in the entity cache if it is enabled.
	 *
	 * @param parametroHijoPO the parametro hijo p o
	 */
	@Override
	public void cacheResult(ParametroHijoPO parametroHijoPO) {
		EntityCacheUtil.putResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOImpl.class, parametroHijoPO.getPrimaryKey(),
			parametroHijoPO);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E,
			new Object[] {
				parametroHijoPO.getCodigo(), parametroHijoPO.getEstado()
			}, parametroHijoPO);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_G_E,
			new Object[] {
				parametroHijoPO.getCodigo(), parametroHijoPO.getCodigoPadre(),
				parametroHijoPO.getEstado()
			}, parametroHijoPO);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_D2_G_E,
			new Object[] {
				parametroHijoPO.getCodigoPadre(), parametroHijoPO.getDato2(),
				parametroHijoPO.getEstado()
			}, parametroHijoPO);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_D2_E,
			new Object[] {
				parametroHijoPO.getCodigo(), parametroHijoPO.getCodigoPadre(),
				parametroHijoPO.getDato2(), parametroHijoPO.getEstado()
			}, parametroHijoPO);

		parametroHijoPO.resetOriginalValues();
	}

	/**
	 * Caches the parametro hijo p os in the entity cache if it is enabled.
	 *
	 * @param parametroHijoPOs the parametro hijo p os
	 */
	@Override
	public void cacheResult(List<ParametroHijoPO> parametroHijoPOs) {
		for (ParametroHijoPO parametroHijoPO : parametroHijoPOs) {
			if (EntityCacheUtil.getResult(
						ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
						ParametroHijoPOImpl.class,
						parametroHijoPO.getPrimaryKey()) == null) {
				cacheResult(parametroHijoPO);
			}
			else {
				parametroHijoPO.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all parametro hijo p os.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ParametroHijoPOImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ParametroHijoPOImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the parametro hijo p o.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ParametroHijoPO parametroHijoPO) {
		EntityCacheUtil.removeResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOImpl.class, parametroHijoPO.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(parametroHijoPO);
	}

	@Override
	public void clearCache(List<ParametroHijoPO> parametroHijoPOs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ParametroHijoPO parametroHijoPO : parametroHijoPOs) {
			EntityCacheUtil.removeResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
				ParametroHijoPOImpl.class, parametroHijoPO.getPrimaryKey());

			clearUniqueFindersCache(parametroHijoPO);
		}
	}

	protected void cacheUniqueFindersCache(ParametroHijoPO parametroHijoPO) {
		if (parametroHijoPO.isNew()) {
			Object[] args = new Object[] {
					parametroHijoPO.getCodigo(), parametroHijoPO.getEstado()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E, args,
				parametroHijoPO);

			args = new Object[] {
					parametroHijoPO.getCodigo(),
					parametroHijoPO.getCodigoPadre(),
					parametroHijoPO.getEstado()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_C_G_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_G_E, args,
				parametroHijoPO);

			args = new Object[] {
					parametroHijoPO.getCodigoPadre(), parametroHijoPO.getDato2(),
					parametroHijoPO.getEstado()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_D2_G_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_D2_G_E, args,
				parametroHijoPO);

			args = new Object[] {
					parametroHijoPO.getCodigo(),
					parametroHijoPO.getCodigoPadre(), parametroHijoPO.getDato2(),
					parametroHijoPO.getEstado()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_C_D2_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_D2_E, args,
				parametroHijoPO);
		}
		else {
			ParametroHijoPOModelImpl parametroHijoPOModelImpl = (ParametroHijoPOModelImpl)parametroHijoPO;

			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_G_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPO.getCodigo(), parametroHijoPO.getEstado()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_G_E, args,
					parametroHijoPO);
			}

			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_C_G_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPO.getCodigo(),
						parametroHijoPO.getCodigoPadre(),
						parametroHijoPO.getEstado()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_C_G_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_G_E, args,
					parametroHijoPO);
			}

			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_D2_G_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPO.getCodigoPadre(),
						parametroHijoPO.getDato2(), parametroHijoPO.getEstado()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_D2_G_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_D2_G_E, args,
					parametroHijoPO);
			}

			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_C_D2_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPO.getCodigo(),
						parametroHijoPO.getCodigoPadre(),
						parametroHijoPO.getDato2(), parametroHijoPO.getEstado()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_C_D2_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_C_D2_E, args,
					parametroHijoPO);
			}
		}
	}

	protected void clearUniqueFindersCache(ParametroHijoPO parametroHijoPO) {
		ParametroHijoPOModelImpl parametroHijoPOModelImpl = (ParametroHijoPOModelImpl)parametroHijoPO;

		Object[] args = new Object[] {
				parametroHijoPO.getCodigo(), parametroHijoPO.getEstado()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G_E, args);

		if ((parametroHijoPOModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_G_E.getColumnBitmask()) != 0) {
			args = new Object[] {
					parametroHijoPOModelImpl.getOriginalCodigo(),
					parametroHijoPOModelImpl.getOriginalEstado()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_G_E, args);
		}

		args = new Object[] {
				parametroHijoPO.getCodigo(), parametroHijoPO.getCodigoPadre(),
				parametroHijoPO.getEstado()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C_G_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_C_G_E, args);

		if ((parametroHijoPOModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_C_G_E.getColumnBitmask()) != 0) {
			args = new Object[] {
					parametroHijoPOModelImpl.getOriginalCodigo(),
					parametroHijoPOModelImpl.getOriginalCodigoPadre(),
					parametroHijoPOModelImpl.getOriginalEstado()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C_G_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_C_G_E, args);
		}

		args = new Object[] {
				parametroHijoPO.getCodigoPadre(), parametroHijoPO.getDato2(),
				parametroHijoPO.getEstado()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_D2_G_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_D2_G_E, args);

		if ((parametroHijoPOModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_D2_G_E.getColumnBitmask()) != 0) {
			args = new Object[] {
					parametroHijoPOModelImpl.getOriginalCodigoPadre(),
					parametroHijoPOModelImpl.getOriginalDato2(),
					parametroHijoPOModelImpl.getOriginalEstado()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_D2_G_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_D2_G_E, args);
		}

		args = new Object[] {
				parametroHijoPO.getCodigo(), parametroHijoPO.getCodigoPadre(),
				parametroHijoPO.getDato2(), parametroHijoPO.getEstado()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C_D2_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_C_D2_E, args);

		if ((parametroHijoPOModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_C_D2_E.getColumnBitmask()) != 0) {
			args = new Object[] {
					parametroHijoPOModelImpl.getOriginalCodigo(),
					parametroHijoPOModelImpl.getOriginalCodigoPadre(),
					parametroHijoPOModelImpl.getOriginalDato2(),
					parametroHijoPOModelImpl.getOriginalEstado()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C_D2_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_C_D2_E, args);
		}
	}

	/**
	 * Creates a new parametro hijo p o with the primary key. Does not add the parametro hijo p o to the database.
	 *
	 * @param idParametroHijo the primary key for the new parametro hijo p o
	 * @return the new parametro hijo p o
	 */
	@Override
	public ParametroHijoPO create(long idParametroHijo) {
		ParametroHijoPO parametroHijoPO = new ParametroHijoPOImpl();

		parametroHijoPO.setNew(true);
		parametroHijoPO.setPrimaryKey(idParametroHijo);

		return parametroHijoPO;
	}

	/**
	 * Removes the parametro hijo p o with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idParametroHijo the primary key of the parametro hijo p o
	 * @return the parametro hijo p o that was removed
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO remove(long idParametroHijo)
		throws NoSuchParametroHijoPOException, SystemException {
		return remove((Serializable)idParametroHijo);
	}

	/**
	 * Removes the parametro hijo p o with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the parametro hijo p o
	 * @return the parametro hijo p o that was removed
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO remove(Serializable primaryKey)
		throws NoSuchParametroHijoPOException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ParametroHijoPO parametroHijoPO = (ParametroHijoPO)session.get(ParametroHijoPOImpl.class,
					primaryKey);

			if (parametroHijoPO == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchParametroHijoPOException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(parametroHijoPO);
		}
		catch (NoSuchParametroHijoPOException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ParametroHijoPO removeImpl(ParametroHijoPO parametroHijoPO)
		throws SystemException {
		parametroHijoPO = toUnwrappedModel(parametroHijoPO);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(parametroHijoPO)) {
				parametroHijoPO = (ParametroHijoPO)session.get(ParametroHijoPOImpl.class,
						parametroHijoPO.getPrimaryKeyObj());
			}

			if (parametroHijoPO != null) {
				session.delete(parametroHijoPO);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (parametroHijoPO != null) {
			clearCache(parametroHijoPO);
		}

		return parametroHijoPO;
	}

	@Override
	public ParametroHijoPO updateImpl(
		pe.com.ibk.pepper.model.ParametroHijoPO parametroHijoPO)
		throws SystemException {
		parametroHijoPO = toUnwrappedModel(parametroHijoPO);

		boolean isNew = parametroHijoPO.isNew();

		ParametroHijoPOModelImpl parametroHijoPOModelImpl = (ParametroHijoPOModelImpl)parametroHijoPO;

		Session session = null;

		try {
			session = openSession();

			if (parametroHijoPO.isNew()) {
				session.save(parametroHijoPO);

				parametroHijoPO.setNew(false);
			}
			else {
				session.merge(parametroHijoPO);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ParametroHijoPOModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPOModelImpl.getOriginalCodigoPadre(),
						parametroHijoPOModelImpl.getOriginalEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G,
					args);

				args = new Object[] {
						parametroHijoPOModelImpl.getCodigoPadre(),
						parametroHijoPOModelImpl.getEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G,
					args);
			}

			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_G_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPOModelImpl.getOriginalCodigoPadre(),
						parametroHijoPOModelImpl.getOriginalDato1(),
						parametroHijoPOModelImpl.getOriginalEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_D_G_E, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_G_E,
					args);

				args = new Object[] {
						parametroHijoPOModelImpl.getCodigoPadre(),
						parametroHijoPOModelImpl.getDato1(),
						parametroHijoPOModelImpl.getEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_D_G_E, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_G_E,
					args);
			}

			if ((parametroHijoPOModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_D_G_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						parametroHijoPOModelImpl.getOriginalCodigoPadre(),
						parametroHijoPOModelImpl.getOriginalDato1(),
						parametroHijoPOModelImpl.getOriginalDato2(),
						parametroHijoPOModelImpl.getOriginalEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_D_D_G_E,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_D_G_E,
					args);

				args = new Object[] {
						parametroHijoPOModelImpl.getCodigoPadre(),
						parametroHijoPOModelImpl.getDato1(),
						parametroHijoPOModelImpl.getDato2(),
						parametroHijoPOModelImpl.getEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_D_D_G_E,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_D_D_G_E,
					args);
			}
		}

		EntityCacheUtil.putResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
			ParametroHijoPOImpl.class, parametroHijoPO.getPrimaryKey(),
			parametroHijoPO);

		clearUniqueFindersCache(parametroHijoPO);
		cacheUniqueFindersCache(parametroHijoPO);

		return parametroHijoPO;
	}

	protected ParametroHijoPO toUnwrappedModel(ParametroHijoPO parametroHijoPO) {
		if (parametroHijoPO instanceof ParametroHijoPOImpl) {
			return parametroHijoPO;
		}

		ParametroHijoPOImpl parametroHijoPOImpl = new ParametroHijoPOImpl();

		parametroHijoPOImpl.setNew(parametroHijoPO.isNew());
		parametroHijoPOImpl.setPrimaryKey(parametroHijoPO.getPrimaryKey());

		parametroHijoPOImpl.setIdParametroHijo(parametroHijoPO.getIdParametroHijo());
		parametroHijoPOImpl.setGroupId(parametroHijoPO.getGroupId());
		parametroHijoPOImpl.setCompanyId(parametroHijoPO.getCompanyId());
		parametroHijoPOImpl.setUserId(parametroHijoPO.getUserId());
		parametroHijoPOImpl.setUserName(parametroHijoPO.getUserName());
		parametroHijoPOImpl.setCreateDate(parametroHijoPO.getCreateDate());
		parametroHijoPOImpl.setModifiedDate(parametroHijoPO.getModifiedDate());
		parametroHijoPOImpl.setCodigoPadre(parametroHijoPO.getCodigoPadre());
		parametroHijoPOImpl.setCodigo(parametroHijoPO.getCodigo());
		parametroHijoPOImpl.setNombre(parametroHijoPO.getNombre());
		parametroHijoPOImpl.setDescripcion(parametroHijoPO.getDescripcion());
		parametroHijoPOImpl.setEstado(parametroHijoPO.isEstado());
		parametroHijoPOImpl.setOrden(parametroHijoPO.getOrden());
		parametroHijoPOImpl.setDato1(parametroHijoPO.getDato1());
		parametroHijoPOImpl.setDato2(parametroHijoPO.getDato2());
		parametroHijoPOImpl.setDato3(parametroHijoPO.getDato3());
		parametroHijoPOImpl.setDato4(parametroHijoPO.getDato4());
		parametroHijoPOImpl.setDato5(parametroHijoPO.getDato5());
		parametroHijoPOImpl.setDato6(parametroHijoPO.getDato6());
		parametroHijoPOImpl.setDato7(parametroHijoPO.getDato7());
		parametroHijoPOImpl.setDato8(parametroHijoPO.getDato8());

		return parametroHijoPOImpl;
	}

	/**
	 * Returns the parametro hijo p o with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the parametro hijo p o
	 * @return the parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByPrimaryKey(Serializable primaryKey)
		throws NoSuchParametroHijoPOException, SystemException {
		ParametroHijoPO parametroHijoPO = fetchByPrimaryKey(primaryKey);

		if (parametroHijoPO == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchParametroHijoPOException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return parametroHijoPO;
	}

	/**
	 * Returns the parametro hijo p o with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchParametroHijoPOException} if it could not be found.
	 *
	 * @param idParametroHijo the primary key of the parametro hijo p o
	 * @return the parametro hijo p o
	 * @throws pe.com.ibk.pepper.NoSuchParametroHijoPOException if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO findByPrimaryKey(long idParametroHijo)
		throws NoSuchParametroHijoPOException, SystemException {
		return findByPrimaryKey((Serializable)idParametroHijo);
	}

	/**
	 * Returns the parametro hijo p o with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the parametro hijo p o
	 * @return the parametro hijo p o, or <code>null</code> if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ParametroHijoPO parametroHijoPO = (ParametroHijoPO)EntityCacheUtil.getResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
				ParametroHijoPOImpl.class, primaryKey);

		if (parametroHijoPO == _nullParametroHijoPO) {
			return null;
		}

		if (parametroHijoPO == null) {
			Session session = null;

			try {
				session = openSession();

				parametroHijoPO = (ParametroHijoPO)session.get(ParametroHijoPOImpl.class,
						primaryKey);

				if (parametroHijoPO != null) {
					cacheResult(parametroHijoPO);
				}
				else {
					EntityCacheUtil.putResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
						ParametroHijoPOImpl.class, primaryKey,
						_nullParametroHijoPO);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ParametroHijoPOModelImpl.ENTITY_CACHE_ENABLED,
					ParametroHijoPOImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return parametroHijoPO;
	}

	/**
	 * Returns the parametro hijo p o with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idParametroHijo the primary key of the parametro hijo p o
	 * @return the parametro hijo p o, or <code>null</code> if a parametro hijo p o with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ParametroHijoPO fetchByPrimaryKey(long idParametroHijo)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idParametroHijo);
	}

	/**
	 * Returns all the parametro hijo p os.
	 *
	 * @return the parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the parametro hijo p os.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @return the range of parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the parametro hijo p os.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroHijoPOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of parametro hijo p os
	 * @param end the upper bound of the range of parametro hijo p os (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ParametroHijoPO> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ParametroHijoPO> list = (List<ParametroHijoPO>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PARAMETROHIJOPO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PARAMETROHIJOPO;

				if (pagination) {
					sql = sql.concat(ParametroHijoPOModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ParametroHijoPO>(list);
				}
				else {
					list = (List<ParametroHijoPO>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the parametro hijo p os from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ParametroHijoPO parametroHijoPO : findAll()) {
			remove(parametroHijoPO);
		}
	}

	/**
	 * Returns the number of parametro hijo p os.
	 *
	 * @return the number of parametro hijo p os
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PARAMETROHIJOPO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the parametro hijo p o persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.ParametroHijoPO")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ParametroHijoPO>> listenersList = new ArrayList<ModelListener<ParametroHijoPO>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ParametroHijoPO>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ParametroHijoPOImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PARAMETROHIJOPO = "SELECT parametroHijoPO FROM ParametroHijoPO parametroHijoPO";
	private static final String _SQL_SELECT_PARAMETROHIJOPO_WHERE = "SELECT parametroHijoPO FROM ParametroHijoPO parametroHijoPO WHERE ";
	private static final String _SQL_COUNT_PARAMETROHIJOPO = "SELECT COUNT(parametroHijoPO) FROM ParametroHijoPO parametroHijoPO";
	private static final String _SQL_COUNT_PARAMETROHIJOPO_WHERE = "SELECT COUNT(parametroHijoPO) FROM ParametroHijoPO parametroHijoPO WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "parametroHijoPO.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ParametroHijoPO exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ParametroHijoPO exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ParametroHijoPOPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idParametroHijo", "groupId", "companyId", "userId", "userName",
				"createDate", "modifiedDate", "codigoPadre", "codigo", "nombre",
				"descripcion", "estado", "orden", "dato1", "dato2", "dato3",
				"dato4", "dato5", "dato6", "dato7", "dato8"
			});
	private static ParametroHijoPO _nullParametroHijoPO = new ParametroHijoPOImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ParametroHijoPO> toCacheModel() {
				return _nullParametroHijoPOCacheModel;
			}
		};

	private static CacheModel<ParametroHijoPO> _nullParametroHijoPOCacheModel = new CacheModel<ParametroHijoPO>() {
			@Override
			public ParametroHijoPO toEntityModel() {
				return _nullParametroHijoPO;
			}
		};
}