
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.generic.request.HttpHeadersV2;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AltaTcRequest {

	private HttpHeadersV2 httpHeader;
    private InformacionEntrega informacionEntrega;
    private GrabarLPD grabarLPD;
    private DatosCliente datosCliente;
    private DatosExpediente datosExpediente;
    private DatosCampania datosCampania;
    private AltaTCProvisional altaTCProvisional;
    private DatosConyuge datosConyuge;
    private DatosMonitor datosMonitor;
    private DatosToken datosToken;

    @JsonCreator
    public AltaTcRequest() {
    	super();
    }

    @JsonCreator
	public AltaTcRequest(HttpHeadersV2 httpHeader,
			InformacionEntrega informacionEntrega, GrabarLPD grabarLPD,
			DatosCliente datosCliente, DatosExpediente datosExpediente,
			DatosCampania datosCampania, AltaTCProvisional altaTCProvisional,
			DatosConyuge datosConyuge, DatosMonitor datosMonitor,
			DatosToken datosToken) {
		super();
		this.httpHeader = httpHeader;
		this.informacionEntrega = informacionEntrega;
		this.grabarLPD = grabarLPD;
		this.datosCliente = datosCliente;
		this.datosExpediente = datosExpediente;
		this.datosCampania = datosCampania;
		this.altaTCProvisional = altaTCProvisional;
		this.datosConyuge = datosConyuge;
		this.datosMonitor = datosMonitor;
		this.datosToken = datosToken;
	}

    @JsonProperty("HttpHeader")
	public HttpHeadersV2 getHttpHeader() {
		return httpHeader;
	}

	public void setHttpHeader(HttpHeadersV2 httpHeader) {
		this.httpHeader = httpHeader;
	}

	@JsonProperty("informacionEntrega")
	public InformacionEntrega getInformacionEntrega() {
		return informacionEntrega;
	}

	public void setInformacionEntrega(InformacionEntrega informacionEntrega) {
		this.informacionEntrega = informacionEntrega;
	}

	@JsonProperty("grabarLPD")
	public GrabarLPD getGrabarLPD() {
		return grabarLPD;
	}

	public void setGrabarLPD(GrabarLPD grabarLPD) {
		this.grabarLPD = grabarLPD;
	}

	@JsonProperty("datosCliente")
	public DatosCliente getDatosCliente() {
		return datosCliente;
	}

	public void setDatosCliente(DatosCliente datosCliente) {
		this.datosCliente = datosCliente;
	}

	@JsonProperty("datosExpediente")
	public DatosExpediente getDatosExpediente() {
		return datosExpediente;
	}

	public void setDatosExpediente(DatosExpediente datosExpediente) {
		this.datosExpediente = datosExpediente;
	}

	@JsonProperty("datosCampania")
	public DatosCampania getDatosCampania() {
		return datosCampania;
	}

	public void setDatosCampania(DatosCampania datosCampania) {
		this.datosCampania = datosCampania;
	}

	@JsonProperty("altaTCProvisional")
	public AltaTCProvisional getAltaTCProvisional() {
		return altaTCProvisional;
	}

	public void setAltaTCProvisional(AltaTCProvisional altaTCProvisional) {
		this.altaTCProvisional = altaTCProvisional;
	}

	@JsonProperty("datosConyuge")
	public DatosConyuge getDatosConyuge() {
		return datosConyuge;
	}

	public void setDatosConyuge(DatosConyuge datosConyuge) {
		this.datosConyuge = datosConyuge;
	}

	@JsonProperty("datosMonitor")
	public DatosMonitor getDatosMonitor() {
		return datosMonitor;
	}

	public void setDatosMonitor(DatosMonitor datosMonitor) {
		this.datosMonitor = datosMonitor;
	}

	@JsonProperty("datosToken")
	public DatosToken getDatosToken() {
		return datosToken;
	}

	public void setDatosToken(DatosToken datosToken) {
		this.datosToken = datosToken;
	}

}
