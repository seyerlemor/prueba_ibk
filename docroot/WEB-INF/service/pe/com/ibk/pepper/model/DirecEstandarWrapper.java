/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DirecEstandar}.
 * </p>
 *
 * @author Interbank
 * @see DirecEstandar
 * @generated
 */
public class DirecEstandarWrapper implements DirecEstandar,
	ModelWrapper<DirecEstandar> {
	public DirecEstandarWrapper(DirecEstandar direcEstandar) {
		_direcEstandar = direcEstandar;
	}

	@Override
	public Class<?> getModelClass() {
		return DirecEstandar.class;
	}

	@Override
	public String getModelClassName() {
		return DirecEstandar.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDetalleDireccion", getIdDetalleDireccion());
		attributes.put("idDireccion", getIdDireccion());
		attributes.put("tipoVia", getTipoVia());
		attributes.put("nombreVia", getNombreVia());
		attributes.put("numero", getNumero());
		attributes.put("manzana", getManzana());
		attributes.put("pisoLote", getPisoLote());
		attributes.put("interior", getInterior());
		attributes.put("urbanizacion", getUrbanizacion());
		attributes.put("referencia", getReferencia());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idDetalleDireccion = (Long)attributes.get("idDetalleDireccion");

		if (idDetalleDireccion != null) {
			setIdDetalleDireccion(idDetalleDireccion);
		}

		Long idDireccion = (Long)attributes.get("idDireccion");

		if (idDireccion != null) {
			setIdDireccion(idDireccion);
		}

		String tipoVia = (String)attributes.get("tipoVia");

		if (tipoVia != null) {
			setTipoVia(tipoVia);
		}

		String nombreVia = (String)attributes.get("nombreVia");

		if (nombreVia != null) {
			setNombreVia(nombreVia);
		}

		String numero = (String)attributes.get("numero");

		if (numero != null) {
			setNumero(numero);
		}

		String manzana = (String)attributes.get("manzana");

		if (manzana != null) {
			setManzana(manzana);
		}

		String pisoLote = (String)attributes.get("pisoLote");

		if (pisoLote != null) {
			setPisoLote(pisoLote);
		}

		String interior = (String)attributes.get("interior");

		if (interior != null) {
			setInterior(interior);
		}

		String urbanizacion = (String)attributes.get("urbanizacion");

		if (urbanizacion != null) {
			setUrbanizacion(urbanizacion);
		}

		String referencia = (String)attributes.get("referencia");

		if (referencia != null) {
			setReferencia(referencia);
		}
	}

	/**
	* Returns the primary key of this direc estandar.
	*
	* @return the primary key of this direc estandar
	*/
	@Override
	public long getPrimaryKey() {
		return _direcEstandar.getPrimaryKey();
	}

	/**
	* Sets the primary key of this direc estandar.
	*
	* @param primaryKey the primary key of this direc estandar
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_direcEstandar.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id detalle direccion of this direc estandar.
	*
	* @return the id detalle direccion of this direc estandar
	*/
	@Override
	public long getIdDetalleDireccion() {
		return _direcEstandar.getIdDetalleDireccion();
	}

	/**
	* Sets the id detalle direccion of this direc estandar.
	*
	* @param idDetalleDireccion the id detalle direccion of this direc estandar
	*/
	@Override
	public void setIdDetalleDireccion(long idDetalleDireccion) {
		_direcEstandar.setIdDetalleDireccion(idDetalleDireccion);
	}

	/**
	* Returns the id direccion of this direc estandar.
	*
	* @return the id direccion of this direc estandar
	*/
	@Override
	public long getIdDireccion() {
		return _direcEstandar.getIdDireccion();
	}

	/**
	* Sets the id direccion of this direc estandar.
	*
	* @param idDireccion the id direccion of this direc estandar
	*/
	@Override
	public void setIdDireccion(long idDireccion) {
		_direcEstandar.setIdDireccion(idDireccion);
	}

	/**
	* Returns the tipo via of this direc estandar.
	*
	* @return the tipo via of this direc estandar
	*/
	@Override
	public java.lang.String getTipoVia() {
		return _direcEstandar.getTipoVia();
	}

	/**
	* Sets the tipo via of this direc estandar.
	*
	* @param tipoVia the tipo via of this direc estandar
	*/
	@Override
	public void setTipoVia(java.lang.String tipoVia) {
		_direcEstandar.setTipoVia(tipoVia);
	}

	/**
	* Returns the nombre via of this direc estandar.
	*
	* @return the nombre via of this direc estandar
	*/
	@Override
	public java.lang.String getNombreVia() {
		return _direcEstandar.getNombreVia();
	}

	/**
	* Sets the nombre via of this direc estandar.
	*
	* @param nombreVia the nombre via of this direc estandar
	*/
	@Override
	public void setNombreVia(java.lang.String nombreVia) {
		_direcEstandar.setNombreVia(nombreVia);
	}

	/**
	* Returns the numero of this direc estandar.
	*
	* @return the numero of this direc estandar
	*/
	@Override
	public java.lang.String getNumero() {
		return _direcEstandar.getNumero();
	}

	/**
	* Sets the numero of this direc estandar.
	*
	* @param numero the numero of this direc estandar
	*/
	@Override
	public void setNumero(java.lang.String numero) {
		_direcEstandar.setNumero(numero);
	}

	/**
	* Returns the manzana of this direc estandar.
	*
	* @return the manzana of this direc estandar
	*/
	@Override
	public java.lang.String getManzana() {
		return _direcEstandar.getManzana();
	}

	/**
	* Sets the manzana of this direc estandar.
	*
	* @param manzana the manzana of this direc estandar
	*/
	@Override
	public void setManzana(java.lang.String manzana) {
		_direcEstandar.setManzana(manzana);
	}

	/**
	* Returns the piso lote of this direc estandar.
	*
	* @return the piso lote of this direc estandar
	*/
	@Override
	public java.lang.String getPisoLote() {
		return _direcEstandar.getPisoLote();
	}

	/**
	* Sets the piso lote of this direc estandar.
	*
	* @param pisoLote the piso lote of this direc estandar
	*/
	@Override
	public void setPisoLote(java.lang.String pisoLote) {
		_direcEstandar.setPisoLote(pisoLote);
	}

	/**
	* Returns the interior of this direc estandar.
	*
	* @return the interior of this direc estandar
	*/
	@Override
	public java.lang.String getInterior() {
		return _direcEstandar.getInterior();
	}

	/**
	* Sets the interior of this direc estandar.
	*
	* @param interior the interior of this direc estandar
	*/
	@Override
	public void setInterior(java.lang.String interior) {
		_direcEstandar.setInterior(interior);
	}

	/**
	* Returns the urbanizacion of this direc estandar.
	*
	* @return the urbanizacion of this direc estandar
	*/
	@Override
	public java.lang.String getUrbanizacion() {
		return _direcEstandar.getUrbanizacion();
	}

	/**
	* Sets the urbanizacion of this direc estandar.
	*
	* @param urbanizacion the urbanizacion of this direc estandar
	*/
	@Override
	public void setUrbanizacion(java.lang.String urbanizacion) {
		_direcEstandar.setUrbanizacion(urbanizacion);
	}

	/**
	* Returns the referencia of this direc estandar.
	*
	* @return the referencia of this direc estandar
	*/
	@Override
	public java.lang.String getReferencia() {
		return _direcEstandar.getReferencia();
	}

	/**
	* Sets the referencia of this direc estandar.
	*
	* @param referencia the referencia of this direc estandar
	*/
	@Override
	public void setReferencia(java.lang.String referencia) {
		_direcEstandar.setReferencia(referencia);
	}

	@Override
	public boolean isNew() {
		return _direcEstandar.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_direcEstandar.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _direcEstandar.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_direcEstandar.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _direcEstandar.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _direcEstandar.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_direcEstandar.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _direcEstandar.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_direcEstandar.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_direcEstandar.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_direcEstandar.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DirecEstandarWrapper((DirecEstandar)_direcEstandar.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.DirecEstandar direcEstandar) {
		return _direcEstandar.compareTo(direcEstandar);
	}

	@Override
	public int hashCode() {
		return _direcEstandar.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.DirecEstandar> toCacheModel() {
		return _direcEstandar.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.DirecEstandar toEscapedModel() {
		return new DirecEstandarWrapper(_direcEstandar.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.DirecEstandar toUnescapedModel() {
		return new DirecEstandarWrapper(_direcEstandar.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _direcEstandar.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _direcEstandar.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_direcEstandar.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DirecEstandarWrapper)) {
			return false;
		}

		DirecEstandarWrapper direcEstandarWrapper = (DirecEstandarWrapper)obj;

		if (Validator.equals(_direcEstandar, direcEstandarWrapper._direcEstandar)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public DirecEstandar getWrappedDirecEstandar() {
		return _direcEstandar;
	}

	@Override
	public DirecEstandar getWrappedModel() {
		return _direcEstandar;
	}

	@Override
	public void resetOriginalValues() {
		_direcEstandar.resetOriginalValues();
	}

	private DirecEstandar _direcEstandar;
}