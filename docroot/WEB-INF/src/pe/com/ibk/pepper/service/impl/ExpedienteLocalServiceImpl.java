/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;
import java.util.List;

import pe.com.ibk.pepper.NoSuchExpedienteException;
import pe.com.ibk.pepper.model.Expediente;
import pe.com.ibk.pepper.service.base.ExpedienteLocalServiceBaseImpl;
import pe.com.ibk.pepper.util.DatesUtil;

/**
 * The implementation of the expediente local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.ExpedienteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.ExpedienteLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.ExpedienteLocalServiceUtil
 */
public class ExpedienteLocalServiceImpl extends ExpedienteLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.ExpedienteLocalServiceUtil} to access the expediente local service.
	 */
	
	private static final Log _log = LogFactoryUtil.getLog(ExpedienteLocalServiceImpl.class);
	
	@Override
	public Expediente registrarExpediente(Expediente expediente) throws SystemException{
		
		Date now = DateUtil.newDate();
		if (expediente.isNew()) {
			expediente.setFechaRegistro(now);
		}
		expediente.setPeriodoRegistro(DatesUtil.obtenerPeriodoActual());
		expedientePersistence.update(expediente);
		
		return expediente;
	}
	
	@Override
	public Expediente buscarExpedienteporId(long idExpediente){
		Expediente expediente = null;
		try {
			expediente = expedientePersistence.findByE_ID(idExpediente);
		} catch (Exception e) {
			_log.error("Error en clase ExpedienteLocalServiceImpl metodo buscarExpedienteporId:: NO EXISTE EN EXPEDIENTE", e);
		}
		return expediente;
	}

	//sin uso
	@Override
	public Expediente buscarPorDNI_Fecha(String dni, Date fecha){
		Expediente expediente = null;
		try {
			expediente = expedientePersistence.findByE_D_F(dni, fecha);
		} catch (NoSuchExpedienteException e) {
			_log.error("Error NoSuchExpedienteException en clase ExpedienteLocalServiceImpl metodo buscarPorDNI_Fecha::", e );
		} catch (SystemException e) {
			_log.error("Error SystemException en clase ExpedienteLocalServiceImpl metodo buscarPorDNI_Fecha::", e );
		};
		return expediente;
	}
	//sin uso
	@Override
	public Expediente buscarPorDNI_Periodo(String dni, String periodo){
		Expediente expediente = null;
		try {
			expediente = expedientePersistence.findByE_D_P(dni, periodo);
		} catch (NoSuchExpedienteException e) {
			_log.error("Error en clase ExpedienteLocalServiceImpl metodo buscarPorDNI_Periodo:: NO EXISTE EL EXPEDIENTE", e );
		} catch (SystemException e) {
			_log.error("Error en clase ExpedienteLocalServiceImpl metodo buscarPorDNI_Periodo::", e );
		};
		return expediente;
	}
	
	@Override
	public Expediente obtenerExpedienteDNIFecha(String documento, String periodo){ 
		try {
		return expedienteFinder.obtenerExpedienteDNIFecha(documento, periodo);
		}catch(Exception e){
			_log.error(e);
			return null;
		}
	}

	@Override
	public List<Object[]> obtenerBusqueda(Integer filas , Integer pagina, String fechaInicio, String fechaFinal, String estado){
		return expedienteFinder.obtenerBusqueda(filas, pagina, fechaInicio, fechaFinal, estado);
	}
}