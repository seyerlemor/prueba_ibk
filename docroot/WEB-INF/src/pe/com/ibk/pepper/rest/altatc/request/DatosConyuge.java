
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DatosConyuge {

	private String tipoDocumento;
    private String numeroDocumento;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DatosConyuge() {
    }

	public DatosConyuge(String tipoDocumento, String numeroDocumento) {
		super();
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@Override
	public String toString() {
		return "DatosConyuge [tipoDocumento=" + tipoDocumento
				+ ", numeroDocumento=" + numeroDocumento + "]";
	}

}
