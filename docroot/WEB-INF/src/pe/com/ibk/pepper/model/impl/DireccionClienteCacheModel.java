/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.DireccionCliente;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing DireccionCliente in entity cache.
 *
 * @author Interbank
 * @see DireccionCliente
 * @generated
 */
public class DireccionClienteCacheModel implements CacheModel<DireccionCliente>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{idDireccion=");
		sb.append(idDireccion);
		sb.append(", idDatoCliente=");
		sb.append(idDatoCliente);
		sb.append(", tipoDireccion=");
		sb.append(tipoDireccion);
		sb.append(", codigoUso=");
		sb.append(codigoUso);
		sb.append(", flagDireccionEstandar=");
		sb.append(flagDireccionEstandar);
		sb.append(", via=");
		sb.append(via);
		sb.append(", departamento=");
		sb.append(departamento);
		sb.append(", provincia=");
		sb.append(provincia);
		sb.append(", distrito=");
		sb.append(distrito);
		sb.append(", pais=");
		sb.append(pais);
		sb.append(", ubigeo=");
		sb.append(ubigeo);
		sb.append(", codigoPostal=");
		sb.append(codigoPostal);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DireccionCliente toEntityModel() {
		DireccionClienteImpl direccionClienteImpl = new DireccionClienteImpl();

		direccionClienteImpl.setIdDireccion(idDireccion);
		direccionClienteImpl.setIdDatoCliente(idDatoCliente);

		if (tipoDireccion == null) {
			direccionClienteImpl.setTipoDireccion(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setTipoDireccion(tipoDireccion);
		}

		if (codigoUso == null) {
			direccionClienteImpl.setCodigoUso(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setCodigoUso(codigoUso);
		}

		if (flagDireccionEstandar == null) {
			direccionClienteImpl.setFlagDireccionEstandar(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setFlagDireccionEstandar(flagDireccionEstandar);
		}

		if (via == null) {
			direccionClienteImpl.setVia(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setVia(via);
		}

		if (departamento == null) {
			direccionClienteImpl.setDepartamento(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setDepartamento(departamento);
		}

		if (provincia == null) {
			direccionClienteImpl.setProvincia(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setProvincia(provincia);
		}

		if (distrito == null) {
			direccionClienteImpl.setDistrito(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setDistrito(distrito);
		}

		if (pais == null) {
			direccionClienteImpl.setPais(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setPais(pais);
		}

		if (ubigeo == null) {
			direccionClienteImpl.setUbigeo(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setUbigeo(ubigeo);
		}

		if (codigoPostal == null) {
			direccionClienteImpl.setCodigoPostal(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setCodigoPostal(codigoPostal);
		}

		if (estado == null) {
			direccionClienteImpl.setEstado(StringPool.BLANK);
		}
		else {
			direccionClienteImpl.setEstado(estado);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			direccionClienteImpl.setFechaRegistro(null);
		}
		else {
			direccionClienteImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		direccionClienteImpl.resetOriginalValues();

		return direccionClienteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idDireccion = objectInput.readLong();
		idDatoCliente = objectInput.readLong();
		tipoDireccion = objectInput.readUTF();
		codigoUso = objectInput.readUTF();
		flagDireccionEstandar = objectInput.readUTF();
		via = objectInput.readUTF();
		departamento = objectInput.readUTF();
		provincia = objectInput.readUTF();
		distrito = objectInput.readUTF();
		pais = objectInput.readUTF();
		ubigeo = objectInput.readUTF();
		codigoPostal = objectInput.readUTF();
		estado = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idDireccion);
		objectOutput.writeLong(idDatoCliente);

		if (tipoDireccion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoDireccion);
		}

		if (codigoUso == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoUso);
		}

		if (flagDireccionEstandar == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(flagDireccionEstandar);
		}

		if (via == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(via);
		}

		if (departamento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(departamento);
		}

		if (provincia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(provincia);
		}

		if (distrito == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(distrito);
		}

		if (pais == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pais);
		}

		if (ubigeo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ubigeo);
		}

		if (codigoPostal == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoPostal);
		}

		if (estado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estado);
		}

		objectOutput.writeLong(fechaRegistro);
	}

	public long idDireccion;
	public long idDatoCliente;
	public String tipoDireccion;
	public String codigoUso;
	public String flagDireccionEstandar;
	public String via;
	public String departamento;
	public String provincia;
	public String distrito;
	public String pais;
	public String ubigeo;
	public String codigoPostal;
	public String estado;
	public long fechaRegistro;
}