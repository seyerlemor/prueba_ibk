
package pe.com.ibk.pepper.rest.campania.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Campaign implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("objective")
    @Expose
    private String objective;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    private final static long serialVersionUID = -1946868513857814744L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Campaign() {
    }

    /**
     * 
     * @param number
     * @param endDate
     * @param name
     * @param id
     * @param priority
     * @param startDate
     * @param objective
     * @param products
     */
    public Campaign(String id, String number, String name, String startDate, String endDate, String objective, String priority, List<Product> products) {
        super();
        this.id = id;
        this.number = number;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.objective = objective;
        this.priority = priority;
        this.products = products;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Campaign withId(String id) {
        this.id = id;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Campaign withNumber(String number) {
        this.number = number;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Campaign withName(String name) {
        this.name = name;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Campaign withStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Campaign withEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public Campaign withObjective(String objective) {
        this.objective = objective;
        return this;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Campaign withPriority(String priority) {
        this.priority = priority;
        return this;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Campaign withProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("number", number).append("name", name).append("startDate", startDate).append("endDate", endDate).append("objective", objective).append("priority", priority).append("products", products).toString();
    }

}
