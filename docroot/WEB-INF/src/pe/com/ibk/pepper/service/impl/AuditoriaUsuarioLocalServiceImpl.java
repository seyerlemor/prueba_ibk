/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;

import pe.com.ibk.pepper.NoSuchAuditoriaUsuarioException;
import pe.com.ibk.pepper.model.AuditoriaUsuario;
import pe.com.ibk.pepper.service.base.AuditoriaUsuarioLocalServiceBaseImpl;

/**
 * The implementation of the auditoria usuario local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.AuditoriaUsuarioLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.AuditoriaUsuarioLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.AuditoriaUsuarioLocalServiceUtil
 */
public class AuditoriaUsuarioLocalServiceImpl
	extends AuditoriaUsuarioLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.AuditoriaUsuarioLocalServiceUtil} to access the auditoria usuario local service.
	 */
	
	private static final Log _log = LogFactoryUtil.getLog(AuditoriaUsuarioLocalServiceImpl.class);
	
	@Override
	public AuditoriaUsuario registrarAuditoriaUsuario(AuditoriaUsuario auditoriaUsuario) throws SystemException{
		Date now = DateUtil.newDate();
		if (auditoriaUsuario.isNew()) {
			auditoriaUsuario.setFechaRegistro(now);
		}
		auditoriaUsuarioPersistence.update(auditoriaUsuario);
		return auditoriaUsuario;
	}	
	
	@Override
	public AuditoriaUsuario buscarAuditoriaUsuarioporId(long idAuditoriaUsuario){
		AuditoriaUsuario auditoriaUsuario = null;
		try {
			auditoriaUsuario = auditoriaUsuarioPersistence.findByAU_ID(idAuditoriaUsuario);
		} catch (NoSuchAuditoriaUsuarioException e) {
			_log.error("Error en clase AuditoriaUsuarioLocalServiceImpl metodo buscarAuditoriaUsuarioporId:: NO EXISTE EN AUDITORIAUSUARIO ", e );
		} catch (SystemException e) {
			_log.error("Error en clase AuditoriaUsuarioLocalServiceImpl metodo buscarAuditoriaUsuarioporId:: ", e );
		}
		return auditoriaUsuario;
	}		
	
	
}