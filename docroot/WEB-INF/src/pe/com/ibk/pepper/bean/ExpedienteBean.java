package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class ExpedienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idExpediente;
	private String dniNumber;
	private String estado;
	private String tipoFlujo;
	private String paso;
	
	private Boolean flagR1;
	private Boolean flagCampania;
	private Boolean flagReniec;
	private Boolean flagConsultarEquifax;
	private Boolean flagValidarEquifax;
	private Boolean flagObtenerInformacion;
	private Boolean flagGeneracionToken;
	private Boolean flagVentaTC;
	private Boolean flagValidacionToken;
	private Boolean flagRecompraGenPOTC;
	private Boolean flagRecompraConPOTC;
	private Boolean flagExtornoGenPOTC;
	private Boolean flagExtornoConPOTC;
	private Boolean flagErrorVentaTC;
	private Boolean flagObtenerTarjeta;
	private Boolean flagLogicaReingreso;
	private Boolean flagCalificacion;
	private Boolean flagCliente;
	private String fechaRegistro;
	private String tipoCampania;
	private int idUsuarioSession;
	private String strJson;
	private String htmlCode;
	private String expedienteCDA;
	private String codigoVendedor;
	private String ip;
	private String navegador;
	private String dispositivo;
	private String screenName;
	private String messageId;
	private String codigoServicioError;
	private Boolean flagConsultarFechaHora;
	private String codigoLogicaReingreso;
	private String pepperType;
	private String commerceId;
	private ComercioBean comercioBean;
	private Boolean flagAntifraude;
	private Boolean flagAfiliacionBancaSMS;
	private String tipoAutenticacion;
	
	public String getExpedienteCDA() {
		return expedienteCDA;
	}
	public void setExpedienteCDA(String expedienteCDA) {
		this.expedienteCDA = expedienteCDA;
	}
	public String getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}
	public Boolean getFlagR1() {
		return flagR1;
	}
	public void setFlagR1(Boolean flagR1) {
		this.flagR1 = flagR1;
	}
	public Boolean getFlagCampania() {
		return flagCampania;
	}
	public void setFlagCampania(Boolean flagCampania) {
		this.flagCampania = flagCampania;
	}
	public Boolean getFlagReniec() {
		return flagReniec;
	}
	public void setFlagReniec(Boolean flagReniec) {
		this.flagReniec = flagReniec;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getTipoCampania() {
		return tipoCampania;
	}
	public void setTipoCampania(String tipoCampania) {
		this.tipoCampania = tipoCampania;
	}
	public Boolean getFlagCalificacion() {
		return flagCalificacion;
	}
	public void setFlagCalificacion(Boolean flagCalificacion) {
		this.flagCalificacion = flagCalificacion;
	}

	public int getIdUsuarioSession() {
		return idUsuarioSession;
	}
	public void setIdUsuarioSession(int idUsuarioSession) {
		this.idUsuarioSession = idUsuarioSession;
	}
	public String getStrJson() {
		return strJson;
	}
	public void setStrJson(String strJson) {
		this.strJson = strJson;
	}
	public long getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(long idExpediente) {
		this.idExpediente = idExpediente;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoFlujo() {
		return tipoFlujo;
	}
	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}
	public String getPaso() {
		return paso;
	}
	public void setPaso(String paso) {
		this.paso = paso;
	}
	
	public String getCodigoVendedor() {
		return codigoVendedor;
	}
	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getNavegador() {
		return navegador;
	}
	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}
	public Boolean getFlagCliente() {
		return flagCliente;
	}
	public void setFlagCliente(Boolean flagCliente) {
		this.flagCliente = flagCliente;
	}
	public Boolean getFlagObtenerInformacion() {
		return flagObtenerInformacion;
	}
	public void setFlagObtenerInformacion(Boolean flagObtenerInformacion) {
		this.flagObtenerInformacion = flagObtenerInformacion;
	}
	public Boolean getFlagGeneracionToken() {
		return flagGeneracionToken;
	}
	public void setFlagGeneracionToken(Boolean flagGeneracionToken) {
		this.flagGeneracionToken = flagGeneracionToken;
	}
	public Boolean getFlagVentaTC() {
		return flagVentaTC;
	}
	public void setFlagVentaTC(Boolean flagVentaTC) {
		this.flagVentaTC = flagVentaTC;
	}
	public Boolean getFlagValidacionToken() {
		return flagValidacionToken;
	}
	public void setFlagValidacionToken(Boolean flagValidacionToken) {
		this.flagValidacionToken = flagValidacionToken;
	}
	public Boolean getFlagRecompraGenPOTC() {
		return flagRecompraGenPOTC;
	}
	public void setFlagRecompraGenPOTC(Boolean flagRecompraGenPOTC) {
		this.flagRecompraGenPOTC = flagRecompraGenPOTC;
	}
	public Boolean getFlagRecompraConPOTC() {
		return flagRecompraConPOTC;
	}
	public void setFlagRecompraConPOTC(Boolean flagRecompraConPOTC) {
		this.flagRecompraConPOTC = flagRecompraConPOTC;
	}
	public Boolean getFlagExtornoGenPOTC() {
		return flagExtornoGenPOTC;
	}
	public void setFlagExtornoGenPOTC(Boolean flagExtornoGenPOTC) {
		this.flagExtornoGenPOTC = flagExtornoGenPOTC;
	}
	public Boolean getFlagExtornoConPOTC() {
		return flagExtornoConPOTC;
	}
	public void setFlagExtornoConPOTC(Boolean flagExtornoConPOTC) {
		this.flagExtornoConPOTC = flagExtornoConPOTC;
	}
	
	public Boolean getFlagErrorVentaTC() {
		return flagErrorVentaTC;
	}
	public void setFlagErrorVentaTC(Boolean flagErrorVentaTC) {
		this.flagErrorVentaTC = flagErrorVentaTC;
	}
	public Boolean getFlagObtenerTarjeta() {
		return flagObtenerTarjeta;
	}
	public void setFlagObtenerTarjeta(Boolean flagObtenerTarjeta) {
		this.flagObtenerTarjeta = flagObtenerTarjeta;
	}
	public Boolean getFlagLogicaReingreso() {
		return flagLogicaReingreso;
	}
	public void setFlagLogicaReingreso(Boolean flagLogicaReingreso) {
		this.flagLogicaReingreso = flagLogicaReingreso;
	}
	public String getCodigoLogicaReingreso() {
		return codigoLogicaReingreso;
	}
	public void setCodigoLogicaReingreso(String codigoLogicaReingreso) {
		this.codigoLogicaReingreso = codigoLogicaReingreso;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getDniNumber() {
		return dniNumber;
	}
	public void setDniNumber(String dniNumber) {
		this.dniNumber = dniNumber;
	}
	public String getHtmlCode() {
		return htmlCode;
	}
	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}
	public String getCodigoServicioError() {
		return codigoServicioError;
	}
	public void setCodigoServicioError(String codigoServicioError) {
		this.codigoServicioError = codigoServicioError;
	}
	public Boolean getFlagConsultarFechaHora() {
		return flagConsultarFechaHora;
	}
	public void setFlagConsultarFechaHora(Boolean flagConsultarFechaHora) {
		this.flagConsultarFechaHora = flagConsultarFechaHora;
	}
	public Boolean getFlagConsultarEquifax() {
		return flagConsultarEquifax;
	}
	public void setFlagConsultarEquifax(Boolean flagConsultarEquifax) {
		this.flagConsultarEquifax = flagConsultarEquifax;
	}
	public Boolean getFlagValidarEquifax() {
		return flagValidarEquifax;
	}
	public void setFlagValidarEquifax(Boolean flagValidarEquifax) {
		this.flagValidarEquifax = flagValidarEquifax;
	}
	public String getPepperType() {
		return pepperType;
	}
	public void setPepperType(String pepperType) {
		this.pepperType = pepperType;
	}
	public String getCommerceId() {
		return commerceId;
	}
	public void setCommerceId(String commerceId) {
		this.commerceId = commerceId;
	}
	
	public ComercioBean getComercioBean() {
		return comercioBean;
	}
	public void setComercioBean(ComercioBean comercioBean) {
		this.comercioBean = comercioBean;
	}
	public Boolean getFlagAntifraude() {
		return flagAntifraude;
	}
	public void setFlagAntifraude(Boolean flagAntifraude) {
		this.flagAntifraude = flagAntifraude;
	}
	public Boolean getFlagAfiliacionBancaSMS() {
		return flagAfiliacionBancaSMS;
	}
	public void setFlagAfiliacionBancaSMS(Boolean flagAfiliacionBancaSMS) {
		this.flagAfiliacionBancaSMS = flagAfiliacionBancaSMS;
	}
	
	public String getTipoAutenticacion() {
		return tipoAutenticacion;
	}
	public void setTipoAutenticacion(String tipoAutenticacion) {
		this.tipoAutenticacion = tipoAutenticacion;
	}
	@Override
	public String toString() {
		return "ExpedienteBean [idExpediente=" + idExpediente + ", dniNumber="
				+ dniNumber + ", estado=" + estado + ", tipoFlujo=" + tipoFlujo
				+ ", paso=" + paso + ", flagR1=" + flagR1 + ", flagCampania="
				+ flagCampania + ", flagReniec=" + flagReniec
				+ ", flagConsultarEquifax=" + flagConsultarEquifax
				+ ", flagValidarEquifax=" + flagValidarEquifax
				+ ", flagObtenerInformacion=" + flagObtenerInformacion
				+ ", flagGeneracionToken=" + flagGeneracionToken
				+ ", flagVentaTC=" + flagVentaTC + ", flagValidacionToken="
				+ flagValidacionToken + ", flagRecompraGenPOTC="
				+ flagRecompraGenPOTC + ", flagRecompraConPOTC="
				+ flagRecompraConPOTC + ", flagExtornoGenPOTC="
				+ flagExtornoGenPOTC + ", flagExtornoConPOTC="
				+ flagExtornoConPOTC + ", flagErrorVentaTC=" + flagErrorVentaTC
				+ ", flagObtenerTarjeta=" + flagObtenerTarjeta
				+ ", flagLogicaReingreso=" + flagLogicaReingreso
				+ ", flagCalificacion=" + flagCalificacion + ", flagCliente="
				+ flagCliente + ", fechaRegistro=" + fechaRegistro
				+ ", tipoCampania=" + tipoCampania + ", idUsuarioSession="
				+ idUsuarioSession + ", strJson=" + strJson + ", htmlCode="
				+ htmlCode + ", expedienteCDA=" + expedienteCDA
				+ ", codigoVendedor=" + codigoVendedor + ", ip=" + ip
				+ ", navegador=" + navegador + ", dispositivo=" + dispositivo
				+ ", screenName=" + screenName + ", messageId=" + messageId
				+ ", codigoServicioError=" + codigoServicioError
				+ ", flagConsultarFechaHora=" + flagConsultarFechaHora
				+ ", codigoLogicaReingreso=" + codigoLogicaReingreso
				+ ", pepperType=" + pepperType + ", commerceId=" + commerceId
				+ ", comercioBean=" + comercioBean + ", flagAntifraude="
				+ flagAntifraude + ", flagAfiliacionBancaSMS="
				+ flagAfiliacionBancaSMS + ", tipoAutenticacion="
				+ tipoAutenticacion + "]";
	}
	
}
