
package pe.com.ibk.pepper.rest.campania.response;


public class Adicionales {

    private String campoAdicional1;
    private String campoAdicional2;
    private String campoAdicional3;
    private String campoAdicional4;
    private String campoAdicional7;
    private String campoAdicional8;
    private String campoAdicional9;
    private String campoAdicional10;
    private String descripcionMarcaTarjeta;
    private String descripcionTipoTarjeta;
    
	public String getCampoAdicional1() {
		return campoAdicional1;
	}
	public void setCampoAdicional1(String campoAdicional1) {
		this.campoAdicional1 = campoAdicional1;
	}
	public String getCampoAdicional2() {
		return campoAdicional2;
	}
	public void setCampoAdicional2(String campoAdicional2) {
		this.campoAdicional2 = campoAdicional2;
	}
	public String getCampoAdicional3() {
		return campoAdicional3;
	}
	public void setCampoAdicional3(String campoAdicional3) {
		this.campoAdicional3 = campoAdicional3;
	}
	public String getCampoAdicional4() {
		return campoAdicional4;
	}
	public void setCampoAdicional4(String campoAdicional4) {
		this.campoAdicional4 = campoAdicional4;
	}
	public String getCampoAdicional7() {
		return campoAdicional7;
	}
	public void setCampoAdicional7(String campoAdicional7) {
		this.campoAdicional7 = campoAdicional7;
	}
	public String getCampoAdicional8() {
		return campoAdicional8;
	}
	public void setCampoAdicional8(String campoAdicional8) {
		this.campoAdicional8 = campoAdicional8;
	}
	public String getCampoAdicional9() {
		return campoAdicional9;
	}
	public void setCampoAdicional9(String campoAdicional9) {
		this.campoAdicional9 = campoAdicional9;
	}
	public String getCampoAdicional10() {
		return campoAdicional10;
	}
	public void setCampoAdicional10(String campoAdicional10) {
		this.campoAdicional10 = campoAdicional10;
	}
	public String getDescripcionMarcaTarjeta() {
		return descripcionMarcaTarjeta;
	}
	public void setDescripcionMarcaTarjeta(String descripcionMarcaTarjeta) {
		this.descripcionMarcaTarjeta = descripcionMarcaTarjeta;
	}
	public String getDescripcionTipoTarjeta() {
		return descripcionTipoTarjeta;
	}
	public void setDescripcionTipoTarjeta(String descripcionTipoTarjeta) {
		this.descripcionTipoTarjeta = descripcionTipoTarjeta;
	}

}
