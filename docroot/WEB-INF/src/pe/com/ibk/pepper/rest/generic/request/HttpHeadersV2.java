package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HttpHeadersV2 {
	private String netId;
	private String consumerId;
	private String messageId;
	private String supervisorId;
	private String userId;
	private String serverId;
	private String deviceId;
	private String branchCode;
	private String referenceNumber;
	private String countryCode;
	private String groupMember;
	private String ibmClientId;
	private String moduleId;
	private String serviceId;
	private String channelCode;
	private String timestamp;
	
	@JsonCreator
	public HttpHeadersV2(
			String netId, String consumerId, String messageId,
			String supervisorId, String userId, String serverId,
			String deviceId, String branchCode, String referenceNumber,
			String countryCode, String groupMember, String ibmClientId,
			String moduleId, String serviceId, String channelCode,
			String timestamp) {
		super();
		this.netId = netId;
		this.consumerId = consumerId;
		this.messageId = messageId;
		this.supervisorId = supervisorId;
		this.userId = userId;
		this.serverId = serverId;
		this.deviceId = deviceId;
		this.branchCode = branchCode;
		this.referenceNumber = referenceNumber;
		this.countryCode = countryCode;
		this.groupMember = groupMember;
		this.ibmClientId = ibmClientId;
		this.moduleId = moduleId;
		this.serviceId = serviceId;
		this.channelCode = channelCode;
		this.timestamp = timestamp;
	}
	
	@JsonCreator
	public HttpHeadersV2(
			String netId, String consumerId, String messageId,
			String supervisorId, String userId, String serverId,
			String deviceId, String branchCode,
			String countryCode, String groupMember,
			String moduleId, String serviceId, String channelCode) {
		super();
		this.netId = netId;
		this.consumerId = consumerId;
		this.messageId = messageId;
		this.supervisorId = supervisorId;
		this.userId = userId;
		this.serverId = serverId;
		this.deviceId = deviceId;
		this.branchCode = branchCode;
		this.countryCode = countryCode;
		this.groupMember = groupMember;
		this.moduleId = moduleId;
		this.serviceId = serviceId;
		this.channelCode = channelCode;
	}
	
	@JsonProperty("netId")
	public String getNetId() {
		return netId;
	}
	public void setNetId(String netId) {
		this.netId = netId;
	}
	@JsonProperty("consumerId")
	public String getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}
	@JsonProperty("messageId")
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	@JsonProperty("supervisorId")
	public String getSupervisorId() {
		return supervisorId;
	}
	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}
	@JsonProperty("userId")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@JsonProperty("serverId")
	public String getServerId() {
		return serverId;
	}
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	@JsonProperty("branchCode")
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	@JsonProperty("referenceNumber")
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	@JsonProperty("countryCode")
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	@JsonProperty("groupMember")
	public String getGroupMember() {
		return groupMember;
	}
	public void setGroupMember(String groupMember) {
		this.groupMember = groupMember;
	}
	@JsonProperty("X-IBM-Client-Id")
	public String getIbmClientId() {
		return ibmClientId;
	}
	public void setIbmClientId(String ibmClientId) {
		this.ibmClientId = ibmClientId;
	}
	@JsonProperty("moduleId")
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	@JsonProperty("serviceId")
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	@JsonProperty("channelCode")
	public String getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	@JsonProperty("timestamp")
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}
