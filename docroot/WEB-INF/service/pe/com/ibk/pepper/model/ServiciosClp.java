/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.ServiciosLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ServiciosClp extends BaseModelImpl<Servicios> implements Servicios {
	public ServiciosClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Servicios.class;
	}

	@Override
	public String getModelClassName() {
		return Servicios.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idServicio;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdServicio(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idServicio;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idServicio", getIdServicio());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("nombre", getNombre());
		attributes.put("codigo", getCodigo());
		attributes.put("descripcion", getDescripcion());
		attributes.put("response", getResponse());
		attributes.put("messageBus", getMessageBus());
		attributes.put("tipo", getTipo());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("respuestaInterna", getRespuestaInterna());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idServicio = (Long)attributes.get("idServicio");

		if (idServicio != null) {
			setIdServicio(idServicio);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Long response = (Long)attributes.get("response");

		if (response != null) {
			setResponse(response);
		}

		String messageBus = (String)attributes.get("messageBus");

		if (messageBus != null) {
			setMessageBus(messageBus);
		}

		String tipo = (String)attributes.get("tipo");

		if (tipo != null) {
			setTipo(tipo);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String respuestaInterna = (String)attributes.get("respuestaInterna");

		if (respuestaInterna != null) {
			setRespuestaInterna(respuestaInterna);
		}
	}

	@Override
	public long getIdServicio() {
		return _idServicio;
	}

	@Override
	public void setIdServicio(long idServicio) {
		_idServicio = idServicio;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setIdServicio", long.class);

				method.invoke(_serviciosRemoteModel, idServicio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setIdExpediente", long.class);

				method.invoke(_serviciosRemoteModel, idExpediente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombre() {
		return _nombre;
	}

	@Override
	public void setNombre(String nombre) {
		_nombre = nombre;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setNombre", String.class);

				method.invoke(_serviciosRemoteModel, nombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigo() {
		return _codigo;
	}

	@Override
	public void setCodigo(String codigo) {
		_codigo = codigo;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigo", String.class);

				method.invoke(_serviciosRemoteModel, codigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescripcion() {
		return _descripcion;
	}

	@Override
	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setDescripcion", String.class);

				method.invoke(_serviciosRemoteModel, descripcion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getResponse() {
		return _response;
	}

	@Override
	public void setResponse(long response) {
		_response = response;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setResponse", long.class);

				method.invoke(_serviciosRemoteModel, response);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMessageBus() {
		return _messageBus;
	}

	@Override
	public void setMessageBus(String messageBus) {
		_messageBus = messageBus;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setMessageBus", String.class);

				method.invoke(_serviciosRemoteModel, messageBus);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipo() {
		return _tipo;
	}

	@Override
	public void setTipo(String tipo) {
		_tipo = tipo;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setTipo", String.class);

				method.invoke(_serviciosRemoteModel, tipo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_serviciosRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRespuestaInterna() {
		return _respuestaInterna;
	}

	@Override
	public void setRespuestaInterna(String respuestaInterna) {
		_respuestaInterna = respuestaInterna;

		if (_serviciosRemoteModel != null) {
			try {
				Class<?> clazz = _serviciosRemoteModel.getClass();

				Method method = clazz.getMethod("setRespuestaInterna",
						String.class);

				method.invoke(_serviciosRemoteModel, respuestaInterna);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getServiciosRemoteModel() {
		return _serviciosRemoteModel;
	}

	public void setServiciosRemoteModel(BaseModel<?> serviciosRemoteModel) {
		_serviciosRemoteModel = serviciosRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _serviciosRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_serviciosRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ServiciosLocalServiceUtil.addServicios(this);
		}
		else {
			ServiciosLocalServiceUtil.updateServicios(this);
		}
	}

	@Override
	public Servicios toEscapedModel() {
		return (Servicios)ProxyUtil.newProxyInstance(Servicios.class.getClassLoader(),
			new Class[] { Servicios.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ServiciosClp clone = new ServiciosClp();

		clone.setIdServicio(getIdServicio());
		clone.setIdExpediente(getIdExpediente());
		clone.setNombre(getNombre());
		clone.setCodigo(getCodigo());
		clone.setDescripcion(getDescripcion());
		clone.setResponse(getResponse());
		clone.setMessageBus(getMessageBus());
		clone.setTipo(getTipo());
		clone.setFechaRegistro(getFechaRegistro());
		clone.setRespuestaInterna(getRespuestaInterna());

		return clone;
	}

	@Override
	public int compareTo(Servicios servicios) {
		long primaryKey = servicios.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ServiciosClp)) {
			return false;
		}

		ServiciosClp servicios = (ServiciosClp)obj;

		long primaryKey = servicios.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{idServicio=");
		sb.append(getIdServicio());
		sb.append(", idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", nombre=");
		sb.append(getNombre());
		sb.append(", codigo=");
		sb.append(getCodigo());
		sb.append(", descripcion=");
		sb.append(getDescripcion());
		sb.append(", response=");
		sb.append(getResponse());
		sb.append(", messageBus=");
		sb.append(getMessageBus());
		sb.append(", tipo=");
		sb.append(getTipo());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append(", respuestaInterna=");
		sb.append(getRespuestaInterna());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Servicios");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idServicio</column-name><column-value><![CDATA[");
		sb.append(getIdServicio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombre</column-name><column-value><![CDATA[");
		sb.append(getNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigo</column-name><column-value><![CDATA[");
		sb.append(getCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripcion</column-name><column-value><![CDATA[");
		sb.append(getDescripcion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>response</column-name><column-value><![CDATA[");
		sb.append(getResponse());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>messageBus</column-name><column-value><![CDATA[");
		sb.append(getMessageBus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipo</column-name><column-value><![CDATA[");
		sb.append(getTipo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>respuestaInterna</column-name><column-value><![CDATA[");
		sb.append(getRespuestaInterna());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idServicio;
	private long _idExpediente;
	private String _nombre;
	private String _codigo;
	private String _descripcion;
	private long _response;
	private String _messageBus;
	private String _tipo;
	private Date _fechaRegistro;
	private String _respuestaInterna;
	private BaseModel<?> _serviciosRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}