/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.ParametroPadrePO;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ParametroPadrePO in entity cache.
 *
 * @author Interbank
 * @see ParametroPadrePO
 * @generated
 */
public class ParametroPadrePOCacheModel implements CacheModel<ParametroPadrePO>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{idParametroPadre=");
		sb.append(idParametroPadre);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", codigoPadre=");
		sb.append(codigoPadre);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", json=");
		sb.append(json);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ParametroPadrePO toEntityModel() {
		ParametroPadrePOImpl parametroPadrePOImpl = new ParametroPadrePOImpl();

		parametroPadrePOImpl.setIdParametroPadre(idParametroPadre);
		parametroPadrePOImpl.setGroupId(groupId);
		parametroPadrePOImpl.setCompanyId(companyId);
		parametroPadrePOImpl.setUserId(userId);

		if (userName == null) {
			parametroPadrePOImpl.setUserName(StringPool.BLANK);
		}
		else {
			parametroPadrePOImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			parametroPadrePOImpl.setCreateDate(null);
		}
		else {
			parametroPadrePOImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			parametroPadrePOImpl.setModifiedDate(null);
		}
		else {
			parametroPadrePOImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (codigoPadre == null) {
			parametroPadrePOImpl.setCodigoPadre(StringPool.BLANK);
		}
		else {
			parametroPadrePOImpl.setCodigoPadre(codigoPadre);
		}

		if (nombre == null) {
			parametroPadrePOImpl.setNombre(StringPool.BLANK);
		}
		else {
			parametroPadrePOImpl.setNombre(nombre);
		}

		if (descripcion == null) {
			parametroPadrePOImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			parametroPadrePOImpl.setDescripcion(descripcion);
		}

		parametroPadrePOImpl.setEstado(estado);

		if (json == null) {
			parametroPadrePOImpl.setJson(StringPool.BLANK);
		}
		else {
			parametroPadrePOImpl.setJson(json);
		}

		parametroPadrePOImpl.resetOriginalValues();

		return parametroPadrePOImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idParametroPadre = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		codigoPadre = objectInput.readUTF();
		nombre = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		estado = objectInput.readBoolean();
		json = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idParametroPadre);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (codigoPadre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoPadre);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		objectOutput.writeBoolean(estado);

		if (json == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(json);
		}
	}

	public long idParametroPadre;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String codigoPadre;
	public String nombre;
	public String descripcion;
	public boolean estado;
	public String json;
}