package pe.com.ibk.pepper.util;

import com.google.gson.Gson;

import java.lang.reflect.Type;


/**
 *  @author Interbank Modulo Tarjeta
 * The Class GsonUtil.
 */
public class GsonUtil {
	
	private static GsonUtil instancia = new GsonUtil();
	private Gson jsonGoogle;

	private GsonUtil() {
	  jsonGoogle = new Gson();
	}

	private Gson getJsonGoogle(){
	  return jsonGoogle;
	}
	
	/**
	 * Convert JSON.
	 *
	 * @param object the object
	 * @return the object
	 */
	private  Object convertJSON(Object object)
	{
		return getJsonGoogle().toJson(object);
	}
	
	/**
	 * Object JSON.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param clazz the clazz
	 * @return the t
	 */
	private  <T> T objectJSON(String json, Class<T> clazz) {
		return getJsonGoogle().fromJson(json, clazz);
	}
	
	/**
	 * Object JSON.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param typeOfT the type of T
	 * @return the t
	 */
	private  <T> T objectJSON(String json, Type typeOfT) {
		return getJsonGoogle().fromJson(json, typeOfT);
	}
	
	/**
	 * Convert to JSON.
	 *
	 * @param objeto the objeto
	 * @return the object
	 */
	public static Object convertToJSON(Object objeto){
		return instancia.convertJSON(objeto);
	}
	
	/**
	 * Object of JSON.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param clazz the clazz
	 * @return the t
	 */
	public static <T> T objectOfJSON(String json, Class<T> clazz) {
		return instancia.objectJSON(json, clazz);
	}
	
	/**
	 * Object of JSON type.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param typeOfT the type of T
	 * @return the t
	 */
	public static <T> T objectOfJSONType(String json,Type typeOfT) {
		return instancia.objectJSON(json, typeOfT);
	}
}