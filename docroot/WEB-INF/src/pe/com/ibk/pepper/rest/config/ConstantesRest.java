package pe.com.ibk.pepper.rest.config;


/**
 * Clase que contiene las constantes del Microservicio
 * 
 * @author Adrian Pareja
 *
 */
public class ConstantesRest {

  public static final String BEAN_ENVIRONMENT = "environment";
  public static final String BEAN_SERVICE_CREAR_CLIENTE = "crearClienteService";
  public static final String BEAN_PRODUCER = "crearClienteProducer";

  public static final String PATH_REST_CREAR_CLIENTE = "crearcliente";
  public static final String PATH_REST_GENERAL = "/";

  public static final String PATH_MSG_HEADER = "Header";
  public static final String PATH_MSG_HEADER_REQUEST = "HeaderRequest";
  public static final String PATH_MSG_MESSAGE_REQUEST = "MessageRequest";
  public static final String PATH_MSG_HEADER_RESPONSE = "HeaderResponse";
  public static final String PATH_MSG_MESSAGE_RESPONSE = "MessageResponse";
  public static final String PATH_MSG_MESSAGE_ID = "messageId";
  public static final String MSG_TYPE_RESPONSE = "response";
  public static final String MSG_TYPE_REQUEST = "request";

  public static final String MSG_ERROR_SERVICE = "Ocurrio un error inesperado";
  public static final String DATE_FORMAT = "dd/MM/YYYY HH:mm:ss";
  
  public static final String MSG_TYPE_STATUS = "status";
  public static final String MSG_TYPE_RESPONSE_TYPE = "responseType";
  
  public static final String MSG_TYPE_BODY = "Body";
  public static final String MSG_TYPE_OBTENER_INFORMACION_PERSONAL = "obtenerInformacionPersonalResponse";
  public static final String MSG_TYPE_FLAG_CLIENTE = "flagCliente";
  
  public static final String SERVICIO_BUS = "BUS";
  public static final String SERVICIO_API = "API";
  
  //Valores Response
  public static final String PATH_MSG_MESSAGE_REQUEST_NEW = "messageRequest";
  public static final String PATH_MSG_HEADER_REQUEST_NEW = "headerRequest";
  
  
  public static final String PATH_MSG_MESSAGE_RESPONSE_NEW = "messageResponse";
  public static final String PATH_MSG_HEADER_NEW = "header";
  public static final String PATH_MSG_HEADER_RESPONSE_NEW = "headerResponse";
  public static final String MSG_TYPE_BODY_NEW = "body";
  
  /* Consulta Campania */
  
  //ConsultarLeadResponse
  public static final String PATH_BODY_CONSULTARLEADRESPONSE = "\"ConsultarLeadResponse\"";
  public static final String PATH_BODY_CONSULTARLEADRESPONSE_NEW = "\"consultarLeadResponse\"";
    
  //Valor CEM
  public static final String PATH_BODY_DETAIL_CEM = "\"CEM\"";
  public static final String PATH_BODY_DETAIL_CEM_NEW = "\"cem\"";   
  
  //R1
  public static final String PATH_BODY_VALIDAR_LISTA_NEGRA = "\"ValidarListaNegra\"";
  public static final String PATH_BODY_VALIDAR_LISTA_NEGRA_NEW = "\"validarListaNegra\"";
  
  public static final String PATH_BODY_RES_VALIDAR_LISTA_NEGRA = "\"ValidarListaNegraResponse\"";
  public static final String PATH_BODY_RES_VALIDAR_LISTA_NEGRA_NEW = "\"validarListaNegraResponse\"";
  
  public static final String PATH_BODY_CONSULTA_CAMPANIA = "\"ConsultarLead\"";
  public static final String PATH_BODY_CONSULTA_CAMPANIA_NEW = "\"consultarLead\"";
    
  public static final String PATH_BODY_RES_CONSULTA_CAMPANIA = "\"ConsultarLeadResponse\"";
  public static final String PATH_BODY_RES_CONSULTA_CAMPANIA_NEW = "\"consultarLeadResponse\"";
  
  //Consultar Preguntas Equifax
  public static final String PATH_BODY_CONSULTA_PREGUNTAS_EQUIFAX = "\"ConsultaPreguntasResponse\"";
  public static final String PATH_BODY_CONSULTA_PREGUNTAS_EQUIFAX_NEW = "\"consultaPreguntasResponse\"";
  
  public static final String PATH_BODY_RES_CONSULTA_PREGUNTAS_EQUIFAX = "\"ConsultaPreguntasResponse\"";
  public static final String PATH_BODY_RES_CONSULTA_PREGUNTAS_EQUIFAX_NEW = "\"consultaPreguntasResponse\"";  
  
  //Validar Preguntas Equifax
  public static final String PATH_BODY_VALIDAR_PREGUNTAS_EQUIFAX = "\"ValidacionPreguntasResponse\"";
  public static final String PATH_BODY_VALIDAR_PREGUNTAS_EQUIFAX_NEW = "\"validacionPreguntasResponse\"";
  
  public static final String PATH_BODY_RES_VALIDAR_PREGUNTAS_EQUIFAX = "\"ValidacionPreguntasResponse\"";
  public static final String PATH_BODY_RES_VALIDAR_PREGUNTAS_EQUIFAX_NEW = "\"validacionPreguntasResponse\"";
  
  /* Reemplazar pregunta1, pregunta2, pregunta1 por pregunta */
  public static final String PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_NEW = "\"pregunta\"";
  public static final String PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_1 = "\"pregunta1\"";
  public static final String PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_2 = "\"pregunta2\"";
  public static final String PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_3 = "\"pregunta3\"";
  public static final String PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_4 = "\"pregunta4\"";
  public static final String PATH_BODY_REQ_VALIDAR_PREGUNTAS_PREGUNTA_EQUIFAX_5 = "\"pregunta5\"";
  
  //Obtener Informacion Persona
  public static final String PATH_BODY_OBTENER_INFORMACION_PERSONA = "\"ObtenerInformacionPersonalResponse\"";
  public static final String PATH_BODY_OBTENER_INFORMACION_PERSONA_NEW = "\"obtenerInformacionPersonalResponse\"";
  
  public static final String PATH_BODY_RES_OBTENER_INFORMACION_PERSONA = "\"ObtenerInformacionPersonalResponse\"";
  public static final String PATH_BODY_RES_OBTENER_INFORMACION_PERSONA_NEW = "\"obtenerInformacionPersonalResponse\"";
  
  public static final String PATH_BODY_ACTUALIZAR_RESPUESTA = "\"ActualizarRespuesta\"";
  public static final String PATH_BODY_ACTUALIZAR_RESPUESTA_NEW = "\"actualizarRespuesta\"";
  
  public static final String PATH_BODY_RES_ACTUALIZAR_RESPUESTA = "\"ActualizarRespuestaResponse\"";
  public static final String PATH_BODY_RES_ACTUALIZAR_RESPUESTA_NEW = "\"actualizarRespuestaResponse\"";
  
  public static final String PATH_BODY_CONSULTAR_AFILIACION = "\"ConsultarAfiliacion\"";
  public static final String PATH_BODY_CONSULTAR_AFILIACION_NEW = "\"consultarAfiliacion\"";
  
  public static final String PATH_BODY_RES_CONSULTAR_AFILIACION = "\"ConsultarAfiliacionResponse\"";
  public static final String PATH_BODY_RES_CONSULTAR_AFILIACION_NEW = "\"consultarAfiliacionResponse\"";
  
  public static final String PATH_BODY_CALIFICACION_CDA = "\"CalificacionCDA\"";
  public static final String PATH_BODY_CALIFICACION_CDA_NEW = "\"calificacionCDA\"";
  
  public static final String PATH_BODY_RES_CALIFICACION_CDA = "\"EvaluarCalificacionResponse\"";
  public static final String PATH_BODY_RES_CALIFICACION_CDA_NEW = "\"evaluarCalificacionResponse\"";
  
  public static final String PATH_BODY_OBTENER_TARJETA = "\"ObtenerTarjetas\"";
  public static final String PATH_BODY_OBTENER_TARJETA_NEW = "\"obtenerTarjetas\"";
  public static final String PATH_BODY_ESTADO_TARJETA = "\"EstTarjeta\"";
  public static final String PATH_BODY_ESTADO_TARJETA_NEW = "\"estTarjeta\"";
  public static final String PATH_BODY_COD_UNICO = "\"CodUnico\"";
  public static final String PATH_BODY_COD_UNICO_NEW = "\"codUnico\"";
  public static final String PATH_BODY_RES_OBTENER_TARJETA = "\"ObtenerTarjetasResponse\"";
  public static final String PATH_BODY_RES_OBTENER_TARJETA_NEW = "\"obtenerTarjetasResponse\"";
  public static final String PATH_BODY_RES_OBT_TARJ_TARJETAS = "\"Tarjetas\"";
  public static final String PATH_BODY_RES_OBT_TARJ_TARJETAS_NEW = "\"tarjetas\"";
  public static final String PATH_BODY_RES_OBT_TARJ_TARJETA = "\"Tarjeta\"";
  public static final String PATH_BODY_RES_OBT_TARJ_TARJETA_NEW = "\"tarjeta\"";
  
  public static final String PATH_BODY_RES_OBT_TARJ_TIPO = "\"Tipo\"";
  public static final String PATH_BODY_RES_OBT_TARJ_TIPO_NEW = "\"tipo\"";
  public static final String PATH_BODY_RES_OBT_TARJ_NUMERO = "\"Numero\"";
  public static final String PATH_BODY_RES_OBT_TARJ_NUMERO_NEW = "\"numero\"";
  public static final String PATH_BODY_RES_OBT_TARJ_INDTIPO = "\"IndTipo\"";
  public static final String PATH_BODY_RES_OBT_TARJ_INDTIPO_NEW = "\"indTipo\"";
  public static final String PATH_BODY_RES_OBT_TARJ_ESTADO = "\"Estado\"";
  public static final String PATH_BODY_RES_OBT_TARJ_ESTADO_NEW = "\"estado\"";
  public static final String PATH_BODY_RES_OBT_TARJ_FACTIV = "\"FechaActivacion\"";
  public static final String PATH_BODY_RES_OBT_TARJ_FACTIV_NEW = "\"fechaActivacion\"";
  public static final String PATH_BODY_RES_OBT_TARJ_FBLOQ = "\"FechaBloqueo\"";
  public static final String PATH_BODY_RES_OBT_TARJ_FBLOQ_NEW = "\"fechaBloqueo\"";
  public static final String PATH_BODY_RES_OBT_TARJ_FCANCEL = "\"FechaCancelacion\"";
  public static final String PATH_BODY_RES_OBT_TARJ_FCANCEL_NEW = "\"fechaCancelacion\"";
  public static final String PATH_BODY_RES_OBT_TARJ_CALPART = "\"CalPart\"";
  public static final String PATH_BODY_RES_OBT_TARJ_CALPART_NEW = "\"calPart\"";
  
  //Lista de Servicios
  public static final String SERVICIO_VALIDACION_R1 = "VALIDACION R1";
  public static final String SERVICIO_CONSULTA_DE_CAMPANIA = "CONSULTA DE CAMPANIA";
  public static final String SERVICIO_OBTENER_INFORMACION_PERSONA = "OBTENER INFORMACION PERSONA";
  public static final String SERVICIO_CALIFICACION_CDA = "CALIFICACION CDA";
  public static final String SERVICIO_ACTUALIZAR_RESPUESTA = "ACTUALIZAR RESPUESTA";
  public static final String SERVICIO_CONSULTAR_RENIEC_BIOMETRIA = "CONSULTAR RENIEC - BIOMETRIA";
  public static final String SERVICIO_TIPIFICACION_INTENTOS = "TIPIFICACION INTENTOS";
  public static final String SERVICIO_CONSULTA_PREGUNTAS_EQUIFAX = "CONSULTA PREGUNTAS - EQUIFAX";
  public static final String SERVICIO_VALIDACION_PREGUNTAS_EQUIFAX = "VALIDACION PREGUNTAS - EQUIFAX";
  public static final String SERVICIO_CONSULTAR_AFILIACION_AON = "CONSULTAR AFILIACION AON";
  public static final String SERVICIO_GENERACION_TOKEN = "GENERACION TOKEN";
  public static final String SERVICIO_VENTA_TC_PROVISIONAL = "VENTA TC PROVISIONAL";
  public static final String SERVICIO_GENERAR_POTC = "GENERAR POTC";
  public static final String SERVICIO_CONSULTAR_PRODUCTOS_POTC = "CONSULTAR PRODUCTOS POTC";
  public static final String SERVICIO_CONSULTAR_TC_PROVISIONAL = "CONSULTARTC PROVISIONAL";
  
  public static final String PATH_BODY_VENTA_TC_NULO_NEW = "\"valorNuloPepper\"";
  public static final String PATH_BODY_VENTA_TC_NULO = "null";
  
  public static final String PATH_BODY_HTTP_NEW = "\"Http\"";
  public static final String PATH_BODY_HTTP = "\"http\"";
  public static final String PATH_BODY_DATA_NEW = "\"Data\"";
  public static final String PATH_BODY_DATA = "\"data\"";
  public static final String PATH_BODY_CONTENT_TYPE_NEW="\"contentType\"";
  public static final String PATH_BODY_CONTENT_TYPE="\"Content-Type\""; 
  
  protected ConstantesRest() {
    throw new IllegalAccessError("Clase Constantes");
  }

}
