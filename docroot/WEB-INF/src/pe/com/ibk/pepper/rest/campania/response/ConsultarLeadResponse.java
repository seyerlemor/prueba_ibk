
package pe.com.ibk.pepper.rest.campania.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsultarLeadResponse implements Serializable{

	private static final long serialVersionUID = -7123370261232433780L;
	
	@SerializedName(value="campanias")
	private Campanias campanias;

	public Campanias getCampanias() {
		return campanias;
	}

	public void setCampanias(Campanias campanias) {
		this.campanias = campanias;
	}
    
}
