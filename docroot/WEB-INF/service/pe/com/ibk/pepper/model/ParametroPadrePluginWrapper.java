/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ParametroPadrePlugin}.
 * </p>
 *
 * @author Interbank
 * @see ParametroPadrePlugin
 * @generated
 */
public class ParametroPadrePluginWrapper implements ParametroPadrePlugin,
	ModelWrapper<ParametroPadrePlugin> {
	public ParametroPadrePluginWrapper(
		ParametroPadrePlugin parametroPadrePlugin) {
		_parametroPadrePlugin = parametroPadrePlugin;
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroPadrePlugin.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroPadrePlugin.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroPadre", getIdParametroPadre());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("json", getJson());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroPadre = (Long)attributes.get("idParametroPadre");

		if (idParametroPadre != null) {
			setIdParametroPadre(idParametroPadre);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String json = (String)attributes.get("json");

		if (json != null) {
			setJson(json);
		}
	}

	/**
	* Returns the primary key of this parametro padre plugin.
	*
	* @return the primary key of this parametro padre plugin
	*/
	@Override
	public long getPrimaryKey() {
		return _parametroPadrePlugin.getPrimaryKey();
	}

	/**
	* Sets the primary key of this parametro padre plugin.
	*
	* @param primaryKey the primary key of this parametro padre plugin
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_parametroPadrePlugin.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id parametro padre of this parametro padre plugin.
	*
	* @return the id parametro padre of this parametro padre plugin
	*/
	@Override
	public long getIdParametroPadre() {
		return _parametroPadrePlugin.getIdParametroPadre();
	}

	/**
	* Sets the id parametro padre of this parametro padre plugin.
	*
	* @param idParametroPadre the id parametro padre of this parametro padre plugin
	*/
	@Override
	public void setIdParametroPadre(long idParametroPadre) {
		_parametroPadrePlugin.setIdParametroPadre(idParametroPadre);
	}

	/**
	* Returns the group ID of this parametro padre plugin.
	*
	* @return the group ID of this parametro padre plugin
	*/
	@Override
	public long getGroupId() {
		return _parametroPadrePlugin.getGroupId();
	}

	/**
	* Sets the group ID of this parametro padre plugin.
	*
	* @param groupId the group ID of this parametro padre plugin
	*/
	@Override
	public void setGroupId(long groupId) {
		_parametroPadrePlugin.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this parametro padre plugin.
	*
	* @return the company ID of this parametro padre plugin
	*/
	@Override
	public long getCompanyId() {
		return _parametroPadrePlugin.getCompanyId();
	}

	/**
	* Sets the company ID of this parametro padre plugin.
	*
	* @param companyId the company ID of this parametro padre plugin
	*/
	@Override
	public void setCompanyId(long companyId) {
		_parametroPadrePlugin.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this parametro padre plugin.
	*
	* @return the user ID of this parametro padre plugin
	*/
	@Override
	public long getUserId() {
		return _parametroPadrePlugin.getUserId();
	}

	/**
	* Sets the user ID of this parametro padre plugin.
	*
	* @param userId the user ID of this parametro padre plugin
	*/
	@Override
	public void setUserId(long userId) {
		_parametroPadrePlugin.setUserId(userId);
	}

	/**
	* Returns the user uuid of this parametro padre plugin.
	*
	* @return the user uuid of this parametro padre plugin
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePlugin.getUserUuid();
	}

	/**
	* Sets the user uuid of this parametro padre plugin.
	*
	* @param userUuid the user uuid of this parametro padre plugin
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_parametroPadrePlugin.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this parametro padre plugin.
	*
	* @return the user name of this parametro padre plugin
	*/
	@Override
	public java.lang.String getUserName() {
		return _parametroPadrePlugin.getUserName();
	}

	/**
	* Sets the user name of this parametro padre plugin.
	*
	* @param userName the user name of this parametro padre plugin
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_parametroPadrePlugin.setUserName(userName);
	}

	/**
	* Returns the create date of this parametro padre plugin.
	*
	* @return the create date of this parametro padre plugin
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _parametroPadrePlugin.getCreateDate();
	}

	/**
	* Sets the create date of this parametro padre plugin.
	*
	* @param createDate the create date of this parametro padre plugin
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_parametroPadrePlugin.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this parametro padre plugin.
	*
	* @return the modified date of this parametro padre plugin
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _parametroPadrePlugin.getModifiedDate();
	}

	/**
	* Sets the modified date of this parametro padre plugin.
	*
	* @param modifiedDate the modified date of this parametro padre plugin
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_parametroPadrePlugin.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the codigo padre of this parametro padre plugin.
	*
	* @return the codigo padre of this parametro padre plugin
	*/
	@Override
	public java.lang.String getCodigoPadre() {
		return _parametroPadrePlugin.getCodigoPadre();
	}

	/**
	* Sets the codigo padre of this parametro padre plugin.
	*
	* @param codigoPadre the codigo padre of this parametro padre plugin
	*/
	@Override
	public void setCodigoPadre(java.lang.String codigoPadre) {
		_parametroPadrePlugin.setCodigoPadre(codigoPadre);
	}

	/**
	* Returns the nombre of this parametro padre plugin.
	*
	* @return the nombre of this parametro padre plugin
	*/
	@Override
	public java.lang.String getNombre() {
		return _parametroPadrePlugin.getNombre();
	}

	/**
	* Sets the nombre of this parametro padre plugin.
	*
	* @param nombre the nombre of this parametro padre plugin
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_parametroPadrePlugin.setNombre(nombre);
	}

	/**
	* Returns the descripcion of this parametro padre plugin.
	*
	* @return the descripcion of this parametro padre plugin
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _parametroPadrePlugin.getDescripcion();
	}

	/**
	* Sets the descripcion of this parametro padre plugin.
	*
	* @param descripcion the descripcion of this parametro padre plugin
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_parametroPadrePlugin.setDescripcion(descripcion);
	}

	/**
	* Returns the estado of this parametro padre plugin.
	*
	* @return the estado of this parametro padre plugin
	*/
	@Override
	public boolean getEstado() {
		return _parametroPadrePlugin.getEstado();
	}

	/**
	* Returns <code>true</code> if this parametro padre plugin is estado.
	*
	* @return <code>true</code> if this parametro padre plugin is estado; <code>false</code> otherwise
	*/
	@Override
	public boolean isEstado() {
		return _parametroPadrePlugin.isEstado();
	}

	/**
	* Sets whether this parametro padre plugin is estado.
	*
	* @param estado the estado of this parametro padre plugin
	*/
	@Override
	public void setEstado(boolean estado) {
		_parametroPadrePlugin.setEstado(estado);
	}

	/**
	* Returns the json of this parametro padre plugin.
	*
	* @return the json of this parametro padre plugin
	*/
	@Override
	public java.lang.String getJson() {
		return _parametroPadrePlugin.getJson();
	}

	/**
	* Sets the json of this parametro padre plugin.
	*
	* @param json the json of this parametro padre plugin
	*/
	@Override
	public void setJson(java.lang.String json) {
		_parametroPadrePlugin.setJson(json);
	}

	@Override
	public boolean isNew() {
		return _parametroPadrePlugin.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_parametroPadrePlugin.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _parametroPadrePlugin.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_parametroPadrePlugin.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _parametroPadrePlugin.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _parametroPadrePlugin.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_parametroPadrePlugin.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _parametroPadrePlugin.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_parametroPadrePlugin.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_parametroPadrePlugin.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_parametroPadrePlugin.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ParametroPadrePluginWrapper((ParametroPadrePlugin)_parametroPadrePlugin.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ParametroPadrePlugin parametroPadrePlugin) {
		return _parametroPadrePlugin.compareTo(parametroPadrePlugin);
	}

	@Override
	public int hashCode() {
		return _parametroPadrePlugin.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.ParametroPadrePlugin> toCacheModel() {
		return _parametroPadrePlugin.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin toEscapedModel() {
		return new ParametroPadrePluginWrapper(_parametroPadrePlugin.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePlugin toUnescapedModel() {
		return new ParametroPadrePluginWrapper(_parametroPadrePlugin.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _parametroPadrePlugin.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _parametroPadrePlugin.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_parametroPadrePlugin.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroPadrePluginWrapper)) {
			return false;
		}

		ParametroPadrePluginWrapper parametroPadrePluginWrapper = (ParametroPadrePluginWrapper)obj;

		if (Validator.equals(_parametroPadrePlugin,
					parametroPadrePluginWrapper._parametroPadrePlugin)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ParametroPadrePlugin getWrappedParametroPadrePlugin() {
		return _parametroPadrePlugin;
	}

	@Override
	public ParametroPadrePlugin getWrappedModel() {
		return _parametroPadrePlugin;
	}

	@Override
	public void resetOriginalValues() {
		_parametroPadrePlugin.resetOriginalValues();
	}

	private ParametroPadrePlugin _parametroPadrePlugin;
}