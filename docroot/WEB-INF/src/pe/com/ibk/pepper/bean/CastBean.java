package pe.com.ibk.pepper.bean;

import javax.portlet.PortletRequest;


/**
 * @author yahir.rivas
 * @param <T>
 * */
public interface CastBean<T> {

	/**
	 * @param portletRequest
	 * @param obj array object
	 * @return collection
	 * */
	T cast(PortletRequest portletRequest, Object[] obj);
	
}
