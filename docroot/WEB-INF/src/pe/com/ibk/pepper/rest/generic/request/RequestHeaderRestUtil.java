package pe.com.ibk.pepper.rest.generic.request;

import java.util.Date;
import java.util.Map;

import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.util.NameParameterWSKey;
import pe.com.ibk.pepper.util.WebServicesUtil;

public class RequestHeaderRestUtil {
	
	private RequestHeaderRestUtil(){}
	
	 public static HeaderRequest getParameterHeaderRequest(Map<String, ParametroHijoPO> nombreMap) {
		 Request request = new Request(); 
		 request.setServiceId(nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1()); 
		 request.setConsumerId(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1());
		 request.setModuleId(nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1());
		 request.setChannelCode(nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1());
		 Date today = new Date();
		 request.setMessageId(nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1());
         request.setTimestamp(WebServicesUtil.toXMLGregorianCalendar(today));
		 request.setCountryCode(nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1());
		 request.setGroupMember(nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1());
		 request.setReferenceNumber(nombreMap.get(NameParameterWSKey.H_REFERENCE_NUMB).getDato1());
		 
		 Identity identityType = new Identity();
		 identityType.setNetId(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1());
		 identityType.setUserId(nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1());
		 identityType.setSupervisorId(nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1());
		 identityType.setDeviceId(nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1());
		 identityType.setServerId(nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1());
		 identityType.setBranchCode(nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1()); 
		 
		 return  new HeaderRequest(request, identityType);
	 }
}
