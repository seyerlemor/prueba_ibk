
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GeneralProductData {

    private List<ProductDetailList> productDetails;
    private String productName;
    
    @JsonCreator
	public GeneralProductData(List<ProductDetailList> productDetails,
			String productName) {
		super();
		this.productDetails = productDetails;
		this.productName = productName;
	}

    @JsonProperty("productDetails")
	public List<ProductDetailList> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(List<ProductDetailList> productDetails) {
		this.productDetails = productDetails;
	}

	@JsonProperty("productName")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


}
