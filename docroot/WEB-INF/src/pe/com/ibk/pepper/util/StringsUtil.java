package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import pe.com.ibk.pepper.bean.JsonLiquidBean;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.DireccionEstandar;


/**
 * 
 *  @author Interbank Modulo Tarjeta
 *
 */
public class StringsUtil {
	
	private static final Log _log = LogFactoryUtil.getLog(StringsUtil.class);

	private StringsUtil() {
	}
	
	/**
	 * Gets the cadena.
	 *
	 * @param cadena the cadena
	 * @return the cadena
	 */
	
	public static String getCadena(String cadena) {
		return GetterUtil.getString(cadena).trim();
	}

	/**
	 * 
	 * @param firstName
	 * @param secondName
	 * @return N
	 */
	public static String getNames(String firstName, String secondName){
		if(Validator.isNotNull(secondName) && secondName.equals(StringPool.BLANK)){
			return new String(firstName + " " + secondName);
		}
		return firstName;
	}

	/**
	 * Gets the address line nuevo formato.
	 *
	 * @param tipoVia the tipo via
	 * @param nombreVia the nombre via
	 * @param numero the numero
	 * @param manzana the manzana
	 * @param lote the lote
	 * @param interior the interior
	 * @param distrito the distrito
	 * @return the address line nuevo formato
	 */
	public static String getAddressLineNuevoFormato(String tipoVia,String nombreVia,String numero,String manzana,String lote,String interior)
	{
		StringBundler sb = new StringBundler();
		 
		sb.append(getCadena(tipoVia,StringPool.SPACE));
		sb.append(WordUtils.capitalizeFully(getCadena(nombreVia,StringPool.SPACE)));
		sb.append(getCadena(numero,StringPool.SPACE));
		sb.append(getCadena(manzana,StringPool.SPACE,"MZ"));
		sb.append(getCadena(lote,StringPool.SPACE,"LT"));
		sb.append(getCadena(interior,StringPool.SPACE,"INT"));
	
		return sb.toString();
	}
	
	public static String getAddressLineFormatoCompleto(String tipoVia,String nombreVia,String numero,String manzana,String lote,
			String interior,String distrito, String departamento, String pais)
	{
		StringBundler sb = new StringBundler();
		sb.append(getCadena(tipoVia,StringPool.SPACE));
		sb.append(WordUtils.capitalizeFully(getCadena(nombreVia,StringPool.SPACE)));
		sb.append(getCadena(numero,StringPool.SPACE));
		sb.append(getCadena(manzana,StringPool.SPACE,"MZ"));
		sb.append(getCadena(lote,StringPool.SPACE,"LT"));
		sb.append(getCadena(interior,StringPool.SPACE,"INT"));
		sb.append(" - ");
		sb.append(distrito.toLowerCase());
		sb.append(StringPool.COMMA_AND_SPACE);
		sb.append(getStringCapitalize(departamento.toLowerCase()));
		sb.append(StringPool.COMMA_AND_SPACE);
		sb.append(getStringCapitalize(pais));
		return sb.toString();
	}
	
	  /**
	    * Gets the cadena.
	    *
	    * @param cadena the cadena
	    * @param parametro the parametro
	    * @return the cadena
	    */
    public static String getCadena(String cadena,String parametro)
    {
	  String cadenaSeteada=GetterUtil.getString(cadena).trim();
	   if(cadenaSeteada.equals(StringPool.BLANK))
	  {
		  return StringPool.BLANK;
	  }
	   return cadenaSeteada.concat(parametro);
    }
    
    /**
     * Gets the cadena.
     *
     * @param cadena the cadena
     * @param parametro the parametro
     * @param textoInicial the texto inicial
     * @return the cadena
     */
    public static String getCadena(String cadena, String parametro,String textoInicial)
    {
 	   String cadenaSeteada=GetterUtil.getString(cadena).trim();
 	   if(cadenaSeteada.equals(StringPool.BLANK))
 	  {
 		  return StringPool.BLANK;
 	  }
 	   return textoInicial.concat(StringPool.SPACE).concat(cadenaSeteada).concat(parametro); 
    }
    
    /**
     * Gets the string capitalize.
     *
     * @param cadena the cadena
     * @return the string capitalize
     */
    
    public static String getStringCapitalize(String cadena)
    {
 	   if(Validator.isNull(cadena))
 		   return StringPool.BLANK;
 	   else
 		  return  WordUtils.capitalizeFully(StringUtils.lowerCase(cadena.trim()));
    }

    /**
     * Gets the mayuscula first leter.
     *
     * @param cadena the cadena
     * @return the mayuscula first leter
     */
    public static String getMayusculaFirstLeter(String cadena)
    {
 	 
 	   if(Validator.isNull(cadena))
 		   return StringPool.BLANK;
 	   else
 	   {
 		   String fisrtCaracter=cadena.substring(0,1);
 		   if("¿".equals(fisrtCaracter))
 			   return "¿".concat(WordUtils.capitalize(StringUtils.lowerCase(cadena).substring(1,2)).concat(StringUtils.lowerCase(cadena).substring(2,cadena.length())));
 		   else
 			   return StringUtil.upperCaseFirstLetter(cadena.toLowerCase());
 	   }
    }
 
    /**
     * Buscar en cadena.
     *
     * @param listaCadena the lista cadena
     * @param codBusqueda the cod busqueda
     * @return true, if successful
     */
    
    public static boolean buscarEnCadena(String[] listaCadena,String codBusqueda)
	{
    	Set<String> companias = new HashSet<>(Arrays.asList(listaCadena)); 
		if (Validator.isNull(codBusqueda)) {
			return false;
		}
		return companias.contains(codBusqueda);
	}
    
    /**
     * Gets the listto list.
     *
     * @param cadena the cadena
     * @param comodin the comodin
     * @return the listto list
     * Se usa una cadena de String se envie de parametro como comodin *,/,
     */
    public static String[] getListtoCadena(String cadena,String comodin)
    {
 	   return cadena.split(Pattern.quote(comodin));
    }
    
    /**
     * Gets the stringto list.
     *
     * @param cadena the cadena
     * @param comodin the comodin
     * @param posicion the posicion
     * @return the stringto list
     */
    public static String getStringtoList(String cadena,String comodin,int posicion){
 		
 	   String[] arrayCadena = cadena.split(Pattern.quote(comodin)); 
 	   if(arrayCadena.length>=posicion)
 		   return arrayCadena[posicion-1];
 	   
 	  return "";
 	}
    
    /**
     * Gets the string formateado.
     *
     * @param cadenaValidar the cadena validar
     * @return the string formateado
     */
	public static String getStringFormateado(String cadenaValidar) {
		if (Validator.isNull(cadenaValidar))
			return StringPool.BLANK;
		else
			return StringUtils.upperCase(cadenaValidar.trim());
	}

	/**
	 * 
	 * @param errorCode Error code to search.
	 * @param codsError Error code list.
	 * @return true if error code is in list, otherwhise false.
	 */
	public static boolean isError(String errorCode, String codsError){
		if(Validator.isNotNull(errorCode) 
				&& StringsUtil.buscarEnCadena(StringsUtil.getListtoCadena(codsError, ","), errorCode)){
			return true;
		}
		return false;
	}

	public static String getObfuscatedCard(String cardNumber, int length){
		StringBundler bundler = new StringBundler(length);
		bundler.append(cardNumber.substring(0, 6));
		for (int i = 6; i < length-4; i++) {
			bundler.append("*");
		}
		bundler.append(cardNumber.substring(length-4, length));
		return bundler.toString();
	}
	
	public static String getRucApertura(String razonSocial){
		if(!razonSocial.contains("<") && !razonSocial.contains(">") && !razonSocial.contains("&")){
			return razonSocial;
		}
		StringBundler bundler = new StringBundler();
		for (int i = 0; i < razonSocial.length(); i++) {
			switch (razonSocial.charAt(i)) {
			case '<':
				bundler.append("<![CDATA[<]]>");
				break;
			case '>':
				bundler.append("<![CDATA[>]]>");
				break;
			case '&':
				bundler.append("<![CDATA[&]]>");
				break;
			default:
				bundler.append(razonSocial.charAt(i));
				break;
			}
		}
		return bundler.toString();
	}
	
	public static String encodeParamUri(List<String> params, List<String> values){
		StringBundler bundler = new StringBundler();
		if(!params.isEmpty()){
			bundler.append("?");
		}else{
			return "";
		}
		
		try {
			for (int i = 0; i < params.size(); i++) {
				if(Validator.isNotNull(params.get(i))){
					bundler.append(params.get(i));
					bundler.append("=");
					bundler.append(encodeBase64Unicode(values.get(i)));
					bundler.append("&");
				}
			}
		} catch (Exception e) {
			_log.error("error en metodo encodeParamUri(): ", e);
		}
		
		return bundler.toString();
	}
	
	public static String encodeBase64Unicode(String value) throws UnsupportedEncodingException{
		byte[] message = value.getBytes("UTF-8");
		return DatatypeConverter.printBase64Binary(message);
	}
	
	public static String enmascararCorreo(String correo){
		String parte1 = correo.substring(0,correo.indexOf('@'));
		String parte2 = correo.substring(correo.indexOf('@'));
		
		StringBundler bundler = new StringBundler();
		for(int i=0 ; i<parte1.length();i++){
			char c = parte1.charAt(i);
			if(i>1){
				bundler.append("*");
			} else {
				bundler.append(c);
			}
		}
		return bundler.append(parte2).toString();
	}
	
	public static String getOnlyHora(){
		Calendar now = Calendar.getInstance();
		StringBundler bundler = new StringBundler();
		
		bundler.append(now.get(Calendar.HOUR_OF_DAY));
		bundler.append(":");
		bundler.append(now.get(Calendar.MINUTE));
		bundler.append(":");
		bundler.append(now.get(Calendar.SECOND));
		bundler.append(" ");
		bundler.append(now.get(Calendar.MILLISECOND));
		
		return bundler.toString();
	}
	
	/**
	 * 
	 * @param begin begin pattern to star string html. Is optional
	 * @param patron String pattern[$1,$2] to repeat in string html
	 * @param fechasDisponibles JsonBean with value[$1] andl label [$2].
	 * @return string html
	 */
	public static String getHtmlLista(String begin, String patron, List<JsonLiquidBean> fechasDisponibles){
		StringBundler bundler = new StringBundler();
		if(Validator.isNotNull(begin)){
			bundler.append(begin);
		}
		
		for (int i = 0; i < fechasDisponibles.size(); i++) {
			bundler.append(patron.replace("$1", fechasDisponibles.get(i).getValue())
					.replace("$2", fechasDisponibles.get(i).getLabel()).replace("$3",StringPool.BLANK.concat(Integer.toString(i+1))));
		}
		
		return bundler.toString();
	}
	
	public static String getParams(String... params){
		StringBundler bundler = new StringBundler();
		for( String param : params){
			bundler.append(StringPool.SLASH);
			bundler.append(param);
		}
		return bundler.toString();
	}
	
	public static String formatAddressPepper(DireccionEstandar direccionEstandar){
		String formatAddress = StringPool.BLANK;
		if(Validator.isNotNull(direccionEstandar)){
			formatAddress+=direccionEstandar.getTipoVia().concat(StringPool.SPACE).concat(direccionEstandar.getNombreVia());
			if(!StringPool.BLANK.equals(direccionEstandar.getNumero()))
					formatAddress+=StringPool.SPACE.concat(Constantes.COD_ADD_NRO).concat(StringPool.SPACE).concat(direccionEstandar.getNumero());
			if(!StringPool.BLANK.equals(direccionEstandar.getInterior()))
					formatAddress+=StringPool.SPACE.concat(Constantes.COD_ADD_INT).concat(StringPool.SPACE).concat(direccionEstandar.getInterior());
			if(!StringPool.BLANK.equals(direccionEstandar.getManzana()))
					formatAddress+=StringPool.SPACE.concat(Constantes.COD_ADD_MZ).concat(StringPool.SPACE).concat(direccionEstandar.getManzana());
			if(!StringPool.BLANK.equals(direccionEstandar.getPisoLote()))
					formatAddress+=StringPool.SPACE.concat(Constantes.COD_ADD_LT).concat(StringPool.SPACE).concat(direccionEstandar.getPisoLote());
		}
		return formatAddress.toUpperCase();
	}
}
