package pe.com.ibk.pepper.bean;

import java.io.Serializable;
import java.util.Date;

public class DireccionClienteBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long idDireccion;
	private long idDatoCliente;
	private String tipoDireccion;
	private String codigoUso;
	private String flagDireccionEstandar;
	private String via;
	private String departamento;
	private String provincia;
	private String distrito;
	private String pais;
	private String ubigeo;
	private String codigoPostal;
	private String estado;
	private Date fechaRegistro;
	
	public long getIdDireccion() {
		return idDireccion;
	}
	public void setIdDireccion(long idDireccion) {
		this.idDireccion = idDireccion;
	}
	public long getIdDatoCliente() {
		return idDatoCliente;
	}
	public void setIdDatoCliente(long idDatoCliente) {
		this.idDatoCliente = idDatoCliente;
	}
	public String getTipoDireccion() {
		return tipoDireccion;
	}
	public void setTipoDireccion(String tipoDireccion) {
		this.tipoDireccion = tipoDireccion;
	}
	public String getCodigoUso() {
		return codigoUso;
	}
	public void setCodigoUso(String codigoUso) {
		this.codigoUso = codigoUso;
	}
	public String getFlagDireccionEstandar() {
		return flagDireccionEstandar;
	}
	public void setFlagDireccionEstandar(String flagDireccionEstandar) {
		this.flagDireccionEstandar = flagDireccionEstandar;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Override
	public String toString() {
		return "DireccionClienteBean [idDireccion=" + idDireccion
				+ ", idDatoCliente=" + idDatoCliente + ", tipoDireccion="
				+ tipoDireccion + ", codigoUso=" + codigoUso
				+ ", flagDireccionEstandar=" + flagDireccionEstandar + ", via="
				+ via + ", departamento=" + departamento + ", provincia="
				+ provincia + ", distrito=" + distrito + ", pais=" + pais
				+ ", ubigeo=" + ubigeo + ", codigoPostal=" + codigoPostal
				+ ", estado=" + estado + ", fechaRegistro=" + fechaRegistro
				+ "]";
	}


	
}
