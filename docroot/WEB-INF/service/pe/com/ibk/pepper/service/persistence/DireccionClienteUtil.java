/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.DireccionCliente;

import java.util.List;

/**
 * The persistence utility for the direccion cliente service. This utility wraps {@link DireccionClientePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see DireccionClientePersistence
 * @see DireccionClientePersistenceImpl
 * @generated
 */
public class DireccionClienteUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(DireccionCliente direccionCliente) {
		getPersistence().clearCache(direccionCliente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DireccionCliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DireccionCliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DireccionCliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static DireccionCliente update(DireccionCliente direccionCliente)
		throws SystemException {
		return getPersistence().update(direccionCliente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static DireccionCliente update(DireccionCliente direccionCliente,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(direccionCliente, serviceContext);
	}

	/**
	* Returns the direccion cliente where idDireccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	*
	* @param idDireccion the id direccion
	* @return the matching direccion cliente
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente findByDC_ID(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException {
		return getPersistence().findByDC_ID(idDireccion);
	}

	/**
	* Returns the direccion cliente where idDireccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idDireccion the id direccion
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente fetchByDC_ID(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDC_ID(idDireccion);
	}

	/**
	* Returns the direccion cliente where idDireccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idDireccion the id direccion
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente fetchByDC_ID(
		long idDireccion, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDC_ID(idDireccion, retrieveFromCache);
	}

	/**
	* Removes the direccion cliente where idDireccion = &#63; from the database.
	*
	* @param idDireccion the id direccion
	* @return the direccion cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente removeByDC_ID(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException {
		return getPersistence().removeByDC_ID(idDireccion);
	}

	/**
	* Returns the number of direccion clientes where idDireccion = &#63;.
	*
	* @param idDireccion the id direccion
	* @return the number of matching direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDC_ID(long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDC_ID(idDireccion);
	}

	/**
	* Returns the direccion cliente where codigoUso = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	*
	* @param codigoUso the codigo uso
	* @return the matching direccion cliente
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente findByDC_CU(
		java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException {
		return getPersistence().findByDC_CU(codigoUso);
	}

	/**
	* Returns the direccion cliente where codigoUso = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoUso the codigo uso
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente fetchByDC_CU(
		java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDC_CU(codigoUso);
	}

	/**
	* Returns the direccion cliente where codigoUso = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoUso the codigo uso
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente fetchByDC_CU(
		java.lang.String codigoUso, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDC_CU(codigoUso, retrieveFromCache);
	}

	/**
	* Removes the direccion cliente where codigoUso = &#63; from the database.
	*
	* @param codigoUso the codigo uso
	* @return the direccion cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente removeByDC_CU(
		java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException {
		return getPersistence().removeByDC_CU(codigoUso);
	}

	/**
	* Returns the number of direccion clientes where codigoUso = &#63;.
	*
	* @param codigoUso the codigo uso
	* @return the number of matching direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDC_CU(java.lang.String codigoUso)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDC_CU(codigoUso);
	}

	/**
	* Caches the direccion cliente in the entity cache if it is enabled.
	*
	* @param direccionCliente the direccion cliente
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.DireccionCliente direccionCliente) {
		getPersistence().cacheResult(direccionCliente);
	}

	/**
	* Caches the direccion clientes in the entity cache if it is enabled.
	*
	* @param direccionClientes the direccion clientes
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.DireccionCliente> direccionClientes) {
		getPersistence().cacheResult(direccionClientes);
	}

	/**
	* Creates a new direccion cliente with the primary key. Does not add the direccion cliente to the database.
	*
	* @param idDireccion the primary key for the new direccion cliente
	* @return the new direccion cliente
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente create(
		long idDireccion) {
		return getPersistence().create(idDireccion);
	}

	/**
	* Removes the direccion cliente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDireccion the primary key of the direccion cliente
	* @return the direccion cliente that was removed
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente remove(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException {
		return getPersistence().remove(idDireccion);
	}

	public static pe.com.ibk.pepper.model.DireccionCliente updateImpl(
		pe.com.ibk.pepper.model.DireccionCliente direccionCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(direccionCliente);
	}

	/**
	* Returns the direccion cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	*
	* @param idDireccion the primary key of the direccion cliente
	* @return the direccion cliente
	* @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente findByPrimaryKey(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDireccionClienteException {
		return getPersistence().findByPrimaryKey(idDireccion);
	}

	/**
	* Returns the direccion cliente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDireccion the primary key of the direccion cliente
	* @return the direccion cliente, or <code>null</code> if a direccion cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.DireccionCliente fetchByPrimaryKey(
		long idDireccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idDireccion);
	}

	/**
	* Returns all the direccion clientes.
	*
	* @return the direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.DireccionCliente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the direccion clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of direccion clientes
	* @param end the upper bound of the range of direccion clientes (not inclusive)
	* @return the range of direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.DireccionCliente> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the direccion clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of direccion clientes
	* @param end the upper bound of the range of direccion clientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.DireccionCliente> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the direccion clientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of direccion clientes.
	*
	* @return the number of direccion clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DireccionClientePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DireccionClientePersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					DireccionClientePersistence.class.getName());

			ReferenceRegistry.registerReference(DireccionClienteUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(DireccionClientePersistence persistence) {
	}

	private static DireccionClientePersistence _persistence;
}