<%@ include file="init.jsp" %>
<script>
	dataLayer.push({
		 'event': 'virtualEvent',
		 'category': 'Error de Formulario',
		 'action': '${errorPage}',
		 'label': '${errorMessage}'
	});
</script>