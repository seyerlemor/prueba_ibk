
package pe.com.ibk.pepper.rest.obtenerinformacion.response;


public class Direccion {

    private String tipoDireccion;
    private String codigoUso;
    private String idSecuencia;
    private String flagDireccionEstandar;
    private DireccionEstandar direccionEstandar;
    private String via;
    private String departamento;
    private String provincia;
    private String distrito;
    private String pais;
    private String ubigeo;
    private String codigoPostal;
    private Boolean tipoEstado;
    private String addressDetail;

    public Direccion(String flagDireccionEstandar, String codigoUso){
    	this.flagDireccionEstandar = flagDireccionEstandar;
    	this.codigoUso = codigoUso;
    }
    
    public Boolean getTipoEstado() {
		return tipoEstado;
	}

	public void setTipoEstado(Boolean tipoEstado) {
		this.tipoEstado = tipoEstado;
	}

	//Para flag='N'
    private String nombreDireccion;
    
    public Direccion() {
		super();
		direccionEstandar = new DireccionEstandar();
	}

    public String getTipoDireccion() {
        return tipoDireccion;
    }

    public void setTipoDireccion(String tipoDireccion) {
        this.tipoDireccion = tipoDireccion;
    }

    public String getCodigoUso() {
        return codigoUso;
    }

    public void setCodigoUso(String codigoUso) {
        this.codigoUso = codigoUso;
    }

    public String getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(String idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public String getFlagDireccionEstandar() {
        return flagDireccionEstandar;
    }

    public void setFlagDireccionEstandar(String flagDireccionEstandar) {
        this.flagDireccionEstandar = flagDireccionEstandar;
    }

    public DireccionEstandar getDireccionEstandar() {
        return direccionEstandar;
    }

    public void setDireccionEstandar(DireccionEstandar direccionEstandar) {
        this.direccionEstandar = direccionEstandar;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

	public String getNombreDireccion() {
		return nombreDireccion;
	}

	public void setNombreDireccion(String nombreDireccion) {
		this.nombreDireccion = nombreDireccion;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	@Override
	public String toString() {
		return "Direccion [tipoDireccion=" + tipoDireccion + ", codigoUso="
				+ codigoUso + ", idSecuencia=" + idSecuencia
				+ ", flagDireccionEstandar=" + flagDireccionEstandar
				+ ", direccionEstandar=" + direccionEstandar + ", via=" + via
				+ ", departamento=" + departamento + ", provincia=" + provincia
				+ ", distrito=" + distrito + ", pais=" + pais + ", ubigeo="
				+ ubigeo + ", codigoPostal=" + codigoPostal + ", tipoEstado="
				+ tipoEstado + ", addressDetail=" + addressDetail
				+ ", nombreDireccion=" + nombreDireccion + "]";
	}

}
