/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ParametroPadrePOLocalService}.
 *
 * @author Interbank
 * @see ParametroPadrePOLocalService
 * @generated
 */
public class ParametroPadrePOLocalServiceWrapper
	implements ParametroPadrePOLocalService,
		ServiceWrapper<ParametroPadrePOLocalService> {
	public ParametroPadrePOLocalServiceWrapper(
		ParametroPadrePOLocalService parametroPadrePOLocalService) {
		_parametroPadrePOLocalService = parametroPadrePOLocalService;
	}

	/**
	* Adds the parametro padre p o to the database. Also notifies the appropriate model listeners.
	*
	* @param parametroPadrePO the parametro padre p o
	* @return the parametro padre p o that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO addParametroPadrePO(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.addParametroPadrePO(parametroPadrePO);
	}

	/**
	* Creates a new parametro padre p o with the primary key. Does not add the parametro padre p o to the database.
	*
	* @param idParametroPadre the primary key for the new parametro padre p o
	* @return the new parametro padre p o
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO createParametroPadrePO(
		long idParametroPadre) {
		return _parametroPadrePOLocalService.createParametroPadrePO(idParametroPadre);
	}

	/**
	* Deletes the parametro padre p o with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idParametroPadre the primary key of the parametro padre p o
	* @return the parametro padre p o that was removed
	* @throws PortalException if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO deleteParametroPadrePO(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.deleteParametroPadrePO(idParametroPadre);
	}

	/**
	* Deletes the parametro padre p o from the database. Also notifies the appropriate model listeners.
	*
	* @param parametroPadrePO the parametro padre p o
	* @return the parametro padre p o that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO deleteParametroPadrePO(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.deleteParametroPadrePO(parametroPadrePO);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _parametroPadrePOLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO fetchParametroPadrePO(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.fetchParametroPadrePO(idParametroPadre);
	}

	/**
	* Returns the parametro padre p o with the primary key.
	*
	* @param idParametroPadre the primary key of the parametro padre p o
	* @return the parametro padre p o
	* @throws PortalException if a parametro padre p o with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO getParametroPadrePO(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.getParametroPadrePO(idParametroPadre);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the parametro padre p os.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePOModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro padre p os
	* @param end the upper bound of the range of parametro padre p os (not inclusive)
	* @return the range of parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePO> getParametroPadrePOs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.getParametroPadrePOs(start, end);
	}

	/**
	* Returns the number of parametro padre p os.
	*
	* @return the number of parametro padre p os
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getParametroPadrePOsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.getParametroPadrePOsCount();
	}

	/**
	* Updates the parametro padre p o in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param parametroPadrePO the parametro padre p o
	* @return the parametro padre p o that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO updateParametroPadrePO(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePOLocalService.updateParametroPadrePO(parametroPadrePO);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _parametroPadrePOLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_parametroPadrePOLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _parametroPadrePOLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ParametroPadrePOLocalService getWrappedParametroPadrePOLocalService() {
		return _parametroPadrePOLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedParametroPadrePOLocalService(
		ParametroPadrePOLocalService parametroPadrePOLocalService) {
		_parametroPadrePOLocalService = parametroPadrePOLocalService;
	}

	@Override
	public ParametroPadrePOLocalService getWrappedService() {
		return _parametroPadrePOLocalService;
	}

	@Override
	public void setWrappedService(
		ParametroPadrePOLocalService parametroPadrePOLocalService) {
		_parametroPadrePOLocalService = parametroPadrePOLocalService;
	}

	private ParametroPadrePOLocalService _parametroPadrePOLocalService;
}