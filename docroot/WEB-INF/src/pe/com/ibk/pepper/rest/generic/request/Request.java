package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Request {

	private String serviceId;
	private String consumerId;
	private String moduleId;
	private String channelCode;
	private String messageId;
	private String timestamp;
	private String countryCode;
	private String groupMember;
	private String referenceNumber;
	
	@JsonCreator
	public Request() {
		super();
	}

	@JsonCreator
	public Request(String serviceId, String consumerId, String moduleId,
			String channelCode, String messageId, String timestamp,
			String countryCode, String groupMember, String referenceNumber) {
		super();
		this.serviceId = serviceId;
		this.consumerId = consumerId;
		this.moduleId = moduleId;
		this.channelCode = channelCode;
		this.messageId = messageId;
		this.timestamp = timestamp;
		this.countryCode = countryCode;
		this.groupMember = groupMember;
		this.referenceNumber = referenceNumber;
	}

	@JsonProperty("serviceId")
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@JsonProperty("consumerId")
	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	@JsonProperty("moduleId")
	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	@JsonProperty("channelCode")
	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	@JsonProperty("messageId")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@JsonProperty("timestamp")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@JsonProperty("countryCode")
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@JsonProperty("groupMember")
	public String getGroupMember() {
		return groupMember;
	}

	public void setGroupMember(String groupMember) {
		this.groupMember = groupMember;
	}

	@JsonProperty("referenceNumber")
	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public String toString() {
		return "RequestGeneric [serviceId=" + serviceId + ", consumerId="
				+ consumerId + ", moduleId=" + moduleId + ", channelCode="
				+ channelCode + ", messageId=" + messageId + ", timestamp="
				+ timestamp + ", countryCode=" + countryCode + ", groupMember="
				+ groupMember + ", referenceNumber=" + referenceNumber + "]";
	}
	

}
