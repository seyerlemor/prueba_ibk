
package pe.com.ibk.pepper.rest.equifax.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Preguntum implements Serializable {

	private static final long serialVersionUID = -4090174694142493334L;
	
	@SerializedName("categoriaPregunta")
	private String categoriaPregunta;
	
	@SerializedName("numeroPregunta")
    private String numeroPregunta;
	
	@SerializedName("descripcionPregunta")
    private String descripcionPregunta;
	
	@SerializedName("opciones")
    private Opciones opciones;
	
	@SerializedName("numeroOpcion")
    private String numeroOpcion;
    
	public String getNumeroOpcion() {
		return numeroOpcion;
	}

	public void setNumeroOpcion(String numeroOpcion) {
		this.numeroOpcion = numeroOpcion;
	}

	public String getCategoriaPregunta() {
        return categoriaPregunta;
    }

    public void setCategoriaPregunta(String categoriaPregunta) {
        this.categoriaPregunta = categoriaPregunta;
    }

    public String getNumeroPregunta() {
        return numeroPregunta;
    }

    public void setNumeroPregunta(String numeroPregunta) {
        this.numeroPregunta = numeroPregunta;
    }

    public String getDescripcionPregunta() {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta) {
        this.descripcionPregunta = descripcionPregunta;
    }

    public Opciones getOpciones() {
        return opciones;
    }

    public void setOpciones(Opciones opciones) {
        this.opciones = opciones;
    }

}
