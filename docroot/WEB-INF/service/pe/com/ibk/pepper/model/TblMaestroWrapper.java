/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TblMaestro}.
 * </p>
 *
 * @author Interbank
 * @see TblMaestro
 * @generated
 */
public class TblMaestroWrapper implements TblMaestro, ModelWrapper<TblMaestro> {
	public TblMaestroWrapper(TblMaestro tblMaestro) {
		_tblMaestro = tblMaestro;
	}

	@Override
	public Class<?> getModelClass() {
		return TblMaestro.class;
	}

	@Override
	public String getModelClassName() {
		return TblMaestro.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idMaestro", getIdMaestro());
		attributes.put("nombreComercio", getNombreComercio());
		attributes.put("idHash", getIdHash());
		attributes.put("tipoTarjeta", getTipoTarjeta());
		attributes.put("urlImagen", getUrlImagen());
		attributes.put("topeOtp", getTopeOtp());
		attributes.put("topeEquifax", getTopeEquifax());
		attributes.put("codigoSeguridad", getCodigoSeguridad());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idMaestro = (Integer)attributes.get("idMaestro");

		if (idMaestro != null) {
			setIdMaestro(idMaestro);
		}

		String nombreComercio = (String)attributes.get("nombreComercio");

		if (nombreComercio != null) {
			setNombreComercio(nombreComercio);
		}

		String idHash = (String)attributes.get("idHash");

		if (idHash != null) {
			setIdHash(idHash);
		}

		String tipoTarjeta = (String)attributes.get("tipoTarjeta");

		if (tipoTarjeta != null) {
			setTipoTarjeta(tipoTarjeta);
		}

		Date urlImagen = (Date)attributes.get("urlImagen");

		if (urlImagen != null) {
			setUrlImagen(urlImagen);
		}

		String topeOtp = (String)attributes.get("topeOtp");

		if (topeOtp != null) {
			setTopeOtp(topeOtp);
		}

		String topeEquifax = (String)attributes.get("topeEquifax");

		if (topeEquifax != null) {
			setTopeEquifax(topeEquifax);
		}

		String codigoSeguridad = (String)attributes.get("codigoSeguridad");

		if (codigoSeguridad != null) {
			setCodigoSeguridad(codigoSeguridad);
		}
	}

	/**
	* Returns the primary key of this tbl maestro.
	*
	* @return the primary key of this tbl maestro
	*/
	@Override
	public int getPrimaryKey() {
		return _tblMaestro.getPrimaryKey();
	}

	/**
	* Sets the primary key of this tbl maestro.
	*
	* @param primaryKey the primary key of this tbl maestro
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_tblMaestro.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id maestro of this tbl maestro.
	*
	* @return the id maestro of this tbl maestro
	*/
	@Override
	public int getIdMaestro() {
		return _tblMaestro.getIdMaestro();
	}

	/**
	* Sets the id maestro of this tbl maestro.
	*
	* @param idMaestro the id maestro of this tbl maestro
	*/
	@Override
	public void setIdMaestro(int idMaestro) {
		_tblMaestro.setIdMaestro(idMaestro);
	}

	/**
	* Returns the nombre comercio of this tbl maestro.
	*
	* @return the nombre comercio of this tbl maestro
	*/
	@Override
	public java.lang.String getNombreComercio() {
		return _tblMaestro.getNombreComercio();
	}

	/**
	* Sets the nombre comercio of this tbl maestro.
	*
	* @param nombreComercio the nombre comercio of this tbl maestro
	*/
	@Override
	public void setNombreComercio(java.lang.String nombreComercio) {
		_tblMaestro.setNombreComercio(nombreComercio);
	}

	/**
	* Returns the id hash of this tbl maestro.
	*
	* @return the id hash of this tbl maestro
	*/
	@Override
	public java.lang.String getIdHash() {
		return _tblMaestro.getIdHash();
	}

	/**
	* Sets the id hash of this tbl maestro.
	*
	* @param idHash the id hash of this tbl maestro
	*/
	@Override
	public void setIdHash(java.lang.String idHash) {
		_tblMaestro.setIdHash(idHash);
	}

	/**
	* Returns the tipo tarjeta of this tbl maestro.
	*
	* @return the tipo tarjeta of this tbl maestro
	*/
	@Override
	public java.lang.String getTipoTarjeta() {
		return _tblMaestro.getTipoTarjeta();
	}

	/**
	* Sets the tipo tarjeta of this tbl maestro.
	*
	* @param tipoTarjeta the tipo tarjeta of this tbl maestro
	*/
	@Override
	public void setTipoTarjeta(java.lang.String tipoTarjeta) {
		_tblMaestro.setTipoTarjeta(tipoTarjeta);
	}

	/**
	* Returns the url imagen of this tbl maestro.
	*
	* @return the url imagen of this tbl maestro
	*/
	@Override
	public java.util.Date getUrlImagen() {
		return _tblMaestro.getUrlImagen();
	}

	/**
	* Sets the url imagen of this tbl maestro.
	*
	* @param urlImagen the url imagen of this tbl maestro
	*/
	@Override
	public void setUrlImagen(java.util.Date urlImagen) {
		_tblMaestro.setUrlImagen(urlImagen);
	}

	/**
	* Returns the tope otp of this tbl maestro.
	*
	* @return the tope otp of this tbl maestro
	*/
	@Override
	public java.lang.String getTopeOtp() {
		return _tblMaestro.getTopeOtp();
	}

	/**
	* Sets the tope otp of this tbl maestro.
	*
	* @param topeOtp the tope otp of this tbl maestro
	*/
	@Override
	public void setTopeOtp(java.lang.String topeOtp) {
		_tblMaestro.setTopeOtp(topeOtp);
	}

	/**
	* Returns the tope equifax of this tbl maestro.
	*
	* @return the tope equifax of this tbl maestro
	*/
	@Override
	public java.lang.String getTopeEquifax() {
		return _tblMaestro.getTopeEquifax();
	}

	/**
	* Sets the tope equifax of this tbl maestro.
	*
	* @param topeEquifax the tope equifax of this tbl maestro
	*/
	@Override
	public void setTopeEquifax(java.lang.String topeEquifax) {
		_tblMaestro.setTopeEquifax(topeEquifax);
	}

	/**
	* Returns the codigo seguridad of this tbl maestro.
	*
	* @return the codigo seguridad of this tbl maestro
	*/
	@Override
	public java.lang.String getCodigoSeguridad() {
		return _tblMaestro.getCodigoSeguridad();
	}

	/**
	* Sets the codigo seguridad of this tbl maestro.
	*
	* @param codigoSeguridad the codigo seguridad of this tbl maestro
	*/
	@Override
	public void setCodigoSeguridad(java.lang.String codigoSeguridad) {
		_tblMaestro.setCodigoSeguridad(codigoSeguridad);
	}

	@Override
	public boolean isNew() {
		return _tblMaestro.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_tblMaestro.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _tblMaestro.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_tblMaestro.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _tblMaestro.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _tblMaestro.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_tblMaestro.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _tblMaestro.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_tblMaestro.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_tblMaestro.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_tblMaestro.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TblMaestroWrapper((TblMaestro)_tblMaestro.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.TblMaestro tblMaestro) {
		return _tblMaestro.compareTo(tblMaestro);
	}

	@Override
	public int hashCode() {
		return _tblMaestro.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.TblMaestro> toCacheModel() {
		return _tblMaestro.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.TblMaestro toEscapedModel() {
		return new TblMaestroWrapper(_tblMaestro.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.TblMaestro toUnescapedModel() {
		return new TblMaestroWrapper(_tblMaestro.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _tblMaestro.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _tblMaestro.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_tblMaestro.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TblMaestroWrapper)) {
			return false;
		}

		TblMaestroWrapper tblMaestroWrapper = (TblMaestroWrapper)obj;

		if (Validator.equals(_tblMaestro, tblMaestroWrapper._tblMaestro)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public TblMaestro getWrappedTblMaestro() {
		return _tblMaestro;
	}

	@Override
	public TblMaestro getWrappedModel() {
		return _tblMaestro;
	}

	@Override
	public void resetOriginalValues() {
		_tblMaestro.resetOriginalValues();
	}

	private TblMaestro _tblMaestro;
}