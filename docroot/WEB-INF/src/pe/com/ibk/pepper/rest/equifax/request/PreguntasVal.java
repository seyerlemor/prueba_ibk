
package pe.com.ibk.pepper.rest.equifax.request;

import java.util.ArrayList;
import java.util.List;

public class PreguntasVal {

    private List<PreguntumVal> pregunta;
 
	public PreguntasVal() {
		super();
		pregunta = new ArrayList<PreguntumVal>();
	}
	
	public PreguntasVal(List<PreguntumVal> pregunta) {
		super();
		this.pregunta = pregunta;
	}
	
	public List<PreguntumVal> getPregunta() {
		return pregunta;
	}
	
	public void setPregunta(List<PreguntumVal> pregunta1) {
		this.pregunta = pregunta1;
	}

}
