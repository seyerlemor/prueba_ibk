
package pe.com.ibk.pepper.rest.equifax.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Opcion implements Serializable {

	private static final long serialVersionUID = 5313150808418406013L;
	
	@SerializedName("numeroOpcion")
	private String numeroOpcion;
	
	@SerializedName("descripcionOpcion")
    private String descripcionOpcion;  
   
	public Opcion(String numeroOpcion, String descripcionOpcion) {
		super();
		this.numeroOpcion = numeroOpcion;
		this.descripcionOpcion = descripcionOpcion;
	}

	public String getNumeroOpcion() {
        return numeroOpcion;
    }

    public void setNumeroOpcion(String numeroOpcion) {
        this.numeroOpcion = numeroOpcion;
    }

    public String getDescripcionOpcion() {
        return descripcionOpcion;
    }

    public void setDescripcionOpcion(String descripcionOpcion) {
        this.descripcionOpcion = descripcionOpcion;
    }

}
