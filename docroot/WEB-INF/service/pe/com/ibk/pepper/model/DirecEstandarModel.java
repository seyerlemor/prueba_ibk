/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the DirecEstandar service. Represents a row in the &quot;T_DIRECCION_ESTANDAR&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link pe.com.ibk.pepper.model.impl.DirecEstandarImpl}.
 * </p>
 *
 * @author Interbank
 * @see DirecEstandar
 * @see pe.com.ibk.pepper.model.impl.DirecEstandarImpl
 * @see pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl
 * @generated
 */
public interface DirecEstandarModel extends BaseModel<DirecEstandar> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a direc estandar model instance should use the {@link DirecEstandar} interface instead.
	 */

	/**
	 * Returns the primary key of this direc estandar.
	 *
	 * @return the primary key of this direc estandar
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this direc estandar.
	 *
	 * @param primaryKey the primary key of this direc estandar
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the id detalle direccion of this direc estandar.
	 *
	 * @return the id detalle direccion of this direc estandar
	 */
	public long getIdDetalleDireccion();

	/**
	 * Sets the id detalle direccion of this direc estandar.
	 *
	 * @param idDetalleDireccion the id detalle direccion of this direc estandar
	 */
	public void setIdDetalleDireccion(long idDetalleDireccion);

	/**
	 * Returns the id direccion of this direc estandar.
	 *
	 * @return the id direccion of this direc estandar
	 */
	public long getIdDireccion();

	/**
	 * Sets the id direccion of this direc estandar.
	 *
	 * @param idDireccion the id direccion of this direc estandar
	 */
	public void setIdDireccion(long idDireccion);

	/**
	 * Returns the tipo via of this direc estandar.
	 *
	 * @return the tipo via of this direc estandar
	 */
	@AutoEscape
	public String getTipoVia();

	/**
	 * Sets the tipo via of this direc estandar.
	 *
	 * @param tipoVia the tipo via of this direc estandar
	 */
	public void setTipoVia(String tipoVia);

	/**
	 * Returns the nombre via of this direc estandar.
	 *
	 * @return the nombre via of this direc estandar
	 */
	@AutoEscape
	public String getNombreVia();

	/**
	 * Sets the nombre via of this direc estandar.
	 *
	 * @param nombreVia the nombre via of this direc estandar
	 */
	public void setNombreVia(String nombreVia);

	/**
	 * Returns the numero of this direc estandar.
	 *
	 * @return the numero of this direc estandar
	 */
	@AutoEscape
	public String getNumero();

	/**
	 * Sets the numero of this direc estandar.
	 *
	 * @param numero the numero of this direc estandar
	 */
	public void setNumero(String numero);

	/**
	 * Returns the manzana of this direc estandar.
	 *
	 * @return the manzana of this direc estandar
	 */
	@AutoEscape
	public String getManzana();

	/**
	 * Sets the manzana of this direc estandar.
	 *
	 * @param manzana the manzana of this direc estandar
	 */
	public void setManzana(String manzana);

	/**
	 * Returns the piso lote of this direc estandar.
	 *
	 * @return the piso lote of this direc estandar
	 */
	@AutoEscape
	public String getPisoLote();

	/**
	 * Sets the piso lote of this direc estandar.
	 *
	 * @param pisoLote the piso lote of this direc estandar
	 */
	public void setPisoLote(String pisoLote);

	/**
	 * Returns the interior of this direc estandar.
	 *
	 * @return the interior of this direc estandar
	 */
	@AutoEscape
	public String getInterior();

	/**
	 * Sets the interior of this direc estandar.
	 *
	 * @param interior the interior of this direc estandar
	 */
	public void setInterior(String interior);

	/**
	 * Returns the urbanizacion of this direc estandar.
	 *
	 * @return the urbanizacion of this direc estandar
	 */
	@AutoEscape
	public String getUrbanizacion();

	/**
	 * Sets the urbanizacion of this direc estandar.
	 *
	 * @param urbanizacion the urbanizacion of this direc estandar
	 */
	public void setUrbanizacion(String urbanizacion);

	/**
	 * Returns the referencia of this direc estandar.
	 *
	 * @return the referencia of this direc estandar
	 */
	@AutoEscape
	public String getReferencia();

	/**
	 * Sets the referencia of this direc estandar.
	 *
	 * @param referencia the referencia of this direc estandar
	 */
	public void setReferencia(String referencia);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(pe.com.ibk.pepper.model.DirecEstandar direcEstandar);

	@Override
	public int hashCode();

	@Override
	public CacheModel<pe.com.ibk.pepper.model.DirecEstandar> toCacheModel();

	@Override
	public pe.com.ibk.pepper.model.DirecEstandar toEscapedModel();

	@Override
	public pe.com.ibk.pepper.model.DirecEstandar toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}