
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DatosExpediente {

    private String codigoPromotor;
    private String codigoPuntoVenta;
    private String codigoCanal;
    private String codigoTienda;
    private String idExpediente;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DatosExpediente() {
    }

    /**
     * 
     * @param codigoTienda
     * @param codigoPromotor
     * @param codigoCanal
     * @param codigoPuntoVenta
     * @param idExpediente
     */
    public DatosExpediente(String codigoPromotor, String codigoPuntoVenta, String codigoCanal, String codigoTienda, String idExpediente) {
        super();
        this.codigoPromotor = codigoPromotor;
        this.codigoPuntoVenta = codigoPuntoVenta;
        this.codigoCanal = codigoCanal;
        this.codigoTienda = codigoTienda;
        this.idExpediente = idExpediente;
    }

    public String getCodigoPromotor() {
        return codigoPromotor;
    }

    public void setCodigoPromotor(String codigoPromotor) {
        this.codigoPromotor = codigoPromotor;
    }

    public String getCodigoPuntoVenta() {
        return codigoPuntoVenta;
    }

    public void setCodigoPuntoVenta(String codigoPuntoVenta) {
        this.codigoPuntoVenta = codigoPuntoVenta;
    }

    public String getCodigoCanal() {
        return codigoCanal;
    }

    public void setCodigoCanal(String codigoCanal) {
        this.codigoCanal = codigoCanal;
    }

    public String getCodigoTienda() {
        return codigoTienda;
    }

    public void setCodigoTienda(String codigoTienda) {
        this.codigoTienda = codigoTienda;
    }

	public String getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(String idExpediente) {
		this.idExpediente = idExpediente;
	}

	@Override
	public String toString() {
		return "DatosExpediente [codigoPromotor=" + codigoPromotor
				+ ", codigoPuntoVenta=" + codigoPuntoVenta + ", codigoCanal="
				+ codigoCanal + ", codigoTienda=" + codigoTienda
				+ ", idExpediente=" + idExpediente + "]";
	}

}
