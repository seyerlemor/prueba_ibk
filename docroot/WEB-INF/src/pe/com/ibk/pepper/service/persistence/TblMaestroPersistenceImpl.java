/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchTblMaestroException;
import pe.com.ibk.pepper.model.TblMaestro;
import pe.com.ibk.pepper.model.impl.TblMaestroImpl;
import pe.com.ibk.pepper.model.impl.TblMaestroModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the tbl maestro service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see TblMaestroPersistence
 * @see TblMaestroUtil
 * @generated
 */
public class TblMaestroPersistenceImpl extends BasePersistenceImpl<TblMaestro>
	implements TblMaestroPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TblMaestroUtil} to access the tbl maestro persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TblMaestroImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroModelImpl.FINDER_CACHE_ENABLED, TblMaestroImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroModelImpl.FINDER_CACHE_ENABLED, TblMaestroImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_M_H = new FinderPath(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroModelImpl.FINDER_CACHE_ENABLED, TblMaestroImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByM_H",
			new String[] { String.class.getName() },
			TblMaestroModelImpl.IDHASH_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_M_H = new FinderPath(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByM_H",
			new String[] { String.class.getName() });

	/**
	 * Returns the tbl maestro where idHash = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchTblMaestroException} if it could not be found.
	 *
	 * @param idHash the id hash
	 * @return the matching tbl maestro
	 * @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a matching tbl maestro could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro findByM_H(String idHash)
		throws NoSuchTblMaestroException, SystemException {
		TblMaestro tblMaestro = fetchByM_H(idHash);

		if (tblMaestro == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idHash=");
			msg.append(idHash);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTblMaestroException(msg.toString());
		}

		return tblMaestro;
	}

	/**
	 * Returns the tbl maestro where idHash = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idHash the id hash
	 * @return the matching tbl maestro, or <code>null</code> if a matching tbl maestro could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro fetchByM_H(String idHash) throws SystemException {
		return fetchByM_H(idHash, true);
	}

	/**
	 * Returns the tbl maestro where idHash = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idHash the id hash
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching tbl maestro, or <code>null</code> if a matching tbl maestro could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro fetchByM_H(String idHash, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idHash };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_M_H,
					finderArgs, this);
		}

		if (result instanceof TblMaestro) {
			TblMaestro tblMaestro = (TblMaestro)result;

			if (!Validator.equals(idHash, tblMaestro.getIdHash())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TBLMAESTRO_WHERE);

			boolean bindIdHash = false;

			if (idHash == null) {
				query.append(_FINDER_COLUMN_M_H_IDHASH_1);
			}
			else if (idHash.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_M_H_IDHASH_3);
			}
			else {
				bindIdHash = true;

				query.append(_FINDER_COLUMN_M_H_IDHASH_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdHash) {
					qPos.add(idHash);
				}

				List<TblMaestro> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_M_H,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"TblMaestroPersistenceImpl.fetchByM_H(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					TblMaestro tblMaestro = list.get(0);

					result = tblMaestro;

					cacheResult(tblMaestro);

					if ((tblMaestro.getIdHash() == null) ||
							!tblMaestro.getIdHash().equals(idHash)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_M_H,
							finderArgs, tblMaestro);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_M_H,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TblMaestro)result;
		}
	}

	/**
	 * Removes the tbl maestro where idHash = &#63; from the database.
	 *
	 * @param idHash the id hash
	 * @return the tbl maestro that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro removeByM_H(String idHash)
		throws NoSuchTblMaestroException, SystemException {
		TblMaestro tblMaestro = findByM_H(idHash);

		return remove(tblMaestro);
	}

	/**
	 * Returns the number of tbl maestros where idHash = &#63;.
	 *
	 * @param idHash the id hash
	 * @return the number of matching tbl maestros
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByM_H(String idHash) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_M_H;

		Object[] finderArgs = new Object[] { idHash };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TBLMAESTRO_WHERE);

			boolean bindIdHash = false;

			if (idHash == null) {
				query.append(_FINDER_COLUMN_M_H_IDHASH_1);
			}
			else if (idHash.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_M_H_IDHASH_3);
			}
			else {
				bindIdHash = true;

				query.append(_FINDER_COLUMN_M_H_IDHASH_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdHash) {
					qPos.add(idHash);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_M_H_IDHASH_1 = "tblMaestro.idHash IS NULL";
	private static final String _FINDER_COLUMN_M_H_IDHASH_2 = "tblMaestro.idHash = ?";
	private static final String _FINDER_COLUMN_M_H_IDHASH_3 = "(tblMaestro.idHash IS NULL OR tblMaestro.idHash = '')";

	public TblMaestroPersistenceImpl() {
		setModelClass(TblMaestro.class);
	}

	/**
	 * Caches the tbl maestro in the entity cache if it is enabled.
	 *
	 * @param tblMaestro the tbl maestro
	 */
	@Override
	public void cacheResult(TblMaestro tblMaestro) {
		EntityCacheUtil.putResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroImpl.class, tblMaestro.getPrimaryKey(), tblMaestro);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_M_H,
			new Object[] { tblMaestro.getIdHash() }, tblMaestro);

		tblMaestro.resetOriginalValues();
	}

	/**
	 * Caches the tbl maestros in the entity cache if it is enabled.
	 *
	 * @param tblMaestros the tbl maestros
	 */
	@Override
	public void cacheResult(List<TblMaestro> tblMaestros) {
		for (TblMaestro tblMaestro : tblMaestros) {
			if (EntityCacheUtil.getResult(
						TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
						TblMaestroImpl.class, tblMaestro.getPrimaryKey()) == null) {
				cacheResult(tblMaestro);
			}
			else {
				tblMaestro.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tbl maestros.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TblMaestroImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TblMaestroImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tbl maestro.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TblMaestro tblMaestro) {
		EntityCacheUtil.removeResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroImpl.class, tblMaestro.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(tblMaestro);
	}

	@Override
	public void clearCache(List<TblMaestro> tblMaestros) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TblMaestro tblMaestro : tblMaestros) {
			EntityCacheUtil.removeResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
				TblMaestroImpl.class, tblMaestro.getPrimaryKey());

			clearUniqueFindersCache(tblMaestro);
		}
	}

	protected void cacheUniqueFindersCache(TblMaestro tblMaestro) {
		if (tblMaestro.isNew()) {
			Object[] args = new Object[] { tblMaestro.getIdHash() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_M_H, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_M_H, args, tblMaestro);
		}
		else {
			TblMaestroModelImpl tblMaestroModelImpl = (TblMaestroModelImpl)tblMaestro;

			if ((tblMaestroModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_M_H.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { tblMaestro.getIdHash() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_M_H, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_M_H, args,
					tblMaestro);
			}
		}
	}

	protected void clearUniqueFindersCache(TblMaestro tblMaestro) {
		TblMaestroModelImpl tblMaestroModelImpl = (TblMaestroModelImpl)tblMaestro;

		Object[] args = new Object[] { tblMaestro.getIdHash() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_M_H, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_M_H, args);

		if ((tblMaestroModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_M_H.getColumnBitmask()) != 0) {
			args = new Object[] { tblMaestroModelImpl.getOriginalIdHash() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_M_H, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_M_H, args);
		}
	}

	/**
	 * Creates a new tbl maestro with the primary key. Does not add the tbl maestro to the database.
	 *
	 * @param idMaestro the primary key for the new tbl maestro
	 * @return the new tbl maestro
	 */
	@Override
	public TblMaestro create(int idMaestro) {
		TblMaestro tblMaestro = new TblMaestroImpl();

		tblMaestro.setNew(true);
		tblMaestro.setPrimaryKey(idMaestro);

		return tblMaestro;
	}

	/**
	 * Removes the tbl maestro with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idMaestro the primary key of the tbl maestro
	 * @return the tbl maestro that was removed
	 * @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a tbl maestro with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro remove(int idMaestro)
		throws NoSuchTblMaestroException, SystemException {
		return remove((Serializable)idMaestro);
	}

	/**
	 * Removes the tbl maestro with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tbl maestro
	 * @return the tbl maestro that was removed
	 * @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a tbl maestro with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro remove(Serializable primaryKey)
		throws NoSuchTblMaestroException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TblMaestro tblMaestro = (TblMaestro)session.get(TblMaestroImpl.class,
					primaryKey);

			if (tblMaestro == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTblMaestroException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tblMaestro);
		}
		catch (NoSuchTblMaestroException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TblMaestro removeImpl(TblMaestro tblMaestro)
		throws SystemException {
		tblMaestro = toUnwrappedModel(tblMaestro);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tblMaestro)) {
				tblMaestro = (TblMaestro)session.get(TblMaestroImpl.class,
						tblMaestro.getPrimaryKeyObj());
			}

			if (tblMaestro != null) {
				session.delete(tblMaestro);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tblMaestro != null) {
			clearCache(tblMaestro);
		}

		return tblMaestro;
	}

	@Override
	public TblMaestro updateImpl(pe.com.ibk.pepper.model.TblMaestro tblMaestro)
		throws SystemException {
		tblMaestro = toUnwrappedModel(tblMaestro);

		boolean isNew = tblMaestro.isNew();

		Session session = null;

		try {
			session = openSession();

			if (tblMaestro.isNew()) {
				session.save(tblMaestro);

				tblMaestro.setNew(false);
			}
			else {
				session.merge(tblMaestro);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TblMaestroModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
			TblMaestroImpl.class, tblMaestro.getPrimaryKey(), tblMaestro);

		clearUniqueFindersCache(tblMaestro);
		cacheUniqueFindersCache(tblMaestro);

		return tblMaestro;
	}

	protected TblMaestro toUnwrappedModel(TblMaestro tblMaestro) {
		if (tblMaestro instanceof TblMaestroImpl) {
			return tblMaestro;
		}

		TblMaestroImpl tblMaestroImpl = new TblMaestroImpl();

		tblMaestroImpl.setNew(tblMaestro.isNew());
		tblMaestroImpl.setPrimaryKey(tblMaestro.getPrimaryKey());

		tblMaestroImpl.setIdMaestro(tblMaestro.getIdMaestro());
		tblMaestroImpl.setNombreComercio(tblMaestro.getNombreComercio());
		tblMaestroImpl.setIdHash(tblMaestro.getIdHash());
		tblMaestroImpl.setTipoTarjeta(tblMaestro.getTipoTarjeta());
		tblMaestroImpl.setUrlImagen(tblMaestro.getUrlImagen());
		tblMaestroImpl.setTopeOtp(tblMaestro.getTopeOtp());
		tblMaestroImpl.setTopeEquifax(tblMaestro.getTopeEquifax());
		tblMaestroImpl.setCodigoSeguridad(tblMaestro.getCodigoSeguridad());

		return tblMaestroImpl;
	}

	/**
	 * Returns the tbl maestro with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tbl maestro
	 * @return the tbl maestro
	 * @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a tbl maestro with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTblMaestroException, SystemException {
		TblMaestro tblMaestro = fetchByPrimaryKey(primaryKey);

		if (tblMaestro == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTblMaestroException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return tblMaestro;
	}

	/**
	 * Returns the tbl maestro with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchTblMaestroException} if it could not be found.
	 *
	 * @param idMaestro the primary key of the tbl maestro
	 * @return the tbl maestro
	 * @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a tbl maestro with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro findByPrimaryKey(int idMaestro)
		throws NoSuchTblMaestroException, SystemException {
		return findByPrimaryKey((Serializable)idMaestro);
	}

	/**
	 * Returns the tbl maestro with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tbl maestro
	 * @return the tbl maestro, or <code>null</code> if a tbl maestro with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TblMaestro tblMaestro = (TblMaestro)EntityCacheUtil.getResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
				TblMaestroImpl.class, primaryKey);

		if (tblMaestro == _nullTblMaestro) {
			return null;
		}

		if (tblMaestro == null) {
			Session session = null;

			try {
				session = openSession();

				tblMaestro = (TblMaestro)session.get(TblMaestroImpl.class,
						primaryKey);

				if (tblMaestro != null) {
					cacheResult(tblMaestro);
				}
				else {
					EntityCacheUtil.putResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
						TblMaestroImpl.class, primaryKey, _nullTblMaestro);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TblMaestroModelImpl.ENTITY_CACHE_ENABLED,
					TblMaestroImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return tblMaestro;
	}

	/**
	 * Returns the tbl maestro with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idMaestro the primary key of the tbl maestro
	 * @return the tbl maestro, or <code>null</code> if a tbl maestro with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblMaestro fetchByPrimaryKey(int idMaestro)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idMaestro);
	}

	/**
	 * Returns all the tbl maestros.
	 *
	 * @return the tbl maestros
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblMaestro> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tbl maestros.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tbl maestros
	 * @param end the upper bound of the range of tbl maestros (not inclusive)
	 * @return the range of tbl maestros
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblMaestro> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tbl maestros.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tbl maestros
	 * @param end the upper bound of the range of tbl maestros (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tbl maestros
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblMaestro> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TblMaestro> list = (List<TblMaestro>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TBLMAESTRO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TBLMAESTRO;

				if (pagination) {
					sql = sql.concat(TblMaestroModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TblMaestro>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TblMaestro>(list);
				}
				else {
					list = (List<TblMaestro>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tbl maestros from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TblMaestro tblMaestro : findAll()) {
			remove(tblMaestro);
		}
	}

	/**
	 * Returns the number of tbl maestros.
	 *
	 * @return the number of tbl maestros
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TBLMAESTRO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the tbl maestro persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.TblMaestro")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TblMaestro>> listenersList = new ArrayList<ModelListener<TblMaestro>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TblMaestro>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TblMaestroImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TBLMAESTRO = "SELECT tblMaestro FROM TblMaestro tblMaestro";
	private static final String _SQL_SELECT_TBLMAESTRO_WHERE = "SELECT tblMaestro FROM TblMaestro tblMaestro WHERE ";
	private static final String _SQL_COUNT_TBLMAESTRO = "SELECT COUNT(tblMaestro) FROM TblMaestro tblMaestro";
	private static final String _SQL_COUNT_TBLMAESTRO_WHERE = "SELECT COUNT(tblMaestro) FROM TblMaestro tblMaestro WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tblMaestro.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TblMaestro exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TblMaestro exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TblMaestroPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idMaestro", "nombreComercio", "idHash", "tipoTarjeta",
				"urlImagen", "topeOtp", "topeEquifax", "codigoSeguridad"
			});
	private static TblMaestro _nullTblMaestro = new TblMaestroImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TblMaestro> toCacheModel() {
				return _nullTblMaestroCacheModel;
			}
		};

	private static CacheModel<TblMaestro> _nullTblMaestroCacheModel = new CacheModel<TblMaestro>() {
			@Override
			public TblMaestro toEntityModel() {
				return _nullTblMaestro;
			}
		};
}