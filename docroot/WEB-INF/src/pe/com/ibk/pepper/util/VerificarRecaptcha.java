package pe.com.ibk.pepper.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.HttpMethod;

import pe.com.ibk.pepper.bean.ResponseWSBean;
import pe.com.ibk.pepper.exceptions.ValidationException;

public class VerificarRecaptcha {
	
	private static final Log _log = LogFactoryUtil.getLog(VerificarRecaptcha.class);
	
	private VerificarRecaptcha(){}
	
    public static boolean verificar(String gRecaptchaResponse) throws ValidationException {
        Boolean success = Boolean.FALSE;
        try {
        	if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
        		throw new ValidationException("Intenta ingresar a un dato no autorizado");
            }
        	ObjectMapper mapper = new ObjectMapper();
        	String url = PrefsPropsUtil.getString(PortalKeys.REST_VAL_RECAPTCHA_ENDPOINT, PortalValues.REST_VAL_RECAPTCHA_ENDPOINT);
        	String secreto = PrefsPropsUtil.getString(PortalKeys.REST_VAL_RECAPTCHA_SECRET, PortalValues.REST_VAL_RECAPTCHA_SECRET);
        	URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod(HttpMethod.POST);
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            String postParams = "secret=" + secreto + "&response=" + gRecaptchaResponse;
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder respuesta = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                respuesta.append(inputLine);
            }
            in.close();
            ResponseWSBean object = mapper.readValue(respuesta.toString(), ResponseWSBean.class);
            if(!object.getSuccess())
            	throw new ValidationException("Intenta ingresar a un dato no autorizado");
            success = object.getSuccess();
        } catch (ValidationException e) {
        	throw e;
		} catch (IOException | SystemException e) {
			_log.error(e);
		}
        
        return success;
    }
}
