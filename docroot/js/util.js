
function mostrarEsCliente(esCliente){
	if(parseInt(esCliente) == 1){
		return "Si";
	}else{
		return "No";
	}
}

function completaDigit(str){
	str=str+"";
	if(str!=null){
		if(str.length>0){
			if(str.length==1){
				str = "0"+str;
				return str;
			}else
				return str;
		}
	}
	return null;	
}

function tipoDocumento(idtipoDocumento){
	if(parseInt(idtipoDocumento) == "1"){
		return "DNI";
	}else
	if(parseInt(idtipoDocumento) == "2"){
		return "CARN\u00c9 DE EXTRANJER\u00cdA";
	}	
	return "";
}

var anno = null, mes = null, dia = null,  hora = null, minuto = null, segund = null;

function formatofecha2(fecha){
	anno = fecha.substring(0,4);
	mes = fecha.substring(5,7)-1;
	dia = fecha.substring(8,10);		
	var d = new Date(anno, mes, dia, 0, 0, 0, 0);
	fecha = completaDigit( d.getDate() ) + "-"  + completaDigit( (d.getMonth()+1) ) + "-" + d.getFullYear()  ;
	return fecha;
}

function formatofecha(fecha){
	anno = fecha.substring(0,4);
	mes = (fecha.substring(5,7)-1);
	dia = fecha.substring(8,10);
	hora = fecha.substring(11,13);
	minuto = fecha.substring(14,16);
	segund = fecha.substring(17,19);	
	var d = new Date(anno, mes, dia, hora, minuto, segund, 0);
	fecha = completaDigit( d.getDate() ) + "-"  + completaDigit( (d.getMonth()+1) ) + "-" + d.getFullYear() + " "  + completaDigit( (d.getHours()) ) + ":"  + completaDigit( (d.getMinutes()) )+ ":"  + completaDigit( (d.getSeconds()) ) ;
	return fecha;
}

function isNull(obj) {
	if (obj == null || obj == undefined) {
		return true;
	}
	return false;
}

function getObject(id) {
	return $(id)[0];
}

function completeFechaDigit(str){
	str=str+"";
	if(str!=null){
		if(str.length>0){
			if(str.length==1){
				str = "0"+str;
				return str;
			}else
				return str;
		}
	}
	return null;
}

/**
 * Se obtiene la extencion del archivo ingresado
 * 
 * @param file
 * @returns
 */
function getExt(file) {
	return (/[.]/.exec(file)) ? /[^.]+$/.exec(file.toLowerCase()) : '';
}

function mostrarError(id){
	var cantRow = $("#msjRespuesta");
	cantRow.removeClass("portlet-msg-error");
	cantRow.removeClass("portlet-msg-info");
	cantRow.addClass("portlet-msg-error");
	cantRow.html("<i class='icon-remove-sign'></i><span>&nbsp;&nbsp;</span>"+ msjerrorFechas[id] );
}

function validarformulario(f1,f2){
	try {
		if(f1 != null && f2!=null){
			
							
				if(f1>(new Date())){
					mostrarError("MENSAJE_ERROR_FECHAS_DESDE_MAYOR");
					return false;
				}			
		
			
				if(f2>(new Date())){
					mostrarError("MENSAJE_ERROR_FECHAS_HASTA_MAYOR_FECHA_ACTUAL");
					return false;
				}
				
		
				if(f2<f1){
					mostrarError("MENSAJE_ERROR_FECHAS_HASTA_MENOR_DESDE");
					return false;
				}			
	
			
			
			if (f2>=f1) {
				return true;
			} 
			
		}				
	} catch (e){

	}	
	return false;
}