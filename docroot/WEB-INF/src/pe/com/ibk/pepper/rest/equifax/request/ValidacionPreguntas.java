
package pe.com.ibk.pepper.rest.equifax.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValidacionPreguntas {

    private PreguntasVal preguntas;
    private InformacionAdicional informacionAdicional;
    
    @JsonCreator
	public ValidacionPreguntas(PreguntasVal preguntas,
			InformacionAdicional informacionAdicional) {
		super();
		this.preguntas = preguntas;
		this.informacionAdicional = informacionAdicional;
	}

    @JsonProperty("preguntas")
	public PreguntasVal getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(PreguntasVal preguntas) {
        this.preguntas = preguntas;
    }

    @JsonProperty("informacionAdicional")
	public InformacionAdicional getInformacionAdicional() {
		return informacionAdicional;
	}

	public void setInformacionAdicional(InformacionAdicional informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

}
