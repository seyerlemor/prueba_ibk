package pe.com.ibk.pepper.formulario.tarjeta.controller;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletPreferences;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import pe.com.ibk.pepper.bean.ClienteBean;
import pe.com.ibk.pepper.bean.ComercioBean;
import pe.com.ibk.pepper.bean.ConfigurarTarjeta;
import pe.com.ibk.pepper.bean.EquifaxBean;
import pe.com.ibk.pepper.bean.ExpedienteBean;
import pe.com.ibk.pepper.bean.FHEntregaBean;
import pe.com.ibk.pepper.bean.FormConyugeBean;
import pe.com.ibk.pepper.bean.JsonErrorLiquidBean;
import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.controller.SolicitudController;
import pe.com.ibk.pepper.exceptions.ValidationException;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.rest.equifax.request.PreguntasVal;
import pe.com.ibk.pepper.rest.equifax.request.PreguntumVal;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Direccion;
import pe.com.ibk.pepper.rest.service.GestorRegistroService;
import pe.com.ibk.pepper.rest.service.GestorServiciosRestService;
import pe.com.ibk.pepper.util.Constantes;
import pe.com.ibk.pepper.util.GsonUtil;
import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;
import pe.com.ibk.pepper.util.RequestParam;
import pe.com.ibk.pepper.util.ResponseMessage;
import pe.com.ibk.pepper.util.SessionKeys;
import pe.com.ibk.pepper.util.UbigeoUtil;
import pe.com.ibk.pepper.util.Utils;
import pe.com.ibk.pepper.util.Validador;
import pe.com.ibk.pepper.util.VerificarRecaptcha;

@Controller
@RequestMapping("view")
public class FormularioTarjetaController extends SolicitudController {

	private static final Log _log = LogFactoryUtil.getLog(FormularioTarjetaController.class);
	
	@Autowired
	private GestorServiciosRestService gestorService;
	@Autowired
	private GestorRegistroService gestorRegistroService;
	
	@ResourceMapping("validarInicioSolicitud")
	public void validarInicioSolicitud(ResourceRequest request,ResourceResponse response,@ModelAttribute ClienteBean clienteBean) throws IOException {
		Map<String, Object> mapResultado = new HashMap<>();
		Boolean isCampania = null;
		String contentHTML = StringPool.BLANK;
		SolicitudBean solicitudBean = new SolicitudBean();
		try {
			Validador.isValidJson(clienteBean);
			Validador.isValidJson(request.getParameter(RequestParam.RECATPCHA_RESPONSE));
			Validador.isValidJson(ParamUtil.getString(request, RequestParam.CHK_PROTECCION_DATOS));
			VerificarRecaptcha.verificar(request.getParameter(RequestParam.RECATPCHA_RESPONSE));
			solicitudBean = setearDatosSolicitud(request, clienteBean, ParamUtil.getString(request, RequestParam.CHK_PROTECCION_DATOS), Constantes.TIPO_FLUJO_VENTA_TC);
			PortletPreferences preferences = request.getPreferences();
			if(Constantes.PEPPER_TYPE_ONLINE.equals(solicitudBean.getExpedienteBean().getPepperType()))
				solicitudBean = gestorService.validarAntiFraude(solicitudBean, preferences.getValue(RequestParam.ENDPOINT_ANTIFRAUDE,StringPool.BLANK), preferences.getValue(RequestParam.PATH_ANTIFRAUDE,StringPool.BLANK));
			if(Constantes.TIPO_API.equals(preferences.getValue(RequestParam.TIPO_CAMPANIA,StringPool.BLANK)))
				solicitudBean = gestorService.getDocumentLead(solicitudBean, preferences.getValue(RequestParam.PATH_DOCUMENT_LEAD,StringPool.BLANK), preferences.getValue(RequestParam.PATH_API_LEAD,StringPool.BLANK), request);
			else
				solicitudBean = gestorService.consultarCampania(solicitudBean, request, preferences.getValue(RequestParam.PATH_CAMPANIA,StringPool.BLANK));
			isCampania = Validator.isNotNull(solicitudBean.getExpedienteBean().getFlagCampania())?solicitudBean.getExpedienteBean().getFlagCampania():Boolean.FALSE;
			if(isCampania){ 
				solicitudBean = gestorService.obtenerApiInformacionPersona(solicitudBean, preferences.getValue(RequestParam.PATH_OBT_INFO_PERSONA,StringPool.BLANK));
				solicitudBean = validarServiciosEnCampania(solicitudBean, request);
				contentHTML = solicitudBean.getExpedienteBean().getHtmlCode();
			}else
				contentHTML = Constantes.URL_NO_CAMPANIA;
		}catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(Utils.getWebserviceExpectionError(e), e);
		} catch (ValidationException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			solicitudBean.getExpedienteBean().setPaso(Constantes.PASO_2);
			solicitudBean.getExpedienteBean().setFlagCampania(isCampania);
			solicitudBean.getExpedienteBean().setHtmlCode(!StringPool.BLANK.equalsIgnoreCase(contentHTML)?contentHTML:Constantes.HTML_ERROR);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBean));
			addSessionUser(solicitudBean, request);
			gestorRegistroService.registrarExpediente(solicitudBean);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}
	
	@ResourceMapping("accessStep2")
	public void accessStep2(ResourceRequest request,ResourceResponse response) throws IOException {
		SolicitudBean solicitudBeanSession = new SolicitudBean();
		Map<String, Object> mapResultado = new HashMap<>();
		String contentHTML = Constantes.HTML_ERROR;
		try {
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			contentHTML = Constantes.URL_EQUIFAX;
			gestorRegistroService.registrarExpediente(solicitudBeanSession);
		} catch (Exception e) {
			contentHTML = Constantes.HTML_ERROR_URL;
			_log.error(e);
		}finally{
			solicitudBeanSession.getExpedienteBean().setPaso(Constantes.PASO_3);
			addSessionUser(solicitudBeanSession, request);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBeanSession));
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}

	@ResourceMapping("validateEquifax")
	public void validateEquifax(ResourceRequest request,ResourceResponse response, EquifaxBean equifaxBean) throws IOException {
		Map<String, Object> mapResultado = new HashMap<>();
		String resultadoEvaluacion = StringPool.BLANK;
		String paso = Constantes.PASO_4;
		SolicitudBean solicitudBeanSession = new SolicitudBean();
		Boolean flagPaso2 = Boolean.FALSE;
		String contentHTML = StringPool.BLANK;
		try {
			PortletPreferences preferences = request.getPreferences();
			Validador.isValidJson(equifaxBean);
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso,SolicitudBean.class);
			solicitudBeanSession.getEquifaxBean().setNroOperacion(solicitudBeanSession.getClienteBean().getNroOperacion());
			List<PreguntumVal> lstPreguntasVal = new ArrayList<>();
			int cantidadPreguntas = Validator.isNotNull(ParamUtil.getInteger(request, "cantidadPreguntas"))?ParamUtil.getInteger(request, "cantidadPreguntas"):0;
			Validador.isValidJson(cantidadPreguntas);
			PreguntasVal pregVal = new PreguntasVal();
			String pregArray = StringPool.BLANK;
			for(int i=1;i<=cantidadPreguntas;i++){
				pregArray = ParamUtil.getString(request, "efx"+i);
				Validador.isValidJson(pregArray);
				String[] rptArray = pregArray.split("\\.");
				PreguntumVal preguntasValBean = new PreguntumVal();
				preguntasValBean.setNumeroOpcion(rptArray[0]);
				preguntasValBean.setNumeroPregunta(rptArray[1]);
				preguntasValBean.setCategoriaPregunta(rptArray[2]);
				lstPreguntasVal.add(preguntasValBean);
			}
			pregVal.setPregunta(lstPreguntasVal);
			solicitudBeanSession.getEquifaxBean().setPreguntasVal(pregVal);
			solicitudBeanSession = gestorService.validarPreguntasEquifax(solicitudBeanSession, preferences.getValue(RequestParam.PATH_VALIDAR_PREGUNTAS,StringPool.BLANK));
			resultadoEvaluacion = solicitudBeanSession.getEquifaxBean().getResultadoEquifax();
			if (resultadoEvaluacion.equals(Constantes.EQUIFAX_RESULTADO_APROBADO)) {
				if(Constantes.TIPO_FLUJO_VENTA_TC.equals(solicitudBeanSession.getExpedienteBean().getTipoFlujo())){
					contentHTML = Constantes.URL_CALIFICACION;
					solicitudBeanSession.getEquifaxBean().setResultadoEquifax(Constantes.RESULTADO_APROBADO);
					solicitudBeanSession.getExpedienteBean().setFlagValidarEquifax(Boolean.parseBoolean(Constantes.FLAG_TRUE));
					flagPaso2 = Boolean.TRUE;
					solicitudBeanSession = validateCDA(solicitudBeanSession, request);
					if(Validator.isNotNull(solicitudBeanSession.getExpedienteBean().getExpedienteCDA()) && !StringPool.BLANK.equals(solicitudBeanSession.getExpedienteBean().getExpedienteCDA()) && !solicitudBeanSession.getClienteBean().getDirecciones().getDireccion().isEmpty()){
						contentHTML = Constantes.URL_CONF_TC;
						paso = Constantes.PASO_5;
					}
					String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
					ComercioBean comercioBean = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
					solicitudBeanSession.getProductoBean().setProvisionalLinePepper(comercioBean.getEquifaxMax());
					solicitudBeanSession.getExpedienteBean().setTipoAutenticacion(Constantes.AUTENTICACION_EQUIFAX);
				}else{
					solicitudBeanSession = gestorService.consultarPOTC(solicitudBeanSession);
					solicitudBeanSession = gestorService.generarPOTC(solicitudBeanSession);
					if(solicitudBeanSession.getOtcBean().getAceptado()) 
						contentHTML = Constantes.URL_EXITO;
				}
			} else {
				contentHTML = Constantes.HTML_EQUIFAX_DESAPROBADO;
				solicitudBeanSession.getEquifaxBean().setResultadoEquifax(Constantes.RESULTADO_DESAPROBADO);
				solicitudBeanSession.getExpedienteBean().setFlagValidarEquifax(Boolean.parseBoolean(Constantes.FLAG_FALSE));
			}
			solicitudBeanSession.getEquifaxBean().setNroIntento(0);	
			gestorRegistroService.registrarExpediente(solicitudBeanSession);
		} catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(Utils.getWebserviceExpectionError(e),e);
		} catch (ValidationException | NumberFormatException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}
		finally{
			solicitudBeanSession.getExpedienteBean().setPaso(paso);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBeanSession));
			addSessionUser(solicitudBeanSession, request);
			SessionKeys.addSessionPortal(request, SessionKeys.FLAG_PASO_2, flagPaso2);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}
	
	@ResourceMapping("getDataConyuge")
	public void getDataConyuge(ResourceRequest request,ResourceResponse response) throws IOException { 
		SolicitudBean solicitudBeanSession = null;
		JSONObject respuesta = JSONFactoryUtil.createJSONObject();
		try {
			String codigo=GetterUtil.getString(ParamUtil.getString(request,Constantes.NAME_VA_REQ_VALOR));
			SolicitudBean solicitudBean = new SolicitudBean();
			Validador.isValidJson(codigo);
			solicitudBean.getExpedienteBean().setDniNumber(codigo);
			solicitudBean.getClienteBean().setDniNumber(codigo);
			PortletPreferences preferences = request.getPreferences();
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			solicitudBean.getExpedienteBean().setMessageId(solicitudBeanSession.getExpedienteBean().getMessageId());
			solicitudBean.getExpedienteBean().setIdExpediente(solicitudBeanSession.getExpedienteBean().getIdExpediente());
			String documentoTitular = solicitudBeanSession.getExpedienteBean().getDniNumber();
			if(!documentoTitular.equals(solicitudBean.getExpedienteBean().getDniNumber())){
				solicitudBean = gestorService.obtenerApiInformacionPersona(solicitudBean, preferences.getValue(RequestParam.PATH_OBT_INFO_PERSONA,StringPool.BLANK));
				if (Validator.isNotNull(solicitudBean.getClienteBean().getDniNumber())) 
					solicitudBeanSession.getClienteBean().setDocumentoConyuge(solicitudBean.getClienteBean().getDniNumber());
				FormConyugeBean formConyuge	= Utils.setSpouseData(solicitudBean);
				if (Validator.isNotNull(formConyuge.getFirstname()) && StringPool.BLANK.equals(formConyuge.getFirstname())) 
					respuesta = JSONFactoryUtil.createJSONObject(GsonUtil.convertToJSON(new JsonErrorLiquidBean(2, Constantes.MSG_ERROR_JS_SOL_DAT_PER)).toString());
				else{
					respuesta.put(RequestParam.CODE, 1);
					respuesta.put(RequestParam.DATA,JSONFactoryUtil.createJSONObject(
							GsonUtil.convertToJSON(formConyuge).toString()));
				}
			}else
				respuesta = JSONFactoryUtil.createJSONObject(GsonUtil.convertToJSON(new JsonErrorLiquidBean(2, Constantes.MSG_ERROR_SAME_DOC)).toString());
		}catch (WebServicesException e) {
			_log.error(Utils.getWebserviceExpectionError(e), e);
		} catch (ValidationException | JSONException e) {
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			addSessionUser(solicitudBeanSession, request);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(respuesta));
		}
	}

	@ResourceMapping("resultadoTC")
	public void resultadoTC(ResourceRequest request,ResourceResponse response) throws IOException {
		SolicitudBean solicitudBeanSession = new SolicitudBean();
		Map<String, Object> mapResultado = new HashMap<>();
		String contentHTML = Constantes.HTML_ERROR;
		try {
			String idBrand = ParamUtil.getString(request, RequestParam.ID_BRAND);
			Validador.isValidJson(idBrand);
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			solicitudBeanSession = Utils.loadOffertoStep(solicitudBeanSession, idBrand);
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			if(themeDisplay.isSignedIn())
				contentHTML = metodoAutenticacionFisico(solicitudBeanSession);
			else
				contentHTML = metodoAutenticacion(solicitudBeanSession, request);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
		} catch (ValidationException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			solicitudBeanSession.getExpedienteBean().setPaso(Constantes.PASO_3);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
			gestorRegistroService.registrarExpediente(solicitudBeanSession);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBeanSession));
			addSessionUser(solicitudBeanSession, request);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}

	@ResourceMapping("generateToken")
	public void generateToken(ResourceRequest request,ResourceResponse response) throws IOException {
		SolicitudBean solicitudBeanSession = null;
		JSONObject json = JSONFactoryUtil.createJSONObject();
		try {
			/**/
			PortletPreferences preferences = request.getPreferences();
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			solicitudBeanSession = gestorService.generarToken(solicitudBeanSession, preferences.getValue(RequestParam.PATH_GENERAR_TOKEN,StringPool.BLANK));
			json.put(RequestParam.CODE, ResponseMessage.SUCCESS.getCode());
		} catch (WebServicesException e) {
			_log.error(Utils.getWebserviceExpectionError(e), e);
		}finally{
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(json));
			addSessionUser(solicitudBeanSession, request);
		}
	}

	@ResourceMapping("validarCalificacionV3")
	public void validarCalificacionV3(ResourceRequest request,ResourceResponse response, @ModelAttribute ClienteBean clienteBean) throws IOException {
		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setDniNumber(clienteBean.getDniNumber());
		SolicitudBean pasoSession = new SolicitudBean();
		Map<String, Object> mapResultado = new HashMap<>();
		String contentHTML = Constantes.HTML_ERROR;
		try {
			Validador.isValidJson(clienteBean);
			PortletPreferences preferences = request.getPreferences();
			pasoSession = JSONFactoryUtil.looseDeserialize(GetterUtil.getString(SessionKeys.getSessionPortal(request,SessionKeys.PASO_BEAN)), SolicitudBean.class);
			pasoSession = Utils.setClientData(pasoSession, clienteBean);
			if(Validator.isNull(pasoSession.getExpedienteBean().getFlagCalificacion()))
				pasoSession = gestorService.evaluarCalificacion(pasoSession, preferences.getValue(RequestParam.ENDPOINT_CALIFICACION,StringPool.BLANK), preferences.getValue(RequestParam.PATH_CALIFICACION,StringPool.BLANK));
			contentHTML = pasoSession.getExpedienteBean().getFlagCalificacion()?Constantes.URL_CONF_TC:Constantes.HTML_NO_CALIFICA;
			gestorRegistroService.registrarExpediente(pasoSession);
		}
		catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(Utils.getWebserviceExpectionError(e), e);
		} catch (ValidationException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			pasoSession.getExpedienteBean().setPaso(Constantes.PASO_5);
			pasoSession.getExpedienteBean().setHtmlCode(contentHTML);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(pasoSession));
			addSessionUser(pasoSession, request);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}
	
	@ResourceMapping("enviarCodigoOtp")
	public void enviarCodigoOtp(ResourceRequest request,ResourceResponse response, @ModelAttribute ClienteBean clienteBean) throws IOException {
		Map<String, Object> mapResultado = new HashMap<>();
		SolicitudBean solicitudBeanSession = new SolicitudBean();
		String contentHTML = Constantes.HTML_ERROR;
		try {
			PortletPreferences preferences = request.getPreferences();
			Validador.isValidJson(clienteBean);
			String optOption = Validator.isNotNull(ParamUtil.getString(request, RequestParam.RADIO_OTP))&&!StringPool.BLANK.equalsIgnoreCase(ParamUtil.getString(request, RequestParam.RADIO_OTP))?ParamUtil.getString(request, RequestParam.RADIO_OTP):Constantes.TYPE_OTP_MULTIPLE;
			String optOptionEmail = Validator.isNotNull(ParamUtil.getString(request, RequestParam.CHK_SEND_EMAIL))?Constantes.TYPE_OTP_MULTIPLE:StringPool.BLANK;
			Validador.isValidJson(optOption);
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			solicitudBeanSession.getClienteBean().setCellPhoneOperator(clienteBean.getCellPhoneOperator());
			optOption = !StringPool.BLANK.equalsIgnoreCase(optOptionEmail)?optOptionEmail:optOption;
			solicitudBeanSession.getProductoBean().setTipoSeguro(optOption);
			solicitudBeanSession = gestorService.generarToken(solicitudBeanSession, preferences.getValue(RequestParam.PATH_GENERAR_TOKEN,StringPool.BLANK));
			contentHTML = Constantes.URL_OTP_VALIDACION;
		} catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(Utils.getWebserviceExpectionError(e), e);
		} catch (ValidationException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			solicitudBeanSession.getExpedienteBean().setPaso(Constantes.PASO_3);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBeanSession));
			addSessionUser(solicitudBeanSession, request);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}
	
	@ResourceMapping("validarCodigoOtp")
	public void validarCodigoOtp(ResourceRequest request,ResourceResponse response) throws IOException {
		Map<String, Object> mapResultado = new HashMap<>();
		SolicitudBean solicitudBeanSession = new SolicitudBean();
		String contentHTML = Constantes.HTML_ERROR;
		String paso = Constantes.PASO_4;
		try {
			String codigoSms = ParamUtil.getString(request, "validationCode");
			PortletPreferences preferences = request.getPreferences();
			Validador.isValidJson(codigoSms);
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			solicitudBeanSession.getProductoBean().setCodigoSms(codigoSms);
			solicitudBeanSession = gestorService.validarToken(solicitudBeanSession, preferences.getValue(RequestParam.ENDPOINT_VALIDAR_TOKEN,StringPool.BLANK), preferences.getValue(RequestParam.PATH_VALIDAR_TOKEN,StringPool.BLANK));
			if(solicitudBeanSession.getOtcBean().getValidado()){
				if(Constantes.TIPO_FLUJO_VENTA_TC.equals(solicitudBeanSession.getExpedienteBean().getTipoFlujo())){
					contentHTML = Constantes.URL_CALIFICACION;
					solicitudBeanSession = validateCDA(solicitudBeanSession, request);
					if(Validator.isNotNull(solicitudBeanSession.getExpedienteBean().getExpedienteCDA()) && !StringPool.BLANK.equals(solicitudBeanSession.getExpedienteBean().getExpedienteCDA()) && Validator.isNotNull(solicitudBeanSession.getClienteBean().getDirecciones().getDireccion()) && !solicitudBeanSession.getClienteBean().getDirecciones().getDireccion().isEmpty()){
						contentHTML = Constantes.URL_CONF_TC;
						paso = Constantes.PASO_5;
					}else if(Validator.isNotNull(solicitudBeanSession.getExpedienteBean().getFlagCalificacion()) && !solicitudBeanSession.getExpedienteBean().getFlagCalificacion())
						contentHTML = Constantes.HTML_NO_CALIFICA;
					String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
					ComercioBean comercioBean = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
					solicitudBeanSession.getProductoBean().setProvisionalLinePepper(comercioBean.getOptMax());
					solicitudBeanSession.getExpedienteBean().setTipoAutenticacion(Constantes.AUTENTICACION_OTP);
				}else{
					solicitudBeanSession = gestorService.consultarPOTC(solicitudBeanSession);
					solicitudBeanSession = gestorService.generarPOTC(solicitudBeanSession);
					if(solicitudBeanSession.getOtcBean().getAceptado()){
						contentHTML = Constantes.URL_POTC;
						paso = Constantes.PASO_6;
					}
				}
				
			}else
				contentHTML = Constantes.HTML_TOKEN_INVALIDO;
			gestorRegistroService.registrarExpediente(solicitudBeanSession);
		} catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(Utils.getWebserviceExpectionError(e), e);
		} catch (ValidationException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			solicitudBeanSession.getExpedienteBean().setPaso(paso);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBeanSession));
			addSessionUser(solicitudBeanSession, request);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}

	@ResourceMapping("addAddress")
	protected void addAddress(ResourceRequest request, ResourceResponse response, @ModelAttribute ConfigurarTarjeta configurarTarjeta){
		try {
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			SolicitudBean solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			PortletPreferences preferences = request.getPreferences();
			Validador.isValidJson(configurarTarjeta);
			Direccion direccionEntrega = Utils.addDeliveryAddress(configurarTarjeta);
			Boolean flagFechaHora3G = Constantes.TIPO_3G.equals(preferences.getValue(RequestParam.TIPO_FECHA_HORA,StringPool.BLANK))?Boolean.TRUE:Boolean.FALSE;
			if(flagFechaHora3G)
				solicitudBeanSession = gestorService.consultarFechaHoraApi3G(solicitudBeanSession, direccionEntrega.getUbigeo(), preferences.getValue(RequestParam.PATH_FECHA_HORA,StringPool.BLANK));
			else
				solicitudBeanSession = gestorService.consultarFechaHoraApi2G(solicitudBeanSession, direccionEntrega, preferences.getValue(RequestParam.PATH_FECHA_HORA,StringPool.BLANK));
			JSONObject json = Utils.loadDeliveryAddress(solicitudBeanSession, flagFechaHora3G, direccionEntrega);
			solicitudBeanSession.getClienteBean().getDirecciones().getDireccion().add(direccionEntrega);
			writeJSON(request, response, json);
			solicitudBeanSession.setFhEntregaBean(new FHEntregaBean());
			addSessionUser(solicitudBeanSession, request);
		} catch (WebServicesException e) {
			_log.error(Utils.getWebserviceExpectionError(e), e);
		} catch (ValidationException | IOException e) {
			_log.error("Error en ValidationException | SystemException | IOException", e);
		}
		
	}
	
	@ResourceMapping("getUbigeo")
	protected void getListaUbigeoxParam(ResourceRequest request, ResourceResponse response){
		String codigo=ParamUtil.getString(request,RequestParam.PARENT);
		String codProvincia=Constantes.DEFAULT_COD_UBIGEO;
		String codDepartamento;
		if(ParamUtil.getString(request,RequestParam.TIPO).equals(Constantes.COD_TIPO_BUSQUEDA_PROVINCIA)){
			codDepartamento=codigo.substring(0,2);
			codProvincia=codigo.substring(2,4);
		} else 
			codDepartamento=codigo;
		try {
			JSONObject respuesta = JSONFactoryUtil.createJSONObject();
			respuesta.put(RequestParam.DATA,JSONFactoryUtil.createJSONArray(GsonUtil.convertToJSON(
					UbigeoUtil.getUbigeosJson(codDepartamento,codProvincia,Constantes.DEFAULT_COD_UBIGEO)).toString()));
			writeJSON(request, response, respuesta);
		} catch (Exception e) {
			_log.error(e);
		}
	}
	
	@ResourceMapping("altaTc")
	public void altaTc(ResourceRequest request,ResourceResponse response, @ModelAttribute ConfigurarTarjeta configurarTarjeta) throws IOException {
		Map<String, Object> mapResultado = new HashMap<>();
		SolicitudBean solicitudBeanSession = new SolicitudBean();
		String contentHTML = Constantes.HTML_ERROR;
		try {
			Validador.isValidJson(configurarTarjeta);
			String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
			ComercioBean comercioBean = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
			int indice = Integer.parseInt(configurarTarjeta.getDeliveryaddress());
			String jsonPaso = GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.PASO_BEAN));
			solicitudBeanSession = JSONFactoryUtil.looseDeserialize(jsonPaso, SolicitudBean.class);
			String fechaPago = request.getPreferences().getValue(RequestParam.REST_VENTA_TC_FECHA_PAGO, PrefsPropsUtil.getString(PortalKeys.REST_VENTA_TC_PROVISIONAL_FECHA_PAGO, PortalValues.REST_VENTA_TC_PROVISIONAL_FECHA_PAGO));
			solicitudBeanSession.getProductoBean().setDiaDePago(fechaPago);
			solicitudBeanSession.getProductoBean().setFlagEnvioEECCEmail(Constantes.FLAG_TRUE);
			solicitudBeanSession.getProductoBean().setEmailStateAccount(configurarTarjeta.getEmailStateAccount().toUpperCase());
			solicitudBeanSession.getProductoBean().setFlagEnvioEECCFisico(Constantes.FLAG_FALSE);
			solicitudBeanSession.getProductoBean().setLineaCredito(Validator.isNotNull(configurarTarjeta.getCreditLine())&&!StringPool.BLANK.equalsIgnoreCase(configurarTarjeta.getCreditLine())?configurarTarjeta.getCreditLine():solicitudBeanSession.getProductoBean().getLineaCredito());
			if(Validator.isNotNull(configurarTarjeta.getDeliverydate()) && !StringPool.BLANK.equalsIgnoreCase(configurarTarjeta.getDeliverydate())){
				String[] listDates = configurarTarjeta.getDeliverydate().split(StringPool.AT);
				solicitudBeanSession.getProductoBean().setDateDelivery(listDates[0]);
				solicitudBeanSession.getProductoBean().setInitTime(listDates[1]);
				solicitudBeanSession.getProductoBean().setFinalTime(listDates[2]);
			}
			solicitudBeanSession = gestorService.altaTc(solicitudBeanSession, indice, comercioBean.getPepperOnlineType(), request);
			if(Constantes.PEPPER_TYPE_PHYSICAL.equalsIgnoreCase(solicitudBeanSession.getExpedienteBean().getPepperType()))
				contentHTML = Constantes.URL_EXITO;
			else{
				solicitudBeanSession = Utils.verDatosTarjeta(solicitudBeanSession, comercioBean, request);
				contentHTML = Constantes.URL_FINAL;
			}
		} catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(e);
		} catch (ValidationException | SystemException e) {
			contentHTML = Constantes.HTML_ERROR_VALIDACION;
			_log.error(Constantes.TEXT_VALIDATION_ERROR, e);
		}finally{
			solicitudBeanSession.getExpedienteBean().setPaso(Constantes.PASO_6);
			gestorRegistroService.registrarExpediente(solicitudBeanSession);
			gestorRegistroService.registrarProducto(solicitudBeanSession);
			gestorRegistroService.registrarCliente(solicitudBeanSession);
			solicitudBeanSession.getExpedienteBean().setHtmlCode(contentHTML);
			addSessionUser(solicitudBeanSession, request);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBeanSession));
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}

	@ResourceMapping("validarProvisional")
	public void validarProvisional(ResourceRequest request,ResourceResponse response,@ModelAttribute ExpedienteBean expedienteBean) throws IOException {
		Map<String, Object> mapResultado = new HashMap<>();
		String contentHTML = Constantes.HTML_ERROR;
		SolicitudBean solicitudBean = new SolicitudBean();
		String paso = Constantes.PASO_3;
		try {
			PortletPreferences preferences = request.getPreferences();
			solicitudBean.getClienteBean().setDniNumber(expedienteBean.getDniNumber());
			solicitudBean = setearDatosSolicitud(request, solicitudBean.getClienteBean(), ParamUtil.getString(request, RequestParam.CHK_PROTECCION_DATOS), expedienteBean.getTipoFlujo());
			solicitudBean = gestorService.obtenerApiInformacionPersona(solicitudBean, preferences.getValue(RequestParam.PATH_OBT_INFO_PERSONA,StringPool.BLANK));
			solicitudBean.getExpedienteBean().setPaso(Constantes.PASO_2);
			if(Utils.validateBlankAndNull(solicitudBean.getClienteBean().getCodigoUnico())){
				solicitudBean.getClienteBean().setCodigoUnico(solicitudBean.getClienteBean().getCodigoUnico().substring(4));
				solicitudBean = gestorService.consultarProvisional(solicitudBean);
				contentHTML = metodoAutenticacionFisico(solicitudBean);
			}else{
				/**/
			}
		}catch (WebServicesException e) {
			contentHTML = e.getCodServicioError();
			_log.error(Utils.getWebserviceExpectionError(e), e);
		}finally{
			solicitudBean.getExpedienteBean().setPaso(paso);
			solicitudBean.getExpedienteBean().setHtmlCode(contentHTML);
			gestorRegistroService.registrarExpediente(solicitudBean);
			mapResultado.put(RequestParam.GO_TO_URL,Utils.getUrlPath(solicitudBean));
			addSessionUser(solicitudBean, request);
			writeJSON(request, response,JSONFactoryUtil.looseSerialize(mapResultado));
		}
	}
}