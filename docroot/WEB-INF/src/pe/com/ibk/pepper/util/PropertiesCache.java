package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;
import java.util.Set;


public class PropertiesCache {
	
	private static final Log _log = LogFactoryUtil.getLog(PropertiesCache.class);

	private final Properties configProp = new Properties();
	
	private static final String CONFIG_SENSITIVE = "CONFIG_SENSITIVE";
	
	private PropertiesCache() {

		String kubernetesSecret = getPropertyFromEnvOrSystem(CONFIG_SENSITIVE);
		
		cargarPropertiesFromString(kubernetesSecret, CONFIG_SENSITIVE, true);
	}
	
	private void cargarPropertiesFromString(String propertiesString, String source, boolean isConfigMap) {
		int kubernetesPropertiesHash;
		String message = "kubernetes: " + source;
		String propertiesWithSpecialChars = reemplazarCaracteresEspeciales(propertiesString);
		try (StringReader properties = new StringReader(propertiesWithSpecialChars)) {
			kubernetesPropertiesHash = propertiesString.hashCode();
			if (!isConfigMap) {
				message = "archivo: " + source;
			}
			_log.debug("cargando properties desde " + message + " hash:" + kubernetesPropertiesHash);
			configProp.load(properties);
		} catch (IOException e) {
			_log.error("Error al cargar los properties desde " + message + " " , e);
		}
	}
	
	private String getPropertyFromEnvOrSystem(String name) {
		String property = System.getenv(name);
		if (property == null) {
			property = System.getProperty(name);
		}
		
		return property;
	}
	
	// Bill Pugh Solution for singleton pattern
	
	private static class LazyHolder{
		private static final PropertiesCache INSTANCE = new PropertiesCache();
		private LazyHolder(){
			
		}
	}
		
		public static PropertiesCache getInstance() {
			return LazyHolder.INSTANCE;
		}
		
		public String getProperty(String key) {
			String property = configProp.getProperty(key);
			if (property == null) {
				_log.debug("no se encontro la propiedad: " + key + " revise sus archivos de properties, configmap o secret");
			}
			return property;
		}
		
		protected static String reemplazarCaracteresEspeciales(String cadenaOriginal) {
			for (SpecialCharactersEnum spe : SpecialCharactersEnum.values()) {
				if (cadenaOriginal.contains(spe.code())) {
					cadenaOriginal = cadenaOriginal.replace(spe.code(), spe.value());
				}
			}
			return cadenaOriginal;
		}
		
		public Set<String> getAllPropertyNames() {
			return configProp.stringPropertyNames();
		}
		
		public boolean containsKey(String key) {
			return configProp.containsKey(key);
		}
}
