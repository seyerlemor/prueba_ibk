package pe.com.ibk.pepper.rest.config;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import pe.com.ibk.pepper.util.Constantes;
import pe.com.ibk.pepper.util.Utils;

public class SecurityRestClientFix {
  private static final String SSL = "SSL";
  private static final String TLS = "TLSv1.2";
  private static Log logger =  LogFactoryUtil.getLog(SecurityRestClient.class);

  private SecurityRestClientFix() {
    throw new IllegalAccessError("Clase EstÃ¡tica");
  }
  
  public static Builder init(String host, String path, Boolean flagAutenticacion) {
		try {
			HostnameVerifier hostnameVerifier = new InsecureHostnameVerifier();
			Client client = ClientBuilder.newBuilder()
					.sslContext(getSSLContextCertified())
					.hostnameVerifier(hostnameVerifier).build();
			if(flagAutenticacion){
				  HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(Utils.REST_USUARIO, Utils.REST_PASSWORD);
				  client.register(feature);
			  }
			WebTarget target = client.target(host).path(path);
			return target.request(MediaType.APPLICATION_JSON_TYPE);
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

  
  public static Builder initAzure(String host, String path, String user, String password, Map<String, String> queryParameters, Map<String, String> templates) {
	  HostnameVerifier hostnameVerifier = new InsecureHostnameVerifier();
	  Client client = ClientBuilder.newBuilder().sslContext(getTrustAllSSLContextCertified()).hostnameVerifier(hostnameVerifier).build();
	  HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(user, password);
	  client.register(feature);
	  WebTarget target = client.target(host).path(path);
	  if (queryParameters != null)
		  for (Entry<String, String> entry : queryParameters.entrySet())
			  target = target.queryParam(entry.getKey(), entry.getValue());
	  if (templates != null) 
		  for (Entry<String, String> entry : templates.entrySet()) 
			  target = target.resolveTemplate(entry.getKey(), entry.getValue());
	  return target.request(Constantes.APPLICATION_JSON_TYPE_UTF8);
	}
  
  public static SSLContext getTrustAllSSLContextCertified() {
		SSLContext context = null;
		try {
			context = SSLContext.getInstance(TLS);
			final TrustManager[] trustManagerArray = { new NullX509TrustManager() };
			context.init(null, trustManagerArray, null);
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			logger.error(e);
		}
		return context;
	}
  
  public static SSLContext getSSLContextCertified() {
	SslConfigurator sslConfig = SslConfigurator.newInstance().securityProtocol(SSL);
	return sslConfig.createSSLContext();
  }
}
