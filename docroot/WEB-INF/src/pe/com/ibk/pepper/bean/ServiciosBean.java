package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class ServiciosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idServicio;
	private long idExpediente;
	private String nombre;
	private String codigo;
	private String descripcion;
	private long response;
	private String messageBus;
	private String tipo;
	private String respuestaInterna;		
	private String busResponseCode;
	
	private Boolean resultR1;
	private String resultCampania;
	private String resultReniec;
	private String resultEquifax;
	private String resultCalificacion;
	private String resultCliente;
	private Boolean resultFlagObtenerInformacionPersona;
	
	private String resultHeaderR1;
	private String resultHeaderCampania;
	private String resultHeaderReniec;
	private String resultHeaderConsPregEqfx;
	private String resultHeaderValidPregEqfx;
	private String resultHeaderCalificacion;
	private String resultHeaderConsAfiliacion;
	private String resultHeaderGenToken;
	private String resultHeaderActualizarRspta;
	private String resultHeaderObtenerInformacionPersona;
	private String resultHeaderVentaTC;
	private String resultHeaderConsultaPOTC;
	private String resultHeaderGenerarPOTC;
	private Integer status;
	
	private String rptaMensajePregEqfx;
	private String rptaMensajeReniec;
	
	
	public String getBusResponseCode() {
		return busResponseCode;
	}
	public void setBusResponseCode(String busResponseCode) {
		this.busResponseCode = busResponseCode;
	}
	public long getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}
	public long getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(long idExpediente) {
		this.idExpediente = idExpediente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public long getResponse() {
		return response;
	}
	public void setResponse(long response) {
		this.response = response;
	}
	public String getMessageBus() {
		return messageBus;
	}
	public void setMessageBus(String messageBus) {
		this.messageBus = messageBus;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getRespuestaInterna() {
		return respuestaInterna;
	}
	public void setRespuestaInterna(String respuestaInterna) {
		this.respuestaInterna = respuestaInterna;
	}
	public String getResultHeaderActualizarRspta() {
		return resultHeaderActualizarRspta;
	}
	public void setResultHeaderActualizarRspta(String resultHeaderActualizarRspta) {
		this.resultHeaderActualizarRspta = resultHeaderActualizarRspta;
	}
	public String getResultHeaderConsAfiliacion() {
		return resultHeaderConsAfiliacion;
	}
	public void setResultHeaderConsAfiliacion(String resultHeaderConsAfiliacion) {
		this.resultHeaderConsAfiliacion = resultHeaderConsAfiliacion;
	}
	public String getResultHeaderGenToken() {
		return resultHeaderGenToken;
	}
	public void setResultHeaderGenToken(String resultHeaderGenToken) {
		this.resultHeaderGenToken = resultHeaderGenToken;
	}
	public String getResultHeaderCalificacion() {
		return resultHeaderCalificacion;
	}
	public void setResultHeaderCalificacion(String resultHeaderCalificacion) {
		this.resultHeaderCalificacion = resultHeaderCalificacion;
	}
	//
	private String resultHeaderTipificacionIntento;
	
	//nuevo 
	private String codigoCanalCampania;
	
	
	public Boolean getResultR1() {
		return resultR1;
	}
	public void setResultR1(Boolean resultR1) {
		this.resultR1 = resultR1;
	}
	public String getResultCampania() {
		return resultCampania;
	}
	public void setResultCampania(String resultCampania) {
		this.resultCampania = resultCampania;
	}
	public String getResultReniec() {
		return resultReniec;
	}
	public void setResultReniec(String resultReniec) {
		this.resultReniec = resultReniec;
	}
	public String getResultEquifax() {
		return resultEquifax;
	}
	public void setResultEquifax(String resultEquifax) {
		this.resultEquifax = resultEquifax;
	}
	public String getResultCalificacion() {
		return resultCalificacion;
	}
	public void setResultCalificacion(String resultCalificacion) {
		this.resultCalificacion = resultCalificacion;
	}
	public String getResultCliente() {
		return resultCliente;
	}
	public void setResultCliente(String resultCliente) {
		this.resultCliente = resultCliente;
	}
	public String getResultHeaderR1() {
		return resultHeaderR1;
	}
	public void setResultHeaderR1(String resultHeaderR1) {
		this.resultHeaderR1 = resultHeaderR1;
	}
	public String getResultHeaderCampania() {
		return resultHeaderCampania;
	}
	public void setResultHeaderCampania(String resultHeaderCampania) {
		this.resultHeaderCampania = resultHeaderCampania;
	}
	public String getResultHeaderReniec() {
		return resultHeaderReniec;
	}
	public void setResultHeaderReniec(String resultHeaderReniec) {
		this.resultHeaderReniec = resultHeaderReniec;
	}
	public String getResultHeaderConsPregEqfx() {
		return resultHeaderConsPregEqfx;
	}
	public void setResultHeaderConsPregEqfx(String resultHeaderConsPregEqfx) {
		this.resultHeaderConsPregEqfx = resultHeaderConsPregEqfx;
	}
	public String getResultHeaderValidPregEqfx() {
		return resultHeaderValidPregEqfx;
	}
	public void setResultHeaderValidPregEqfx(String resultHeaderValidPregEqfx) {
		this.resultHeaderValidPregEqfx = resultHeaderValidPregEqfx;
	}
	
	public String getCodigoCanalCampania() {
		return codigoCanalCampania;
	}
	public void setCodigoCanalCampania(String codigoCanalCampania) {
		this.codigoCanalCampania = codigoCanalCampania;
	}
	public Boolean getResultFlagObtenerInformacionPersona() {
		return resultFlagObtenerInformacionPersona;
	}
	public void setResultFlagObtenerInformacionPersona(
			Boolean resultFlagObtenerInformacionPersona) {
		this.resultFlagObtenerInformacionPersona = resultFlagObtenerInformacionPersona;
	}
	public String getResultHeaderObtenerInformacionPersona() {
		return resultHeaderObtenerInformacionPersona;
	}
	public void setResultHeaderObtenerInformacionPersona(
			String resultHeaderObtenerInformacionPersona) {
		this.resultHeaderObtenerInformacionPersona = resultHeaderObtenerInformacionPersona;
	}		
	
	public String getResultHeaderTipificacionIntento() {
		return resultHeaderTipificacionIntento;
	}
	public void setResultHeaderTipificacionIntento(
			String resultHeaderTipificacionIntento) {
		this.resultHeaderTipificacionIntento = resultHeaderTipificacionIntento;
	}
	public String getResultHeaderVentaTC() {
		return resultHeaderVentaTC;
	}
	public void setResultHeaderVentaTC(String resultHeaderVentaTC) {
		this.resultHeaderVentaTC = resultHeaderVentaTC;
	}
	public String getResultHeaderConsultaPOTC() {
		return resultHeaderConsultaPOTC;
	}
	public void setResultHeaderConsultaPOTC(String resultHeaderConsultaPOTC) {
		this.resultHeaderConsultaPOTC = resultHeaderConsultaPOTC;
	}
		
	public String getResultHeaderGenerarPOTC() {
		return resultHeaderGenerarPOTC;
	}
	public void setResultHeaderGenerarPOTC(String resultHeaderGenerarPOTC) {
		this.resultHeaderGenerarPOTC = resultHeaderGenerarPOTC;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRptaMensajePregEqfx() {
		return rptaMensajePregEqfx;
	}
	public void setRptaMensajePregEqfx(String rptaMensajePregEqfx) {
		this.rptaMensajePregEqfx = rptaMensajePregEqfx;
	}
	public String getRptaMensajeReniec() {
		return rptaMensajeReniec;
	}
	public void setRptaMensajeReniec(String rptaMensajeReniec) {
		this.rptaMensajeReniec = rptaMensajeReniec;
	}
	@Override
	public String toString() {
		return "ServiciosBean [idServicio=" + idServicio + ", idExpediente="
				+ idExpediente + ", nombre=" + nombre + ", codigo=" + codigo
				+ ", descripcion=" + descripcion + ", response=" + response
				+ ", messageBus=" + messageBus + ", tipo=" + tipo
				+ ", respuestaInterna=" + respuestaInterna
				+ ", busResponseCode=" + busResponseCode + ", resultR1="
				+ resultR1 + ", resultCampania=" + resultCampania
				+ ", resultReniec=" + resultReniec + ", resultEquifax="
				+ resultEquifax + ", resultCalificacion=" + resultCalificacion
				+ ", resultCliente=" + resultCliente
				+ ", resultFlagObtenerInformacionPersona="
				+ resultFlagObtenerInformacionPersona + ", resultHeaderR1="
				+ resultHeaderR1 + ", resultHeaderCampania="
				+ resultHeaderCampania + ", resultHeaderReniec="
				+ resultHeaderReniec + ", resultHeaderConsPregEqfx="
				+ resultHeaderConsPregEqfx + ", resultHeaderValidPregEqfx="
				+ resultHeaderValidPregEqfx + ", resultHeaderCalificacion="
				+ resultHeaderCalificacion + ", resultHeaderConsAfiliacion="
				+ resultHeaderConsAfiliacion + ", resultHeaderGenToken="
				+ resultHeaderGenToken + ", resultHeaderActualizarRspta="
				+ resultHeaderActualizarRspta
				+ ", resultHeaderObtenerInformacionPersona="
				+ resultHeaderObtenerInformacionPersona
				+ ", resultHeaderVentaTC=" + resultHeaderVentaTC
				+ ", resultHeaderConsultaPOTC=" + resultHeaderConsultaPOTC
				+ ", resultHeaderGenerarPOTC=" + resultHeaderGenerarPOTC
				+ ", status=" + status + ", rptaMensajeCalificacion="
				+ rptaMensajePregEqfx + ", rptaMensajeReniec="
				+ rptaMensajeReniec + ", resultHeaderTipificacionIntento="
				+ resultHeaderTipificacionIntento + ", codigoCanalCampania="
				+ codigoCanalCampania + "]";
	}
	
}
