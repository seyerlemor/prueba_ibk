package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

import java.util.ArrayList;
import java.util.List;

import pe.com.ibk.pepper.model.ClienteTransaccion;
import pe.com.ibk.pepper.model.impl.ClienteTransaccionImpl;

public class ClienteTransaccionFinderImpl extends BasePersistenceImpl<ClienteTransaccion> implements ClienteTransaccionFinder{
	
	
	public static final String FINDER_TRANSACCIONES = "ResultadoPepper.obtenerTransacciones";
	
	private static final Log _log = LogFactoryUtil.getLog(ClienteTransaccionFinderImpl.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClienteTransaccion> obtenerTransacciones(long idExpediente, String tipoFlujo) {
		Session session = null;
		List<ClienteTransaccion> out = new ArrayList<ClienteTransaccion>();
		try {
			session = openSession();
			String sql = CustomSQLUtil.get(FINDER_TRANSACCIONES);
			SQLQuery q = session.createSQLQuery(sql);
			q.addEntity("ClienteTransaccion", ClienteTransaccionImpl.class);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(idExpediente);
			qPos.add(tipoFlujo);
			out = q.list();
		} catch (Exception e) {
	        _log.debug(e);
		} finally {
			closeSession(session);
		}
		return out;
	}	
	
}
