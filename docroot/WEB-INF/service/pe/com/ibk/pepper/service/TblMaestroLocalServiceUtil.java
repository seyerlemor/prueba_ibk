/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for TblMaestro. This utility wraps
 * {@link pe.com.ibk.pepper.service.impl.TblMaestroLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Interbank
 * @see TblMaestroLocalService
 * @see pe.com.ibk.pepper.service.base.TblMaestroLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.impl.TblMaestroLocalServiceImpl
 * @generated
 */
public class TblMaestroLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link pe.com.ibk.pepper.service.impl.TblMaestroLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the tbl maestro to the database. Also notifies the appropriate model listeners.
	*
	* @param tblMaestro the tbl maestro
	* @return the tbl maestro that was added
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblMaestro addTblMaestro(
		pe.com.ibk.pepper.model.TblMaestro tblMaestro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addTblMaestro(tblMaestro);
	}

	/**
	* Creates a new tbl maestro with the primary key. Does not add the tbl maestro to the database.
	*
	* @param idMaestro the primary key for the new tbl maestro
	* @return the new tbl maestro
	*/
	public static pe.com.ibk.pepper.model.TblMaestro createTblMaestro(
		int idMaestro) {
		return getService().createTblMaestro(idMaestro);
	}

	/**
	* Deletes the tbl maestro with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idMaestro the primary key of the tbl maestro
	* @return the tbl maestro that was removed
	* @throws PortalException if a tbl maestro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblMaestro deleteTblMaestro(
		int idMaestro)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteTblMaestro(idMaestro);
	}

	/**
	* Deletes the tbl maestro from the database. Also notifies the appropriate model listeners.
	*
	* @param tblMaestro the tbl maestro
	* @return the tbl maestro that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblMaestro deleteTblMaestro(
		pe.com.ibk.pepper.model.TblMaestro tblMaestro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteTblMaestro(tblMaestro);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static pe.com.ibk.pepper.model.TblMaestro fetchTblMaestro(
		int idMaestro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchTblMaestro(idMaestro);
	}

	/**
	* Returns the tbl maestro with the primary key.
	*
	* @param idMaestro the primary key of the tbl maestro
	* @return the tbl maestro
	* @throws PortalException if a tbl maestro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblMaestro getTblMaestro(
		int idMaestro)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getTblMaestro(idMaestro);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the tbl maestros.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tbl maestros
	* @param end the upper bound of the range of tbl maestros (not inclusive)
	* @return the range of tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.TblMaestro> getTblMaestros(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTblMaestros(start, end);
	}

	/**
	* Returns the number of tbl maestros.
	*
	* @return the number of tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public static int getTblMaestrosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTblMaestrosCount();
	}

	/**
	* Updates the tbl maestro in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param tblMaestro the tbl maestro
	* @return the tbl maestro that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblMaestro updateTblMaestro(
		pe.com.ibk.pepper.model.TblMaestro tblMaestro)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateTblMaestro(tblMaestro);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static pe.com.ibk.pepper.model.TblMaestro buscarDatosMaestroporHash(
		java.lang.String idHash) {
		return getService().buscarDatosMaestroporHash(idHash);
	}

	public static void clearService() {
		_service = null;
	}

	public static TblMaestroLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					TblMaestroLocalService.class.getName());

			if (invokableLocalService instanceof TblMaestroLocalService) {
				_service = (TblMaestroLocalService)invokableLocalService;
			}
			else {
				_service = new TblMaestroLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(TblMaestroLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(TblMaestroLocalService service) {
	}

	private static TblMaestroLocalService _service;
}