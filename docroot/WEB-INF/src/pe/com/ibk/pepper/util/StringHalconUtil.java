package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

public class StringHalconUtil {
	
	
	
	public StringHalconUtil() {
		super();
	}

	public static String getFirtsName(String nombreCompleto){		
		String firtsNameArray[] = nombreCompleto.split(" "); 
		return firtsNameArray[0];
	}
	
	public static String toCapitalize(String string){
		string = string.trim();
		String []strings = null;
		String stringToCapitalize = "";
		if(string.indexOf(" ")>0){
			strings = string.split(" ");
		}else{
			strings = new String[1];
			strings[0] = string;
		}
		for(String cadena : strings){
			String tmp = cadena.trim().toUpperCase();
			if(tmp.length()>1){
				tmp = tmp.replace(tmp.substring(1), tmp.substring(1).toLowerCase());
			}
			stringToCapitalize += tmp + " ";
		}
		return stringToCapitalize.trim();
	}
	
	public static String toCapitalizeFirts(String texto){
		texto = texto.toUpperCase(); 
		if(Validador.isNotNull(texto)){
			String firts = texto.substring(0,1);
			if(firts.equals("\u00a1") || firts.equals("\u00bf")){  //Â¿Â¡
				texto=texto.replace(texto.substring(2), StringUtil.toLowerCase(texto.substring(2))); 
			}else{
				texto=texto.replace(texto.substring(1), StringUtil.toLowerCase(texto.substring(1)));
			}
		}else{
			texto=StringPool.BLANK;
		}		 
		
		return texto.trim();		
	}
	
	public static String toUpperCase(String texto){
		if(Validador.isNotNull(texto)){
			return cleanTildesUpperCase(texto);
		}else{
			return StringPool.BLANK;
		}	
	}
	
	public static String cleanTildesUpperCase(String texto){ 
		texto = GetterUtil.getString(texto).toUpperCase();
		texto = texto.replace("\u00c4", "A").replace("\u00cb", "E").replace("\u00cf", "I").replace("\u00d6", "O").replace("\u00dc", "U");
	    return texto.replace("\u00c1", "A").replace("\u00c9", "E").replace("\u00cd", "I").replace("\u00d3", "O").replace("\u00da", "U").replace("\u00b4","");
	}

}
