/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.TblMaestro;

/**
 * The persistence interface for the tbl maestro service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see TblMaestroPersistenceImpl
 * @see TblMaestroUtil
 * @generated
 */
public interface TblMaestroPersistence extends BasePersistence<TblMaestro> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TblMaestroUtil} to access the tbl maestro persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the tbl maestro where idHash = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchTblMaestroException} if it could not be found.
	*
	* @param idHash the id hash
	* @return the matching tbl maestro
	* @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a matching tbl maestro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro findByM_H(java.lang.String idHash)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblMaestroException;

	/**
	* Returns the tbl maestro where idHash = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idHash the id hash
	* @return the matching tbl maestro, or <code>null</code> if a matching tbl maestro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro fetchByM_H(
		java.lang.String idHash)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tbl maestro where idHash = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idHash the id hash
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching tbl maestro, or <code>null</code> if a matching tbl maestro could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro fetchByM_H(
		java.lang.String idHash, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the tbl maestro where idHash = &#63; from the database.
	*
	* @param idHash the id hash
	* @return the tbl maestro that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro removeByM_H(
		java.lang.String idHash)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblMaestroException;

	/**
	* Returns the number of tbl maestros where idHash = &#63;.
	*
	* @param idHash the id hash
	* @return the number of matching tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public int countByM_H(java.lang.String idHash)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the tbl maestro in the entity cache if it is enabled.
	*
	* @param tblMaestro the tbl maestro
	*/
	public void cacheResult(pe.com.ibk.pepper.model.TblMaestro tblMaestro);

	/**
	* Caches the tbl maestros in the entity cache if it is enabled.
	*
	* @param tblMaestros the tbl maestros
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.TblMaestro> tblMaestros);

	/**
	* Creates a new tbl maestro with the primary key. Does not add the tbl maestro to the database.
	*
	* @param idMaestro the primary key for the new tbl maestro
	* @return the new tbl maestro
	*/
	public pe.com.ibk.pepper.model.TblMaestro create(int idMaestro);

	/**
	* Removes the tbl maestro with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idMaestro the primary key of the tbl maestro
	* @return the tbl maestro that was removed
	* @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a tbl maestro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro remove(int idMaestro)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblMaestroException;

	public pe.com.ibk.pepper.model.TblMaestro updateImpl(
		pe.com.ibk.pepper.model.TblMaestro tblMaestro)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tbl maestro with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchTblMaestroException} if it could not be found.
	*
	* @param idMaestro the primary key of the tbl maestro
	* @return the tbl maestro
	* @throws pe.com.ibk.pepper.NoSuchTblMaestroException if a tbl maestro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro findByPrimaryKey(int idMaestro)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblMaestroException;

	/**
	* Returns the tbl maestro with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idMaestro the primary key of the tbl maestro
	* @return the tbl maestro, or <code>null</code> if a tbl maestro with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblMaestro fetchByPrimaryKey(int idMaestro)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tbl maestros.
	*
	* @return the tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblMaestro> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tbl maestros.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tbl maestros
	* @param end the upper bound of the range of tbl maestros (not inclusive)
	* @return the range of tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblMaestro> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tbl maestros.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblMaestroModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tbl maestros
	* @param end the upper bound of the range of tbl maestros (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblMaestro> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the tbl maestros from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tbl maestros.
	*
	* @return the number of tbl maestros
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}