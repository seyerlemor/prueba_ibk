/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Expediente service. Represents a row in the &quot;T_CLIENTE_EXPEDIENTE&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link pe.com.ibk.pepper.model.impl.ExpedienteImpl}.
 * </p>
 *
 * @author Interbank
 * @see Expediente
 * @see pe.com.ibk.pepper.model.impl.ExpedienteImpl
 * @see pe.com.ibk.pepper.model.impl.ExpedienteModelImpl
 * @generated
 */
public interface ExpedienteModel extends BaseModel<Expediente> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a expediente model instance should use the {@link Expediente} interface instead.
	 */

	/**
	 * Returns the primary key of this expediente.
	 *
	 * @return the primary key of this expediente
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this expediente.
	 *
	 * @param primaryKey the primary key of this expediente
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the id expediente of this expediente.
	 *
	 * @return the id expediente of this expediente
	 */
	public long getIdExpediente();

	/**
	 * Sets the id expediente of this expediente.
	 *
	 * @param idExpediente the id expediente of this expediente
	 */
	public void setIdExpediente(long idExpediente);

	/**
	 * Returns the documento of this expediente.
	 *
	 * @return the documento of this expediente
	 */
	@AutoEscape
	public String getDocumento();

	/**
	 * Sets the documento of this expediente.
	 *
	 * @param documento the documento of this expediente
	 */
	public void setDocumento(String documento);

	/**
	 * Returns the estado of this expediente.
	 *
	 * @return the estado of this expediente
	 */
	@AutoEscape
	public String getEstado();

	/**
	 * Sets the estado of this expediente.
	 *
	 * @param estado the estado of this expediente
	 */
	public void setEstado(String estado);

	/**
	 * Returns the paso of this expediente.
	 *
	 * @return the paso of this expediente
	 */
	@AutoEscape
	public String getPaso();

	/**
	 * Sets the paso of this expediente.
	 *
	 * @param paso the paso of this expediente
	 */
	public void setPaso(String paso);

	/**
	 * Returns the pagina of this expediente.
	 *
	 * @return the pagina of this expediente
	 */
	@AutoEscape
	public String getPagina();

	/**
	 * Sets the pagina of this expediente.
	 *
	 * @param pagina the pagina of this expediente
	 */
	public void setPagina(String pagina);

	/**
	 * Returns the tipo flujo of this expediente.
	 *
	 * @return the tipo flujo of this expediente
	 */
	@AutoEscape
	public String getTipoFlujo();

	/**
	 * Sets the tipo flujo of this expediente.
	 *
	 * @param tipoFlujo the tipo flujo of this expediente
	 */
	public void setTipoFlujo(String tipoFlujo);

	/**
	 * Returns the flag r1 of this expediente.
	 *
	 * @return the flag r1 of this expediente
	 */
	@AutoEscape
	public String getFlagR1();

	/**
	 * Sets the flag r1 of this expediente.
	 *
	 * @param flagR1 the flag r1 of this expediente
	 */
	public void setFlagR1(String flagR1);

	/**
	 * Returns the flag campania of this expediente.
	 *
	 * @return the flag campania of this expediente
	 */
	@AutoEscape
	public String getFlagCampania();

	/**
	 * Sets the flag campania of this expediente.
	 *
	 * @param flagCampania the flag campania of this expediente
	 */
	public void setFlagCampania(String flagCampania);

	/**
	 * Returns the flag reniec of this expediente.
	 *
	 * @return the flag reniec of this expediente
	 */
	@AutoEscape
	public String getFlagReniec();

	/**
	 * Sets the flag reniec of this expediente.
	 *
	 * @param flagReniec the flag reniec of this expediente
	 */
	public void setFlagReniec(String flagReniec);

	/**
	 * Returns the flag equifax of this expediente.
	 *
	 * @return the flag equifax of this expediente
	 */
	@AutoEscape
	public String getFlagEquifax();

	/**
	 * Sets the flag equifax of this expediente.
	 *
	 * @param flagEquifax the flag equifax of this expediente
	 */
	public void setFlagEquifax(String flagEquifax);

	/**
	 * Returns the fecha registro of this expediente.
	 *
	 * @return the fecha registro of this expediente
	 */
	public Date getFechaRegistro();

	/**
	 * Sets the fecha registro of this expediente.
	 *
	 * @param fechaRegistro the fecha registro of this expediente
	 */
	public void setFechaRegistro(Date fechaRegistro);

	/**
	 * Returns the tipo campania of this expediente.
	 *
	 * @return the tipo campania of this expediente
	 */
	@AutoEscape
	public String getTipoCampania();

	/**
	 * Sets the tipo campania of this expediente.
	 *
	 * @param tipoCampania the tipo campania of this expediente
	 */
	public void setTipoCampania(String tipoCampania);

	/**
	 * Returns the flag calificacion of this expediente.
	 *
	 * @return the flag calificacion of this expediente
	 */
	@AutoEscape
	public String getFlagCalificacion();

	/**
	 * Sets the flag calificacion of this expediente.
	 *
	 * @param flagCalificacion the flag calificacion of this expediente
	 */
	public void setFlagCalificacion(String flagCalificacion);

	/**
	 * Returns the flag cliente of this expediente.
	 *
	 * @return the flag cliente of this expediente
	 */
	@AutoEscape
	public String getFlagCliente();

	/**
	 * Sets the flag cliente of this expediente.
	 *
	 * @param flagCliente the flag cliente of this expediente
	 */
	public void setFlagCliente(String flagCliente);

	/**
	 * Returns the id usuario session of this expediente.
	 *
	 * @return the id usuario session of this expediente
	 */
	public int getIdUsuarioSession();

	/**
	 * Sets the id usuario session of this expediente.
	 *
	 * @param idUsuarioSession the id usuario session of this expediente
	 */
	public void setIdUsuarioSession(int idUsuarioSession);

	/**
	 * Returns the str json of this expediente.
	 *
	 * @return the str json of this expediente
	 */
	@AutoEscape
	public String getStrJson();

	/**
	 * Sets the str json of this expediente.
	 *
	 * @param strJson the str json of this expediente
	 */
	public void setStrJson(String strJson);

	/**
	 * Returns the form html of this expediente.
	 *
	 * @return the form html of this expediente
	 */
	@AutoEscape
	public String getFormHtml();

	/**
	 * Sets the form html of this expediente.
	 *
	 * @param formHtml the form html of this expediente
	 */
	public void setFormHtml(String formHtml);

	/**
	 * Returns the codigo html of this expediente.
	 *
	 * @return the codigo html of this expediente
	 */
	@AutoEscape
	public String getCodigoHtml();

	/**
	 * Sets the codigo html of this expediente.
	 *
	 * @param codigoHtml the codigo html of this expediente
	 */
	public void setCodigoHtml(String codigoHtml);

	/**
	 * Returns the periodo registro of this expediente.
	 *
	 * @return the periodo registro of this expediente
	 */
	@AutoEscape
	public String getPeriodoRegistro();

	/**
	 * Sets the periodo registro of this expediente.
	 *
	 * @param periodoRegistro the periodo registro of this expediente
	 */
	public void setPeriodoRegistro(String periodoRegistro);

	/**
	 * Returns the modificacion of this expediente.
	 *
	 * @return the modificacion of this expediente
	 */
	public Date getModificacion();

	/**
	 * Sets the modificacion of this expediente.
	 *
	 * @param modificacion the modificacion of this expediente
	 */
	public void setModificacion(Date modificacion);

	/**
	 * Returns the flag tipificacion of this expediente.
	 *
	 * @return the flag tipificacion of this expediente
	 */
	@AutoEscape
	public String getFlagTipificacion();

	/**
	 * Sets the flag tipificacion of this expediente.
	 *
	 * @param flagTipificacion the flag tipificacion of this expediente
	 */
	public void setFlagTipificacion(String flagTipificacion);

	/**
	 * Returns the flag actualizar respuesta of this expediente.
	 *
	 * @return the flag actualizar respuesta of this expediente
	 */
	@AutoEscape
	public String getFlagActualizarRespuesta();

	/**
	 * Sets the flag actualizar respuesta of this expediente.
	 *
	 * @param flagActualizarRespuesta the flag actualizar respuesta of this expediente
	 */
	public void setFlagActualizarRespuesta(String flagActualizarRespuesta);

	/**
	 * Returns the flag obtener informacion of this expediente.
	 *
	 * @return the flag obtener informacion of this expediente
	 */
	@AutoEscape
	public String getFlagObtenerInformacion();

	/**
	 * Sets the flag obtener informacion of this expediente.
	 *
	 * @param flagObtenerInformacion the flag obtener informacion of this expediente
	 */
	public void setFlagObtenerInformacion(String flagObtenerInformacion);

	/**
	 * Returns the flag consultar afiliacion of this expediente.
	 *
	 * @return the flag consultar afiliacion of this expediente
	 */
	@AutoEscape
	public String getFlagConsultarAfiliacion();

	/**
	 * Sets the flag consultar afiliacion of this expediente.
	 *
	 * @param flagConsultarAfiliacion the flag consultar afiliacion of this expediente
	 */
	public void setFlagConsultarAfiliacion(String flagConsultarAfiliacion);

	/**
	 * Returns the flag generacion token of this expediente.
	 *
	 * @return the flag generacion token of this expediente
	 */
	@AutoEscape
	public String getFlagGeneracionToken();

	/**
	 * Sets the flag generacion token of this expediente.
	 *
	 * @param flagGeneracionToken the flag generacion token of this expediente
	 */
	public void setFlagGeneracionToken(String flagGeneracionToken);

	/**
	 * Returns the flag venta t c of this expediente.
	 *
	 * @return the flag venta t c of this expediente
	 */
	@AutoEscape
	public String getFlagVentaTC();

	/**
	 * Sets the flag venta t c of this expediente.
	 *
	 * @param flagVentaTC the flag venta t c of this expediente
	 */
	public void setFlagVentaTC(String flagVentaTC);

	/**
	 * Returns the flag validacion token of this expediente.
	 *
	 * @return the flag validacion token of this expediente
	 */
	@AutoEscape
	public String getFlagValidacionToken();

	/**
	 * Sets the flag validacion token of this expediente.
	 *
	 * @param flagValidacionToken the flag validacion token of this expediente
	 */
	public void setFlagValidacionToken(String flagValidacionToken);

	/**
	 * Returns the flag recompra gen p o t c of this expediente.
	 *
	 * @return the flag recompra gen p o t c of this expediente
	 */
	@AutoEscape
	public String getFlagRecompraGenPOTC();

	/**
	 * Sets the flag recompra gen p o t c of this expediente.
	 *
	 * @param flagRecompraGenPOTC the flag recompra gen p o t c of this expediente
	 */
	public void setFlagRecompraGenPOTC(String flagRecompraGenPOTC);

	/**
	 * Returns the flag recompra con p o t c of this expediente.
	 *
	 * @return the flag recompra con p o t c of this expediente
	 */
	@AutoEscape
	public String getFlagRecompraConPOTC();

	/**
	 * Sets the flag recompra con p o t c of this expediente.
	 *
	 * @param flagRecompraConPOTC the flag recompra con p o t c of this expediente
	 */
	public void setFlagRecompraConPOTC(String flagRecompraConPOTC);

	/**
	 * Returns the flag extorno gen p o t c of this expediente.
	 *
	 * @return the flag extorno gen p o t c of this expediente
	 */
	@AutoEscape
	public String getFlagExtornoGenPOTC();

	/**
	 * Sets the flag extorno gen p o t c of this expediente.
	 *
	 * @param flagExtornoGenPOTC the flag extorno gen p o t c of this expediente
	 */
	public void setFlagExtornoGenPOTC(String flagExtornoGenPOTC);

	/**
	 * Returns the flag extorno con p o t c of this expediente.
	 *
	 * @return the flag extorno con p o t c of this expediente
	 */
	@AutoEscape
	public String getFlagExtornoConPOTC();

	/**
	 * Sets the flag extorno con p o t c of this expediente.
	 *
	 * @param flagExtornoConPOTC the flag extorno con p o t c of this expediente
	 */
	public void setFlagExtornoConPOTC(String flagExtornoConPOTC);

	/**
	 * Returns the ip of this expediente.
	 *
	 * @return the ip of this expediente
	 */
	@AutoEscape
	public String getIp();

	/**
	 * Sets the ip of this expediente.
	 *
	 * @param ip the ip of this expediente
	 */
	public void setIp(String ip);

	/**
	 * Returns the navegador of this expediente.
	 *
	 * @return the navegador of this expediente
	 */
	@AutoEscape
	public String getNavegador();

	/**
	 * Sets the navegador of this expediente.
	 *
	 * @param navegador the navegador of this expediente
	 */
	public void setNavegador(String navegador);

	/**
	 * Returns the pepper type of this expediente.
	 *
	 * @return the pepper type of this expediente
	 */
	@AutoEscape
	public String getPepperType();

	/**
	 * Sets the pepper type of this expediente.
	 *
	 * @param pepperType the pepper type of this expediente
	 */
	public void setPepperType(String pepperType);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(pe.com.ibk.pepper.model.Expediente expediente);

	@Override
	public int hashCode();

	@Override
	public CacheModel<pe.com.ibk.pepper.model.Expediente> toCacheModel();

	@Override
	public pe.com.ibk.pepper.model.Expediente toEscapedModel();

	@Override
	public pe.com.ibk.pepper.model.Expediente toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}