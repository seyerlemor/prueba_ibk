
package pe.com.ibk.pepper.rest.equifax.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ConsultaPreguntas {

    private Candidato candidato;
    private InformacionAdicional informacionAdicional;    
    
    @JsonCreator
	public ConsultaPreguntas(Candidato candidato, InformacionAdicional informacionAdicional) {
		super();
		this.candidato = candidato;
		this.informacionAdicional = informacionAdicional;
	}

    @JsonProperty("candidato")
	public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    @JsonProperty("informacionAdicional")
    public InformacionAdicional getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(InformacionAdicional informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

}
