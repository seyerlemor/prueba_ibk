<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL id="validarCodigoOtp" var="validarCodigoOtp" />
<portlet:resourceURL id="generateToken" var="generateToken" />
<portlet:resourceURL var="accessStep2" id="accessStep2"></portlet:resourceURL>
	
	<div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    <liferay-ui:journal-article articleId="${contentStep.articleId}" groupId="${contentStep.groupId}" />
                    <div class="lh-ff-content-col">
                      <div class="lh-ff-section lh-ff-section--notification">
                        <div class="lh-ff-section__wrapper">
                          <div class="lh-ff-section__text">
                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 lh-ff-section__text__title">${renderRequest.preferences.getValue('otpPartTitle','')} ${pasoBean.clienteBean.firstname},</h3>
                            <p class="lh-typo__p1">${renderRequest.preferences.getValue('otpText001','')}
                            <br/>
                            ${requestScope.formatEmailAddress}
                            </p>
							<form:form   action="${validarCodigoOtp}" autocomplete="off" commandName="clienteBean" class="lh-ff-form" novalidate="novalidate" >
                                    <div class="lh-ff-group">
                                                  <div class="lh-ff-group__item lh-ff-group__item--sm">
                                                    <label class="lh-ff__label" for="validationCode">${renderRequest.preferences.getValue('otpValidateCodeRequiredMessage','')}
                                                    </label>
                                                                <div class="lh-ff-group__field">
                                                                        <input class="lh-input" id="validationCode" maxlength="8" name="validationCode" data-allowed-keys="alphanumeric" data-validate-required data-validate-required-message="${renderRequest.preferences.getValue('otpValidateCodeRequiredMessage','')}" data-validate-length="8" data-validate-length-message="${renderRequest.preferences.getValue('otpValidateCodeLengthMessage','')}" type="text">
                                                                </div>
                                                  </div>
                                    </div>
                              <div class="lh-ff-group">
                                <div class="lh-ff-group__item lh-ff-group__item--buttons lh-flex--sm">
                                  <button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit"><span>${renderRequest.preferences.getValue('otpTokenBtnListo','')}</span></button>
                                  <p class="lh-typo__p3 lh-flex-vertical-start" id="block-msg-code" style="opacity:0">
                                  	<span class="lh-color-light-green" id="tryMsg" data-msg-end="${renderRequest.preferences.getValue('otpTokenLimiteEnvioText','')}">
                                  		${renderRequest.preferences.getValue('otpTokenText001','')} <br>
                                  	</span>
                                  	<a class="lh-link lh-link--bold lh-link--arrow lh-link--underline" id="sendCode" data-action="${generateToken}" 
                                  		data-attempts="${renderRequest.preferences.getValue('otpTokenLimiteEnvioNum','')}">
                                  		${renderRequest.preferences.getValue('otpTokenText002','')}</a>
                                  	<a class="lh-link lh-link--bold lh-link--arrow lh-link--underline lh-hide" id="identificate" data-action="${accessStep2}">
                                  		${renderRequest.preferences.getValue('otpTokenText003','')}
                                  	</a>
                                  	<span class="lh-color-gray-dark lh-hide" id="countdown" 
                                  		data-counter-time="${renderRequest.preferences.getValue('otpTokenCounterNum','')}">
                                  		${renderRequest.preferences.getValue('otpTokenText004','')}
                                  		<span class="nowrap">
                                  			${renderRequest.preferences.getValue('otpTokenCounterText','')}
                                  		</span>
                                  	</span>
                                  </p>
                                </div>
                              </div>
                                    <div class="lh-form__error">
                                      <div class="lh-typo__p2 lh-form__error-message" data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
                                    </div>
                            </form:form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>