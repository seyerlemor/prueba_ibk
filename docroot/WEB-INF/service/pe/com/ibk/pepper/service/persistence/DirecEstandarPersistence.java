/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.DirecEstandar;

/**
 * The persistence interface for the direc estandar service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see DirecEstandarPersistenceImpl
 * @see DirecEstandarUtil
 * @generated
 */
public interface DirecEstandarPersistence extends BasePersistence<DirecEstandar> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DirecEstandarUtil} to access the direc estandar persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the direc estandar where idDetalleDireccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDirecEstandarException} if it could not be found.
	*
	* @param idDetalleDireccion the id detalle direccion
	* @return the matching direc estandar
	* @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a matching direc estandar could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar findByDE_ID(
		long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDirecEstandarException;

	/**
	* Returns the direc estandar where idDetalleDireccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idDetalleDireccion the id detalle direccion
	* @return the matching direc estandar, or <code>null</code> if a matching direc estandar could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar fetchByDE_ID(
		long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the direc estandar where idDetalleDireccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idDetalleDireccion the id detalle direccion
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching direc estandar, or <code>null</code> if a matching direc estandar could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar fetchByDE_ID(
		long idDetalleDireccion, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the direc estandar where idDetalleDireccion = &#63; from the database.
	*
	* @param idDetalleDireccion the id detalle direccion
	* @return the direc estandar that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar removeByDE_ID(
		long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDirecEstandarException;

	/**
	* Returns the number of direc estandars where idDetalleDireccion = &#63;.
	*
	* @param idDetalleDireccion the id detalle direccion
	* @return the number of matching direc estandars
	* @throws SystemException if a system exception occurred
	*/
	public int countByDE_ID(long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the direc estandar in the entity cache if it is enabled.
	*
	* @param direcEstandar the direc estandar
	*/
	public void cacheResult(pe.com.ibk.pepper.model.DirecEstandar direcEstandar);

	/**
	* Caches the direc estandars in the entity cache if it is enabled.
	*
	* @param direcEstandars the direc estandars
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.DirecEstandar> direcEstandars);

	/**
	* Creates a new direc estandar with the primary key. Does not add the direc estandar to the database.
	*
	* @param idDetalleDireccion the primary key for the new direc estandar
	* @return the new direc estandar
	*/
	public pe.com.ibk.pepper.model.DirecEstandar create(long idDetalleDireccion);

	/**
	* Removes the direc estandar with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDetalleDireccion the primary key of the direc estandar
	* @return the direc estandar that was removed
	* @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a direc estandar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar remove(long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDirecEstandarException;

	public pe.com.ibk.pepper.model.DirecEstandar updateImpl(
		pe.com.ibk.pepper.model.DirecEstandar direcEstandar)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the direc estandar with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchDirecEstandarException} if it could not be found.
	*
	* @param idDetalleDireccion the primary key of the direc estandar
	* @return the direc estandar
	* @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a direc estandar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar findByPrimaryKey(
		long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchDirecEstandarException;

	/**
	* Returns the direc estandar with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDetalleDireccion the primary key of the direc estandar
	* @return the direc estandar, or <code>null</code> if a direc estandar with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.DirecEstandar fetchByPrimaryKey(
		long idDetalleDireccion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the direc estandars.
	*
	* @return the direc estandars
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.DirecEstandar> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the direc estandars.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of direc estandars
	* @param end the upper bound of the range of direc estandars (not inclusive)
	* @return the range of direc estandars
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.DirecEstandar> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the direc estandars.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of direc estandars
	* @param end the upper bound of the range of direc estandars (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of direc estandars
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.DirecEstandar> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the direc estandars from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of direc estandars.
	*
	* @return the number of direc estandars
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}