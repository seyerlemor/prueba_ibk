package pe.com.ibk.pepper.bean;

import java.io.Serializable;
import java.util.Date;

public class PreguntasEquifaxBean  implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idPregunta;
	private long idCabecera;
	private String codigoPregunta;
	private String descripPregunta;
	private String codigoRespuesta;
	private String descripRespuesta;
	private Date fechaRegistro;

	public long getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(long idPregunta) {
		this.idPregunta = idPregunta;
	}

	public long getIdCabecera() {
		return idCabecera;
	}
	public void setIdCabecera(long idCabecera) {
		this.idCabecera = idCabecera;
	}
	public String getCodigoPregunta() {
		return codigoPregunta;
	}
	public void setCodigoPregunta(String codigoPregunta) {
		this.codigoPregunta = codigoPregunta;
	}
	public String getDescripPregunta() {
		return descripPregunta;
	}
	public void setDescripPregunta(String descripPregunta) {
		this.descripPregunta = descripPregunta;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getDescripRespuesta() {
		return descripRespuesta;
	}
	public void setDescripRespuesta(String descripRespuesta) {
		this.descripRespuesta = descripRespuesta;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	@Override
	public String toString() {
		return "PreguntasEquifaxBean [idPregunta=" + idPregunta
				+ ", idCabecera=" + idCabecera + ", codigoPregunta="
				+ codigoPregunta + ", descripPregunta=" + descripPregunta
				+ ", codigoRespuesta=" + codigoRespuesta
				+ ", descripRespuesta=" + descripRespuesta + ", fechaRegistro="
				+ fechaRegistro + "]";
	}


	
	
}
