package pe.com.ibk.pepper.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DatesUtil {

	private static final Log logger = LogFactoryUtil.getLog(DatesUtil.class);

	private DatesUtil() {}
	
	public static String obtenerPeriodoActual(){
        Calendar fecha = Calendar.getInstance();
        int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH) + 1;
        return String.valueOf(mes).concat(String.valueOf(anio));
	}

	public static Date stringAdate(String fecha){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD);
		Date dtfecha = null;
		try {
			dtfecha = formatoDelTexto.parse(fecha);
		} catch (ParseException ex) {
			logger.error(ex);
		}		
		return dtfecha;
	}
	
	public static Date stringAdatePeriodoFecha(String periodo){
		int anio = Integer.parseInt(periodo.substring(0,4));
		int mes = Integer.parseInt(periodo.substring(4,6));
		int dia = 1;
		Calendar calendario=Calendar.getInstance();
		calendario.set(anio, mes, dia);
		int ultimoDiaMes=calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
		dia = ultimoDiaMes;
		String fecha = dia+" "+mes+" "+anio+",00:00";
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd MM yyyy, HH:mm");
		Date dtfecha = null;
		try {
			dtfecha = formatoDelTexto.parse(fecha);
		} catch (ParseException e) {
			logger.error(e);
		}		
		return dtfecha;
	}
	
	public static Date stringAdate2(String fecha){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		Date dtfecha = null;
		try {
			dtfecha = formatoDelTexto.parse(fecha);
		} catch (ParseException e) {
			logger.error(e);
		}		
		return dtfecha;
	}
	
	public static Date stringAdateFechaOperacion(String fecha){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd MMM yyyy HH:mm");
		Date dtfecha = null;
		try {
			dtfecha = formatoDelTexto.parse(fecha);
		} catch (ParseException e) {
			logger.error(e);
		}		
		return dtfecha;
	}

	public static String completeFechaDigit(String numero){
		String str = StringPool.BLANK;
		String newNumber = numero;
		if(Validator.isNotNull(newNumber)){
			Integer valor = Integer.parseInt(newNumber)+1;
			newNumber = String.valueOf(valor);
			int longitud = newNumber.length();
			if(longitud==1){
				str ="0"+Integer.parseInt(newNumber);
			}else{
				str = StringPool.BLANK+Integer.parseInt(newNumber);
			}
		}
		return str;
	}
    public static String formatExpirationDate(String fechaVencimiento){
    	return fechaVencimiento.substring(4,6).concat(StringPool.SLASH).concat(fechaVencimiento.substring(2,4));
    }
    
    public static String generarFechaImagen(){
		DateFormat fecha = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return fecha.format(new Date());
	}
	
	public static Date convertSringToDate(String fecha) throws ParseException{
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD,Locale.getDefault());
		return formatoDelTexto.parse(fecha);
	}
	

	public static String formatoFechaActualDDMMYYYY(){
		Date myDate = new Date();
		return new SimpleDateFormat("dd/MM/yyyy").format(myDate);
	}
	
	public static String formatoFechaActualYYYYMMDD(){
		Date myDate = new Date();
		return new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD).format(myDate);
	}
	
	public static String formatoFechaNac(String fecha){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
		String fechanac = StringPool.BLANK;
        try {
            Date date = formatter.parse(fecha);
            fechanac = formatter2.format(date);
        } catch (ParseException e) {
        	logger.error(e);
        }
        return fechanac;
	}
	
	public static String formatoFechaNacimiento(String fecha){
		String fechaFormateada = StringPool.BLANK;
        try {
        	if(Validator.isNotNull(fecha) && !fechaFormateada.equals(fecha)){
        		String[] nuevaFecha = fecha.split("-");
            	fechaFormateada = nuevaFecha[2] + "/" + nuevaFecha[1] + "/" + nuevaFecha[0];
        	}
        } catch (Exception e) {
        	logger.error(e);
        }
        return fechaFormateada;
	}
	
	public static String formatoFechaNacimiento2(String fecha){
		String[] nuevaFecha = fecha.split("/");
		String fechaFormateada = StringPool.BLANK;
        try {
        	fechaFormateada = nuevaFecha[2] + "-" + nuevaFecha[1] + "-" + nuevaFecha[0];
        } catch (Exception e) {
        	logger.error(e);
        }
        return fechaFormateada;
	}
	
	
	public static String obtenerPeriodoFecha(String fecha){
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD);
		SimpleDateFormat formatter2 = new SimpleDateFormat("Myyyy");
		String periodo = StringPool.BLANK;
		Date date = new Date();
        try {
        	if(!fecha.equals(StringPool.BLANK))
        		date = formatter.parse(fecha);
            periodo = formatter2.format(date);
        } catch (ParseException e) {
            logger.error(e);
        }
        return periodo;
	}
	
	public static String formatoMessageId(){
		Date myDate = new Date();
		String mili = String.valueOf(System.currentTimeMillis()).substring(10);
		return (new SimpleDateFormat(Constantes.FORMATO_FECHA_YYYYMMDDHHMMSS).format(myDate)).concat(mili);
	}
	
	public static String formatoFechaHoraRegistro(){
		return new SimpleDateFormat("dd MMM yyyy, HH:mm").format(new Date());
	}

	public static String formatoFechaHoraRegistroGMT5(){
		  SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm");
          Date datetime = new Date();
          sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));
		return sdf.format(datetime);
	}
	
	public static String formatoFechayyyyMMdd(String fecha){
		String fechaFinal = StringPool.BLANK;
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new java.text.SimpleDateFormat(Constantes.FORMATO_FECHA_YYYY_MM_DD);
			if(!fecha.equals(StringPool.BLANK))
				date = formatter.parse(fecha);		
			
			fechaFinal = formatter.format(date);
			} catch (java.text.ParseException e) {
				logger.error(e);
			}
		return fechaFinal;
	}
	
	public static String[] obtenerFecha(String fecha){
		return fecha.split("-");
	}
	
	public static Date convertSringToDateReport(String fecha) throws ParseException{
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return formatoDelTexto.parse(fecha);
	}
    
}
