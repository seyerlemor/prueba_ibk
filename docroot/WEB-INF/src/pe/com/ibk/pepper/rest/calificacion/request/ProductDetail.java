
package pe.com.ibk.pepper.rest.calificacion.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProductDetail {

    private String value;
    private String name;
    
    @JsonCreator
	public ProductDetail(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
    
    @JsonProperty("value")
	public String getValue() {
		return value;
	}
    
	public void setValue(String value) {
		this.value = value;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

}
