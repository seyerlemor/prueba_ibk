/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

import pe.com.ibk.pepper.NoSuchParametroHijoPOException;
import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.service.base.ParametroHijoPOLocalServiceBaseImpl;

/**
 * The implementation of the parametro hijo p o local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.ParametroHijoPOLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.ParametroHijoPOLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil
 */
public class ParametroHijoPOLocalServiceImpl
	extends ParametroHijoPOLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil} to access the parametro hijo p o local service.
	 */
private static final Log _log = LogFactoryUtil.getLog(ParametroHijoPOLocalServiceImpl.class);
	
	public List<ParametroHijoPO> getParametroHijosByCodigoPadre(String codigoPadre, int start, int end) throws SystemException{
		return parametroHijoPOPersistence.findByC_G(codigoPadre, Boolean.TRUE, start, end);
	}
	
	public ParametroHijoPO getParametroHijoByCodigoAndCodigoPadre(String codigo, String codigoPadre) throws SystemException{
		try {
			return parametroHijoPOPersistence.findByC_C_G_E(codigo, codigoPadre,  Boolean.TRUE);
		} catch (NoSuchParametroHijoPOException e) {
			_log.error("NoSuchParametroHijoPluginException:", e);
			return null;
		}
	}
	
	public List<ParametroHijoPO> getParametroHijoByCodigoAndCodigoPadreAndDato(String dato, String codigoPadre) throws SystemException{
		return parametroHijoPOPersistence.findByC_D_G_E(codigoPadre, dato, Boolean.TRUE);
	}
	
	public List<ParametroHijoPO> getParametroHijoByCodigoAndCodigoPadreAndDato1AndDato2(String dato1, String dato2, String codigoPadre) throws SystemException{
		return parametroHijoPOPersistence.findByC_D_D_G_E(codigoPadre, dato1, dato2, Boolean.TRUE);
	}
	
	public ParametroHijoPO getParametroHijoByCodigoAndCodigoPadreAndDato2(String dato2, String codigo, String codigoPadre) throws SystemException{
		try {
			return parametroHijoPOPersistence.findByC_C_D2_E(codigo, codigoPadre, dato2, Boolean.TRUE);
		} catch (NoSuchParametroHijoPOException e) {
			_log.error("NoSuchParametroHijoPluginException:");
			return null;
		}
	}
}