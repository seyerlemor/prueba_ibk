/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.CampaniaLocalServiceUtil;
import pe.com.ibk.pepper.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class CampaniaClp extends BaseModelImpl<Campania> implements Campania {
	public CampaniaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Campania.class;
	}

	@Override
	public String getModelClassName() {
		return Campania.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idCampania;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdCampania(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idCampania;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCampania", getIdCampania());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("codigoCampania", getCodigoCampania());
		attributes.put("nombreCampania", getNombreCampania());
		attributes.put("codigoOferta", getCodigoOferta());
		attributes.put("nombreOferta", getNombreOferta());
		attributes.put("codigoTratamiento", getCodigoTratamiento());
		attributes.put("nombreTratamiento", getNombreTratamiento());
		attributes.put("codigoUnico", getCodigoUnico());
		attributes.put("codigoProducto", getCodigoProducto());
		attributes.put("core", getCore());
		attributes.put("nombreProducto", getNombreProducto());
		attributes.put("tipoCampania", getTipoCampania());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idCampania = (Long)attributes.get("idCampania");

		if (idCampania != null) {
			setIdCampania(idCampania);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String codigoCampania = (String)attributes.get("codigoCampania");

		if (codigoCampania != null) {
			setCodigoCampania(codigoCampania);
		}

		String nombreCampania = (String)attributes.get("nombreCampania");

		if (nombreCampania != null) {
			setNombreCampania(nombreCampania);
		}

		String codigoOferta = (String)attributes.get("codigoOferta");

		if (codigoOferta != null) {
			setCodigoOferta(codigoOferta);
		}

		String nombreOferta = (String)attributes.get("nombreOferta");

		if (nombreOferta != null) {
			setNombreOferta(nombreOferta);
		}

		String codigoTratamiento = (String)attributes.get("codigoTratamiento");

		if (codigoTratamiento != null) {
			setCodigoTratamiento(codigoTratamiento);
		}

		String nombreTratamiento = (String)attributes.get("nombreTratamiento");

		if (nombreTratamiento != null) {
			setNombreTratamiento(nombreTratamiento);
		}

		String codigoUnico = (String)attributes.get("codigoUnico");

		if (codigoUnico != null) {
			setCodigoUnico(codigoUnico);
		}

		String codigoProducto = (String)attributes.get("codigoProducto");

		if (codigoProducto != null) {
			setCodigoProducto(codigoProducto);
		}

		String core = (String)attributes.get("core");

		if (core != null) {
			setCore(core);
		}

		String nombreProducto = (String)attributes.get("nombreProducto");

		if (nombreProducto != null) {
			setNombreProducto(nombreProducto);
		}

		String tipoCampania = (String)attributes.get("tipoCampania");

		if (tipoCampania != null) {
			setTipoCampania(tipoCampania);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public long getIdCampania() {
		return _idCampania;
	}

	@Override
	public void setIdCampania(long idCampania) {
		_idCampania = idCampania;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdCampania", long.class);

				method.invoke(_campaniaRemoteModel, idCampania);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdExpediente", long.class);

				method.invoke(_campaniaRemoteModel, idExpediente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoCampania() {
		return _codigoCampania;
	}

	@Override
	public void setCodigoCampania(String codigoCampania) {
		_codigoCampania = codigoCampania;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoCampania",
						String.class);

				method.invoke(_campaniaRemoteModel, codigoCampania);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreCampania() {
		return _nombreCampania;
	}

	@Override
	public void setNombreCampania(String nombreCampania) {
		_nombreCampania = nombreCampania;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreCampania",
						String.class);

				method.invoke(_campaniaRemoteModel, nombreCampania);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoOferta() {
		return _codigoOferta;
	}

	@Override
	public void setCodigoOferta(String codigoOferta) {
		_codigoOferta = codigoOferta;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoOferta", String.class);

				method.invoke(_campaniaRemoteModel, codigoOferta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreOferta() {
		return _nombreOferta;
	}

	@Override
	public void setNombreOferta(String nombreOferta) {
		_nombreOferta = nombreOferta;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreOferta", String.class);

				method.invoke(_campaniaRemoteModel, nombreOferta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoTratamiento() {
		return _codigoTratamiento;
	}

	@Override
	public void setCodigoTratamiento(String codigoTratamiento) {
		_codigoTratamiento = codigoTratamiento;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoTratamiento",
						String.class);

				method.invoke(_campaniaRemoteModel, codigoTratamiento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreTratamiento() {
		return _nombreTratamiento;
	}

	@Override
	public void setNombreTratamiento(String nombreTratamiento) {
		_nombreTratamiento = nombreTratamiento;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreTratamiento",
						String.class);

				method.invoke(_campaniaRemoteModel, nombreTratamiento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoUnico() {
		return _codigoUnico;
	}

	@Override
	public void setCodigoUnico(String codigoUnico) {
		_codigoUnico = codigoUnico;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoUnico", String.class);

				method.invoke(_campaniaRemoteModel, codigoUnico);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoProducto() {
		return _codigoProducto;
	}

	@Override
	public void setCodigoProducto(String codigoProducto) {
		_codigoProducto = codigoProducto;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoProducto",
						String.class);

				method.invoke(_campaniaRemoteModel, codigoProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCore() {
		return _core;
	}

	@Override
	public void setCore(String core) {
		_core = core;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setCore", String.class);

				method.invoke(_campaniaRemoteModel, core);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreProducto() {
		return _nombreProducto;
	}

	@Override
	public void setNombreProducto(String nombreProducto) {
		_nombreProducto = nombreProducto;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreProducto",
						String.class);

				method.invoke(_campaniaRemoteModel, nombreProducto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoCampania() {
		return _tipoCampania;
	}

	@Override
	public void setTipoCampania(String tipoCampania) {
		_tipoCampania = tipoCampania;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoCampania", String.class);

				method.invoke(_campaniaRemoteModel, tipoCampania);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_campaniaRemoteModel != null) {
			try {
				Class<?> clazz = _campaniaRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_campaniaRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCampaniaRemoteModel() {
		return _campaniaRemoteModel;
	}

	public void setCampaniaRemoteModel(BaseModel<?> campaniaRemoteModel) {
		_campaniaRemoteModel = campaniaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _campaniaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_campaniaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CampaniaLocalServiceUtil.addCampania(this);
		}
		else {
			CampaniaLocalServiceUtil.updateCampania(this);
		}
	}

	@Override
	public Campania toEscapedModel() {
		return (Campania)ProxyUtil.newProxyInstance(Campania.class.getClassLoader(),
			new Class[] { Campania.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CampaniaClp clone = new CampaniaClp();

		clone.setIdCampania(getIdCampania());
		clone.setIdExpediente(getIdExpediente());
		clone.setCodigoCampania(getCodigoCampania());
		clone.setNombreCampania(getNombreCampania());
		clone.setCodigoOferta(getCodigoOferta());
		clone.setNombreOferta(getNombreOferta());
		clone.setCodigoTratamiento(getCodigoTratamiento());
		clone.setNombreTratamiento(getNombreTratamiento());
		clone.setCodigoUnico(getCodigoUnico());
		clone.setCodigoProducto(getCodigoProducto());
		clone.setCore(getCore());
		clone.setNombreProducto(getNombreProducto());
		clone.setTipoCampania(getTipoCampania());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(Campania campania) {
		long primaryKey = campania.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CampaniaClp)) {
			return false;
		}

		CampaniaClp campania = (CampaniaClp)obj;

		long primaryKey = campania.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{idCampania=");
		sb.append(getIdCampania());
		sb.append(", idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", codigoCampania=");
		sb.append(getCodigoCampania());
		sb.append(", nombreCampania=");
		sb.append(getNombreCampania());
		sb.append(", codigoOferta=");
		sb.append(getCodigoOferta());
		sb.append(", nombreOferta=");
		sb.append(getNombreOferta());
		sb.append(", codigoTratamiento=");
		sb.append(getCodigoTratamiento());
		sb.append(", nombreTratamiento=");
		sb.append(getNombreTratamiento());
		sb.append(", codigoUnico=");
		sb.append(getCodigoUnico());
		sb.append(", codigoProducto=");
		sb.append(getCodigoProducto());
		sb.append(", core=");
		sb.append(getCore());
		sb.append(", nombreProducto=");
		sb.append(getNombreProducto());
		sb.append(", tipoCampania=");
		sb.append(getTipoCampania());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Campania");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idCampania</column-name><column-value><![CDATA[");
		sb.append(getIdCampania());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoCampania</column-name><column-value><![CDATA[");
		sb.append(getCodigoCampania());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreCampania</column-name><column-value><![CDATA[");
		sb.append(getNombreCampania());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoOferta</column-name><column-value><![CDATA[");
		sb.append(getCodigoOferta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreOferta</column-name><column-value><![CDATA[");
		sb.append(getNombreOferta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoTratamiento</column-name><column-value><![CDATA[");
		sb.append(getCodigoTratamiento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreTratamiento</column-name><column-value><![CDATA[");
		sb.append(getNombreTratamiento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoUnico</column-name><column-value><![CDATA[");
		sb.append(getCodigoUnico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoProducto</column-name><column-value><![CDATA[");
		sb.append(getCodigoProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>core</column-name><column-value><![CDATA[");
		sb.append(getCore());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreProducto</column-name><column-value><![CDATA[");
		sb.append(getNombreProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoCampania</column-name><column-value><![CDATA[");
		sb.append(getTipoCampania());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idCampania;
	private long _idExpediente;
	private String _codigoCampania;
	private String _nombreCampania;
	private String _codigoOferta;
	private String _nombreOferta;
	private String _codigoTratamiento;
	private String _nombreTratamiento;
	private String _codigoUnico;
	private String _codigoProducto;
	private String _core;
	private String _nombreProducto;
	private String _tipoCampania;
	private Date _fechaRegistro;
	private BaseModel<?> _campaniaRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}