
package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HttpHeaderV2
{
	private String consumerId;
	private String messageId;
	private String timestamp;
	private String serviceId;
	private String netId;
	private String userId;
	private String supervisorId;
	private String deviceId;
	private String branchId;
	private String clientId;
	private String branchCode;
	private String channelId;
	private String cardIdType;
	

    public HttpHeaderV2(){
    	super();
    }
    
    @JsonCreator
    public HttpHeaderV2(String timestamp, String consumerId, String messageId, String clientId) {
    	super();
        this.timestamp = timestamp;
        this.consumerId = consumerId; 
        this.messageId = messageId;
        this.clientId = clientId;
    }
    
    @JsonCreator
	public HttpHeaderV2(String consumerId, String messageId, String serviceId, String netId, String userId, String supervisorId, String deviceId, String branchId, String branchCode){
    	super();
		this.consumerId=consumerId;
		this.messageId=messageId;
		this.serviceId=serviceId;
		this.netId=netId;
		this.userId=userId;
		this.supervisorId=supervisorId;
		this.deviceId=deviceId;
		this.branchId=branchId;
		this.branchCode=branchCode;	
	}
    
    @JsonCreator
	public HttpHeaderV2(String consumerId, String messageId, String timestamp, String serviceId, String netId, String userId, String supervisorId, String deviceId, String branchId, String clientId){
    	super();
		this.consumerId=consumerId;
		this.messageId=messageId;
		this.timestamp=timestamp;
		this.serviceId=serviceId;
		this.netId=netId;
		this.userId=userId;
		this.supervisorId=supervisorId;
		this.deviceId=deviceId;
		this.branchId=branchId;
		this.clientId=clientId;	
	}

    @JsonCreator
    public HttpHeaderV2(String consumerId, String messageId, String timestamp,
			String serviceId, String netId, String userId, String supervisorId,
			String deviceId, String clientId, String branchId, 
			String channelId, String cardIdType) {
		super();
		this.consumerId = consumerId;
		this.messageId = messageId;
		this.timestamp = timestamp;
		this.serviceId = serviceId;
		this.netId = netId;
		this.userId = userId;
		this.supervisorId = supervisorId;
		this.deviceId = deviceId;
		this.clientId = clientId;
		this.branchId = branchId;
		this.channelId = channelId;
		this.cardIdType = cardIdType;
	}

	@JsonProperty("X-INT-Consumer-Id")
	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	@JsonProperty("X-INT-Message-Id")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@JsonProperty("X-INT-Timestamp")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@JsonProperty("X-INT-Service-Id")
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@JsonProperty("X-INT-Net-Id")
	public String getNetId() {
		return netId;
	}

	public void setNetId(String netId) {
		this.netId = netId;
	}

	@JsonProperty("X-INT-User-Id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonProperty("X-INT-Supervisor-Id")
	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	@JsonProperty("X-INT-Device-Id")
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@JsonProperty("X-INT-Branch-Id")
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	@JsonProperty("X-IBM-Client-Id")
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonProperty("X-INT-Branch-Code")
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@JsonProperty("X-INT-Channel-Id")
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	@JsonProperty("X-INT-CardId-Type")
	public String getCardIdType() {
		return cardIdType;
	}

	public void setCardIdType(String cardIdType) {
		this.cardIdType = cardIdType;
	}

}
