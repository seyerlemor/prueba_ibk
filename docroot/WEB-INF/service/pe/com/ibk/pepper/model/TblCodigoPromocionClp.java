/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.TblCodigoPromocionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class TblCodigoPromocionClp extends BaseModelImpl<TblCodigoPromocion>
	implements TblCodigoPromocion {
	public TblCodigoPromocionClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return TblCodigoPromocion.class;
	}

	@Override
	public String getModelClassName() {
		return TblCodigoPromocion.class.getName();
	}

	@Override
	public int getPrimaryKey() {
		return _idCodigo;
	}

	@Override
	public void setPrimaryKey(int primaryKey) {
		setIdCodigo(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idCodigo;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idCodigo", getIdCodigo());
		attributes.put("codigo", getCodigo());
		attributes.put("comercio", getComercio());
		attributes.put("estado", getEstado());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idCodigo = (Integer)attributes.get("idCodigo");

		if (idCodigo != null) {
			setIdCodigo(idCodigo);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String comercio = (String)attributes.get("comercio");

		if (comercio != null) {
			setComercio(comercio);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public int getIdCodigo() {
		return _idCodigo;
	}

	@Override
	public void setIdCodigo(int idCodigo) {
		_idCodigo = idCodigo;

		if (_tblCodigoPromocionRemoteModel != null) {
			try {
				Class<?> clazz = _tblCodigoPromocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdCodigo", int.class);

				method.invoke(_tblCodigoPromocionRemoteModel, idCodigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigo() {
		return _codigo;
	}

	@Override
	public void setCodigo(String codigo) {
		_codigo = codigo;

		if (_tblCodigoPromocionRemoteModel != null) {
			try {
				Class<?> clazz = _tblCodigoPromocionRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigo", String.class);

				method.invoke(_tblCodigoPromocionRemoteModel, codigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getComercio() {
		return _comercio;
	}

	@Override
	public void setComercio(String comercio) {
		_comercio = comercio;

		if (_tblCodigoPromocionRemoteModel != null) {
			try {
				Class<?> clazz = _tblCodigoPromocionRemoteModel.getClass();

				Method method = clazz.getMethod("setComercio", String.class);

				method.invoke(_tblCodigoPromocionRemoteModel, comercio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstado() {
		return _estado;
	}

	@Override
	public void setEstado(String estado) {
		_estado = estado;

		if (_tblCodigoPromocionRemoteModel != null) {
			try {
				Class<?> clazz = _tblCodigoPromocionRemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", String.class);

				method.invoke(_tblCodigoPromocionRemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_tblCodigoPromocionRemoteModel != null) {
			try {
				Class<?> clazz = _tblCodigoPromocionRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_tblCodigoPromocionRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTblCodigoPromocionRemoteModel() {
		return _tblCodigoPromocionRemoteModel;
	}

	public void setTblCodigoPromocionRemoteModel(
		BaseModel<?> tblCodigoPromocionRemoteModel) {
		_tblCodigoPromocionRemoteModel = tblCodigoPromocionRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _tblCodigoPromocionRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_tblCodigoPromocionRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TblCodigoPromocionLocalServiceUtil.addTblCodigoPromocion(this);
		}
		else {
			TblCodigoPromocionLocalServiceUtil.updateTblCodigoPromocion(this);
		}
	}

	@Override
	public TblCodigoPromocion toEscapedModel() {
		return (TblCodigoPromocion)ProxyUtil.newProxyInstance(TblCodigoPromocion.class.getClassLoader(),
			new Class[] { TblCodigoPromocion.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TblCodigoPromocionClp clone = new TblCodigoPromocionClp();

		clone.setIdCodigo(getIdCodigo());
		clone.setCodigo(getCodigo());
		clone.setComercio(getComercio());
		clone.setEstado(getEstado());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(TblCodigoPromocion tblCodigoPromocion) {
		int value = 0;

		if (getIdCodigo() < tblCodigoPromocion.getIdCodigo()) {
			value = -1;
		}
		else if (getIdCodigo() > tblCodigoPromocion.getIdCodigo()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TblCodigoPromocionClp)) {
			return false;
		}

		TblCodigoPromocionClp tblCodigoPromocion = (TblCodigoPromocionClp)obj;

		int primaryKey = tblCodigoPromocion.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{idCodigo=");
		sb.append(getIdCodigo());
		sb.append(", codigo=");
		sb.append(getCodigo());
		sb.append(", comercio=");
		sb.append(getComercio());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.TblCodigoPromocion");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idCodigo</column-name><column-value><![CDATA[");
		sb.append(getIdCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigo</column-name><column-value><![CDATA[");
		sb.append(getCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>comercio</column-name><column-value><![CDATA[");
		sb.append(getComercio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _idCodigo;
	private String _codigo;
	private String _comercio;
	private String _estado;
	private Date _fechaRegistro;
	private BaseModel<?> _tblCodigoPromocionRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}