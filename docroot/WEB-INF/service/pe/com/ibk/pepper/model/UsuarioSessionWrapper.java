/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UsuarioSession}.
 * </p>
 *
 * @author Interbank
 * @see UsuarioSession
 * @generated
 */
public class UsuarioSessionWrapper implements UsuarioSession,
	ModelWrapper<UsuarioSession> {
	public UsuarioSessionWrapper(UsuarioSession usuarioSession) {
		_usuarioSession = usuarioSession;
	}

	@Override
	public Class<?> getModelClass() {
		return UsuarioSession.class;
	}

	@Override
	public String getModelClassName() {
		return UsuarioSession.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("idSession", getIdSession());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("tienda", getTienda());
		attributes.put("estado", getEstado());
		attributes.put("userId", getUserId());
		attributes.put("tiendaId", getTiendaId());
		attributes.put("establecimiento", getEstablecimiento());
		attributes.put("nombreVendedor", getNombreVendedor());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idUsuarioSession = (Long)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		String idSession = (String)attributes.get("idSession");

		if (idSession != null) {
			setIdSession(idSession);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String tienda = (String)attributes.get("tienda");

		if (tienda != null) {
			setTienda(tienda);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long tiendaId = (Long)attributes.get("tiendaId");

		if (tiendaId != null) {
			setTiendaId(tiendaId);
		}

		String establecimiento = (String)attributes.get("establecimiento");

		if (establecimiento != null) {
			setEstablecimiento(establecimiento);
		}

		String nombreVendedor = (String)attributes.get("nombreVendedor");

		if (nombreVendedor != null) {
			setNombreVendedor(nombreVendedor);
		}
	}

	/**
	* Returns the primary key of this usuario session.
	*
	* @return the primary key of this usuario session
	*/
	@Override
	public long getPrimaryKey() {
		return _usuarioSession.getPrimaryKey();
	}

	/**
	* Sets the primary key of this usuario session.
	*
	* @param primaryKey the primary key of this usuario session
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_usuarioSession.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id usuario session of this usuario session.
	*
	* @return the id usuario session of this usuario session
	*/
	@Override
	public long getIdUsuarioSession() {
		return _usuarioSession.getIdUsuarioSession();
	}

	/**
	* Sets the id usuario session of this usuario session.
	*
	* @param idUsuarioSession the id usuario session of this usuario session
	*/
	@Override
	public void setIdUsuarioSession(long idUsuarioSession) {
		_usuarioSession.setIdUsuarioSession(idUsuarioSession);
	}

	/**
	* Returns the id session of this usuario session.
	*
	* @return the id session of this usuario session
	*/
	@Override
	public java.lang.String getIdSession() {
		return _usuarioSession.getIdSession();
	}

	/**
	* Sets the id session of this usuario session.
	*
	* @param idSession the id session of this usuario session
	*/
	@Override
	public void setIdSession(java.lang.String idSession) {
		_usuarioSession.setIdSession(idSession);
	}

	/**
	* Returns the fecha registro of this usuario session.
	*
	* @return the fecha registro of this usuario session
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _usuarioSession.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this usuario session.
	*
	* @param fechaRegistro the fecha registro of this usuario session
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_usuarioSession.setFechaRegistro(fechaRegistro);
	}

	/**
	* Returns the tienda of this usuario session.
	*
	* @return the tienda of this usuario session
	*/
	@Override
	public java.lang.String getTienda() {
		return _usuarioSession.getTienda();
	}

	/**
	* Sets the tienda of this usuario session.
	*
	* @param tienda the tienda of this usuario session
	*/
	@Override
	public void setTienda(java.lang.String tienda) {
		_usuarioSession.setTienda(tienda);
	}

	/**
	* Returns the estado of this usuario session.
	*
	* @return the estado of this usuario session
	*/
	@Override
	public java.lang.String getEstado() {
		return _usuarioSession.getEstado();
	}

	/**
	* Sets the estado of this usuario session.
	*
	* @param estado the estado of this usuario session
	*/
	@Override
	public void setEstado(java.lang.String estado) {
		_usuarioSession.setEstado(estado);
	}

	/**
	* Returns the user ID of this usuario session.
	*
	* @return the user ID of this usuario session
	*/
	@Override
	public long getUserId() {
		return _usuarioSession.getUserId();
	}

	/**
	* Sets the user ID of this usuario session.
	*
	* @param userId the user ID of this usuario session
	*/
	@Override
	public void setUserId(long userId) {
		_usuarioSession.setUserId(userId);
	}

	/**
	* Returns the user uuid of this usuario session.
	*
	* @return the user uuid of this usuario session
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSession.getUserUuid();
	}

	/**
	* Sets the user uuid of this usuario session.
	*
	* @param userUuid the user uuid of this usuario session
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_usuarioSession.setUserUuid(userUuid);
	}

	/**
	* Returns the tienda ID of this usuario session.
	*
	* @return the tienda ID of this usuario session
	*/
	@Override
	public long getTiendaId() {
		return _usuarioSession.getTiendaId();
	}

	/**
	* Sets the tienda ID of this usuario session.
	*
	* @param tiendaId the tienda ID of this usuario session
	*/
	@Override
	public void setTiendaId(long tiendaId) {
		_usuarioSession.setTiendaId(tiendaId);
	}

	/**
	* Returns the establecimiento of this usuario session.
	*
	* @return the establecimiento of this usuario session
	*/
	@Override
	public java.lang.String getEstablecimiento() {
		return _usuarioSession.getEstablecimiento();
	}

	/**
	* Sets the establecimiento of this usuario session.
	*
	* @param establecimiento the establecimiento of this usuario session
	*/
	@Override
	public void setEstablecimiento(java.lang.String establecimiento) {
		_usuarioSession.setEstablecimiento(establecimiento);
	}

	/**
	* Returns the nombre vendedor of this usuario session.
	*
	* @return the nombre vendedor of this usuario session
	*/
	@Override
	public java.lang.String getNombreVendedor() {
		return _usuarioSession.getNombreVendedor();
	}

	/**
	* Sets the nombre vendedor of this usuario session.
	*
	* @param nombreVendedor the nombre vendedor of this usuario session
	*/
	@Override
	public void setNombreVendedor(java.lang.String nombreVendedor) {
		_usuarioSession.setNombreVendedor(nombreVendedor);
	}

	@Override
	public boolean isNew() {
		return _usuarioSession.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_usuarioSession.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _usuarioSession.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_usuarioSession.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _usuarioSession.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _usuarioSession.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_usuarioSession.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _usuarioSession.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_usuarioSession.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_usuarioSession.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_usuarioSession.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UsuarioSessionWrapper((UsuarioSession)_usuarioSession.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.UsuarioSession usuarioSession) {
		return _usuarioSession.compareTo(usuarioSession);
	}

	@Override
	public int hashCode() {
		return _usuarioSession.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.UsuarioSession> toCacheModel() {
		return _usuarioSession.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.UsuarioSession toEscapedModel() {
		return new UsuarioSessionWrapper(_usuarioSession.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.UsuarioSession toUnescapedModel() {
		return new UsuarioSessionWrapper(_usuarioSession.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _usuarioSession.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _usuarioSession.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_usuarioSession.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UsuarioSessionWrapper)) {
			return false;
		}

		UsuarioSessionWrapper usuarioSessionWrapper = (UsuarioSessionWrapper)obj;

		if (Validator.equals(_usuarioSession,
					usuarioSessionWrapper._usuarioSession)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UsuarioSession getWrappedUsuarioSession() {
		return _usuarioSession;
	}

	@Override
	public UsuarioSession getWrappedModel() {
		return _usuarioSession;
	}

	@Override
	public void resetOriginalValues() {
		_usuarioSession.resetOriginalValues();
	}

	private UsuarioSession _usuarioSession;
}