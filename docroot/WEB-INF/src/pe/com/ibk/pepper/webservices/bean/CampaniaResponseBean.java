package pe.com.ibk.pepper.webservices.bean;

import java.util.ArrayList;
import java.util.List;

import pe.com.ibk.pepper.util.Validador;

public class CampaniaResponseBean {


	private List<CampaniaBean> campanias;
	private String codigoUnico;
	private String tipoDocumento;

	
	public List<CampaniaBean> getCampanias() {
		if (campanias == null) {
			campanias = new ArrayList<>();
		}
		return campanias;
	}

	public String getCodigoUnico() {
		return codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public void setCampanias(List<CampaniaBean> campanias) {
		this.campanias = campanias;
	}

	public class CampaniaBean {

		private AdicionalResponseBean adicionalResponseBean;
		private DetalleResponseBean detalleResponseBean;
		private CabeceraResponseBean cabeceraResponseBean;
		private OfertaResponseBean  ofertaResponseBean;


		public OfertaResponseBean getOfertaResponseBean() {
			if (ofertaResponseBean == null) {
				ofertaResponseBean= new OfertaResponseBean();
			}
			return ofertaResponseBean;
		}

		public void setOfertaResponseBean(OfertaResponseBean ofertaResponseBean) {
			this.ofertaResponseBean = ofertaResponseBean;
		}

		public DetalleResponseBean getDetalleResponseBean() {
			if (detalleResponseBean == null) {
				detalleResponseBean = new DetalleResponseBean();
			}
			return detalleResponseBean;
		}

		public void setDetalleResponseBean(DetalleResponseBean detalleResponseBean) {
			this.detalleResponseBean = detalleResponseBean;
		}

		public CabeceraResponseBean getCabeceraResponseBean() {
			if (cabeceraResponseBean == null) {
				cabeceraResponseBean = new CabeceraResponseBean();
			}
			return cabeceraResponseBean;
		}

		public void setCabeceraResponseBean(CabeceraResponseBean cabeceraResponseBean) {
			this.cabeceraResponseBean = cabeceraResponseBean;
		}

		public AdicionalResponseBean getAdicionalResponseBean() {
			if (adicionalResponseBean == null) {
				adicionalResponseBean = new AdicionalResponseBean();
			}
			return adicionalResponseBean;
		}

		public void setAdicionalResponseBean(AdicionalResponseBean adicionalResponseBean) {
			this.adicionalResponseBean = adicionalResponseBean;
		}

		@Override
		public String toString() {
				
			return (Validador.isNotNull(ofertaResponseBean))?
					"CampaniaBean [adicionalResponseBean="
					+ adicionalResponseBean.toString() + ", detalleResponseBean="
					+ detalleResponseBean.toString() + ", cabeceraResponseBean="
					+ cabeceraResponseBean.toString() + ", OfertaResponseBean="
					+ ofertaResponseBean.toString() + "]":"CampaniaBean [adicionalResponseBean="
					+ adicionalResponseBean.toString() + ", detalleResponseBean="
					+ detalleResponseBean.toString() + ", cabeceraResponseBean="
					+ cabeceraResponseBean.toString();
		}

	}

	public class AdicionalResponseBean {

		private String descripcionMarcaTarjeta;
		private String descripcionTipoTarjeta;
		public String getDescripcionMarcaTarjeta() {
			return descripcionMarcaTarjeta;
		}
		
		public void setDescripcionMarcaTarjeta(String descripcionMarcaTarjeta) {
			this.descripcionMarcaTarjeta = descripcionMarcaTarjeta;
		}
		public String getDescripcionTipoTarjeta() {
			return descripcionTipoTarjeta;
		}
		public void setDescripcionTipoTarjeta(String descripcionTipoTarjeta) {
			this.descripcionTipoTarjeta = descripcionTipoTarjeta;
		}
		
		@Override
		public String toString() {
			return "AdicionalResponseBean [descripcionMarcaTarjeta="
					+ descripcionMarcaTarjeta + ", descripcionTipoTarjeta="
					+ descripcionTipoTarjeta + "]";
		}
	}
	

	public class DetalleResponseBean {

		private String marcaTarjeta;
		private String tipoTarjeta;
		private String monto;
		private String tasa2;
		private String tipoCliente;
		private String cobroMembresia;
		private String tea;
		private String seguroDesgravamen; 
		private String idMoneda;
		
		public String getIdMoneda() {
			return idMoneda;
		}
		public void setIdMoneda(String idMoneda) {
			this.idMoneda = idMoneda;
		}
		public String getCobroMembresia() {
			return cobroMembresia;
		}
		public void setCobroMembresia(String cobroMembresia) {
			this.cobroMembresia = cobroMembresia;
		}
		public String getTea() {
			return tea;
		}
		public void setTea(String tea) {
			this.tea = tea;
		}
		public String getSeguroDesgravamen() {
			return seguroDesgravamen;
		}
		public void setSeguroDesgravamen(String seguroDesgravamen) {
			this.seguroDesgravamen = seguroDesgravamen;
		}
		public String getMonto() {
			return monto;
		}
		public void setMonto(String monto) {
			this.monto = monto;
		}
		public String getTasa2() {
			return tasa2;
		}
		public void setTasa2(String tasa2) {
			this.tasa2 = tasa2;
		}
		public String getTipoCliente() {
			return tipoCliente;
		}
		public void setTipoCliente(String tipoCliente) {
			this.tipoCliente = tipoCliente;
		}
		public String getMarcaTarjeta() {
			return marcaTarjeta;
		}
		public void setMarcaTarjeta(String marcaTarjeta) {
			this.marcaTarjeta = marcaTarjeta;
		}
		public String getTipoTarjeta() {
			return tipoTarjeta;
		}
		public void setTipoTarjeta(String tipoTarjeta) {
			this.tipoTarjeta = tipoTarjeta;
		}
		
		@Override
		public String toString() {
			return "DetalleResponseBean [marcaTarjeta=" + marcaTarjeta
					+ ", tipoTarjeta=" + tipoTarjeta + "]";
		}
	}
	
	public class CabeceraResponseBean{
		
		private String codigoCampania;
	    private String nombreCampania;
	    private String codigoProducto;

		public String getCodigoProducto() {
			return codigoProducto;
		}

		public void setCodigoProducto(String codigoProducto) {
			this.codigoProducto = codigoProducto;
		}

		public String getNombreCampania() {
			return nombreCampania;
		}
		
		public void setNombreCampania(String nombreCampania) {
			this.nombreCampania = nombreCampania;
		}
		public String getCodigoCampania() {
			return codigoCampania;
		}
		public void setCodigoCampania(String codigoCampania) {
			this.codigoCampania = codigoCampania;
		}
	
		
		@Override
		public String toString() {
			return "CabeceraResponseBean [codigoCampania=" + codigoCampania
					+ ", nombreCampania=" + nombreCampania + "]";
		}
	}
	
	public class OfertaResponseBean{
		private String codigoOferta;
		private String nombreOferta;
		private String codigoTratamiento;
		private String nombreTratamiento;
		private String codigoProductoRespuesta;
	    private String coreProductoRespuesta;
	    private String nombreProductoRespuesta;
	    
		public String getCodigoProductoRespuesta() {
			return codigoProductoRespuesta;
		}

		public void setCodigoProductoRespuesta(String codigoProductoRespuesta) {
			this.codigoProductoRespuesta = codigoProductoRespuesta;
		}

		public String getCoreProductoRespuesta() {
			return coreProductoRespuesta;
		}

		public void setCoreProductoRespuesta(String coreProductoRespuesta) {
			this.coreProductoRespuesta = coreProductoRespuesta;
		}

		public String getNombreProductoRespuesta() {
			return nombreProductoRespuesta;
		}

		public void setNombreProductoRespuesta(String nombreProductoRespuesta) {
			this.nombreProductoRespuesta = nombreProductoRespuesta;
		}
		
		public String getCodigoOferta() {
			return codigoOferta;
		}
		public void setCodigoOferta(String codigoOferta) {
			this.codigoOferta = codigoOferta;
		}
		public String getNombreOferta() {
			return nombreOferta;
		}
		public void setNombreOferta(String nombreOferta) {
			this.nombreOferta = nombreOferta;
		}
		public String getCodigoTratamiento() {
			return codigoTratamiento;
		}
		public void setCodigoTratamiento(String codigoTratamiento) {
			this.codigoTratamiento = codigoTratamiento;
		}
		public String getNombreTratamiento() {
			return nombreTratamiento;
		}
		public void setNombreTratamiento(String nombreTratamiento) {
			this.nombreTratamiento = nombreTratamiento;
		}
	
		@Override
		public String toString() {
			return "OfertaResponseBean [codigoOferta=" + codigoOferta
					+ ", nombreOferta=" + nombreOferta + ", codigoTratamiento="
					+ codigoTratamiento + ", nombreTratamiento="
					+ nombreTratamiento + ", codigoProductoRespuesta="
					+ codigoProductoRespuesta + ", coreProductoRespuesta="
					+ coreProductoRespuesta + ", nombreProductoRespuesta="
					+ nombreProductoRespuesta + "]";
		}
	}
}
