package pe.com.ibk.pepper.bean;

public class ServiceBean {

	private String requestString;
	private String messageId;
	private String endPoint;
	private String path;
	private String nombreWs;
	private String htmlError;
	private Boolean flagAutenticacion;
	private String metodoHttp;
	private Boolean esRestrictivo;
	
	public ServiceBean(String requestString, String messageId, String endPoint,
			String path, String nombreWs, String htmlError,
			Boolean flagAutenticacion, String metodoHttp, Boolean esRestrictivo) {
		super();
		this.requestString = requestString;
		this.messageId = messageId;
		this.endPoint = endPoint;
		this.path = path;
		this.nombreWs = nombreWs;
		this.htmlError = htmlError;
		this.flagAutenticacion = flagAutenticacion;
		this.metodoHttp = metodoHttp;
		this.esRestrictivo = esRestrictivo;
	}
	public String getRequestString() {
		return requestString;
	}
	public void setRequestString(String requestString) {
		this.requestString = requestString;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getNombreWs() {
		return nombreWs;
	}
	public void setNombreWs(String nombreWs) {
		this.nombreWs = nombreWs;
	}
	public String getHtmlError() {
		return htmlError;
	}
	public void setHtmlError(String htmlError) {
		this.htmlError = htmlError;
	}
	public Boolean getFlagAutenticacion() {
		return flagAutenticacion;
	}
	public void setFlagAutenticacion(Boolean flagAutenticacion) {
		this.flagAutenticacion = flagAutenticacion;
	}
	public String getMetodoHttp() {
		return metodoHttp;
	}
	public void setMetodoHttp(String metodoHttp) {
		this.metodoHttp = metodoHttp;
	}
	public Boolean getEsRestrictivo() {
		return esRestrictivo;
	}
	public void setEsRestrictivo(Boolean esRestrictivo) {
		this.esRestrictivo = esRestrictivo;
	}
	@Override
	public String toString() {
		return "ServiceBean [requestString=" + requestString + ", messageId="
				+ messageId + ", endPoint=" + endPoint + ", path=" + path
				+ ", nombreWs=" + nombreWs + ", htmlError=" + htmlError
				+ ", flagAutenticacion=" + flagAutenticacion + ", metodoHttp="
				+ metodoHttp + ", esRestrictivo=" + esRestrictivo + "]";
	}
	
}
