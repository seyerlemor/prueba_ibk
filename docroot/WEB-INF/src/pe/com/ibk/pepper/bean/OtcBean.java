package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class OtcBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idExpediente;
	private int idOtc;
	private String tipoFlujo;
	
	//Generar POTC
	private String potc;
	private Boolean aceptado;
	private String mensaje;
	
	private int idProductoUsuario;
	private String idProducto;
	
	//Validacion Token
	private Boolean validado;
	private String codigoSms;
	private String flagEnvioToken;
	private String token;
	private String fechaHoraOperacion;
	
	
	public String getFechaHoraOperacion() {
		return fechaHoraOperacion;
	}
	public void setFechaHoraOperacion(String fechaHoraOperacion) {
		this.fechaHoraOperacion = fechaHoraOperacion;
	}
	public String getFlagEnvioToken() {
		return flagEnvioToken;
	}
	public void setFlagEnvioToken(String flagEnvioToken) {
		this.flagEnvioToken = flagEnvioToken;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCodigoSms() {
		return codigoSms;
	}
	public void setCodigoSms(String codigoSms) {
		this.codigoSms = codigoSms;
	}
	public int getIdProductoUsuario() {
		return idProductoUsuario;
	}
	public void setIdProductoUsuario(int idProductoUsuario) {
		this.idProductoUsuario = idProductoUsuario;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public int getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	public int getIdOtc() {
		return idOtc;
	}
	public void setIdOtc(int idOtc) {
		this.idOtc = idOtc;
	}
	public String getTipoFlujo() {
		return tipoFlujo;
	}
	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}
	public String getPotc() {
		return potc;
	}
	public void setPotc(String potc) {
		this.potc = potc;
	}
	public Boolean getAceptado() {
		return aceptado;
	}
	public void setAceptado(Boolean aceptado) {
		this.aceptado = aceptado;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Boolean getValidado() {
		return validado;
	}
	public void setValidado(Boolean validado) {
		this.validado = validado;
	}
	@Override
	public String toString() {
		return "OtcBean [idExpediente=" + idExpediente + ", idOtc=" + idOtc
				+ ", tipoFlujo=" + tipoFlujo + ", potc=" + potc + ", aceptado="
				+ aceptado + ", mensaje=" + mensaje + ", idProductoUsuario="
				+ idProductoUsuario + ", idProducto=" + idProducto
				+ ", validado=" + validado + ", codigoSms=" + codigoSms
				+ ", flagEnvioToken=" + flagEnvioToken + ", token=" + token
				+ "]";
	}

}
