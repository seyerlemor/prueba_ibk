/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchLogQRadarException;
import pe.com.ibk.pepper.model.LogQRadar;
import pe.com.ibk.pepper.model.impl.LogQRadarImpl;
import pe.com.ibk.pepper.model.impl.LogQRadarModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the log q radar service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see LogQRadarPersistence
 * @see LogQRadarUtil
 * @generated
 */
public class LogQRadarPersistenceImpl extends BasePersistenceImpl<LogQRadar>
	implements LogQRadarPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LogQRadarUtil} to access the log q radar persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LogQRadarImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
			LogQRadarModelImpl.FINDER_CACHE_ENABLED, LogQRadarImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
			LogQRadarModelImpl.FINDER_CACHE_ENABLED, LogQRadarImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
			LogQRadarModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public LogQRadarPersistenceImpl() {
		setModelClass(LogQRadar.class);
	}

	/**
	 * Caches the log q radar in the entity cache if it is enabled.
	 *
	 * @param logQRadar the log q radar
	 */
	@Override
	public void cacheResult(LogQRadar logQRadar) {
		EntityCacheUtil.putResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
			LogQRadarImpl.class, logQRadar.getPrimaryKey(), logQRadar);

		logQRadar.resetOriginalValues();
	}

	/**
	 * Caches the log q radars in the entity cache if it is enabled.
	 *
	 * @param logQRadars the log q radars
	 */
	@Override
	public void cacheResult(List<LogQRadar> logQRadars) {
		for (LogQRadar logQRadar : logQRadars) {
			if (EntityCacheUtil.getResult(
						LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
						LogQRadarImpl.class, logQRadar.getPrimaryKey()) == null) {
				cacheResult(logQRadar);
			}
			else {
				logQRadar.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all log q radars.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LogQRadarImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LogQRadarImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the log q radar.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LogQRadar logQRadar) {
		EntityCacheUtil.removeResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
			LogQRadarImpl.class, logQRadar.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LogQRadar> logQRadars) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LogQRadar logQRadar : logQRadars) {
			EntityCacheUtil.removeResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
				LogQRadarImpl.class, logQRadar.getPrimaryKey());
		}
	}

	/**
	 * Creates a new log q radar with the primary key. Does not add the log q radar to the database.
	 *
	 * @param idLogQRadar the primary key for the new log q radar
	 * @return the new log q radar
	 */
	@Override
	public LogQRadar create(int idLogQRadar) {
		LogQRadar logQRadar = new LogQRadarImpl();

		logQRadar.setNew(true);
		logQRadar.setPrimaryKey(idLogQRadar);

		return logQRadar;
	}

	/**
	 * Removes the log q radar with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idLogQRadar the primary key of the log q radar
	 * @return the log q radar that was removed
	 * @throws pe.com.ibk.pepper.NoSuchLogQRadarException if a log q radar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LogQRadar remove(int idLogQRadar)
		throws NoSuchLogQRadarException, SystemException {
		return remove((Serializable)idLogQRadar);
	}

	/**
	 * Removes the log q radar with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the log q radar
	 * @return the log q radar that was removed
	 * @throws pe.com.ibk.pepper.NoSuchLogQRadarException if a log q radar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LogQRadar remove(Serializable primaryKey)
		throws NoSuchLogQRadarException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LogQRadar logQRadar = (LogQRadar)session.get(LogQRadarImpl.class,
					primaryKey);

			if (logQRadar == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLogQRadarException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(logQRadar);
		}
		catch (NoSuchLogQRadarException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LogQRadar removeImpl(LogQRadar logQRadar)
		throws SystemException {
		logQRadar = toUnwrappedModel(logQRadar);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(logQRadar)) {
				logQRadar = (LogQRadar)session.get(LogQRadarImpl.class,
						logQRadar.getPrimaryKeyObj());
			}

			if (logQRadar != null) {
				session.delete(logQRadar);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (logQRadar != null) {
			clearCache(logQRadar);
		}

		return logQRadar;
	}

	@Override
	public LogQRadar updateImpl(pe.com.ibk.pepper.model.LogQRadar logQRadar)
		throws SystemException {
		logQRadar = toUnwrappedModel(logQRadar);

		boolean isNew = logQRadar.isNew();

		Session session = null;

		try {
			session = openSession();

			if (logQRadar.isNew()) {
				session.save(logQRadar);

				logQRadar.setNew(false);
			}
			else {
				session.merge(logQRadar);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
			LogQRadarImpl.class, logQRadar.getPrimaryKey(), logQRadar);

		return logQRadar;
	}

	protected LogQRadar toUnwrappedModel(LogQRadar logQRadar) {
		if (logQRadar instanceof LogQRadarImpl) {
			return logQRadar;
		}

		LogQRadarImpl logQRadarImpl = new LogQRadarImpl();

		logQRadarImpl.setNew(logQRadar.isNew());
		logQRadarImpl.setPrimaryKey(logQRadar.getPrimaryKey());

		logQRadarImpl.setIdLogQRadar(logQRadar.getIdLogQRadar());
		logQRadarImpl.setPrioridad(logQRadar.getPrioridad());
		logQRadarImpl.setSeveridad(logQRadar.getSeveridad());
		logQRadarImpl.setRecurso(logQRadar.getRecurso());
		logQRadarImpl.setUtc(logQRadar.getUtc());
		logQRadarImpl.setLogCreador(logQRadar.getLogCreador());
		logQRadarImpl.setTipoEvento(logQRadar.getTipoEvento());
		logQRadarImpl.setRegistroUsuario(logQRadar.getRegistroUsuario());
		logQRadarImpl.setOrigenEvento(logQRadar.getOrigenEvento());
		logQRadarImpl.setRespuesta(logQRadar.getRespuesta());
		logQRadarImpl.setAmbiente(logQRadar.getAmbiente());
		logQRadarImpl.setProducto(logQRadar.getProducto());
		logQRadarImpl.setIdProducto(logQRadar.getIdProducto());
		logQRadarImpl.setFechaHora(logQRadar.getFechaHora());

		return logQRadarImpl;
	}

	/**
	 * Returns the log q radar with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the log q radar
	 * @return the log q radar
	 * @throws pe.com.ibk.pepper.NoSuchLogQRadarException if a log q radar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LogQRadar findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLogQRadarException, SystemException {
		LogQRadar logQRadar = fetchByPrimaryKey(primaryKey);

		if (logQRadar == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLogQRadarException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return logQRadar;
	}

	/**
	 * Returns the log q radar with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchLogQRadarException} if it could not be found.
	 *
	 * @param idLogQRadar the primary key of the log q radar
	 * @return the log q radar
	 * @throws pe.com.ibk.pepper.NoSuchLogQRadarException if a log q radar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LogQRadar findByPrimaryKey(int idLogQRadar)
		throws NoSuchLogQRadarException, SystemException {
		return findByPrimaryKey((Serializable)idLogQRadar);
	}

	/**
	 * Returns the log q radar with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the log q radar
	 * @return the log q radar, or <code>null</code> if a log q radar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LogQRadar fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		LogQRadar logQRadar = (LogQRadar)EntityCacheUtil.getResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
				LogQRadarImpl.class, primaryKey);

		if (logQRadar == _nullLogQRadar) {
			return null;
		}

		if (logQRadar == null) {
			Session session = null;

			try {
				session = openSession();

				logQRadar = (LogQRadar)session.get(LogQRadarImpl.class,
						primaryKey);

				if (logQRadar != null) {
					cacheResult(logQRadar);
				}
				else {
					EntityCacheUtil.putResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
						LogQRadarImpl.class, primaryKey, _nullLogQRadar);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(LogQRadarModelImpl.ENTITY_CACHE_ENABLED,
					LogQRadarImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return logQRadar;
	}

	/**
	 * Returns the log q radar with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idLogQRadar the primary key of the log q radar
	 * @return the log q radar, or <code>null</code> if a log q radar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LogQRadar fetchByPrimaryKey(int idLogQRadar)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idLogQRadar);
	}

	/**
	 * Returns all the log q radars.
	 *
	 * @return the log q radars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LogQRadar> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the log q radars.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.LogQRadarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of log q radars
	 * @param end the upper bound of the range of log q radars (not inclusive)
	 * @return the range of log q radars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LogQRadar> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the log q radars.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.LogQRadarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of log q radars
	 * @param end the upper bound of the range of log q radars (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of log q radars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<LogQRadar> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LogQRadar> list = (List<LogQRadar>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LOGQRADAR);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LOGQRADAR;

				if (pagination) {
					sql = sql.concat(LogQRadarModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LogQRadar>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<LogQRadar>(list);
				}
				else {
					list = (List<LogQRadar>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the log q radars from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (LogQRadar logQRadar : findAll()) {
			remove(logQRadar);
		}
	}

	/**
	 * Returns the number of log q radars.
	 *
	 * @return the number of log q radars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LOGQRADAR);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the log q radar persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.LogQRadar")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LogQRadar>> listenersList = new ArrayList<ModelListener<LogQRadar>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LogQRadar>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LogQRadarImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_LOGQRADAR = "SELECT logQRadar FROM LogQRadar logQRadar";
	private static final String _SQL_COUNT_LOGQRADAR = "SELECT COUNT(logQRadar) FROM LogQRadar logQRadar";
	private static final String _ORDER_BY_ENTITY_ALIAS = "logQRadar.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LogQRadar exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LogQRadarPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idLogQRadar", "prioridad", "severidad", "recurso", "utc",
				"logCreador", "tipoEvento", "registroUsuario", "origenEvento",
				"respuesta", "ambiente", "producto", "idProducto", "fechaHora"
			});
	private static LogQRadar _nullLogQRadar = new LogQRadarImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LogQRadar> toCacheModel() {
				return _nullLogQRadarCacheModel;
			}
		};

	private static CacheModel<LogQRadar> _nullLogQRadarCacheModel = new CacheModel<LogQRadar>() {
			@Override
			public LogQRadar toEntityModel() {
				return _nullLogQRadar;
			}
		};
}