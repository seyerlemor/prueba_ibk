/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.Campania;

/**
 * The persistence interface for the campania service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see CampaniaPersistenceImpl
 * @see CampaniaUtil
 * @generated
 */
public interface CampaniaPersistence extends BasePersistence<Campania> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CampaniaUtil} to access the campania persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the campania where idCampania = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchCampaniaException} if it could not be found.
	*
	* @param idCampania the id campania
	* @return the matching campania
	* @throws pe.com.ibk.pepper.NoSuchCampaniaException if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania findByC_ID(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	/**
	* Returns the campania where idCampania = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idCampania the id campania
	* @return the matching campania, or <code>null</code> if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania fetchByC_ID(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the campania where idCampania = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idCampania the id campania
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching campania, or <code>null</code> if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania fetchByC_ID(long idCampania,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the campania where idCampania = &#63; from the database.
	*
	* @param idCampania the id campania
	* @return the campania that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania removeByC_ID(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	/**
	* Returns the number of campanias where idCampania = &#63;.
	*
	* @param idCampania the id campania
	* @return the number of matching campanias
	* @throws SystemException if a system exception occurred
	*/
	public int countByC_ID(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the campanias where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the matching campanias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Campania> findByIdExpediente(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the campanias where idExpediente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param start the lower bound of the range of campanias
	* @param end the upper bound of the range of campanias (not inclusive)
	* @return the range of matching campanias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Campania> findByIdExpediente(
		long idExpediente, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the campanias where idExpediente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param start the lower bound of the range of campanias
	* @param end the upper bound of the range of campanias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching campanias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Campania> findByIdExpediente(
		long idExpediente, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first campania in the ordered set where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching campania
	* @throws pe.com.ibk.pepper.NoSuchCampaniaException if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania findByIdExpediente_First(
		long idExpediente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	/**
	* Returns the first campania in the ordered set where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching campania, or <code>null</code> if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania fetchByIdExpediente_First(
		long idExpediente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last campania in the ordered set where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching campania
	* @throws pe.com.ibk.pepper.NoSuchCampaniaException if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania findByIdExpediente_Last(
		long idExpediente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	/**
	* Returns the last campania in the ordered set where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching campania, or <code>null</code> if a matching campania could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania fetchByIdExpediente_Last(
		long idExpediente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the campanias before and after the current campania in the ordered set where idExpediente = &#63;.
	*
	* @param idCampania the primary key of the current campania
	* @param idExpediente the id expediente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next campania
	* @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania[] findByIdExpediente_PrevAndNext(
		long idCampania, long idExpediente,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	/**
	* Removes all the campanias where idExpediente = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @throws SystemException if a system exception occurred
	*/
	public void removeByIdExpediente(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of campanias where idExpediente = &#63;.
	*
	* @param idExpediente the id expediente
	* @return the number of matching campanias
	* @throws SystemException if a system exception occurred
	*/
	public int countByIdExpediente(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the campania in the entity cache if it is enabled.
	*
	* @param campania the campania
	*/
	public void cacheResult(pe.com.ibk.pepper.model.Campania campania);

	/**
	* Caches the campanias in the entity cache if it is enabled.
	*
	* @param campanias the campanias
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Campania> campanias);

	/**
	* Creates a new campania with the primary key. Does not add the campania to the database.
	*
	* @param idCampania the primary key for the new campania
	* @return the new campania
	*/
	public pe.com.ibk.pepper.model.Campania create(long idCampania);

	/**
	* Removes the campania with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCampania the primary key of the campania
	* @return the campania that was removed
	* @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania remove(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	public pe.com.ibk.pepper.model.Campania updateImpl(
		pe.com.ibk.pepper.model.Campania campania)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the campania with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchCampaniaException} if it could not be found.
	*
	* @param idCampania the primary key of the campania
	* @return the campania
	* @throws pe.com.ibk.pepper.NoSuchCampaniaException if a campania with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania findByPrimaryKey(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchCampaniaException;

	/**
	* Returns the campania with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idCampania the primary key of the campania
	* @return the campania, or <code>null</code> if a campania with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Campania fetchByPrimaryKey(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the campanias.
	*
	* @return the campanias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Campania> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the campanias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of campanias
	* @param end the upper bound of the range of campanias (not inclusive)
	* @return the range of campanias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Campania> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the campanias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of campanias
	* @param end the upper bound of the range of campanias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of campanias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Campania> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the campanias from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of campanias.
	*
	* @return the number of campanias
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}