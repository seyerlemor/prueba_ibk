/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author Interbank
 */
public class ClienteTransaccionFinderUtil {
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> obtenerTransacciones(
		long idExpediente, java.lang.String tipoFlujo) {
		return getFinder().obtenerTransacciones(idExpediente, tipoFlujo);
	}

	public static ClienteTransaccionFinder getFinder() {
		if (_finder == null) {
			_finder = (ClienteTransaccionFinder)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ClienteTransaccionFinder.class.getName());

			ReferenceRegistry.registerReference(ClienteTransaccionFinderUtil.class,
				"_finder");
		}

		return _finder;
	}

	public void setFinder(ClienteTransaccionFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(ClienteTransaccionFinderUtil.class,
			"_finder");
	}

	private static ClienteTransaccionFinder _finder;
}