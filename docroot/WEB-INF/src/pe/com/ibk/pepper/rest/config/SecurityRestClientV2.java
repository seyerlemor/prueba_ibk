package pe.com.ibk.pepper.rest.config;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;

import java.io.File;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.SslConfigurator;

import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;



public class SecurityRestClientV2 {
  private static final String SSL = "SSL";

  private static Log logger =  LogFactoryUtil.getLog(SecurityRestClientV2.class);

  private SecurityRestClientV2() {
    throw new IllegalAccessError("Clase EstÃ¡tica");
  }
  
  /**
   * Inicializacion de SecurityRestClient
   * @param host
   * @param path
   * @param user
   * @param password
   * @return
   */
  public static Builder init(String host, String path, String headerName, String headerValue, String tipoServicio, MultivaluedMap<String, Object> headers) {
    try {
    	HostnameVerifier hostnameVerifier = new InsecureHostnameVerifier();
    	logger.debug("::host::>" + host);
    	logger.debug(":path::>" + path);
    	logger.debug(":headerName::>" + headerName);
    	logger.debug(":headerValue::>" + headerValue);
    	Client client = ClientBuilder.newBuilder().sslContext(getSSLContextCertified(tipoServicio))
          .hostnameVerifier(hostnameVerifier).build();
    	WebTarget target = client.target(host).path(path);
    	return target.request(MediaType.APPLICATION_JSON_TYPE).headers(headers);
//    	return target.request(MediaType.APPLICATION_JSON_TYPE).header(headerName, headerValue);
    } catch (Exception e) {
    	logger.error("Excepcion el la inicializacion de SecurityRestClient ", e);
    }
    return null;
  }

  /**
   * Retorna el protocolo SSL del contexto actual
   * 
   * @return SSLContext Contexto con protocolo SSL
   */
  public static SSLContext getSSLContextCertified(String tipoServicio) {
    SSLContext context = null;
    try {
    	
    	String httpProtocols = System.getProperty("https.protocols");
    	logger.debug("httpProtocols::>"+httpProtocols); 
        System.setProperty("https.protocols", "TLSv1.2");
        if (httpProtocols != null) {
            System.setProperty("https.protocols", httpProtocols);
        }
    	String resourceDir = PrefsPropsUtil.getString(PortalKeys.REST_UBICACION_JKS, PortalValues.REST_UBICACION_JKS);
    	SslConfigurator sslConfig = null;
    	 if(tipoServicio.equals(ConstantesRest.SERVICIO_API)){
    		       sslConfig = SslConfigurator.newInstance()
    		    		   .keyStoreFile(resourceDir + File.separator
    		    		              + PrefsPropsUtil.getString(PortalKeys.REST_API_JKS_NAME, PortalValues.REST_API_JKS_NAME))
    		    		          .keyPassword(PrefsPropsUtil.getString(PortalKeys.REST_API_JKS_PASSW, PortalValues.REST_API_JKS_PASSW))
    		    		          .keyStoreType("JKS")
    		          .securityProtocol(SSL);
    		      context = sslConfig.createSSLContext();
    	 } else if(tipoServicio.equals(ConstantesRest.SERVICIO_BUS)){
    		       sslConfig = SslConfigurator.newInstance()
    		          .keyStoreFile(resourceDir + File.separator
    		              + PrefsPropsUtil.getString(PortalKeys.REST_BUS_JKS_NAME, PortalValues.REST_BUS_JKS_NAME))
    		          .keyPassword(PrefsPropsUtil.getString(PortalKeys.REST_BUS_JKS_PASSW, PortalValues.REST_BUS_JKS_PASSW))
    		          .keyStoreType("JKS")
    		          .securityProtocol(SSL);
    		      context = sslConfig.createSSLContext();
    	 }
        context = sslConfig.createSSLContext();
    } catch (Exception e) {
      logger.error("Excepcion en el metodo getSSLContextCertified", e);
    }
    return context;
  }
}
