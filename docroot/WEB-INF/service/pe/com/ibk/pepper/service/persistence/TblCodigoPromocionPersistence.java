/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.TblCodigoPromocion;

/**
 * The persistence interface for the tbl codigo promocion service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see TblCodigoPromocionPersistenceImpl
 * @see TblCodigoPromocionUtil
 * @generated
 */
public interface TblCodigoPromocionPersistence extends BasePersistence<TblCodigoPromocion> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TblCodigoPromocionUtil} to access the tbl codigo promocion persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the tbl codigo promocions where estado = &#63;.
	*
	* @param estado the estado
	* @return the matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findByCP_E(
		java.lang.String estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tbl codigo promocions where estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param estado the estado
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @return the range of matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findByCP_E(
		java.lang.String estado, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tbl codigo promocions where estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param estado the estado
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findByCP_E(
		java.lang.String estado, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tbl codigo promocion in the ordered set where estado = &#63;.
	*
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion findByCP_E_First(
		java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Returns the first tbl codigo promocion in the ordered set where estado = &#63;.
	*
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion fetchByCP_E_First(
		java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tbl codigo promocion in the ordered set where estado = &#63;.
	*
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion findByCP_E_Last(
		java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Returns the last tbl codigo promocion in the ordered set where estado = &#63;.
	*
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion fetchByCP_E_Last(
		java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tbl codigo promocions before and after the current tbl codigo promocion in the ordered set where estado = &#63;.
	*
	* @param idCodigo the primary key of the current tbl codigo promocion
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion[] findByCP_E_PrevAndNext(
		int idCodigo, java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Removes all the tbl codigo promocions where estado = &#63; from the database.
	*
	* @param estado the estado
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCP_E(java.lang.String estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tbl codigo promocions where estado = &#63;.
	*
	* @param estado the estado
	* @return the number of matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public int countByCP_E(java.lang.String estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tbl codigo promocions where comercio = &#63; and estado = &#63;.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @return the matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findByCP_C_E(
		java.lang.String comercio, java.lang.String estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tbl codigo promocions where comercio = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param comercio the comercio
	* @param estado the estado
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @return the range of matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findByCP_C_E(
		java.lang.String comercio, java.lang.String estado, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tbl codigo promocions where comercio = &#63; and estado = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param comercio the comercio
	* @param estado the estado
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findByCP_C_E(
		java.lang.String comercio, java.lang.String estado, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion findByCP_C_E_First(
		java.lang.String comercio, java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Returns the first tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion fetchByCP_C_E_First(
		java.lang.String comercio, java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion findByCP_C_E_Last(
		java.lang.String comercio, java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Returns the last tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion fetchByCP_C_E_Last(
		java.lang.String comercio, java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tbl codigo promocions before and after the current tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	*
	* @param idCodigo the primary key of the current tbl codigo promocion
	* @param comercio the comercio
	* @param estado the estado
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion[] findByCP_C_E_PrevAndNext(
		int idCodigo, java.lang.String comercio, java.lang.String estado,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Removes all the tbl codigo promocions where comercio = &#63; and estado = &#63; from the database.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCP_C_E(java.lang.String comercio,
		java.lang.String estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tbl codigo promocions where comercio = &#63; and estado = &#63;.
	*
	* @param comercio the comercio
	* @param estado the estado
	* @return the number of matching tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public int countByCP_C_E(java.lang.String comercio, java.lang.String estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the tbl codigo promocion in the entity cache if it is enabled.
	*
	* @param tblCodigoPromocion the tbl codigo promocion
	*/
	public void cacheResult(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion);

	/**
	* Caches the tbl codigo promocions in the entity cache if it is enabled.
	*
	* @param tblCodigoPromocions the tbl codigo promocions
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> tblCodigoPromocions);

	/**
	* Creates a new tbl codigo promocion with the primary key. Does not add the tbl codigo promocion to the database.
	*
	* @param idCodigo the primary key for the new tbl codigo promocion
	* @return the new tbl codigo promocion
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion create(int idCodigo);

	/**
	* Removes the tbl codigo promocion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCodigo the primary key of the tbl codigo promocion
	* @return the tbl codigo promocion that was removed
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion remove(int idCodigo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	public pe.com.ibk.pepper.model.TblCodigoPromocion updateImpl(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tbl codigo promocion with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchTblCodigoPromocionException} if it could not be found.
	*
	* @param idCodigo the primary key of the tbl codigo promocion
	* @return the tbl codigo promocion
	* @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion findByPrimaryKey(
		int idCodigo)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;

	/**
	* Returns the tbl codigo promocion with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idCodigo the primary key of the tbl codigo promocion
	* @return the tbl codigo promocion, or <code>null</code> if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.TblCodigoPromocion fetchByPrimaryKey(
		int idCodigo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tbl codigo promocions.
	*
	* @return the tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tbl codigo promocions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @return the range of tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tbl codigo promocions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the tbl codigo promocions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tbl codigo promocions.
	*
	* @return the number of tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}