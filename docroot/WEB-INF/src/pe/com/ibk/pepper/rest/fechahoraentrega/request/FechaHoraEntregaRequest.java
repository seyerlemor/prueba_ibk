package pe.com.ibk.pepper.rest.fechahoraentrega.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.generic.request.HttpHeadersV2;
import pe.com.ibk.pepper.rest.generic.request.QueryParamsV2;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FechaHoraEntregaRequest {
	private HttpHeadersV2 headers;
	private QueryParamsV2 queryParams;
	
	public FechaHoraEntregaRequest() {
		super();
	}
	@JsonCreator
	public FechaHoraEntregaRequest(HttpHeadersV2 headers, QueryParamsV2 queryParams) {
		super();
		this.headers = headers;
		this.queryParams = queryParams;
	}
	@JsonProperty("HttpHeader")
	public HttpHeadersV2 getHeaders() {
		return headers;
	}
	public void setHeaders(HttpHeadersV2 headers) {
		this.headers = headers;
	}
	@JsonProperty("QueryParams")
	public QueryParamsV2 getQueryParams() {
		return queryParams;
	}
	public void setQueryParams(QueryParamsV2 queryParams) {
		this.queryParams = queryParams;
	}
	
}
