
package pe.com.ibk.pepper.rest.fechahoraentrega.response;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Courier {

    private String id;
    private String name;
    private Boolean isAvailable;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Courier() {
    }

    /**
     * 
     * @param id
     * @param name
     * @param isAvailable
     */
    public Courier(String id, String name, Boolean isAvailable) {
        super();
        this.id = id;
        this.name = name;
        this.isAvailable = isAvailable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("isAvailable", isAvailable).toString();
    }

}
