/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchDireccionClienteException;
import pe.com.ibk.pepper.model.DireccionCliente;
import pe.com.ibk.pepper.model.impl.DireccionClienteImpl;
import pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the direccion cliente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see DireccionClientePersistence
 * @see DireccionClienteUtil
 * @generated
 */
public class DireccionClientePersistenceImpl extends BasePersistenceImpl<DireccionCliente>
	implements DireccionClientePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DireccionClienteUtil} to access the direccion cliente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DireccionClienteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED,
			DireccionClienteImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED,
			DireccionClienteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_DC_ID = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED,
			DireccionClienteImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByDC_ID", new String[] { Long.class.getName() },
			DireccionClienteModelImpl.IDDIRECCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DC_ID = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDC_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the direccion cliente where idDireccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	 *
	 * @param idDireccion the id direccion
	 * @return the matching direccion cliente
	 * @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a matching direccion cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente findByDC_ID(long idDireccion)
		throws NoSuchDireccionClienteException, SystemException {
		DireccionCliente direccionCliente = fetchByDC_ID(idDireccion);

		if (direccionCliente == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idDireccion=");
			msg.append(idDireccion);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDireccionClienteException(msg.toString());
		}

		return direccionCliente;
	}

	/**
	 * Returns the direccion cliente where idDireccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idDireccion the id direccion
	 * @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente fetchByDC_ID(long idDireccion)
		throws SystemException {
		return fetchByDC_ID(idDireccion, true);
	}

	/**
	 * Returns the direccion cliente where idDireccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idDireccion the id direccion
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente fetchByDC_ID(long idDireccion,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idDireccion };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DC_ID,
					finderArgs, this);
		}

		if (result instanceof DireccionCliente) {
			DireccionCliente direccionCliente = (DireccionCliente)result;

			if ((idDireccion != direccionCliente.getIdDireccion())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_DIRECCIONCLIENTE_WHERE);

			query.append(_FINDER_COLUMN_DC_ID_IDDIRECCION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idDireccion);

				List<DireccionCliente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"DireccionClientePersistenceImpl.fetchByDC_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					DireccionCliente direccionCliente = list.get(0);

					result = direccionCliente;

					cacheResult(direccionCliente);

					if ((direccionCliente.getIdDireccion() != idDireccion)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID,
							finderArgs, direccionCliente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DireccionCliente)result;
		}
	}

	/**
	 * Removes the direccion cliente where idDireccion = &#63; from the database.
	 *
	 * @param idDireccion the id direccion
	 * @return the direccion cliente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente removeByDC_ID(long idDireccion)
		throws NoSuchDireccionClienteException, SystemException {
		DireccionCliente direccionCliente = findByDC_ID(idDireccion);

		return remove(direccionCliente);
	}

	/**
	 * Returns the number of direccion clientes where idDireccion = &#63;.
	 *
	 * @param idDireccion the id direccion
	 * @return the number of matching direccion clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDC_ID(long idDireccion) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DC_ID;

		Object[] finderArgs = new Object[] { idDireccion };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DIRECCIONCLIENTE_WHERE);

			query.append(_FINDER_COLUMN_DC_ID_IDDIRECCION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idDireccion);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DC_ID_IDDIRECCION_2 = "direccionCliente.idDireccion = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_DC_CU = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED,
			DireccionClienteImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByDC_CU", new String[] { String.class.getName() },
			DireccionClienteModelImpl.CODIGOUSO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DC_CU = new FinderPath(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDC_CU",
			new String[] { String.class.getName() });

	/**
	 * Returns the direccion cliente where codigoUso = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	 *
	 * @param codigoUso the codigo uso
	 * @return the matching direccion cliente
	 * @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a matching direccion cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente findByDC_CU(String codigoUso)
		throws NoSuchDireccionClienteException, SystemException {
		DireccionCliente direccionCliente = fetchByDC_CU(codigoUso);

		if (direccionCliente == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("codigoUso=");
			msg.append(codigoUso);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDireccionClienteException(msg.toString());
		}

		return direccionCliente;
	}

	/**
	 * Returns the direccion cliente where codigoUso = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param codigoUso the codigo uso
	 * @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente fetchByDC_CU(String codigoUso)
		throws SystemException {
		return fetchByDC_CU(codigoUso, true);
	}

	/**
	 * Returns the direccion cliente where codigoUso = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param codigoUso the codigo uso
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching direccion cliente, or <code>null</code> if a matching direccion cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente fetchByDC_CU(String codigoUso,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { codigoUso };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DC_CU,
					finderArgs, this);
		}

		if (result instanceof DireccionCliente) {
			DireccionCliente direccionCliente = (DireccionCliente)result;

			if (!Validator.equals(codigoUso, direccionCliente.getCodigoUso())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_DIRECCIONCLIENTE_WHERE);

			boolean bindCodigoUso = false;

			if (codigoUso == null) {
				query.append(_FINDER_COLUMN_DC_CU_CODIGOUSO_1);
			}
			else if (codigoUso.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DC_CU_CODIGOUSO_3);
			}
			else {
				bindCodigoUso = true;

				query.append(_FINDER_COLUMN_DC_CU_CODIGOUSO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoUso) {
					qPos.add(codigoUso);
				}

				List<DireccionCliente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_CU,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"DireccionClientePersistenceImpl.fetchByDC_CU(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					DireccionCliente direccionCliente = list.get(0);

					result = direccionCliente;

					cacheResult(direccionCliente);

					if ((direccionCliente.getCodigoUso() == null) ||
							!direccionCliente.getCodigoUso().equals(codigoUso)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_CU,
							finderArgs, direccionCliente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_CU,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DireccionCliente)result;
		}
	}

	/**
	 * Removes the direccion cliente where codigoUso = &#63; from the database.
	 *
	 * @param codigoUso the codigo uso
	 * @return the direccion cliente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente removeByDC_CU(String codigoUso)
		throws NoSuchDireccionClienteException, SystemException {
		DireccionCliente direccionCliente = findByDC_CU(codigoUso);

		return remove(direccionCliente);
	}

	/**
	 * Returns the number of direccion clientes where codigoUso = &#63;.
	 *
	 * @param codigoUso the codigo uso
	 * @return the number of matching direccion clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDC_CU(String codigoUso) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DC_CU;

		Object[] finderArgs = new Object[] { codigoUso };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DIRECCIONCLIENTE_WHERE);

			boolean bindCodigoUso = false;

			if (codigoUso == null) {
				query.append(_FINDER_COLUMN_DC_CU_CODIGOUSO_1);
			}
			else if (codigoUso.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DC_CU_CODIGOUSO_3);
			}
			else {
				bindCodigoUso = true;

				query.append(_FINDER_COLUMN_DC_CU_CODIGOUSO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCodigoUso) {
					qPos.add(codigoUso);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DC_CU_CODIGOUSO_1 = "direccionCliente.codigoUso IS NULL";
	private static final String _FINDER_COLUMN_DC_CU_CODIGOUSO_2 = "direccionCliente.codigoUso = ?";
	private static final String _FINDER_COLUMN_DC_CU_CODIGOUSO_3 = "(direccionCliente.codigoUso IS NULL OR direccionCliente.codigoUso = '')";

	public DireccionClientePersistenceImpl() {
		setModelClass(DireccionCliente.class);
	}

	/**
	 * Caches the direccion cliente in the entity cache if it is enabled.
	 *
	 * @param direccionCliente the direccion cliente
	 */
	@Override
	public void cacheResult(DireccionCliente direccionCliente) {
		EntityCacheUtil.putResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteImpl.class, direccionCliente.getPrimaryKey(),
			direccionCliente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID,
			new Object[] { direccionCliente.getIdDireccion() }, direccionCliente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_CU,
			new Object[] { direccionCliente.getCodigoUso() }, direccionCliente);

		direccionCliente.resetOriginalValues();
	}

	/**
	 * Caches the direccion clientes in the entity cache if it is enabled.
	 *
	 * @param direccionClientes the direccion clientes
	 */
	@Override
	public void cacheResult(List<DireccionCliente> direccionClientes) {
		for (DireccionCliente direccionCliente : direccionClientes) {
			if (EntityCacheUtil.getResult(
						DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
						DireccionClienteImpl.class,
						direccionCliente.getPrimaryKey()) == null) {
				cacheResult(direccionCliente);
			}
			else {
				direccionCliente.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all direccion clientes.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DireccionClienteImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DireccionClienteImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the direccion cliente.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DireccionCliente direccionCliente) {
		EntityCacheUtil.removeResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteImpl.class, direccionCliente.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(direccionCliente);
	}

	@Override
	public void clearCache(List<DireccionCliente> direccionClientes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DireccionCliente direccionCliente : direccionClientes) {
			EntityCacheUtil.removeResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
				DireccionClienteImpl.class, direccionCliente.getPrimaryKey());

			clearUniqueFindersCache(direccionCliente);
		}
	}

	protected void cacheUniqueFindersCache(DireccionCliente direccionCliente) {
		if (direccionCliente.isNew()) {
			Object[] args = new Object[] { direccionCliente.getIdDireccion() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DC_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID, args,
				direccionCliente);

			args = new Object[] { direccionCliente.getCodigoUso() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DC_CU, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_CU, args,
				direccionCliente);
		}
		else {
			DireccionClienteModelImpl direccionClienteModelImpl = (DireccionClienteModelImpl)direccionCliente;

			if ((direccionClienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DC_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { direccionCliente.getIdDireccion() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DC_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID, args,
					direccionCliente);
			}

			if ((direccionClienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DC_CU.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { direccionCliente.getCodigoUso() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DC_CU, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_CU, args,
					direccionCliente);
			}
		}
	}

	protected void clearUniqueFindersCache(DireccionCliente direccionCliente) {
		DireccionClienteModelImpl direccionClienteModelImpl = (DireccionClienteModelImpl)direccionCliente;

		Object[] args = new Object[] { direccionCliente.getIdDireccion() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DC_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_ID, args);

		if ((direccionClienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_DC_ID.getColumnBitmask()) != 0) {
			args = new Object[] {
					direccionClienteModelImpl.getOriginalIdDireccion()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DC_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_ID, args);
		}

		args = new Object[] { direccionCliente.getCodigoUso() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DC_CU, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_CU, args);

		if ((direccionClienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_DC_CU.getColumnBitmask()) != 0) {
			args = new Object[] { direccionClienteModelImpl.getOriginalCodigoUso() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DC_CU, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_CU, args);
		}
	}

	/**
	 * Creates a new direccion cliente with the primary key. Does not add the direccion cliente to the database.
	 *
	 * @param idDireccion the primary key for the new direccion cliente
	 * @return the new direccion cliente
	 */
	@Override
	public DireccionCliente create(long idDireccion) {
		DireccionCliente direccionCliente = new DireccionClienteImpl();

		direccionCliente.setNew(true);
		direccionCliente.setPrimaryKey(idDireccion);

		return direccionCliente;
	}

	/**
	 * Removes the direccion cliente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idDireccion the primary key of the direccion cliente
	 * @return the direccion cliente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente remove(long idDireccion)
		throws NoSuchDireccionClienteException, SystemException {
		return remove((Serializable)idDireccion);
	}

	/**
	 * Removes the direccion cliente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the direccion cliente
	 * @return the direccion cliente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente remove(Serializable primaryKey)
		throws NoSuchDireccionClienteException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DireccionCliente direccionCliente = (DireccionCliente)session.get(DireccionClienteImpl.class,
					primaryKey);

			if (direccionCliente == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDireccionClienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(direccionCliente);
		}
		catch (NoSuchDireccionClienteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DireccionCliente removeImpl(DireccionCliente direccionCliente)
		throws SystemException {
		direccionCliente = toUnwrappedModel(direccionCliente);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(direccionCliente)) {
				direccionCliente = (DireccionCliente)session.get(DireccionClienteImpl.class,
						direccionCliente.getPrimaryKeyObj());
			}

			if (direccionCliente != null) {
				session.delete(direccionCliente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (direccionCliente != null) {
			clearCache(direccionCliente);
		}

		return direccionCliente;
	}

	@Override
	public DireccionCliente updateImpl(
		pe.com.ibk.pepper.model.DireccionCliente direccionCliente)
		throws SystemException {
		direccionCliente = toUnwrappedModel(direccionCliente);

		boolean isNew = direccionCliente.isNew();

		Session session = null;

		try {
			session = openSession();

			if (direccionCliente.isNew()) {
				session.save(direccionCliente);

				direccionCliente.setNew(false);
			}
			else {
				session.merge(direccionCliente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DireccionClienteModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
			DireccionClienteImpl.class, direccionCliente.getPrimaryKey(),
			direccionCliente);

		clearUniqueFindersCache(direccionCliente);
		cacheUniqueFindersCache(direccionCliente);

		return direccionCliente;
	}

	protected DireccionCliente toUnwrappedModel(
		DireccionCliente direccionCliente) {
		if (direccionCliente instanceof DireccionClienteImpl) {
			return direccionCliente;
		}

		DireccionClienteImpl direccionClienteImpl = new DireccionClienteImpl();

		direccionClienteImpl.setNew(direccionCliente.isNew());
		direccionClienteImpl.setPrimaryKey(direccionCliente.getPrimaryKey());

		direccionClienteImpl.setIdDireccion(direccionCliente.getIdDireccion());
		direccionClienteImpl.setIdDatoCliente(direccionCliente.getIdDatoCliente());
		direccionClienteImpl.setTipoDireccion(direccionCliente.getTipoDireccion());
		direccionClienteImpl.setCodigoUso(direccionCliente.getCodigoUso());
		direccionClienteImpl.setFlagDireccionEstandar(direccionCliente.getFlagDireccionEstandar());
		direccionClienteImpl.setVia(direccionCliente.getVia());
		direccionClienteImpl.setDepartamento(direccionCliente.getDepartamento());
		direccionClienteImpl.setProvincia(direccionCliente.getProvincia());
		direccionClienteImpl.setDistrito(direccionCliente.getDistrito());
		direccionClienteImpl.setPais(direccionCliente.getPais());
		direccionClienteImpl.setUbigeo(direccionCliente.getUbigeo());
		direccionClienteImpl.setCodigoPostal(direccionCliente.getCodigoPostal());
		direccionClienteImpl.setEstado(direccionCliente.getEstado());
		direccionClienteImpl.setFechaRegistro(direccionCliente.getFechaRegistro());

		return direccionClienteImpl;
	}

	/**
	 * Returns the direccion cliente with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the direccion cliente
	 * @return the direccion cliente
	 * @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDireccionClienteException, SystemException {
		DireccionCliente direccionCliente = fetchByPrimaryKey(primaryKey);

		if (direccionCliente == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDireccionClienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return direccionCliente;
	}

	/**
	 * Returns the direccion cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchDireccionClienteException} if it could not be found.
	 *
	 * @param idDireccion the primary key of the direccion cliente
	 * @return the direccion cliente
	 * @throws pe.com.ibk.pepper.NoSuchDireccionClienteException if a direccion cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente findByPrimaryKey(long idDireccion)
		throws NoSuchDireccionClienteException, SystemException {
		return findByPrimaryKey((Serializable)idDireccion);
	}

	/**
	 * Returns the direccion cliente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the direccion cliente
	 * @return the direccion cliente, or <code>null</code> if a direccion cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		DireccionCliente direccionCliente = (DireccionCliente)EntityCacheUtil.getResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
				DireccionClienteImpl.class, primaryKey);

		if (direccionCliente == _nullDireccionCliente) {
			return null;
		}

		if (direccionCliente == null) {
			Session session = null;

			try {
				session = openSession();

				direccionCliente = (DireccionCliente)session.get(DireccionClienteImpl.class,
						primaryKey);

				if (direccionCliente != null) {
					cacheResult(direccionCliente);
				}
				else {
					EntityCacheUtil.putResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
						DireccionClienteImpl.class, primaryKey,
						_nullDireccionCliente);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DireccionClienteModelImpl.ENTITY_CACHE_ENABLED,
					DireccionClienteImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return direccionCliente;
	}

	/**
	 * Returns the direccion cliente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idDireccion the primary key of the direccion cliente
	 * @return the direccion cliente, or <code>null</code> if a direccion cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DireccionCliente fetchByPrimaryKey(long idDireccion)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idDireccion);
	}

	/**
	 * Returns all the direccion clientes.
	 *
	 * @return the direccion clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DireccionCliente> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the direccion clientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of direccion clientes
	 * @param end the upper bound of the range of direccion clientes (not inclusive)
	 * @return the range of direccion clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DireccionCliente> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the direccion clientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DireccionClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of direccion clientes
	 * @param end the upper bound of the range of direccion clientes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of direccion clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DireccionCliente> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DireccionCliente> list = (List<DireccionCliente>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DIRECCIONCLIENTE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DIRECCIONCLIENTE;

				if (pagination) {
					sql = sql.concat(DireccionClienteModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DireccionCliente>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DireccionCliente>(list);
				}
				else {
					list = (List<DireccionCliente>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the direccion clientes from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (DireccionCliente direccionCliente : findAll()) {
			remove(direccionCliente);
		}
	}

	/**
	 * Returns the number of direccion clientes.
	 *
	 * @return the number of direccion clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DIRECCIONCLIENTE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the direccion cliente persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.DireccionCliente")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DireccionCliente>> listenersList = new ArrayList<ModelListener<DireccionCliente>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DireccionCliente>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DireccionClienteImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DIRECCIONCLIENTE = "SELECT direccionCliente FROM DireccionCliente direccionCliente";
	private static final String _SQL_SELECT_DIRECCIONCLIENTE_WHERE = "SELECT direccionCliente FROM DireccionCliente direccionCliente WHERE ";
	private static final String _SQL_COUNT_DIRECCIONCLIENTE = "SELECT COUNT(direccionCliente) FROM DireccionCliente direccionCliente";
	private static final String _SQL_COUNT_DIRECCIONCLIENTE_WHERE = "SELECT COUNT(direccionCliente) FROM DireccionCliente direccionCliente WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "direccionCliente.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DireccionCliente exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DireccionCliente exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DireccionClientePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idDireccion", "idDatoCliente", "tipoDireccion", "codigoUso",
				"flagDireccionEstandar", "via", "departamento", "provincia",
				"distrito", "pais", "ubigeo", "codigoPostal", "estado",
				"fechaRegistro"
			});
	private static DireccionCliente _nullDireccionCliente = new DireccionClienteImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DireccionCliente> toCacheModel() {
				return _nullDireccionClienteCacheModel;
			}
		};

	private static CacheModel<DireccionCliente> _nullDireccionClienteCacheModel = new CacheModel<DireccionCliente>() {
			@Override
			public DireccionCliente toEntityModel() {
				return _nullDireccionCliente;
			}
		};
}