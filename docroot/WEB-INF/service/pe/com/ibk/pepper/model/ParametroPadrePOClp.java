/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.ParametroPadrePOLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ParametroPadrePOClp extends BaseModelImpl<ParametroPadrePO>
	implements ParametroPadrePO {
	public ParametroPadrePOClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroPadrePO.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroPadrePO.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idParametroPadre;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdParametroPadre(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idParametroPadre;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroPadre", getIdParametroPadre());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("json", getJson());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroPadre = (Long)attributes.get("idParametroPadre");

		if (idParametroPadre != null) {
			setIdParametroPadre(idParametroPadre);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String json = (String)attributes.get("json");

		if (json != null) {
			setJson(json);
		}
	}

	@Override
	public long getIdParametroPadre() {
		return _idParametroPadre;
	}

	@Override
	public void setIdParametroPadre(long idParametroPadre) {
		_idParametroPadre = idParametroPadre;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setIdParametroPadre",
						long.class);

				method.invoke(_parametroPadrePORemoteModel, idParametroPadre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_parametroPadrePORemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_parametroPadrePORemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_parametroPadrePORemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public String getUserName() {
		return _userName;
	}

	@Override
	public void setUserName(String userName) {
		_userName = userName;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setUserName", String.class);

				method.invoke(_parametroPadrePORemoteModel, userName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setCreateDate", Date.class);

				method.invoke(_parametroPadrePORemoteModel, createDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setModifiedDate", Date.class);

				method.invoke(_parametroPadrePORemoteModel, modifiedDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoPadre() {
		return _codigoPadre;
	}

	@Override
	public void setCodigoPadre(String codigoPadre) {
		_codigoPadre = codigoPadre;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoPadre", String.class);

				method.invoke(_parametroPadrePORemoteModel, codigoPadre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombre() {
		return _nombre;
	}

	@Override
	public void setNombre(String nombre) {
		_nombre = nombre;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setNombre", String.class);

				method.invoke(_parametroPadrePORemoteModel, nombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescripcion() {
		return _descripcion;
	}

	@Override
	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setDescripcion", String.class);

				method.invoke(_parametroPadrePORemoteModel, descripcion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getEstado() {
		return _estado;
	}

	@Override
	public boolean isEstado() {
		return _estado;
	}

	@Override
	public void setEstado(boolean estado) {
		_estado = estado;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", boolean.class);

				method.invoke(_parametroPadrePORemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getJson() {
		return _json;
	}

	@Override
	public void setJson(String json) {
		_json = json;

		if (_parametroPadrePORemoteModel != null) {
			try {
				Class<?> clazz = _parametroPadrePORemoteModel.getClass();

				Method method = clazz.getMethod("setJson", String.class);

				method.invoke(_parametroPadrePORemoteModel, json);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getParametroPadrePORemoteModel() {
		return _parametroPadrePORemoteModel;
	}

	public void setParametroPadrePORemoteModel(
		BaseModel<?> parametroPadrePORemoteModel) {
		_parametroPadrePORemoteModel = parametroPadrePORemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _parametroPadrePORemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_parametroPadrePORemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ParametroPadrePOLocalServiceUtil.addParametroPadrePO(this);
		}
		else {
			ParametroPadrePOLocalServiceUtil.updateParametroPadrePO(this);
		}
	}

	@Override
	public ParametroPadrePO toEscapedModel() {
		return (ParametroPadrePO)ProxyUtil.newProxyInstance(ParametroPadrePO.class.getClassLoader(),
			new Class[] { ParametroPadrePO.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ParametroPadrePOClp clone = new ParametroPadrePOClp();

		clone.setIdParametroPadre(getIdParametroPadre());
		clone.setGroupId(getGroupId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setCodigoPadre(getCodigoPadre());
		clone.setNombre(getNombre());
		clone.setDescripcion(getDescripcion());
		clone.setEstado(getEstado());
		clone.setJson(getJson());

		return clone;
	}

	@Override
	public int compareTo(ParametroPadrePO parametroPadrePO) {
		long primaryKey = parametroPadrePO.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroPadrePOClp)) {
			return false;
		}

		ParametroPadrePOClp parametroPadrePO = (ParametroPadrePOClp)obj;

		long primaryKey = parametroPadrePO.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{idParametroPadre=");
		sb.append(getIdParametroPadre());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", codigoPadre=");
		sb.append(getCodigoPadre());
		sb.append(", nombre=");
		sb.append(getNombre());
		sb.append(", descripcion=");
		sb.append(getDescripcion());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", json=");
		sb.append(getJson());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.ParametroPadrePO");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idParametroPadre</column-name><column-value><![CDATA[");
		sb.append(getIdParametroPadre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoPadre</column-name><column-value><![CDATA[");
		sb.append(getCodigoPadre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombre</column-name><column-value><![CDATA[");
		sb.append(getNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripcion</column-name><column-value><![CDATA[");
		sb.append(getDescripcion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>json</column-name><column-value><![CDATA[");
		sb.append(getJson());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idParametroPadre;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _codigoPadre;
	private String _nombre;
	private String _descripcion;
	private boolean _estado;
	private String _json;
	private BaseModel<?> _parametroPadrePORemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}