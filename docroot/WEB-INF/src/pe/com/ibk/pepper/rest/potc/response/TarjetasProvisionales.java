
package pe.com.ibk.pepper.rest.potc.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TarjetasProvisionales implements Serializable{

	private static final long serialVersionUID = -8229595407931225370L;
	
	@SerializedName(value="provisional")
	private Provisional provisional;

	public Provisional getProvisional() {
		return provisional;
	}


	public void setProvisional(Provisional provisional) {
		this.provisional = provisional;
	}
    

}
