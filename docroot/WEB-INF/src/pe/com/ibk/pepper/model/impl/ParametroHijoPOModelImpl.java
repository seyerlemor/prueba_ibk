/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.model.ParametroHijoPOModel;
import pe.com.ibk.pepper.model.ParametroHijoPOSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the ParametroHijoPO service. Represents a row in the &quot;T_PARAMETRO_DETALLE&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link pe.com.ibk.pepper.model.ParametroHijoPOModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ParametroHijoPOImpl}.
 * </p>
 *
 * @author Interbank
 * @see ParametroHijoPOImpl
 * @see pe.com.ibk.pepper.model.ParametroHijoPO
 * @see pe.com.ibk.pepper.model.ParametroHijoPOModel
 * @generated
 */
@JSON(strict = true)
public class ParametroHijoPOModelImpl extends BaseModelImpl<ParametroHijoPO>
	implements ParametroHijoPOModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a parametro hijo p o model instance should use the {@link pe.com.ibk.pepper.model.ParametroHijoPO} interface instead.
	 */
	public static final String TABLE_NAME = "T_PARAMETRO_DETALLE";
	public static final Object[][] TABLE_COLUMNS = {
			{ "PADE_IDPARAMETRO", Types.BIGINT },
			{ "PADE_GROUP_ID", Types.BIGINT },
			{ "PADE_COMPANY_ID", Types.BIGINT },
			{ "PADE_USER_ID", Types.BIGINT },
			{ "PADE_USER_NAME", Types.VARCHAR },
			{ "PADE_CREATE_DATE", Types.TIMESTAMP },
			{ "PADE_MODIFIED_DATE", Types.TIMESTAMP },
			{ "PADE_CODIGO_PADRE", Types.VARCHAR },
			{ "PADE_CODIGO", Types.VARCHAR },
			{ "PADE_NOMBRE", Types.VARCHAR },
			{ "PADE_DESCRIPCION", Types.VARCHAR },
			{ "PADE_ESTADO", Types.BOOLEAN },
			{ "PADE_ORDEN", Types.INTEGER },
			{ "PADE_DATO01", Types.VARCHAR },
			{ "PADE_DATO02", Types.VARCHAR },
			{ "PADE_DATO03", Types.VARCHAR },
			{ "PADE_DATO04", Types.VARCHAR },
			{ "PADE_DATO05", Types.VARCHAR },
			{ "PADE_DATO06", Types.VARCHAR },
			{ "PADE_DATO07", Types.VARCHAR },
			{ "PADE_DATO08", Types.VARCHAR }
		};
	public static final String TABLE_SQL_CREATE = "create table T_PARAMETRO_DETALLE (PADE_IDPARAMETRO LONG not null primary key IDENTITY,PADE_GROUP_ID LONG,PADE_COMPANY_ID LONG,PADE_USER_ID LONG,PADE_USER_NAME VARCHAR(75) null,PADE_CREATE_DATE DATE null,PADE_MODIFIED_DATE DATE null,PADE_CODIGO_PADRE VARCHAR(75) null,PADE_CODIGO VARCHAR(75) null,PADE_NOMBRE VARCHAR(75) null,PADE_DESCRIPCION VARCHAR(75) null,PADE_ESTADO BOOLEAN,PADE_ORDEN INTEGER,PADE_DATO01 VARCHAR(75) null,PADE_DATO02 VARCHAR(75) null,PADE_DATO03 VARCHAR(75) null,PADE_DATO04 VARCHAR(75) null,PADE_DATO05 VARCHAR(75) null,PADE_DATO06 VARCHAR(75) null,PADE_DATO07 VARCHAR(75) null,PADE_DATO08 VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table T_PARAMETRO_DETALLE";
	public static final String ORDER_BY_JPQL = " ORDER BY parametroHijoPO.orden ASC";
	public static final String ORDER_BY_SQL = " ORDER BY T_PARAMETRO_DETALLE.PADE_ORDEN ASC";
	public static final String DATA_SOURCE = "pepperDataSource";
	public static final String SESSION_FACTORY = "pepperSessionFactory";
	public static final String TX_MANAGER = "pepperTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.pe.com.ibk.pepper.model.ParametroHijoPO"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.pe.com.ibk.pepper.model.ParametroHijoPO"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.pe.com.ibk.pepper.model.ParametroHijoPO"),
			true);
	public static long CODIGO_COLUMN_BITMASK = 1L;
	public static long CODIGOPADRE_COLUMN_BITMASK = 2L;
	public static long DATO1_COLUMN_BITMASK = 4L;
	public static long DATO2_COLUMN_BITMASK = 8L;
	public static long DESCRIPCION_COLUMN_BITMASK = 16L;
	public static long ESTADO_COLUMN_BITMASK = 32L;
	public static long NOMBRE_COLUMN_BITMASK = 64L;
	public static long ORDEN_COLUMN_BITMASK = 128L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static ParametroHijoPO toModel(ParametroHijoPOSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		ParametroHijoPO model = new ParametroHijoPOImpl();

		model.setIdParametroHijo(soapModel.getIdParametroHijo());
		model.setGroupId(soapModel.getGroupId());
		model.setCompanyId(soapModel.getCompanyId());
		model.setUserId(soapModel.getUserId());
		model.setUserName(soapModel.getUserName());
		model.setCreateDate(soapModel.getCreateDate());
		model.setModifiedDate(soapModel.getModifiedDate());
		model.setCodigoPadre(soapModel.getCodigoPadre());
		model.setCodigo(soapModel.getCodigo());
		model.setNombre(soapModel.getNombre());
		model.setDescripcion(soapModel.getDescripcion());
		model.setEstado(soapModel.getEstado());
		model.setOrden(soapModel.getOrden());
		model.setDato1(soapModel.getDato1());
		model.setDato2(soapModel.getDato2());
		model.setDato3(soapModel.getDato3());
		model.setDato4(soapModel.getDato4());
		model.setDato5(soapModel.getDato5());
		model.setDato6(soapModel.getDato6());
		model.setDato7(soapModel.getDato7());
		model.setDato8(soapModel.getDato8());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<ParametroHijoPO> toModels(
		ParametroHijoPOSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<ParametroHijoPO> models = new ArrayList<ParametroHijoPO>(soapModels.length);

		for (ParametroHijoPOSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.pe.com.ibk.pepper.model.ParametroHijoPO"));

	public ParametroHijoPOModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _idParametroHijo;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdParametroHijo(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idParametroHijo;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroHijoPO.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroHijoPO.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroHijo", getIdParametroHijo());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("codigo", getCodigo());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("orden", getOrden());
		attributes.put("dato1", getDato1());
		attributes.put("dato2", getDato2());
		attributes.put("dato3", getDato3());
		attributes.put("dato4", getDato4());
		attributes.put("dato5", getDato5());
		attributes.put("dato6", getDato6());
		attributes.put("dato7", getDato7());
		attributes.put("dato8", getDato8());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroHijo = (Long)attributes.get("idParametroHijo");

		if (idParametroHijo != null) {
			setIdParametroHijo(idParametroHijo);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Integer orden = (Integer)attributes.get("orden");

		if (orden != null) {
			setOrden(orden);
		}

		String dato1 = (String)attributes.get("dato1");

		if (dato1 != null) {
			setDato1(dato1);
		}

		String dato2 = (String)attributes.get("dato2");

		if (dato2 != null) {
			setDato2(dato2);
		}

		String dato3 = (String)attributes.get("dato3");

		if (dato3 != null) {
			setDato3(dato3);
		}

		String dato4 = (String)attributes.get("dato4");

		if (dato4 != null) {
			setDato4(dato4);
		}

		String dato5 = (String)attributes.get("dato5");

		if (dato5 != null) {
			setDato5(dato5);
		}

		String dato6 = (String)attributes.get("dato6");

		if (dato6 != null) {
			setDato6(dato6);
		}

		String dato7 = (String)attributes.get("dato7");

		if (dato7 != null) {
			setDato7(dato7);
		}

		String dato8 = (String)attributes.get("dato8");

		if (dato8 != null) {
			setDato8(dato8);
		}
	}

	@JSON
	@Override
	public long getIdParametroHijo() {
		return _idParametroHijo;
	}

	@Override
	public void setIdParametroHijo(long idParametroHijo) {
		_idParametroHijo = idParametroHijo;
	}

	@JSON
	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	@JSON
	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	@JSON
	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@JSON
	@Override
	public String getUserName() {
		if (_userName == null) {
			return StringPool.BLANK;
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		_userName = userName;
	}

	@JSON
	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	@JSON
	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	@JSON
	@Override
	public String getCodigoPadre() {
		if (_codigoPadre == null) {
			return StringPool.BLANK;
		}
		else {
			return _codigoPadre;
		}
	}

	@Override
	public void setCodigoPadre(String codigoPadre) {
		_columnBitmask |= CODIGOPADRE_COLUMN_BITMASK;

		if (_originalCodigoPadre == null) {
			_originalCodigoPadre = _codigoPadre;
		}

		_codigoPadre = codigoPadre;
	}

	public String getOriginalCodigoPadre() {
		return GetterUtil.getString(_originalCodigoPadre);
	}

	@JSON
	@Override
	public String getCodigo() {
		if (_codigo == null) {
			return StringPool.BLANK;
		}
		else {
			return _codigo;
		}
	}

	@Override
	public void setCodigo(String codigo) {
		_columnBitmask |= CODIGO_COLUMN_BITMASK;

		if (_originalCodigo == null) {
			_originalCodigo = _codigo;
		}

		_codigo = codigo;
	}

	public String getOriginalCodigo() {
		return GetterUtil.getString(_originalCodigo);
	}

	@JSON
	@Override
	public String getNombre() {
		if (_nombre == null) {
			return StringPool.BLANK;
		}
		else {
			return _nombre;
		}
	}

	@Override
	public void setNombre(String nombre) {
		_columnBitmask |= NOMBRE_COLUMN_BITMASK;

		if (_originalNombre == null) {
			_originalNombre = _nombre;
		}

		_nombre = nombre;
	}

	public String getOriginalNombre() {
		return GetterUtil.getString(_originalNombre);
	}

	@JSON
	@Override
	public String getDescripcion() {
		if (_descripcion == null) {
			return StringPool.BLANK;
		}
		else {
			return _descripcion;
		}
	}

	@Override
	public void setDescripcion(String descripcion) {
		_columnBitmask |= DESCRIPCION_COLUMN_BITMASK;

		if (_originalDescripcion == null) {
			_originalDescripcion = _descripcion;
		}

		_descripcion = descripcion;
	}

	public String getOriginalDescripcion() {
		return GetterUtil.getString(_originalDescripcion);
	}

	@JSON
	@Override
	public boolean getEstado() {
		return _estado;
	}

	@Override
	public boolean isEstado() {
		return _estado;
	}

	@Override
	public void setEstado(boolean estado) {
		_columnBitmask |= ESTADO_COLUMN_BITMASK;

		if (!_setOriginalEstado) {
			_setOriginalEstado = true;

			_originalEstado = _estado;
		}

		_estado = estado;
	}

	public boolean getOriginalEstado() {
		return _originalEstado;
	}

	@JSON
	@Override
	public int getOrden() {
		return _orden;
	}

	@Override
	public void setOrden(int orden) {
		_columnBitmask = -1L;

		_orden = orden;
	}

	@JSON
	@Override
	public String getDato1() {
		if (_dato1 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato1;
		}
	}

	@Override
	public void setDato1(String dato1) {
		_columnBitmask |= DATO1_COLUMN_BITMASK;

		if (_originalDato1 == null) {
			_originalDato1 = _dato1;
		}

		_dato1 = dato1;
	}

	public String getOriginalDato1() {
		return GetterUtil.getString(_originalDato1);
	}

	@JSON
	@Override
	public String getDato2() {
		if (_dato2 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato2;
		}
	}

	@Override
	public void setDato2(String dato2) {
		_columnBitmask |= DATO2_COLUMN_BITMASK;

		if (_originalDato2 == null) {
			_originalDato2 = _dato2;
		}

		_dato2 = dato2;
	}

	public String getOriginalDato2() {
		return GetterUtil.getString(_originalDato2);
	}

	@JSON
	@Override
	public String getDato3() {
		if (_dato3 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato3;
		}
	}

	@Override
	public void setDato3(String dato3) {
		_dato3 = dato3;
	}

	@JSON
	@Override
	public String getDato4() {
		if (_dato4 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato4;
		}
	}

	@Override
	public void setDato4(String dato4) {
		_dato4 = dato4;
	}

	@JSON
	@Override
	public String getDato5() {
		if (_dato5 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato5;
		}
	}

	@Override
	public void setDato5(String dato5) {
		_dato5 = dato5;
	}

	@JSON
	@Override
	public String getDato6() {
		if (_dato6 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato6;
		}
	}

	@Override
	public void setDato6(String dato6) {
		_dato6 = dato6;
	}

	@JSON
	@Override
	public String getDato7() {
		if (_dato7 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato7;
		}
	}

	@Override
	public void setDato7(String dato7) {
		_dato7 = dato7;
	}

	@JSON
	@Override
	public String getDato8() {
		if (_dato8 == null) {
			return StringPool.BLANK;
		}
		else {
			return _dato8;
		}
	}

	@Override
	public void setDato8(String dato8) {
		_dato8 = dato8;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(getCompanyId(),
			ParametroHijoPO.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public ParametroHijoPO toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (ParametroHijoPO)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		ParametroHijoPOImpl parametroHijoPOImpl = new ParametroHijoPOImpl();

		parametroHijoPOImpl.setIdParametroHijo(getIdParametroHijo());
		parametroHijoPOImpl.setGroupId(getGroupId());
		parametroHijoPOImpl.setCompanyId(getCompanyId());
		parametroHijoPOImpl.setUserId(getUserId());
		parametroHijoPOImpl.setUserName(getUserName());
		parametroHijoPOImpl.setCreateDate(getCreateDate());
		parametroHijoPOImpl.setModifiedDate(getModifiedDate());
		parametroHijoPOImpl.setCodigoPadre(getCodigoPadre());
		parametroHijoPOImpl.setCodigo(getCodigo());
		parametroHijoPOImpl.setNombre(getNombre());
		parametroHijoPOImpl.setDescripcion(getDescripcion());
		parametroHijoPOImpl.setEstado(getEstado());
		parametroHijoPOImpl.setOrden(getOrden());
		parametroHijoPOImpl.setDato1(getDato1());
		parametroHijoPOImpl.setDato2(getDato2());
		parametroHijoPOImpl.setDato3(getDato3());
		parametroHijoPOImpl.setDato4(getDato4());
		parametroHijoPOImpl.setDato5(getDato5());
		parametroHijoPOImpl.setDato6(getDato6());
		parametroHijoPOImpl.setDato7(getDato7());
		parametroHijoPOImpl.setDato8(getDato8());

		parametroHijoPOImpl.resetOriginalValues();

		return parametroHijoPOImpl;
	}

	@Override
	public int compareTo(ParametroHijoPO parametroHijoPO) {
		int value = 0;

		if (getOrden() < parametroHijoPO.getOrden()) {
			value = -1;
		}
		else if (getOrden() > parametroHijoPO.getOrden()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroHijoPO)) {
			return false;
		}

		ParametroHijoPO parametroHijoPO = (ParametroHijoPO)obj;

		long primaryKey = parametroHijoPO.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		ParametroHijoPOModelImpl parametroHijoPOModelImpl = this;

		parametroHijoPOModelImpl._originalCodigoPadre = parametroHijoPOModelImpl._codigoPadre;

		parametroHijoPOModelImpl._originalCodigo = parametroHijoPOModelImpl._codigo;

		parametroHijoPOModelImpl._originalNombre = parametroHijoPOModelImpl._nombre;

		parametroHijoPOModelImpl._originalDescripcion = parametroHijoPOModelImpl._descripcion;

		parametroHijoPOModelImpl._originalEstado = parametroHijoPOModelImpl._estado;

		parametroHijoPOModelImpl._setOriginalEstado = false;

		parametroHijoPOModelImpl._originalDato1 = parametroHijoPOModelImpl._dato1;

		parametroHijoPOModelImpl._originalDato2 = parametroHijoPOModelImpl._dato2;

		parametroHijoPOModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<ParametroHijoPO> toCacheModel() {
		ParametroHijoPOCacheModel parametroHijoPOCacheModel = new ParametroHijoPOCacheModel();

		parametroHijoPOCacheModel.idParametroHijo = getIdParametroHijo();

		parametroHijoPOCacheModel.groupId = getGroupId();

		parametroHijoPOCacheModel.companyId = getCompanyId();

		parametroHijoPOCacheModel.userId = getUserId();

		parametroHijoPOCacheModel.userName = getUserName();

		String userName = parametroHijoPOCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			parametroHijoPOCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			parametroHijoPOCacheModel.createDate = createDate.getTime();
		}
		else {
			parametroHijoPOCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			parametroHijoPOCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			parametroHijoPOCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		parametroHijoPOCacheModel.codigoPadre = getCodigoPadre();

		String codigoPadre = parametroHijoPOCacheModel.codigoPadre;

		if ((codigoPadre != null) && (codigoPadre.length() == 0)) {
			parametroHijoPOCacheModel.codigoPadre = null;
		}

		parametroHijoPOCacheModel.codigo = getCodigo();

		String codigo = parametroHijoPOCacheModel.codigo;

		if ((codigo != null) && (codigo.length() == 0)) {
			parametroHijoPOCacheModel.codigo = null;
		}

		parametroHijoPOCacheModel.nombre = getNombre();

		String nombre = parametroHijoPOCacheModel.nombre;

		if ((nombre != null) && (nombre.length() == 0)) {
			parametroHijoPOCacheModel.nombre = null;
		}

		parametroHijoPOCacheModel.descripcion = getDescripcion();

		String descripcion = parametroHijoPOCacheModel.descripcion;

		if ((descripcion != null) && (descripcion.length() == 0)) {
			parametroHijoPOCacheModel.descripcion = null;
		}

		parametroHijoPOCacheModel.estado = getEstado();

		parametroHijoPOCacheModel.orden = getOrden();

		parametroHijoPOCacheModel.dato1 = getDato1();

		String dato1 = parametroHijoPOCacheModel.dato1;

		if ((dato1 != null) && (dato1.length() == 0)) {
			parametroHijoPOCacheModel.dato1 = null;
		}

		parametroHijoPOCacheModel.dato2 = getDato2();

		String dato2 = parametroHijoPOCacheModel.dato2;

		if ((dato2 != null) && (dato2.length() == 0)) {
			parametroHijoPOCacheModel.dato2 = null;
		}

		parametroHijoPOCacheModel.dato3 = getDato3();

		String dato3 = parametroHijoPOCacheModel.dato3;

		if ((dato3 != null) && (dato3.length() == 0)) {
			parametroHijoPOCacheModel.dato3 = null;
		}

		parametroHijoPOCacheModel.dato4 = getDato4();

		String dato4 = parametroHijoPOCacheModel.dato4;

		if ((dato4 != null) && (dato4.length() == 0)) {
			parametroHijoPOCacheModel.dato4 = null;
		}

		parametroHijoPOCacheModel.dato5 = getDato5();

		String dato5 = parametroHijoPOCacheModel.dato5;

		if ((dato5 != null) && (dato5.length() == 0)) {
			parametroHijoPOCacheModel.dato5 = null;
		}

		parametroHijoPOCacheModel.dato6 = getDato6();

		String dato6 = parametroHijoPOCacheModel.dato6;

		if ((dato6 != null) && (dato6.length() == 0)) {
			parametroHijoPOCacheModel.dato6 = null;
		}

		parametroHijoPOCacheModel.dato7 = getDato7();

		String dato7 = parametroHijoPOCacheModel.dato7;

		if ((dato7 != null) && (dato7.length() == 0)) {
			parametroHijoPOCacheModel.dato7 = null;
		}

		parametroHijoPOCacheModel.dato8 = getDato8();

		String dato8 = parametroHijoPOCacheModel.dato8;

		if ((dato8 != null) && (dato8.length() == 0)) {
			parametroHijoPOCacheModel.dato8 = null;
		}

		return parametroHijoPOCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(43);

		sb.append("{idParametroHijo=");
		sb.append(getIdParametroHijo());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", codigoPadre=");
		sb.append(getCodigoPadre());
		sb.append(", codigo=");
		sb.append(getCodigo());
		sb.append(", nombre=");
		sb.append(getNombre());
		sb.append(", descripcion=");
		sb.append(getDescripcion());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", orden=");
		sb.append(getOrden());
		sb.append(", dato1=");
		sb.append(getDato1());
		sb.append(", dato2=");
		sb.append(getDato2());
		sb.append(", dato3=");
		sb.append(getDato3());
		sb.append(", dato4=");
		sb.append(getDato4());
		sb.append(", dato5=");
		sb.append(getDato5());
		sb.append(", dato6=");
		sb.append(getDato6());
		sb.append(", dato7=");
		sb.append(getDato7());
		sb.append(", dato8=");
		sb.append(getDato8());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(67);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.ParametroHijoPO");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idParametroHijo</column-name><column-value><![CDATA[");
		sb.append(getIdParametroHijo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoPadre</column-name><column-value><![CDATA[");
		sb.append(getCodigoPadre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigo</column-name><column-value><![CDATA[");
		sb.append(getCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombre</column-name><column-value><![CDATA[");
		sb.append(getNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripcion</column-name><column-value><![CDATA[");
		sb.append(getDescripcion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>orden</column-name><column-value><![CDATA[");
		sb.append(getOrden());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato1</column-name><column-value><![CDATA[");
		sb.append(getDato1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato2</column-name><column-value><![CDATA[");
		sb.append(getDato2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato3</column-name><column-value><![CDATA[");
		sb.append(getDato3());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato4</column-name><column-value><![CDATA[");
		sb.append(getDato4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato5</column-name><column-value><![CDATA[");
		sb.append(getDato5());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato6</column-name><column-value><![CDATA[");
		sb.append(getDato6());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato7</column-name><column-value><![CDATA[");
		sb.append(getDato7());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dato8</column-name><column-value><![CDATA[");
		sb.append(getDato8());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = ParametroHijoPO.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			ParametroHijoPO.class
		};
	private long _idParametroHijo;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _codigoPadre;
	private String _originalCodigoPadre;
	private String _codigo;
	private String _originalCodigo;
	private String _nombre;
	private String _originalNombre;
	private String _descripcion;
	private String _originalDescripcion;
	private boolean _estado;
	private boolean _originalEstado;
	private boolean _setOriginalEstado;
	private int _orden;
	private String _dato1;
	private String _originalDato1;
	private String _dato2;
	private String _originalDato2;
	private String _dato3;
	private String _dato4;
	private String _dato5;
	private String _dato6;
	private String _dato7;
	private String _dato8;
	private long _columnBitmask;
	private ParametroHijoPO _escapedModel;
}