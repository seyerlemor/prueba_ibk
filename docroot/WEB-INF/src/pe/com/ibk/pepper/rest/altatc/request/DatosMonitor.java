
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DatosMonitor {
    private String dispositivo;
    private String ip;
    private String tipoAutenticacion;

    public DatosMonitor() {
    }
    
    public DatosMonitor(String dispositivo, String ip, String tipoAutenticacion) {
        super();
        this.dispositivo = dispositivo;
        this.ip = ip;
        this.tipoAutenticacion = tipoAutenticacion;
    }

    public String getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getTipoAutenticacion() {
		return tipoAutenticacion;
	}

	public void setTipoAutenticacion(String tipoAutenticacion) {
		this.tipoAutenticacion = tipoAutenticacion;
	}

}
