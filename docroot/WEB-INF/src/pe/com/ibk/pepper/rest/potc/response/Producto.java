
package pe.com.ibk.pepper.rest.potc.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Producto implements Serializable{

	private static final long serialVersionUID = -7425194781011725858L;

	@SerializedName(value="idProducto")
    private String idProducto;
    
	@SerializedName(value="idProductoUsuario")
    private String idProductoUsuario;

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getIdProductoUsuario() {
		return idProductoUsuario;
	}

	public void setIdProductoUsuario(String idProductoUsuario) {
		this.idProductoUsuario = idProductoUsuario;
	}

}
