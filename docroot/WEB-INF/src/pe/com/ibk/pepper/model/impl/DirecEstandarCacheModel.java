/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.DirecEstandar;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing DirecEstandar in entity cache.
 *
 * @author Interbank
 * @see DirecEstandar
 * @generated
 */
public class DirecEstandarCacheModel implements CacheModel<DirecEstandar>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{idDetalleDireccion=");
		sb.append(idDetalleDireccion);
		sb.append(", idDireccion=");
		sb.append(idDireccion);
		sb.append(", tipoVia=");
		sb.append(tipoVia);
		sb.append(", nombreVia=");
		sb.append(nombreVia);
		sb.append(", numero=");
		sb.append(numero);
		sb.append(", manzana=");
		sb.append(manzana);
		sb.append(", pisoLote=");
		sb.append(pisoLote);
		sb.append(", interior=");
		sb.append(interior);
		sb.append(", urbanizacion=");
		sb.append(urbanizacion);
		sb.append(", referencia=");
		sb.append(referencia);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DirecEstandar toEntityModel() {
		DirecEstandarImpl direcEstandarImpl = new DirecEstandarImpl();

		direcEstandarImpl.setIdDetalleDireccion(idDetalleDireccion);
		direcEstandarImpl.setIdDireccion(idDireccion);

		if (tipoVia == null) {
			direcEstandarImpl.setTipoVia(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setTipoVia(tipoVia);
		}

		if (nombreVia == null) {
			direcEstandarImpl.setNombreVia(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setNombreVia(nombreVia);
		}

		if (numero == null) {
			direcEstandarImpl.setNumero(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setNumero(numero);
		}

		if (manzana == null) {
			direcEstandarImpl.setManzana(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setManzana(manzana);
		}

		if (pisoLote == null) {
			direcEstandarImpl.setPisoLote(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setPisoLote(pisoLote);
		}

		if (interior == null) {
			direcEstandarImpl.setInterior(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setInterior(interior);
		}

		if (urbanizacion == null) {
			direcEstandarImpl.setUrbanizacion(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setUrbanizacion(urbanizacion);
		}

		if (referencia == null) {
			direcEstandarImpl.setReferencia(StringPool.BLANK);
		}
		else {
			direcEstandarImpl.setReferencia(referencia);
		}

		direcEstandarImpl.resetOriginalValues();

		return direcEstandarImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idDetalleDireccion = objectInput.readLong();
		idDireccion = objectInput.readLong();
		tipoVia = objectInput.readUTF();
		nombreVia = objectInput.readUTF();
		numero = objectInput.readUTF();
		manzana = objectInput.readUTF();
		pisoLote = objectInput.readUTF();
		interior = objectInput.readUTF();
		urbanizacion = objectInput.readUTF();
		referencia = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idDetalleDireccion);
		objectOutput.writeLong(idDireccion);

		if (tipoVia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoVia);
		}

		if (nombreVia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreVia);
		}

		if (numero == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numero);
		}

		if (manzana == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(manzana);
		}

		if (pisoLote == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pisoLote);
		}

		if (interior == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(interior);
		}

		if (urbanizacion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(urbanizacion);
		}

		if (referencia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(referencia);
		}
	}

	public long idDetalleDireccion;
	public long idDireccion;
	public String tipoVia;
	public String nombreVia;
	public String numero;
	public String manzana;
	public String pisoLote;
	public String interior;
	public String urbanizacion;
	public String referencia;
}