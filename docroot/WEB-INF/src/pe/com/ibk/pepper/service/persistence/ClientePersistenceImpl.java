/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchClienteException;
import pe.com.ibk.pepper.model.Cliente;
import pe.com.ibk.pepper.model.impl.ClienteImpl;
import pe.com.ibk.pepper.model.impl.ClienteModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the cliente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ClientePersistence
 * @see ClienteUtil
 * @generated
 */
public class ClientePersistenceImpl extends BasePersistenceImpl<Cliente>
	implements ClientePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ClienteUtil} to access the cliente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ClienteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, ClienteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, ClienteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_C_E = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, ClienteImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByC_E",
			new String[] { Long.class.getName() },
			ClienteModelImpl.IDEXPEDIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_E = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_E",
			new String[] { Long.class.getName() });

	/**
	 * Returns the cliente where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching cliente
	 * @throws pe.com.ibk.pepper.NoSuchClienteException if a matching cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente findByC_E(long idExpediente)
		throws NoSuchClienteException, SystemException {
		Cliente cliente = fetchByC_E(idExpediente);

		if (cliente == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idExpediente=");
			msg.append(idExpediente);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchClienteException(msg.toString());
		}

		return cliente;
	}

	/**
	 * Returns the cliente where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente fetchByC_E(long idExpediente) throws SystemException {
		return fetchByC_E(idExpediente, true);
	}

	/**
	 * Returns the cliente where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idExpediente the id expediente
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente fetchByC_E(long idExpediente, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idExpediente };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_E,
					finderArgs, this);
		}

		if (result instanceof Cliente) {
			Cliente cliente = (Cliente)result;

			if ((idExpediente != cliente.getIdExpediente())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CLIENTE_WHERE);

			query.append(_FINDER_COLUMN_C_E_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				List<Cliente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ClientePersistenceImpl.fetchByC_E(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Cliente cliente = list.get(0);

					result = cliente;

					cacheResult(cliente);

					if ((cliente.getIdExpediente() != idExpediente)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_E,
							finderArgs, cliente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Cliente)result;
		}
	}

	/**
	 * Removes the cliente where idExpediente = &#63; from the database.
	 *
	 * @param idExpediente the id expediente
	 * @return the cliente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente removeByC_E(long idExpediente)
		throws NoSuchClienteException, SystemException {
		Cliente cliente = findByC_E(idExpediente);

		return remove(cliente);
	}

	/**
	 * Returns the number of clientes where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @return the number of matching clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_E(long idExpediente) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_E;

		Object[] finderArgs = new Object[] { idExpediente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLIENTE_WHERE);

			query.append(_FINDER_COLUMN_C_E_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_E_IDEXPEDIENTE_2 = "cliente.idExpediente = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_DC_ID = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, ClienteImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByDC_ID",
			new String[] { Long.class.getName() },
			ClienteModelImpl.IDDATOCLIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DC_ID = new FinderPath(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDC_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the cliente where idDatoCliente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	 *
	 * @param idDatoCliente the id dato cliente
	 * @return the matching cliente
	 * @throws pe.com.ibk.pepper.NoSuchClienteException if a matching cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente findByDC_ID(long idDatoCliente)
		throws NoSuchClienteException, SystemException {
		Cliente cliente = fetchByDC_ID(idDatoCliente);

		if (cliente == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idDatoCliente=");
			msg.append(idDatoCliente);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchClienteException(msg.toString());
		}

		return cliente;
	}

	/**
	 * Returns the cliente where idDatoCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idDatoCliente the id dato cliente
	 * @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente fetchByDC_ID(long idDatoCliente) throws SystemException {
		return fetchByDC_ID(idDatoCliente, true);
	}

	/**
	 * Returns the cliente where idDatoCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idDatoCliente the id dato cliente
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching cliente, or <code>null</code> if a matching cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente fetchByDC_ID(long idDatoCliente, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idDatoCliente };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DC_ID,
					finderArgs, this);
		}

		if (result instanceof Cliente) {
			Cliente cliente = (Cliente)result;

			if ((idDatoCliente != cliente.getIdDatoCliente())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CLIENTE_WHERE);

			query.append(_FINDER_COLUMN_DC_ID_IDDATOCLIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idDatoCliente);

				List<Cliente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ClientePersistenceImpl.fetchByDC_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Cliente cliente = list.get(0);

					result = cliente;

					cacheResult(cliente);

					if ((cliente.getIdDatoCliente() != idDatoCliente)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID,
							finderArgs, cliente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Cliente)result;
		}
	}

	/**
	 * Removes the cliente where idDatoCliente = &#63; from the database.
	 *
	 * @param idDatoCliente the id dato cliente
	 * @return the cliente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente removeByDC_ID(long idDatoCliente)
		throws NoSuchClienteException, SystemException {
		Cliente cliente = findByDC_ID(idDatoCliente);

		return remove(cliente);
	}

	/**
	 * Returns the number of clientes where idDatoCliente = &#63;.
	 *
	 * @param idDatoCliente the id dato cliente
	 * @return the number of matching clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDC_ID(long idDatoCliente) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DC_ID;

		Object[] finderArgs = new Object[] { idDatoCliente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLIENTE_WHERE);

			query.append(_FINDER_COLUMN_DC_ID_IDDATOCLIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idDatoCliente);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DC_ID_IDDATOCLIENTE_2 = "cliente.idDatoCliente = ?";

	public ClientePersistenceImpl() {
		setModelClass(Cliente.class);
	}

	/**
	 * Caches the cliente in the entity cache if it is enabled.
	 *
	 * @param cliente the cliente
	 */
	@Override
	public void cacheResult(Cliente cliente) {
		EntityCacheUtil.putResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteImpl.class, cliente.getPrimaryKey(), cliente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_E,
			new Object[] { cliente.getIdExpediente() }, cliente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID,
			new Object[] { cliente.getIdDatoCliente() }, cliente);

		cliente.resetOriginalValues();
	}

	/**
	 * Caches the clientes in the entity cache if it is enabled.
	 *
	 * @param clientes the clientes
	 */
	@Override
	public void cacheResult(List<Cliente> clientes) {
		for (Cliente cliente : clientes) {
			if (EntityCacheUtil.getResult(
						ClienteModelImpl.ENTITY_CACHE_ENABLED,
						ClienteImpl.class, cliente.getPrimaryKey()) == null) {
				cacheResult(cliente);
			}
			else {
				cliente.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all clientes.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ClienteImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ClienteImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the cliente.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Cliente cliente) {
		EntityCacheUtil.removeResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteImpl.class, cliente.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(cliente);
	}

	@Override
	public void clearCache(List<Cliente> clientes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Cliente cliente : clientes) {
			EntityCacheUtil.removeResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
				ClienteImpl.class, cliente.getPrimaryKey());

			clearUniqueFindersCache(cliente);
		}
	}

	protected void cacheUniqueFindersCache(Cliente cliente) {
		if (cliente.isNew()) {
			Object[] args = new Object[] { cliente.getIdExpediente() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_E, args, cliente);

			args = new Object[] { cliente.getIdDatoCliente() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DC_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID, args, cliente);
		}
		else {
			ClienteModelImpl clienteModelImpl = (ClienteModelImpl)cliente;

			if ((clienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { cliente.getIdExpediente() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_E, args,
					cliente);
			}

			if ((clienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DC_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { cliente.getIdDatoCliente() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DC_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DC_ID, args,
					cliente);
			}
		}
	}

	protected void clearUniqueFindersCache(Cliente cliente) {
		ClienteModelImpl clienteModelImpl = (ClienteModelImpl)cliente;

		Object[] args = new Object[] { cliente.getIdExpediente() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_E, args);

		if ((clienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_E.getColumnBitmask()) != 0) {
			args = new Object[] { clienteModelImpl.getOriginalIdExpediente() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_E, args);
		}

		args = new Object[] { cliente.getIdDatoCliente() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DC_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_ID, args);

		if ((clienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_DC_ID.getColumnBitmask()) != 0) {
			args = new Object[] { clienteModelImpl.getOriginalIdDatoCliente() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DC_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DC_ID, args);
		}
	}

	/**
	 * Creates a new cliente with the primary key. Does not add the cliente to the database.
	 *
	 * @param idDatoCliente the primary key for the new cliente
	 * @return the new cliente
	 */
	@Override
	public Cliente create(long idDatoCliente) {
		Cliente cliente = new ClienteImpl();

		cliente.setNew(true);
		cliente.setPrimaryKey(idDatoCliente);

		return cliente;
	}

	/**
	 * Removes the cliente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idDatoCliente the primary key of the cliente
	 * @return the cliente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente remove(long idDatoCliente)
		throws NoSuchClienteException, SystemException {
		return remove((Serializable)idDatoCliente);
	}

	/**
	 * Removes the cliente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the cliente
	 * @return the cliente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente remove(Serializable primaryKey)
		throws NoSuchClienteException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Cliente cliente = (Cliente)session.get(ClienteImpl.class, primaryKey);

			if (cliente == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchClienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(cliente);
		}
		catch (NoSuchClienteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Cliente removeImpl(Cliente cliente) throws SystemException {
		cliente = toUnwrappedModel(cliente);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(cliente)) {
				cliente = (Cliente)session.get(ClienteImpl.class,
						cliente.getPrimaryKeyObj());
			}

			if (cliente != null) {
				session.delete(cliente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (cliente != null) {
			clearCache(cliente);
		}

		return cliente;
	}

	@Override
	public Cliente updateImpl(pe.com.ibk.pepper.model.Cliente cliente)
		throws SystemException {
		cliente = toUnwrappedModel(cliente);

		boolean isNew = cliente.isNew();

		Session session = null;

		try {
			session = openSession();

			if (cliente.isNew()) {
				session.save(cliente);

				cliente.setNew(false);
			}
			else {
				session.merge(cliente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ClienteModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
			ClienteImpl.class, cliente.getPrimaryKey(), cliente);

		clearUniqueFindersCache(cliente);
		cacheUniqueFindersCache(cliente);

		return cliente;
	}

	protected Cliente toUnwrappedModel(Cliente cliente) {
		if (cliente instanceof ClienteImpl) {
			return cliente;
		}

		ClienteImpl clienteImpl = new ClienteImpl();

		clienteImpl.setNew(cliente.isNew());
		clienteImpl.setPrimaryKey(cliente.getPrimaryKey());

		clienteImpl.setIdDatoCliente(cliente.getIdDatoCliente());
		clienteImpl.setIdExpediente(cliente.getIdExpediente());
		clienteImpl.setPrimerNombre(cliente.getPrimerNombre());
		clienteImpl.setSegundoNombre(cliente.getSegundoNombre());
		clienteImpl.setApellidoPaterno(cliente.getApellidoPaterno());
		clienteImpl.setApellidoMaterno(cliente.getApellidoMaterno());
		clienteImpl.setSexo(cliente.getSexo());
		clienteImpl.setCorreo(cliente.getCorreo());
		clienteImpl.setTelefono(cliente.getTelefono());
		clienteImpl.setEstadoCivil(cliente.getEstadoCivil());
		clienteImpl.setSituacionLaboral(cliente.getSituacionLaboral());
		clienteImpl.setRucEmpresaTrabajo(cliente.getRucEmpresaTrabajo());
		clienteImpl.setNivelEducacion(cliente.getNivelEducacion());
		clienteImpl.setOcupacion(cliente.getOcupacion());
		clienteImpl.setCargoActual(cliente.getCargoActual());
		clienteImpl.setActividadNegocio(cliente.getActividadNegocio());
		clienteImpl.setAntiguedadLaboral(cliente.getAntiguedadLaboral());
		clienteImpl.setFechaRegistro(cliente.getFechaRegistro());
		clienteImpl.setPorcentajeParticipacion(cliente.getPorcentajeParticipacion());
		clienteImpl.setIngresoMensualFijo(cliente.getIngresoMensualFijo());
		clienteImpl.setIngresoMensualVariable(cliente.getIngresoMensualVariable());
		clienteImpl.setDocumento(cliente.getDocumento());
		clienteImpl.setTerminosCondiciones(cliente.getTerminosCondiciones());
		clienteImpl.setTipoDocumento(cliente.getTipoDocumento());
		clienteImpl.setFlagPEP(cliente.getFlagPEP());
		clienteImpl.setFechaNacimiento(cliente.getFechaNacimiento());
		clienteImpl.setOperador(cliente.getOperador());

		return clienteImpl;
	}

	/**
	 * Returns the cliente with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the cliente
	 * @return the cliente
	 * @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente findByPrimaryKey(Serializable primaryKey)
		throws NoSuchClienteException, SystemException {
		Cliente cliente = fetchByPrimaryKey(primaryKey);

		if (cliente == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchClienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return cliente;
	}

	/**
	 * Returns the cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchClienteException} if it could not be found.
	 *
	 * @param idDatoCliente the primary key of the cliente
	 * @return the cliente
	 * @throws pe.com.ibk.pepper.NoSuchClienteException if a cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente findByPrimaryKey(long idDatoCliente)
		throws NoSuchClienteException, SystemException {
		return findByPrimaryKey((Serializable)idDatoCliente);
	}

	/**
	 * Returns the cliente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the cliente
	 * @return the cliente, or <code>null</code> if a cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Cliente cliente = (Cliente)EntityCacheUtil.getResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
				ClienteImpl.class, primaryKey);

		if (cliente == _nullCliente) {
			return null;
		}

		if (cliente == null) {
			Session session = null;

			try {
				session = openSession();

				cliente = (Cliente)session.get(ClienteImpl.class, primaryKey);

				if (cliente != null) {
					cacheResult(cliente);
				}
				else {
					EntityCacheUtil.putResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
						ClienteImpl.class, primaryKey, _nullCliente);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ClienteModelImpl.ENTITY_CACHE_ENABLED,
					ClienteImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return cliente;
	}

	/**
	 * Returns the cliente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idDatoCliente the primary key of the cliente
	 * @return the cliente, or <code>null</code> if a cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cliente fetchByPrimaryKey(long idDatoCliente)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idDatoCliente);
	}

	/**
	 * Returns all the clientes.
	 *
	 * @return the clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Cliente> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the clientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of clientes
	 * @param end the upper bound of the range of clientes (not inclusive)
	 * @return the range of clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Cliente> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the clientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of clientes
	 * @param end the upper bound of the range of clientes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Cliente> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Cliente> list = (List<Cliente>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLIENTE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLIENTE;

				if (pagination) {
					sql = sql.concat(ClienteModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Cliente>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Cliente>(list);
				}
				else {
					list = (List<Cliente>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the clientes from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Cliente cliente : findAll()) {
			remove(cliente);
		}
	}

	/**
	 * Returns the number of clientes.
	 *
	 * @return the number of clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLIENTE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the cliente persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.Cliente")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Cliente>> listenersList = new ArrayList<ModelListener<Cliente>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Cliente>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ClienteImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLIENTE = "SELECT cliente FROM Cliente cliente";
	private static final String _SQL_SELECT_CLIENTE_WHERE = "SELECT cliente FROM Cliente cliente WHERE ";
	private static final String _SQL_COUNT_CLIENTE = "SELECT COUNT(cliente) FROM Cliente cliente";
	private static final String _SQL_COUNT_CLIENTE_WHERE = "SELECT COUNT(cliente) FROM Cliente cliente WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "cliente.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Cliente exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Cliente exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ClientePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idDatoCliente", "idExpediente", "primerNombre", "segundoNombre",
				"apellidoPaterno", "apellidoMaterno", "sexo", "correo",
				"telefono", "estadoCivil", "situacionLaboral",
				"rucEmpresaTrabajo", "nivelEducacion", "ocupacion",
				"cargoActual", "actividadNegocio", "antiguedadLaboral",
				"fechaRegistro", "porcentajeParticipacion", "ingresoMensualFijo",
				"ingresoMensualVariable", "documento", "terminosCondiciones",
				"tipoDocumento", "flagPEP", "fechaNacimiento", "operador"
			});
	private static Cliente _nullCliente = new ClienteImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Cliente> toCacheModel() {
				return _nullClienteCacheModel;
			}
		};

	private static CacheModel<Cliente> _nullClienteCacheModel = new CacheModel<Cliente>() {
			@Override
			public Cliente toEntityModel() {
				return _nullCliente;
			}
		};
}