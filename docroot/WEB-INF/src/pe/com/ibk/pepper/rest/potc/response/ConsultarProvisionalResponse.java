
package pe.com.ibk.pepper.rest.potc.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsultarProvisionalResponse implements Serializable{

	private static final long serialVersionUID = -8299301626173500144L;
	
	@SerializedName(value="tarjetasProvisionales")
	private TarjetasProvisionales tarjetasProvisionales;

	public TarjetasProvisionales getTarjetasProvisionales() {
		return tarjetasProvisionales;
	}

	public void setTarjetasProvisionales(TarjetasProvisionales tarjetasProvisionales) {
		this.tarjetasProvisionales = tarjetasProvisionales;
	}
    
}
