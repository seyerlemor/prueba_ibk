package pe.com.ibk.pepper.bean;


public class SolicitudBean {

	private ExpedienteBean expedienteBean;
	private ServiciosBean serviciosBean;
	private ClienteBean clienteBean;
	private EquifaxBean equifaxBean;
	private ProductoBean productoBean;
	private OtcBean otcBean;	
	private UsuarioSessionBean usuarioSessionBean;
	private String tipoFlujo;
	private String restauracion;
	private RequestBean requestBean;
	private OfertasBean ofertasBean;
	private FHEntregaBean fhEntregaBean;

	public SolicitudBean() {
		super();
		expedienteBean = new ExpedienteBean();
		serviciosBean = new ServiciosBean();
		clienteBean = new ClienteBean();
		equifaxBean = new EquifaxBean();
		productoBean = new ProductoBean();
		otcBean = new OtcBean();
		usuarioSessionBean = new UsuarioSessionBean();
		requestBean = new RequestBean();
		fhEntregaBean = new FHEntregaBean();
	}

	public String getRestauracion() {
		return restauracion;
	}

	public void setRestauracion(String restauracion) {
		this.restauracion = restauracion;
	}
	
	public String getTipoFlujo() {
		return tipoFlujo;
	}


	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}

	public ExpedienteBean getExpedienteBean() {
		return expedienteBean;
	}
	public void setExpedienteBean(ExpedienteBean expedienteBean) {
		this.expedienteBean = expedienteBean;
	}
	public ServiciosBean getServiciosBean() {
		return serviciosBean;
	}
	public void setServiciosBean(ServiciosBean serviciosBean) {
		this.serviciosBean = serviciosBean;
	}
	public ClienteBean getClienteBean() {
		return clienteBean;
	}
	public void setClienteBean(ClienteBean clienteBean) {
		this.clienteBean = clienteBean;
	}
	public EquifaxBean getEquifaxBean() {
		return equifaxBean;
	}
	public void setEquifaxBean(EquifaxBean equifaxBean) {
		this.equifaxBean = equifaxBean;
	}
	public ProductoBean getProductoBean() {
		return productoBean;
	}
	public void setProductoBean(ProductoBean productoBean) {
		this.productoBean = productoBean;
	}
	public OtcBean getOtcBean() {
		return otcBean;
	}
	public void setOtcBean(OtcBean otcBean) {
		this.otcBean = otcBean;
	}
	public UsuarioSessionBean getUsuarioSessionBean() {
		return usuarioSessionBean;
	}
	public void setUsuarioSessionBean(UsuarioSessionBean usuarioSessionBean) {
		this.usuarioSessionBean = usuarioSessionBean;
	}

	public RequestBean getRequestBean() {
		return requestBean;
	}

	public void setRequestBean(RequestBean requestBean) {
		this.requestBean = requestBean;
	}

	public OfertasBean getOfertasBean() {
		return ofertasBean;
	}

	public void setOfertasBean(OfertasBean ofertasBean) {
		this.ofertasBean = ofertasBean;
	}

	public FHEntregaBean getFhEntregaBean() {
		return fhEntregaBean;
	}

	public void setFhEntregaBean(FHEntregaBean fhEntregaBean) {
		this.fhEntregaBean = fhEntregaBean;
	}

	@Override
	public String toString() {
		return "PasoBean [expedienteBean=" + expedienteBean
				+ ", serviciosBean=" + serviciosBean + ", clienteBean="
				+ clienteBean + ", equifaxBean="
				+ equifaxBean + ", productoBean=" + productoBean + ", otcBean="
				+ otcBean + ", usuarioSessionBean=" + usuarioSessionBean + ", tipoFlujo="
				+ tipoFlujo + ", restauracion=" + restauracion
				+ ", requestBean=" + requestBean + ", ofertasBean="
				+ ofertasBean + ", fhEntregaBean=" + fhEntregaBean + "]";
	}

}
