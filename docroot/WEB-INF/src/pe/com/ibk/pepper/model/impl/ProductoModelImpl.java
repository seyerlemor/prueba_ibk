/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import pe.com.ibk.pepper.model.Producto;
import pe.com.ibk.pepper.model.ProductoModel;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the Producto service. Represents a row in the &quot;T_PRODUCTO_CLIENTE&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link pe.com.ibk.pepper.model.ProductoModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ProductoImpl}.
 * </p>
 *
 * @author Interbank
 * @see ProductoImpl
 * @see pe.com.ibk.pepper.model.Producto
 * @see pe.com.ibk.pepper.model.ProductoModel
 * @generated
 */
public class ProductoModelImpl extends BaseModelImpl<Producto>
	implements ProductoModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a producto model instance should use the {@link pe.com.ibk.pepper.model.Producto} interface instead.
	 */
	public static final String TABLE_NAME = "T_PRODUCTO_CLIENTE";
	public static final Object[][] TABLE_COLUMNS = {
			{ "prcl_IdProducto", Types.BIGINT },
			{ "xpdt_IdExpediente", Types.BIGINT },
			{ "prcl_TipoProducto", Types.VARCHAR },
			{ "prcl_CodigoProducto", Types.VARCHAR },
			{ "prcl_Moneda", Types.VARCHAR },
			{ "prcl_MarcaProducto", Types.VARCHAR },
			{ "prcl_LineaCredito", Types.VARCHAR },
			{ "prcl_DiaPago", Types.VARCHAR },
			{ "prcl_TasaProducto", Types.VARCHAR },
			{ "prcl_NombreTitularProducto", Types.VARCHAR },
			{ "prcl_TipoSeguro", Types.VARCHAR },
			{ "prcl_CostoSeguro", Types.VARCHAR },
			{ "prcl_FechaRegistro", Types.TIMESTAMP },
			{ "prcl_FlagEnvioEECCFisico", Types.VARCHAR },
			{ "prcl_FlagEnvioEECCEmail", Types.VARCHAR },
			{ "prcl_FlagTerminos", Types.VARCHAR },
			{ "prcl_FlagCargoCompra", Types.VARCHAR },
			{ "prcl_Claveventa", Types.VARCHAR },
			{ "prcl_CodigoOperacion", Types.VARCHAR },
			{ "prcl_FechaHoraOperacion", Types.TIMESTAMP },
			{ "prcl_CodigoUnico", Types.VARCHAR },
			{ "prcl_NumTarjetaTitular", Types.VARCHAR },
			{ "prcl_NumCuenta", Types.VARCHAR },
			{ "prcl_FechaAltaTitular", Types.TIMESTAMP },
			{ "prcl_FechaVencTitular", Types.TIMESTAMP },
			{ "prcl_NumTarjetaProvisional", Types.VARCHAR },
			{ "prcl_FechaAltaProvisional", Types.TIMESTAMP },
			{ "prcl_FechaVencProvisional", Types.TIMESTAMP },
			{ "prcl_RazonNoUsoTarjeta", Types.VARCHAR },
			{ "prcl_Motivo", Types.VARCHAR }
		};
	public static final String TABLE_SQL_CREATE = "create table T_PRODUCTO_CLIENTE (prcl_IdProducto LONG not null primary key IDENTITY,xpdt_IdExpediente LONG,prcl_TipoProducto VARCHAR(75) null,prcl_CodigoProducto VARCHAR(75) null,prcl_Moneda VARCHAR(75) null,prcl_MarcaProducto VARCHAR(75) null,prcl_LineaCredito VARCHAR(75) null,prcl_DiaPago VARCHAR(75) null,prcl_TasaProducto VARCHAR(75) null,prcl_NombreTitularProducto VARCHAR(75) null,prcl_TipoSeguro VARCHAR(75) null,prcl_CostoSeguro VARCHAR(75) null,prcl_FechaRegistro DATE null,prcl_FlagEnvioEECCFisico VARCHAR(75) null,prcl_FlagEnvioEECCEmail VARCHAR(75) null,prcl_FlagTerminos VARCHAR(75) null,prcl_FlagCargoCompra VARCHAR(75) null,prcl_Claveventa VARCHAR(75) null,prcl_CodigoOperacion VARCHAR(75) null,prcl_FechaHoraOperacion DATE null,prcl_CodigoUnico VARCHAR(75) null,prcl_NumTarjetaTitular VARCHAR(75) null,prcl_NumCuenta VARCHAR(75) null,prcl_FechaAltaTitular DATE null,prcl_FechaVencTitular DATE null,prcl_NumTarjetaProvisional VARCHAR(75) null,prcl_FechaAltaProvisional DATE null,prcl_FechaVencProvisional DATE null,prcl_RazonNoUsoTarjeta VARCHAR(75) null,prcl_Motivo VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table T_PRODUCTO_CLIENTE";
	public static final String ORDER_BY_JPQL = " ORDER BY producto.idProducto ASC";
	public static final String ORDER_BY_SQL = " ORDER BY T_PRODUCTO_CLIENTE.prcl_IdProducto ASC";
	public static final String DATA_SOURCE = "pepperDataSource";
	public static final String SESSION_FACTORY = "pepperSessionFactory";
	public static final String TX_MANAGER = "pepperTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.pe.com.ibk.pepper.model.Producto"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.pe.com.ibk.pepper.model.Producto"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.pe.com.ibk.pepper.model.Producto"),
			true);
	public static long IDEXPEDIENTE_COLUMN_BITMASK = 1L;
	public static long IDPRODUCTO_COLUMN_BITMASK = 2L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.pe.com.ibk.pepper.model.Producto"));

	public ProductoModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _idProducto;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdProducto(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idProducto;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return Producto.class;
	}

	@Override
	public String getModelClassName() {
		return Producto.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idProducto", getIdProducto());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("tipoProducto", getTipoProducto());
		attributes.put("codigoProducto", getCodigoProducto());
		attributes.put("moneda", getMoneda());
		attributes.put("marcaProducto", getMarcaProducto());
		attributes.put("lineaCredito", getLineaCredito());
		attributes.put("diaPago", getDiaPago());
		attributes.put("tasaProducto", getTasaProducto());
		attributes.put("nombreTitularProducto", getNombreTitularProducto());
		attributes.put("tipoSeguro", getTipoSeguro());
		attributes.put("costoSeguro", getCostoSeguro());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("flagEnvioEECCFisico", getFlagEnvioEECCFisico());
		attributes.put("flagEnvioEECCEmail", getFlagEnvioEECCEmail());
		attributes.put("flagTerminos", getFlagTerminos());
		attributes.put("flagCargoCompra", getFlagCargoCompra());
		attributes.put("claveventa", getClaveventa());
		attributes.put("codigoOperacion", getCodigoOperacion());
		attributes.put("fechaHoraOperacion", getFechaHoraOperacion());
		attributes.put("codigoUnico", getCodigoUnico());
		attributes.put("numTarjetaTitular", getNumTarjetaTitular());
		attributes.put("numCuenta", getNumCuenta());
		attributes.put("fechaAltaTitular", getFechaAltaTitular());
		attributes.put("fechaVencTitular", getFechaVencTitular());
		attributes.put("numTarjetaProvisional", getNumTarjetaProvisional());
		attributes.put("fechaAltaProvisional", getFechaAltaProvisional());
		attributes.put("fechaVencProvisional", getFechaVencProvisional());
		attributes.put("RazonNoUsoTarjeta", getRazonNoUsoTarjeta());
		attributes.put("Motivo", getMotivo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idProducto = (Long)attributes.get("idProducto");

		if (idProducto != null) {
			setIdProducto(idProducto);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String tipoProducto = (String)attributes.get("tipoProducto");

		if (tipoProducto != null) {
			setTipoProducto(tipoProducto);
		}

		String codigoProducto = (String)attributes.get("codigoProducto");

		if (codigoProducto != null) {
			setCodigoProducto(codigoProducto);
		}

		String moneda = (String)attributes.get("moneda");

		if (moneda != null) {
			setMoneda(moneda);
		}

		String marcaProducto = (String)attributes.get("marcaProducto");

		if (marcaProducto != null) {
			setMarcaProducto(marcaProducto);
		}

		String lineaCredito = (String)attributes.get("lineaCredito");

		if (lineaCredito != null) {
			setLineaCredito(lineaCredito);
		}

		String diaPago = (String)attributes.get("diaPago");

		if (diaPago != null) {
			setDiaPago(diaPago);
		}

		String tasaProducto = (String)attributes.get("tasaProducto");

		if (tasaProducto != null) {
			setTasaProducto(tasaProducto);
		}

		String nombreTitularProducto = (String)attributes.get(
				"nombreTitularProducto");

		if (nombreTitularProducto != null) {
			setNombreTitularProducto(nombreTitularProducto);
		}

		String tipoSeguro = (String)attributes.get("tipoSeguro");

		if (tipoSeguro != null) {
			setTipoSeguro(tipoSeguro);
		}

		String costoSeguro = (String)attributes.get("costoSeguro");

		if (costoSeguro != null) {
			setCostoSeguro(costoSeguro);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String flagEnvioEECCFisico = (String)attributes.get(
				"flagEnvioEECCFisico");

		if (flagEnvioEECCFisico != null) {
			setFlagEnvioEECCFisico(flagEnvioEECCFisico);
		}

		String flagEnvioEECCEmail = (String)attributes.get("flagEnvioEECCEmail");

		if (flagEnvioEECCEmail != null) {
			setFlagEnvioEECCEmail(flagEnvioEECCEmail);
		}

		String flagTerminos = (String)attributes.get("flagTerminos");

		if (flagTerminos != null) {
			setFlagTerminos(flagTerminos);
		}

		String flagCargoCompra = (String)attributes.get("flagCargoCompra");

		if (flagCargoCompra != null) {
			setFlagCargoCompra(flagCargoCompra);
		}

		String claveventa = (String)attributes.get("claveventa");

		if (claveventa != null) {
			setClaveventa(claveventa);
		}

		String codigoOperacion = (String)attributes.get("codigoOperacion");

		if (codigoOperacion != null) {
			setCodigoOperacion(codigoOperacion);
		}

		Date fechaHoraOperacion = (Date)attributes.get("fechaHoraOperacion");

		if (fechaHoraOperacion != null) {
			setFechaHoraOperacion(fechaHoraOperacion);
		}

		String codigoUnico = (String)attributes.get("codigoUnico");

		if (codigoUnico != null) {
			setCodigoUnico(codigoUnico);
		}

		String numTarjetaTitular = (String)attributes.get("numTarjetaTitular");

		if (numTarjetaTitular != null) {
			setNumTarjetaTitular(numTarjetaTitular);
		}

		String numCuenta = (String)attributes.get("numCuenta");

		if (numCuenta != null) {
			setNumCuenta(numCuenta);
		}

		Date fechaAltaTitular = (Date)attributes.get("fechaAltaTitular");

		if (fechaAltaTitular != null) {
			setFechaAltaTitular(fechaAltaTitular);
		}

		Date fechaVencTitular = (Date)attributes.get("fechaVencTitular");

		if (fechaVencTitular != null) {
			setFechaVencTitular(fechaVencTitular);
		}

		String numTarjetaProvisional = (String)attributes.get(
				"numTarjetaProvisional");

		if (numTarjetaProvisional != null) {
			setNumTarjetaProvisional(numTarjetaProvisional);
		}

		Date fechaAltaProvisional = (Date)attributes.get("fechaAltaProvisional");

		if (fechaAltaProvisional != null) {
			setFechaAltaProvisional(fechaAltaProvisional);
		}

		Date fechaVencProvisional = (Date)attributes.get("fechaVencProvisional");

		if (fechaVencProvisional != null) {
			setFechaVencProvisional(fechaVencProvisional);
		}

		String RazonNoUsoTarjeta = (String)attributes.get("RazonNoUsoTarjeta");

		if (RazonNoUsoTarjeta != null) {
			setRazonNoUsoTarjeta(RazonNoUsoTarjeta);
		}

		String Motivo = (String)attributes.get("Motivo");

		if (Motivo != null) {
			setMotivo(Motivo);
		}
	}

	@Override
	public long getIdProducto() {
		return _idProducto;
	}

	@Override
	public void setIdProducto(long idProducto) {
		_columnBitmask |= IDPRODUCTO_COLUMN_BITMASK;

		if (!_setOriginalIdProducto) {
			_setOriginalIdProducto = true;

			_originalIdProducto = _idProducto;
		}

		_idProducto = idProducto;
	}

	public long getOriginalIdProducto() {
		return _originalIdProducto;
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_columnBitmask |= IDEXPEDIENTE_COLUMN_BITMASK;

		if (!_setOriginalIdExpediente) {
			_setOriginalIdExpediente = true;

			_originalIdExpediente = _idExpediente;
		}

		_idExpediente = idExpediente;
	}

	public long getOriginalIdExpediente() {
		return _originalIdExpediente;
	}

	@Override
	public String getTipoProducto() {
		if (_tipoProducto == null) {
			return StringPool.BLANK;
		}
		else {
			return _tipoProducto;
		}
	}

	@Override
	public void setTipoProducto(String tipoProducto) {
		_tipoProducto = tipoProducto;
	}

	@Override
	public String getCodigoProducto() {
		if (_codigoProducto == null) {
			return StringPool.BLANK;
		}
		else {
			return _codigoProducto;
		}
	}

	@Override
	public void setCodigoProducto(String codigoProducto) {
		_codigoProducto = codigoProducto;
	}

	@Override
	public String getMoneda() {
		if (_moneda == null) {
			return StringPool.BLANK;
		}
		else {
			return _moneda;
		}
	}

	@Override
	public void setMoneda(String moneda) {
		_moneda = moneda;
	}

	@Override
	public String getMarcaProducto() {
		if (_marcaProducto == null) {
			return StringPool.BLANK;
		}
		else {
			return _marcaProducto;
		}
	}

	@Override
	public void setMarcaProducto(String marcaProducto) {
		_marcaProducto = marcaProducto;
	}

	@Override
	public String getLineaCredito() {
		if (_lineaCredito == null) {
			return StringPool.BLANK;
		}
		else {
			return _lineaCredito;
		}
	}

	@Override
	public void setLineaCredito(String lineaCredito) {
		_lineaCredito = lineaCredito;
	}

	@Override
	public String getDiaPago() {
		if (_diaPago == null) {
			return StringPool.BLANK;
		}
		else {
			return _diaPago;
		}
	}

	@Override
	public void setDiaPago(String diaPago) {
		_diaPago = diaPago;
	}

	@Override
	public String getTasaProducto() {
		if (_tasaProducto == null) {
			return StringPool.BLANK;
		}
		else {
			return _tasaProducto;
		}
	}

	@Override
	public void setTasaProducto(String tasaProducto) {
		_tasaProducto = tasaProducto;
	}

	@Override
	public String getNombreTitularProducto() {
		if (_nombreTitularProducto == null) {
			return StringPool.BLANK;
		}
		else {
			return _nombreTitularProducto;
		}
	}

	@Override
	public void setNombreTitularProducto(String nombreTitularProducto) {
		_nombreTitularProducto = nombreTitularProducto;
	}

	@Override
	public String getTipoSeguro() {
		if (_tipoSeguro == null) {
			return StringPool.BLANK;
		}
		else {
			return _tipoSeguro;
		}
	}

	@Override
	public void setTipoSeguro(String tipoSeguro) {
		_tipoSeguro = tipoSeguro;
	}

	@Override
	public String getCostoSeguro() {
		if (_costoSeguro == null) {
			return StringPool.BLANK;
		}
		else {
			return _costoSeguro;
		}
	}

	@Override
	public void setCostoSeguro(String costoSeguro) {
		_costoSeguro = costoSeguro;
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	@Override
	public String getFlagEnvioEECCFisico() {
		if (_flagEnvioEECCFisico == null) {
			return StringPool.BLANK;
		}
		else {
			return _flagEnvioEECCFisico;
		}
	}

	@Override
	public void setFlagEnvioEECCFisico(String flagEnvioEECCFisico) {
		_flagEnvioEECCFisico = flagEnvioEECCFisico;
	}

	@Override
	public String getFlagEnvioEECCEmail() {
		if (_flagEnvioEECCEmail == null) {
			return StringPool.BLANK;
		}
		else {
			return _flagEnvioEECCEmail;
		}
	}

	@Override
	public void setFlagEnvioEECCEmail(String flagEnvioEECCEmail) {
		_flagEnvioEECCEmail = flagEnvioEECCEmail;
	}

	@Override
	public String getFlagTerminos() {
		if (_flagTerminos == null) {
			return StringPool.BLANK;
		}
		else {
			return _flagTerminos;
		}
	}

	@Override
	public void setFlagTerminos(String flagTerminos) {
		_flagTerminos = flagTerminos;
	}

	@Override
	public String getFlagCargoCompra() {
		if (_flagCargoCompra == null) {
			return StringPool.BLANK;
		}
		else {
			return _flagCargoCompra;
		}
	}

	@Override
	public void setFlagCargoCompra(String flagCargoCompra) {
		_flagCargoCompra = flagCargoCompra;
	}

	@Override
	public String getClaveventa() {
		if (_claveventa == null) {
			return StringPool.BLANK;
		}
		else {
			return _claveventa;
		}
	}

	@Override
	public void setClaveventa(String claveventa) {
		_claveventa = claveventa;
	}

	@Override
	public String getCodigoOperacion() {
		if (_codigoOperacion == null) {
			return StringPool.BLANK;
		}
		else {
			return _codigoOperacion;
		}
	}

	@Override
	public void setCodigoOperacion(String codigoOperacion) {
		_codigoOperacion = codigoOperacion;
	}

	@Override
	public Date getFechaHoraOperacion() {
		return _fechaHoraOperacion;
	}

	@Override
	public void setFechaHoraOperacion(Date fechaHoraOperacion) {
		_fechaHoraOperacion = fechaHoraOperacion;
	}

	@Override
	public String getCodigoUnico() {
		if (_codigoUnico == null) {
			return StringPool.BLANK;
		}
		else {
			return _codigoUnico;
		}
	}

	@Override
	public void setCodigoUnico(String codigoUnico) {
		_codigoUnico = codigoUnico;
	}

	@Override
	public String getNumTarjetaTitular() {
		if (_numTarjetaTitular == null) {
			return StringPool.BLANK;
		}
		else {
			return _numTarjetaTitular;
		}
	}

	@Override
	public void setNumTarjetaTitular(String numTarjetaTitular) {
		_numTarjetaTitular = numTarjetaTitular;
	}

	@Override
	public String getNumCuenta() {
		if (_numCuenta == null) {
			return StringPool.BLANK;
		}
		else {
			return _numCuenta;
		}
	}

	@Override
	public void setNumCuenta(String numCuenta) {
		_numCuenta = numCuenta;
	}

	@Override
	public Date getFechaAltaTitular() {
		return _fechaAltaTitular;
	}

	@Override
	public void setFechaAltaTitular(Date fechaAltaTitular) {
		_fechaAltaTitular = fechaAltaTitular;
	}

	@Override
	public Date getFechaVencTitular() {
		return _fechaVencTitular;
	}

	@Override
	public void setFechaVencTitular(Date fechaVencTitular) {
		_fechaVencTitular = fechaVencTitular;
	}

	@Override
	public String getNumTarjetaProvisional() {
		if (_numTarjetaProvisional == null) {
			return StringPool.BLANK;
		}
		else {
			return _numTarjetaProvisional;
		}
	}

	@Override
	public void setNumTarjetaProvisional(String numTarjetaProvisional) {
		_numTarjetaProvisional = numTarjetaProvisional;
	}

	@Override
	public Date getFechaAltaProvisional() {
		return _fechaAltaProvisional;
	}

	@Override
	public void setFechaAltaProvisional(Date fechaAltaProvisional) {
		_fechaAltaProvisional = fechaAltaProvisional;
	}

	@Override
	public Date getFechaVencProvisional() {
		return _fechaVencProvisional;
	}

	@Override
	public void setFechaVencProvisional(Date fechaVencProvisional) {
		_fechaVencProvisional = fechaVencProvisional;
	}

	@Override
	public String getRazonNoUsoTarjeta() {
		if (_RazonNoUsoTarjeta == null) {
			return StringPool.BLANK;
		}
		else {
			return _RazonNoUsoTarjeta;
		}
	}

	@Override
	public void setRazonNoUsoTarjeta(String RazonNoUsoTarjeta) {
		_RazonNoUsoTarjeta = RazonNoUsoTarjeta;
	}

	@Override
	public String getMotivo() {
		if (_Motivo == null) {
			return StringPool.BLANK;
		}
		else {
			return _Motivo;
		}
	}

	@Override
	public void setMotivo(String Motivo) {
		_Motivo = Motivo;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			Producto.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public Producto toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (Producto)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		ProductoImpl productoImpl = new ProductoImpl();

		productoImpl.setIdProducto(getIdProducto());
		productoImpl.setIdExpediente(getIdExpediente());
		productoImpl.setTipoProducto(getTipoProducto());
		productoImpl.setCodigoProducto(getCodigoProducto());
		productoImpl.setMoneda(getMoneda());
		productoImpl.setMarcaProducto(getMarcaProducto());
		productoImpl.setLineaCredito(getLineaCredito());
		productoImpl.setDiaPago(getDiaPago());
		productoImpl.setTasaProducto(getTasaProducto());
		productoImpl.setNombreTitularProducto(getNombreTitularProducto());
		productoImpl.setTipoSeguro(getTipoSeguro());
		productoImpl.setCostoSeguro(getCostoSeguro());
		productoImpl.setFechaRegistro(getFechaRegistro());
		productoImpl.setFlagEnvioEECCFisico(getFlagEnvioEECCFisico());
		productoImpl.setFlagEnvioEECCEmail(getFlagEnvioEECCEmail());
		productoImpl.setFlagTerminos(getFlagTerminos());
		productoImpl.setFlagCargoCompra(getFlagCargoCompra());
		productoImpl.setClaveventa(getClaveventa());
		productoImpl.setCodigoOperacion(getCodigoOperacion());
		productoImpl.setFechaHoraOperacion(getFechaHoraOperacion());
		productoImpl.setCodigoUnico(getCodigoUnico());
		productoImpl.setNumTarjetaTitular(getNumTarjetaTitular());
		productoImpl.setNumCuenta(getNumCuenta());
		productoImpl.setFechaAltaTitular(getFechaAltaTitular());
		productoImpl.setFechaVencTitular(getFechaVencTitular());
		productoImpl.setNumTarjetaProvisional(getNumTarjetaProvisional());
		productoImpl.setFechaAltaProvisional(getFechaAltaProvisional());
		productoImpl.setFechaVencProvisional(getFechaVencProvisional());
		productoImpl.setRazonNoUsoTarjeta(getRazonNoUsoTarjeta());
		productoImpl.setMotivo(getMotivo());

		productoImpl.resetOriginalValues();

		return productoImpl;
	}

	@Override
	public int compareTo(Producto producto) {
		long primaryKey = producto.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Producto)) {
			return false;
		}

		Producto producto = (Producto)obj;

		long primaryKey = producto.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		ProductoModelImpl productoModelImpl = this;

		productoModelImpl._originalIdProducto = productoModelImpl._idProducto;

		productoModelImpl._setOriginalIdProducto = false;

		productoModelImpl._originalIdExpediente = productoModelImpl._idExpediente;

		productoModelImpl._setOriginalIdExpediente = false;

		productoModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<Producto> toCacheModel() {
		ProductoCacheModel productoCacheModel = new ProductoCacheModel();

		productoCacheModel.idProducto = getIdProducto();

		productoCacheModel.idExpediente = getIdExpediente();

		productoCacheModel.tipoProducto = getTipoProducto();

		String tipoProducto = productoCacheModel.tipoProducto;

		if ((tipoProducto != null) && (tipoProducto.length() == 0)) {
			productoCacheModel.tipoProducto = null;
		}

		productoCacheModel.codigoProducto = getCodigoProducto();

		String codigoProducto = productoCacheModel.codigoProducto;

		if ((codigoProducto != null) && (codigoProducto.length() == 0)) {
			productoCacheModel.codigoProducto = null;
		}

		productoCacheModel.moneda = getMoneda();

		String moneda = productoCacheModel.moneda;

		if ((moneda != null) && (moneda.length() == 0)) {
			productoCacheModel.moneda = null;
		}

		productoCacheModel.marcaProducto = getMarcaProducto();

		String marcaProducto = productoCacheModel.marcaProducto;

		if ((marcaProducto != null) && (marcaProducto.length() == 0)) {
			productoCacheModel.marcaProducto = null;
		}

		productoCacheModel.lineaCredito = getLineaCredito();

		String lineaCredito = productoCacheModel.lineaCredito;

		if ((lineaCredito != null) && (lineaCredito.length() == 0)) {
			productoCacheModel.lineaCredito = null;
		}

		productoCacheModel.diaPago = getDiaPago();

		String diaPago = productoCacheModel.diaPago;

		if ((diaPago != null) && (diaPago.length() == 0)) {
			productoCacheModel.diaPago = null;
		}

		productoCacheModel.tasaProducto = getTasaProducto();

		String tasaProducto = productoCacheModel.tasaProducto;

		if ((tasaProducto != null) && (tasaProducto.length() == 0)) {
			productoCacheModel.tasaProducto = null;
		}

		productoCacheModel.nombreTitularProducto = getNombreTitularProducto();

		String nombreTitularProducto = productoCacheModel.nombreTitularProducto;

		if ((nombreTitularProducto != null) &&
				(nombreTitularProducto.length() == 0)) {
			productoCacheModel.nombreTitularProducto = null;
		}

		productoCacheModel.tipoSeguro = getTipoSeguro();

		String tipoSeguro = productoCacheModel.tipoSeguro;

		if ((tipoSeguro != null) && (tipoSeguro.length() == 0)) {
			productoCacheModel.tipoSeguro = null;
		}

		productoCacheModel.costoSeguro = getCostoSeguro();

		String costoSeguro = productoCacheModel.costoSeguro;

		if ((costoSeguro != null) && (costoSeguro.length() == 0)) {
			productoCacheModel.costoSeguro = null;
		}

		Date fechaRegistro = getFechaRegistro();

		if (fechaRegistro != null) {
			productoCacheModel.fechaRegistro = fechaRegistro.getTime();
		}
		else {
			productoCacheModel.fechaRegistro = Long.MIN_VALUE;
		}

		productoCacheModel.flagEnvioEECCFisico = getFlagEnvioEECCFisico();

		String flagEnvioEECCFisico = productoCacheModel.flagEnvioEECCFisico;

		if ((flagEnvioEECCFisico != null) &&
				(flagEnvioEECCFisico.length() == 0)) {
			productoCacheModel.flagEnvioEECCFisico = null;
		}

		productoCacheModel.flagEnvioEECCEmail = getFlagEnvioEECCEmail();

		String flagEnvioEECCEmail = productoCacheModel.flagEnvioEECCEmail;

		if ((flagEnvioEECCEmail != null) && (flagEnvioEECCEmail.length() == 0)) {
			productoCacheModel.flagEnvioEECCEmail = null;
		}

		productoCacheModel.flagTerminos = getFlagTerminos();

		String flagTerminos = productoCacheModel.flagTerminos;

		if ((flagTerminos != null) && (flagTerminos.length() == 0)) {
			productoCacheModel.flagTerminos = null;
		}

		productoCacheModel.flagCargoCompra = getFlagCargoCompra();

		String flagCargoCompra = productoCacheModel.flagCargoCompra;

		if ((flagCargoCompra != null) && (flagCargoCompra.length() == 0)) {
			productoCacheModel.flagCargoCompra = null;
		}

		productoCacheModel.claveventa = getClaveventa();

		String claveventa = productoCacheModel.claveventa;

		if ((claveventa != null) && (claveventa.length() == 0)) {
			productoCacheModel.claveventa = null;
		}

		productoCacheModel.codigoOperacion = getCodigoOperacion();

		String codigoOperacion = productoCacheModel.codigoOperacion;

		if ((codigoOperacion != null) && (codigoOperacion.length() == 0)) {
			productoCacheModel.codigoOperacion = null;
		}

		Date fechaHoraOperacion = getFechaHoraOperacion();

		if (fechaHoraOperacion != null) {
			productoCacheModel.fechaHoraOperacion = fechaHoraOperacion.getTime();
		}
		else {
			productoCacheModel.fechaHoraOperacion = Long.MIN_VALUE;
		}

		productoCacheModel.codigoUnico = getCodigoUnico();

		String codigoUnico = productoCacheModel.codigoUnico;

		if ((codigoUnico != null) && (codigoUnico.length() == 0)) {
			productoCacheModel.codigoUnico = null;
		}

		productoCacheModel.numTarjetaTitular = getNumTarjetaTitular();

		String numTarjetaTitular = productoCacheModel.numTarjetaTitular;

		if ((numTarjetaTitular != null) && (numTarjetaTitular.length() == 0)) {
			productoCacheModel.numTarjetaTitular = null;
		}

		productoCacheModel.numCuenta = getNumCuenta();

		String numCuenta = productoCacheModel.numCuenta;

		if ((numCuenta != null) && (numCuenta.length() == 0)) {
			productoCacheModel.numCuenta = null;
		}

		Date fechaAltaTitular = getFechaAltaTitular();

		if (fechaAltaTitular != null) {
			productoCacheModel.fechaAltaTitular = fechaAltaTitular.getTime();
		}
		else {
			productoCacheModel.fechaAltaTitular = Long.MIN_VALUE;
		}

		Date fechaVencTitular = getFechaVencTitular();

		if (fechaVencTitular != null) {
			productoCacheModel.fechaVencTitular = fechaVencTitular.getTime();
		}
		else {
			productoCacheModel.fechaVencTitular = Long.MIN_VALUE;
		}

		productoCacheModel.numTarjetaProvisional = getNumTarjetaProvisional();

		String numTarjetaProvisional = productoCacheModel.numTarjetaProvisional;

		if ((numTarjetaProvisional != null) &&
				(numTarjetaProvisional.length() == 0)) {
			productoCacheModel.numTarjetaProvisional = null;
		}

		Date fechaAltaProvisional = getFechaAltaProvisional();

		if (fechaAltaProvisional != null) {
			productoCacheModel.fechaAltaProvisional = fechaAltaProvisional.getTime();
		}
		else {
			productoCacheModel.fechaAltaProvisional = Long.MIN_VALUE;
		}

		Date fechaVencProvisional = getFechaVencProvisional();

		if (fechaVencProvisional != null) {
			productoCacheModel.fechaVencProvisional = fechaVencProvisional.getTime();
		}
		else {
			productoCacheModel.fechaVencProvisional = Long.MIN_VALUE;
		}

		productoCacheModel.RazonNoUsoTarjeta = getRazonNoUsoTarjeta();

		String RazonNoUsoTarjeta = productoCacheModel.RazonNoUsoTarjeta;

		if ((RazonNoUsoTarjeta != null) && (RazonNoUsoTarjeta.length() == 0)) {
			productoCacheModel.RazonNoUsoTarjeta = null;
		}

		productoCacheModel.Motivo = getMotivo();

		String Motivo = productoCacheModel.Motivo;

		if ((Motivo != null) && (Motivo.length() == 0)) {
			productoCacheModel.Motivo = null;
		}

		return productoCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(61);

		sb.append("{idProducto=");
		sb.append(getIdProducto());
		sb.append(", idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", tipoProducto=");
		sb.append(getTipoProducto());
		sb.append(", codigoProducto=");
		sb.append(getCodigoProducto());
		sb.append(", moneda=");
		sb.append(getMoneda());
		sb.append(", marcaProducto=");
		sb.append(getMarcaProducto());
		sb.append(", lineaCredito=");
		sb.append(getLineaCredito());
		sb.append(", diaPago=");
		sb.append(getDiaPago());
		sb.append(", tasaProducto=");
		sb.append(getTasaProducto());
		sb.append(", nombreTitularProducto=");
		sb.append(getNombreTitularProducto());
		sb.append(", tipoSeguro=");
		sb.append(getTipoSeguro());
		sb.append(", costoSeguro=");
		sb.append(getCostoSeguro());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append(", flagEnvioEECCFisico=");
		sb.append(getFlagEnvioEECCFisico());
		sb.append(", flagEnvioEECCEmail=");
		sb.append(getFlagEnvioEECCEmail());
		sb.append(", flagTerminos=");
		sb.append(getFlagTerminos());
		sb.append(", flagCargoCompra=");
		sb.append(getFlagCargoCompra());
		sb.append(", claveventa=");
		sb.append(getClaveventa());
		sb.append(", codigoOperacion=");
		sb.append(getCodigoOperacion());
		sb.append(", fechaHoraOperacion=");
		sb.append(getFechaHoraOperacion());
		sb.append(", codigoUnico=");
		sb.append(getCodigoUnico());
		sb.append(", numTarjetaTitular=");
		sb.append(getNumTarjetaTitular());
		sb.append(", numCuenta=");
		sb.append(getNumCuenta());
		sb.append(", fechaAltaTitular=");
		sb.append(getFechaAltaTitular());
		sb.append(", fechaVencTitular=");
		sb.append(getFechaVencTitular());
		sb.append(", numTarjetaProvisional=");
		sb.append(getNumTarjetaProvisional());
		sb.append(", fechaAltaProvisional=");
		sb.append(getFechaAltaProvisional());
		sb.append(", fechaVencProvisional=");
		sb.append(getFechaVencProvisional());
		sb.append(", RazonNoUsoTarjeta=");
		sb.append(getRazonNoUsoTarjeta());
		sb.append(", Motivo=");
		sb.append(getMotivo());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(94);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Producto");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idProducto</column-name><column-value><![CDATA[");
		sb.append(getIdProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoProducto</column-name><column-value><![CDATA[");
		sb.append(getTipoProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoProducto</column-name><column-value><![CDATA[");
		sb.append(getCodigoProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>moneda</column-name><column-value><![CDATA[");
		sb.append(getMoneda());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>marcaProducto</column-name><column-value><![CDATA[");
		sb.append(getMarcaProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lineaCredito</column-name><column-value><![CDATA[");
		sb.append(getLineaCredito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>diaPago</column-name><column-value><![CDATA[");
		sb.append(getDiaPago());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tasaProducto</column-name><column-value><![CDATA[");
		sb.append(getTasaProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreTitularProducto</column-name><column-value><![CDATA[");
		sb.append(getNombreTitularProducto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoSeguro</column-name><column-value><![CDATA[");
		sb.append(getTipoSeguro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>costoSeguro</column-name><column-value><![CDATA[");
		sb.append(getCostoSeguro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagEnvioEECCFisico</column-name><column-value><![CDATA[");
		sb.append(getFlagEnvioEECCFisico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagEnvioEECCEmail</column-name><column-value><![CDATA[");
		sb.append(getFlagEnvioEECCEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagTerminos</column-name><column-value><![CDATA[");
		sb.append(getFlagTerminos());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagCargoCompra</column-name><column-value><![CDATA[");
		sb.append(getFlagCargoCompra());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>claveventa</column-name><column-value><![CDATA[");
		sb.append(getClaveventa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoOperacion</column-name><column-value><![CDATA[");
		sb.append(getCodigoOperacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaHoraOperacion</column-name><column-value><![CDATA[");
		sb.append(getFechaHoraOperacion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoUnico</column-name><column-value><![CDATA[");
		sb.append(getCodigoUnico());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numTarjetaTitular</column-name><column-value><![CDATA[");
		sb.append(getNumTarjetaTitular());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numCuenta</column-name><column-value><![CDATA[");
		sb.append(getNumCuenta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaAltaTitular</column-name><column-value><![CDATA[");
		sb.append(getFechaAltaTitular());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaVencTitular</column-name><column-value><![CDATA[");
		sb.append(getFechaVencTitular());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numTarjetaProvisional</column-name><column-value><![CDATA[");
		sb.append(getNumTarjetaProvisional());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaAltaProvisional</column-name><column-value><![CDATA[");
		sb.append(getFechaAltaProvisional());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaVencProvisional</column-name><column-value><![CDATA[");
		sb.append(getFechaVencProvisional());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>RazonNoUsoTarjeta</column-name><column-value><![CDATA[");
		sb.append(getRazonNoUsoTarjeta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>Motivo</column-name><column-value><![CDATA[");
		sb.append(getMotivo());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = Producto.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			Producto.class
		};
	private long _idProducto;
	private long _originalIdProducto;
	private boolean _setOriginalIdProducto;
	private long _idExpediente;
	private long _originalIdExpediente;
	private boolean _setOriginalIdExpediente;
	private String _tipoProducto;
	private String _codigoProducto;
	private String _moneda;
	private String _marcaProducto;
	private String _lineaCredito;
	private String _diaPago;
	private String _tasaProducto;
	private String _nombreTitularProducto;
	private String _tipoSeguro;
	private String _costoSeguro;
	private Date _fechaRegistro;
	private String _flagEnvioEECCFisico;
	private String _flagEnvioEECCEmail;
	private String _flagTerminos;
	private String _flagCargoCompra;
	private String _claveventa;
	private String _codigoOperacion;
	private Date _fechaHoraOperacion;
	private String _codigoUnico;
	private String _numTarjetaTitular;
	private String _numCuenta;
	private Date _fechaAltaTitular;
	private Date _fechaVencTitular;
	private String _numTarjetaProvisional;
	private Date _fechaAltaProvisional;
	private Date _fechaVencProvisional;
	private String _RazonNoUsoTarjeta;
	private String _Motivo;
	private long _columnBitmask;
	private Producto _escapedModel;
}