/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AuditoriaCliente}.
 * </p>
 *
 * @author Interbank
 * @see AuditoriaCliente
 * @generated
 */
public class AuditoriaClienteWrapper implements AuditoriaCliente,
	ModelWrapper<AuditoriaCliente> {
	public AuditoriaClienteWrapper(AuditoriaCliente auditoriaCliente) {
		_auditoriaCliente = auditoriaCliente;
	}

	@Override
	public Class<?> getModelClass() {
		return AuditoriaCliente.class;
	}

	@Override
	public String getModelClassName() {
		return AuditoriaCliente.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idAuditoriaCliente", getIdAuditoriaCliente());
		attributes.put("idClienteTransaccion", getIdClienteTransaccion());
		attributes.put("paso", getPaso());
		attributes.put("pagina", getPagina());
		attributes.put("tipoFlujo", getTipoFlujo());
		attributes.put("strJson", getStrJson());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idAuditoriaCliente = (Long)attributes.get("idAuditoriaCliente");

		if (idAuditoriaCliente != null) {
			setIdAuditoriaCliente(idAuditoriaCliente);
		}

		Long idClienteTransaccion = (Long)attributes.get("idClienteTransaccion");

		if (idClienteTransaccion != null) {
			setIdClienteTransaccion(idClienteTransaccion);
		}

		String paso = (String)attributes.get("paso");

		if (paso != null) {
			setPaso(paso);
		}

		String pagina = (String)attributes.get("pagina");

		if (pagina != null) {
			setPagina(pagina);
		}

		String tipoFlujo = (String)attributes.get("tipoFlujo");

		if (tipoFlujo != null) {
			setTipoFlujo(tipoFlujo);
		}

		String strJson = (String)attributes.get("strJson");

		if (strJson != null) {
			setStrJson(strJson);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this auditoria cliente.
	*
	* @return the primary key of this auditoria cliente
	*/
	@Override
	public long getPrimaryKey() {
		return _auditoriaCliente.getPrimaryKey();
	}

	/**
	* Sets the primary key of this auditoria cliente.
	*
	* @param primaryKey the primary key of this auditoria cliente
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_auditoriaCliente.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id auditoria cliente of this auditoria cliente.
	*
	* @return the id auditoria cliente of this auditoria cliente
	*/
	@Override
	public long getIdAuditoriaCliente() {
		return _auditoriaCliente.getIdAuditoriaCliente();
	}

	/**
	* Sets the id auditoria cliente of this auditoria cliente.
	*
	* @param idAuditoriaCliente the id auditoria cliente of this auditoria cliente
	*/
	@Override
	public void setIdAuditoriaCliente(long idAuditoriaCliente) {
		_auditoriaCliente.setIdAuditoriaCliente(idAuditoriaCliente);
	}

	/**
	* Returns the id cliente transaccion of this auditoria cliente.
	*
	* @return the id cliente transaccion of this auditoria cliente
	*/
	@Override
	public long getIdClienteTransaccion() {
		return _auditoriaCliente.getIdClienteTransaccion();
	}

	/**
	* Sets the id cliente transaccion of this auditoria cliente.
	*
	* @param idClienteTransaccion the id cliente transaccion of this auditoria cliente
	*/
	@Override
	public void setIdClienteTransaccion(long idClienteTransaccion) {
		_auditoriaCliente.setIdClienteTransaccion(idClienteTransaccion);
	}

	/**
	* Returns the paso of this auditoria cliente.
	*
	* @return the paso of this auditoria cliente
	*/
	@Override
	public java.lang.String getPaso() {
		return _auditoriaCliente.getPaso();
	}

	/**
	* Sets the paso of this auditoria cliente.
	*
	* @param paso the paso of this auditoria cliente
	*/
	@Override
	public void setPaso(java.lang.String paso) {
		_auditoriaCliente.setPaso(paso);
	}

	/**
	* Returns the pagina of this auditoria cliente.
	*
	* @return the pagina of this auditoria cliente
	*/
	@Override
	public java.lang.String getPagina() {
		return _auditoriaCliente.getPagina();
	}

	/**
	* Sets the pagina of this auditoria cliente.
	*
	* @param pagina the pagina of this auditoria cliente
	*/
	@Override
	public void setPagina(java.lang.String pagina) {
		_auditoriaCliente.setPagina(pagina);
	}

	/**
	* Returns the tipo flujo of this auditoria cliente.
	*
	* @return the tipo flujo of this auditoria cliente
	*/
	@Override
	public java.lang.String getTipoFlujo() {
		return _auditoriaCliente.getTipoFlujo();
	}

	/**
	* Sets the tipo flujo of this auditoria cliente.
	*
	* @param tipoFlujo the tipo flujo of this auditoria cliente
	*/
	@Override
	public void setTipoFlujo(java.lang.String tipoFlujo) {
		_auditoriaCliente.setTipoFlujo(tipoFlujo);
	}

	/**
	* Returns the str json of this auditoria cliente.
	*
	* @return the str json of this auditoria cliente
	*/
	@Override
	public java.lang.String getStrJson() {
		return _auditoriaCliente.getStrJson();
	}

	/**
	* Sets the str json of this auditoria cliente.
	*
	* @param strJson the str json of this auditoria cliente
	*/
	@Override
	public void setStrJson(java.lang.String strJson) {
		_auditoriaCliente.setStrJson(strJson);
	}

	/**
	* Returns the fecha registro of this auditoria cliente.
	*
	* @return the fecha registro of this auditoria cliente
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _auditoriaCliente.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this auditoria cliente.
	*
	* @param fechaRegistro the fecha registro of this auditoria cliente
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_auditoriaCliente.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _auditoriaCliente.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_auditoriaCliente.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _auditoriaCliente.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_auditoriaCliente.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _auditoriaCliente.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _auditoriaCliente.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_auditoriaCliente.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _auditoriaCliente.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_auditoriaCliente.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_auditoriaCliente.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_auditoriaCliente.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AuditoriaClienteWrapper((AuditoriaCliente)_auditoriaCliente.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.AuditoriaCliente auditoriaCliente) {
		return _auditoriaCliente.compareTo(auditoriaCliente);
	}

	@Override
	public int hashCode() {
		return _auditoriaCliente.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.AuditoriaCliente> toCacheModel() {
		return _auditoriaCliente.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaCliente toEscapedModel() {
		return new AuditoriaClienteWrapper(_auditoriaCliente.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaCliente toUnescapedModel() {
		return new AuditoriaClienteWrapper(_auditoriaCliente.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _auditoriaCliente.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _auditoriaCliente.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_auditoriaCliente.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditoriaClienteWrapper)) {
			return false;
		}

		AuditoriaClienteWrapper auditoriaClienteWrapper = (AuditoriaClienteWrapper)obj;

		if (Validator.equals(_auditoriaCliente,
					auditoriaClienteWrapper._auditoriaCliente)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public AuditoriaCliente getWrappedAuditoriaCliente() {
		return _auditoriaCliente;
	}

	@Override
	public AuditoriaCliente getWrappedModel() {
		return _auditoriaCliente;
	}

	@Override
	public void resetOriginalValues() {
		_auditoriaCliente.resetOriginalValues();
	}

	private AuditoriaCliente _auditoriaCliente;
}