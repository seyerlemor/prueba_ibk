package pe.com.ibk.pepper.bean;

public class DynamicTextBean {
	
	private String v101001;
	private String v101002;
	private String v101003;
	private String v101004;
	private String v101005;
	private String v101006;
	
	private String v201001;
	private String v201002;
	private String v201003;
	private String v201004;
	
	private String v401001;
	private String v401002;
	private String v401003;
	private String v401004;
	private String v401005;
	
	private String v501001;
	private String v501002;
	private String v501003;
	private String v501004;
	private String v501005;
	private String v501006;
	private String v501007;
	private String v501008;
	private String v501009;
	
	private String v601001;
	private String v601002;
	private String v601003;
	private String v601004;
	private String v601005;
	private String v601006;
	private String v601007;
	private String v601008;
	private String v601009;
	
	public String getV101001() {
		return v101001;
	}
	public void setV101001(String v101001) {
		this.v101001 = v101001;
	}
	public String getV101002() {
		return v101002;
	}
	public void setV101002(String v101002) {
		this.v101002 = v101002;
	}
	public String getV101003() {
		return v101003;
	}
	public void setV101003(String v101003) {
		this.v101003 = v101003;
	}
	public String getV101004() {
		return v101004;
	}
	public void setV101004(String v101004) {
		this.v101004 = v101004;
	}
	public String getV101005() {
		return v101005;
	}
	public void setV101005(String v101005) {
		this.v101005 = v101005;
	}
	public String getV101006() {
		return v101006;
	}
	public void setV101006(String v101006) {
		this.v101006 = v101006;
	}
	public String getV201001() {
		return v201001;
	}
	public void setV201001(String v201001) {
		this.v201001 = v201001;
	}
	public String getV201002() {
		return v201002;
	}
	public void setV201002(String v201002) {
		this.v201002 = v201002;
	}
	public String getV201003() {
		return v201003;
	}
	public void setV201003(String v201003) {
		this.v201003 = v201003;
	}
	public String getV201004() {
		return v201004;
	}
	public void setV201004(String v201004) {
		this.v201004 = v201004;
	}
	public String getV401001() {
		return v401001;
	}
	public void setV401001(String v401001) {
		this.v401001 = v401001;
	}
	public String getV401002() {
		return v401002;
	}
	public void setV401002(String v401002) {
		this.v401002 = v401002;
	}
	public String getV401003() {
		return v401003;
	}
	public void setV401003(String v401003) {
		this.v401003 = v401003;
	}
	public String getV401004() {
		return v401004;
	}
	public void setV401004(String v401004) {
		this.v401004 = v401004;
	}
	public String getV401005() {
		return v401005;
	}
	public void setV401005(String v401005) {
		this.v401005 = v401005;
	}
	public String getV501001() {
		return v501001;
	}
	public void setV501001(String v501001) {
		this.v501001 = v501001;
	}
	public String getV501002() {
		return v501002;
	}
	public void setV501002(String v501002) {
		this.v501002 = v501002;
	}
	public String getV501003() {
		return v501003;
	}
	public void setV501003(String v501003) {
		this.v501003 = v501003;
	}
	public String getV501004() {
		return v501004;
	}
	public void setV501004(String v501004) {
		this.v501004 = v501004;
	}
	public String getV501005() {
		return v501005;
	}
	public void setV501005(String v501005) {
		this.v501005 = v501005;
	}
	public String getV501006() {
		return v501006;
	}
	public void setV501006(String v501006) {
		this.v501006 = v501006;
	}
	public String getV501007() {
		return v501007;
	}
	public void setV501007(String v501007) {
		this.v501007 = v501007;
	}
	public String getV501008() {
		return v501008;
	}
	public void setV501008(String v501008) {
		this.v501008 = v501008;
	}
	public String getV501009() {
		return v501009;
	}
	public void setV501009(String v501009) {
		this.v501009 = v501009;
	}
	public String getV601001() {
		return v601001;
	}
	public void setV601001(String v601001) {
		this.v601001 = v601001;
	}
	public String getV601002() {
		return v601002;
	}
	public void setV601002(String v601002) {
		this.v601002 = v601002;
	}
	public String getV601003() {
		return v601003;
	}
	public void setV601003(String v601003) {
		this.v601003 = v601003;
	}
	public String getV601004() {
		return v601004;
	}
	public void setV601004(String v601004) {
		this.v601004 = v601004;
	}
	public String getV601005() {
		return v601005;
	}
	public void setV601005(String v601005) {
		this.v601005 = v601005;
	}
	public String getV601006() {
		return v601006;
	}
	public void setV601006(String v601006) {
		this.v601006 = v601006;
	}
	public String getV601007() {
		return v601007;
	}
	public void setV601007(String v601007) {
		this.v601007 = v601007;
	}
	public String getV601008() {
		return v601008;
	}
	public void setV601008(String v601008) {
		this.v601008 = v601008;
	}
	public String getV601009() {
		return v601009;
	}
	public void setV601009(String v601009) {
		this.v601009 = v601009;
	}
	@Override
	public String toString() {
		return "DynamicTextBean [v101001=" + v101001 + ", v101002=" + v101002
				+ ", v101003=" + v101003 + ", v101004=" + v101004
				+ ", v101005=" + v101005 + ", v101006=" + v101006
				+ ", v201001=" + v201001 + ", v201002=" + v201002
				+ ", v201003=" + v201003 + ", v201004=" + v201004
				+ ", v401001=" + v401001 + ", v401002=" + v401002
				+ ", v401003=" + v401003 + ", v401004=" + v401004
				+ ", v401005=" + v401005 + ", v501001=" + v501001
				+ ", v501002=" + v501002 + ", v501003=" + v501003
				+ ", v501004=" + v501004 + ", v501005=" + v501005
				+ ", v501006=" + v501006 + ", v501007=" + v501007
				+ ", v501008=" + v501008 + ", v501009=" + v501009
				+ ", v601001=" + v601001 + ", v601002=" + v601002
				+ ", v601003=" + v601003 + ", v601004=" + v601004
				+ ", v601005=" + v601005 + ", v601006=" + v601006
				+ ", v601007=" + v601007 + ", v601008=" + v601008
				+ ", v601009=" + v601009 + "]";
	}
	
}
