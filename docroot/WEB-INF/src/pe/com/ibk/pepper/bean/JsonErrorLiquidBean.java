package pe.com.ibk.pepper.bean;

public class JsonErrorLiquidBean {
	Integer code;
	String message;
	
	
	
	public JsonErrorLiquidBean(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}



	@Override
	public String toString() {
		return "JsonErrorLiquidBean [code=" + code + ", message=" + message
				+ "]";
	}
	
	

}
