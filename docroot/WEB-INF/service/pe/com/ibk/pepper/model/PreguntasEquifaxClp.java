/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.PreguntasEquifaxLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class PreguntasEquifaxClp extends BaseModelImpl<PreguntasEquifax>
	implements PreguntasEquifax {
	public PreguntasEquifaxClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return PreguntasEquifax.class;
	}

	@Override
	public String getModelClassName() {
		return PreguntasEquifax.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idPregunta;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdPregunta(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idPregunta;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idPregunta", getIdPregunta());
		attributes.put("idCabecera", getIdCabecera());
		attributes.put("codigoPregunta", getCodigoPregunta());
		attributes.put("descripPregunta", getDescripPregunta());
		attributes.put("codigoRespuesta", getCodigoRespuesta());
		attributes.put("descripRespuesta", getDescripRespuesta());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idPregunta = (Long)attributes.get("idPregunta");

		if (idPregunta != null) {
			setIdPregunta(idPregunta);
		}

		Long idCabecera = (Long)attributes.get("idCabecera");

		if (idCabecera != null) {
			setIdCabecera(idCabecera);
		}

		String codigoPregunta = (String)attributes.get("codigoPregunta");

		if (codigoPregunta != null) {
			setCodigoPregunta(codigoPregunta);
		}

		String descripPregunta = (String)attributes.get("descripPregunta");

		if (descripPregunta != null) {
			setDescripPregunta(descripPregunta);
		}

		String codigoRespuesta = (String)attributes.get("codigoRespuesta");

		if (codigoRespuesta != null) {
			setCodigoRespuesta(codigoRespuesta);
		}

		String descripRespuesta = (String)attributes.get("descripRespuesta");

		if (descripRespuesta != null) {
			setDescripRespuesta(descripRespuesta);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public long getIdPregunta() {
		return _idPregunta;
	}

	@Override
	public void setIdPregunta(long idPregunta) {
		_idPregunta = idPregunta;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setIdPregunta", long.class);

				method.invoke(_preguntasEquifaxRemoteModel, idPregunta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdCabecera() {
		return _idCabecera;
	}

	@Override
	public void setIdCabecera(long idCabecera) {
		_idCabecera = idCabecera;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setIdCabecera", long.class);

				method.invoke(_preguntasEquifaxRemoteModel, idCabecera);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoPregunta() {
		return _codigoPregunta;
	}

	@Override
	public void setCodigoPregunta(String codigoPregunta) {
		_codigoPregunta = codigoPregunta;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoPregunta",
						String.class);

				method.invoke(_preguntasEquifaxRemoteModel, codigoPregunta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescripPregunta() {
		return _descripPregunta;
	}

	@Override
	public void setDescripPregunta(String descripPregunta) {
		_descripPregunta = descripPregunta;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setDescripPregunta",
						String.class);

				method.invoke(_preguntasEquifaxRemoteModel, descripPregunta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoRespuesta() {
		return _codigoRespuesta;
	}

	@Override
	public void setCodigoRespuesta(String codigoRespuesta) {
		_codigoRespuesta = codigoRespuesta;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoRespuesta",
						String.class);

				method.invoke(_preguntasEquifaxRemoteModel, codigoRespuesta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescripRespuesta() {
		return _descripRespuesta;
	}

	@Override
	public void setDescripRespuesta(String descripRespuesta) {
		_descripRespuesta = descripRespuesta;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setDescripRespuesta",
						String.class);

				method.invoke(_preguntasEquifaxRemoteModel, descripRespuesta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_preguntasEquifaxRemoteModel != null) {
			try {
				Class<?> clazz = _preguntasEquifaxRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_preguntasEquifaxRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPreguntasEquifaxRemoteModel() {
		return _preguntasEquifaxRemoteModel;
	}

	public void setPreguntasEquifaxRemoteModel(
		BaseModel<?> preguntasEquifaxRemoteModel) {
		_preguntasEquifaxRemoteModel = preguntasEquifaxRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _preguntasEquifaxRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_preguntasEquifaxRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PreguntasEquifaxLocalServiceUtil.addPreguntasEquifax(this);
		}
		else {
			PreguntasEquifaxLocalServiceUtil.updatePreguntasEquifax(this);
		}
	}

	@Override
	public PreguntasEquifax toEscapedModel() {
		return (PreguntasEquifax)ProxyUtil.newProxyInstance(PreguntasEquifax.class.getClassLoader(),
			new Class[] { PreguntasEquifax.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PreguntasEquifaxClp clone = new PreguntasEquifaxClp();

		clone.setIdPregunta(getIdPregunta());
		clone.setIdCabecera(getIdCabecera());
		clone.setCodigoPregunta(getCodigoPregunta());
		clone.setDescripPregunta(getDescripPregunta());
		clone.setCodigoRespuesta(getCodigoRespuesta());
		clone.setDescripRespuesta(getDescripRespuesta());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(PreguntasEquifax preguntasEquifax) {
		long primaryKey = preguntasEquifax.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PreguntasEquifaxClp)) {
			return false;
		}

		PreguntasEquifaxClp preguntasEquifax = (PreguntasEquifaxClp)obj;

		long primaryKey = preguntasEquifax.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{idPregunta=");
		sb.append(getIdPregunta());
		sb.append(", idCabecera=");
		sb.append(getIdCabecera());
		sb.append(", codigoPregunta=");
		sb.append(getCodigoPregunta());
		sb.append(", descripPregunta=");
		sb.append(getDescripPregunta());
		sb.append(", codigoRespuesta=");
		sb.append(getCodigoRespuesta());
		sb.append(", descripRespuesta=");
		sb.append(getDescripRespuesta());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.PreguntasEquifax");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idPregunta</column-name><column-value><![CDATA[");
		sb.append(getIdPregunta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idCabecera</column-name><column-value><![CDATA[");
		sb.append(getIdCabecera());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoPregunta</column-name><column-value><![CDATA[");
		sb.append(getCodigoPregunta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripPregunta</column-name><column-value><![CDATA[");
		sb.append(getDescripPregunta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoRespuesta</column-name><column-value><![CDATA[");
		sb.append(getCodigoRespuesta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripRespuesta</column-name><column-value><![CDATA[");
		sb.append(getDescripRespuesta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idPregunta;
	private long _idCabecera;
	private String _codigoPregunta;
	private String _descripPregunta;
	private String _codigoRespuesta;
	private String _descripRespuesta;
	private Date _fechaRegistro;
	private BaseModel<?> _preguntasEquifaxRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}