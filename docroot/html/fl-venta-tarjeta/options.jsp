<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />


<div class="lh-ff lh-ff__pauta lh-ff__pauta1">
	<div class="lq-container">
		<div class="lh-ff-block lh-ff-block--pauta lh-ff-block--active">
			<div class="lq-row lq-clearfix">
				<style type="text/css">
					.lh-ff__pauta--img::before {
						background: -webkit-linear-gradient(-45deg, #33b768 0%, #2aa85b 100%);
						background: -o-linear-gradient(-45deg, #33b768 0%, #2aa85b 100%);
						background: linear-gradient(135deg, #33b768 0%, #2aa85b 100%);
					}
				</style>
				<div class="lh-ff__pauta__col1 lh-ff__pauta--img">
					<div class="lh-ff__pauta__container lh-flex--md">
						<div class="lh-ff__pauta__content">
							<div
								class="lh-ff__pauta__group lh-ff__pauta__group--leftcs lh-ff__pauta__group--full-width-xs">
								<div
									class="lh-ff__pauta__title lh-typo__maintitle lh-typo__maintitle--2 lh-ff__pauta__title--next-description">¡Hola
									<%= themeDisplay.getUser().getFirstName() %>!</div>
								<div class="lh-ff__pauta__title lh-typo__maintitle lh-typo__maintitle--3 lh-ff__pauta__title--next-description">
									<c:out value="${requestScope.tradeName}"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="lh-ff__pauta__col2">
					<div class="lh-ff__pauta__col2-after"></div>
					<div class="lh-ff__pauta_content">
						<form class="lh-ff-form"
							action="#" novalidate>
							<div class="lh-ff-section">
								<h4
									class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">Selecciona
									la opción deseada</h4>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item">
										<a href="/solicitar/tarjeta/credito/inicio${requestScope.tradeUrl}"
											class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block">
											<span>Nuevo Cliente</span>
										</a>
									</div>
								</div>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item">
										<a href="/solicitar/tarjeta/credito/recompra${requestScope.tradeUrl}"
											class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block">
											<span>Código de re-compra</span>
										</a>
									</div>
								</div>
								<div class="lh-ff-group">
									<div class="lh-ff-group__item">
										<a href="/solicitar/tarjeta/credito/extorno${requestScope.tradeUrl}"
											class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block">
											<span>Extorno de productos</span>
										</a>
									</div>
								</div>
								<div class="lq-row">
									<div class="lh-ff-info-col">
										<span class="lh-typo__obs"><a href="#">FAQ /
												Noticias Importantes</a></span>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

