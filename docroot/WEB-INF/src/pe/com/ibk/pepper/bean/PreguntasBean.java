package pe.com.ibk.pepper.bean;

import java.math.BigInteger;
import java.util.List;

import pe.com.ibk.pepper.webservices.bean.OpcionesBean;

public class PreguntasBean {
	
	
	private String categoriaPregunta;
	private BigInteger numeroPregunta;
	private String descripcionPregunta;
	private List<OpcionesBean> opciones;
	private BigInteger numeroOpcion;
		
	public BigInteger getNumeroOpcion() {
		return numeroOpcion;
	}
	public void setNumeroOpcion(BigInteger numeroOpcion) {
		this.numeroOpcion = numeroOpcion;
	}
	public String getCategoriaPregunta() {
		return categoriaPregunta;
	}
	public void setCategoriaPregunta(String categoriaPregunta) {
		this.categoriaPregunta = categoriaPregunta;
	}
	public BigInteger getNumeroPregunta() {
		return numeroPregunta;
	}
	public void setNumeroPregunta(BigInteger numeroPregunta) {
		this.numeroPregunta = numeroPregunta;
	}
	public String getDescripcionPregunta() {
		return descripcionPregunta;
	}
	public void setDescripcionPregunta(String descripcionPregunta) {
		this.descripcionPregunta = descripcionPregunta;
	}
	public List<OpcionesBean> getOpciones() {
		return opciones;
	}
	public void setOpciones(List<OpcionesBean> opciones) {
		this.opciones = opciones;
	}
	@Override
	public String toString() {
		return "PreguntasBean [categoriaPregunta=" + categoriaPregunta
				+ ", numeroPregunta=" + numeroPregunta
				+ ", descripcionPregunta=" + descripcionPregunta
				+ ", opciones=" + opciones + ", numeroOpcion=" + numeroOpcion
				+ "]";
	}

	
}
