package pe.com.ibk.pepper.bean;

import java.util.ArrayList;
import java.util.List;

public class OfertasBean {
	
	private List<OfertaBean> ofertaBean = new ArrayList<>();

	public List<OfertaBean> getOfertaBean() {
		return ofertaBean;
	}

	public void setOfertaBean(List<OfertaBean> ofertaBean) {
		this.ofertaBean = ofertaBean;
	}

	@Override
	public String toString() {
		return "OfertasBean [ofertaBean=" + ofertaBean + "]";
	}
	
	
}
