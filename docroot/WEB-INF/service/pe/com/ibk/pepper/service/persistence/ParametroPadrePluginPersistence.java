/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.ParametroPadrePlugin;

/**
 * The persistence interface for the parametro padre plugin service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ParametroPadrePluginPersistenceImpl
 * @see ParametroPadrePluginUtil
 * @generated
 */
public interface ParametroPadrePluginPersistence extends BasePersistence<ParametroPadrePlugin> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ParametroPadrePluginUtil} to access the parametro padre plugin persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the parametro padre plugin where codigoPadre = &#63; and estado = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePluginException} if it could not be found.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByC_G_E(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the parametro padre plugin where codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByC_G_E(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the parametro padre plugin where codigoPadre = &#63; and estado = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByC_G_E(
		java.lang.String codigoPadre, boolean estado, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the parametro padre plugin where codigoPadre = &#63; and estado = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the parametro padre plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin removeByC_G_E(
		java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the number of parametro padre plugins where codigoPadre = &#63; and estado = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @param estado the estado
	* @return the number of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public int countByC_G_E(java.lang.String codigoPadre, boolean estado)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the parametro padre plugin where codigoPadre = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePluginException} if it could not be found.
	*
	* @param codigoPadre the codigo padre
	* @return the matching parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByC_G(
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the parametro padre plugin where codigoPadre = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @return the matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByC_G(
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the parametro padre plugin where codigoPadre = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param codigoPadre the codigo padre
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByC_G(
		java.lang.String codigoPadre, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the parametro padre plugin where codigoPadre = &#63; from the database.
	*
	* @param codigoPadre the codigo padre
	* @return the parametro padre plugin that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin removeByC_G(
		java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the number of parametro padre plugins where codigoPadre = &#63;.
	*
	* @param codigoPadre the codigo padre
	* @return the number of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public int countByC_G(java.lang.String codigoPadre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the parametro padre plugins where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @return the matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findByN_G(
		java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the parametro padre plugins where nombre LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @return the range of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findByN_G(
		java.lang.String nombre, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the parametro padre plugins where nombre LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findByN_G(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first parametro padre plugin in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByN_G_First(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the first parametro padre plugin in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByN_G_First(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last parametro padre plugin in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByN_G_Last(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the last parametro padre plugin in the ordered set where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByN_G_Last(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the parametro padre plugins before and after the current parametro padre plugin in the ordered set where nombre LIKE &#63;.
	*
	* @param idParametroPadre the primary key of the current parametro padre plugin
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin[] findByN_G_PrevAndNext(
		long idParametroPadre, java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Removes all the parametro padre plugins where nombre LIKE &#63; from the database.
	*
	* @param nombre the nombre
	* @throws SystemException if a system exception occurred
	*/
	public void removeByN_G(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of parametro padre plugins where nombre LIKE &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public int countByN_G(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the parametro padre plugins where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the parametro padre plugins where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @return the range of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the parametro padre plugins where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first parametro padre plugin in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the first parametro padre plugin in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last parametro padre plugin in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the last parametro padre plugin in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching parametro padre plugin, or <code>null</code> if a matching parametro padre plugin could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the parametro padre plugins before and after the current parametro padre plugin in the ordered set where groupId = &#63;.
	*
	* @param idParametroPadre the primary key of the current parametro padre plugin
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin[] findByGroupId_PrevAndNext(
		long idParametroPadre, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Removes all the parametro padre plugins where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of parametro padre plugins where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the parametro padre plugin in the entity cache if it is enabled.
	*
	* @param parametroPadrePlugin the parametro padre plugin
	*/
	public void cacheResult(
		pe.com.ibk.pepper.model.ParametroPadrePlugin parametroPadrePlugin);

	/**
	* Caches the parametro padre plugins in the entity cache if it is enabled.
	*
	* @param parametroPadrePlugins the parametro padre plugins
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> parametroPadrePlugins);

	/**
	* Creates a new parametro padre plugin with the primary key. Does not add the parametro padre plugin to the database.
	*
	* @param idParametroPadre the primary key for the new parametro padre plugin
	* @return the new parametro padre plugin
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin create(
		long idParametroPadre);

	/**
	* Removes the parametro padre plugin with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idParametroPadre the primary key of the parametro padre plugin
	* @return the parametro padre plugin that was removed
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin remove(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	public pe.com.ibk.pepper.model.ParametroPadrePlugin updateImpl(
		pe.com.ibk.pepper.model.ParametroPadrePlugin parametroPadrePlugin)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the parametro padre plugin with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchParametroPadrePluginException} if it could not be found.
	*
	* @param idParametroPadre the primary key of the parametro padre plugin
	* @return the parametro padre plugin
	* @throws pe.com.ibk.pepper.NoSuchParametroPadrePluginException if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin findByPrimaryKey(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchParametroPadrePluginException;

	/**
	* Returns the parametro padre plugin with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idParametroPadre the primary key of the parametro padre plugin
	* @return the parametro padre plugin, or <code>null</code> if a parametro padre plugin with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.ParametroPadrePlugin fetchByPrimaryKey(
		long idParametroPadre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the parametro padre plugins.
	*
	* @return the parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the parametro padre plugins.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @return the range of parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the parametro padre plugins.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ParametroPadrePluginModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of parametro padre plugins
	* @param end the upper bound of the range of parametro padre plugins (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.ParametroPadrePlugin> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the parametro padre plugins from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of parametro padre plugins.
	*
	* @return the number of parametro padre plugins
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}