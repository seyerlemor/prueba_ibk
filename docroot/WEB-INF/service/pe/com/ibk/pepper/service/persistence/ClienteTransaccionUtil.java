/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.ClienteTransaccion;

import java.util.List;

/**
 * The persistence utility for the cliente transaccion service. This utility wraps {@link ClienteTransaccionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ClienteTransaccionPersistence
 * @see ClienteTransaccionPersistenceImpl
 * @generated
 */
public class ClienteTransaccionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ClienteTransaccion clienteTransaccion) {
		getPersistence().clearCache(clienteTransaccion);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ClienteTransaccion> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ClienteTransaccion> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ClienteTransaccion> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ClienteTransaccion update(
		ClienteTransaccion clienteTransaccion) throws SystemException {
		return getPersistence().update(clienteTransaccion);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ClienteTransaccion update(
		ClienteTransaccion clienteTransaccion, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(clienteTransaccion, serviceContext);
	}

	/**
	* Returns the cliente transaccion where idClienteTransaccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchClienteTransaccionException} if it could not be found.
	*
	* @param idClienteTransaccion the id cliente transaccion
	* @return the matching cliente transaccion
	* @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion findByCT_ID(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence().findByCT_ID(idClienteTransaccion);
	}

	/**
	* Returns the cliente transaccion where idClienteTransaccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idClienteTransaccion the id cliente transaccion
	* @return the matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion fetchByCT_ID(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCT_ID(idClienteTransaccion);
	}

	/**
	* Returns the cliente transaccion where idClienteTransaccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idClienteTransaccion the id cliente transaccion
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion fetchByCT_ID(
		long idClienteTransaccion, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCT_ID(idClienteTransaccion, retrieveFromCache);
	}

	/**
	* Removes the cliente transaccion where idClienteTransaccion = &#63; from the database.
	*
	* @param idClienteTransaccion the id cliente transaccion
	* @return the cliente transaccion that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion removeByCT_ID(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence().removeByCT_ID(idClienteTransaccion);
	}

	/**
	* Returns the number of cliente transaccions where idClienteTransaccion = &#63;.
	*
	* @param idClienteTransaccion the id cliente transaccion
	* @return the number of matching cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCT_ID(long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCT_ID(idClienteTransaccion);
	}

	/**
	* Returns all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @return the matching cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> findByIdExpediente(
		long idExpediente, java.lang.String tipoFlujo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdExpediente(idExpediente, tipoFlujo);
	}

	/**
	* Returns a range of all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param start the lower bound of the range of cliente transaccions
	* @param end the upper bound of the range of cliente transaccions (not inclusive)
	* @return the range of matching cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> findByIdExpediente(
		long idExpediente, java.lang.String tipoFlujo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdExpediente(idExpediente, tipoFlujo, start, end);
	}

	/**
	* Returns an ordered range of all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param start the lower bound of the range of cliente transaccions
	* @param end the upper bound of the range of cliente transaccions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> findByIdExpediente(
		long idExpediente, java.lang.String tipoFlujo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdExpediente(idExpediente, tipoFlujo, start, end,
			orderByComparator);
	}

	/**
	* Returns the first cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching cliente transaccion
	* @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion findByIdExpediente_First(
		long idExpediente, java.lang.String tipoFlujo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence()
				   .findByIdExpediente_First(idExpediente, tipoFlujo,
			orderByComparator);
	}

	/**
	* Returns the first cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion fetchByIdExpediente_First(
		long idExpediente, java.lang.String tipoFlujo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdExpediente_First(idExpediente, tipoFlujo,
			orderByComparator);
	}

	/**
	* Returns the last cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching cliente transaccion
	* @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion findByIdExpediente_Last(
		long idExpediente, java.lang.String tipoFlujo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence()
				   .findByIdExpediente_Last(idExpediente, tipoFlujo,
			orderByComparator);
	}

	/**
	* Returns the last cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching cliente transaccion, or <code>null</code> if a matching cliente transaccion could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion fetchByIdExpediente_Last(
		long idExpediente, java.lang.String tipoFlujo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdExpediente_Last(idExpediente, tipoFlujo,
			orderByComparator);
	}

	/**
	* Returns the cliente transaccions before and after the current cliente transaccion in the ordered set where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idClienteTransaccion the primary key of the current cliente transaccion
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next cliente transaccion
	* @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion[] findByIdExpediente_PrevAndNext(
		long idClienteTransaccion, long idExpediente,
		java.lang.String tipoFlujo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence()
				   .findByIdExpediente_PrevAndNext(idClienteTransaccion,
			idExpediente, tipoFlujo, orderByComparator);
	}

	/**
	* Removes all the cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdExpediente(long idExpediente,
		java.lang.String tipoFlujo)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdExpediente(idExpediente, tipoFlujo);
	}

	/**
	* Returns the number of cliente transaccions where idExpediente = &#63; and tipoFlujo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipoFlujo the tipo flujo
	* @return the number of matching cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdExpediente(long idExpediente,
		java.lang.String tipoFlujo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdExpediente(idExpediente, tipoFlujo);
	}

	/**
	* Caches the cliente transaccion in the entity cache if it is enabled.
	*
	* @param clienteTransaccion the cliente transaccion
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion) {
		getPersistence().cacheResult(clienteTransaccion);
	}

	/**
	* Caches the cliente transaccions in the entity cache if it is enabled.
	*
	* @param clienteTransaccions the cliente transaccions
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> clienteTransaccions) {
		getPersistence().cacheResult(clienteTransaccions);
	}

	/**
	* Creates a new cliente transaccion with the primary key. Does not add the cliente transaccion to the database.
	*
	* @param idClienteTransaccion the primary key for the new cliente transaccion
	* @return the new cliente transaccion
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion create(
		long idClienteTransaccion) {
		return getPersistence().create(idClienteTransaccion);
	}

	/**
	* Removes the cliente transaccion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idClienteTransaccion the primary key of the cliente transaccion
	* @return the cliente transaccion that was removed
	* @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion remove(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence().remove(idClienteTransaccion);
	}

	public static pe.com.ibk.pepper.model.ClienteTransaccion updateImpl(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clienteTransaccion);
	}

	/**
	* Returns the cliente transaccion with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchClienteTransaccionException} if it could not be found.
	*
	* @param idClienteTransaccion the primary key of the cliente transaccion
	* @return the cliente transaccion
	* @throws pe.com.ibk.pepper.NoSuchClienteTransaccionException if a cliente transaccion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion findByPrimaryKey(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchClienteTransaccionException {
		return getPersistence().findByPrimaryKey(idClienteTransaccion);
	}

	/**
	* Returns the cliente transaccion with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idClienteTransaccion the primary key of the cliente transaccion
	* @return the cliente transaccion, or <code>null</code> if a cliente transaccion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.ClienteTransaccion fetchByPrimaryKey(
		long idClienteTransaccion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idClienteTransaccion);
	}

	/**
	* Returns all the cliente transaccions.
	*
	* @return the cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the cliente transaccions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of cliente transaccions
	* @param end the upper bound of the range of cliente transaccions (not inclusive)
	* @return the range of cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the cliente transaccions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ClienteTransaccionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of cliente transaccions
	* @param end the upper bound of the range of cliente transaccions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.ClienteTransaccion> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the cliente transaccions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of cliente transaccions.
	*
	* @return the number of cliente transaccions
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ClienteTransaccionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ClienteTransaccionPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ClienteTransaccionPersistence.class.getName());

			ReferenceRegistry.registerReference(ClienteTransaccionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ClienteTransaccionPersistence persistence) {
	}

	private static ClienteTransaccionPersistence _persistence;
}