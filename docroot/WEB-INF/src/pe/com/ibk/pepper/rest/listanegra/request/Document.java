package pe.com.ibk.pepper.rest.listanegra.request;

public class Document {
	private String type;
	private String documentId;

	public Document() {
		super();
	}
	
	public Document(String type, String documentId) {
		super();
		this.type = type;
		this.documentId = documentId;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	
}
