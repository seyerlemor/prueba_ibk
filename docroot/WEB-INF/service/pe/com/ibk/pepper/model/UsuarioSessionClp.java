/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.UsuarioSessionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class UsuarioSessionClp extends BaseModelImpl<UsuarioSession>
	implements UsuarioSession {
	public UsuarioSessionClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UsuarioSession.class;
	}

	@Override
	public String getModelClassName() {
		return UsuarioSession.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idUsuarioSession;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdUsuarioSession(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idUsuarioSession;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("idSession", getIdSession());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("tienda", getTienda());
		attributes.put("estado", getEstado());
		attributes.put("userId", getUserId());
		attributes.put("tiendaId", getTiendaId());
		attributes.put("establecimiento", getEstablecimiento());
		attributes.put("nombreVendedor", getNombreVendedor());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idUsuarioSession = (Long)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		String idSession = (String)attributes.get("idSession");

		if (idSession != null) {
			setIdSession(idSession);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String tienda = (String)attributes.get("tienda");

		if (tienda != null) {
			setTienda(tienda);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long tiendaId = (Long)attributes.get("tiendaId");

		if (tiendaId != null) {
			setTiendaId(tiendaId);
		}

		String establecimiento = (String)attributes.get("establecimiento");

		if (establecimiento != null) {
			setEstablecimiento(establecimiento);
		}

		String nombreVendedor = (String)attributes.get("nombreVendedor");

		if (nombreVendedor != null) {
			setNombreVendedor(nombreVendedor);
		}
	}

	@Override
	public long getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	@Override
	public void setIdUsuarioSession(long idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdUsuarioSession",
						long.class);

				method.invoke(_usuarioSessionRemoteModel, idUsuarioSession);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdSession() {
		return _idSession;
	}

	@Override
	public void setIdSession(String idSession) {
		_idSession = idSession;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdSession", String.class);

				method.invoke(_usuarioSessionRemoteModel, idSession);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_usuarioSessionRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTienda() {
		return _tienda;
	}

	@Override
	public void setTienda(String tienda) {
		_tienda = tienda;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setTienda", String.class);

				method.invoke(_usuarioSessionRemoteModel, tienda);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstado() {
		return _estado;
	}

	@Override
	public void setEstado(String estado) {
		_estado = estado;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", String.class);

				method.invoke(_usuarioSessionRemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_usuarioSessionRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public long getTiendaId() {
		return _tiendaId;
	}

	@Override
	public void setTiendaId(long tiendaId) {
		_tiendaId = tiendaId;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setTiendaId", long.class);

				method.invoke(_usuarioSessionRemoteModel, tiendaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstablecimiento() {
		return _establecimiento;
	}

	@Override
	public void setEstablecimiento(String establecimiento) {
		_establecimiento = establecimiento;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setEstablecimiento",
						String.class);

				method.invoke(_usuarioSessionRemoteModel, establecimiento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombreVendedor() {
		return _nombreVendedor;
	}

	@Override
	public void setNombreVendedor(String nombreVendedor) {
		_nombreVendedor = nombreVendedor;

		if (_usuarioSessionRemoteModel != null) {
			try {
				Class<?> clazz = _usuarioSessionRemoteModel.getClass();

				Method method = clazz.getMethod("setNombreVendedor",
						String.class);

				method.invoke(_usuarioSessionRemoteModel, nombreVendedor);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUsuarioSessionRemoteModel() {
		return _usuarioSessionRemoteModel;
	}

	public void setUsuarioSessionRemoteModel(
		BaseModel<?> usuarioSessionRemoteModel) {
		_usuarioSessionRemoteModel = usuarioSessionRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _usuarioSessionRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_usuarioSessionRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UsuarioSessionLocalServiceUtil.addUsuarioSession(this);
		}
		else {
			UsuarioSessionLocalServiceUtil.updateUsuarioSession(this);
		}
	}

	@Override
	public UsuarioSession toEscapedModel() {
		return (UsuarioSession)ProxyUtil.newProxyInstance(UsuarioSession.class.getClassLoader(),
			new Class[] { UsuarioSession.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UsuarioSessionClp clone = new UsuarioSessionClp();

		clone.setIdUsuarioSession(getIdUsuarioSession());
		clone.setIdSession(getIdSession());
		clone.setFechaRegistro(getFechaRegistro());
		clone.setTienda(getTienda());
		clone.setEstado(getEstado());
		clone.setUserId(getUserId());
		clone.setTiendaId(getTiendaId());
		clone.setEstablecimiento(getEstablecimiento());
		clone.setNombreVendedor(getNombreVendedor());

		return clone;
	}

	@Override
	public int compareTo(UsuarioSession usuarioSession) {
		long primaryKey = usuarioSession.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UsuarioSessionClp)) {
			return false;
		}

		UsuarioSessionClp usuarioSession = (UsuarioSessionClp)obj;

		long primaryKey = usuarioSession.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{idUsuarioSession=");
		sb.append(getIdUsuarioSession());
		sb.append(", idSession=");
		sb.append(getIdSession());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append(", tienda=");
		sb.append(getTienda());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", tiendaId=");
		sb.append(getTiendaId());
		sb.append(", establecimiento=");
		sb.append(getEstablecimiento());
		sb.append(", nombreVendedor=");
		sb.append(getNombreVendedor());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.UsuarioSession");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idUsuarioSession</column-name><column-value><![CDATA[");
		sb.append(getIdUsuarioSession());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idSession</column-name><column-value><![CDATA[");
		sb.append(getIdSession());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tienda</column-name><column-value><![CDATA[");
		sb.append(getTienda());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tiendaId</column-name><column-value><![CDATA[");
		sb.append(getTiendaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>establecimiento</column-name><column-value><![CDATA[");
		sb.append(getEstablecimiento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombreVendedor</column-name><column-value><![CDATA[");
		sb.append(getNombreVendedor());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idUsuarioSession;
	private String _idSession;
	private Date _fechaRegistro;
	private String _tienda;
	private String _estado;
	private long _userId;
	private String _userUuid;
	private long _tiendaId;
	private String _establecimiento;
	private String _nombreVendedor;
	private BaseModel<?> _usuarioSessionRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}