/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class ProductoSoap implements Serializable {
	public static ProductoSoap toSoapModel(Producto model) {
		ProductoSoap soapModel = new ProductoSoap();

		soapModel.setIdProducto(model.getIdProducto());
		soapModel.setIdExpediente(model.getIdExpediente());
		soapModel.setTipoProducto(model.getTipoProducto());
		soapModel.setCodigoProducto(model.getCodigoProducto());
		soapModel.setMoneda(model.getMoneda());
		soapModel.setMarcaProducto(model.getMarcaProducto());
		soapModel.setLineaCredito(model.getLineaCredito());
		soapModel.setDiaPago(model.getDiaPago());
		soapModel.setTasaProducto(model.getTasaProducto());
		soapModel.setNombreTitularProducto(model.getNombreTitularProducto());
		soapModel.setTipoSeguro(model.getTipoSeguro());
		soapModel.setCostoSeguro(model.getCostoSeguro());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setFlagEnvioEECCFisico(model.getFlagEnvioEECCFisico());
		soapModel.setFlagEnvioEECCEmail(model.getFlagEnvioEECCEmail());
		soapModel.setFlagTerminos(model.getFlagTerminos());
		soapModel.setFlagCargoCompra(model.getFlagCargoCompra());
		soapModel.setClaveventa(model.getClaveventa());
		soapModel.setCodigoOperacion(model.getCodigoOperacion());
		soapModel.setFechaHoraOperacion(model.getFechaHoraOperacion());
		soapModel.setCodigoUnico(model.getCodigoUnico());
		soapModel.setNumTarjetaTitular(model.getNumTarjetaTitular());
		soapModel.setNumCuenta(model.getNumCuenta());
		soapModel.setFechaAltaTitular(model.getFechaAltaTitular());
		soapModel.setFechaVencTitular(model.getFechaVencTitular());
		soapModel.setNumTarjetaProvisional(model.getNumTarjetaProvisional());
		soapModel.setFechaAltaProvisional(model.getFechaAltaProvisional());
		soapModel.setFechaVencProvisional(model.getFechaVencProvisional());
		soapModel.setRazonNoUsoTarjeta(model.getRazonNoUsoTarjeta());
		soapModel.setMotivo(model.getMotivo());

		return soapModel;
	}

	public static ProductoSoap[] toSoapModels(Producto[] models) {
		ProductoSoap[] soapModels = new ProductoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProductoSoap[][] toSoapModels(Producto[][] models) {
		ProductoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProductoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProductoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProductoSoap[] toSoapModels(List<Producto> models) {
		List<ProductoSoap> soapModels = new ArrayList<ProductoSoap>(models.size());

		for (Producto model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProductoSoap[soapModels.size()]);
	}

	public ProductoSoap() {
	}

	public long getPrimaryKey() {
		return _idProducto;
	}

	public void setPrimaryKey(long pk) {
		setIdProducto(pk);
	}

	public long getIdProducto() {
		return _idProducto;
	}

	public void setIdProducto(long idProducto) {
		_idProducto = idProducto;
	}

	public long getIdExpediente() {
		return _idExpediente;
	}

	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;
	}

	public String getTipoProducto() {
		return _tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		_tipoProducto = tipoProducto;
	}

	public String getCodigoProducto() {
		return _codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		_codigoProducto = codigoProducto;
	}

	public String getMoneda() {
		return _moneda;
	}

	public void setMoneda(String moneda) {
		_moneda = moneda;
	}

	public String getMarcaProducto() {
		return _marcaProducto;
	}

	public void setMarcaProducto(String marcaProducto) {
		_marcaProducto = marcaProducto;
	}

	public String getLineaCredito() {
		return _lineaCredito;
	}

	public void setLineaCredito(String lineaCredito) {
		_lineaCredito = lineaCredito;
	}

	public String getDiaPago() {
		return _diaPago;
	}

	public void setDiaPago(String diaPago) {
		_diaPago = diaPago;
	}

	public String getTasaProducto() {
		return _tasaProducto;
	}

	public void setTasaProducto(String tasaProducto) {
		_tasaProducto = tasaProducto;
	}

	public String getNombreTitularProducto() {
		return _nombreTitularProducto;
	}

	public void setNombreTitularProducto(String nombreTitularProducto) {
		_nombreTitularProducto = nombreTitularProducto;
	}

	public String getTipoSeguro() {
		return _tipoSeguro;
	}

	public void setTipoSeguro(String tipoSeguro) {
		_tipoSeguro = tipoSeguro;
	}

	public String getCostoSeguro() {
		return _costoSeguro;
	}

	public void setCostoSeguro(String costoSeguro) {
		_costoSeguro = costoSeguro;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getFlagEnvioEECCFisico() {
		return _flagEnvioEECCFisico;
	}

	public void setFlagEnvioEECCFisico(String flagEnvioEECCFisico) {
		_flagEnvioEECCFisico = flagEnvioEECCFisico;
	}

	public String getFlagEnvioEECCEmail() {
		return _flagEnvioEECCEmail;
	}

	public void setFlagEnvioEECCEmail(String flagEnvioEECCEmail) {
		_flagEnvioEECCEmail = flagEnvioEECCEmail;
	}

	public String getFlagTerminos() {
		return _flagTerminos;
	}

	public void setFlagTerminos(String flagTerminos) {
		_flagTerminos = flagTerminos;
	}

	public String getFlagCargoCompra() {
		return _flagCargoCompra;
	}

	public void setFlagCargoCompra(String flagCargoCompra) {
		_flagCargoCompra = flagCargoCompra;
	}

	public String getClaveventa() {
		return _claveventa;
	}

	public void setClaveventa(String claveventa) {
		_claveventa = claveventa;
	}

	public String getCodigoOperacion() {
		return _codigoOperacion;
	}

	public void setCodigoOperacion(String codigoOperacion) {
		_codigoOperacion = codigoOperacion;
	}

	public Date getFechaHoraOperacion() {
		return _fechaHoraOperacion;
	}

	public void setFechaHoraOperacion(Date fechaHoraOperacion) {
		_fechaHoraOperacion = fechaHoraOperacion;
	}

	public String getCodigoUnico() {
		return _codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {
		_codigoUnico = codigoUnico;
	}

	public String getNumTarjetaTitular() {
		return _numTarjetaTitular;
	}

	public void setNumTarjetaTitular(String numTarjetaTitular) {
		_numTarjetaTitular = numTarjetaTitular;
	}

	public String getNumCuenta() {
		return _numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		_numCuenta = numCuenta;
	}

	public Date getFechaAltaTitular() {
		return _fechaAltaTitular;
	}

	public void setFechaAltaTitular(Date fechaAltaTitular) {
		_fechaAltaTitular = fechaAltaTitular;
	}

	public Date getFechaVencTitular() {
		return _fechaVencTitular;
	}

	public void setFechaVencTitular(Date fechaVencTitular) {
		_fechaVencTitular = fechaVencTitular;
	}

	public String getNumTarjetaProvisional() {
		return _numTarjetaProvisional;
	}

	public void setNumTarjetaProvisional(String numTarjetaProvisional) {
		_numTarjetaProvisional = numTarjetaProvisional;
	}

	public Date getFechaAltaProvisional() {
		return _fechaAltaProvisional;
	}

	public void setFechaAltaProvisional(Date fechaAltaProvisional) {
		_fechaAltaProvisional = fechaAltaProvisional;
	}

	public Date getFechaVencProvisional() {
		return _fechaVencProvisional;
	}

	public void setFechaVencProvisional(Date fechaVencProvisional) {
		_fechaVencProvisional = fechaVencProvisional;
	}

	public String getRazonNoUsoTarjeta() {
		return _RazonNoUsoTarjeta;
	}

	public void setRazonNoUsoTarjeta(String RazonNoUsoTarjeta) {
		_RazonNoUsoTarjeta = RazonNoUsoTarjeta;
	}

	public String getMotivo() {
		return _Motivo;
	}

	public void setMotivo(String Motivo) {
		_Motivo = Motivo;
	}

	private long _idProducto;
	private long _idExpediente;
	private String _tipoProducto;
	private String _codigoProducto;
	private String _moneda;
	private String _marcaProducto;
	private String _lineaCredito;
	private String _diaPago;
	private String _tasaProducto;
	private String _nombreTitularProducto;
	private String _tipoSeguro;
	private String _costoSeguro;
	private Date _fechaRegistro;
	private String _flagEnvioEECCFisico;
	private String _flagEnvioEECCEmail;
	private String _flagTerminos;
	private String _flagCargoCompra;
	private String _claveventa;
	private String _codigoOperacion;
	private Date _fechaHoraOperacion;
	private String _codigoUnico;
	private String _numTarjetaTitular;
	private String _numCuenta;
	private Date _fechaAltaTitular;
	private Date _fechaVencTitular;
	private String _numTarjetaProvisional;
	private Date _fechaAltaProvisional;
	private Date _fechaVencProvisional;
	private String _RazonNoUsoTarjeta;
	private String _Motivo;
}