package pe.com.ibk.pepper.rest.potc.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VinculacionProducto {
	
    private String codigoEmisorProducto;
    private String tipoProducto;
    private String numeroCuenta;
    private String numeroTarjeta;
    private String vencimientoTarjeta;
    private String nombreUsuarioProducto;
    private String aliasProducto;
    private String idTerminal;
    private String numeroTransaccion;
    private String marcaProducto;
    private String nombreProducto;
    private String detalleProducto;
    private String nombreEmisorProducto;
    private String actualizarProducto;
    private String listarProductos;
    
    @JsonCreator
	public VinculacionProducto(String codigoEmisorProducto,
			String tipoProducto, String numeroCuenta, String numeroTarjeta,
			String vencimientoTarjeta, String nombreUsuarioProducto,
			String aliasProducto, String idTerminal, String numeroTransaccion,
			String marcaProducto, String nombreProducto,
			String detalleProducto, String nombreEmisorProducto,
			String actualizarProducto, String listarProductos) {
		super();
		this.codigoEmisorProducto = codigoEmisorProducto;
		this.tipoProducto = tipoProducto;
		this.numeroCuenta = numeroCuenta;
		this.numeroTarjeta = numeroTarjeta;
		this.vencimientoTarjeta = vencimientoTarjeta;
		this.nombreUsuarioProducto = nombreUsuarioProducto;
		this.aliasProducto = aliasProducto;
		this.idTerminal = idTerminal;
		this.numeroTransaccion = numeroTransaccion;
		this.marcaProducto = marcaProducto;
		this.nombreProducto = nombreProducto;
		this.detalleProducto = detalleProducto;
		this.nombreEmisorProducto = nombreEmisorProducto;
		this.actualizarProducto = actualizarProducto;
		this.listarProductos = listarProductos;
	}

    @JsonProperty("codigoEmisorProducto")
	public String getCodigoEmisorProducto() {
		return codigoEmisorProducto;
	}

	public void setCodigoEmisorProducto(String codigoEmisorProducto) {
		this.codigoEmisorProducto = codigoEmisorProducto;
	}

	@JsonProperty("tipoProducto")
	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	@JsonProperty("numeroCuenta")
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	@JsonProperty("numeroTarjeta")
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	@JsonProperty("vencimientoTarjeta")
	public String getVencimientoTarjeta() {
		return vencimientoTarjeta;
	}

	public void setVencimientoTarjeta(String vencimientoTarjeta) {
		this.vencimientoTarjeta = vencimientoTarjeta;
	}

	@JsonProperty("nombreUsuarioProducto")
	public String getNombreUsuarioProducto() {
		return nombreUsuarioProducto;
	}

	public void setNombreUsuarioProducto(String nombreUsuarioProducto) {
		this.nombreUsuarioProducto = nombreUsuarioProducto;
	}

	@JsonProperty("aliasProducto")
	public String getAliasProducto() {
		return aliasProducto;
	}

	public void setAliasProducto(String aliasProducto) {
		this.aliasProducto = aliasProducto;
	}

	@JsonProperty("idTerminal")
	public String getIdTerminal() {
		return idTerminal;
	}

	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}

	@JsonProperty("numeroTransaccion")
	public String getNumeroTransaccion() {
		return numeroTransaccion;
	}

	public void setNumeroTransaccion(String numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}

	@JsonProperty("marcaProducto")
	public String getMarcaProducto() {
		return marcaProducto;
	}

	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}

	@JsonProperty("nombreProducto")
	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	@JsonProperty("detalleProducto")
	public String getDetalleProducto() {
		return detalleProducto;
	}

	public void setDetalleProducto(String detalleProducto) {
		this.detalleProducto = detalleProducto;
	}

	@JsonProperty("nombreEmisorProducto")
	public String getNombreEmisorProducto() {
		return nombreEmisorProducto;
	}

	public void setNombreEmisorProducto(String nombreEmisorProducto) {
		this.nombreEmisorProducto = nombreEmisorProducto;
	}

	@JsonProperty("actualizarProducto")
	public String getActualizarProducto() {
		return actualizarProducto;
	}

	public void setActualizarProducto(String actualizarProducto) {
		this.actualizarProducto = actualizarProducto;
	}

	@JsonProperty("listarProductos")
	public String getListarProductos() {
		return listarProductos;
	}

	public void setListarProductos(String listarProductos) {
		this.listarProductos = listarProductos;
	}
    
}
