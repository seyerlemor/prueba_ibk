/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.DireccionClienteLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class DireccionClienteClp extends BaseModelImpl<DireccionCliente>
	implements DireccionCliente {
	public DireccionClienteClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return DireccionCliente.class;
	}

	@Override
	public String getModelClassName() {
		return DireccionCliente.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idDireccion;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdDireccion(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idDireccion;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDireccion", getIdDireccion());
		attributes.put("idDatoCliente", getIdDatoCliente());
		attributes.put("tipoDireccion", getTipoDireccion());
		attributes.put("codigoUso", getCodigoUso());
		attributes.put("flagDireccionEstandar", getFlagDireccionEstandar());
		attributes.put("via", getVia());
		attributes.put("departamento", getDepartamento());
		attributes.put("provincia", getProvincia());
		attributes.put("distrito", getDistrito());
		attributes.put("pais", getPais());
		attributes.put("ubigeo", getUbigeo());
		attributes.put("codigoPostal", getCodigoPostal());
		attributes.put("estado", getEstado());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idDireccion = (Long)attributes.get("idDireccion");

		if (idDireccion != null) {
			setIdDireccion(idDireccion);
		}

		Long idDatoCliente = (Long)attributes.get("idDatoCliente");

		if (idDatoCliente != null) {
			setIdDatoCliente(idDatoCliente);
		}

		String tipoDireccion = (String)attributes.get("tipoDireccion");

		if (tipoDireccion != null) {
			setTipoDireccion(tipoDireccion);
		}

		String codigoUso = (String)attributes.get("codigoUso");

		if (codigoUso != null) {
			setCodigoUso(codigoUso);
		}

		String flagDireccionEstandar = (String)attributes.get(
				"flagDireccionEstandar");

		if (flagDireccionEstandar != null) {
			setFlagDireccionEstandar(flagDireccionEstandar);
		}

		String via = (String)attributes.get("via");

		if (via != null) {
			setVia(via);
		}

		String departamento = (String)attributes.get("departamento");

		if (departamento != null) {
			setDepartamento(departamento);
		}

		String provincia = (String)attributes.get("provincia");

		if (provincia != null) {
			setProvincia(provincia);
		}

		String distrito = (String)attributes.get("distrito");

		if (distrito != null) {
			setDistrito(distrito);
		}

		String pais = (String)attributes.get("pais");

		if (pais != null) {
			setPais(pais);
		}

		String ubigeo = (String)attributes.get("ubigeo");

		if (ubigeo != null) {
			setUbigeo(ubigeo);
		}

		String codigoPostal = (String)attributes.get("codigoPostal");

		if (codigoPostal != null) {
			setCodigoPostal(codigoPostal);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public long getIdDireccion() {
		return _idDireccion;
	}

	@Override
	public void setIdDireccion(long idDireccion) {
		_idDireccion = idDireccion;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdDireccion", long.class);

				method.invoke(_direccionClienteRemoteModel, idDireccion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdDatoCliente() {
		return _idDatoCliente;
	}

	@Override
	public void setIdDatoCliente(long idDatoCliente) {
		_idDatoCliente = idDatoCliente;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setIdDatoCliente", long.class);

				method.invoke(_direccionClienteRemoteModel, idDatoCliente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoDireccion() {
		return _tipoDireccion;
	}

	@Override
	public void setTipoDireccion(String tipoDireccion) {
		_tipoDireccion = tipoDireccion;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoDireccion", String.class);

				method.invoke(_direccionClienteRemoteModel, tipoDireccion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoUso() {
		return _codigoUso;
	}

	@Override
	public void setCodigoUso(String codigoUso) {
		_codigoUso = codigoUso;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoUso", String.class);

				method.invoke(_direccionClienteRemoteModel, codigoUso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFlagDireccionEstandar() {
		return _flagDireccionEstandar;
	}

	@Override
	public void setFlagDireccionEstandar(String flagDireccionEstandar) {
		_flagDireccionEstandar = flagDireccionEstandar;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFlagDireccionEstandar",
						String.class);

				method.invoke(_direccionClienteRemoteModel,
					flagDireccionEstandar);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVia() {
		return _via;
	}

	@Override
	public void setVia(String via) {
		_via = via;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setVia", String.class);

				method.invoke(_direccionClienteRemoteModel, via);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDepartamento() {
		return _departamento;
	}

	@Override
	public void setDepartamento(String departamento) {
		_departamento = departamento;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setDepartamento", String.class);

				method.invoke(_direccionClienteRemoteModel, departamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getProvincia() {
		return _provincia;
	}

	@Override
	public void setProvincia(String provincia) {
		_provincia = provincia;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setProvincia", String.class);

				method.invoke(_direccionClienteRemoteModel, provincia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDistrito() {
		return _distrito;
	}

	@Override
	public void setDistrito(String distrito) {
		_distrito = distrito;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setDistrito", String.class);

				method.invoke(_direccionClienteRemoteModel, distrito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPais() {
		return _pais;
	}

	@Override
	public void setPais(String pais) {
		_pais = pais;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setPais", String.class);

				method.invoke(_direccionClienteRemoteModel, pais);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUbigeo() {
		return _ubigeo;
	}

	@Override
	public void setUbigeo(String ubigeo) {
		_ubigeo = ubigeo;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setUbigeo", String.class);

				method.invoke(_direccionClienteRemoteModel, ubigeo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigoPostal() {
		return _codigoPostal;
	}

	@Override
	public void setCodigoPostal(String codigoPostal) {
		_codigoPostal = codigoPostal;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigoPostal", String.class);

				method.invoke(_direccionClienteRemoteModel, codigoPostal);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstado() {
		return _estado;
	}

	@Override
	public void setEstado(String estado) {
		_estado = estado;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", String.class);

				method.invoke(_direccionClienteRemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_direccionClienteRemoteModel != null) {
			try {
				Class<?> clazz = _direccionClienteRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_direccionClienteRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getDireccionClienteRemoteModel() {
		return _direccionClienteRemoteModel;
	}

	public void setDireccionClienteRemoteModel(
		BaseModel<?> direccionClienteRemoteModel) {
		_direccionClienteRemoteModel = direccionClienteRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _direccionClienteRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_direccionClienteRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			DireccionClienteLocalServiceUtil.addDireccionCliente(this);
		}
		else {
			DireccionClienteLocalServiceUtil.updateDireccionCliente(this);
		}
	}

	@Override
	public DireccionCliente toEscapedModel() {
		return (DireccionCliente)ProxyUtil.newProxyInstance(DireccionCliente.class.getClassLoader(),
			new Class[] { DireccionCliente.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DireccionClienteClp clone = new DireccionClienteClp();

		clone.setIdDireccion(getIdDireccion());
		clone.setIdDatoCliente(getIdDatoCliente());
		clone.setTipoDireccion(getTipoDireccion());
		clone.setCodigoUso(getCodigoUso());
		clone.setFlagDireccionEstandar(getFlagDireccionEstandar());
		clone.setVia(getVia());
		clone.setDepartamento(getDepartamento());
		clone.setProvincia(getProvincia());
		clone.setDistrito(getDistrito());
		clone.setPais(getPais());
		clone.setUbigeo(getUbigeo());
		clone.setCodigoPostal(getCodigoPostal());
		clone.setEstado(getEstado());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(DireccionCliente direccionCliente) {
		long primaryKey = direccionCliente.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DireccionClienteClp)) {
			return false;
		}

		DireccionClienteClp direccionCliente = (DireccionClienteClp)obj;

		long primaryKey = direccionCliente.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{idDireccion=");
		sb.append(getIdDireccion());
		sb.append(", idDatoCliente=");
		sb.append(getIdDatoCliente());
		sb.append(", tipoDireccion=");
		sb.append(getTipoDireccion());
		sb.append(", codigoUso=");
		sb.append(getCodigoUso());
		sb.append(", flagDireccionEstandar=");
		sb.append(getFlagDireccionEstandar());
		sb.append(", via=");
		sb.append(getVia());
		sb.append(", departamento=");
		sb.append(getDepartamento());
		sb.append(", provincia=");
		sb.append(getProvincia());
		sb.append(", distrito=");
		sb.append(getDistrito());
		sb.append(", pais=");
		sb.append(getPais());
		sb.append(", ubigeo=");
		sb.append(getUbigeo());
		sb.append(", codigoPostal=");
		sb.append(getCodigoPostal());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.DireccionCliente");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idDireccion</column-name><column-value><![CDATA[");
		sb.append(getIdDireccion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idDatoCliente</column-name><column-value><![CDATA[");
		sb.append(getIdDatoCliente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoDireccion</column-name><column-value><![CDATA[");
		sb.append(getTipoDireccion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoUso</column-name><column-value><![CDATA[");
		sb.append(getCodigoUso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flagDireccionEstandar</column-name><column-value><![CDATA[");
		sb.append(getFlagDireccionEstandar());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>via</column-name><column-value><![CDATA[");
		sb.append(getVia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>departamento</column-name><column-value><![CDATA[");
		sb.append(getDepartamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>provincia</column-name><column-value><![CDATA[");
		sb.append(getProvincia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>distrito</column-name><column-value><![CDATA[");
		sb.append(getDistrito());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pais</column-name><column-value><![CDATA[");
		sb.append(getPais());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ubigeo</column-name><column-value><![CDATA[");
		sb.append(getUbigeo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigoPostal</column-name><column-value><![CDATA[");
		sb.append(getCodigoPostal());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idDireccion;
	private long _idDatoCliente;
	private String _tipoDireccion;
	private String _codigoUso;
	private String _flagDireccionEstandar;
	private String _via;
	private String _departamento;
	private String _provincia;
	private String _distrito;
	private String _pais;
	private String _ubigeo;
	private String _codigoPostal;
	private String _estado;
	private Date _fechaRegistro;
	private BaseModel<?> _direccionClienteRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}