/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.Servicios;

import java.util.List;

/**
 * The persistence utility for the servicios service. This utility wraps {@link ServiciosPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ServiciosPersistence
 * @see ServiciosPersistenceImpl
 * @generated
 */
public class ServiciosUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Servicios servicios) {
		getPersistence().clearCache(servicios);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Servicios> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Servicios> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Servicios> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Servicios update(Servicios servicios)
		throws SystemException {
		return getPersistence().update(servicios);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Servicios update(Servicios servicios,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(servicios, serviceContext);
	}

	/**
	* Returns the servicios where idServicio = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchServiciosException} if it could not be found.
	*
	* @param idServicio the id servicio
	* @return the matching servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios findByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence().findByS_ID(idServicio);
	}

	/**
	* Returns the servicios where idServicio = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idServicio the id servicio
	* @return the matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios fetchByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByS_ID(idServicio);
	}

	/**
	* Returns the servicios where idServicio = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idServicio the id servicio
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios fetchByS_ID(
		long idServicio, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByS_ID(idServicio, retrieveFromCache);
	}

	/**
	* Removes the servicios where idServicio = &#63; from the database.
	*
	* @param idServicio the id servicio
	* @return the servicios that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios removeByS_ID(
		long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence().removeByS_ID(idServicio);
	}

	/**
	* Returns the number of servicioses where idServicio = &#63;.
	*
	* @param idServicio the id servicio
	* @return the number of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByS_ID(idServicio);
	}

	/**
	* Returns all the servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @return the matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Servicios> findByIdExpediente(
		long idExpediente, java.lang.String tipo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdExpediente(idExpediente, tipo);
	}

	/**
	* Returns a range of all the servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @return the range of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Servicios> findByIdExpediente(
		long idExpediente, java.lang.String tipo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdExpediente(idExpediente, tipo, start, end);
	}

	/**
	* Returns an ordered range of all the servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Servicios> findByIdExpediente(
		long idExpediente, java.lang.String tipo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdExpediente(idExpediente, tipo, start, end,
			orderByComparator);
	}

	/**
	* Returns the first servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios findByIdExpediente_First(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence()
				   .findByIdExpediente_First(idExpediente, tipo,
			orderByComparator);
	}

	/**
	* Returns the first servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios fetchByIdExpediente_First(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdExpediente_First(idExpediente, tipo,
			orderByComparator);
	}

	/**
	* Returns the last servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios findByIdExpediente_Last(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence()
				   .findByIdExpediente_Last(idExpediente, tipo,
			orderByComparator);
	}

	/**
	* Returns the last servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios fetchByIdExpediente_Last(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdExpediente_Last(idExpediente, tipo,
			orderByComparator);
	}

	/**
	* Returns the servicioses before and after the current servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idServicio the primary key of the current servicios
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios[] findByIdExpediente_PrevAndNext(
		long idServicio, long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence()
				   .findByIdExpediente_PrevAndNext(idServicio, idExpediente,
			tipo, orderByComparator);
	}

	/**
	* Removes all the servicioses where idExpediente = &#63; and tipo = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdExpediente(long idExpediente,
		java.lang.String tipo)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdExpediente(idExpediente, tipo);
	}

	/**
	* Returns the number of servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @return the number of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdExpediente(long idExpediente,
		java.lang.String tipo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdExpediente(idExpediente, tipo);
	}

	/**
	* Caches the servicios in the entity cache if it is enabled.
	*
	* @param servicios the servicios
	*/
	public static void cacheResult(pe.com.ibk.pepper.model.Servicios servicios) {
		getPersistence().cacheResult(servicios);
	}

	/**
	* Caches the servicioses in the entity cache if it is enabled.
	*
	* @param servicioses the servicioses
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Servicios> servicioses) {
		getPersistence().cacheResult(servicioses);
	}

	/**
	* Creates a new servicios with the primary key. Does not add the servicios to the database.
	*
	* @param idServicio the primary key for the new servicios
	* @return the new servicios
	*/
	public static pe.com.ibk.pepper.model.Servicios create(long idServicio) {
		return getPersistence().create(idServicio);
	}

	/**
	* Removes the servicios with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios that was removed
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios remove(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence().remove(idServicio);
	}

	public static pe.com.ibk.pepper.model.Servicios updateImpl(
		pe.com.ibk.pepper.model.Servicios servicios)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(servicios);
	}

	/**
	* Returns the servicios with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchServiciosException} if it could not be found.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios findByPrimaryKey(
		long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException {
		return getPersistence().findByPrimaryKey(idServicio);
	}

	/**
	* Returns the servicios with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios, or <code>null</code> if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.Servicios fetchByPrimaryKey(
		long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idServicio);
	}

	/**
	* Returns all the servicioses.
	*
	* @return the servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Servicios> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the servicioses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @return the range of servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Servicios> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the servicioses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.Servicios> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the servicioses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of servicioses.
	*
	* @return the number of servicioses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ServiciosPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ServiciosPersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					ServiciosPersistence.class.getName());

			ReferenceRegistry.registerReference(ServiciosUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ServiciosPersistence persistence) {
	}

	private static ServiciosPersistence _persistence;
}