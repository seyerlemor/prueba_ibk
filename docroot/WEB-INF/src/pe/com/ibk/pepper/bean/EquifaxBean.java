package pe.com.ibk.pepper.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.com.ibk.pepper.rest.equifax.request.PreguntasVal;
import pe.com.ibk.pepper.rest.equifax.response.Preguntum;

public class EquifaxBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idExpediente;
	private String modelo;
	private String nroOperacion;
	private String resultadoEquifax;
	
	/* Valores de entrada */
	private String totalRechazados;
	private String totalDesaprobados;
	private String totalSinRespuesta;
	//Agregados
	private String usuario;
	private String fecha;
	private String hora;
	private String nombreConsultado;
	private int nroIntento;
	
	public int getNroIntento() {
		return nroIntento;
	}
	public void setNroIntento(int nroIntento) {
		this.nroIntento = nroIntento;
	}
	public String getResultadoEquifax() {
		return resultadoEquifax;
	}
	public void setResultadoEquifax(String resultadoEquifax) {
		this.resultadoEquifax = resultadoEquifax;
	}
	public String getNroOperacion() {
		return nroOperacion;
	}
	public void setNroOperacion(String nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public EquifaxBean(){
		preguntas = new ArrayList<Preguntum>();
	}
	
	private List<Preguntum> preguntas;
	
	private PreguntasVal preguntasVal;
	
	public PreguntasVal getPreguntasVal() {
		return preguntasVal;
	}
	public void setPreguntasVal(PreguntasVal preguntasVal) {
		this.preguntasVal = preguntasVal;
	}
	public int getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTotalRechazados() {
		return totalRechazados;
	}
	public void setTotalRechazados(String totalRechazados) {
		this.totalRechazados = totalRechazados;
	}
	public String getTotalDesaprobados() {
		return totalDesaprobados;
	}
	public void setTotalDesaprobados(String totalDesaprobados) {
		this.totalDesaprobados = totalDesaprobados;
	}
	public String getTotalSinRespuesta() {
		return totalSinRespuesta;
	}
	public void setTotalSinRespuesta(String totalSinRespuesta) {
		this.totalSinRespuesta = totalSinRespuesta;
	}
	public List<Preguntum> getPreguntas() {
		return preguntas;
	}
	public void setPreguntas(List<Preguntum> preguntas) {
		this.preguntas = preguntas;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getNombreConsultado() {
		return nombreConsultado;
	}
	public void setNombreConsultado(String nombreConsultado) {
		this.nombreConsultado = nombreConsultado;
	}
	@Override
	public String toString() {
		return "EquifaxBean [idExpediente=" + idExpediente + ", modelo="
				+ modelo + ", nroOperacion=" + nroOperacion
				+ ", resultadoEquifax=" + resultadoEquifax
				+ ", totalRechazados=" + totalRechazados
				+ ", totalDesaprobados=" + totalDesaprobados
				+ ", totalSinRespuesta=" + totalSinRespuesta + ", usuario="
				+ usuario + ", fecha=" + fecha + ", hora=" + hora
				+ ", nombreConsultado=" + nombreConsultado + ", nroIntento="
				+ nroIntento + ", preguntas=" + preguntas + ", preguntasVal="
				+ preguntasVal + "]";
	}

}