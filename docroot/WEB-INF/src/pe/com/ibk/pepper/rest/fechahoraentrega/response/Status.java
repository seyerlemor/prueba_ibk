package pe.com.ibk.pepper.rest.fechahoraentrega.response;

public class Status {
	private String responseType;
	private String busResponseCode;
	private String busResponseMessage;
	private String srvResponseCode;
	private String srvResponseMessage;
	private String messageIdRes;
	
	public Status() {
		super();
	}
	
	public Status(String responseType, String busResponseCode,
			String busResponseMessage, String srvResponseCode,
			String srvResponseMessage, String messageIdRes) {
		super();
		this.responseType = responseType;
		this.busResponseCode = busResponseCode;
		this.busResponseMessage = busResponseMessage;
		this.srvResponseCode = srvResponseCode;
		this.srvResponseMessage = srvResponseMessage;
		this.messageIdRes = messageIdRes;
	}
	
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	public String getBusResponseCode() {
		return busResponseCode;
	}
	public void setBusResponseCode(String busResponseCode) {
		this.busResponseCode = busResponseCode;
	}
	public String getBusResponseMessage() {
		return busResponseMessage;
	}
	public void setBusResponseMessage(String busResponseMessage) {
		this.busResponseMessage = busResponseMessage;
	}
	public String getSrvResponseCode() {
		return srvResponseCode;
	}
	public void setSrvResponseCode(String srvResponseCode) {
		this.srvResponseCode = srvResponseCode;
	}
	public String getSrvResponseMessage() {
		return srvResponseMessage;
	}
	public void setSrvResponseMessage(String srvResponseMessage) {
		this.srvResponseMessage = srvResponseMessage;
	}
	public String getMessageIdRes() {
		return messageIdRes;
	}
	public void setMessageIdRes(String messageIdRes) {
		this.messageIdRes = messageIdRes;
	}
	
}
