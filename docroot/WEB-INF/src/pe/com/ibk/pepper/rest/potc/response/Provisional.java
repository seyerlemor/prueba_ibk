
package pe.com.ibk.pepper.rest.potc.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Provisional implements Serializable{

	private static final long serialVersionUID = 7948531047961755006L;
	
	@SerializedName(value="numeroTarjeta")
	private String numeroTarjeta;
	
	@SerializedName(value="fechaVencimiento")
    private String fechaVencimiento;

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

}
