
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DatosCampania {

    private String idCampania;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DatosCampania() {
    }

    /**
     * 
     * @param idCampania
     */
    public DatosCampania(String idCampania) {
        super();
        this.idCampania = idCampania;
    }

    public String getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(String idCampania) {
        this.idCampania = idCampania;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idCampania", idCampania).toString();
    }

}
