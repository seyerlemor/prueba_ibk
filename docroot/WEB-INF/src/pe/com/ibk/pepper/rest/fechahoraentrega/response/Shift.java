
package pe.com.ibk.pepper.rest.fechahoraentrega.response;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Shift {

    private Integer id;
    private String hour;
    private String description;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Shift() {
    }

    /**
     * 
     * @param id
     * @param description
     * @param hour
     */
    public Shift(Integer id, String hour, String description) {
        super();
        this.id = id;
        this.hour = hour;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("hour", hour).append("description", description).toString();
    }

}
