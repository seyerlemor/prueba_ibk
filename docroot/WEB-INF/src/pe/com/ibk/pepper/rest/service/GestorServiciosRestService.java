package pe.com.ibk.pepper.rest.service;

import javax.portlet.ResourceRequest;

import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.bean.RegistroAuditoriaBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;

public interface GestorServiciosRestService {

	public SolicitudBean validarAntiFraude(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException;
	
	public SolicitudBean consultarCampania(SolicitudBean pasoBean, ResourceRequest request, String path) throws WebServicesException;
	
	public SolicitudBean getDocumentLead(SolicitudBean pasoBean, String path, String path2, ResourceRequest request) throws WebServicesException;
	
	public SolicitudBean getApitLead(SolicitudBean pasoBean, String path, ResourceRequest request) throws WebServicesException;

	public SolicitudBean obtenerApiInformacionPersona(SolicitudBean pasoBean, String path) throws WebServicesException;
	
	public SolicitudBean validarListaNegra(SolicitudBean pasoBean, String path) throws WebServicesException;	
	
	public SolicitudBean validarListaNegra2G(SolicitudBean pasoBean, String path) throws WebServicesException;

	public SolicitudBean validarLogicaReingreso(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException;

	public SolicitudBean validarBancaSMS(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException;
	
	public SolicitudBean generarToken(SolicitudBean pasoBean, String path) throws WebServicesException;

	public SolicitudBean validarToken(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException ;
	
	public SolicitudBean consultarPreguntasEquifax(SolicitudBean pasoBean, String path) throws WebServicesException;

	public SolicitudBean validarPreguntasEquifax(SolicitudBean pasoBean, String path) throws WebServicesException;
	
	public SolicitudBean evaluarCalificacion(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException;
	
	public SolicitudBean consultarFechaHoraApi3G(SolicitudBean pasoBean, String codigoUbigeo, String path) throws WebServicesException;
	
	public SolicitudBean consultarFechaHoraApi2G(SolicitudBean pasoBean, pe.com.ibk.pepper.rest.obtenerinformacion.response.Direccion direccionActual, String path) throws WebServicesException;
	
	public SolicitudBean altaTc(SolicitudBean pasoBean, int indice, String pepperType, ResourceRequest request) throws WebServicesException;
	
	public SolicitudBean consultarProvisional(SolicitudBean pasoBean) throws WebServicesException ;
	
	public SolicitudBean consultarPOTC(SolicitudBean pasoBean) throws WebServicesException; 
	
	public SolicitudBean generarPOTC(SolicitudBean pasoBean) throws WebServicesException;
	
	public void registrarAuditoria(RegistroAuditoriaBean registroAuditoriaBean) throws WebServicesException;
}
