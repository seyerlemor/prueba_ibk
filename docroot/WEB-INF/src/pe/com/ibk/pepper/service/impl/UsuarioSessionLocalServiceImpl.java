/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;

import pe.com.ibk.pepper.NoSuchUsuarioSessionException;
import pe.com.ibk.pepper.model.UsuarioSession;
import pe.com.ibk.pepper.service.base.UsuarioSessionLocalServiceBaseImpl;

/**
 * The implementation of the usuario session local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.UsuarioSessionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.UsuarioSessionLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.UsuarioSessionLocalServiceUtil
 */
public class UsuarioSessionLocalServiceImpl
	extends UsuarioSessionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.UsuarioSessionLocalServiceUtil} to access the usuario session local service.
	 */
	private static final Log _log = LogFactoryUtil.getLog(UsuarioSessionLocalServiceImpl.class);
	@Override
	public UsuarioSession registrarUsuarioSession(UsuarioSession usuarioSession) throws SystemException{
		
		Date now = DateUtil.newDate();
		if (usuarioSession.isNew()) {
			usuarioSession.setFechaRegistro(now);
		}
		usuarioSessionPersistence.update(usuarioSession);
		return usuarioSession;
	}		
	
	@Override
	public UsuarioSession buscarUsuarioSessionporId(long idUsuarioSession){
		UsuarioSession usuarioSession = null;
		try {
				usuarioSession = usuarioSessionPersistence.findByUS_ID(idUsuarioSession);
		} catch (NoSuchUsuarioSessionException | SystemException e) {
			_log.debug("Error en clase UsuarioSessionLocalServiceImpl metodo buscarUsuarioSessionporId:: NO EXISTE EN UsuarioSession ", e);
		}	
		return usuarioSession;
	}	
	
	@Override
	public UsuarioSession buscarUsuarioSession(String idUsuarioSession){
		UsuarioSession usuarioSession = null;
		try {
				usuarioSession = usuarioSessionPersistence.findByU_S_S(idUsuarioSession);
		} catch (NoSuchUsuarioSessionException | SystemException e) {
			_log.debug("Error en clase UsuarioSessionLocalServiceImpl metodo buscarUsuarioSessionporSession: NO EXISTE EN UsuarioSession ");
		}	
		return usuarioSession;
	}
}