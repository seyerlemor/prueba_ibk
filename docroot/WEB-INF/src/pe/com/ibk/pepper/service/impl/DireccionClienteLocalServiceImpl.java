/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;

import pe.com.ibk.pepper.NoSuchDireccionClienteException;
import pe.com.ibk.pepper.model.DireccionCliente;
import pe.com.ibk.pepper.service.base.DireccionClienteLocalServiceBaseImpl;

/**
 * The implementation of the direccion cliente local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.DireccionClienteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.DireccionClienteLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.DireccionClienteLocalServiceUtil
 */
public class DireccionClienteLocalServiceImpl
	extends DireccionClienteLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.DireccionClienteLocalServiceUtil} to access the direccion cliente local service.
	 */
	
	private static final Log _log = LogFactoryUtil.getLog(DireccionClienteLocalServiceImpl.class);
	
	@Override
	public DireccionCliente registrarDireccionCliente(DireccionCliente direccionCliente) throws SystemException{
		Date now = DateUtil.newDate();
		if (direccionCliente.isNew()) {
			direccionCliente.setFechaRegistro(now);
		}
		direccionClientePersistence.update(direccionCliente);
		return direccionCliente;
	}	

	@Override
	public DireccionCliente buscarDireccionClienteporId(long idDireccionCliente){
		DireccionCliente direccionCliente = null;
		try {
			direccionCliente = direccionClientePersistence.findByDC_ID(idDireccionCliente);
		} catch (NoSuchDireccionClienteException e) {
			_log.error("Error en clase DireccionClienteocalServiceImpl metodo buscarDireccionClienteporId:: NO EXISTE EN DIRECCIONCLIENTE ", e );
		} catch (SystemException e) {
			_log.error("Error en clase DireccionClienteLocalServiceImpl metodo buscarDireccionClienteporId:: ", e );
		}
		return direccionCliente;
	}	
	
	@Override
	public DireccionCliente buscarDireccionClienteporCodigoUso(String codigoUso){
		DireccionCliente direccionCliente = null;
		try {
			direccionCliente = direccionClientePersistence.findByDC_CU(codigoUso);
		} catch (NoSuchDireccionClienteException e) {
			_log.error("Error en clase DireccionClienteocalServiceImpl metodo buscarDireccionClienteporCodigoUso:: NO EXISTE EN DIRECCIONCLIENTE " );
		} catch (SystemException e) {
			_log.error("Error en clase DireccionClienteLocalServiceImpl metodo buscarDireccionClienteporCodigoUso:: " );
		}
		return direccionCliente;
	}	
	
}