/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.ClienteTransaccion;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ClienteTransaccion in entity cache.
 *
 * @author Interbank
 * @see ClienteTransaccion
 * @generated
 */
public class ClienteTransaccionCacheModel implements CacheModel<ClienteTransaccion>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{idClienteTransaccion=");
		sb.append(idClienteTransaccion);
		sb.append(", idUsuarioSession=");
		sb.append(idUsuarioSession);
		sb.append(", idExpediente=");
		sb.append(idExpediente);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", paso=");
		sb.append(paso);
		sb.append(", pagina=");
		sb.append(pagina);
		sb.append(", tipoFlujo=");
		sb.append(tipoFlujo);
		sb.append(", strJson=");
		sb.append(strJson);
		sb.append(", restauracion=");
		sb.append(restauracion);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ClienteTransaccion toEntityModel() {
		ClienteTransaccionImpl clienteTransaccionImpl = new ClienteTransaccionImpl();

		clienteTransaccionImpl.setIdClienteTransaccion(idClienteTransaccion);
		clienteTransaccionImpl.setIdUsuarioSession(idUsuarioSession);
		clienteTransaccionImpl.setIdExpediente(idExpediente);

		if (estado == null) {
			clienteTransaccionImpl.setEstado(StringPool.BLANK);
		}
		else {
			clienteTransaccionImpl.setEstado(estado);
		}

		if (paso == null) {
			clienteTransaccionImpl.setPaso(StringPool.BLANK);
		}
		else {
			clienteTransaccionImpl.setPaso(paso);
		}

		if (pagina == null) {
			clienteTransaccionImpl.setPagina(StringPool.BLANK);
		}
		else {
			clienteTransaccionImpl.setPagina(pagina);
		}

		if (tipoFlujo == null) {
			clienteTransaccionImpl.setTipoFlujo(StringPool.BLANK);
		}
		else {
			clienteTransaccionImpl.setTipoFlujo(tipoFlujo);
		}

		if (strJson == null) {
			clienteTransaccionImpl.setStrJson(StringPool.BLANK);
		}
		else {
			clienteTransaccionImpl.setStrJson(strJson);
		}

		if (restauracion == null) {
			clienteTransaccionImpl.setRestauracion(StringPool.BLANK);
		}
		else {
			clienteTransaccionImpl.setRestauracion(restauracion);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			clienteTransaccionImpl.setFechaRegistro(null);
		}
		else {
			clienteTransaccionImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		clienteTransaccionImpl.resetOriginalValues();

		return clienteTransaccionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idClienteTransaccion = objectInput.readLong();
		idUsuarioSession = objectInput.readLong();
		idExpediente = objectInput.readLong();
		estado = objectInput.readUTF();
		paso = objectInput.readUTF();
		pagina = objectInput.readUTF();
		tipoFlujo = objectInput.readUTF();
		strJson = objectInput.readUTF();
		restauracion = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idClienteTransaccion);
		objectOutput.writeLong(idUsuarioSession);
		objectOutput.writeLong(idExpediente);

		if (estado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estado);
		}

		if (paso == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paso);
		}

		if (pagina == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pagina);
		}

		if (tipoFlujo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoFlujo);
		}

		if (strJson == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(strJson);
		}

		if (restauracion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(restauracion);
		}

		objectOutput.writeLong(fechaRegistro);
	}

	public long idClienteTransaccion;
	public long idUsuarioSession;
	public long idExpediente;
	public String estado;
	public String paso;
	public String pagina;
	public String tipoFlujo;
	public String strJson;
	public String restauracion;
	public long fechaRegistro;
}