/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.LogQRadar;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing LogQRadar in entity cache.
 *
 * @author Interbank
 * @see LogQRadar
 * @generated
 */
public class LogQRadarCacheModel implements CacheModel<LogQRadar>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{idLogQRadar=");
		sb.append(idLogQRadar);
		sb.append(", prioridad=");
		sb.append(prioridad);
		sb.append(", severidad=");
		sb.append(severidad);
		sb.append(", recurso=");
		sb.append(recurso);
		sb.append(", utc=");
		sb.append(utc);
		sb.append(", logCreador=");
		sb.append(logCreador);
		sb.append(", tipoEvento=");
		sb.append(tipoEvento);
		sb.append(", registroUsuario=");
		sb.append(registroUsuario);
		sb.append(", origenEvento=");
		sb.append(origenEvento);
		sb.append(", respuesta=");
		sb.append(respuesta);
		sb.append(", ambiente=");
		sb.append(ambiente);
		sb.append(", producto=");
		sb.append(producto);
		sb.append(", idProducto=");
		sb.append(idProducto);
		sb.append(", fechaHora=");
		sb.append(fechaHora);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LogQRadar toEntityModel() {
		LogQRadarImpl logQRadarImpl = new LogQRadarImpl();

		logQRadarImpl.setIdLogQRadar(idLogQRadar);
		logQRadarImpl.setPrioridad(prioridad);
		logQRadarImpl.setSeveridad(severidad);
		logQRadarImpl.setRecurso(recurso);

		if (utc == Long.MIN_VALUE) {
			logQRadarImpl.setUtc(null);
		}
		else {
			logQRadarImpl.setUtc(new Date(utc));
		}

		if (logCreador == null) {
			logQRadarImpl.setLogCreador(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setLogCreador(logCreador);
		}

		if (tipoEvento == null) {
			logQRadarImpl.setTipoEvento(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setTipoEvento(tipoEvento);
		}

		if (registroUsuario == null) {
			logQRadarImpl.setRegistroUsuario(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setRegistroUsuario(registroUsuario);
		}

		if (origenEvento == null) {
			logQRadarImpl.setOrigenEvento(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setOrigenEvento(origenEvento);
		}

		if (respuesta == null) {
			logQRadarImpl.setRespuesta(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setRespuesta(respuesta);
		}

		if (ambiente == null) {
			logQRadarImpl.setAmbiente(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setAmbiente(ambiente);
		}

		if (producto == null) {
			logQRadarImpl.setProducto(StringPool.BLANK);
		}
		else {
			logQRadarImpl.setProducto(producto);
		}

		logQRadarImpl.setIdProducto(idProducto);

		if (fechaHora == Long.MIN_VALUE) {
			logQRadarImpl.setFechaHora(null);
		}
		else {
			logQRadarImpl.setFechaHora(new Date(fechaHora));
		}

		logQRadarImpl.resetOriginalValues();

		return logQRadarImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idLogQRadar = objectInput.readInt();
		prioridad = objectInput.readInt();
		severidad = objectInput.readInt();
		recurso = objectInput.readInt();
		utc = objectInput.readLong();
		logCreador = objectInput.readUTF();
		tipoEvento = objectInput.readUTF();
		registroUsuario = objectInput.readUTF();
		origenEvento = objectInput.readUTF();
		respuesta = objectInput.readUTF();
		ambiente = objectInput.readUTF();
		producto = objectInput.readUTF();
		idProducto = objectInput.readInt();
		fechaHora = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idLogQRadar);
		objectOutput.writeInt(prioridad);
		objectOutput.writeInt(severidad);
		objectOutput.writeInt(recurso);
		objectOutput.writeLong(utc);

		if (logCreador == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(logCreador);
		}

		if (tipoEvento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoEvento);
		}

		if (registroUsuario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(registroUsuario);
		}

		if (origenEvento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(origenEvento);
		}

		if (respuesta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(respuesta);
		}

		if (ambiente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ambiente);
		}

		if (producto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(producto);
		}

		objectOutput.writeInt(idProducto);
		objectOutput.writeLong(fechaHora);
	}

	public int idLogQRadar;
	public int prioridad;
	public int severidad;
	public int recurso;
	public long utc;
	public String logCreador;
	public String tipoEvento;
	public String registroUsuario;
	public String origenEvento;
	public String respuesta;
	public String ambiente;
	public String producto;
	public int idProducto;
	public long fechaHora;
}