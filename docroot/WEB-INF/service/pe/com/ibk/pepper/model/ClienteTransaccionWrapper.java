/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ClienteTransaccion}.
 * </p>
 *
 * @author Interbank
 * @see ClienteTransaccion
 * @generated
 */
public class ClienteTransaccionWrapper implements ClienteTransaccion,
	ModelWrapper<ClienteTransaccion> {
	public ClienteTransaccionWrapper(ClienteTransaccion clienteTransaccion) {
		_clienteTransaccion = clienteTransaccion;
	}

	@Override
	public Class<?> getModelClass() {
		return ClienteTransaccion.class;
	}

	@Override
	public String getModelClassName() {
		return ClienteTransaccion.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idClienteTransaccion", getIdClienteTransaccion());
		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("estado", getEstado());
		attributes.put("paso", getPaso());
		attributes.put("pagina", getPagina());
		attributes.put("tipoFlujo", getTipoFlujo());
		attributes.put("strJson", getStrJson());
		attributes.put("restauracion", getRestauracion());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idClienteTransaccion = (Long)attributes.get("idClienteTransaccion");

		if (idClienteTransaccion != null) {
			setIdClienteTransaccion(idClienteTransaccion);
		}

		Long idUsuarioSession = (Long)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String paso = (String)attributes.get("paso");

		if (paso != null) {
			setPaso(paso);
		}

		String pagina = (String)attributes.get("pagina");

		if (pagina != null) {
			setPagina(pagina);
		}

		String tipoFlujo = (String)attributes.get("tipoFlujo");

		if (tipoFlujo != null) {
			setTipoFlujo(tipoFlujo);
		}

		String strJson = (String)attributes.get("strJson");

		if (strJson != null) {
			setStrJson(strJson);
		}

		String restauracion = (String)attributes.get("restauracion");

		if (restauracion != null) {
			setRestauracion(restauracion);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this cliente transaccion.
	*
	* @return the primary key of this cliente transaccion
	*/
	@Override
	public long getPrimaryKey() {
		return _clienteTransaccion.getPrimaryKey();
	}

	/**
	* Sets the primary key of this cliente transaccion.
	*
	* @param primaryKey the primary key of this cliente transaccion
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clienteTransaccion.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id cliente transaccion of this cliente transaccion.
	*
	* @return the id cliente transaccion of this cliente transaccion
	*/
	@Override
	public long getIdClienteTransaccion() {
		return _clienteTransaccion.getIdClienteTransaccion();
	}

	/**
	* Sets the id cliente transaccion of this cliente transaccion.
	*
	* @param idClienteTransaccion the id cliente transaccion of this cliente transaccion
	*/
	@Override
	public void setIdClienteTransaccion(long idClienteTransaccion) {
		_clienteTransaccion.setIdClienteTransaccion(idClienteTransaccion);
	}

	/**
	* Returns the id usuario session of this cliente transaccion.
	*
	* @return the id usuario session of this cliente transaccion
	*/
	@Override
	public long getIdUsuarioSession() {
		return _clienteTransaccion.getIdUsuarioSession();
	}

	/**
	* Sets the id usuario session of this cliente transaccion.
	*
	* @param idUsuarioSession the id usuario session of this cliente transaccion
	*/
	@Override
	public void setIdUsuarioSession(long idUsuarioSession) {
		_clienteTransaccion.setIdUsuarioSession(idUsuarioSession);
	}

	/**
	* Returns the id expediente of this cliente transaccion.
	*
	* @return the id expediente of this cliente transaccion
	*/
	@Override
	public long getIdExpediente() {
		return _clienteTransaccion.getIdExpediente();
	}

	/**
	* Sets the id expediente of this cliente transaccion.
	*
	* @param idExpediente the id expediente of this cliente transaccion
	*/
	@Override
	public void setIdExpediente(long idExpediente) {
		_clienteTransaccion.setIdExpediente(idExpediente);
	}

	/**
	* Returns the estado of this cliente transaccion.
	*
	* @return the estado of this cliente transaccion
	*/
	@Override
	public java.lang.String getEstado() {
		return _clienteTransaccion.getEstado();
	}

	/**
	* Sets the estado of this cliente transaccion.
	*
	* @param estado the estado of this cliente transaccion
	*/
	@Override
	public void setEstado(java.lang.String estado) {
		_clienteTransaccion.setEstado(estado);
	}

	/**
	* Returns the paso of this cliente transaccion.
	*
	* @return the paso of this cliente transaccion
	*/
	@Override
	public java.lang.String getPaso() {
		return _clienteTransaccion.getPaso();
	}

	/**
	* Sets the paso of this cliente transaccion.
	*
	* @param paso the paso of this cliente transaccion
	*/
	@Override
	public void setPaso(java.lang.String paso) {
		_clienteTransaccion.setPaso(paso);
	}

	/**
	* Returns the pagina of this cliente transaccion.
	*
	* @return the pagina of this cliente transaccion
	*/
	@Override
	public java.lang.String getPagina() {
		return _clienteTransaccion.getPagina();
	}

	/**
	* Sets the pagina of this cliente transaccion.
	*
	* @param pagina the pagina of this cliente transaccion
	*/
	@Override
	public void setPagina(java.lang.String pagina) {
		_clienteTransaccion.setPagina(pagina);
	}

	/**
	* Returns the tipo flujo of this cliente transaccion.
	*
	* @return the tipo flujo of this cliente transaccion
	*/
	@Override
	public java.lang.String getTipoFlujo() {
		return _clienteTransaccion.getTipoFlujo();
	}

	/**
	* Sets the tipo flujo of this cliente transaccion.
	*
	* @param tipoFlujo the tipo flujo of this cliente transaccion
	*/
	@Override
	public void setTipoFlujo(java.lang.String tipoFlujo) {
		_clienteTransaccion.setTipoFlujo(tipoFlujo);
	}

	/**
	* Returns the str json of this cliente transaccion.
	*
	* @return the str json of this cliente transaccion
	*/
	@Override
	public java.lang.String getStrJson() {
		return _clienteTransaccion.getStrJson();
	}

	/**
	* Sets the str json of this cliente transaccion.
	*
	* @param strJson the str json of this cliente transaccion
	*/
	@Override
	public void setStrJson(java.lang.String strJson) {
		_clienteTransaccion.setStrJson(strJson);
	}

	/**
	* Returns the restauracion of this cliente transaccion.
	*
	* @return the restauracion of this cliente transaccion
	*/
	@Override
	public java.lang.String getRestauracion() {
		return _clienteTransaccion.getRestauracion();
	}

	/**
	* Sets the restauracion of this cliente transaccion.
	*
	* @param restauracion the restauracion of this cliente transaccion
	*/
	@Override
	public void setRestauracion(java.lang.String restauracion) {
		_clienteTransaccion.setRestauracion(restauracion);
	}

	/**
	* Returns the fecha registro of this cliente transaccion.
	*
	* @return the fecha registro of this cliente transaccion
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _clienteTransaccion.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this cliente transaccion.
	*
	* @param fechaRegistro the fecha registro of this cliente transaccion
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_clienteTransaccion.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _clienteTransaccion.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clienteTransaccion.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clienteTransaccion.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clienteTransaccion.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clienteTransaccion.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clienteTransaccion.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clienteTransaccion.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clienteTransaccion.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clienteTransaccion.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clienteTransaccion.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clienteTransaccion.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ClienteTransaccionWrapper((ClienteTransaccion)_clienteTransaccion.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ClienteTransaccion clienteTransaccion) {
		return _clienteTransaccion.compareTo(clienteTransaccion);
	}

	@Override
	public int hashCode() {
		return _clienteTransaccion.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.ClienteTransaccion> toCacheModel() {
		return _clienteTransaccion.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.ClienteTransaccion toEscapedModel() {
		return new ClienteTransaccionWrapper(_clienteTransaccion.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.ClienteTransaccion toUnescapedModel() {
		return new ClienteTransaccionWrapper(_clienteTransaccion.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clienteTransaccion.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clienteTransaccion.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clienteTransaccion.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClienteTransaccionWrapper)) {
			return false;
		}

		ClienteTransaccionWrapper clienteTransaccionWrapper = (ClienteTransaccionWrapper)obj;

		if (Validator.equals(_clienteTransaccion,
					clienteTransaccionWrapper._clienteTransaccion)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ClienteTransaccion getWrappedClienteTransaccion() {
		return _clienteTransaccion;
	}

	@Override
	public ClienteTransaccion getWrappedModel() {
		return _clienteTransaccion;
	}

	@Override
	public void resetOriginalValues() {
		_clienteTransaccion.resetOriginalValues();
	}

	private ClienteTransaccion _clienteTransaccion;
}