/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;

import pe.com.ibk.pepper.model.DirecEstandar;
import pe.com.ibk.pepper.service.base.DirecEstandarLocalServiceBaseImpl;

/**
 * The implementation of the direc estandar local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.DirecEstandarLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.DirecEstandarLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.DirecEstandarLocalServiceUtil
 */
public class DirecEstandarLocalServiceImpl
	extends DirecEstandarLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.DirecEstandarLocalServiceUtil} to access the direc estandar local service.
	 */
	@Override
	public DirecEstandar registrarDireccionEstandar(DirecEstandar direccionEstandar) throws SystemException{
		direcEstandarPersistence.update(direccionEstandar);
		return direccionEstandar;
	}	
}