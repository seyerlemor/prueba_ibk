/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchDirecEstandarException;
import pe.com.ibk.pepper.model.DirecEstandar;
import pe.com.ibk.pepper.model.impl.DirecEstandarImpl;
import pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the direc estandar service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see DirecEstandarPersistence
 * @see DirecEstandarUtil
 * @generated
 */
public class DirecEstandarPersistenceImpl extends BasePersistenceImpl<DirecEstandar>
	implements DirecEstandarPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DirecEstandarUtil} to access the direc estandar persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DirecEstandarImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarModelImpl.FINDER_CACHE_ENABLED,
			DirecEstandarImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarModelImpl.FINDER_CACHE_ENABLED,
			DirecEstandarImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_DE_ID = new FinderPath(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarModelImpl.FINDER_CACHE_ENABLED,
			DirecEstandarImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByDE_ID",
			new String[] { Long.class.getName() },
			DirecEstandarModelImpl.IDDETALLEDIRECCION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DE_ID = new FinderPath(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDE_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the direc estandar where idDetalleDireccion = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchDirecEstandarException} if it could not be found.
	 *
	 * @param idDetalleDireccion the id detalle direccion
	 * @return the matching direc estandar
	 * @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a matching direc estandar could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar findByDE_ID(long idDetalleDireccion)
		throws NoSuchDirecEstandarException, SystemException {
		DirecEstandar direcEstandar = fetchByDE_ID(idDetalleDireccion);

		if (direcEstandar == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idDetalleDireccion=");
			msg.append(idDetalleDireccion);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDirecEstandarException(msg.toString());
		}

		return direcEstandar;
	}

	/**
	 * Returns the direc estandar where idDetalleDireccion = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idDetalleDireccion the id detalle direccion
	 * @return the matching direc estandar, or <code>null</code> if a matching direc estandar could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar fetchByDE_ID(long idDetalleDireccion)
		throws SystemException {
		return fetchByDE_ID(idDetalleDireccion, true);
	}

	/**
	 * Returns the direc estandar where idDetalleDireccion = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idDetalleDireccion the id detalle direccion
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching direc estandar, or <code>null</code> if a matching direc estandar could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar fetchByDE_ID(long idDetalleDireccion,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idDetalleDireccion };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DE_ID,
					finderArgs, this);
		}

		if (result instanceof DirecEstandar) {
			DirecEstandar direcEstandar = (DirecEstandar)result;

			if ((idDetalleDireccion != direcEstandar.getIdDetalleDireccion())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_DIRECESTANDAR_WHERE);

			query.append(_FINDER_COLUMN_DE_ID_IDDETALLEDIRECCION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idDetalleDireccion);

				List<DirecEstandar> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DE_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"DirecEstandarPersistenceImpl.fetchByDE_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					DirecEstandar direcEstandar = list.get(0);

					result = direcEstandar;

					cacheResult(direcEstandar);

					if ((direcEstandar.getIdDetalleDireccion() != idDetalleDireccion)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DE_ID,
							finderArgs, direcEstandar);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DE_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DirecEstandar)result;
		}
	}

	/**
	 * Removes the direc estandar where idDetalleDireccion = &#63; from the database.
	 *
	 * @param idDetalleDireccion the id detalle direccion
	 * @return the direc estandar that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar removeByDE_ID(long idDetalleDireccion)
		throws NoSuchDirecEstandarException, SystemException {
		DirecEstandar direcEstandar = findByDE_ID(idDetalleDireccion);

		return remove(direcEstandar);
	}

	/**
	 * Returns the number of direc estandars where idDetalleDireccion = &#63;.
	 *
	 * @param idDetalleDireccion the id detalle direccion
	 * @return the number of matching direc estandars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByDE_ID(long idDetalleDireccion) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DE_ID;

		Object[] finderArgs = new Object[] { idDetalleDireccion };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DIRECESTANDAR_WHERE);

			query.append(_FINDER_COLUMN_DE_ID_IDDETALLEDIRECCION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idDetalleDireccion);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DE_ID_IDDETALLEDIRECCION_2 = "direcEstandar.idDetalleDireccion = ?";

	public DirecEstandarPersistenceImpl() {
		setModelClass(DirecEstandar.class);
	}

	/**
	 * Caches the direc estandar in the entity cache if it is enabled.
	 *
	 * @param direcEstandar the direc estandar
	 */
	@Override
	public void cacheResult(DirecEstandar direcEstandar) {
		EntityCacheUtil.putResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarImpl.class, direcEstandar.getPrimaryKey(),
			direcEstandar);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DE_ID,
			new Object[] { direcEstandar.getIdDetalleDireccion() },
			direcEstandar);

		direcEstandar.resetOriginalValues();
	}

	/**
	 * Caches the direc estandars in the entity cache if it is enabled.
	 *
	 * @param direcEstandars the direc estandars
	 */
	@Override
	public void cacheResult(List<DirecEstandar> direcEstandars) {
		for (DirecEstandar direcEstandar : direcEstandars) {
			if (EntityCacheUtil.getResult(
						DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
						DirecEstandarImpl.class, direcEstandar.getPrimaryKey()) == null) {
				cacheResult(direcEstandar);
			}
			else {
				direcEstandar.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all direc estandars.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DirecEstandarImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DirecEstandarImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the direc estandar.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DirecEstandar direcEstandar) {
		EntityCacheUtil.removeResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarImpl.class, direcEstandar.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(direcEstandar);
	}

	@Override
	public void clearCache(List<DirecEstandar> direcEstandars) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DirecEstandar direcEstandar : direcEstandars) {
			EntityCacheUtil.removeResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
				DirecEstandarImpl.class, direcEstandar.getPrimaryKey());

			clearUniqueFindersCache(direcEstandar);
		}
	}

	protected void cacheUniqueFindersCache(DirecEstandar direcEstandar) {
		if (direcEstandar.isNew()) {
			Object[] args = new Object[] { direcEstandar.getIdDetalleDireccion() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DE_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DE_ID, args,
				direcEstandar);
		}
		else {
			DirecEstandarModelImpl direcEstandarModelImpl = (DirecEstandarModelImpl)direcEstandar;

			if ((direcEstandarModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DE_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						direcEstandar.getIdDetalleDireccion()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DE_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DE_ID, args,
					direcEstandar);
			}
		}
	}

	protected void clearUniqueFindersCache(DirecEstandar direcEstandar) {
		DirecEstandarModelImpl direcEstandarModelImpl = (DirecEstandarModelImpl)direcEstandar;

		Object[] args = new Object[] { direcEstandar.getIdDetalleDireccion() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DE_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DE_ID, args);

		if ((direcEstandarModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_DE_ID.getColumnBitmask()) != 0) {
			args = new Object[] {
					direcEstandarModelImpl.getOriginalIdDetalleDireccion()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DE_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DE_ID, args);
		}
	}

	/**
	 * Creates a new direc estandar with the primary key. Does not add the direc estandar to the database.
	 *
	 * @param idDetalleDireccion the primary key for the new direc estandar
	 * @return the new direc estandar
	 */
	@Override
	public DirecEstandar create(long idDetalleDireccion) {
		DirecEstandar direcEstandar = new DirecEstandarImpl();

		direcEstandar.setNew(true);
		direcEstandar.setPrimaryKey(idDetalleDireccion);

		return direcEstandar;
	}

	/**
	 * Removes the direc estandar with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idDetalleDireccion the primary key of the direc estandar
	 * @return the direc estandar that was removed
	 * @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a direc estandar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar remove(long idDetalleDireccion)
		throws NoSuchDirecEstandarException, SystemException {
		return remove((Serializable)idDetalleDireccion);
	}

	/**
	 * Removes the direc estandar with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the direc estandar
	 * @return the direc estandar that was removed
	 * @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a direc estandar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar remove(Serializable primaryKey)
		throws NoSuchDirecEstandarException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DirecEstandar direcEstandar = (DirecEstandar)session.get(DirecEstandarImpl.class,
					primaryKey);

			if (direcEstandar == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDirecEstandarException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(direcEstandar);
		}
		catch (NoSuchDirecEstandarException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DirecEstandar removeImpl(DirecEstandar direcEstandar)
		throws SystemException {
		direcEstandar = toUnwrappedModel(direcEstandar);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(direcEstandar)) {
				direcEstandar = (DirecEstandar)session.get(DirecEstandarImpl.class,
						direcEstandar.getPrimaryKeyObj());
			}

			if (direcEstandar != null) {
				session.delete(direcEstandar);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (direcEstandar != null) {
			clearCache(direcEstandar);
		}

		return direcEstandar;
	}

	@Override
	public DirecEstandar updateImpl(
		pe.com.ibk.pepper.model.DirecEstandar direcEstandar)
		throws SystemException {
		direcEstandar = toUnwrappedModel(direcEstandar);

		boolean isNew = direcEstandar.isNew();

		Session session = null;

		try {
			session = openSession();

			if (direcEstandar.isNew()) {
				session.save(direcEstandar);

				direcEstandar.setNew(false);
			}
			else {
				session.merge(direcEstandar);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DirecEstandarModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
			DirecEstandarImpl.class, direcEstandar.getPrimaryKey(),
			direcEstandar);

		clearUniqueFindersCache(direcEstandar);
		cacheUniqueFindersCache(direcEstandar);

		return direcEstandar;
	}

	protected DirecEstandar toUnwrappedModel(DirecEstandar direcEstandar) {
		if (direcEstandar instanceof DirecEstandarImpl) {
			return direcEstandar;
		}

		DirecEstandarImpl direcEstandarImpl = new DirecEstandarImpl();

		direcEstandarImpl.setNew(direcEstandar.isNew());
		direcEstandarImpl.setPrimaryKey(direcEstandar.getPrimaryKey());

		direcEstandarImpl.setIdDetalleDireccion(direcEstandar.getIdDetalleDireccion());
		direcEstandarImpl.setIdDireccion(direcEstandar.getIdDireccion());
		direcEstandarImpl.setTipoVia(direcEstandar.getTipoVia());
		direcEstandarImpl.setNombreVia(direcEstandar.getNombreVia());
		direcEstandarImpl.setNumero(direcEstandar.getNumero());
		direcEstandarImpl.setManzana(direcEstandar.getManzana());
		direcEstandarImpl.setPisoLote(direcEstandar.getPisoLote());
		direcEstandarImpl.setInterior(direcEstandar.getInterior());
		direcEstandarImpl.setUrbanizacion(direcEstandar.getUrbanizacion());
		direcEstandarImpl.setReferencia(direcEstandar.getReferencia());

		return direcEstandarImpl;
	}

	/**
	 * Returns the direc estandar with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the direc estandar
	 * @return the direc estandar
	 * @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a direc estandar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDirecEstandarException, SystemException {
		DirecEstandar direcEstandar = fetchByPrimaryKey(primaryKey);

		if (direcEstandar == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDirecEstandarException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return direcEstandar;
	}

	/**
	 * Returns the direc estandar with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchDirecEstandarException} if it could not be found.
	 *
	 * @param idDetalleDireccion the primary key of the direc estandar
	 * @return the direc estandar
	 * @throws pe.com.ibk.pepper.NoSuchDirecEstandarException if a direc estandar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar findByPrimaryKey(long idDetalleDireccion)
		throws NoSuchDirecEstandarException, SystemException {
		return findByPrimaryKey((Serializable)idDetalleDireccion);
	}

	/**
	 * Returns the direc estandar with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the direc estandar
	 * @return the direc estandar, or <code>null</code> if a direc estandar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		DirecEstandar direcEstandar = (DirecEstandar)EntityCacheUtil.getResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
				DirecEstandarImpl.class, primaryKey);

		if (direcEstandar == _nullDirecEstandar) {
			return null;
		}

		if (direcEstandar == null) {
			Session session = null;

			try {
				session = openSession();

				direcEstandar = (DirecEstandar)session.get(DirecEstandarImpl.class,
						primaryKey);

				if (direcEstandar != null) {
					cacheResult(direcEstandar);
				}
				else {
					EntityCacheUtil.putResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
						DirecEstandarImpl.class, primaryKey, _nullDirecEstandar);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(DirecEstandarModelImpl.ENTITY_CACHE_ENABLED,
					DirecEstandarImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return direcEstandar;
	}

	/**
	 * Returns the direc estandar with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idDetalleDireccion the primary key of the direc estandar
	 * @return the direc estandar, or <code>null</code> if a direc estandar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DirecEstandar fetchByPrimaryKey(long idDetalleDireccion)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idDetalleDireccion);
	}

	/**
	 * Returns all the direc estandars.
	 *
	 * @return the direc estandars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DirecEstandar> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the direc estandars.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of direc estandars
	 * @param end the upper bound of the range of direc estandars (not inclusive)
	 * @return the range of direc estandars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DirecEstandar> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the direc estandars.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.DirecEstandarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of direc estandars
	 * @param end the upper bound of the range of direc estandars (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of direc estandars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<DirecEstandar> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DirecEstandar> list = (List<DirecEstandar>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DIRECESTANDAR);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DIRECESTANDAR;

				if (pagination) {
					sql = sql.concat(DirecEstandarModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DirecEstandar>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<DirecEstandar>(list);
				}
				else {
					list = (List<DirecEstandar>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the direc estandars from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (DirecEstandar direcEstandar : findAll()) {
			remove(direcEstandar);
		}
	}

	/**
	 * Returns the number of direc estandars.
	 *
	 * @return the number of direc estandars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DIRECESTANDAR);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the direc estandar persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.DirecEstandar")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DirecEstandar>> listenersList = new ArrayList<ModelListener<DirecEstandar>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DirecEstandar>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DirecEstandarImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_DIRECESTANDAR = "SELECT direcEstandar FROM DirecEstandar direcEstandar";
	private static final String _SQL_SELECT_DIRECESTANDAR_WHERE = "SELECT direcEstandar FROM DirecEstandar direcEstandar WHERE ";
	private static final String _SQL_COUNT_DIRECESTANDAR = "SELECT COUNT(direcEstandar) FROM DirecEstandar direcEstandar";
	private static final String _SQL_COUNT_DIRECESTANDAR_WHERE = "SELECT COUNT(direcEstandar) FROM DirecEstandar direcEstandar WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "direcEstandar.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DirecEstandar exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DirecEstandar exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DirecEstandarPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idDetalleDireccion", "idDireccion", "tipoVia", "nombreVia",
				"numero", "manzana", "pisoLote", "interior", "urbanizacion",
				"referencia"
			});
	private static DirecEstandar _nullDirecEstandar = new DirecEstandarImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DirecEstandar> toCacheModel() {
				return _nullDirecEstandarCacheModel;
			}
		};

	private static CacheModel<DirecEstandar> _nullDirecEstandarCacheModel = new CacheModel<DirecEstandar>() {
			@Override
			public DirecEstandar toEntityModel() {
				return _nullDirecEstandar;
			}
		};
}