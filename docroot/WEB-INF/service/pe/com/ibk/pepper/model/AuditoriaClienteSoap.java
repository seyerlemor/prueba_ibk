/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class AuditoriaClienteSoap implements Serializable {
	public static AuditoriaClienteSoap toSoapModel(AuditoriaCliente model) {
		AuditoriaClienteSoap soapModel = new AuditoriaClienteSoap();

		soapModel.setIdAuditoriaCliente(model.getIdAuditoriaCliente());
		soapModel.setIdClienteTransaccion(model.getIdClienteTransaccion());
		soapModel.setPaso(model.getPaso());
		soapModel.setPagina(model.getPagina());
		soapModel.setTipoFlujo(model.getTipoFlujo());
		soapModel.setStrJson(model.getStrJson());
		soapModel.setFechaRegistro(model.getFechaRegistro());

		return soapModel;
	}

	public static AuditoriaClienteSoap[] toSoapModels(AuditoriaCliente[] models) {
		AuditoriaClienteSoap[] soapModels = new AuditoriaClienteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AuditoriaClienteSoap[][] toSoapModels(
		AuditoriaCliente[][] models) {
		AuditoriaClienteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AuditoriaClienteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AuditoriaClienteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AuditoriaClienteSoap[] toSoapModels(
		List<AuditoriaCliente> models) {
		List<AuditoriaClienteSoap> soapModels = new ArrayList<AuditoriaClienteSoap>(models.size());

		for (AuditoriaCliente model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AuditoriaClienteSoap[soapModels.size()]);
	}

	public AuditoriaClienteSoap() {
	}

	public long getPrimaryKey() {
		return _idAuditoriaCliente;
	}

	public void setPrimaryKey(long pk) {
		setIdAuditoriaCliente(pk);
	}

	public long getIdAuditoriaCliente() {
		return _idAuditoriaCliente;
	}

	public void setIdAuditoriaCliente(long idAuditoriaCliente) {
		_idAuditoriaCliente = idAuditoriaCliente;
	}

	public long getIdClienteTransaccion() {
		return _idClienteTransaccion;
	}

	public void setIdClienteTransaccion(long idClienteTransaccion) {
		_idClienteTransaccion = idClienteTransaccion;
	}

	public String getPaso() {
		return _paso;
	}

	public void setPaso(String paso) {
		_paso = paso;
	}

	public String getPagina() {
		return _pagina;
	}

	public void setPagina(String pagina) {
		_pagina = pagina;
	}

	public String getTipoFlujo() {
		return _tipoFlujo;
	}

	public void setTipoFlujo(String tipoFlujo) {
		_tipoFlujo = tipoFlujo;
	}

	public String getStrJson() {
		return _strJson;
	}

	public void setStrJson(String strJson) {
		_strJson = strJson;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	private long _idAuditoriaCliente;
	private long _idClienteTransaccion;
	private String _paso;
	private String _pagina;
	private String _tipoFlujo;
	private String _strJson;
	private Date _fechaRegistro;
}