/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ParametroHijoPlugin}.
 * </p>
 *
 * @author Interbank
 * @see ParametroHijoPlugin
 * @generated
 */
public class ParametroHijoPluginWrapper implements ParametroHijoPlugin,
	ModelWrapper<ParametroHijoPlugin> {
	public ParametroHijoPluginWrapper(ParametroHijoPlugin parametroHijoPlugin) {
		_parametroHijoPlugin = parametroHijoPlugin;
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroHijoPlugin.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroHijoPlugin.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroHijo", getIdParametroHijo());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("codigo", getCodigo());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("orden", getOrden());
		attributes.put("dato1", getDato1());
		attributes.put("dato2", getDato2());
		attributes.put("dato3", getDato3());
		attributes.put("dato4", getDato4());
		attributes.put("dato5", getDato5());
		attributes.put("dato6", getDato6());
		attributes.put("dato7", getDato7());
		attributes.put("dato8", getDato8());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroHijo = (Long)attributes.get("idParametroHijo");

		if (idParametroHijo != null) {
			setIdParametroHijo(idParametroHijo);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		Integer orden = (Integer)attributes.get("orden");

		if (orden != null) {
			setOrden(orden);
		}

		String dato1 = (String)attributes.get("dato1");

		if (dato1 != null) {
			setDato1(dato1);
		}

		String dato2 = (String)attributes.get("dato2");

		if (dato2 != null) {
			setDato2(dato2);
		}

		String dato3 = (String)attributes.get("dato3");

		if (dato3 != null) {
			setDato3(dato3);
		}

		String dato4 = (String)attributes.get("dato4");

		if (dato4 != null) {
			setDato4(dato4);
		}

		String dato5 = (String)attributes.get("dato5");

		if (dato5 != null) {
			setDato5(dato5);
		}

		String dato6 = (String)attributes.get("dato6");

		if (dato6 != null) {
			setDato6(dato6);
		}

		String dato7 = (String)attributes.get("dato7");

		if (dato7 != null) {
			setDato7(dato7);
		}

		String dato8 = (String)attributes.get("dato8");

		if (dato8 != null) {
			setDato8(dato8);
		}
	}

	/**
	* Returns the primary key of this parametro hijo plugin.
	*
	* @return the primary key of this parametro hijo plugin
	*/
	@Override
	public long getPrimaryKey() {
		return _parametroHijoPlugin.getPrimaryKey();
	}

	/**
	* Sets the primary key of this parametro hijo plugin.
	*
	* @param primaryKey the primary key of this parametro hijo plugin
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_parametroHijoPlugin.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id parametro hijo of this parametro hijo plugin.
	*
	* @return the id parametro hijo of this parametro hijo plugin
	*/
	@Override
	public long getIdParametroHijo() {
		return _parametroHijoPlugin.getIdParametroHijo();
	}

	/**
	* Sets the id parametro hijo of this parametro hijo plugin.
	*
	* @param idParametroHijo the id parametro hijo of this parametro hijo plugin
	*/
	@Override
	public void setIdParametroHijo(long idParametroHijo) {
		_parametroHijoPlugin.setIdParametroHijo(idParametroHijo);
	}

	/**
	* Returns the group ID of this parametro hijo plugin.
	*
	* @return the group ID of this parametro hijo plugin
	*/
	@Override
	public long getGroupId() {
		return _parametroHijoPlugin.getGroupId();
	}

	/**
	* Sets the group ID of this parametro hijo plugin.
	*
	* @param groupId the group ID of this parametro hijo plugin
	*/
	@Override
	public void setGroupId(long groupId) {
		_parametroHijoPlugin.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this parametro hijo plugin.
	*
	* @return the company ID of this parametro hijo plugin
	*/
	@Override
	public long getCompanyId() {
		return _parametroHijoPlugin.getCompanyId();
	}

	/**
	* Sets the company ID of this parametro hijo plugin.
	*
	* @param companyId the company ID of this parametro hijo plugin
	*/
	@Override
	public void setCompanyId(long companyId) {
		_parametroHijoPlugin.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this parametro hijo plugin.
	*
	* @return the user ID of this parametro hijo plugin
	*/
	@Override
	public long getUserId() {
		return _parametroHijoPlugin.getUserId();
	}

	/**
	* Sets the user ID of this parametro hijo plugin.
	*
	* @param userId the user ID of this parametro hijo plugin
	*/
	@Override
	public void setUserId(long userId) {
		_parametroHijoPlugin.setUserId(userId);
	}

	/**
	* Returns the user uuid of this parametro hijo plugin.
	*
	* @return the user uuid of this parametro hijo plugin
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroHijoPlugin.getUserUuid();
	}

	/**
	* Sets the user uuid of this parametro hijo plugin.
	*
	* @param userUuid the user uuid of this parametro hijo plugin
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_parametroHijoPlugin.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this parametro hijo plugin.
	*
	* @return the user name of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getUserName() {
		return _parametroHijoPlugin.getUserName();
	}

	/**
	* Sets the user name of this parametro hijo plugin.
	*
	* @param userName the user name of this parametro hijo plugin
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_parametroHijoPlugin.setUserName(userName);
	}

	/**
	* Returns the create date of this parametro hijo plugin.
	*
	* @return the create date of this parametro hijo plugin
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _parametroHijoPlugin.getCreateDate();
	}

	/**
	* Sets the create date of this parametro hijo plugin.
	*
	* @param createDate the create date of this parametro hijo plugin
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_parametroHijoPlugin.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this parametro hijo plugin.
	*
	* @return the modified date of this parametro hijo plugin
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _parametroHijoPlugin.getModifiedDate();
	}

	/**
	* Sets the modified date of this parametro hijo plugin.
	*
	* @param modifiedDate the modified date of this parametro hijo plugin
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_parametroHijoPlugin.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the codigo padre of this parametro hijo plugin.
	*
	* @return the codigo padre of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getCodigoPadre() {
		return _parametroHijoPlugin.getCodigoPadre();
	}

	/**
	* Sets the codigo padre of this parametro hijo plugin.
	*
	* @param codigoPadre the codigo padre of this parametro hijo plugin
	*/
	@Override
	public void setCodigoPadre(java.lang.String codigoPadre) {
		_parametroHijoPlugin.setCodigoPadre(codigoPadre);
	}

	/**
	* Returns the codigo of this parametro hijo plugin.
	*
	* @return the codigo of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getCodigo() {
		return _parametroHijoPlugin.getCodigo();
	}

	/**
	* Sets the codigo of this parametro hijo plugin.
	*
	* @param codigo the codigo of this parametro hijo plugin
	*/
	@Override
	public void setCodigo(java.lang.String codigo) {
		_parametroHijoPlugin.setCodigo(codigo);
	}

	/**
	* Returns the nombre of this parametro hijo plugin.
	*
	* @return the nombre of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getNombre() {
		return _parametroHijoPlugin.getNombre();
	}

	/**
	* Sets the nombre of this parametro hijo plugin.
	*
	* @param nombre the nombre of this parametro hijo plugin
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_parametroHijoPlugin.setNombre(nombre);
	}

	/**
	* Returns the descripcion of this parametro hijo plugin.
	*
	* @return the descripcion of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _parametroHijoPlugin.getDescripcion();
	}

	/**
	* Sets the descripcion of this parametro hijo plugin.
	*
	* @param descripcion the descripcion of this parametro hijo plugin
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_parametroHijoPlugin.setDescripcion(descripcion);
	}

	/**
	* Returns the estado of this parametro hijo plugin.
	*
	* @return the estado of this parametro hijo plugin
	*/
	@Override
	public boolean getEstado() {
		return _parametroHijoPlugin.getEstado();
	}

	/**
	* Returns <code>true</code> if this parametro hijo plugin is estado.
	*
	* @return <code>true</code> if this parametro hijo plugin is estado; <code>false</code> otherwise
	*/
	@Override
	public boolean isEstado() {
		return _parametroHijoPlugin.isEstado();
	}

	/**
	* Sets whether this parametro hijo plugin is estado.
	*
	* @param estado the estado of this parametro hijo plugin
	*/
	@Override
	public void setEstado(boolean estado) {
		_parametroHijoPlugin.setEstado(estado);
	}

	/**
	* Returns the orden of this parametro hijo plugin.
	*
	* @return the orden of this parametro hijo plugin
	*/
	@Override
	public int getOrden() {
		return _parametroHijoPlugin.getOrden();
	}

	/**
	* Sets the orden of this parametro hijo plugin.
	*
	* @param orden the orden of this parametro hijo plugin
	*/
	@Override
	public void setOrden(int orden) {
		_parametroHijoPlugin.setOrden(orden);
	}

	/**
	* Returns the dato1 of this parametro hijo plugin.
	*
	* @return the dato1 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato1() {
		return _parametroHijoPlugin.getDato1();
	}

	/**
	* Sets the dato1 of this parametro hijo plugin.
	*
	* @param dato1 the dato1 of this parametro hijo plugin
	*/
	@Override
	public void setDato1(java.lang.String dato1) {
		_parametroHijoPlugin.setDato1(dato1);
	}

	/**
	* Returns the dato2 of this parametro hijo plugin.
	*
	* @return the dato2 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato2() {
		return _parametroHijoPlugin.getDato2();
	}

	/**
	* Sets the dato2 of this parametro hijo plugin.
	*
	* @param dato2 the dato2 of this parametro hijo plugin
	*/
	@Override
	public void setDato2(java.lang.String dato2) {
		_parametroHijoPlugin.setDato2(dato2);
	}

	/**
	* Returns the dato3 of this parametro hijo plugin.
	*
	* @return the dato3 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato3() {
		return _parametroHijoPlugin.getDato3();
	}

	/**
	* Sets the dato3 of this parametro hijo plugin.
	*
	* @param dato3 the dato3 of this parametro hijo plugin
	*/
	@Override
	public void setDato3(java.lang.String dato3) {
		_parametroHijoPlugin.setDato3(dato3);
	}

	/**
	* Returns the dato4 of this parametro hijo plugin.
	*
	* @return the dato4 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato4() {
		return _parametroHijoPlugin.getDato4();
	}

	/**
	* Sets the dato4 of this parametro hijo plugin.
	*
	* @param dato4 the dato4 of this parametro hijo plugin
	*/
	@Override
	public void setDato4(java.lang.String dato4) {
		_parametroHijoPlugin.setDato4(dato4);
	}

	/**
	* Returns the dato5 of this parametro hijo plugin.
	*
	* @return the dato5 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato5() {
		return _parametroHijoPlugin.getDato5();
	}

	/**
	* Sets the dato5 of this parametro hijo plugin.
	*
	* @param dato5 the dato5 of this parametro hijo plugin
	*/
	@Override
	public void setDato5(java.lang.String dato5) {
		_parametroHijoPlugin.setDato5(dato5);
	}

	/**
	* Returns the dato6 of this parametro hijo plugin.
	*
	* @return the dato6 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato6() {
		return _parametroHijoPlugin.getDato6();
	}

	/**
	* Sets the dato6 of this parametro hijo plugin.
	*
	* @param dato6 the dato6 of this parametro hijo plugin
	*/
	@Override
	public void setDato6(java.lang.String dato6) {
		_parametroHijoPlugin.setDato6(dato6);
	}

	/**
	* Returns the dato7 of this parametro hijo plugin.
	*
	* @return the dato7 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato7() {
		return _parametroHijoPlugin.getDato7();
	}

	/**
	* Sets the dato7 of this parametro hijo plugin.
	*
	* @param dato7 the dato7 of this parametro hijo plugin
	*/
	@Override
	public void setDato7(java.lang.String dato7) {
		_parametroHijoPlugin.setDato7(dato7);
	}

	/**
	* Returns the dato8 of this parametro hijo plugin.
	*
	* @return the dato8 of this parametro hijo plugin
	*/
	@Override
	public java.lang.String getDato8() {
		return _parametroHijoPlugin.getDato8();
	}

	/**
	* Sets the dato8 of this parametro hijo plugin.
	*
	* @param dato8 the dato8 of this parametro hijo plugin
	*/
	@Override
	public void setDato8(java.lang.String dato8) {
		_parametroHijoPlugin.setDato8(dato8);
	}

	@Override
	public boolean isNew() {
		return _parametroHijoPlugin.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_parametroHijoPlugin.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _parametroHijoPlugin.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_parametroHijoPlugin.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _parametroHijoPlugin.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _parametroHijoPlugin.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_parametroHijoPlugin.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _parametroHijoPlugin.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_parametroHijoPlugin.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_parametroHijoPlugin.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_parametroHijoPlugin.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ParametroHijoPluginWrapper((ParametroHijoPlugin)_parametroHijoPlugin.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ParametroHijoPlugin parametroHijoPlugin) {
		return _parametroHijoPlugin.compareTo(parametroHijoPlugin);
	}

	@Override
	public int hashCode() {
		return _parametroHijoPlugin.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.ParametroHijoPlugin> toCacheModel() {
		return _parametroHijoPlugin.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroHijoPlugin toEscapedModel() {
		return new ParametroHijoPluginWrapper(_parametroHijoPlugin.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroHijoPlugin toUnescapedModel() {
		return new ParametroHijoPluginWrapper(_parametroHijoPlugin.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _parametroHijoPlugin.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _parametroHijoPlugin.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_parametroHijoPlugin.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroHijoPluginWrapper)) {
			return false;
		}

		ParametroHijoPluginWrapper parametroHijoPluginWrapper = (ParametroHijoPluginWrapper)obj;

		if (Validator.equals(_parametroHijoPlugin,
					parametroHijoPluginWrapper._parametroHijoPlugin)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ParametroHijoPlugin getWrappedParametroHijoPlugin() {
		return _parametroHijoPlugin;
	}

	@Override
	public ParametroHijoPlugin getWrappedModel() {
		return _parametroHijoPlugin;
	}

	@Override
	public void resetOriginalValues() {
		_parametroHijoPlugin.resetOriginalValues();
	}

	private ParametroHijoPlugin _parametroHijoPlugin;
}