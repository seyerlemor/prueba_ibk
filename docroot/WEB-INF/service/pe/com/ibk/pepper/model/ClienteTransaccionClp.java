/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClienteTransaccionLocalServiceUtil;
import pe.com.ibk.pepper.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class ClienteTransaccionClp extends BaseModelImpl<ClienteTransaccion>
	implements ClienteTransaccion {
	public ClienteTransaccionClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ClienteTransaccion.class;
	}

	@Override
	public String getModelClassName() {
		return ClienteTransaccion.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idClienteTransaccion;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdClienteTransaccion(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idClienteTransaccion;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idClienteTransaccion", getIdClienteTransaccion());
		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("estado", getEstado());
		attributes.put("paso", getPaso());
		attributes.put("pagina", getPagina());
		attributes.put("tipoFlujo", getTipoFlujo());
		attributes.put("strJson", getStrJson());
		attributes.put("restauracion", getRestauracion());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idClienteTransaccion = (Long)attributes.get("idClienteTransaccion");

		if (idClienteTransaccion != null) {
			setIdClienteTransaccion(idClienteTransaccion);
		}

		Long idUsuarioSession = (Long)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String estado = (String)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String paso = (String)attributes.get("paso");

		if (paso != null) {
			setPaso(paso);
		}

		String pagina = (String)attributes.get("pagina");

		if (pagina != null) {
			setPagina(pagina);
		}

		String tipoFlujo = (String)attributes.get("tipoFlujo");

		if (tipoFlujo != null) {
			setTipoFlujo(tipoFlujo);
		}

		String strJson = (String)attributes.get("strJson");

		if (strJson != null) {
			setStrJson(strJson);
		}

		String restauracion = (String)attributes.get("restauracion");

		if (restauracion != null) {
			setRestauracion(restauracion);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	@Override
	public long getIdClienteTransaccion() {
		return _idClienteTransaccion;
	}

	@Override
	public void setIdClienteTransaccion(long idClienteTransaccion) {
		_idClienteTransaccion = idClienteTransaccion;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdClienteTransaccion",
						long.class);

				method.invoke(_clienteTransaccionRemoteModel,
					idClienteTransaccion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdUsuarioSession() {
		return _idUsuarioSession;
	}

	@Override
	public void setIdUsuarioSession(long idUsuarioSession) {
		_idUsuarioSession = idUsuarioSession;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdUsuarioSession",
						long.class);

				method.invoke(_clienteTransaccionRemoteModel, idUsuarioSession);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdExpediente() {
		return _idExpediente;
	}

	@Override
	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdExpediente", long.class);

				method.invoke(_clienteTransaccionRemoteModel, idExpediente);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEstado() {
		return _estado;
	}

	@Override
	public void setEstado(String estado) {
		_estado = estado;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setEstado", String.class);

				method.invoke(_clienteTransaccionRemoteModel, estado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPaso() {
		return _paso;
	}

	@Override
	public void setPaso(String paso) {
		_paso = paso;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setPaso", String.class);

				method.invoke(_clienteTransaccionRemoteModel, paso);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPagina() {
		return _pagina;
	}

	@Override
	public void setPagina(String pagina) {
		_pagina = pagina;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setPagina", String.class);

				method.invoke(_clienteTransaccionRemoteModel, pagina);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTipoFlujo() {
		return _tipoFlujo;
	}

	@Override
	public void setTipoFlujo(String tipoFlujo) {
		_tipoFlujo = tipoFlujo;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setTipoFlujo", String.class);

				method.invoke(_clienteTransaccionRemoteModel, tipoFlujo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStrJson() {
		return _strJson;
	}

	@Override
	public void setStrJson(String strJson) {
		_strJson = strJson;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setStrJson", String.class);

				method.invoke(_clienteTransaccionRemoteModel, strJson);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRestauracion() {
		return _restauracion;
	}

	@Override
	public void setRestauracion(String restauracion) {
		_restauracion = restauracion;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setRestauracion", String.class);

				method.invoke(_clienteTransaccionRemoteModel, restauracion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	@Override
	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;

		if (_clienteTransaccionRemoteModel != null) {
			try {
				Class<?> clazz = _clienteTransaccionRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaRegistro", Date.class);

				method.invoke(_clienteTransaccionRemoteModel, fechaRegistro);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getClienteTransaccionRemoteModel() {
		return _clienteTransaccionRemoteModel;
	}

	public void setClienteTransaccionRemoteModel(
		BaseModel<?> clienteTransaccionRemoteModel) {
		_clienteTransaccionRemoteModel = clienteTransaccionRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clienteTransaccionRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clienteTransaccionRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ClienteTransaccionLocalServiceUtil.addClienteTransaccion(this);
		}
		else {
			ClienteTransaccionLocalServiceUtil.updateClienteTransaccion(this);
		}
	}

	@Override
	public ClienteTransaccion toEscapedModel() {
		return (ClienteTransaccion)ProxyUtil.newProxyInstance(ClienteTransaccion.class.getClassLoader(),
			new Class[] { ClienteTransaccion.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ClienteTransaccionClp clone = new ClienteTransaccionClp();

		clone.setIdClienteTransaccion(getIdClienteTransaccion());
		clone.setIdUsuarioSession(getIdUsuarioSession());
		clone.setIdExpediente(getIdExpediente());
		clone.setEstado(getEstado());
		clone.setPaso(getPaso());
		clone.setPagina(getPagina());
		clone.setTipoFlujo(getTipoFlujo());
		clone.setStrJson(getStrJson());
		clone.setRestauracion(getRestauracion());
		clone.setFechaRegistro(getFechaRegistro());

		return clone;
	}

	@Override
	public int compareTo(ClienteTransaccion clienteTransaccion) {
		long primaryKey = clienteTransaccion.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClienteTransaccionClp)) {
			return false;
		}

		ClienteTransaccionClp clienteTransaccion = (ClienteTransaccionClp)obj;

		long primaryKey = clienteTransaccion.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{idClienteTransaccion=");
		sb.append(getIdClienteTransaccion());
		sb.append(", idUsuarioSession=");
		sb.append(getIdUsuarioSession());
		sb.append(", idExpediente=");
		sb.append(getIdExpediente());
		sb.append(", estado=");
		sb.append(getEstado());
		sb.append(", paso=");
		sb.append(getPaso());
		sb.append(", pagina=");
		sb.append(getPagina());
		sb.append(", tipoFlujo=");
		sb.append(getTipoFlujo());
		sb.append(", strJson=");
		sb.append(getStrJson());
		sb.append(", restauracion=");
		sb.append(getRestauracion());
		sb.append(", fechaRegistro=");
		sb.append(getFechaRegistro());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.ClienteTransaccion");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idClienteTransaccion</column-name><column-value><![CDATA[");
		sb.append(getIdClienteTransaccion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idUsuarioSession</column-name><column-value><![CDATA[");
		sb.append(getIdUsuarioSession());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idExpediente</column-name><column-value><![CDATA[");
		sb.append(getIdExpediente());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estado</column-name><column-value><![CDATA[");
		sb.append(getEstado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>paso</column-name><column-value><![CDATA[");
		sb.append(getPaso());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>pagina</column-name><column-value><![CDATA[");
		sb.append(getPagina());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tipoFlujo</column-name><column-value><![CDATA[");
		sb.append(getTipoFlujo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>strJson</column-name><column-value><![CDATA[");
		sb.append(getStrJson());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>restauracion</column-name><column-value><![CDATA[");
		sb.append(getRestauracion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaRegistro</column-name><column-value><![CDATA[");
		sb.append(getFechaRegistro());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idClienteTransaccion;
	private long _idUsuarioSession;
	private long _idExpediente;
	private String _estado;
	private String _paso;
	private String _pagina;
	private String _tipoFlujo;
	private String _strJson;
	private String _restauracion;
	private Date _fechaRegistro;
	private BaseModel<?> _clienteTransaccionRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}