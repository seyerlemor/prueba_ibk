
package pe.com.ibk.pepper.rest.potc.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Generacion {

    private String dominio;
    private String tipoDocumento;
    private String numeroDocumento;
    private String flagNuevoCliente;
    private String accion;
    private String operador;
    private String numeroCelular;
    private String email;
    private List<Parametro> parametros;
    private AltaUsuario altaUsuario;
    private String flagNuevoProducto;
    private VinculacionProducto vinculacionProducto;
    private CrearPOTC crearPOTC;
    private String version;

    @JsonCreator
    public Generacion() {
		super();
	}

    @JsonProperty("dominio")
	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	@JsonProperty("tipoDocumento")
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@JsonProperty("numeroDocumento")
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@JsonProperty("flagNuevoCliente")
	public String getFlagNuevoCliente() {
		return flagNuevoCliente;
	}

	public void setFlagNuevoCliente(String flagNuevoCliente) {
		this.flagNuevoCliente = flagNuevoCliente;
	}

	@JsonProperty("accion")
	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@JsonProperty("operador")
	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	@JsonProperty("numeroCelular")
	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("parametros")
	public List<Parametro> getParametros() {
		return parametros;
	}

	public void setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
	}

	@JsonProperty("altaUsuario")
	public AltaUsuario getAltaUsuario() {
		return altaUsuario;
	}

	public void setAltaUsuario(AltaUsuario altaUsuario) {
		this.altaUsuario = altaUsuario;
	}

	@JsonProperty("flagNuevoProducto")
	public String getFlagNuevoProducto() {
		return flagNuevoProducto;
	}

	public void setFlagNuevoProducto(String flagNuevoProducto) {
		this.flagNuevoProducto = flagNuevoProducto;
	}

	@JsonProperty("vinculacionProducto")
	public VinculacionProducto getVinculacionProducto() {
		return vinculacionProducto;
	}

	public void setVinculacionProducto(VinculacionProducto vinculacionProducto) {
		this.vinculacionProducto = vinculacionProducto;
	}

	@JsonProperty("crearPOTC")
	public CrearPOTC getCrearPOTC() {
		return crearPOTC;
	}

	public void setCrearPOTC(CrearPOTC crearPOTC) {
		this.crearPOTC = crearPOTC;
	}

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
