/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import pe.com.ibk.pepper.model.AuditoriaCliente;

import java.util.List;

/**
 * The persistence utility for the auditoria cliente service. This utility wraps {@link AuditoriaClientePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see AuditoriaClientePersistence
 * @see AuditoriaClientePersistenceImpl
 * @generated
 */
public class AuditoriaClienteUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AuditoriaCliente auditoriaCliente) {
		getPersistence().clearCache(auditoriaCliente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AuditoriaCliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AuditoriaCliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AuditoriaCliente> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static AuditoriaCliente update(AuditoriaCliente auditoriaCliente)
		throws SystemException {
		return getPersistence().update(auditoriaCliente);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static AuditoriaCliente update(AuditoriaCliente auditoriaCliente,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(auditoriaCliente, serviceContext);
	}

	/**
	* Returns the auditoria cliente where idAuditoriaCliente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaClienteException} if it could not be found.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the matching auditoria cliente
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a matching auditoria cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente findByAC_ID(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException {
		return getPersistence().findByAC_ID(idAuditoriaCliente);
	}

	/**
	* Returns the auditoria cliente where idAuditoriaCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the matching auditoria cliente, or <code>null</code> if a matching auditoria cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente fetchByAC_ID(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByAC_ID(idAuditoriaCliente);
	}

	/**
	* Returns the auditoria cliente where idAuditoriaCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching auditoria cliente, or <code>null</code> if a matching auditoria cliente could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente fetchByAC_ID(
		long idAuditoriaCliente, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAC_ID(idAuditoriaCliente, retrieveFromCache);
	}

	/**
	* Removes the auditoria cliente where idAuditoriaCliente = &#63; from the database.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the auditoria cliente that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente removeByAC_ID(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException {
		return getPersistence().removeByAC_ID(idAuditoriaCliente);
	}

	/**
	* Returns the number of auditoria clientes where idAuditoriaCliente = &#63;.
	*
	* @param idAuditoriaCliente the id auditoria cliente
	* @return the number of matching auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAC_ID(long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByAC_ID(idAuditoriaCliente);
	}

	/**
	* Caches the auditoria cliente in the entity cache if it is enabled.
	*
	* @param auditoriaCliente the auditoria cliente
	*/
	public static void cacheResult(
		pe.com.ibk.pepper.model.AuditoriaCliente auditoriaCliente) {
		getPersistence().cacheResult(auditoriaCliente);
	}

	/**
	* Caches the auditoria clientes in the entity cache if it is enabled.
	*
	* @param auditoriaClientes the auditoria clientes
	*/
	public static void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> auditoriaClientes) {
		getPersistence().cacheResult(auditoriaClientes);
	}

	/**
	* Creates a new auditoria cliente with the primary key. Does not add the auditoria cliente to the database.
	*
	* @param idAuditoriaCliente the primary key for the new auditoria cliente
	* @return the new auditoria cliente
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente create(
		long idAuditoriaCliente) {
		return getPersistence().create(idAuditoriaCliente);
	}

	/**
	* Removes the auditoria cliente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idAuditoriaCliente the primary key of the auditoria cliente
	* @return the auditoria cliente that was removed
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente remove(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException {
		return getPersistence().remove(idAuditoriaCliente);
	}

	public static pe.com.ibk.pepper.model.AuditoriaCliente updateImpl(
		pe.com.ibk.pepper.model.AuditoriaCliente auditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(auditoriaCliente);
	}

	/**
	* Returns the auditoria cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaClienteException} if it could not be found.
	*
	* @param idAuditoriaCliente the primary key of the auditoria cliente
	* @return the auditoria cliente
	* @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente findByPrimaryKey(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchAuditoriaClienteException {
		return getPersistence().findByPrimaryKey(idAuditoriaCliente);
	}

	/**
	* Returns the auditoria cliente with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idAuditoriaCliente the primary key of the auditoria cliente
	* @return the auditoria cliente, or <code>null</code> if a auditoria cliente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.AuditoriaCliente fetchByPrimaryKey(
		long idAuditoriaCliente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idAuditoriaCliente);
	}

	/**
	* Returns all the auditoria clientes.
	*
	* @return the auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the auditoria clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria clientes
	* @param end the upper bound of the range of auditoria clientes (not inclusive)
	* @return the range of auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the auditoria clientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of auditoria clientes
	* @param end the upper bound of the range of auditoria clientes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.AuditoriaCliente> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the auditoria clientes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of auditoria clientes.
	*
	* @return the number of auditoria clientes
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AuditoriaClientePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AuditoriaClientePersistence)PortletBeanLocatorUtil.locate(pe.com.ibk.pepper.service.ClpSerializer.getServletContextName(),
					AuditoriaClientePersistence.class.getName());

			ReferenceRegistry.registerReference(AuditoriaClienteUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(AuditoriaClientePersistence persistence) {
	}

	private static AuditoriaClientePersistence _persistence;
}