/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchProductoException;
import pe.com.ibk.pepper.model.Producto;
import pe.com.ibk.pepper.model.impl.ProductoImpl;
import pe.com.ibk.pepper.model.impl.ProductoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the producto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ProductoPersistence
 * @see ProductoUtil
 * @generated
 */
public class ProductoPersistenceImpl extends BasePersistenceImpl<Producto>
	implements ProductoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductoUtil} to access the producto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, ProductoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, ProductoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_PC_ID = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, ProductoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByPC_ID",
			new String[] { Long.class.getName() },
			ProductoModelImpl.IDPRODUCTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PC_ID = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPC_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the producto where idProducto = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	 *
	 * @param idProducto the id producto
	 * @return the matching producto
	 * @throws pe.com.ibk.pepper.NoSuchProductoException if a matching producto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto findByPC_ID(long idProducto)
		throws NoSuchProductoException, SystemException {
		Producto producto = fetchByPC_ID(idProducto);

		if (producto == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idProducto=");
			msg.append(idProducto);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProductoException(msg.toString());
		}

		return producto;
	}

	/**
	 * Returns the producto where idProducto = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idProducto the id producto
	 * @return the matching producto, or <code>null</code> if a matching producto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto fetchByPC_ID(long idProducto) throws SystemException {
		return fetchByPC_ID(idProducto, true);
	}

	/**
	 * Returns the producto where idProducto = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idProducto the id producto
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching producto, or <code>null</code> if a matching producto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto fetchByPC_ID(long idProducto, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idProducto };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PC_ID,
					finderArgs, this);
		}

		if (result instanceof Producto) {
			Producto producto = (Producto)result;

			if ((idProducto != producto.getIdProducto())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PRODUCTO_WHERE);

			query.append(_FINDER_COLUMN_PC_ID_IDPRODUCTO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idProducto);

				List<Producto> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ProductoPersistenceImpl.fetchByPC_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Producto producto = list.get(0);

					result = producto;

					cacheResult(producto);

					if ((producto.getIdProducto() != idProducto)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_ID,
							finderArgs, producto);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PC_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Producto)result;
		}
	}

	/**
	 * Removes the producto where idProducto = &#63; from the database.
	 *
	 * @param idProducto the id producto
	 * @return the producto that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto removeByPC_ID(long idProducto)
		throws NoSuchProductoException, SystemException {
		Producto producto = findByPC_ID(idProducto);

		return remove(producto);
	}

	/**
	 * Returns the number of productos where idProducto = &#63;.
	 *
	 * @param idProducto the id producto
	 * @return the number of matching productos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPC_ID(long idProducto) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PC_ID;

		Object[] finderArgs = new Object[] { idProducto };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTO_WHERE);

			query.append(_FINDER_COLUMN_PC_ID_IDPRODUCTO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idProducto);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PC_ID_IDPRODUCTO_2 = "producto.idProducto = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_PC_E = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, ProductoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByPC_E",
			new String[] { Long.class.getName() },
			ProductoModelImpl.IDEXPEDIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PC_E = new FinderPath(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPC_E",
			new String[] { Long.class.getName() });

	/**
	 * Returns the producto where idExpediente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching producto
	 * @throws pe.com.ibk.pepper.NoSuchProductoException if a matching producto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto findByPC_E(long idExpediente)
		throws NoSuchProductoException, SystemException {
		Producto producto = fetchByPC_E(idExpediente);

		if (producto == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idExpediente=");
			msg.append(idExpediente);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProductoException(msg.toString());
		}

		return producto;
	}

	/**
	 * Returns the producto where idExpediente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idExpediente the id expediente
	 * @return the matching producto, or <code>null</code> if a matching producto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto fetchByPC_E(long idExpediente) throws SystemException {
		return fetchByPC_E(idExpediente, true);
	}

	/**
	 * Returns the producto where idExpediente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idExpediente the id expediente
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching producto, or <code>null</code> if a matching producto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto fetchByPC_E(long idExpediente, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idExpediente };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PC_E,
					finderArgs, this);
		}

		if (result instanceof Producto) {
			Producto producto = (Producto)result;

			if ((idExpediente != producto.getIdExpediente())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PRODUCTO_WHERE);

			query.append(_FINDER_COLUMN_PC_E_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				List<Producto> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_E,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ProductoPersistenceImpl.fetchByPC_E(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Producto producto = list.get(0);

					result = producto;

					cacheResult(producto);

					if ((producto.getIdExpediente() != idExpediente)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_E,
							finderArgs, producto);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PC_E,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Producto)result;
		}
	}

	/**
	 * Removes the producto where idExpediente = &#63; from the database.
	 *
	 * @param idExpediente the id expediente
	 * @return the producto that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto removeByPC_E(long idExpediente)
		throws NoSuchProductoException, SystemException {
		Producto producto = findByPC_E(idExpediente);

		return remove(producto);
	}

	/**
	 * Returns the number of productos where idExpediente = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @return the number of matching productos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPC_E(long idExpediente) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PC_E;

		Object[] finderArgs = new Object[] { idExpediente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTO_WHERE);

			query.append(_FINDER_COLUMN_PC_E_IDEXPEDIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PC_E_IDEXPEDIENTE_2 = "producto.idExpediente = ?";

	public ProductoPersistenceImpl() {
		setModelClass(Producto.class);
	}

	/**
	 * Caches the producto in the entity cache if it is enabled.
	 *
	 * @param producto the producto
	 */
	@Override
	public void cacheResult(Producto producto) {
		EntityCacheUtil.putResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoImpl.class, producto.getPrimaryKey(), producto);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_ID,
			new Object[] { producto.getIdProducto() }, producto);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_E,
			new Object[] { producto.getIdExpediente() }, producto);

		producto.resetOriginalValues();
	}

	/**
	 * Caches the productos in the entity cache if it is enabled.
	 *
	 * @param productos the productos
	 */
	@Override
	public void cacheResult(List<Producto> productos) {
		for (Producto producto : productos) {
			if (EntityCacheUtil.getResult(
						ProductoModelImpl.ENTITY_CACHE_ENABLED,
						ProductoImpl.class, producto.getPrimaryKey()) == null) {
				cacheResult(producto);
			}
			else {
				producto.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all productos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProductoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProductoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the producto.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Producto producto) {
		EntityCacheUtil.removeResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoImpl.class, producto.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(producto);
	}

	@Override
	public void clearCache(List<Producto> productos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Producto producto : productos) {
			EntityCacheUtil.removeResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
				ProductoImpl.class, producto.getPrimaryKey());

			clearUniqueFindersCache(producto);
		}
	}

	protected void cacheUniqueFindersCache(Producto producto) {
		if (producto.isNew()) {
			Object[] args = new Object[] { producto.getIdProducto() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PC_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_ID, args, producto);

			args = new Object[] { producto.getIdExpediente() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PC_E, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_E, args, producto);
		}
		else {
			ProductoModelImpl productoModelImpl = (ProductoModelImpl)producto;

			if ((productoModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PC_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { producto.getIdProducto() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PC_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_ID, args,
					producto);
			}

			if ((productoModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PC_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { producto.getIdExpediente() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PC_E, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PC_E, args,
					producto);
			}
		}
	}

	protected void clearUniqueFindersCache(Producto producto) {
		ProductoModelImpl productoModelImpl = (ProductoModelImpl)producto;

		Object[] args = new Object[] { producto.getIdProducto() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PC_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PC_ID, args);

		if ((productoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PC_ID.getColumnBitmask()) != 0) {
			args = new Object[] { productoModelImpl.getOriginalIdProducto() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PC_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PC_ID, args);
		}

		args = new Object[] { producto.getIdExpediente() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PC_E, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PC_E, args);

		if ((productoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PC_E.getColumnBitmask()) != 0) {
			args = new Object[] { productoModelImpl.getOriginalIdExpediente() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PC_E, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PC_E, args);
		}
	}

	/**
	 * Creates a new producto with the primary key. Does not add the producto to the database.
	 *
	 * @param idProducto the primary key for the new producto
	 * @return the new producto
	 */
	@Override
	public Producto create(long idProducto) {
		Producto producto = new ProductoImpl();

		producto.setNew(true);
		producto.setPrimaryKey(idProducto);

		return producto;
	}

	/**
	 * Removes the producto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idProducto the primary key of the producto
	 * @return the producto that was removed
	 * @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto remove(long idProducto)
		throws NoSuchProductoException, SystemException {
		return remove((Serializable)idProducto);
	}

	/**
	 * Removes the producto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the producto
	 * @return the producto that was removed
	 * @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto remove(Serializable primaryKey)
		throws NoSuchProductoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Producto producto = (Producto)session.get(ProductoImpl.class,
					primaryKey);

			if (producto == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(producto);
		}
		catch (NoSuchProductoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Producto removeImpl(Producto producto) throws SystemException {
		producto = toUnwrappedModel(producto);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(producto)) {
				producto = (Producto)session.get(ProductoImpl.class,
						producto.getPrimaryKeyObj());
			}

			if (producto != null) {
				session.delete(producto);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (producto != null) {
			clearCache(producto);
		}

		return producto;
	}

	@Override
	public Producto updateImpl(pe.com.ibk.pepper.model.Producto producto)
		throws SystemException {
		producto = toUnwrappedModel(producto);

		boolean isNew = producto.isNew();

		Session session = null;

		try {
			session = openSession();

			if (producto.isNew()) {
				session.save(producto);

				producto.setNew(false);
			}
			else {
				session.merge(producto);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProductoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
			ProductoImpl.class, producto.getPrimaryKey(), producto);

		clearUniqueFindersCache(producto);
		cacheUniqueFindersCache(producto);

		return producto;
	}

	protected Producto toUnwrappedModel(Producto producto) {
		if (producto instanceof ProductoImpl) {
			return producto;
		}

		ProductoImpl productoImpl = new ProductoImpl();

		productoImpl.setNew(producto.isNew());
		productoImpl.setPrimaryKey(producto.getPrimaryKey());

		productoImpl.setIdProducto(producto.getIdProducto());
		productoImpl.setIdExpediente(producto.getIdExpediente());
		productoImpl.setTipoProducto(producto.getTipoProducto());
		productoImpl.setCodigoProducto(producto.getCodigoProducto());
		productoImpl.setMoneda(producto.getMoneda());
		productoImpl.setMarcaProducto(producto.getMarcaProducto());
		productoImpl.setLineaCredito(producto.getLineaCredito());
		productoImpl.setDiaPago(producto.getDiaPago());
		productoImpl.setTasaProducto(producto.getTasaProducto());
		productoImpl.setNombreTitularProducto(producto.getNombreTitularProducto());
		productoImpl.setTipoSeguro(producto.getTipoSeguro());
		productoImpl.setCostoSeguro(producto.getCostoSeguro());
		productoImpl.setFechaRegistro(producto.getFechaRegistro());
		productoImpl.setFlagEnvioEECCFisico(producto.getFlagEnvioEECCFisico());
		productoImpl.setFlagEnvioEECCEmail(producto.getFlagEnvioEECCEmail());
		productoImpl.setFlagTerminos(producto.getFlagTerminos());
		productoImpl.setFlagCargoCompra(producto.getFlagCargoCompra());
		productoImpl.setClaveventa(producto.getClaveventa());
		productoImpl.setCodigoOperacion(producto.getCodigoOperacion());
		productoImpl.setFechaHoraOperacion(producto.getFechaHoraOperacion());
		productoImpl.setCodigoUnico(producto.getCodigoUnico());
		productoImpl.setNumTarjetaTitular(producto.getNumTarjetaTitular());
		productoImpl.setNumCuenta(producto.getNumCuenta());
		productoImpl.setFechaAltaTitular(producto.getFechaAltaTitular());
		productoImpl.setFechaVencTitular(producto.getFechaVencTitular());
		productoImpl.setNumTarjetaProvisional(producto.getNumTarjetaProvisional());
		productoImpl.setFechaAltaProvisional(producto.getFechaAltaProvisional());
		productoImpl.setFechaVencProvisional(producto.getFechaVencProvisional());
		productoImpl.setRazonNoUsoTarjeta(producto.getRazonNoUsoTarjeta());
		productoImpl.setMotivo(producto.getMotivo());

		return productoImpl;
	}

	/**
	 * Returns the producto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the producto
	 * @return the producto
	 * @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProductoException, SystemException {
		Producto producto = fetchByPrimaryKey(primaryKey);

		if (producto == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProductoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return producto;
	}

	/**
	 * Returns the producto with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchProductoException} if it could not be found.
	 *
	 * @param idProducto the primary key of the producto
	 * @return the producto
	 * @throws pe.com.ibk.pepper.NoSuchProductoException if a producto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto findByPrimaryKey(long idProducto)
		throws NoSuchProductoException, SystemException {
		return findByPrimaryKey((Serializable)idProducto);
	}

	/**
	 * Returns the producto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the producto
	 * @return the producto, or <code>null</code> if a producto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Producto producto = (Producto)EntityCacheUtil.getResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
				ProductoImpl.class, primaryKey);

		if (producto == _nullProducto) {
			return null;
		}

		if (producto == null) {
			Session session = null;

			try {
				session = openSession();

				producto = (Producto)session.get(ProductoImpl.class, primaryKey);

				if (producto != null) {
					cacheResult(producto);
				}
				else {
					EntityCacheUtil.putResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
						ProductoImpl.class, primaryKey, _nullProducto);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ProductoModelImpl.ENTITY_CACHE_ENABLED,
					ProductoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return producto;
	}

	/**
	 * Returns the producto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idProducto the primary key of the producto
	 * @return the producto, or <code>null</code> if a producto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Producto fetchByPrimaryKey(long idProducto)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idProducto);
	}

	/**
	 * Returns all the productos.
	 *
	 * @return the productos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Producto> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the productos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of productos
	 * @param end the upper bound of the range of productos (not inclusive)
	 * @return the range of productos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Producto> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the productos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of productos
	 * @param end the upper bound of the range of productos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of productos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Producto> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Producto> list = (List<Producto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRODUCTO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTO;

				if (pagination) {
					sql = sql.concat(ProductoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Producto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Producto>(list);
				}
				else {
					list = (List<Producto>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the productos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Producto producto : findAll()) {
			remove(producto);
		}
	}

	/**
	 * Returns the number of productos.
	 *
	 * @return the number of productos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the producto persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.Producto")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Producto>> listenersList = new ArrayList<ModelListener<Producto>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Producto>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProductoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PRODUCTO = "SELECT producto FROM Producto producto";
	private static final String _SQL_SELECT_PRODUCTO_WHERE = "SELECT producto FROM Producto producto WHERE ";
	private static final String _SQL_COUNT_PRODUCTO = "SELECT COUNT(producto) FROM Producto producto";
	private static final String _SQL_COUNT_PRODUCTO_WHERE = "SELECT COUNT(producto) FROM Producto producto WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "producto.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Producto exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Producto exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProductoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idProducto", "idExpediente", "tipoProducto", "codigoProducto",
				"moneda", "marcaProducto", "lineaCredito", "diaPago",
				"tasaProducto", "nombreTitularProducto", "tipoSeguro",
				"costoSeguro", "fechaRegistro", "flagEnvioEECCFisico",
				"flagEnvioEECCEmail", "flagTerminos", "flagCargoCompra",
				"claveventa", "codigoOperacion", "fechaHoraOperacion",
				"codigoUnico", "numTarjetaTitular", "numCuenta",
				"fechaAltaTitular", "fechaVencTitular", "numTarjetaProvisional",
				"fechaAltaProvisional", "fechaVencProvisional",
				"RazonNoUsoTarjeta", "Motivo"
			});
	private static Producto _nullProducto = new ProductoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Producto> toCacheModel() {
				return _nullProductoCacheModel;
			}
		};

	private static CacheModel<Producto> _nullProductoCacheModel = new CacheModel<Producto>() {
			@Override
			public Producto toEntityModel() {
				return _nullProducto;
			}
		};
}