/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CampaniaLocalService}.
 *
 * @author Interbank
 * @see CampaniaLocalService
 * @generated
 */
public class CampaniaLocalServiceWrapper implements CampaniaLocalService,
	ServiceWrapper<CampaniaLocalService> {
	public CampaniaLocalServiceWrapper(
		CampaniaLocalService campaniaLocalService) {
		_campaniaLocalService = campaniaLocalService;
	}

	/**
	* Adds the campania to the database. Also notifies the appropriate model listeners.
	*
	* @param campania the campania
	* @return the campania that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Campania addCampania(
		pe.com.ibk.pepper.model.Campania campania)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.addCampania(campania);
	}

	/**
	* Creates a new campania with the primary key. Does not add the campania to the database.
	*
	* @param idCampania the primary key for the new campania
	* @return the new campania
	*/
	@Override
	public pe.com.ibk.pepper.model.Campania createCampania(long idCampania) {
		return _campaniaLocalService.createCampania(idCampania);
	}

	/**
	* Deletes the campania with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCampania the primary key of the campania
	* @return the campania that was removed
	* @throws PortalException if a campania with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Campania deleteCampania(long idCampania)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.deleteCampania(idCampania);
	}

	/**
	* Deletes the campania from the database. Also notifies the appropriate model listeners.
	*
	* @param campania the campania
	* @return the campania that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Campania deleteCampania(
		pe.com.ibk.pepper.model.Campania campania)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.deleteCampania(campania);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _campaniaLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public pe.com.ibk.pepper.model.Campania fetchCampania(long idCampania)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.fetchCampania(idCampania);
	}

	/**
	* Returns the campania with the primary key.
	*
	* @param idCampania the primary key of the campania
	* @return the campania
	* @throws PortalException if a campania with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Campania getCampania(long idCampania)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.getCampania(idCampania);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the campanias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.CampaniaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of campanias
	* @param end the upper bound of the range of campanias (not inclusive)
	* @return the range of campanias
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.Campania> getCampanias(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.getCampanias(start, end);
	}

	/**
	* Returns the number of campanias.
	*
	* @return the number of campanias
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCampaniasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.getCampaniasCount();
	}

	/**
	* Updates the campania in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param campania the campania
	* @return the campania that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Campania updateCampania(
		pe.com.ibk.pepper.model.Campania campania)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.updateCampania(campania);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _campaniaLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_campaniaLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _campaniaLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public pe.com.ibk.pepper.model.Campania registrarCampania(
		pe.com.ibk.pepper.model.Campania campania)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.registrarCampania(campania);
	}

	@Override
	public pe.com.ibk.pepper.model.Campania buscarCampaniaporId(long idCampania) {
		return _campaniaLocalService.buscarCampaniaporId(idCampania);
	}

	@Override
	public java.util.List<pe.com.ibk.pepper.model.Campania> getCampaniasByExpediente(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _campaniaLocalService.getCampaniasByExpediente(idExpediente);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CampaniaLocalService getWrappedCampaniaLocalService() {
		return _campaniaLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCampaniaLocalService(
		CampaniaLocalService campaniaLocalService) {
		_campaniaLocalService = campaniaLocalService;
	}

	@Override
	public CampaniaLocalService getWrappedService() {
		return _campaniaLocalService;
	}

	@Override
	public void setWrappedService(CampaniaLocalService campaniaLocalService) {
		_campaniaLocalService = campaniaLocalService;
	}

	private CampaniaLocalService _campaniaLocalService;
}