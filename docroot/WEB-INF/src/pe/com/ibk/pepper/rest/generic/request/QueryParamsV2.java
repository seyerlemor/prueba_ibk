
package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QueryParamsV2 {

	/*API SIEBEL*/
    private String channelId;
    private String userId;
    
    /*ANTIFRAUDE*/
    private String ip;
    private String phoneNumber;
    private String email;
    
    /*API SIEBEL*/
    private String documentType;
    private String productId;
    
    /*2DA GENERACION API FECHA Y HORA*/
    private String fechaActual;
	private String tipoEmision;
	private String canal;
	private String tipoVenta;
	private String marcaTarjeta;
	private String tipoTarjeta;
	private String tipoMoneda;
	private String lineaCredito;
	private String codigoDepartamento;
	private String codigoProvincia;
	private String codigoDistrito;
	private String indicadorVentaDigital;
	
	/*3RA GENERACION API FECHA Y HORA*/
	private String issuanceType;
	private String channelCode;	
	private String saleType;
	private String cardBrand;
	private String cardType;
	private String currencyId;
	private String creditLine;
	private String ubigeoCode;
	private String isDigitalSale;
	/**/

	public QueryParamsV2(){
		super();
	}
	@JsonCreator
    public QueryParamsV2(String channelId, String userId) {
		super();
        this.channelId = channelId;
        this.userId = userId;
    }
	@JsonCreator
    public QueryParamsV2(String ip, String phoneNumber, String email) {
		super();
		this.ip = ip;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}
	@JsonCreator
    public QueryParamsV2(String channelId, String documentType, String productId, String siebel) {
		super();
        this.channelId = channelId;
        this.documentType = documentType;
        this.productId = productId;
    }
	@JsonCreator
    public QueryParamsV2(String fechaActual, String tipoEmision, String canal,
			String tipoVenta, String marcaTarjeta, String tipoTarjeta,
			String tipoMoneda, String lineaCredito, String codigoDepartamento,
			String codigoProvincia, String codigoDistrito,
			String indicadorVentaDigital) {
		super();
		this.fechaActual = fechaActual;
		this.tipoEmision = tipoEmision;
		this.canal = canal;
		this.tipoVenta = tipoVenta;
		this.marcaTarjeta = marcaTarjeta;
		this.tipoTarjeta = tipoTarjeta;
		this.tipoMoneda = tipoMoneda;
		this.lineaCredito = lineaCredito;
		this.codigoDepartamento = codigoDepartamento;
		this.codigoProvincia = codigoProvincia;
		this.codigoDistrito = codigoDistrito;
		this.indicadorVentaDigital = indicadorVentaDigital;
	}
	@JsonCreator
	public QueryParamsV2(String issuanceType,
			String channelCode, String saleType, String cardBrand,
			String cardType, String currencyId, String creditLine,
			String ubigeoCode, String isDigitalSale) {
		super();
		this.issuanceType = issuanceType;
		this.channelCode = channelCode;
		this.saleType = saleType;
		this.cardBrand = cardBrand;
		this.cardType = cardType;
		this.currencyId = currencyId;
		this.creditLine = creditLine;
		this.ubigeoCode = ubigeoCode;
		this.isDigitalSale = isDigitalSale;
	}
    
	@JsonCreator
	public QueryParamsV2(String documentType) {
		this.documentType = documentType;
	}
	@JsonProperty("channelId")
    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    @JsonProperty("userId")
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	@JsonProperty("ip")
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@JsonProperty("phoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@JsonProperty("documentType")
	public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    @JsonProperty("productId")
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
    @JsonProperty("fechaActual")
    public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	@JsonProperty("tipoEmision")
	public String getTipoEmision() {
		return tipoEmision;
	}
	public void setTipoEmision(String tipoEmision) {
		this.tipoEmision = tipoEmision;
	}
	@JsonProperty("canal")
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	@JsonProperty("tipoVenta")
	public String getTipoVenta() {
		return tipoVenta;
	}
	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}
	@JsonProperty("marcaTarjeta")
	public String getMarcaTarjeta() {
		return marcaTarjeta;
	}
	public void setMarcaTarjeta(String marcaTarjeta) {
		this.marcaTarjeta = marcaTarjeta;
	}
	@JsonProperty("tipoTarjeta")
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	@JsonProperty("tipoMoneda")
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	@JsonProperty("lineaCredito")
	public String getLineaCredito() {
		return lineaCredito;
	}
	public void setLineaCredito(String lineaCredito) {
		this.lineaCredito = lineaCredito;
	}
	@JsonProperty("codigoDepartamento")
	public String getCodigoDepartamento() {
		return codigoDepartamento;
	}
	public void setCodigoDepartamento(String codigoDepartamento) {
		this.codigoDepartamento = codigoDepartamento;
	}
	@JsonProperty("codigoProvincia")
	public String getCodigoProvincia() {
		return codigoProvincia;
	}
	public void setCodigoProvincia(String codigoProvincia) {
		this.codigoProvincia = codigoProvincia;
	}
	@JsonProperty("codigoDistrito")
	public String getCodigoDistrito() {
		return codigoDistrito;
	}
	public void setCodigoDistrito(String codigoDistrito) {
		this.codigoDistrito = codigoDistrito;
	}
	@JsonProperty("indicadorVentaDigital")
	public String getIndicadorVentaDigital() {
		return indicadorVentaDigital;
	}
	public void setIndicadorVentaDigital(String indicadorVentaDigital) {
		this.indicadorVentaDigital = indicadorVentaDigital;
	}
	@JsonProperty("issuanceType")
	public String getIssuanceType() {
		return issuanceType;
	}

	public void setIssuanceType(String issuanceType) {
		this.issuanceType = issuanceType;
	}
	@JsonProperty("channelCode")
	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	@JsonProperty("saleType")
	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}
	@JsonProperty("cardBrand")
	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}
	@JsonProperty("cardType")
	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	@JsonProperty("currencyId")
	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	@JsonProperty("creditLine")
	public String getCreditLine() {
		return creditLine;
	}

	public void setCreditLine(String creditLine) {
		this.creditLine = creditLine;
	}
	@JsonProperty("ubigeoCode")
	public String getUbigeoCode() {
		return ubigeoCode;
	}

	public void setUbigeoCode(String ubigeoCode) {
		this.ubigeoCode = ubigeoCode;
	}
	@JsonProperty("isDigitalSale")
	public String getIsDigitalSale() {
		return isDigitalSale;
	}

	public void setIsDigitalSale(String isDigitalSale) {
		this.isDigitalSale = isDigitalSale;
	}
}
