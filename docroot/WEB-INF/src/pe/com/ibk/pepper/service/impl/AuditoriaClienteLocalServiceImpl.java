/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;

import java.util.Date;

import pe.com.ibk.pepper.NoSuchAuditoriaClienteException;
import pe.com.ibk.pepper.model.AuditoriaCliente;
import pe.com.ibk.pepper.service.base.AuditoriaClienteLocalServiceBaseImpl;

/**
 * The implementation of the auditoria cliente local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link pe.com.ibk.pepper.service.AuditoriaClienteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Interbank
 * @see pe.com.ibk.pepper.service.base.AuditoriaClienteLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.AuditoriaClienteLocalServiceUtil
 */
public class AuditoriaClienteLocalServiceImpl
	extends AuditoriaClienteLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link pe.com.ibk.pepper.service.AuditoriaClienteLocalServiceUtil} to access the auditoria cliente local service.
	 */
	
	private static final Log _log = LogFactoryUtil.getLog(AuditoriaClienteLocalServiceImpl.class);
	
	@Override
	public AuditoriaCliente registrarAuditoriaCliente(AuditoriaCliente auditoriaCliente) throws SystemException{
		Date now = DateUtil.newDate();
		if (auditoriaCliente.isNew()) {
			auditoriaCliente.setFechaRegistro(now);
		}
		auditoriaClientePersistence.update(auditoriaCliente);
		return auditoriaCliente;
	}	

	@Override
	public AuditoriaCliente buscarAuditoriaClienteporId(long idAuditoriaCliente){
		AuditoriaCliente auditoriaCliente = null;
		try {
			auditoriaCliente = auditoriaClientePersistence.findByAC_ID(idAuditoriaCliente);
		} catch (NoSuchAuditoriaClienteException e) {
			_log.error("Error en clase AuditoriaClienteLocalServiceImpl metodo buscarAuditoriaClienteporId:: NO EXISTE EN AUDITORIA CLIENTE ", e );
		} catch (SystemException e) {
			_log.error("Error en clase AuditoriaClienteLocalServiceImpl metodo buscarAuditoriaClienteporId:: ", e );
		}
		return auditoriaCliente;
	}			
	
	
}