<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ include file="/html/analytics.jsp"%>

<liferay-theme:defineObjects />
<portlet:resourceURL id="enviarHome" var="enviarHome" />
<portlet:defineObjects />
       
<div class="lq-container">
	<div class="lh-ff">
		<div class="lh-ff-block lh-ff-block--active">
			<div class="lq-row">
				<div class="lh-ff-title-col">
				<h3 class="lh-ff-title-col__title lh-typo__sectitle lh-typo__sectitle--2">Ya cuentas con una tarjeta de crédito.</h3>
				</div>
				
				<div class="lh-ff-content-col">
					<div class="lh-ff-section lh-ff-section--comp">
	                    <div class="lh-ff-product">
	                      <div class="lh-ff-product__image"><img srcset="/pepper-v2-theme/images/tc/normal/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg 2x" src="/pepper-v2-theme/images/tc/normal/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg" alt=""></div>
	                      <div class="lh-ff-product__description">
	                        <div class="lh-ff-product__content">
	                          <h3 class="lh-typo__commontitle lh-typo__commontitle--1 lh-ff-product__title">${pasoBean.productoBean.descripcionTipoProducto}</h3>
	                          <p class="lh-typo__p3">Disfruta la libertad de elegir entre puntos y millas con la tarjeta que te permite canjear viajes y productos</p>
	                        </div>
	                      </div>
	                    </div>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
       
          
          
          
          
          
          