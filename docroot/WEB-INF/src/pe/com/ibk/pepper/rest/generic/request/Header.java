package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Header {

	private HeaderRequest headerRequest;
	
	@JsonCreator
	public Header(HeaderRequest headerRequest) {
		super();
		this.headerRequest = headerRequest;
	}
	
	@JsonProperty("HeaderRequest")
	public HeaderRequest getHeaderRequest() {
        return headerRequest;
    }

    public void setHeaderRequest(HeaderRequest headerRequest) {
        this.headerRequest = headerRequest;
    }

}
