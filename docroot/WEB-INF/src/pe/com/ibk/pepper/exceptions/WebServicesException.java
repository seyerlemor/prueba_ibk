package pe.com.ibk.pepper.exceptions;


public class WebServicesException extends Exception {

	private String nombreWS;
	private String trazaMessage;
	private String typeError;
	private String codServicioError;
	private Exception exception;
	
	private static final long serialVersionUID = -892747895166253824L;
	
		
	public WebServicesException(String nombreWS, String codServicioError) {
		super();
		this.nombreWS = nombreWS;
		this.codServicioError = codServicioError;
	}

	public WebServicesException() {
		super();
	}
	
	public WebServicesException(String message, Throwable cause){
		super(message,cause);
	}
	
	public WebServicesException(String message){	
		super(message);
	}
	
	public WebServicesException(Throwable cause){
		super(cause);
	}
	
	public WebServicesException(String typeError, String codServicioError,String nombreWS, String trazaMessage){
		super(trazaMessage);
		this.typeError = typeError;
		this.nombreWS = nombreWS;
		this.trazaMessage = trazaMessage;
		this.codServicioError = codServicioError;
	}
	
	public WebServicesException(String typeError, String codServicioError,String nombreWS, String trazaMessage, Exception exception){
		super(trazaMessage);
		this.typeError = typeError;
		this.nombreWS = nombreWS;
		this.trazaMessage = trazaMessage;
		this.codServicioError = codServicioError;
		this.exception = exception;
	}
	
	public WebServicesException(String nombreWS, String codServicioError, Exception exception){
		super();
		this.nombreWS = nombreWS;
		this.codServicioError = codServicioError;
		this.exception = exception;
	}
	
	public WebServicesException(Exception exception){
		this.exception = exception;
	}
	
	public String getTrazaMessage() {
		return trazaMessage;
	}

	public void setTrazaMessage(String trazaMessage) {
		this.trazaMessage = trazaMessage;
	}

	public String getNombreWS() {
		return nombreWS;
	}

	public void setNombreWS(String nombreWS) {
		this.nombreWS = nombreWS;
	}
	
	public String getTypeError() {
		return typeError;
	}

	public void setTypeError(String typeError) {
		this.typeError = typeError;
	}

	public String getCodServicioError() {
		return codServicioError;
	}

	public void setCodServicioError(String codServicioError) {
		this.codServicioError = codServicioError;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

}
