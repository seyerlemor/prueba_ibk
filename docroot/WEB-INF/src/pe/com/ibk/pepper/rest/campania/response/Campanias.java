
package pe.com.ibk.pepper.rest.campania.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Campanias implements Serializable{

	private static final long serialVersionUID = -1329111684601073282L;
	
	@SerializedName(value="campania")
	private List<Campanium> campania;

	public List<Campanium> getCampania() {
		return campania;
	}

	public void setCampania(List<Campanium> campania) {
		this.campania = campania;
	}    
    
}
