
package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TemplateParamsV2{

	/*API CAMPANIA*/
    private String leadId;
    
    /*ANTIFRAUDE*/
    private String documentNumber;

    @JsonCreator
    public TemplateParamsV2(String leadId, String siebel) {
        super();
        this.leadId = leadId;
    }
    
    @JsonCreator
	public TemplateParamsV2(String documentNumber) {
		super();
		this.documentNumber = documentNumber;
	}

    @JsonProperty("leadId")
    public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	
	@JsonProperty("documentNumber")
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
}
