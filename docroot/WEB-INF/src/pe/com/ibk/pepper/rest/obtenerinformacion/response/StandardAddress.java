package pe.com.ibk.pepper.rest.obtenerinformacion.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StandardAddress  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@SerializedName("roadType")
	private String roadType;
	
	@SerializedName("roadName")
	private String roadName;
	
	@SerializedName("roadNumber")
	private String roadNumber;
	
	@SerializedName("block")
	private String block;
	
	@SerializedName("apartment")
	private String apartment;
	
	@SerializedName("inside")
	private String inside;
	
	@SerializedName("location")
	private String location;
	
	@SerializedName("reference")
	private String reference;

	public String getRoadType() {
		return roadType;
	}

	public void setRoadType(String roadType) {
		this.roadType = roadType;
	}

	public String getRoadName() {
		return roadName;
	}

	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}

	public String getRoadNumber() {
		return roadNumber;
	}

	public void setRoadNumber(String roadNumber) {
		this.roadNumber = roadNumber;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public String getInside() {
		return inside;
	}

	public void setInside(String inside) {
		this.inside = inside;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
}
