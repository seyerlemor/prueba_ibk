
package pe.com.ibk.pepper.rest.obtenerinformacion.response;

import java.util.ArrayList;
import java.util.List;

public class Direcciones {

    private List<Direccion> direccion = null;
    
    public Direcciones() {
		super();
		direccion = new ArrayList<>();
	}

	public List<Direccion> getDireccion() {
		return direccion;
	}

	public void setDireccion(List<Direccion> direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Direcciones [direccion=" + direccion + "]";
	}


}
