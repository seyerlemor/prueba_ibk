/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class ServiciosSoap implements Serializable {
	public static ServiciosSoap toSoapModel(Servicios model) {
		ServiciosSoap soapModel = new ServiciosSoap();

		soapModel.setIdServicio(model.getIdServicio());
		soapModel.setIdExpediente(model.getIdExpediente());
		soapModel.setNombre(model.getNombre());
		soapModel.setCodigo(model.getCodigo());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setResponse(model.getResponse());
		soapModel.setMessageBus(model.getMessageBus());
		soapModel.setTipo(model.getTipo());
		soapModel.setFechaRegistro(model.getFechaRegistro());
		soapModel.setRespuestaInterna(model.getRespuestaInterna());

		return soapModel;
	}

	public static ServiciosSoap[] toSoapModels(Servicios[] models) {
		ServiciosSoap[] soapModels = new ServiciosSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ServiciosSoap[][] toSoapModels(Servicios[][] models) {
		ServiciosSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ServiciosSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ServiciosSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ServiciosSoap[] toSoapModels(List<Servicios> models) {
		List<ServiciosSoap> soapModels = new ArrayList<ServiciosSoap>(models.size());

		for (Servicios model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ServiciosSoap[soapModels.size()]);
	}

	public ServiciosSoap() {
	}

	public long getPrimaryKey() {
		return _idServicio;
	}

	public void setPrimaryKey(long pk) {
		setIdServicio(pk);
	}

	public long getIdServicio() {
		return _idServicio;
	}

	public void setIdServicio(long idServicio) {
		_idServicio = idServicio;
	}

	public long getIdExpediente() {
		return _idExpediente;
	}

	public void setIdExpediente(long idExpediente) {
		_idExpediente = idExpediente;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getCodigo() {
		return _codigo;
	}

	public void setCodigo(String codigo) {
		_codigo = codigo;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public long getResponse() {
		return _response;
	}

	public void setResponse(long response) {
		_response = response;
	}

	public String getMessageBus() {
		return _messageBus;
	}

	public void setMessageBus(String messageBus) {
		_messageBus = messageBus;
	}

	public String getTipo() {
		return _tipo;
	}

	public void setTipo(String tipo) {
		_tipo = tipo;
	}

	public Date getFechaRegistro() {
		return _fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		_fechaRegistro = fechaRegistro;
	}

	public String getRespuestaInterna() {
		return _respuestaInterna;
	}

	public void setRespuestaInterna(String respuestaInterna) {
		_respuestaInterna = respuestaInterna;
	}

	private long _idServicio;
	private long _idExpediente;
	private String _nombre;
	private String _codigo;
	private String _descripcion;
	private long _response;
	private String _messageBus;
	private String _tipo;
	private Date _fechaRegistro;
	private String _respuestaInterna;
}