<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
            <div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    ${commerceText.v601001}
                    <div class="lh-ff-content-col">
                     	<div class="lh-ff-section lh-ff-section--notification">
	                        <div class="lh-ff-section__wrapper lh-ff-section__wrapper--image-to-left">
	                          <div class="lh-ff-section__image" style="background-image:url(${themeDisplay.pathThemeImages}/form-framework/ff-result-check.png)"></div>
	                          <div class="lh-ff-section__text">
	                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 lh-ff-section__text__title">${pasoBean.clienteBean.firstname}</h3>
								<c:choose>
								<c:when test="${requestScope.tipoMensaje=='E'}">
							    	${renderRequest.preferences.getValue('recExt002','')}
							  	</c:when>
							  	<c:otherwise>
							    	${renderRequest.preferences.getValue('recExt001','')}
							  	</c:otherwise>
							</c:choose>
	                          </div>
	                        </div>
	                      </div>
                      <div class="lh-ff-section">
                        <h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">${renderRequest.preferences.getValue('recExt003','')}</h4>
                        <div class="lh-ff-info">
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('recExt004','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDescripcionProducto">${requestScope.fechaOperacion}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('recExt005','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDescripcionProducto">${requestScope.formatCellNumber}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('recExt006','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDescripcionProducto">${requestScope.formatEmailAddress}</div>
                          </div>
                        </div>
                      </div>
                      ${renderRequest.preferences.getValue('recExt007','')}
                    </div>
                  </div>
                </div>
        </div>
      </div>