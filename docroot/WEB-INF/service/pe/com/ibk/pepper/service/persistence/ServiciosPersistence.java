/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import pe.com.ibk.pepper.model.Servicios;

/**
 * The persistence interface for the servicios service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ServiciosPersistenceImpl
 * @see ServiciosUtil
 * @generated
 */
public interface ServiciosPersistence extends BasePersistence<Servicios> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ServiciosUtil} to access the servicios persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the servicios where idServicio = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchServiciosException} if it could not be found.
	*
	* @param idServicio the id servicio
	* @return the matching servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios findByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	/**
	* Returns the servicios where idServicio = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param idServicio the id servicio
	* @return the matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios fetchByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the servicios where idServicio = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param idServicio the id servicio
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios fetchByS_ID(long idServicio,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the servicios where idServicio = &#63; from the database.
	*
	* @param idServicio the id servicio
	* @return the servicios that was removed
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios removeByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	/**
	* Returns the number of servicioses where idServicio = &#63;.
	*
	* @param idServicio the id servicio
	* @return the number of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public int countByS_ID(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @return the matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Servicios> findByIdExpediente(
		long idExpediente, java.lang.String tipo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @return the range of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Servicios> findByIdExpediente(
		long idExpediente, java.lang.String tipo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Servicios> findByIdExpediente(
		long idExpediente, java.lang.String tipo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios findByIdExpediente_First(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	/**
	* Returns the first servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios fetchByIdExpediente_First(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios findByIdExpediente_Last(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	/**
	* Returns the last servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching servicios, or <code>null</code> if a matching servicios could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios fetchByIdExpediente_Last(
		long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the servicioses before and after the current servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idServicio the primary key of the current servicios
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios[] findByIdExpediente_PrevAndNext(
		long idServicio, long idExpediente, java.lang.String tipo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	/**
	* Removes all the servicioses where idExpediente = &#63; and tipo = &#63; from the database.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @throws SystemException if a system exception occurred
	*/
	public void removeByIdExpediente(long idExpediente, java.lang.String tipo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of servicioses where idExpediente = &#63; and tipo = &#63;.
	*
	* @param idExpediente the id expediente
	* @param tipo the tipo
	* @return the number of matching servicioses
	* @throws SystemException if a system exception occurred
	*/
	public int countByIdExpediente(long idExpediente, java.lang.String tipo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the servicios in the entity cache if it is enabled.
	*
	* @param servicios the servicios
	*/
	public void cacheResult(pe.com.ibk.pepper.model.Servicios servicios);

	/**
	* Caches the servicioses in the entity cache if it is enabled.
	*
	* @param servicioses the servicioses
	*/
	public void cacheResult(
		java.util.List<pe.com.ibk.pepper.model.Servicios> servicioses);

	/**
	* Creates a new servicios with the primary key. Does not add the servicios to the database.
	*
	* @param idServicio the primary key for the new servicios
	* @return the new servicios
	*/
	public pe.com.ibk.pepper.model.Servicios create(long idServicio);

	/**
	* Removes the servicios with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios that was removed
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios remove(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	public pe.com.ibk.pepper.model.Servicios updateImpl(
		pe.com.ibk.pepper.model.Servicios servicios)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the servicios with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchServiciosException} if it could not be found.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios
	* @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios findByPrimaryKey(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException,
			pe.com.ibk.pepper.NoSuchServiciosException;

	/**
	* Returns the servicios with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idServicio the primary key of the servicios
	* @return the servicios, or <code>null</code> if a servicios with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public pe.com.ibk.pepper.model.Servicios fetchByPrimaryKey(long idServicio)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the servicioses.
	*
	* @return the servicioses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Servicios> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the servicioses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @return the range of servicioses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Servicios> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the servicioses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of servicioses
	* @param end the upper bound of the range of servicioses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of servicioses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<pe.com.ibk.pepper.model.Servicios> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the servicioses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of servicioses.
	*
	* @return the number of servicioses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}