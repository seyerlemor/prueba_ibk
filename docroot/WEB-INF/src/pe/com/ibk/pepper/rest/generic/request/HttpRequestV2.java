
package pe.com.ibk.pepper.rest.generic.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.listanegra.request.Adrress;
import pe.com.ibk.pepper.rest.listanegra.request.Document;
import pe.com.ibk.pepper.rest.listanegra.request.Phone;
import pe.com.ibk.pepper.rest.potc.request.Generacion;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HttpRequestV2{

    private HttpHeaderV2 httpHeader;
    private QueryParamsV2 queryParams;
    private TemplateParamsV2 templateParams;
    
    /*OBTENER INFO PERSONA*/
    private String documentId;
    
    /*LOGICA DE REINGRESO*/
    private String documentType;
	private String channelId;
	
	/*FW4*/
	private MessageRequestV2 messageRequest;
	
	/*BD FRAUDE*/
	private Document document;
	private Phone phone;
	private Adrress adrress;
	
	/*POTC*/
	private Generacion generacion;

    public HttpRequestV2(){
    	super();
    }
    
    @JsonCreator
    public HttpRequestV2(HttpHeaderV2 httpHeader, QueryParamsV2 queryParams, TemplateParamsV2 templateParams) {
        super();
        this.httpHeader = httpHeader;
        this.queryParams = queryParams;
        this.templateParams = templateParams;
    }
    
    @JsonCreator
	public HttpRequestV2(HttpHeaderV2 httpHeader, QueryParamsV2 queryParams, String documentId){
    	super();
		this.httpHeader = httpHeader;
		this.queryParams = queryParams;
		this.documentId = documentId;
	}
    
    @JsonCreator
    public HttpRequestV2(HttpHeaderV2 httpHeader, TemplateParamsV2 templateParams) {
        super();
        this.httpHeader = httpHeader;
        this.templateParams = templateParams;
    }
    
    @JsonCreator
	public HttpRequestV2(HttpHeaderV2 httpHeader, String documentType, String documentId, String channelId){
    	super();
        this.httpHeader = httpHeader;
		this.documentType = documentType;
		this.documentId = documentId;
		this.channelId = channelId;
	}
    
    @JsonCreator
    public HttpRequestV2(HttpHeaderV2 httpHeader, MessageRequestV2 messageRequest) {
		super();
		this.httpHeader = httpHeader;
		this.messageRequest = messageRequest;
	}
    
    @JsonCreator
    public HttpRequestV2(MessageRequestV2 messageRequest) {
		super();
		this.messageRequest = messageRequest;
	}

	public HttpRequestV2(HttpHeaderV2 httpHeader, Document document, Phone phone, Adrress adrress) {
		super();
		this.httpHeader = httpHeader;
		this.document = document;
		this.phone = phone;
		this.adrress = adrress;
	}
	
	@JsonCreator
    public HttpRequestV2(HttpHeaderV2 httpHeader, Generacion generacion) {
		super();
		this.httpHeader = httpHeader;
		this.generacion = generacion;
	}

	@JsonProperty("HttpHeader")
    public HttpHeaderV2 getHttpHeader() {
        return httpHeader;
    }

    public void setHttpHeader(HttpHeaderV2 httpHeader) {
        this.httpHeader = httpHeader;
    }

    @JsonProperty("QueryParams")
    public QueryParamsV2 getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(QueryParamsV2 queryParams) {
        this.queryParams = queryParams;
    }

    @JsonProperty("TemplateParams")
    public TemplateParamsV2 getTemplateParams() {
        return templateParams;
    }

    public void setTemplateParams(TemplateParamsV2 templateParams) {
        this.templateParams = templateParams;
    }
    
    @JsonProperty("documentId")
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	@JsonProperty("documentType")
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	@JsonProperty("channelId")
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
	@JsonProperty("MessageRequest")
	public MessageRequestV2 getMessageRequest() {
		return messageRequest;
	}

	public void setMessageRequest(MessageRequestV2 messageRequest) {
		this.messageRequest = messageRequest;
	}

	@JsonProperty("document")
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	@JsonProperty("phone")
	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	@JsonProperty("adrress")
	public Adrress getAdrress() {
		return adrress;
	}

	public void setAdrress(Adrress adrress) {
		this.adrress = adrress;
	}

	@JsonProperty("generacion")
	public Generacion getGeneracion() {
		return generacion;
	}

	public void setGeneracion(Generacion generacion) {
		this.generacion = generacion;
	}
	
}
