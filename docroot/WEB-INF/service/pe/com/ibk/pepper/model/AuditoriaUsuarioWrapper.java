/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AuditoriaUsuario}.
 * </p>
 *
 * @author Interbank
 * @see AuditoriaUsuario
 * @generated
 */
public class AuditoriaUsuarioWrapper implements AuditoriaUsuario,
	ModelWrapper<AuditoriaUsuario> {
	public AuditoriaUsuarioWrapper(AuditoriaUsuario auditoriaUsuario) {
		_auditoriaUsuario = auditoriaUsuario;
	}

	@Override
	public Class<?> getModelClass() {
		return AuditoriaUsuario.class;
	}

	@Override
	public String getModelClassName() {
		return AuditoriaUsuario.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idAuditoriaUsuario", getIdAuditoriaUsuario());
		attributes.put("idUsuarioSession", getIdUsuarioSession());
		attributes.put("fechaRegistro", getFechaRegistro());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idAuditoriaUsuario = (Long)attributes.get("idAuditoriaUsuario");

		if (idAuditoriaUsuario != null) {
			setIdAuditoriaUsuario(idAuditoriaUsuario);
		}

		Long idUsuarioSession = (Long)attributes.get("idUsuarioSession");

		if (idUsuarioSession != null) {
			setIdUsuarioSession(idUsuarioSession);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}
	}

	/**
	* Returns the primary key of this auditoria usuario.
	*
	* @return the primary key of this auditoria usuario
	*/
	@Override
	public long getPrimaryKey() {
		return _auditoriaUsuario.getPrimaryKey();
	}

	/**
	* Sets the primary key of this auditoria usuario.
	*
	* @param primaryKey the primary key of this auditoria usuario
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_auditoriaUsuario.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id auditoria usuario of this auditoria usuario.
	*
	* @return the id auditoria usuario of this auditoria usuario
	*/
	@Override
	public long getIdAuditoriaUsuario() {
		return _auditoriaUsuario.getIdAuditoriaUsuario();
	}

	/**
	* Sets the id auditoria usuario of this auditoria usuario.
	*
	* @param idAuditoriaUsuario the id auditoria usuario of this auditoria usuario
	*/
	@Override
	public void setIdAuditoriaUsuario(long idAuditoriaUsuario) {
		_auditoriaUsuario.setIdAuditoriaUsuario(idAuditoriaUsuario);
	}

	/**
	* Returns the id usuario session of this auditoria usuario.
	*
	* @return the id usuario session of this auditoria usuario
	*/
	@Override
	public long getIdUsuarioSession() {
		return _auditoriaUsuario.getIdUsuarioSession();
	}

	/**
	* Sets the id usuario session of this auditoria usuario.
	*
	* @param idUsuarioSession the id usuario session of this auditoria usuario
	*/
	@Override
	public void setIdUsuarioSession(long idUsuarioSession) {
		_auditoriaUsuario.setIdUsuarioSession(idUsuarioSession);
	}

	/**
	* Returns the fecha registro of this auditoria usuario.
	*
	* @return the fecha registro of this auditoria usuario
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _auditoriaUsuario.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this auditoria usuario.
	*
	* @param fechaRegistro the fecha registro of this auditoria usuario
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_auditoriaUsuario.setFechaRegistro(fechaRegistro);
	}

	@Override
	public boolean isNew() {
		return _auditoriaUsuario.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_auditoriaUsuario.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _auditoriaUsuario.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_auditoriaUsuario.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _auditoriaUsuario.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _auditoriaUsuario.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_auditoriaUsuario.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _auditoriaUsuario.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_auditoriaUsuario.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_auditoriaUsuario.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_auditoriaUsuario.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AuditoriaUsuarioWrapper((AuditoriaUsuario)_auditoriaUsuario.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.AuditoriaUsuario auditoriaUsuario) {
		return _auditoriaUsuario.compareTo(auditoriaUsuario);
	}

	@Override
	public int hashCode() {
		return _auditoriaUsuario.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.AuditoriaUsuario> toCacheModel() {
		return _auditoriaUsuario.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario toEscapedModel() {
		return new AuditoriaUsuarioWrapper(_auditoriaUsuario.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.AuditoriaUsuario toUnescapedModel() {
		return new AuditoriaUsuarioWrapper(_auditoriaUsuario.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _auditoriaUsuario.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _auditoriaUsuario.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_auditoriaUsuario.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditoriaUsuarioWrapper)) {
			return false;
		}

		AuditoriaUsuarioWrapper auditoriaUsuarioWrapper = (AuditoriaUsuarioWrapper)obj;

		if (Validator.equals(_auditoriaUsuario,
					auditoriaUsuarioWrapper._auditoriaUsuario)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public AuditoriaUsuario getWrappedAuditoriaUsuario() {
		return _auditoriaUsuario;
	}

	@Override
	public AuditoriaUsuario getWrappedModel() {
		return _auditoriaUsuario;
	}

	@Override
	public void resetOriginalValues() {
		_auditoriaUsuario.resetOriginalValues();
	}

	private AuditoriaUsuario _auditoriaUsuario;
}