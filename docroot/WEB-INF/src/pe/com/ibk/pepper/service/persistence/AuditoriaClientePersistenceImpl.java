/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchAuditoriaClienteException;
import pe.com.ibk.pepper.model.AuditoriaCliente;
import pe.com.ibk.pepper.model.impl.AuditoriaClienteImpl;
import pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the auditoria cliente service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see AuditoriaClientePersistence
 * @see AuditoriaClienteUtil
 * @generated
 */
public class AuditoriaClientePersistenceImpl extends BasePersistenceImpl<AuditoriaCliente>
	implements AuditoriaClientePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AuditoriaClienteUtil} to access the auditoria cliente persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AuditoriaClienteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteModelImpl.FINDER_CACHE_ENABLED,
			AuditoriaClienteImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteModelImpl.FINDER_CACHE_ENABLED,
			AuditoriaClienteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_AC_ID = new FinderPath(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteModelImpl.FINDER_CACHE_ENABLED,
			AuditoriaClienteImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByAC_ID", new String[] { Long.class.getName() },
			AuditoriaClienteModelImpl.IDAUDITORIACLIENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_AC_ID = new FinderPath(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAC_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the auditoria cliente where idAuditoriaCliente = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaClienteException} if it could not be found.
	 *
	 * @param idAuditoriaCliente the id auditoria cliente
	 * @return the matching auditoria cliente
	 * @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a matching auditoria cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente findByAC_ID(long idAuditoriaCliente)
		throws NoSuchAuditoriaClienteException, SystemException {
		AuditoriaCliente auditoriaCliente = fetchByAC_ID(idAuditoriaCliente);

		if (auditoriaCliente == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idAuditoriaCliente=");
			msg.append(idAuditoriaCliente);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchAuditoriaClienteException(msg.toString());
		}

		return auditoriaCliente;
	}

	/**
	 * Returns the auditoria cliente where idAuditoriaCliente = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idAuditoriaCliente the id auditoria cliente
	 * @return the matching auditoria cliente, or <code>null</code> if a matching auditoria cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente fetchByAC_ID(long idAuditoriaCliente)
		throws SystemException {
		return fetchByAC_ID(idAuditoriaCliente, true);
	}

	/**
	 * Returns the auditoria cliente where idAuditoriaCliente = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idAuditoriaCliente the id auditoria cliente
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching auditoria cliente, or <code>null</code> if a matching auditoria cliente could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente fetchByAC_ID(long idAuditoriaCliente,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idAuditoriaCliente };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_AC_ID,
					finderArgs, this);
		}

		if (result instanceof AuditoriaCliente) {
			AuditoriaCliente auditoriaCliente = (AuditoriaCliente)result;

			if ((idAuditoriaCliente != auditoriaCliente.getIdAuditoriaCliente())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_AUDITORIACLIENTE_WHERE);

			query.append(_FINDER_COLUMN_AC_ID_IDAUDITORIACLIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idAuditoriaCliente);

				List<AuditoriaCliente> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_AC_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"AuditoriaClientePersistenceImpl.fetchByAC_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					AuditoriaCliente auditoriaCliente = list.get(0);

					result = auditoriaCliente;

					cacheResult(auditoriaCliente);

					if ((auditoriaCliente.getIdAuditoriaCliente() != idAuditoriaCliente)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_AC_ID,
							finderArgs, auditoriaCliente);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_AC_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AuditoriaCliente)result;
		}
	}

	/**
	 * Removes the auditoria cliente where idAuditoriaCliente = &#63; from the database.
	 *
	 * @param idAuditoriaCliente the id auditoria cliente
	 * @return the auditoria cliente that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente removeByAC_ID(long idAuditoriaCliente)
		throws NoSuchAuditoriaClienteException, SystemException {
		AuditoriaCliente auditoriaCliente = findByAC_ID(idAuditoriaCliente);

		return remove(auditoriaCliente);
	}

	/**
	 * Returns the number of auditoria clientes where idAuditoriaCliente = &#63;.
	 *
	 * @param idAuditoriaCliente the id auditoria cliente
	 * @return the number of matching auditoria clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByAC_ID(long idAuditoriaCliente) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_AC_ID;

		Object[] finderArgs = new Object[] { idAuditoriaCliente };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_AUDITORIACLIENTE_WHERE);

			query.append(_FINDER_COLUMN_AC_ID_IDAUDITORIACLIENTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idAuditoriaCliente);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_AC_ID_IDAUDITORIACLIENTE_2 = "auditoriaCliente.idAuditoriaCliente = ?";

	public AuditoriaClientePersistenceImpl() {
		setModelClass(AuditoriaCliente.class);
	}

	/**
	 * Caches the auditoria cliente in the entity cache if it is enabled.
	 *
	 * @param auditoriaCliente the auditoria cliente
	 */
	@Override
	public void cacheResult(AuditoriaCliente auditoriaCliente) {
		EntityCacheUtil.putResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteImpl.class, auditoriaCliente.getPrimaryKey(),
			auditoriaCliente);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_AC_ID,
			new Object[] { auditoriaCliente.getIdAuditoriaCliente() },
			auditoriaCliente);

		auditoriaCliente.resetOriginalValues();
	}

	/**
	 * Caches the auditoria clientes in the entity cache if it is enabled.
	 *
	 * @param auditoriaClientes the auditoria clientes
	 */
	@Override
	public void cacheResult(List<AuditoriaCliente> auditoriaClientes) {
		for (AuditoriaCliente auditoriaCliente : auditoriaClientes) {
			if (EntityCacheUtil.getResult(
						AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
						AuditoriaClienteImpl.class,
						auditoriaCliente.getPrimaryKey()) == null) {
				cacheResult(auditoriaCliente);
			}
			else {
				auditoriaCliente.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all auditoria clientes.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AuditoriaClienteImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AuditoriaClienteImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the auditoria cliente.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AuditoriaCliente auditoriaCliente) {
		EntityCacheUtil.removeResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteImpl.class, auditoriaCliente.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(auditoriaCliente);
	}

	@Override
	public void clearCache(List<AuditoriaCliente> auditoriaClientes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AuditoriaCliente auditoriaCliente : auditoriaClientes) {
			EntityCacheUtil.removeResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
				AuditoriaClienteImpl.class, auditoriaCliente.getPrimaryKey());

			clearUniqueFindersCache(auditoriaCliente);
		}
	}

	protected void cacheUniqueFindersCache(AuditoriaCliente auditoriaCliente) {
		if (auditoriaCliente.isNew()) {
			Object[] args = new Object[] {
					auditoriaCliente.getIdAuditoriaCliente()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_AC_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_AC_ID, args,
				auditoriaCliente);
		}
		else {
			AuditoriaClienteModelImpl auditoriaClienteModelImpl = (AuditoriaClienteModelImpl)auditoriaCliente;

			if ((auditoriaClienteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_AC_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						auditoriaCliente.getIdAuditoriaCliente()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_AC_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_AC_ID, args,
					auditoriaCliente);
			}
		}
	}

	protected void clearUniqueFindersCache(AuditoriaCliente auditoriaCliente) {
		AuditoriaClienteModelImpl auditoriaClienteModelImpl = (AuditoriaClienteModelImpl)auditoriaCliente;

		Object[] args = new Object[] { auditoriaCliente.getIdAuditoriaCliente() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_AC_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_AC_ID, args);

		if ((auditoriaClienteModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_AC_ID.getColumnBitmask()) != 0) {
			args = new Object[] {
					auditoriaClienteModelImpl.getOriginalIdAuditoriaCliente()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_AC_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_AC_ID, args);
		}
	}

	/**
	 * Creates a new auditoria cliente with the primary key. Does not add the auditoria cliente to the database.
	 *
	 * @param idAuditoriaCliente the primary key for the new auditoria cliente
	 * @return the new auditoria cliente
	 */
	@Override
	public AuditoriaCliente create(long idAuditoriaCliente) {
		AuditoriaCliente auditoriaCliente = new AuditoriaClienteImpl();

		auditoriaCliente.setNew(true);
		auditoriaCliente.setPrimaryKey(idAuditoriaCliente);

		return auditoriaCliente;
	}

	/**
	 * Removes the auditoria cliente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idAuditoriaCliente the primary key of the auditoria cliente
	 * @return the auditoria cliente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente remove(long idAuditoriaCliente)
		throws NoSuchAuditoriaClienteException, SystemException {
		return remove((Serializable)idAuditoriaCliente);
	}

	/**
	 * Removes the auditoria cliente with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the auditoria cliente
	 * @return the auditoria cliente that was removed
	 * @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente remove(Serializable primaryKey)
		throws NoSuchAuditoriaClienteException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AuditoriaCliente auditoriaCliente = (AuditoriaCliente)session.get(AuditoriaClienteImpl.class,
					primaryKey);

			if (auditoriaCliente == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAuditoriaClienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(auditoriaCliente);
		}
		catch (NoSuchAuditoriaClienteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AuditoriaCliente removeImpl(AuditoriaCliente auditoriaCliente)
		throws SystemException {
		auditoriaCliente = toUnwrappedModel(auditoriaCliente);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(auditoriaCliente)) {
				auditoriaCliente = (AuditoriaCliente)session.get(AuditoriaClienteImpl.class,
						auditoriaCliente.getPrimaryKeyObj());
			}

			if (auditoriaCliente != null) {
				session.delete(auditoriaCliente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (auditoriaCliente != null) {
			clearCache(auditoriaCliente);
		}

		return auditoriaCliente;
	}

	@Override
	public AuditoriaCliente updateImpl(
		pe.com.ibk.pepper.model.AuditoriaCliente auditoriaCliente)
		throws SystemException {
		auditoriaCliente = toUnwrappedModel(auditoriaCliente);

		boolean isNew = auditoriaCliente.isNew();

		Session session = null;

		try {
			session = openSession();

			if (auditoriaCliente.isNew()) {
				session.save(auditoriaCliente);

				auditoriaCliente.setNew(false);
			}
			else {
				session.merge(auditoriaCliente);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AuditoriaClienteModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
			AuditoriaClienteImpl.class, auditoriaCliente.getPrimaryKey(),
			auditoriaCliente);

		clearUniqueFindersCache(auditoriaCliente);
		cacheUniqueFindersCache(auditoriaCliente);

		return auditoriaCliente;
	}

	protected AuditoriaCliente toUnwrappedModel(
		AuditoriaCliente auditoriaCliente) {
		if (auditoriaCliente instanceof AuditoriaClienteImpl) {
			return auditoriaCliente;
		}

		AuditoriaClienteImpl auditoriaClienteImpl = new AuditoriaClienteImpl();

		auditoriaClienteImpl.setNew(auditoriaCliente.isNew());
		auditoriaClienteImpl.setPrimaryKey(auditoriaCliente.getPrimaryKey());

		auditoriaClienteImpl.setIdAuditoriaCliente(auditoriaCliente.getIdAuditoriaCliente());
		auditoriaClienteImpl.setIdClienteTransaccion(auditoriaCliente.getIdClienteTransaccion());
		auditoriaClienteImpl.setPaso(auditoriaCliente.getPaso());
		auditoriaClienteImpl.setPagina(auditoriaCliente.getPagina());
		auditoriaClienteImpl.setTipoFlujo(auditoriaCliente.getTipoFlujo());
		auditoriaClienteImpl.setStrJson(auditoriaCliente.getStrJson());
		auditoriaClienteImpl.setFechaRegistro(auditoriaCliente.getFechaRegistro());

		return auditoriaClienteImpl;
	}

	/**
	 * Returns the auditoria cliente with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the auditoria cliente
	 * @return the auditoria cliente
	 * @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAuditoriaClienteException, SystemException {
		AuditoriaCliente auditoriaCliente = fetchByPrimaryKey(primaryKey);

		if (auditoriaCliente == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAuditoriaClienteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return auditoriaCliente;
	}

	/**
	 * Returns the auditoria cliente with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchAuditoriaClienteException} if it could not be found.
	 *
	 * @param idAuditoriaCliente the primary key of the auditoria cliente
	 * @return the auditoria cliente
	 * @throws pe.com.ibk.pepper.NoSuchAuditoriaClienteException if a auditoria cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente findByPrimaryKey(long idAuditoriaCliente)
		throws NoSuchAuditoriaClienteException, SystemException {
		return findByPrimaryKey((Serializable)idAuditoriaCliente);
	}

	/**
	 * Returns the auditoria cliente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the auditoria cliente
	 * @return the auditoria cliente, or <code>null</code> if a auditoria cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		AuditoriaCliente auditoriaCliente = (AuditoriaCliente)EntityCacheUtil.getResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
				AuditoriaClienteImpl.class, primaryKey);

		if (auditoriaCliente == _nullAuditoriaCliente) {
			return null;
		}

		if (auditoriaCliente == null) {
			Session session = null;

			try {
				session = openSession();

				auditoriaCliente = (AuditoriaCliente)session.get(AuditoriaClienteImpl.class,
						primaryKey);

				if (auditoriaCliente != null) {
					cacheResult(auditoriaCliente);
				}
				else {
					EntityCacheUtil.putResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
						AuditoriaClienteImpl.class, primaryKey,
						_nullAuditoriaCliente);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AuditoriaClienteModelImpl.ENTITY_CACHE_ENABLED,
					AuditoriaClienteImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return auditoriaCliente;
	}

	/**
	 * Returns the auditoria cliente with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idAuditoriaCliente the primary key of the auditoria cliente
	 * @return the auditoria cliente, or <code>null</code> if a auditoria cliente with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditoriaCliente fetchByPrimaryKey(long idAuditoriaCliente)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idAuditoriaCliente);
	}

	/**
	 * Returns all the auditoria clientes.
	 *
	 * @return the auditoria clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditoriaCliente> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the auditoria clientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of auditoria clientes
	 * @param end the upper bound of the range of auditoria clientes (not inclusive)
	 * @return the range of auditoria clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditoriaCliente> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the auditoria clientes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.AuditoriaClienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of auditoria clientes
	 * @param end the upper bound of the range of auditoria clientes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of auditoria clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditoriaCliente> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AuditoriaCliente> list = (List<AuditoriaCliente>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_AUDITORIACLIENTE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_AUDITORIACLIENTE;

				if (pagination) {
					sql = sql.concat(AuditoriaClienteModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AuditoriaCliente>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AuditoriaCliente>(list);
				}
				else {
					list = (List<AuditoriaCliente>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the auditoria clientes from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (AuditoriaCliente auditoriaCliente : findAll()) {
			remove(auditoriaCliente);
		}
	}

	/**
	 * Returns the number of auditoria clientes.
	 *
	 * @return the number of auditoria clientes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_AUDITORIACLIENTE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the auditoria cliente persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.AuditoriaCliente")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AuditoriaCliente>> listenersList = new ArrayList<ModelListener<AuditoriaCliente>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AuditoriaCliente>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AuditoriaClienteImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_AUDITORIACLIENTE = "SELECT auditoriaCliente FROM AuditoriaCliente auditoriaCliente";
	private static final String _SQL_SELECT_AUDITORIACLIENTE_WHERE = "SELECT auditoriaCliente FROM AuditoriaCliente auditoriaCliente WHERE ";
	private static final String _SQL_COUNT_AUDITORIACLIENTE = "SELECT COUNT(auditoriaCliente) FROM AuditoriaCliente auditoriaCliente";
	private static final String _SQL_COUNT_AUDITORIACLIENTE_WHERE = "SELECT COUNT(auditoriaCliente) FROM AuditoriaCliente auditoriaCliente WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "auditoriaCliente.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AuditoriaCliente exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AuditoriaCliente exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AuditoriaClientePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idAuditoriaCliente", "idClienteTransaccion", "paso", "pagina",
				"tipoFlujo", "strJson", "fechaRegistro"
			});
	private static AuditoriaCliente _nullAuditoriaCliente = new AuditoriaClienteImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AuditoriaCliente> toCacheModel() {
				return _nullAuditoriaClienteCacheModel;
			}
		};

	private static CacheModel<AuditoriaCliente> _nullAuditoriaClienteCacheModel = new CacheModel<AuditoriaCliente>() {
			@Override
			public AuditoriaCliente toEntityModel() {
				return _nullAuditoriaCliente;
			}
		};
}