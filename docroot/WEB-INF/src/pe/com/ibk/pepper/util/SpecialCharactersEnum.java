package pe.com.ibk.pepper.util;

public enum SpecialCharactersEnum {
	
	//CUSTOMER E1001 - E1099
	A_LC_TILDE("&aacute;","á"),
	E_LC_TILDE("&eacute;","é"),
	I_LC_TILDE("&iacute;","í"),
	O_LC_TILDE("&oacute;","ó"),
	U_LC_TILDE("&uacute;","ú"),
	A_UC_TILDE("&Aacute;","Á"),
	E_UC_TILDE("&Eacute;","É"),
	I_UC_TILDE("&Iacute;","Í"),
	O_UC_TILDE("&Oacute;","Ó"),
	U_UC_TILDE("&Uacute;","Ú"),
	N_LC_TILDE("&ntilde;","ñ"),
	N_UC_TILDE("&Ntilde;","Ñ"),
	R_REG("&reg;","®");
	
	private String code;
	private String value;
	SpecialCharactersEnum(String code,String value) {
        this.code = code;
        this.value = value;
    }
    public String code() {
        return code;
    }
    
    public String value() {
        return value;
    }
}




