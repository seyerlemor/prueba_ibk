
package pe.com.ibk.pepper.rest.token.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.generic.request.HttpHeadersV2;

public class ValidarTokenRequest {

	private HttpHeadersV2 httpHeader;
    private String key;
    private String token;
    private String messageId;

    @JsonCreator
	public ValidarTokenRequest(HttpHeadersV2 httpHeader, String key, String token, String messageId) {
    	super();
        this.httpHeader = httpHeader;
        this.key = key;
        this.token = token;
        this.messageId = messageId; 
    }
    
    @JsonProperty("HttpHeader")
    public HttpHeadersV2 getHttpHeader() {
        return httpHeader;
    }

    public void setHttpHeader(HttpHeadersV2 httpHeader) {
        this.httpHeader = httpHeader;
    }
    
    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("messageId")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
}
