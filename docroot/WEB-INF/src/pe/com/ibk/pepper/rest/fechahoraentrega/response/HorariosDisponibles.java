package pe.com.ibk.pepper.rest.fechahoraentrega.response;

import java.util.List;

public class HorariosDisponibles {
	private List<Turno> turno;
	
	public HorariosDisponibles() {
		super();
	}

	public HorariosDisponibles(List<Turno> turno) {
		super();
		this.turno = turno;
	}

	public List<Turno> getTurno() {
		return turno;
	}

	public void setTurno(List<Turno> turno) {
		this.turno = turno;
	}
	
}
