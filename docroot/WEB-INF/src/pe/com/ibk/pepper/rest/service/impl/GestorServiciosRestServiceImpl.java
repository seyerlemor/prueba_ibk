package pe.com.ibk.pepper.rest.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletPreferences;
import javax.portlet.ResourceRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import pe.com.ibk.pepper.bean.ComercioBean;
import pe.com.ibk.pepper.bean.DireccionClienteBean;
import pe.com.ibk.pepper.bean.EquifaxBean;
import pe.com.ibk.pepper.bean.OfertaBean;
import pe.com.ibk.pepper.bean.OfertasBean;
import pe.com.ibk.pepper.bean.RegistroAuditoriaBean;
import pe.com.ibk.pepper.bean.ServiceBean;
import pe.com.ibk.pepper.bean.SolicitudBean;
import pe.com.ibk.pepper.exceptions.WebServicesException;
import pe.com.ibk.pepper.model.ParametroHijoPO;
import pe.com.ibk.pepper.model.Ubigeo;
import pe.com.ibk.pepper.model.impl.ParametroHijoPOImpl;
import pe.com.ibk.pepper.rest.altatc.request.AltaTCProvisional;
import pe.com.ibk.pepper.rest.altatc.request.AltaTcRequest;
import pe.com.ibk.pepper.rest.altatc.request.DatosCampania;
import pe.com.ibk.pepper.rest.altatc.request.DatosCliente;
import pe.com.ibk.pepper.rest.altatc.request.DatosConyuge;
import pe.com.ibk.pepper.rest.altatc.request.DatosExpediente;
import pe.com.ibk.pepper.rest.altatc.request.DatosMonitor;
import pe.com.ibk.pepper.rest.altatc.request.DatosToken;
import pe.com.ibk.pepper.rest.altatc.request.GrabarLPD;
import pe.com.ibk.pepper.rest.calificacion.request.Address;
import pe.com.ibk.pepper.rest.calificacion.request.Customer;
import pe.com.ibk.pepper.rest.calificacion.request.Evaluation;
import pe.com.ibk.pepper.rest.calificacion.request.GeneralProductData;
import pe.com.ibk.pepper.rest.calificacion.request.IdentityDocument;
import pe.com.ibk.pepper.rest.calificacion.request.Product;
import pe.com.ibk.pepper.rest.calificacion.request.ProductDetail;
import pe.com.ibk.pepper.rest.calificacion.request.ProductDetailList;
import pe.com.ibk.pepper.rest.campania.request.ConsultarLead;
import pe.com.ibk.pepper.rest.campania.response.Campanium;
import pe.com.ibk.pepper.rest.campania.response.ConsultarLeadResponse;
import pe.com.ibk.pepper.rest.campania.response.Lead;
import pe.com.ibk.pepper.rest.equifax.request.Candidato;
import pe.com.ibk.pepper.rest.equifax.request.ConsultaPreguntas;
import pe.com.ibk.pepper.rest.equifax.request.InformacionAdicional;
import pe.com.ibk.pepper.rest.equifax.request.PreguntasVal;
import pe.com.ibk.pepper.rest.equifax.request.ValidacionPreguntas;
import pe.com.ibk.pepper.rest.fechahoraentrega.request.FechaHoraEntregaRequest;
import pe.com.ibk.pepper.rest.generic.request.BodyV2;
import pe.com.ibk.pepper.rest.generic.request.HttpHeaderV2;
import pe.com.ibk.pepper.rest.generic.request.HttpHeadersV2;
import pe.com.ibk.pepper.rest.generic.request.HttpRequestV2;
import pe.com.ibk.pepper.rest.generic.request.MessageRequestV2;
import pe.com.ibk.pepper.rest.generic.request.QueryParamsV2;
import pe.com.ibk.pepper.rest.generic.request.TemplateParamsV2;
import pe.com.ibk.pepper.rest.generic.response.HttpResponseV2;
import pe.com.ibk.pepper.rest.listanegra.request.Document;
import pe.com.ibk.pepper.rest.listanegra.request.Documento;
import pe.com.ibk.pepper.rest.listanegra.request.Phone;
import pe.com.ibk.pepper.rest.listanegra.request.Telefono;
import pe.com.ibk.pepper.rest.listanegra.request.ValidarListaNegra;
import pe.com.ibk.pepper.rest.obtenerinformacion.response.Direccion;
import pe.com.ibk.pepper.rest.potc.request.AltaUsuario;
import pe.com.ibk.pepper.rest.potc.request.ConsultarProductos;
import pe.com.ibk.pepper.rest.potc.request.ConsultarProvisional;
import pe.com.ibk.pepper.rest.potc.request.CrearPOTC;
import pe.com.ibk.pepper.rest.potc.request.Generacion;
import pe.com.ibk.pepper.rest.potc.request.Parametro;
import pe.com.ibk.pepper.rest.potc.request.PotcRequest;
import pe.com.ibk.pepper.rest.potc.request.VinculacionProducto;
import pe.com.ibk.pepper.rest.potc.response.Producto;
import pe.com.ibk.pepper.rest.potc.response.Provisional;
import pe.com.ibk.pepper.rest.registroauditoria.request.RegistroAuditoriaRequest;
import pe.com.ibk.pepper.rest.service.GestorServiciosRestService;
import pe.com.ibk.pepper.rest.token.request.GenerarTokenRequest;
import pe.com.ibk.pepper.rest.token.request.Parameter;
import pe.com.ibk.pepper.rest.token.request.ValidarTokenRequest;
import pe.com.ibk.pepper.rest.util.RestRegistroAuditoriaServiceUtil;
import pe.com.ibk.pepper.rest.util.RestServiceUtil;
import pe.com.ibk.pepper.service.ParametroHijoPOLocalServiceUtil;
import pe.com.ibk.pepper.service.UbigeoLocalServiceUtil;
import pe.com.ibk.pepper.util.Constantes;
import pe.com.ibk.pepper.util.NameParameterWSKey;
import pe.com.ibk.pepper.util.PortalKeys;
import pe.com.ibk.pepper.util.PortalValues;
import pe.com.ibk.pepper.util.RequestParam;
import pe.com.ibk.pepper.util.ServicioUtil;
import pe.com.ibk.pepper.util.SessionKeys;
import pe.com.ibk.pepper.util.UbigeoUtil;
import pe.com.ibk.pepper.util.Utils;
import pe.com.ibk.pepper.util.WebServicesUtil;
import pe.com.ibk.pepper.webservices.util.ParameterBodyWS;

@Service
public class GestorServiciosRestServiceImpl implements GestorServiciosRestService {
	
		private static final Log _log = LogFactoryUtil.getLog(GestorServiciosRestServiceImpl.class);
		
		@Override
		public SolicitudBean validarAntiFraude(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException{
			Boolean flagAntifraude = Boolean.TRUE;
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_CAMP, pasoBean.getExpedienteBean().getMessageId());
			HttpHeaderV2 httpHeader =  new HttpHeaderV2(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(), nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(),  nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1());
			QueryParamsV2 queryParams = new QueryParamsV2(pasoBean.getExpedienteBean().getIp(), pasoBean.getClienteBean().getCellPhoneNumber(), pasoBean.getClienteBean().getEmailAddress());
			TemplateParamsV2 templateParams = new TemplateParamsV2(pasoBean.getExpedienteBean().getDniNumber());
			HttpRequestV2 httpRequest = new HttpRequestV2(httpHeader, queryParams, templateParams);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), endPoint, path, Constantes.COD_ANTIFRAUDE, Constantes.HTML_ERROR_ANTIFRAUDE, Boolean.FALSE, Constantes.HTTP_PUT, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(Validator.isNull(httpResponse))
				flagAntifraude = Boolean.FALSE;
			else if(!httpResponse.getValid())
				throw new WebServicesException(Constantes.COD_ANTIFRAUDE, Constantes.HTML_ERROR_ANTIFRAUDE);
			pasoBean.getExpedienteBean().setFlagAntifraude(flagAntifraude);
			return pasoBean;
		}
		
		@Override
		public SolicitudBean consultarCampania(SolicitudBean pasoBean, ResourceRequest request, String path) throws WebServicesException {		
			Map<String, ParametroHijoPO> nombreMap = new HashMap<>();		
			Boolean flagCampania = Boolean.FALSE;
			try {
				String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
				ComercioBean comercioBean = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
				nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_CAMP, pasoBean.getExpedienteBean().getMessageId());
				String codigoProducto = PrefsPropsUtil.getString(PortalKeys.REST_CAMPANIA_CODIGO_PRODUCTO, PortalValues.REST_CAMPANIA_CODIGO_PRODUCTO);
				pasoBean.getProductoBean().setCodigoProducto(codigoProducto);
				
				ConsultarLead consultarLead = new ConsultarLead(
						nombreMap.get(ParameterBodyWS.B_TIPO_BUSQUEDA).getDato1(), nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1()
						, pasoBean.getExpedienteBean().getDniNumber(), nombreMap.get(ParameterBodyWS.B_FLG_DATOS_OFERTA).getDato1()
						, nombreMap.get(ParameterBodyWS.B_FLG_NUEVO_BUS).getDato1(), nombreMap.get(ParameterBodyWS.B_FLG_CONSULT_DIR).getDato1()
						, nombreMap.get(ParameterBodyWS.B_COD_CANAL).getDato1());
				MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(consultarLead));
				HttpRequestV2 httpRequest = new HttpRequestV2(messageRequest);
				String requestString = ServicioUtil.generarRequestString(httpRequest);
				ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_FWK4_HOST, path, Constantes.CONS_CAMPANIA, Constantes.HTML_ERROR_CONS_CAMP, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
				String response = RestServiceUtil.getSecurityRestClient(serviceBean);
				Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
				HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
				ConsultarLeadResponse leadResponse = httpResponse.getMessageResponse().getBody().getConsultarLeadResponse();
				List<Campanium> lstCampanias = Validator.isNotNull(leadResponse.getCampanias())?leadResponse.getCampanias().getCampania():new ArrayList<Campanium>();
				List<OfertaBean> lstOferta = new ArrayList<>();
				for(Campanium lead : lstCampanias)
					if(lead.getCabecera().getIdProductoPrincipal().equals(codigoProducto)  && !flagCampania){
						lstOferta = Utils.addOfferListFirst(lstOferta, comercioBean.getAllowedCards(), lead.getDetalle().getMarcaTarjeta(), lead);
						lstOferta = Utils.addOfferList(lstOferta, comercioBean.getAllowedCards(), lead.getAdicionales().getCampoAdicional9(), lead.getAdicionales().getCampoAdicional10());
						lstOferta = Utils.addOfferList(lstOferta, comercioBean.getAllowedCards(), lead.getAdicionales().getCampoAdicional1(), lead.getAdicionales().getCampoAdicional2());
						lstOferta = Utils.addOfferList(lstOferta, comercioBean.getAllowedCards(), lstOferta.size()<3?lead.getAdicionales().getCampoAdicional3():StringPool.BLANK, lead.getAdicionales().getCampoAdicional4());
						OfertasBean ofertasBean = new OfertasBean();
						ofertasBean.setOfertaBean(lstOferta);
						pasoBean.setOfertasBean(ofertasBean);
						flagCampania = lstOferta.isEmpty()?Boolean.FALSE:Boolean.TRUE;
						pasoBean.setProductoBean(Utils.setCampaignGeneralData(flagCampania, lead, pasoBean.getProductoBean()));
					}
			} catch (SystemException e) 	{
				throw new WebServicesException(Constantes.CONS_CAMPANIA, Constantes.HTML_ERROR_CONS_CAMP, e);
			}	finally{
				pasoBean.getExpedienteBean().setFlagCampania(flagCampania);
			}	
			return pasoBean;		
		}

		@Override
		public SolicitudBean getDocumentLead(SolicitudBean pasoBean, String path, String path2, ResourceRequest request) throws WebServicesException {
			Boolean flagApiBanner = Boolean.FALSE;
			SolicitudBean pasoBeanSession = new SolicitudBean();
			try {
				Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_CAMP, pasoBean.getExpedienteBean().getMessageId());
				HttpHeaderV2 header =  new HttpHeaderV2(WebServicesUtil.toXMLGregorianCalendar(new Date()), nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), pasoBean.getExpedienteBean().getMessageId(), nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1());
				QueryParamsV2 queryParams = new QueryParamsV2(nombreMap.get(ParameterBodyWS.B_COD_CANAL).getDato1(), "01", PrefsPropsUtil.getString(PortalKeys.REST_CAMPANIA_CODIGO_PRODUCTO, PortalValues.REST_CAMPANIA_CODIGO_PRODUCTO), StringPool.BLANK);
				TemplateParamsV2 templateParams = new TemplateParamsV2(pasoBean.getExpedienteBean().getDniNumber());
				HttpRequestV2 httpRequest = new HttpRequestV2(header, queryParams, templateParams);
				String requestString = ServicioUtil.generarRequestString(httpRequest);
				ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path, Constantes.CONS_CAMPANIA, Constantes.HTML_ERROR_CONS_CAMP, Boolean.FALSE, Constantes.HTTP_GET, Boolean.TRUE);
				String response = RestServiceUtil.getSecurityRestClient(serviceBean);
				Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
				HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
				if(!httpResponse.getLeads().isEmpty()){
					pasoBean.getProductoBean().setCodigoProducto(httpResponse.getLeads().get(0).getId());
					pasoBeanSession = getApitLead(pasoBean, path2, request);
					flagApiBanner = Boolean.TRUE;
				}
			} catch (SystemException e) {
				throw new WebServicesException(Constantes.CONS_CAMPANIA, Constantes.HTML_ERROR_CONS_CAMP, e);
			}finally{
				pasoBeanSession.getExpedienteBean().setFlagCampania(flagApiBanner);
			}
			return pasoBeanSession;	
		}
		
		@Override
		public SolicitudBean getApitLead(SolicitudBean pasoBean, String path, ResourceRequest request) throws WebServicesException {
			Boolean flagApiDetail = Boolean.FALSE;
			String jsonCommerce = Validator.isNotNull(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN))?GetterUtil.getString(SessionKeys.getSessionPortal(request, SessionKeys.COMERCIO_BEAN)):StringPool.BLANK;
			ComercioBean comercioBean = JSONFactoryUtil.looseDeserialize(jsonCommerce, ComercioBean.class);
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_CAMP, pasoBean.getExpedienteBean().getMessageId());
			HttpHeaderV2 header =  new HttpHeaderV2(WebServicesUtil.toXMLGregorianCalendar(new Date()), nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), pasoBean.getExpedienteBean().getMessageId(), nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1());
			QueryParamsV2 queryParams = new QueryParamsV2(nombreMap.get(ParameterBodyWS.B_COD_CANAL).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1());
			TemplateParamsV2 templateParams = new TemplateParamsV2(pasoBean.getProductoBean().getCodigoProducto(), StringPool.BLANK);
			HttpRequestV2 httpRequest = new HttpRequestV2(header, queryParams, templateParams);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path, Constantes.CONS_CAMPANIA, Constantes.HTML_ERROR_CONS_CAMP, Boolean.FALSE, Constantes.HTTP_GET, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(!httpResponse.getLeads().isEmpty()){
				List<OfertaBean> lstOferta = new ArrayList<>();
				Lead lead = httpResponse.getLeads().get(0);
				lstOferta = Utils.addOfferListApiFirst(lstOferta, comercioBean.getAllowedCards(), lead.getCardBrand().getId(), lead);
				lstOferta = Utils.addOfferListApi(lstOferta, comercioBean.getAllowedCards(), lead.getAdditionals().get(0).getItem9(), lead.getAdditionals().get(0).getItem10());
				lstOferta = Utils.addOfferListApi(lstOferta, comercioBean.getAllowedCards(), lead.getAdditionals().get(0).getItem1(), lead.getAdditionals().get(0).getItem2());
				lstOferta = Utils.addOfferListApi(lstOferta, comercioBean.getAllowedCards(), lstOferta.size()<3?lead.getAdditionals().get(0).getItem3():StringPool.BLANK, lead.getAdditionals().get(0).getItem4());
				OfertasBean ofertasBean = new OfertasBean();
				ofertasBean.setOfertaBean(lstOferta);
				pasoBean.setOfertasBean(ofertasBean);
				if(!lstOferta.isEmpty())
					flagApiDetail = Boolean.TRUE;
				pasoBean.setProductoBean(Utils.setCampaignGeneralDataApi(flagApiDetail, lead, pasoBean.getProductoBean()));
			}
			pasoBean.getExpedienteBean().setFlagCampania(flagApiDetail);
			return pasoBean;
		}

		@Override
		public SolicitudBean obtenerApiInformacionPersona(SolicitudBean pasoBean, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_OBT_INFO_PERS, pasoBean.getExpedienteBean().getMessageId());	
			HttpHeaderV2 httpHeader =  new HttpHeaderV2(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(), WebServicesUtil.toXMLGregorianCalendar(new Date()), nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(),  nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1());
			QueryParamsV2 queryParams = new QueryParamsV2(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1());
			HttpRequestV2 httpRequest = new HttpRequestV2(httpHeader, queryParams, pasoBean.getExpedienteBean().getDniNumber());
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path.concat(pasoBean.getExpedienteBean().getDniNumber()), Constantes.OBT_INFO_PERSONA, Constantes.HTML_ERROR_OBT_INFO, Boolean.TRUE, Constantes.HTTP_GET, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			SolicitudBean pasoBeanSession = Utils.setServicePersonDataToPasoBean(httpResponse, pasoBean);
			pasoBeanSession.getExpedienteBean().setFlagObtenerInformacion(Boolean.TRUE);
			return pasoBeanSession;
		}
		
		@Override
		public SolicitudBean validarListaNegra(SolicitudBean pasoBean, String path) throws WebServicesException  {
			Boolean flagResponse = Boolean.FALSE; 
			Map<String, ParametroHijoPO>  nombreMap = getMapNombreParametrosWS(Constantes.WS_OBT_INFO_PERS, pasoBean.getExpedienteBean().getMessageId());	
			HttpHeaderV2 header = new HttpHeaderV2(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(), WebServicesUtil.toXMLGregorianCalendar(new Date()), nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1());
			Document document = new Document(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1(), pasoBean.getExpedienteBean().getDniNumber());
			Phone phone = new Phone(Constantes.TYPE_2_PHONE, StringUtils.EMPTY, pasoBean.getClienteBean().getCellPhoneNumber());
			HttpRequestV2 httpRequest = new HttpRequestV2(header, document, phone, Utils.agregarDireccionBdFraude(pasoBean));
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path, Constantes.VALID_LISTA_NEGRA, Constantes.HTML_ERROR_LISTA_NEGRA, Boolean.TRUE, Constantes.HTTP_POST, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(Boolean.parseBoolean(httpResponse.getIsFraud()))
				flagResponse = Boolean.TRUE;
			pasoBean.getExpedienteBean().setFlagR1(flagResponse);
			return pasoBean;		
		}	

		@Override
		public SolicitudBean validarListaNegra2G(SolicitudBean pasoBean, String path) throws WebServicesException  {
			Boolean flagResponse = Boolean.FALSE; 
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_OBT_INFO_PERS, pasoBean.getExpedienteBean().getMessageId());	
			HttpHeaderV2 header = new HttpHeaderV2(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(), WebServicesUtil.toXMLGregorianCalendar(new Date()), nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1());
			Documento documento = new Documento(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1(), pasoBean.getExpedienteBean().getDniNumber());
			Telefono telefono = new Telefono("2", "1", pasoBean.getClienteBean().getCellPhoneNumber());
			ValidarListaNegra validarListaNegra = new ValidarListaNegra(documento, telefono);
			MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(validarListaNegra));
			HttpRequestV2 httpRequest = new HttpRequestV2(header, messageRequest);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path, Constantes.VALID_LISTA_NEGRA, Constantes.HTML_ERROR_LISTA_NEGRA, Boolean.TRUE, Constantes.HTTP_POST, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(Boolean.parseBoolean(httpResponse.getMessageResponse().getBody().getValidarListaNegraResponse().getFlagValidarListaNegra()))
				flagResponse = Boolean.TRUE;
			pasoBean.getExpedienteBean().setFlagR1(flagResponse);
			return pasoBean;		
		}

		@Override
		public SolicitudBean validarLogicaReingreso(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException {		
			Boolean flagRetryLogic = Boolean.TRUE;
			String retryLogicCode = StringPool.BLANK;
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_AUT_TOKEN, pasoBean.getExpedienteBean().getMessageId()); 
			HttpHeaderV2 header = new HttpHeaderV2(nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(), nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
					nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1());
			HttpRequestV2 httpRequest = new HttpRequestV2(header, Constantes.TIPO_INTEGRANTE1, pasoBean.getExpedienteBean().getDniNumber(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1());
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), endPoint, path, Constantes.RETRY_LOGIC, Constantes.HTML_ERROR_LOGICA_REINGRESO, Boolean.FALSE, Constantes.HTTP_POST, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(Validator.isNull(httpResponse))
				flagRetryLogic = Boolean.FALSE;
			else if(Utils.validateBlankAndNull(httpResponse.getErrorCode()))
				retryLogicCode = httpResponse.getErrorCode();
			else if(Utils.validateBlankAndNull(httpResponse.getFileId()) 
					&& Utils.validateBlankAndNull(httpResponse.getProductHost()) 
					&& Utils.validateBlankAndNull(httpResponse.getSubProductHost())){
				pasoBean.getExpedienteBean().setExpedienteCDA(httpResponse.getFileId());
				pasoBean.getProductoBean().setMarcaProducto(httpResponse.getProductHost());
				pasoBean.getProductoBean().setTipoProducto(httpResponse.getSubProductHost());
				pasoBean.getExpedienteBean().setFlagCalificacion(Validator.isNotNull(httpResponse.getRegisterDate())?Boolean.TRUE:null);
				retryLogicCode = Constantes.WITH_FILE;
			}else
				retryLogicCode = Constantes.WITHOUT_FILE;
			pasoBean.getExpedienteBean().setFlagLogicaReingreso(flagRetryLogic);
			pasoBean.getExpedienteBean().setCodigoLogicaReingreso(retryLogicCode);
			return pasoBean;
		}
		
		@Override
		public SolicitudBean validarBancaSMS(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException{
			Boolean flagAfiliacionBancaSMS = Boolean.TRUE;
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_OBT_INFO_PERS, pasoBean.getExpedienteBean().getMessageId());
			HttpHeaderV2 httpHeader =  new HttpHeaderV2(
					nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1()
					, WebServicesUtil.toXMLGregorianCalendar(new Date()), nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1()
					, nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1()
					, nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1()
					, nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1()
					, nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1(), nombreMap.get(NameParameterWSKey.H_CARD_ID_TYPE).getDato1());
			TemplateParamsV2 templateParams = new TemplateParamsV2(pasoBean.getClienteBean().getCodigoUnico());
			HttpRequestV2 httpRequest = new HttpRequestV2(httpHeader, templateParams);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), endPoint, path, Constantes.COD_BANCASMS, Constantes.HTML_ERROR_BANCA_SMS, Boolean.TRUE, Constantes.HTTP_GET, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(Validator.isNull(httpResponse) || Utils.validateBlankAndNull(httpResponse.getHttpCode()))
				flagAfiliacionBancaSMS = Boolean.FALSE;
			else if(httpResponse.getEnrollmentInfo().isEmpty())
				throw new WebServicesException(Constantes.COD_BANCASMS, Constantes.HTML_ERROR_BANCA_SMS);
			else{
				pasoBean.getClienteBean().setCellPhoneNumberOtp(httpResponse.getEnrollmentInfo().get(0).getPhone().getNumber());
				pasoBean.getClienteBean().setCellPhoneOperator(httpResponse.getEnrollmentInfo().get(0).getPhone().getOperatorCode());
			}
			pasoBean.getExpedienteBean().setFlagAfiliacionBancaSMS(flagAfiliacionBancaSMS);
			return pasoBean;
		}
		
		@Override
		public SolicitudBean generarToken(SolicitudBean pasoBean, String path) throws WebServicesException {		
			Boolean flagGeneracionToken = Boolean.FALSE;
			try {
				Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_AUT_TOKEN, pasoBean.getExpedienteBean().getMessageId()); 
				HttpHeadersV2  headers = new HttpHeadersV2(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), pasoBean.getExpedienteBean().getMessageId(),
								nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1(), 
								nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1(), nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1());
				String email = Validator.isNotNull(pasoBean.getProductoBean().getEmailSaleConstancy())?pasoBean.getProductoBean().getEmailSaleConstancy():pasoBean.getClienteBean().getEmailAddressOtp();
				String sendMethod = Utils.validateBlankAndNull(pasoBean.getProductoBean().getTipoSeguro())?pasoBean.getProductoBean().getTipoSeguro():nombreMap.get(ParameterBodyWS.B_SEND_METHOD).getDato1();
				List<Parameter> parameters = new ArrayList<>();
				Parameter param1 = new Parameter(nombreMap.get(ParameterBodyWS.B_KEY1).getDato1(), nombreMap.get(ParameterBodyWS.B_VALUE1).getDato1());
				parameters.add(param1);
				Parameter param2 = new Parameter(nombreMap.get(ParameterBodyWS.B_KEY2).getDato1(), nombreMap.get(ParameterBodyWS.B_VALUE2).getDato1());
				parameters.add(param2);
				Parameter param3 = new Parameter("Descripcion", StringPool.BLANK);
				parameters.add(param3);
				GenerarTokenRequest tokenRequest = new GenerarTokenRequest(headers, pasoBean.getExpedienteBean().getDniNumber(), nombreMap.get(ParameterBodyWS.B_ID_TOKEN).getDato1()
						, email, pasoBean.getClienteBean().getCellPhoneNumberOtp(), pasoBean.getClienteBean().getCellPhoneOperator()
						, sendMethod, nombreMap.get(ParameterBodyWS.B_CHANNEL).getDato1(), pasoBean.getExpedienteBean().getMessageId(), parameters);
				String requestString = ServicioUtil.generarRequestString(tokenRequest);
				ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), PrefsPropsUtil.getString(PortalKeys.REST_AUT_TOKEN_ENDPOINT, PortalValues.REST_AUT_TOKEN_ENDPOINT), path, Constantes.GEN_TOKEN, Constantes.HTML_ERROR_GENERAR_TOKEN, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
				String response = RestServiceUtil.getSecurityRestClient(serviceBean);
				Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
				HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
				flagGeneracionToken = httpResponse.getSendkey();
				pasoBean.getProductoBean().setToken(httpResponse.getCaptcha());	
			}  catch (SystemException e) {
				throw new WebServicesException(Constantes.GEN_TOKEN, Constantes.HTML_ERROR_GENERAR_TOKEN, e);
			}finally{
				pasoBean.getExpedienteBean().setFlagGeneracionToken(flagGeneracionToken);	
			}
			return pasoBean;
		}

		@Override
		public SolicitudBean validarToken(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_AUT_TOKEN, pasoBean.getExpedienteBean().getMessageId()); 
			HttpHeadersV2  headers = new HttpHeadersV2(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), pasoBean.getExpedienteBean().getMessageId(),
							nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(),
							nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1(), nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1());
			ValidarTokenRequest tokenRequest = new ValidarTokenRequest(headers, pasoBean.getProductoBean().getCodigoSms()
					, pasoBean.getProductoBean().getToken(), pasoBean.getExpedienteBean().getMessageId());
			String requestString = ServicioUtil.generarRequestString(tokenRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), endPoint, path, Constantes.VALID_TOKEN, Constantes.HTML_ERROR_VALIDAR_TOKEN, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			Boolean flagValidacionToken = httpResponse.getValidated();
			pasoBean.getOtcBean().setValidado(flagValidacionToken);
			return pasoBean;
		}	

		@Override
		public SolicitudBean consultarPreguntasEquifax(SolicitudBean pasoBean, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_PREG_EQFX, pasoBean.getExpedienteBean().getMessageId());
			Candidato candidato = new Candidato(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1(), pasoBean.getExpedienteBean().getDniNumber(),
					pasoBean.getClienteBean().getNombres(), pasoBean.getClienteBean().getLastname(), pasoBean.getClienteBean().getMotherslastname(),
					nombreMap.get(ParameterBodyWS.B_INICIO_VALIDA).getDato1(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
			InformacionAdicional informacionAdicional = new InformacionAdicional(pasoBean.getEquifaxBean().getModelo());
			ConsultaPreguntas consultaPreguntas = new ConsultaPreguntas(candidato, informacionAdicional);
			MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(consultaPreguntas));
			HttpRequestV2 httpRequest = new HttpRequestV2(messageRequest);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_FWK4_HOST, path, Constantes.CONS_PREG_EQUIFAX, Constantes.HTML_ERROR_CONSULTAR_EQUIFAX, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			EquifaxBean equifaxBean = pasoBean.getEquifaxBean();				
			equifaxBean.setNroOperacion(httpResponse.getMessageResponse().getBody().getConsultaPreguntasResponse().getNumeroOperacion());
			equifaxBean.setPreguntas(httpResponse.getMessageResponse().getBody().getConsultaPreguntasResponse().getPreguntas().getPregunta());				
			pasoBean.setEquifaxBean(equifaxBean);	
			pasoBean.getExpedienteBean().setFlagConsultarEquifax(Boolean.TRUE);
			return pasoBean;		
		}

		@Override
		public SolicitudBean validarPreguntasEquifax(SolicitudBean pasoBean, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_PREG_EQFX, pasoBean.getExpedienteBean().getMessageId());
			InformacionAdicional informacionAdicional = new InformacionAdicional(pasoBean.getEquifaxBean().getModelo(), pasoBean.getEquifaxBean().getNroOperacion());
			ValidacionPreguntas validacionPreguntas = new ValidacionPreguntas(pasoBean.getEquifaxBean().getPreguntasVal(), informacionAdicional);
			MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(validacionPreguntas));
			HttpRequestV2 httpRequest = new HttpRequestV2(messageRequest);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_FWK4_HOST, path, Constantes.VALID_PREG_EQUIFAX, Constantes.HTML_ERROR_VALIDAR_EQUIFAX, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			pasoBean.getEquifaxBean().setNroOperacion(httpResponse.getMessageResponse().getBody().getValidacionPreguntasResponse().getNumeroOperacion());
			pasoBean.getEquifaxBean().setResultadoEquifax(httpResponse.getMessageResponse().getBody().getValidacionPreguntasResponse().getResultadoEvaluacion());
			pasoBean.getExpedienteBean().setFlagValidarEquifax(Boolean.TRUE);
			pasoBean.getEquifaxBean().setPreguntas(null);
			pasoBean.getEquifaxBean().setPreguntasVal(new PreguntasVal());
			return pasoBean;		
		}	

		@Override
		public SolicitudBean evaluarCalificacion(SolicitudBean pasoBean, String endPoint, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CALIF_CDA, pasoBean.getExpedienteBean().getMessageId());
			List<Customer> customers = new ArrayList<>();
			Customer customer1 = cargarIntegranteV2(pasoBean, nombreMap, Constantes.TYPE_INTEGRANTE1);
			customers.add(customer1);
			if((Constantes.ESTADO_TIPO_CASADO.equals(pasoBean.getClienteBean().getCivilStatus()) || Constantes.ESTADO_TIPO_CONVIVIENTE.equals(pasoBean.getClienteBean().getCivilStatus())) && Constantes.FLUJO_REGULAR==pasoBean.getProductoBean().getFlujoCampana()){
				Customer customer2 = cargarIntegranteV2(pasoBean, nombreMap, Constantes.TYPE_INTEGRANTE2);
				customers.add(customer2);
			}else{
				customer1.setCivilState(Constantes.ESTADO_TIPO_SOLTERO);
				pasoBean.getClienteBean().setCivilStatus(Constantes.ESTADO_TIPO_SOLTERO);
			}
			List<ProductDetailList> productDetails = new ArrayList<>();
			ProductDetailList productDetail1 = new ProductDetailList();
			ProductDetail val1 = new ProductDetail(nombreMap.get(ParameterBodyWS.B_MONEDA).getDato1(), pasoBean.getProductoBean().getIdMoneda());
			productDetail1.setProductDetail(val1);
			productDetails.add(productDetail1);
			ProductDetailList productDetail2 = new ProductDetailList();
			ProductDetail val2 = new ProductDetail(nombreMap.get(ParameterBodyWS.B_IND_DISP_EFECTIVO).getDato1(), nombreMap.get(ParameterBodyWS.B_VAL_DISP_EFECTIVO).getDato1());
			productDetail2.setProductDetail(val2);
			productDetails.add(productDetail2);
			ProductDetailList productDetail3 = new ProductDetailList();
			ProductDetail val3 = new ProductDetail(nombreMap.get(ParameterBodyWS.B_FECHA_PAGO).getDato1(), pasoBean.getProductoBean().getDiaDePago());
			productDetail3.setProductDetail(val3);
			productDetails.add(productDetail3);
			GeneralProductData generalProductData = new GeneralProductData(productDetails, nombreMap.get(ParameterBodyWS.B_NOMBRE_PROD).getDato1());
			Product product = new Product(generalProductData, pasoBean.getProductoBean().getMarcaProducto(), nombreMap.get(ParameterBodyWS.B_COD_TIPO_SOLIC).getDato1()
					, nombreMap.get(ParameterBodyWS.B_COD_CLASE_PROD).getDato1(), pasoBean.getProductoBean().getTipoProducto());
			Evaluation evaluation = new Evaluation(customers, product);
			MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(evaluation));
			HttpRequestV2 httpRequest = new HttpRequestV2(messageRequest);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), endPoint, path, Constantes.CALIF_CDA, Constantes.HTML_ERROR_CALIFICACION, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			Boolean flagCalificacion = Boolean.parseBoolean(httpResponse.getMessageResponse().getBody().getEvaluation().getResultCod().equals(Constantes.EVALUACION_CDA_CALIFICA)?Constantes.FLAG_TRUE:Constantes.FLAG_FALSE);
			pasoBean.getExpedienteBean().setExpedienteCDA(flagCalificacion?httpResponse.getMessageResponse().getBody().getEvaluation().getRecord().getRecordCod():null);
			pasoBean.getExpedienteBean().setFlagCalificacion(flagCalificacion);
			return pasoBean;
		}
		
		public Customer cargarIntegranteV2(SolicitudBean pasoBean, Map<String, ParametroHijoPO> nombreMap, Integer tipoIntegrante){
			Customer integrante = new Customer();
			integrante.setTypeCustomer(tipoIntegrante);
			boolean flgIntegrante = tipoIntegrante.equals(Constantes.TYPE_INTEGRANTE2)?true:false;
			List<IdentityDocument> identityDocuments = new ArrayList<>();
			IdentityDocument identityDocument = new IdentityDocument();
			identityDocument.setType(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1());
			identityDocument.setNumber(flgIntegrante?pasoBean.getClienteBean().getDocumentoConyuge():pasoBean.getExpedienteBean().getDniNumber());
			identityDocuments.add(identityDocument);
			integrante.setIdentityDocument(identityDocuments);
			List<Address> addresses = new ArrayList<>();
			Address address = new Address();
			//Ubigeo Domicilio
			String departamentoDom = pasoBean.getClienteBean().getAddressDepartamento();
			String provinciaDom = pasoBean.getClienteBean().getAddressProvincia();
			String distritoDom = pasoBean.getClienteBean().getAddressDistrito();
			if(Validator.isNull(departamentoDom) || Validator.isNull(provinciaDom) || Validator.isNull(distritoDom)){
				departamentoDom = "LIMA";
				provinciaDom = "LIMA";
				distritoDom = "LIMA";
			}else{
				departamentoDom = Utils.reemplazarTildes(departamentoDom);
				provinciaDom = Utils.reemplazarTildes(provinciaDom);
				distritoDom = Utils.reemplazarTildes(distritoDom);
			}		
			Ubigeo ubigeoDom = UbigeoLocalServiceUtil.getUbigeoByNombre(departamentoDom, provinciaDom, distritoDom);
			if(Validator.isNull(ubigeoDom)){
				address.setDepartament(Constantes.UBIGEO_LIMA_DEPARTAMENTO);
				address.setProvince(Constantes.UBIGEO_LIMA_PROVINCIA);
				address.setDistrict(Constantes.UBIGEO_LIMA_DISTRITO);
			}else{
				address.setDepartament(ubigeoDom.getCodDepartamento());
				address.setProvince(ubigeoDom.getCodProvincia());
				address.setDistrict(ubigeoDom.getCodDistrito());
			}
			addresses.add(address);
			integrante.setAddress(addresses);
			integrante.setCivilState(pasoBean.getClienteBean().getCivilStatus());
			String employmentSituation = Validator.isNotNull(pasoBean.getClienteBean().getEmploymentStatus())?pasoBean.getClienteBean().getEmploymentStatus():StringPool.BLANK;
			integrante.setEmploymentSituation(employmentSituation);
			return integrante;
		}
		
		@Override
		public SolicitudBean consultarFechaHoraApi3G(SolicitudBean pasoBean, String ubigeo, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_OBT_INFO_PERS, pasoBean.getExpedienteBean().getMessageId());	
			HttpHeadersV2  headers = new HttpHeadersV2(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_REFERENCE_NUMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1(), nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1(), 
							WebServicesUtil.toXMLGregorianCalendar(new Date()));
			BigDecimal lineaCredito = new BigDecimal(pasoBean.getProductoBean().getLineaCredito());
			QueryParamsV2 queryParams = new QueryParamsV2("01", "99", "T",
							pasoBean.getProductoBean().getMarcaProducto(), pasoBean.getProductoBean().getTipoProducto(),
							"604", String.valueOf(lineaCredito.intValue()),
							ubigeo, "0");
			FechaHoraEntregaRequest httpRequest = new FechaHoraEntregaRequest(headers, queryParams);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path, Constantes.CONS_FECHA_HORA, Constantes.HTML_ERROR_FECHA_HORA, Boolean.TRUE, Constantes.HTTP_GET, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			pasoBean.getFhEntregaBean().setHttpResponseV2(httpResponse);
			if(Validator.isNull(httpResponse))
				throw new WebServicesException(Constantes.CONS_FECHA_HORA, Constantes.HTML_ERROR_FECHA_HORA);
			pasoBean.getExpedienteBean().setFlagConsultarFechaHora(Boolean.TRUE);
			return pasoBean;
		}
		
		@Override
		public SolicitudBean consultarFechaHoraApi2G(SolicitudBean pasoBean, pe.com.ibk.pepper.rest.obtenerinformacion.response.Direccion direccionActual, String path) throws WebServicesException {
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_OBT_INFO_PERS, pasoBean.getExpedienteBean().getMessageId());	
			HttpHeadersV2 headers = new HttpHeadersV2(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_REFERENCE_NUMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1(), nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1(), 
							WebServicesUtil.toXMLGregorianCalendar(new Date()));
			String ubigeo = direccionActual.getUbigeo();
			String codDepartamento = ubigeo.substring(0, 2);
			String codProvincia = ubigeo.substring(2, 4);
			String codDistrito = ubigeo.substring(4);
			//Configurables 
			BigDecimal lineaCredito = new BigDecimal(pasoBean.getProductoBean().getLineaCredito());
			QueryParamsV2 queryParams = new QueryParamsV2(WebServicesUtil.toXMLGregorianCalendar(new Date()).substring(0, 10), "01", "99", "T",
							pasoBean.getProductoBean().getMarcaProducto(), pasoBean.getProductoBean().getTipoProducto(),
							"604", String.valueOf(lineaCredito.intValue()),
							codDepartamento, codProvincia, codDistrito, "1");
			FechaHoraEntregaRequest httpRequest = new FechaHoraEntregaRequest(headers, queryParams);
			String requestString = ServicioUtil.generarRequestString(httpRequest);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_API_HOST, path, Constantes.CONS_FECHA_HORA, Constantes.HTML_ERROR_FECHA_HORA, Boolean.TRUE, Constantes.HTTP_GET, Boolean.FALSE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			pasoBean.getFhEntregaBean().setHttpResponseV2(httpResponse);
			if(Validator.isNull(httpResponse))
				throw new WebServicesException(Constantes.CONS_FECHA_HORA, Constantes.HTML_ERROR_FECHA_HORA);
			pasoBean.getExpedienteBean().setFlagConsultarFechaHora(Boolean.TRUE);
			return pasoBean;
		}
		
		@Override
		public SolicitudBean altaTc(SolicitudBean pasoBean, int indice, String pepperType, ResourceRequest request) throws WebServicesException {		
			AltaTcRequest requestAltaTc = new AltaTcRequest();
			Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_VENTA_TC, pasoBean.getExpedienteBean().getMessageId());	
			pe.com.ibk.pepper.rest.altatc.request.InformacionEntrega informacionEntrega = new pe.com.ibk.pepper.rest.altatc.request.InformacionEntrega(); 
			pe.com.ibk.pepper.rest.obtenerinformacion.response.Direccion direccionEntrega = pasoBean.getClienteBean().getDirecciones().getDireccion().get(indice);
			informacionEntrega.setTipoDireccion("0");
			informacionEntrega.setHoraFin(pasoBean.getProductoBean().getFinalTime());
			informacionEntrega.setCodigoDepartamento(direccionEntrega.getUbigeo().substring(0,2));
			informacionEntrega.setCodigoDistrito(direccionEntrega.getUbigeo().substring(4));
			informacionEntrega.setDireccion(direccionEntrega.getAddressDetail());
			informacionEntrega.setFechaEntrega(pasoBean.getProductoBean().getDateDelivery());
			informacionEntrega.setCodigoProvincia(direccionEntrega.getUbigeo().substring(2,4));
			informacionEntrega.setCodigoTienda("898");
			informacionEntrega.setReferencia(direccionEntrega.getDireccionEstandar().getReferencia());
			informacionEntrega.setEmail(pasoBean.getProductoBean().getEmailStateAccount());
			informacionEntrega.setHoraInicio(pasoBean.getProductoBean().getInitTime());
			informacionEntrega.setNumeroTelefono(pasoBean.getClienteBean().getCellPhoneNumber());
			requestAltaTc.setInformacionEntrega(informacionEntrega);
			GrabarLPD grabarLPD = new GrabarLPD();
			grabarLPD.setTipoConsentimiento("01");
			grabarLPD.setFlagCliente(Constantes.CLIENTE_SI.equalsIgnoreCase(pasoBean.getClienteBean().getFlagCliente())?Constantes.NUMERO_CLIENTE_SI:Constantes.NUMERO_CLIENTE_NO);
			grabarLPD.setFlagLPD(pasoBean.getProductoBean().getFlagLPDP());
			requestAltaTc.setGrabarLPD(grabarLPD);
			DatosCliente datosCliente = new DatosCliente();
			datosCliente.setApellidoPaterno(pasoBean.getClienteBean().getLastname());
			datosCliente.setSegundoNombre(pasoBean.getClienteBean().getMiddlename());
			Direccion direccionPrincipal = pasoBean.getClienteBean().getDirecciones().getDireccion().get(0);
			datosCliente.setReferenciaUbicacion(Validator.isNotNull(direccionPrincipal.getDireccionEstandar().getReferencia())?direccionPrincipal.getDireccionEstandar().getReferencia():StringPool.BLANK);
			datosCliente.setNombreVia(direccionPrincipal.getDireccionEstandar().getNombreVia());
			datosCliente.setFechaNacimiento(pasoBean.getClienteBean().getBirthday());
			datosCliente.setCodigoPaisNacimiento(nombreMap.get(ParameterBodyWS.B_COD_PAIS_NACIMIENTO).getDato1());
			datosCliente.setNombreUrbanizacion(direccionPrincipal.getDireccionEstandar().getUrbanizacion());
			datosCliente.setCodigoOcupacion(pasoBean.getClienteBean().getOcupacion());
			datosCliente.setCodigoSituacionLaboral(StringPool.BLANK.equalsIgnoreCase(pasoBean.getClienteBean().getEmploymentStatus())?null:pasoBean.getClienteBean().getEmploymentStatus());
			datosCliente.setCodigoPaisResidencia(nombreMap.get(ParameterBodyWS.B_COD_PAIS_RESIDENCIA).getDato1());
			datosCliente.setIdManzana(direccionPrincipal.getDireccionEstandar().getManzana());
			datosCliente.setCodigoUnico(Validator.isNotNull(pasoBean.getClienteBean().getCodigoUnico())?pasoBean.getClienteBean().getCodigoUnico().substring(4):pasoBean.getClienteBean().getCodigoUnico());
			datosCliente.setNombreEmpresa(pasoBean.getClienteBean().getCompanyName());
			datosCliente.setApellidoMaterno(pasoBean.getClienteBean().getMotherslastname());
			datosCliente.setNumeroCalle(direccionPrincipal.getDireccionEstandar().getNumero());
			datosCliente.setCodigoPaisNacionalidad(nombreMap.get(ParameterBodyWS.B_COD_PAIS_NACIONALIDAD).getDato1());
			datosCliente.setNumeroDocumento(pasoBean.getClienteBean().getDniNumber());
			datosCliente.setTipoNacionalidad(nombreMap.get(ParameterBodyWS.B_COD_NACIONALIDAD).getDato1());
			datosCliente.setEmail(pasoBean.getClienteBean().getEmailAddress());
			datosCliente.setIdInterior(direccionPrincipal.getDireccionEstandar().getInterior());
			datosCliente.setNumeroTelefono(pasoBean.getClienteBean().getCellPhoneNumber());
			datosCliente.setCodigoUbigeo(direccionPrincipal.getUbigeo());
			datosCliente.setPrimerNombre(pasoBean.getClienteBean().getFirstname());
			datosCliente.setTipoVia(direccionPrincipal.getDireccionEstandar().getTipoVia());
			datosCliente.setEstadoCivil(pasoBean.getClienteBean().getCivilStatus());
			datosCliente.setTipoDocumento(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1());
			datosCliente.setSexo(pasoBean.getClienteBean().getGender());
			datosCliente.setIdLote(direccionPrincipal.getDireccionEstandar().getPisoLote());
			if(Constantes.CLIENTE_NO.equals(pasoBean.getClienteBean().getFlagCliente())){
				DireccionClienteBean direAux = UbigeoUtil.getUbigeoByCod(direccionPrincipal.getDistrito());
				datosCliente.setDistrito(direAux.getDistrito());
				datosCliente.setProvincia(direAux.getProvincia());
				datosCliente.setDepartamento(direAux.getDepartamento());
			}
			requestAltaTc.setDatosCliente(datosCliente);
			DatosExpediente datosExpediente = new DatosExpediente();
			datosExpediente.setCodigoPromotor(nombreMap.get(ParameterBodyWS.B_COD_PROMOTOR).getDato1());
			datosExpediente.setCodigoPuntoVenta(nombreMap.get(ParameterBodyWS.B_COD_PTO_VENTA).getDato1());
			datosExpediente.setCodigoCanal(nombreMap.get(ParameterBodyWS.B_COD_CANAL).getDato1());
			datosExpediente.setCodigoTienda(nombreMap.get(ParameterBodyWS.B_COD_TIENDA).getDato1());
			datosExpediente.setIdExpediente(pasoBean.getExpedienteBean().getExpedienteCDA());
			requestAltaTc.setDatosExpediente(datosExpediente);
			DatosCampania datosCampania = new DatosCampania();
			datosCampania.setIdCampania(pasoBean.getProductoBean().getCodigoCampana());
			requestAltaTc.setDatosCampania(datosCampania);
			AltaTCProvisional altaTCProvisional = new AltaTCProvisional();
			altaTCProvisional.setCodigoMoneda(pasoBean.getProductoBean().getIdMoneda());
			altaTCProvisional.setImporteLinea(pasoBean.getProductoBean().getLineaCredito());
			altaTCProvisional.setCodigoCorrespondencia(Constantes.CODIGO_CORRESP_EMAPER);
			altaTCProvisional.setImporteLineaPepper(Constantes.PEPPER_TYPE_MATERIAL.equalsIgnoreCase(pepperType)?pasoBean.getProductoBean().getLineaCredito():Utils.validateLineaPepper(pasoBean));
			altaTCProvisional.setCodigoMarca(pasoBean.getProductoBean().getMarcaProducto());
			altaTCProvisional.setCodigoConvenio(StringPool.BLANK);
			altaTCProvisional.setIndicadorTipo(pasoBean.getProductoBean().getTipoProducto());
			altaTCProvisional.setDiaPago(pasoBean.getProductoBean().getDiaDePago());
			altaTCProvisional.setIndicadorTipoCliente(pasoBean.getProductoBean().getTipoCliente());
			altaTCProvisional.setIndicadorDevolucionCashback(Constantes.INDICADOR_DEVOLUCION_CASHBACK);
			altaTCProvisional.setCodigoVendedor(Constantes.CODIGO_VENDEDOR);
			requestAltaTc.setAltaTCProvisional(altaTCProvisional);
			if(Validator.isNotNull(pasoBean.getClienteBean().getDocumentoConyuge()) && !StringPool.BLANK.equalsIgnoreCase(pasoBean.getClienteBean().getDocumentoConyuge())){
				DatosConyuge datosConyuge = new DatosConyuge(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1(), pasoBean.getClienteBean().getDocumentoConyuge());
				requestAltaTc.setDatosConyuge(datosConyuge);
			}
			DatosMonitor datosMonitor = new DatosMonitor(pasoBean.getExpedienteBean().getDispositivo(), pasoBean.getExpedienteBean().getIp(), pasoBean.getExpedienteBean().getTipoAutenticacion());
			PortletPreferences preferences = request.getPreferences();
			String endPoint = preferences.getValue(RequestParam.ENDPOINT_PEPPER_ONLINE_PROV,StringPool.BLANK);
			String path = preferences.getValue(RequestParam.PATH_PEPPER_ONLINE_PROV,StringPool.BLANK);
			if(Constantes.PEPPER_TYPE_CARDLESS.equalsIgnoreCase(pepperType)){
				endPoint = preferences.getValue(RequestParam.ENDPOINT_PEPPER_ONLINE_CARDLESS,StringPool.BLANK);
				path = preferences.getValue(RequestParam.PATH_PEPPER_ONLINE_CARDLESS,StringPool.BLANK);
			}else if(Constantes.PEPPER_TYPE_MATERIAL.equalsIgnoreCase(pepperType)){
				endPoint = preferences.getValue(RequestParam.ENDPOINT_PEPPER_ONLINE_MATERIAL,StringPool.BLANK);
				path = preferences.getValue(RequestParam.PATH_PEPPER_ONLINE_MATERIAL,StringPool.BLANK);
				DatosToken datosToken = new DatosToken(pasoBean.getClienteBean().getCellPhoneOperator());
				requestAltaTc.setDatosToken(datosToken);
				datosMonitor.setTipoAutenticacion(null);
			}
			requestAltaTc.setDatosMonitor(datosMonitor);
			HttpHeadersV2  httpHeader = new HttpHeadersV2(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_REFERENCE_NUMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1(), nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1(),
							nombreMap.get(NameParameterWSKey.H_CLIENT_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1(),
							nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1(), WebServicesUtil.toXMLGregorianCalendar(new Date()));
			requestAltaTc.setHttpHeader(httpHeader);
			String requestString = ServicioUtil.generarRequestString(requestAltaTc);
			ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), endPoint, path, Constantes.VENTA_TC, Constantes.HTML_ERROR_ALTA_TC, Boolean.FALSE, Constantes.HTTP_POST, Boolean.TRUE);
			String response = RestServiceUtil.getSecurityRestClient(serviceBean);
			Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
			HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
			if(!Validator.isNull(httpResponse)){
				pasoBean.getProductoBean().setCvv(httpResponse.getCvv2());
				pasoBean.getProductoBean().setCodigo4csc(httpResponse.getCodigo4csc());
				pasoBean.getProductoBean().setNumTarjetaProvisional(httpResponse.getNumeroTarjeta());
				pasoBean.getProductoBean().setFechaVencProvisional(httpResponse.getFechaVencimiento());
				pasoBean.getExpedienteBean().setFlagVentaTC(Boolean.TRUE);
				return pasoBean;
			}else
				throw new WebServicesException(serviceBean.getNombreWs(), serviceBean.getHtmlError());
		}
		
		@Override
		public SolicitudBean consultarProvisional(SolicitudBean pasoBean) throws WebServicesException {
			try {	
				Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_PROV, pasoBean.getExpedienteBean().getMessageId());
				ConsultarProvisional consultarProvisional = new ConsultarProvisional(pasoBean.getClienteBean().getCodigoUnico());
				MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(consultarProvisional));
				HttpRequestV2 httpRequest = new HttpRequestV2(messageRequest);
				String requestString = ServicioUtil.generarRequestString(httpRequest);
				ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_FWK4_HOST, PrefsPropsUtil.getString(PortalKeys.REST_CONSULTAR_PROV_RESOURCE, PortalValues.REST_CONSULTAR_PROV_RESOURCE), Constantes.CONS_PROV, Constantes.HTML_ERROR_POTC, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
				String response = RestServiceUtil.getSecurityRestClient(serviceBean);
				Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
				HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
				Provisional tarjetaProvisional = httpResponse.getMessageResponse().getBody().getConsultarProvisionalResponse().getTarjetasProvisionales().getProvisional();
				pasoBean.getProductoBean().setNumTarjetaProvisional(tarjetaProvisional.getNumeroTarjeta());
				pasoBean.getProductoBean().setFechaVencProvisional(tarjetaProvisional.getFechaVencimiento());
			} catch (SystemException e) {
				throw new WebServicesException(Constantes.CONS_PROV, Constantes.HTML_ERROR_POTC, e);
			}
			return pasoBean;
		}
		
		@Override
		public SolicitudBean consultarPOTC(SolicitudBean pasoBean) throws WebServicesException {
			try {
				Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_CONS_POTC, pasoBean.getExpedienteBean().getMessageId());	
				ConsultarProductos consultarProducto = new ConsultarProductos(
						nombreMap.get(ParameterBodyWS.B_DOMINIO).getDato1(), nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1()
						, pasoBean.getExpedienteBean().getDniNumber(), nombreMap.get(ParameterBodyWS.B_VERSION).getDato1());
				MessageRequestV2 messageRequest = new MessageRequestV2(ServicioUtil.obtenerHeaderRequest(nombreMap), new BodyV2(consultarProducto));
				HttpRequestV2 httpRequest = new HttpRequestV2(messageRequest);
				String requestString = ServicioUtil.generarRequestString(httpRequest);
				ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), Utils.REST_FWK4_HOST, PrefsPropsUtil.getString(PortalKeys.REST_CONSULTAR_POTC_RESOURCE, PortalValues.REST_CONSULTAR_POTC_RESOURCE), Constantes.CONS_PROV, Constantes.HTML_ERROR_POTC, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
				String response = RestServiceUtil.getSecurityRestClient(serviceBean);
				Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
				HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
				Producto producto = httpResponse.getMessageResponse().getBody().getConsultarProductosResponse().getProductos().getProducto();
				pasoBean.getOtcBean().setIdProducto(producto.getIdProducto());
				pasoBean.getOtcBean().setIdProductoUsuario(Integer.parseInt(producto.getIdProductoUsuario()));
			} catch (SystemException e) {
				throw new WebServicesException(Constantes.CONS_PROV, Constantes.HTML_ERROR_POTC, e);
			} 
			return pasoBean;		
		}

		@Override
		public SolicitudBean generarPOTC(SolicitudBean pasoBean) throws WebServicesException {
			try {
				Generacion generacion = new Generacion();
				generacion.setNumeroDocumento(pasoBean.getExpedienteBean().getDniNumber());
				String correo = Validator.isNotNull(pasoBean.getProductoBean().getEmailSaleConstancy())?pasoBean.getProductoBean().getEmailSaleConstancy():pasoBean.getClienteBean().getEmailAddress();
				Map<String, ParametroHijoPO> nombreMap = getMapNombreParametrosWS(Constantes.WS_GEN_POTC, pasoBean.getExpedienteBean().getMessageId());
				Boolean flagConsultaPOTC = Boolean.FALSE;
				if(Validator.isNull(pasoBean.getOtcBean().getIdProducto()) || Validator.isNull(pasoBean.getOtcBean().getIdProductoUsuario()))
					flagConsultaPOTC = Boolean.TRUE;
				Boolean flagNuevoCliente = Boolean.valueOf(flagConsultaPOTC?Constantes.FLAG_TRUE:nombreMap.get(ParameterBodyWS.B_FLAG_NUEVO_CLIE).getDato1());
				generacion.setDominio(nombreMap.get(ParameterBodyWS.B_DOMINIO).getDato1());
				generacion.setTipoDocumento(nombreMap.get(ParameterBodyWS.B_TIPO_DOCUMENTO).getDato1());
				generacion.setFlagNuevoCliente(flagConsultaPOTC?Constantes.FLAG_TRUE:nombreMap.get(ParameterBodyWS.B_FLAG_NUEVO_CLIE).getDato1());
				generacion.setEmail(correo);
				generacion.setAccion(nombreMap.get(ParameterBodyWS.B_ACCION).getDato1());
				generacion.setOperador(pasoBean.getClienteBean().getCellPhoneOperator());
				generacion.setNumeroCelular(pasoBean.getClienteBean().getCellPhoneNumberOtp());
				List<Parametro> lstParam = new ArrayList<>();
				Parametro param1 = new Parametro(nombreMap.get(ParameterBodyWS.B_KEY1).getDato1(), nombreMap.get(ParameterBodyWS.B_VALUE1).getDato1());
				lstParam.add(param1);
				Parametro param2 = new Parametro(nombreMap.get(ParameterBodyWS.B_KEY2).getDato1(), pasoBean.getClienteBean().getFirstname());
				lstParam.add(param2);
				generacion.setParametros(lstParam);
				if(flagNuevoCliente){
					AltaUsuario altaUsuario = new AltaUsuario(StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, nombreMap.get(ParameterBodyWS.B_NIV_CONF_USU).getDato1());
					generacion.setAltaUsuario(altaUsuario);
				}
				generacion.setFlagNuevoProducto(flagConsultaPOTC?Constantes.FLAG_TRUE:nombreMap.get(ParameterBodyWS.B_FLAG_NUEVO_PROD).getDato1());
				Boolean flagNuevoProducto = Boolean.valueOf(flagConsultaPOTC?Constantes.FLAG_TRUE:nombreMap.get(ParameterBodyWS.B_FLAG_NUEVO_PROD).getDato1());
				if(flagNuevoProducto){
					VinculacionProducto vinculacionProducto = new VinculacionProducto(
							nombreMap.get(ParameterBodyWS.B_COD_EMISOR_PROD).getDato1(), nombreMap.get(ParameterBodyWS.B_TIPO_PROD).getDato1()
							, StringPool.BLANK, pasoBean.getProductoBean().getNumTarjetaProvisional(), pasoBean.getProductoBean().getFechaVencProvisional()
							, pasoBean.getClienteBean().getFirstname(), pasoBean.getProductoBean().getDescripcionMarca()
							, nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(), nombreMap.get(ParameterBodyWS.B_NUM_TRANSACCION).getDato1()
							, pasoBean.getProductoBean().getMarcaProducto(), pasoBean.getProductoBean().getDescripcionTipoProducto()
							, nombreMap.get(ParameterBodyWS.B_DETALLE_PROD).getDato1(), nombreMap.get(ParameterBodyWS.B_NOM_EMISOR_PROD).getDato1()
							, Constantes.FLAG_FALSE, Constantes.FLAG_FALSE);
					generacion.setVinculacionProducto(vinculacionProducto);
				}
				CrearPOTC crearPOTC = new CrearPOTC();
				crearPOTC.setIdProducto(flagNuevoCliente?null:pasoBean.getOtcBean().getIdProducto());
				crearPOTC.setIdProductoUsuario(flagNuevoCliente?null:String.valueOf(pasoBean.getOtcBean().getIdProductoUsuario()));
				String tipoPotc = StringPool.BLANK;
				if(Constantes.TIPO_FLUJO_EXTORNO.equals(pasoBean.getExpedienteBean().getTipoFlujo()))
					tipoPotc = Constantes.TIPO_POTC_EXTORNO;
				else if(Constantes.TIPO_FLUJO_RECOMPRA.equals(pasoBean.getExpedienteBean().getTipoFlujo()))
					tipoPotc = Constantes.TIPO_POTC_RECOMPRA;
				crearPOTC.setTipoPOTC(tipoPotc);
				crearPOTC.setSuitePOTC(StringPool.BLANK);
				crearPOTC.setInformacionValidacionPOTC(StringPool.BLANK);
				generacion.setCrearPOTC(crearPOTC);
				generacion.setVersion(nombreMap.get(ParameterBodyWS.B_VERSION).getDato1());
				HttpHeadersV2  header = new HttpHeadersV2(nombreMap.get(NameParameterWSKey.H_NET_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_CONSUMER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_MESSAGEID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_SUPERV_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_USER_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_SERVER_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_DEVICE_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_BRANCH_CODE).getDato1(), nombreMap.get(NameParameterWSKey.H_REFERENCE_NUMB).getDato1(),
								nombreMap.get(NameParameterWSKey.H_COUNTRY_COD).getDato1(), nombreMap.get(NameParameterWSKey.H_GROUP_MEMB).getDato1(),
								null, nombreMap.get(NameParameterWSKey.H_MODULE_ID).getDato1(),
								nombreMap.get(NameParameterWSKey.H_SERVICE_ID).getDato1(), nombreMap.get(NameParameterWSKey.H_CHANNEL_COD).getDato1(), WebServicesUtil.toXMLGregorianCalendar(new Date()));
				PotcRequest httpRequest = new PotcRequest(header, generacion);
				String requestString = ServicioUtil.generarRequestString(httpRequest);
				ServiceBean serviceBean = new ServiceBean(requestString, pasoBean.getExpedienteBean().getMessageId(), PrefsPropsUtil.getString(PortalKeys.REST_GENERAR_POTC_ENDPOINT, PortalValues.REST_GENERAR_POTC_ENDPOINT), PrefsPropsUtil.getString(PortalKeys.REST_GENERAR_POTC_RESOURCE, PortalValues.REST_GENERAR_POTC_RESOURCE), Constantes.GEN_POTC, Constantes.HTML_ERROR_GENERAR_POTC, Boolean.TRUE, Constantes.HTTP_POST, Boolean.TRUE);
				String response = RestServiceUtil.getSecurityRestClient(serviceBean);
				Gson gsonResponse = new GsonBuilder().setPrettyPrinting().create(); 
				HttpResponseV2 httpResponse = gsonResponse.fromJson(response, HttpResponseV2.class);
				pasoBean.getOtcBean().setPotc(httpResponse.getPotc());
				pasoBean.getOtcBean().setAceptado(httpResponse.getAceptado());
			} catch (SystemException e) {
				throw new WebServicesException(Constantes.GEN_POTC, Constantes.HTML_ERROR_GENERAR_POTC, e);
			}
			return pasoBean;		
		}
		
		@Override
		public void registrarAuditoria(RegistroAuditoriaBean registroAuditoriaBean) throws WebServicesException {
			RegistroAuditoriaRequest registroAuditoriaRequest = new RegistroAuditoriaRequest(
					registroAuditoriaBean.getDestinationId(), registroAuditoriaBean.getMessage(), registroAuditoriaBean.getMessageId()
					, registroAuditoriaBean.getMessageType(), Constantes.SERVICEID_LOG);
			RestRegistroAuditoriaServiceUtil.registroAudtoriaLog(registroAuditoriaRequest);	
		}
		
		@SuppressWarnings("unchecked")
		private Map<String, ParametroHijoPO> getMapNombreParametrosWS(String nameParamPadre, String messageId){
			Map<String, ParametroHijoPO> nombreMap = new HashMap<>();
			try{
				List<ParametroHijoPO> listParam;
				String cacheDistIn = ("myListParam").concat(nameParamPadre).concat("-Cache");
				String cacheDistOut = ("listParam").concat(nameParamPadre);
				listParam = (List<ParametroHijoPO>) MultiVMPoolUtil.getCache(cacheDistIn).get(cacheDistOut);
				if(Validator.isNull(listParam) || !(listParam instanceof ParametroHijoPOImpl)){
					listParam = ParametroHijoPOLocalServiceUtil.getParametroHijosByCodigoPadre(nameParamPadre, QueryUtil.ALL_POS , QueryUtil.ALL_POS);
					MultiVMPoolUtil.getCache(cacheDistIn).put(cacheDistOut, (Serializable)listParam);
				}
				for(ParametroHijoPO obj:listParam)
					nombreMap.put(obj.getCodigo(),obj);
				ParametroHijoPO parametro = ParametroHijoPOLocalServiceUtil.createParametroHijoPO(-1);
				parametro.setDato1(messageId);
				nombreMap.put(Constantes.MESSAGEID,parametro);
			}catch (SystemException e){
				_log.error("Error en metodo getMapNombreParametrosWS::", e);
			}
			return nombreMap;
		}
	}
