/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchTblCodigoPromocionException;
import pe.com.ibk.pepper.model.TblCodigoPromocion;
import pe.com.ibk.pepper.model.impl.TblCodigoPromocionImpl;
import pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the tbl codigo promocion service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see TblCodigoPromocionPersistence
 * @see TblCodigoPromocionUtil
 * @generated
 */
public class TblCodigoPromocionPersistenceImpl extends BasePersistenceImpl<TblCodigoPromocion>
	implements TblCodigoPromocionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TblCodigoPromocionUtil} to access the tbl codigo promocion persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TblCodigoPromocionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED,
			TblCodigoPromocionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED,
			TblCodigoPromocionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CP_E = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED,
			TblCodigoPromocionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCP_E",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_E = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED,
			TblCodigoPromocionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCP_E",
			new String[] { String.class.getName() },
			TblCodigoPromocionModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CP_E = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCP_E",
			new String[] { String.class.getName() });

	/**
	 * Returns all the tbl codigo promocions where estado = &#63;.
	 *
	 * @param estado the estado
	 * @return the matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findByCP_E(String estado)
		throws SystemException {
		return findByCP_E(estado, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tbl codigo promocions where estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param estado the estado
	 * @param start the lower bound of the range of tbl codigo promocions
	 * @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	 * @return the range of matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findByCP_E(String estado, int start, int end)
		throws SystemException {
		return findByCP_E(estado, start, end, null);
	}

	/**
	 * Returns an ordered range of all the tbl codigo promocions where estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param estado the estado
	 * @param start the lower bound of the range of tbl codigo promocions
	 * @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findByCP_E(String estado, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_E;
			finderArgs = new Object[] { estado };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CP_E;
			finderArgs = new Object[] { estado, start, end, orderByComparator };
		}

		List<TblCodigoPromocion> list = (List<TblCodigoPromocion>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TblCodigoPromocion tblCodigoPromocion : list) {
				if (!Validator.equals(estado, tblCodigoPromocion.getEstado())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TBLCODIGOPROMOCION_WHERE);

			boolean bindEstado = false;

			if (estado == null) {
				query.append(_FINDER_COLUMN_CP_E_ESTADO_1);
			}
			else if (estado.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CP_E_ESTADO_3);
			}
			else {
				bindEstado = true;

				query.append(_FINDER_COLUMN_CP_E_ESTADO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TblCodigoPromocionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEstado) {
					qPos.add(estado);
				}

				if (!pagination) {
					list = (List<TblCodigoPromocion>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TblCodigoPromocion>(list);
				}
				else {
					list = (List<TblCodigoPromocion>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first tbl codigo promocion in the ordered set where estado = &#63;.
	 *
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion findByCP_E_First(String estado,
		OrderByComparator orderByComparator)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = fetchByCP_E_First(estado,
				orderByComparator);

		if (tblCodigoPromocion != null) {
			return tblCodigoPromocion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTblCodigoPromocionException(msg.toString());
	}

	/**
	 * Returns the first tbl codigo promocion in the ordered set where estado = &#63;.
	 *
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion fetchByCP_E_First(String estado,
		OrderByComparator orderByComparator) throws SystemException {
		List<TblCodigoPromocion> list = findByCP_E(estado, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last tbl codigo promocion in the ordered set where estado = &#63;.
	 *
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion findByCP_E_Last(String estado,
		OrderByComparator orderByComparator)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = fetchByCP_E_Last(estado,
				orderByComparator);

		if (tblCodigoPromocion != null) {
			return tblCodigoPromocion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTblCodigoPromocionException(msg.toString());
	}

	/**
	 * Returns the last tbl codigo promocion in the ordered set where estado = &#63;.
	 *
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion fetchByCP_E_Last(String estado,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCP_E(estado);

		if (count == 0) {
			return null;
		}

		List<TblCodigoPromocion> list = findByCP_E(estado, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the tbl codigo promocions before and after the current tbl codigo promocion in the ordered set where estado = &#63;.
	 *
	 * @param idCodigo the primary key of the current tbl codigo promocion
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion[] findByCP_E_PrevAndNext(int idCodigo,
		String estado, OrderByComparator orderByComparator)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = findByPrimaryKey(idCodigo);

		Session session = null;

		try {
			session = openSession();

			TblCodigoPromocion[] array = new TblCodigoPromocionImpl[3];

			array[0] = getByCP_E_PrevAndNext(session, tblCodigoPromocion,
					estado, orderByComparator, true);

			array[1] = tblCodigoPromocion;

			array[2] = getByCP_E_PrevAndNext(session, tblCodigoPromocion,
					estado, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TblCodigoPromocion getByCP_E_PrevAndNext(Session session,
		TblCodigoPromocion tblCodigoPromocion, String estado,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TBLCODIGOPROMOCION_WHERE);

		boolean bindEstado = false;

		if (estado == null) {
			query.append(_FINDER_COLUMN_CP_E_ESTADO_1);
		}
		else if (estado.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CP_E_ESTADO_3);
		}
		else {
			bindEstado = true;

			query.append(_FINDER_COLUMN_CP_E_ESTADO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TblCodigoPromocionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindEstado) {
			qPos.add(estado);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(tblCodigoPromocion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TblCodigoPromocion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the tbl codigo promocions where estado = &#63; from the database.
	 *
	 * @param estado the estado
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCP_E(String estado) throws SystemException {
		for (TblCodigoPromocion tblCodigoPromocion : findByCP_E(estado,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(tblCodigoPromocion);
		}
	}

	/**
	 * Returns the number of tbl codigo promocions where estado = &#63;.
	 *
	 * @param estado the estado
	 * @return the number of matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCP_E(String estado) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CP_E;

		Object[] finderArgs = new Object[] { estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TBLCODIGOPROMOCION_WHERE);

			boolean bindEstado = false;

			if (estado == null) {
				query.append(_FINDER_COLUMN_CP_E_ESTADO_1);
			}
			else if (estado.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CP_E_ESTADO_3);
			}
			else {
				bindEstado = true;

				query.append(_FINDER_COLUMN_CP_E_ESTADO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEstado) {
					qPos.add(estado);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CP_E_ESTADO_1 = "tblCodigoPromocion.estado IS NULL";
	private static final String _FINDER_COLUMN_CP_E_ESTADO_2 = "tblCodigoPromocion.estado = ?";
	private static final String _FINDER_COLUMN_CP_E_ESTADO_3 = "(tblCodigoPromocion.estado IS NULL OR tblCodigoPromocion.estado = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CP_C_E = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED,
			TblCodigoPromocionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCP_C_E",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_C_E =
		new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED,
			TblCodigoPromocionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCP_C_E",
			new String[] { String.class.getName(), String.class.getName() },
			TblCodigoPromocionModelImpl.COMERCIO_COLUMN_BITMASK |
			TblCodigoPromocionModelImpl.ESTADO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CP_C_E = new FinderPath(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCP_C_E",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the tbl codigo promocions where comercio = &#63; and estado = &#63;.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @return the matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findByCP_C_E(String comercio, String estado)
		throws SystemException {
		return findByCP_C_E(comercio, estado, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tbl codigo promocions where comercio = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param start the lower bound of the range of tbl codigo promocions
	 * @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	 * @return the range of matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findByCP_C_E(String comercio,
		String estado, int start, int end) throws SystemException {
		return findByCP_C_E(comercio, estado, start, end, null);
	}

	/**
	 * Returns an ordered range of all the tbl codigo promocions where comercio = &#63; and estado = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param start the lower bound of the range of tbl codigo promocions
	 * @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findByCP_C_E(String comercio,
		String estado, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_C_E;
			finderArgs = new Object[] { comercio, estado };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CP_C_E;
			finderArgs = new Object[] {
					comercio, estado,
					
					start, end, orderByComparator
				};
		}

		List<TblCodigoPromocion> list = (List<TblCodigoPromocion>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TblCodigoPromocion tblCodigoPromocion : list) {
				if (!Validator.equals(comercio, tblCodigoPromocion.getComercio()) ||
						!Validator.equals(estado, tblCodigoPromocion.getEstado())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_TBLCODIGOPROMOCION_WHERE);

			boolean bindComercio = false;

			if (comercio == null) {
				query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_1);
			}
			else if (comercio.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_3);
			}
			else {
				bindComercio = true;

				query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_2);
			}

			boolean bindEstado = false;

			if (estado == null) {
				query.append(_FINDER_COLUMN_CP_C_E_ESTADO_1);
			}
			else if (estado.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CP_C_E_ESTADO_3);
			}
			else {
				bindEstado = true;

				query.append(_FINDER_COLUMN_CP_C_E_ESTADO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TblCodigoPromocionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindComercio) {
					qPos.add(comercio);
				}

				if (bindEstado) {
					qPos.add(estado);
				}

				if (!pagination) {
					list = (List<TblCodigoPromocion>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TblCodigoPromocion>(list);
				}
				else {
					list = (List<TblCodigoPromocion>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion findByCP_C_E_First(String comercio,
		String estado, OrderByComparator orderByComparator)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = fetchByCP_C_E_First(comercio,
				estado, orderByComparator);

		if (tblCodigoPromocion != null) {
			return tblCodigoPromocion;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("comercio=");
		msg.append(comercio);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTblCodigoPromocionException(msg.toString());
	}

	/**
	 * Returns the first tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion fetchByCP_C_E_First(String comercio,
		String estado, OrderByComparator orderByComparator)
		throws SystemException {
		List<TblCodigoPromocion> list = findByCP_C_E(comercio, estado, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion findByCP_C_E_Last(String comercio, String estado,
		OrderByComparator orderByComparator)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = fetchByCP_C_E_Last(comercio,
				estado, orderByComparator);

		if (tblCodigoPromocion != null) {
			return tblCodigoPromocion;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("comercio=");
		msg.append(comercio);

		msg.append(", estado=");
		msg.append(estado);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTblCodigoPromocionException(msg.toString());
	}

	/**
	 * Returns the last tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tbl codigo promocion, or <code>null</code> if a matching tbl codigo promocion could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion fetchByCP_C_E_Last(String comercio,
		String estado, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCP_C_E(comercio, estado);

		if (count == 0) {
			return null;
		}

		List<TblCodigoPromocion> list = findByCP_C_E(comercio, estado,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the tbl codigo promocions before and after the current tbl codigo promocion in the ordered set where comercio = &#63; and estado = &#63;.
	 *
	 * @param idCodigo the primary key of the current tbl codigo promocion
	 * @param comercio the comercio
	 * @param estado the estado
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion[] findByCP_C_E_PrevAndNext(int idCodigo,
		String comercio, String estado, OrderByComparator orderByComparator)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = findByPrimaryKey(idCodigo);

		Session session = null;

		try {
			session = openSession();

			TblCodigoPromocion[] array = new TblCodigoPromocionImpl[3];

			array[0] = getByCP_C_E_PrevAndNext(session, tblCodigoPromocion,
					comercio, estado, orderByComparator, true);

			array[1] = tblCodigoPromocion;

			array[2] = getByCP_C_E_PrevAndNext(session, tblCodigoPromocion,
					comercio, estado, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TblCodigoPromocion getByCP_C_E_PrevAndNext(Session session,
		TblCodigoPromocion tblCodigoPromocion, String comercio, String estado,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TBLCODIGOPROMOCION_WHERE);

		boolean bindComercio = false;

		if (comercio == null) {
			query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_1);
		}
		else if (comercio.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_3);
		}
		else {
			bindComercio = true;

			query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_2);
		}

		boolean bindEstado = false;

		if (estado == null) {
			query.append(_FINDER_COLUMN_CP_C_E_ESTADO_1);
		}
		else if (estado.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CP_C_E_ESTADO_3);
		}
		else {
			bindEstado = true;

			query.append(_FINDER_COLUMN_CP_C_E_ESTADO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TblCodigoPromocionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindComercio) {
			qPos.add(comercio);
		}

		if (bindEstado) {
			qPos.add(estado);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(tblCodigoPromocion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TblCodigoPromocion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the tbl codigo promocions where comercio = &#63; and estado = &#63; from the database.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCP_C_E(String comercio, String estado)
		throws SystemException {
		for (TblCodigoPromocion tblCodigoPromocion : findByCP_C_E(comercio,
				estado, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(tblCodigoPromocion);
		}
	}

	/**
	 * Returns the number of tbl codigo promocions where comercio = &#63; and estado = &#63;.
	 *
	 * @param comercio the comercio
	 * @param estado the estado
	 * @return the number of matching tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCP_C_E(String comercio, String estado)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CP_C_E;

		Object[] finderArgs = new Object[] { comercio, estado };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_TBLCODIGOPROMOCION_WHERE);

			boolean bindComercio = false;

			if (comercio == null) {
				query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_1);
			}
			else if (comercio.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_3);
			}
			else {
				bindComercio = true;

				query.append(_FINDER_COLUMN_CP_C_E_COMERCIO_2);
			}

			boolean bindEstado = false;

			if (estado == null) {
				query.append(_FINDER_COLUMN_CP_C_E_ESTADO_1);
			}
			else if (estado.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CP_C_E_ESTADO_3);
			}
			else {
				bindEstado = true;

				query.append(_FINDER_COLUMN_CP_C_E_ESTADO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindComercio) {
					qPos.add(comercio);
				}

				if (bindEstado) {
					qPos.add(estado);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CP_C_E_COMERCIO_1 = "tblCodigoPromocion.comercio IS NULL AND ";
	private static final String _FINDER_COLUMN_CP_C_E_COMERCIO_2 = "tblCodigoPromocion.comercio = ? AND ";
	private static final String _FINDER_COLUMN_CP_C_E_COMERCIO_3 = "(tblCodigoPromocion.comercio IS NULL OR tblCodigoPromocion.comercio = '') AND ";
	private static final String _FINDER_COLUMN_CP_C_E_ESTADO_1 = "tblCodigoPromocion.estado IS NULL";
	private static final String _FINDER_COLUMN_CP_C_E_ESTADO_2 = "tblCodigoPromocion.estado = ?";
	private static final String _FINDER_COLUMN_CP_C_E_ESTADO_3 = "(tblCodigoPromocion.estado IS NULL OR tblCodigoPromocion.estado = '')";

	public TblCodigoPromocionPersistenceImpl() {
		setModelClass(TblCodigoPromocion.class);
	}

	/**
	 * Caches the tbl codigo promocion in the entity cache if it is enabled.
	 *
	 * @param tblCodigoPromocion the tbl codigo promocion
	 */
	@Override
	public void cacheResult(TblCodigoPromocion tblCodigoPromocion) {
		EntityCacheUtil.putResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionImpl.class, tblCodigoPromocion.getPrimaryKey(),
			tblCodigoPromocion);

		tblCodigoPromocion.resetOriginalValues();
	}

	/**
	 * Caches the tbl codigo promocions in the entity cache if it is enabled.
	 *
	 * @param tblCodigoPromocions the tbl codigo promocions
	 */
	@Override
	public void cacheResult(List<TblCodigoPromocion> tblCodigoPromocions) {
		for (TblCodigoPromocion tblCodigoPromocion : tblCodigoPromocions) {
			if (EntityCacheUtil.getResult(
						TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
						TblCodigoPromocionImpl.class,
						tblCodigoPromocion.getPrimaryKey()) == null) {
				cacheResult(tblCodigoPromocion);
			}
			else {
				tblCodigoPromocion.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tbl codigo promocions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TblCodigoPromocionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TblCodigoPromocionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tbl codigo promocion.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TblCodigoPromocion tblCodigoPromocion) {
		EntityCacheUtil.removeResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionImpl.class, tblCodigoPromocion.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<TblCodigoPromocion> tblCodigoPromocions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TblCodigoPromocion tblCodigoPromocion : tblCodigoPromocions) {
			EntityCacheUtil.removeResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
				TblCodigoPromocionImpl.class, tblCodigoPromocion.getPrimaryKey());
		}
	}

	/**
	 * Creates a new tbl codigo promocion with the primary key. Does not add the tbl codigo promocion to the database.
	 *
	 * @param idCodigo the primary key for the new tbl codigo promocion
	 * @return the new tbl codigo promocion
	 */
	@Override
	public TblCodigoPromocion create(int idCodigo) {
		TblCodigoPromocion tblCodigoPromocion = new TblCodigoPromocionImpl();

		tblCodigoPromocion.setNew(true);
		tblCodigoPromocion.setPrimaryKey(idCodigo);

		return tblCodigoPromocion;
	}

	/**
	 * Removes the tbl codigo promocion with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idCodigo the primary key of the tbl codigo promocion
	 * @return the tbl codigo promocion that was removed
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion remove(int idCodigo)
		throws NoSuchTblCodigoPromocionException, SystemException {
		return remove((Serializable)idCodigo);
	}

	/**
	 * Removes the tbl codigo promocion with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tbl codigo promocion
	 * @return the tbl codigo promocion that was removed
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion remove(Serializable primaryKey)
		throws NoSuchTblCodigoPromocionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TblCodigoPromocion tblCodigoPromocion = (TblCodigoPromocion)session.get(TblCodigoPromocionImpl.class,
					primaryKey);

			if (tblCodigoPromocion == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTblCodigoPromocionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tblCodigoPromocion);
		}
		catch (NoSuchTblCodigoPromocionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TblCodigoPromocion removeImpl(
		TblCodigoPromocion tblCodigoPromocion) throws SystemException {
		tblCodigoPromocion = toUnwrappedModel(tblCodigoPromocion);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tblCodigoPromocion)) {
				tblCodigoPromocion = (TblCodigoPromocion)session.get(TblCodigoPromocionImpl.class,
						tblCodigoPromocion.getPrimaryKeyObj());
			}

			if (tblCodigoPromocion != null) {
				session.delete(tblCodigoPromocion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tblCodigoPromocion != null) {
			clearCache(tblCodigoPromocion);
		}

		return tblCodigoPromocion;
	}

	@Override
	public TblCodigoPromocion updateImpl(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion)
		throws SystemException {
		tblCodigoPromocion = toUnwrappedModel(tblCodigoPromocion);

		boolean isNew = tblCodigoPromocion.isNew();

		TblCodigoPromocionModelImpl tblCodigoPromocionModelImpl = (TblCodigoPromocionModelImpl)tblCodigoPromocion;

		Session session = null;

		try {
			session = openSession();

			if (tblCodigoPromocion.isNew()) {
				session.save(tblCodigoPromocion);

				tblCodigoPromocion.setNew(false);
			}
			else {
				session.merge(tblCodigoPromocion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TblCodigoPromocionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((tblCodigoPromocionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						tblCodigoPromocionModelImpl.getOriginalEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CP_E, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_E,
					args);

				args = new Object[] { tblCodigoPromocionModelImpl.getEstado() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CP_E, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_E,
					args);
			}

			if ((tblCodigoPromocionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_C_E.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						tblCodigoPromocionModelImpl.getOriginalComercio(),
						tblCodigoPromocionModelImpl.getOriginalEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CP_C_E, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_C_E,
					args);

				args = new Object[] {
						tblCodigoPromocionModelImpl.getComercio(),
						tblCodigoPromocionModelImpl.getEstado()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CP_C_E, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CP_C_E,
					args);
			}
		}

		EntityCacheUtil.putResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
			TblCodigoPromocionImpl.class, tblCodigoPromocion.getPrimaryKey(),
			tblCodigoPromocion);

		return tblCodigoPromocion;
	}

	protected TblCodigoPromocion toUnwrappedModel(
		TblCodigoPromocion tblCodigoPromocion) {
		if (tblCodigoPromocion instanceof TblCodigoPromocionImpl) {
			return tblCodigoPromocion;
		}

		TblCodigoPromocionImpl tblCodigoPromocionImpl = new TblCodigoPromocionImpl();

		tblCodigoPromocionImpl.setNew(tblCodigoPromocion.isNew());
		tblCodigoPromocionImpl.setPrimaryKey(tblCodigoPromocion.getPrimaryKey());

		tblCodigoPromocionImpl.setIdCodigo(tblCodigoPromocion.getIdCodigo());
		tblCodigoPromocionImpl.setCodigo(tblCodigoPromocion.getCodigo());
		tblCodigoPromocionImpl.setComercio(tblCodigoPromocion.getComercio());
		tblCodigoPromocionImpl.setEstado(tblCodigoPromocion.getEstado());
		tblCodigoPromocionImpl.setFechaRegistro(tblCodigoPromocion.getFechaRegistro());

		return tblCodigoPromocionImpl;
	}

	/**
	 * Returns the tbl codigo promocion with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tbl codigo promocion
	 * @return the tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTblCodigoPromocionException, SystemException {
		TblCodigoPromocion tblCodigoPromocion = fetchByPrimaryKey(primaryKey);

		if (tblCodigoPromocion == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTblCodigoPromocionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return tblCodigoPromocion;
	}

	/**
	 * Returns the tbl codigo promocion with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchTblCodigoPromocionException} if it could not be found.
	 *
	 * @param idCodigo the primary key of the tbl codigo promocion
	 * @return the tbl codigo promocion
	 * @throws pe.com.ibk.pepper.NoSuchTblCodigoPromocionException if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion findByPrimaryKey(int idCodigo)
		throws NoSuchTblCodigoPromocionException, SystemException {
		return findByPrimaryKey((Serializable)idCodigo);
	}

	/**
	 * Returns the tbl codigo promocion with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tbl codigo promocion
	 * @return the tbl codigo promocion, or <code>null</code> if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TblCodigoPromocion tblCodigoPromocion = (TblCodigoPromocion)EntityCacheUtil.getResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
				TblCodigoPromocionImpl.class, primaryKey);

		if (tblCodigoPromocion == _nullTblCodigoPromocion) {
			return null;
		}

		if (tblCodigoPromocion == null) {
			Session session = null;

			try {
				session = openSession();

				tblCodigoPromocion = (TblCodigoPromocion)session.get(TblCodigoPromocionImpl.class,
						primaryKey);

				if (tblCodigoPromocion != null) {
					cacheResult(tblCodigoPromocion);
				}
				else {
					EntityCacheUtil.putResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
						TblCodigoPromocionImpl.class, primaryKey,
						_nullTblCodigoPromocion);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TblCodigoPromocionModelImpl.ENTITY_CACHE_ENABLED,
					TblCodigoPromocionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return tblCodigoPromocion;
	}

	/**
	 * Returns the tbl codigo promocion with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idCodigo the primary key of the tbl codigo promocion
	 * @return the tbl codigo promocion, or <code>null</code> if a tbl codigo promocion with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TblCodigoPromocion fetchByPrimaryKey(int idCodigo)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idCodigo);
	}

	/**
	 * Returns all the tbl codigo promocions.
	 *
	 * @return the tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tbl codigo promocions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tbl codigo promocions
	 * @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	 * @return the range of tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tbl codigo promocions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tbl codigo promocions
	 * @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TblCodigoPromocion> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TblCodigoPromocion> list = (List<TblCodigoPromocion>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TBLCODIGOPROMOCION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TBLCODIGOPROMOCION;

				if (pagination) {
					sql = sql.concat(TblCodigoPromocionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TblCodigoPromocion>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TblCodigoPromocion>(list);
				}
				else {
					list = (List<TblCodigoPromocion>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tbl codigo promocions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TblCodigoPromocion tblCodigoPromocion : findAll()) {
			remove(tblCodigoPromocion);
		}
	}

	/**
	 * Returns the number of tbl codigo promocions.
	 *
	 * @return the number of tbl codigo promocions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TBLCODIGOPROMOCION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the tbl codigo promocion persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.TblCodigoPromocion")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TblCodigoPromocion>> listenersList = new ArrayList<ModelListener<TblCodigoPromocion>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TblCodigoPromocion>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TblCodigoPromocionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TBLCODIGOPROMOCION = "SELECT tblCodigoPromocion FROM TblCodigoPromocion tblCodigoPromocion";
	private static final String _SQL_SELECT_TBLCODIGOPROMOCION_WHERE = "SELECT tblCodigoPromocion FROM TblCodigoPromocion tblCodigoPromocion WHERE ";
	private static final String _SQL_COUNT_TBLCODIGOPROMOCION = "SELECT COUNT(tblCodigoPromocion) FROM TblCodigoPromocion tblCodigoPromocion";
	private static final String _SQL_COUNT_TBLCODIGOPROMOCION_WHERE = "SELECT COUNT(tblCodigoPromocion) FROM TblCodigoPromocion tblCodigoPromocion WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tblCodigoPromocion.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TblCodigoPromocion exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TblCodigoPromocion exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TblCodigoPromocionPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idCodigo", "codigo", "comercio", "estado", "fechaRegistro"
			});
	private static TblCodigoPromocion _nullTblCodigoPromocion = new TblCodigoPromocionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TblCodigoPromocion> toCacheModel() {
				return _nullTblCodigoPromocionCacheModel;
			}
		};

	private static CacheModel<TblCodigoPromocion> _nullTblCodigoPromocionCacheModel =
		new CacheModel<TblCodigoPromocion>() {
			@Override
			public TblCodigoPromocion toEntityModel() {
				return _nullTblCodigoPromocion;
			}
		};
}