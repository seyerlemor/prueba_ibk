/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.AuditoriaCliente;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing AuditoriaCliente in entity cache.
 *
 * @author Interbank
 * @see AuditoriaCliente
 * @generated
 */
public class AuditoriaClienteCacheModel implements CacheModel<AuditoriaCliente>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{idAuditoriaCliente=");
		sb.append(idAuditoriaCliente);
		sb.append(", idClienteTransaccion=");
		sb.append(idClienteTransaccion);
		sb.append(", paso=");
		sb.append(paso);
		sb.append(", pagina=");
		sb.append(pagina);
		sb.append(", tipoFlujo=");
		sb.append(tipoFlujo);
		sb.append(", strJson=");
		sb.append(strJson);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AuditoriaCliente toEntityModel() {
		AuditoriaClienteImpl auditoriaClienteImpl = new AuditoriaClienteImpl();

		auditoriaClienteImpl.setIdAuditoriaCliente(idAuditoriaCliente);
		auditoriaClienteImpl.setIdClienteTransaccion(idClienteTransaccion);

		if (paso == null) {
			auditoriaClienteImpl.setPaso(StringPool.BLANK);
		}
		else {
			auditoriaClienteImpl.setPaso(paso);
		}

		if (pagina == null) {
			auditoriaClienteImpl.setPagina(StringPool.BLANK);
		}
		else {
			auditoriaClienteImpl.setPagina(pagina);
		}

		if (tipoFlujo == null) {
			auditoriaClienteImpl.setTipoFlujo(StringPool.BLANK);
		}
		else {
			auditoriaClienteImpl.setTipoFlujo(tipoFlujo);
		}

		if (strJson == null) {
			auditoriaClienteImpl.setStrJson(StringPool.BLANK);
		}
		else {
			auditoriaClienteImpl.setStrJson(strJson);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			auditoriaClienteImpl.setFechaRegistro(null);
		}
		else {
			auditoriaClienteImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		auditoriaClienteImpl.resetOriginalValues();

		return auditoriaClienteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idAuditoriaCliente = objectInput.readLong();
		idClienteTransaccion = objectInput.readLong();
		paso = objectInput.readUTF();
		pagina = objectInput.readUTF();
		tipoFlujo = objectInput.readUTF();
		strJson = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idAuditoriaCliente);
		objectOutput.writeLong(idClienteTransaccion);

		if (paso == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paso);
		}

		if (pagina == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pagina);
		}

		if (tipoFlujo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoFlujo);
		}

		if (strJson == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(strJson);
		}

		objectOutput.writeLong(fechaRegistro);
	}

	public long idAuditoriaCliente;
	public long idClienteTransaccion;
	public String paso;
	public String pagina;
	public String tipoFlujo;
	public String strJson;
	public long fechaRegistro;
}