package pe.com.ibk.pepper.bean;

public class RequestBean {

	private String requestCalificacionCDA;

	public String getRequestCalificacionCDA() {
		return requestCalificacionCDA;
	}

	public void setRequestCalificacionCDA(String requestCalificacionCDA) {
		this.requestCalificacionCDA = requestCalificacionCDA;
	}

	@Override
	public String toString() {
		return "RequestBean [requestCalificacionCDA=" + requestCalificacionCDA
				+ "]";
	}
}
