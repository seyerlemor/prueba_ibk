package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class ProductoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idExpediente;
	private int idProducto;
	private String tipoProducto;
	private String codigoProducto;
	private String moneda;
	private String marcaProducto;
	private String lineaCredito;
	private String diaDePago;
	private String tasaProducto;
	private String nombreTitularProducto;
	private String tipoSeguro;
	private String costoSeguro;
	private String flagEnvioEECCFisico;
	private String flagEnvioEECCEmail;
	private String terminos;
	private String purchasePolicies;
	private String claveVenta;
	private String codigoOperacion;
	private String fechaHoraOperacion;
	private String idMoneda;
	private String fechaPago;
	private String tipoCliente;
	private String descripcionMarca;
	private String descripcionTipoProducto;
	private String cobroMembresia;
	private String tea;
	private String seguroDesgravamen;
	
	private String codigoUnico;
	private String numTarjetaTitular;
	private String numCuenta;
	private String fechaAltaTitular;
	private String fechaVencTitular;
	private String numTarjetaProvisional;
	private String fechaAltaProvisional;
	private String fechaVencProvisional;
	private String codigoSms;
	private String envioEECC;
	private String flagEnvioToken;
	private String token;
	
	private String emailSaleConstancy;
	private String emailStateAccount;
	
	private String flagSeguroIndividualTarjeta;
	private String flagLPDP;
	
	private int flujoCampana;
	private String codigoCampana;
	private String tipoClienteCampana;		
	private String descripcionContenido;
	private String razonNoUsoTarjeta;
	private String motivo;
	private String lineaCreditoMinima;
	private String cvv;
	private String codigo4csc;
	private String provisionalLinePepper;
	private String dateDelivery;
	private String initTime;
	private String finalTime;
	private String cvvCode;
	private String uniquePromotionalCode;
	private String correlativeCode;

	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getDescripcionContenido() {
		return descripcionContenido;
	}
	public void setDescripcionContenido(String descripcionContenido) {
		this.descripcionContenido = descripcionContenido;
	}
	public String getTerminos() {
		return terminos;
	}
	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}
	public String getCodigoSms() {
		return codigoSms;
	}
	public void setCodigoSms(String codigoSms) {
		this.codigoSms = codigoSms;
	}
	public String getEnvioEECC() {
		return envioEECC;
	}
	public void setEnvioEECC(String envioEECC) {
		this.envioEECC = envioEECC;
	}
	public String getFlagEnvioToken() {
		return flagEnvioToken;
	}
	public void setFlagEnvioToken(String flagEnvioToken) {
		this.flagEnvioToken = flagEnvioToken;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getFlagSeguroIndividualTarjeta() {
		return flagSeguroIndividualTarjeta;
	}
	public void setFlagSeguroIndividualTarjeta(String flagSeguroIndividualTarjeta) {
		this.flagSeguroIndividualTarjeta = flagSeguroIndividualTarjeta;
	}
	public String getFlagLPDP() {
		return flagLPDP;
	}
	public void setFlagLPDP(String flagLPDP) {
		this.flagLPDP = flagLPDP;
	}
	public String getCodigoCampana() {
		return codigoCampana;
	}
	public void setCodigoCampana(String codigoCampana) {
		this.codigoCampana = codigoCampana;
	}
	public String getTipoClienteCampana() {
		return tipoClienteCampana;
	}
	public void setTipoClienteCampana(String tipoClienteCampana) {
		this.tipoClienteCampana = tipoClienteCampana;
	}
	public String getCodigoUnico() {
		return codigoUnico;
	}
	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}
	public String getNumTarjetaTitular() {
		return numTarjetaTitular;
	}
	public void setNumTarjetaTitular(String numTarjetaTitular) {
		this.numTarjetaTitular = numTarjetaTitular;
	}
	public String getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getFechaAltaTitular() {
		return fechaAltaTitular;
	}
	public void setFechaAltaTitular(String fechaAltaTitular) {
		this.fechaAltaTitular = fechaAltaTitular;
	}
	public String getFechaVencTitular() {
		return fechaVencTitular;
	}
	public void setFechaVencTitular(String fechaVencTitular) {
		this.fechaVencTitular = fechaVencTitular;
	}
	public String getNumTarjetaProvisional() {
		return numTarjetaProvisional;
	}
	public void setNumTarjetaProvisional(String numTarjetaProvisional) {
		this.numTarjetaProvisional = numTarjetaProvisional;
	}
	public String getFechaAltaProvisional() {
		return fechaAltaProvisional;
	}
	public void setFechaAltaProvisional(String fechaAltaProvisional) {
		this.fechaAltaProvisional = fechaAltaProvisional;
	}
	public String getFechaVencProvisional() {
		return fechaVencProvisional;
	}
	public void setFechaVencProvisional(String fechaVencProvisional) {
		this.fechaVencProvisional = fechaVencProvisional;
	}
	public int getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public String getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMarcaProducto() {
		return marcaProducto;
	}
	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}
	public String getLineaCredito() {
		return lineaCredito;
	}
	public void setLineaCredito(String lineaCredito) {
		this.lineaCredito = lineaCredito;
	}
	public String getDiaDePago() {
		return diaDePago;
	}
	public void setDiaDePago(String diaDePago) {
		this.diaDePago = diaDePago;
	}
	public String getTasaProducto() {
		return tasaProducto;
	}
	public void setTasaProducto(String tasaProducto) {
		this.tasaProducto = tasaProducto;
	}
	public String getNombreTitularProducto() {
		return nombreTitularProducto;
	}
	public void setNombreTitularProducto(String nombreTitularProducto) {
		this.nombreTitularProducto = nombreTitularProducto;
	}
	public String getTipoSeguro() {
		return tipoSeguro;
	}
	public void setTipoSeguro(String tipoSeguro) {
		this.tipoSeguro = tipoSeguro;
	}
	public String getCostoSeguro() {
		return costoSeguro;
	}
	public void setCostoSeguro(String costoSeguro) {
		this.costoSeguro = costoSeguro;
	}
	public String getFlagEnvioEECCFisico() {
		return flagEnvioEECCFisico;
	}
	public void setFlagEnvioEECCFisico(String flagEnvioEECCFisico) {
		this.flagEnvioEECCFisico = flagEnvioEECCFisico;
	}
	public String getFlagEnvioEECCEmail() {
		return flagEnvioEECCEmail;
	}
	public void setFlagEnvioEECCEmail(String flagEnvioEECCEmail) {
		this.flagEnvioEECCEmail = flagEnvioEECCEmail;
	}
	public String getClaveVenta() {
		return claveVenta;
	}
	public void setClaveVenta(String claveVenta) {
		this.claveVenta = claveVenta;
	}
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	public String getFechaHoraOperacion() {
		return fechaHoraOperacion;
	}
	public void setFechaHoraOperacion(String fechaHoraOperacion) {
		this.fechaHoraOperacion = fechaHoraOperacion;
	}
	public String getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getDescripcionMarca() {
		return descripcionMarca;
	}
	public void setDescripcionMarca(String descripcionMarca) {
		this.descripcionMarca = descripcionMarca;
	}
	public String getDescripcionTipoProducto() {
		return descripcionTipoProducto;
	}
	public void setDescripcionTipoProducto(String descripcionTipoProducto) {
		this.descripcionTipoProducto = descripcionTipoProducto;
	}
	public String getCobroMembresia() {
		return cobroMembresia;
	}
	public void setCobroMembresia(String cobroMembresia) {
		this.cobroMembresia = cobroMembresia;
	}
	public String getTea() {
		return tea;
	}
	public void setTea(String tea) {
		this.tea = tea;
	}
	public String getSeguroDesgravamen() {
		return seguroDesgravamen;
	}
	public void setSeguroDesgravamen(String seguroDesgravamen) {
		this.seguroDesgravamen = seguroDesgravamen;
	}
	public String getRazonNoUsoTarjeta() {
		return razonNoUsoTarjeta;
	}
	public void setRazonNoUsoTarjeta(String razonNoUsoTarjeta) {
		this.razonNoUsoTarjeta = razonNoUsoTarjeta;
	}
	public String getEmailSaleConstancy() {
		return emailSaleConstancy;
	}
	public void setEmailSaleConstancy(String emailSaleConstancy) {
		this.emailSaleConstancy = emailSaleConstancy;
	}
	public String getEmailStateAccount() {
		return emailStateAccount;
	}
	public void setEmailStateAccount(String emailStateAccount) {
		this.emailStateAccount = emailStateAccount;
	}
	public String getPurchasePolicies() {
		return purchasePolicies;
	}
	public void setPurchasePolicies(String purchasePolicies) {
		this.purchasePolicies = purchasePolicies;
	}
	public String getLineaCreditoMinima() {
		return lineaCreditoMinima;
	}
	public void setLineaCreditoMinima(String lineaCreditoMinima) {
		this.lineaCreditoMinima = lineaCreditoMinima;
	}
	
	public int getFlujoCampana() {
		return flujoCampana;
	}
	public void setFlujoCampana(int flujoCampana) {
		this.flujoCampana = flujoCampana;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getCodigo4csc() {
		return codigo4csc;
	}
	public void setCodigo4csc(String codigo4csc) {
		this.codigo4csc = codigo4csc;
	}
	public String getProvisionalLinePepper() {
		return provisionalLinePepper;
	}
	public void setProvisionalLinePepper(String provisionalLinePepper) {
		this.provisionalLinePepper = provisionalLinePepper;
	}
	public String getDateDelivery() {
		return dateDelivery;
	}
	public void setDateDelivery(String dateDelivery) {
		this.dateDelivery = dateDelivery;
	}
	public String getInitTime() {
		return initTime;
	}
	public void setInitTime(String initTime) {
		this.initTime = initTime;
	}
	public String getFinalTime() {
		return finalTime;
	}
	public void setFinalTime(String finalTime) {
		this.finalTime = finalTime;
	}
	public String getCvvCode() {
		return cvvCode;
	}
	public void setCvvCode(String cvvCode) {
		this.cvvCode = cvvCode;
	}
	public String getUniquePromotionalCode() {
		return uniquePromotionalCode;
	}
	public void setUniquePromotionalCode(String uniquePromotionalCode) {
		this.uniquePromotionalCode = uniquePromotionalCode;
	}
	
	public String getCorrelativeCode() {
		return correlativeCode;
	}
	public void setCorrelativeCode(String correlativeCode) {
		this.correlativeCode = correlativeCode;
	}
	@Override
	public String toString() {
		return "ProductoBean [idExpediente=" + idExpediente + ", idProducto="
				+ idProducto + ", tipoProducto=" + tipoProducto
				+ ", codigoProducto=" + codigoProducto + ", moneda=" + moneda
				+ ", marcaProducto=" + marcaProducto + ", lineaCredito="
				+ lineaCredito + ", diaDePago=" + diaDePago + ", tasaProducto="
				+ tasaProducto + ", nombreTitularProducto="
				+ nombreTitularProducto + ", tipoSeguro=" + tipoSeguro
				+ ", costoSeguro=" + costoSeguro + ", flagEnvioEECCFisico="
				+ flagEnvioEECCFisico + ", flagEnvioEECCEmail="
				+ flagEnvioEECCEmail + ", terminos=" + terminos
				+ ", purchasePolicies=" + purchasePolicies + ", claveVenta="
				+ claveVenta + ", codigoOperacion=" + codigoOperacion
				+ ", fechaHoraOperacion=" + fechaHoraOperacion + ", idMoneda="
				+ idMoneda + ", fechaPago=" + fechaPago + ", tipoCliente="
				+ tipoCliente + ", descripcionMarca=" + descripcionMarca
				+ ", descripcionTipoProducto=" + descripcionTipoProducto
				+ ", cobroMembresia=" + cobroMembresia + ", tea=" + tea
				+ ", seguroDesgravamen=" + seguroDesgravamen + ", codigoUnico="
				+ codigoUnico + ", numTarjetaTitular=" + numTarjetaTitular
				+ ", numCuenta=" + numCuenta + ", fechaAltaTitular="
				+ fechaAltaTitular + ", fechaVencTitular=" + fechaVencTitular
				+ ", numTarjetaProvisional=" + numTarjetaProvisional
				+ ", fechaAltaProvisional=" + fechaAltaProvisional
				+ ", fechaVencProvisional=" + fechaVencProvisional
				+ ", codigoSms=" + codigoSms + ", envioEECC=" + envioEECC
				+ ", flagEnvioToken=" + flagEnvioToken + ", token=" + token
				+ ", emailSaleConstancy=" + emailSaleConstancy
				+ ", emailStateAccount=" + emailStateAccount
				+ ", flagSeguroIndividualTarjeta="
				+ flagSeguroIndividualTarjeta + ", flagLPDP=" + flagLPDP
				+ ", flujoCampana=" + flujoCampana + ", codigoCampana="
				+ codigoCampana + ", tipoClienteCampana=" + tipoClienteCampana
				+ ", descripcionContenido=" + descripcionContenido
				+ ", razonNoUsoTarjeta=" + razonNoUsoTarjeta + ", motivo="
				+ motivo + ", lineaCreditoMinima=" + lineaCreditoMinima
				+ ", cvv=" + cvv + ", codigo4csc=" + codigo4csc
				+ ", provisionalLinePepper=" + provisionalLinePepper
				+ ", dateDelivery=" + dateDelivery + ", initTime=" + initTime
				+ ", finalTime=" + finalTime + ", cvvCode=" + cvvCode
				+ ", uniquePromotionalCode=" + uniquePromotionalCode + "]";
	}
	
}
