/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchServiciosException;
import pe.com.ibk.pepper.model.Servicios;
import pe.com.ibk.pepper.model.impl.ServiciosImpl;
import pe.com.ibk.pepper.model.impl.ServiciosModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the servicios service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see ServiciosPersistence
 * @see ServiciosUtil
 * @generated
 */
public class ServiciosPersistenceImpl extends BasePersistenceImpl<Servicios>
	implements ServiciosPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ServiciosUtil} to access the servicios persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ServiciosImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, ServiciosImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, ServiciosImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_S_ID = new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, ServiciosImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByS_ID",
			new String[] { Long.class.getName() },
			ServiciosModelImpl.IDSERVICIO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_S_ID = new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByS_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the servicios where idServicio = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchServiciosException} if it could not be found.
	 *
	 * @param idServicio the id servicio
	 * @return the matching servicios
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios findByS_ID(long idServicio)
		throws NoSuchServiciosException, SystemException {
		Servicios servicios = fetchByS_ID(idServicio);

		if (servicios == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idServicio=");
			msg.append(idServicio);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchServiciosException(msg.toString());
		}

		return servicios;
	}

	/**
	 * Returns the servicios where idServicio = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idServicio the id servicio
	 * @return the matching servicios, or <code>null</code> if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios fetchByS_ID(long idServicio) throws SystemException {
		return fetchByS_ID(idServicio, true);
	}

	/**
	 * Returns the servicios where idServicio = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idServicio the id servicio
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching servicios, or <code>null</code> if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios fetchByS_ID(long idServicio, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { idServicio };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_S_ID,
					finderArgs, this);
		}

		if (result instanceof Servicios) {
			Servicios servicios = (Servicios)result;

			if ((idServicio != servicios.getIdServicio())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_SERVICIOS_WHERE);

			query.append(_FINDER_COLUMN_S_ID_IDSERVICIO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idServicio);

				List<Servicios> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ServiciosPersistenceImpl.fetchByS_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Servicios servicios = list.get(0);

					result = servicios;

					cacheResult(servicios);

					if ((servicios.getIdServicio() != idServicio)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_ID,
							finderArgs, servicios);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_S_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Servicios)result;
		}
	}

	/**
	 * Removes the servicios where idServicio = &#63; from the database.
	 *
	 * @param idServicio the id servicio
	 * @return the servicios that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios removeByS_ID(long idServicio)
		throws NoSuchServiciosException, SystemException {
		Servicios servicios = findByS_ID(idServicio);

		return remove(servicios);
	}

	/**
	 * Returns the number of servicioses where idServicio = &#63;.
	 *
	 * @param idServicio the id servicio
	 * @return the number of matching servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByS_ID(long idServicio) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_S_ID;

		Object[] finderArgs = new Object[] { idServicio };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SERVICIOS_WHERE);

			query.append(_FINDER_COLUMN_S_ID_IDSERVICIO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idServicio);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_S_ID_IDSERVICIO_2 = "servicios.idServicio = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEXPEDIENTE =
		new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, ServiciosImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdExpediente",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE =
		new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, ServiciosImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdExpediente",
			new String[] { Long.class.getName(), String.class.getName() },
			ServiciosModelImpl.IDEXPEDIENTE_COLUMN_BITMASK |
			ServiciosModelImpl.TIPO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEXPEDIENTE = new FinderPath(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdExpediente",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the servicioses where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @return the matching servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Servicios> findByIdExpediente(long idExpediente, String tipo)
		throws SystemException {
		return findByIdExpediente(idExpediente, tipo, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the servicioses where idExpediente = &#63; and tipo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param start the lower bound of the range of servicioses
	 * @param end the upper bound of the range of servicioses (not inclusive)
	 * @return the range of matching servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Servicios> findByIdExpediente(long idExpediente, String tipo,
		int start, int end) throws SystemException {
		return findByIdExpediente(idExpediente, tipo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the servicioses where idExpediente = &#63; and tipo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param start the lower bound of the range of servicioses
	 * @param end the upper bound of the range of servicioses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Servicios> findByIdExpediente(long idExpediente, String tipo,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE;
			finderArgs = new Object[] { idExpediente, tipo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEXPEDIENTE;
			finderArgs = new Object[] {
					idExpediente, tipo,
					
					start, end, orderByComparator
				};
		}

		List<Servicios> list = (List<Servicios>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Servicios servicios : list) {
				if ((idExpediente != servicios.getIdExpediente()) ||
						!Validator.equals(tipo, servicios.getTipo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_SERVICIOS_WHERE);

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

			boolean bindTipo = false;

			if (tipo == null) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_1);
			}
			else if (tipo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_3);
			}
			else {
				bindTipo = true;

				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ServiciosModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				if (bindTipo) {
					qPos.add(tipo);
				}

				if (!pagination) {
					list = (List<Servicios>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Servicios>(list);
				}
				else {
					list = (List<Servicios>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching servicios
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios findByIdExpediente_First(long idExpediente, String tipo,
		OrderByComparator orderByComparator)
		throws NoSuchServiciosException, SystemException {
		Servicios servicios = fetchByIdExpediente_First(idExpediente, tipo,
				orderByComparator);

		if (servicios != null) {
			return servicios;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idExpediente=");
		msg.append(idExpediente);

		msg.append(", tipo=");
		msg.append(tipo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchServiciosException(msg.toString());
	}

	/**
	 * Returns the first servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching servicios, or <code>null</code> if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios fetchByIdExpediente_First(long idExpediente, String tipo,
		OrderByComparator orderByComparator) throws SystemException {
		List<Servicios> list = findByIdExpediente(idExpediente, tipo, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching servicios
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios findByIdExpediente_Last(long idExpediente, String tipo,
		OrderByComparator orderByComparator)
		throws NoSuchServiciosException, SystemException {
		Servicios servicios = fetchByIdExpediente_Last(idExpediente, tipo,
				orderByComparator);

		if (servicios != null) {
			return servicios;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("idExpediente=");
		msg.append(idExpediente);

		msg.append(", tipo=");
		msg.append(tipo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchServiciosException(msg.toString());
	}

	/**
	 * Returns the last servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching servicios, or <code>null</code> if a matching servicios could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios fetchByIdExpediente_Last(long idExpediente, String tipo,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdExpediente(idExpediente, tipo);

		if (count == 0) {
			return null;
		}

		List<Servicios> list = findByIdExpediente(idExpediente, tipo,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the servicioses before and after the current servicios in the ordered set where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idServicio the primary key of the current servicios
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next servicios
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios[] findByIdExpediente_PrevAndNext(long idServicio,
		long idExpediente, String tipo, OrderByComparator orderByComparator)
		throws NoSuchServiciosException, SystemException {
		Servicios servicios = findByPrimaryKey(idServicio);

		Session session = null;

		try {
			session = openSession();

			Servicios[] array = new ServiciosImpl[3];

			array[0] = getByIdExpediente_PrevAndNext(session, servicios,
					idExpediente, tipo, orderByComparator, true);

			array[1] = servicios;

			array[2] = getByIdExpediente_PrevAndNext(session, servicios,
					idExpediente, tipo, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Servicios getByIdExpediente_PrevAndNext(Session session,
		Servicios servicios, long idExpediente, String tipo,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SERVICIOS_WHERE);

		query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

		boolean bindTipo = false;

		if (tipo == null) {
			query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_1);
		}
		else if (tipo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_3);
		}
		else {
			bindTipo = true;

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ServiciosModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(idExpediente);

		if (bindTipo) {
			qPos.add(tipo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(servicios);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Servicios> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the servicioses where idExpediente = &#63; and tipo = &#63; from the database.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdExpediente(long idExpediente, String tipo)
		throws SystemException {
		for (Servicios servicios : findByIdExpediente(idExpediente, tipo,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(servicios);
		}
	}

	/**
	 * Returns the number of servicioses where idExpediente = &#63; and tipo = &#63;.
	 *
	 * @param idExpediente the id expediente
	 * @param tipo the tipo
	 * @return the number of matching servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdExpediente(long idExpediente, String tipo)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEXPEDIENTE;

		Object[] finderArgs = new Object[] { idExpediente, tipo };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SERVICIOS_WHERE);

			query.append(_FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2);

			boolean bindTipo = false;

			if (tipo == null) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_1);
			}
			else if (tipo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_3);
			}
			else {
				bindTipo = true;

				query.append(_FINDER_COLUMN_IDEXPEDIENTE_TIPO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idExpediente);

				if (bindTipo) {
					qPos.add(tipo);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEXPEDIENTE_IDEXPEDIENTE_2 = "servicios.idExpediente = ? AND ";
	private static final String _FINDER_COLUMN_IDEXPEDIENTE_TIPO_1 = "servicios.tipo IS NULL";
	private static final String _FINDER_COLUMN_IDEXPEDIENTE_TIPO_2 = "servicios.tipo = ?";
	private static final String _FINDER_COLUMN_IDEXPEDIENTE_TIPO_3 = "(servicios.tipo IS NULL OR servicios.tipo = '')";

	public ServiciosPersistenceImpl() {
		setModelClass(Servicios.class);
	}

	/**
	 * Caches the servicios in the entity cache if it is enabled.
	 *
	 * @param servicios the servicios
	 */
	@Override
	public void cacheResult(Servicios servicios) {
		EntityCacheUtil.putResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosImpl.class, servicios.getPrimaryKey(), servicios);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_ID,
			new Object[] { servicios.getIdServicio() }, servicios);

		servicios.resetOriginalValues();
	}

	/**
	 * Caches the servicioses in the entity cache if it is enabled.
	 *
	 * @param servicioses the servicioses
	 */
	@Override
	public void cacheResult(List<Servicios> servicioses) {
		for (Servicios servicios : servicioses) {
			if (EntityCacheUtil.getResult(
						ServiciosModelImpl.ENTITY_CACHE_ENABLED,
						ServiciosImpl.class, servicios.getPrimaryKey()) == null) {
				cacheResult(servicios);
			}
			else {
				servicios.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all servicioses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ServiciosImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ServiciosImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the servicios.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Servicios servicios) {
		EntityCacheUtil.removeResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosImpl.class, servicios.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(servicios);
	}

	@Override
	public void clearCache(List<Servicios> servicioses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Servicios servicios : servicioses) {
			EntityCacheUtil.removeResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
				ServiciosImpl.class, servicios.getPrimaryKey());

			clearUniqueFindersCache(servicios);
		}
	}

	protected void cacheUniqueFindersCache(Servicios servicios) {
		if (servicios.isNew()) {
			Object[] args = new Object[] { servicios.getIdServicio() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_S_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_ID, args, servicios);
		}
		else {
			ServiciosModelImpl serviciosModelImpl = (ServiciosModelImpl)servicios;

			if ((serviciosModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_S_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { servicios.getIdServicio() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_S_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_ID, args,
					servicios);
			}
		}
	}

	protected void clearUniqueFindersCache(Servicios servicios) {
		ServiciosModelImpl serviciosModelImpl = (ServiciosModelImpl)servicios;

		Object[] args = new Object[] { servicios.getIdServicio() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_S_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_S_ID, args);

		if ((serviciosModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_S_ID.getColumnBitmask()) != 0) {
			args = new Object[] { serviciosModelImpl.getOriginalIdServicio() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_S_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_S_ID, args);
		}
	}

	/**
	 * Creates a new servicios with the primary key. Does not add the servicios to the database.
	 *
	 * @param idServicio the primary key for the new servicios
	 * @return the new servicios
	 */
	@Override
	public Servicios create(long idServicio) {
		Servicios servicios = new ServiciosImpl();

		servicios.setNew(true);
		servicios.setPrimaryKey(idServicio);

		return servicios;
	}

	/**
	 * Removes the servicios with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idServicio the primary key of the servicios
	 * @return the servicios that was removed
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios remove(long idServicio)
		throws NoSuchServiciosException, SystemException {
		return remove((Serializable)idServicio);
	}

	/**
	 * Removes the servicios with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the servicios
	 * @return the servicios that was removed
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios remove(Serializable primaryKey)
		throws NoSuchServiciosException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Servicios servicios = (Servicios)session.get(ServiciosImpl.class,
					primaryKey);

			if (servicios == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchServiciosException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(servicios);
		}
		catch (NoSuchServiciosException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Servicios removeImpl(Servicios servicios)
		throws SystemException {
		servicios = toUnwrappedModel(servicios);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(servicios)) {
				servicios = (Servicios)session.get(ServiciosImpl.class,
						servicios.getPrimaryKeyObj());
			}

			if (servicios != null) {
				session.delete(servicios);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (servicios != null) {
			clearCache(servicios);
		}

		return servicios;
	}

	@Override
	public Servicios updateImpl(pe.com.ibk.pepper.model.Servicios servicios)
		throws SystemException {
		servicios = toUnwrappedModel(servicios);

		boolean isNew = servicios.isNew();

		ServiciosModelImpl serviciosModelImpl = (ServiciosModelImpl)servicios;

		Session session = null;

		try {
			session = openSession();

			if (servicios.isNew()) {
				session.save(servicios);

				servicios.setNew(false);
			}
			else {
				session.merge(servicios);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ServiciosModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((serviciosModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						serviciosModelImpl.getOriginalIdExpediente(),
						serviciosModelImpl.getOriginalTipo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEXPEDIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE,
					args);

				args = new Object[] {
						serviciosModelImpl.getIdExpediente(),
						serviciosModelImpl.getTipo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEXPEDIENTE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEXPEDIENTE,
					args);
			}
		}

		EntityCacheUtil.putResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
			ServiciosImpl.class, servicios.getPrimaryKey(), servicios);

		clearUniqueFindersCache(servicios);
		cacheUniqueFindersCache(servicios);

		return servicios;
	}

	protected Servicios toUnwrappedModel(Servicios servicios) {
		if (servicios instanceof ServiciosImpl) {
			return servicios;
		}

		ServiciosImpl serviciosImpl = new ServiciosImpl();

		serviciosImpl.setNew(servicios.isNew());
		serviciosImpl.setPrimaryKey(servicios.getPrimaryKey());

		serviciosImpl.setIdServicio(servicios.getIdServicio());
		serviciosImpl.setIdExpediente(servicios.getIdExpediente());
		serviciosImpl.setNombre(servicios.getNombre());
		serviciosImpl.setCodigo(servicios.getCodigo());
		serviciosImpl.setDescripcion(servicios.getDescripcion());
		serviciosImpl.setResponse(servicios.getResponse());
		serviciosImpl.setMessageBus(servicios.getMessageBus());
		serviciosImpl.setTipo(servicios.getTipo());
		serviciosImpl.setFechaRegistro(servicios.getFechaRegistro());
		serviciosImpl.setRespuestaInterna(servicios.getRespuestaInterna());

		return serviciosImpl;
	}

	/**
	 * Returns the servicios with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the servicios
	 * @return the servicios
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios findByPrimaryKey(Serializable primaryKey)
		throws NoSuchServiciosException, SystemException {
		Servicios servicios = fetchByPrimaryKey(primaryKey);

		if (servicios == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchServiciosException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return servicios;
	}

	/**
	 * Returns the servicios with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchServiciosException} if it could not be found.
	 *
	 * @param idServicio the primary key of the servicios
	 * @return the servicios
	 * @throws pe.com.ibk.pepper.NoSuchServiciosException if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios findByPrimaryKey(long idServicio)
		throws NoSuchServiciosException, SystemException {
		return findByPrimaryKey((Serializable)idServicio);
	}

	/**
	 * Returns the servicios with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the servicios
	 * @return the servicios, or <code>null</code> if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Servicios servicios = (Servicios)EntityCacheUtil.getResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
				ServiciosImpl.class, primaryKey);

		if (servicios == _nullServicios) {
			return null;
		}

		if (servicios == null) {
			Session session = null;

			try {
				session = openSession();

				servicios = (Servicios)session.get(ServiciosImpl.class,
						primaryKey);

				if (servicios != null) {
					cacheResult(servicios);
				}
				else {
					EntityCacheUtil.putResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
						ServiciosImpl.class, primaryKey, _nullServicios);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ServiciosModelImpl.ENTITY_CACHE_ENABLED,
					ServiciosImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return servicios;
	}

	/**
	 * Returns the servicios with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idServicio the primary key of the servicios
	 * @return the servicios, or <code>null</code> if a servicios with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Servicios fetchByPrimaryKey(long idServicio)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idServicio);
	}

	/**
	 * Returns all the servicioses.
	 *
	 * @return the servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Servicios> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the servicioses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of servicioses
	 * @param end the upper bound of the range of servicioses (not inclusive)
	 * @return the range of servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Servicios> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the servicioses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ServiciosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of servicioses
	 * @param end the upper bound of the range of servicioses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Servicios> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Servicios> list = (List<Servicios>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SERVICIOS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SERVICIOS;

				if (pagination) {
					sql = sql.concat(ServiciosModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Servicios>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Servicios>(list);
				}
				else {
					list = (List<Servicios>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the servicioses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Servicios servicios : findAll()) {
			remove(servicios);
		}
	}

	/**
	 * Returns the number of servicioses.
	 *
	 * @return the number of servicioses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SERVICIOS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the servicios persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.Servicios")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Servicios>> listenersList = new ArrayList<ModelListener<Servicios>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Servicios>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ServiciosImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_SERVICIOS = "SELECT servicios FROM Servicios servicios";
	private static final String _SQL_SELECT_SERVICIOS_WHERE = "SELECT servicios FROM Servicios servicios WHERE ";
	private static final String _SQL_COUNT_SERVICIOS = "SELECT COUNT(servicios) FROM Servicios servicios";
	private static final String _SQL_COUNT_SERVICIOS_WHERE = "SELECT COUNT(servicios) FROM Servicios servicios WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "servicios.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Servicios exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Servicios exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ServiciosPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idServicio", "idExpediente", "nombre", "codigo", "descripcion",
				"response", "messageBus", "tipo", "fechaRegistro",
				"respuestaInterna"
			});
	private static Servicios _nullServicios = new ServiciosImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Servicios> toCacheModel() {
				return _nullServiciosCacheModel;
			}
		};

	private static CacheModel<Servicios> _nullServiciosCacheModel = new CacheModel<Servicios>() {
			@Override
			public Servicios toEntityModel() {
				return _nullServicios;
			}
		};
}