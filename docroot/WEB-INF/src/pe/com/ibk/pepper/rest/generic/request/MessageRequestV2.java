package pe.com.ibk.pepper.rest.generic.request;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.ibk.pepper.rest.generic.request.Header;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MessageRequestV2 {

	private Header header;
	private BodyV2 body;

	@JsonCreator
	public MessageRequestV2(Header header, BodyV2 body) {
		super();
		this.header = header;
		this.body = body;
	}

	@JsonProperty("Header")
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	@JsonProperty("Body")
	public BodyV2 getBody() {
		return body;
	}

	public void setBody(BodyV2 body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "MessageRequest_ [header=" + header + ", body=" + body + "]";
	}

	
}
