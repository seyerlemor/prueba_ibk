/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Interbank
 * @generated
 */
public class ParametroPadrePluginSoap implements Serializable {
	public static ParametroPadrePluginSoap toSoapModel(
		ParametroPadrePlugin model) {
		ParametroPadrePluginSoap soapModel = new ParametroPadrePluginSoap();

		soapModel.setIdParametroPadre(model.getIdParametroPadre());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCodigoPadre(model.getCodigoPadre());
		soapModel.setNombre(model.getNombre());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setEstado(model.getEstado());
		soapModel.setJson(model.getJson());

		return soapModel;
	}

	public static ParametroPadrePluginSoap[] toSoapModels(
		ParametroPadrePlugin[] models) {
		ParametroPadrePluginSoap[] soapModels = new ParametroPadrePluginSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ParametroPadrePluginSoap[][] toSoapModels(
		ParametroPadrePlugin[][] models) {
		ParametroPadrePluginSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ParametroPadrePluginSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ParametroPadrePluginSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ParametroPadrePluginSoap[] toSoapModels(
		List<ParametroPadrePlugin> models) {
		List<ParametroPadrePluginSoap> soapModels = new ArrayList<ParametroPadrePluginSoap>(models.size());

		for (ParametroPadrePlugin model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ParametroPadrePluginSoap[soapModels.size()]);
	}

	public ParametroPadrePluginSoap() {
	}

	public long getPrimaryKey() {
		return _idParametroPadre;
	}

	public void setPrimaryKey(long pk) {
		setIdParametroPadre(pk);
	}

	public long getIdParametroPadre() {
		return _idParametroPadre;
	}

	public void setIdParametroPadre(long idParametroPadre) {
		_idParametroPadre = idParametroPadre;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getCodigoPadre() {
		return _codigoPadre;
	}

	public void setCodigoPadre(String codigoPadre) {
		_codigoPadre = codigoPadre;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public boolean getEstado() {
		return _estado;
	}

	public boolean isEstado() {
		return _estado;
	}

	public void setEstado(boolean estado) {
		_estado = estado;
	}

	public String getJson() {
		return _json;
	}

	public void setJson(String json) {
		_json = json;
	}

	private long _idParametroPadre;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _codigoPadre;
	private String _nombre;
	private String _descripcion;
	private boolean _estado;
	private String _json;
}