
package pe.com.ibk.pepper.rest.generic.response;


public class HeaderResponse {

    private String timestamp;
    private Status status;

    public HeaderResponse() {
		super();
	}

	public HeaderResponse(String timestamp, Status status) {
		super();
		this.timestamp = timestamp;
		this.status = status;
	}
	
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
	@Override
	public String toString() {
		return "HeaderResponse [timestamp=" + timestamp + ", status=" + status
				+ "]";
	}

}
