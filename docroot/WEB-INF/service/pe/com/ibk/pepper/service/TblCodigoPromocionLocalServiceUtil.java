/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for TblCodigoPromocion. This utility wraps
 * {@link pe.com.ibk.pepper.service.impl.TblCodigoPromocionLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Interbank
 * @see TblCodigoPromocionLocalService
 * @see pe.com.ibk.pepper.service.base.TblCodigoPromocionLocalServiceBaseImpl
 * @see pe.com.ibk.pepper.service.impl.TblCodigoPromocionLocalServiceImpl
 * @generated
 */
public class TblCodigoPromocionLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link pe.com.ibk.pepper.service.impl.TblCodigoPromocionLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the tbl codigo promocion to the database. Also notifies the appropriate model listeners.
	*
	* @param tblCodigoPromocion the tbl codigo promocion
	* @return the tbl codigo promocion that was added
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblCodigoPromocion addTblCodigoPromocion(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addTblCodigoPromocion(tblCodigoPromocion);
	}

	/**
	* Creates a new tbl codigo promocion with the primary key. Does not add the tbl codigo promocion to the database.
	*
	* @param idCodigo the primary key for the new tbl codigo promocion
	* @return the new tbl codigo promocion
	*/
	public static pe.com.ibk.pepper.model.TblCodigoPromocion createTblCodigoPromocion(
		int idCodigo) {
		return getService().createTblCodigoPromocion(idCodigo);
	}

	/**
	* Deletes the tbl codigo promocion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idCodigo the primary key of the tbl codigo promocion
	* @return the tbl codigo promocion that was removed
	* @throws PortalException if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblCodigoPromocion deleteTblCodigoPromocion(
		int idCodigo)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteTblCodigoPromocion(idCodigo);
	}

	/**
	* Deletes the tbl codigo promocion from the database. Also notifies the appropriate model listeners.
	*
	* @param tblCodigoPromocion the tbl codigo promocion
	* @return the tbl codigo promocion that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblCodigoPromocion deleteTblCodigoPromocion(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteTblCodigoPromocion(tblCodigoPromocion);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static pe.com.ibk.pepper.model.TblCodigoPromocion fetchTblCodigoPromocion(
		int idCodigo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchTblCodigoPromocion(idCodigo);
	}

	/**
	* Returns the tbl codigo promocion with the primary key.
	*
	* @param idCodigo the primary key of the tbl codigo promocion
	* @return the tbl codigo promocion
	* @throws PortalException if a tbl codigo promocion with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblCodigoPromocion getTblCodigoPromocion(
		int idCodigo)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getTblCodigoPromocion(idCodigo);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the tbl codigo promocions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.TblCodigoPromocionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tbl codigo promocions
	* @param end the upper bound of the range of tbl codigo promocions (not inclusive)
	* @return the range of tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> getTblCodigoPromocions(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTblCodigoPromocions(start, end);
	}

	/**
	* Returns the number of tbl codigo promocions.
	*
	* @return the number of tbl codigo promocions
	* @throws SystemException if a system exception occurred
	*/
	public static int getTblCodigoPromocionsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTblCodigoPromocionsCount();
	}

	/**
	* Updates the tbl codigo promocion in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param tblCodigoPromocion the tbl codigo promocion
	* @return the tbl codigo promocion that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static pe.com.ibk.pepper.model.TblCodigoPromocion updateTblCodigoPromocion(
		pe.com.ibk.pepper.model.TblCodigoPromocion tblCodigoPromocion)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateTblCodigoPromocion(tblCodigoPromocion);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<pe.com.ibk.pepper.model.TblCodigoPromocion> listarCodigoPromocion(
		java.lang.String estado) {
		return getService().listarCodigoPromocion(estado);
	}

	public static pe.com.ibk.pepper.model.TblCodigoPromocion obtenerCodigoPromocion(
		java.lang.String estado) {
		return getService().obtenerCodigoPromocion(estado);
	}

	public static pe.com.ibk.pepper.model.TblCodigoPromocion obtenerCodigoPromocionByComercio(
		java.lang.String estado, java.lang.String comercio) {
		return getService().obtenerCodigoPromocionByComercio(estado, comercio);
	}

	public static void clearService() {
		_service = null;
	}

	public static TblCodigoPromocionLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					TblCodigoPromocionLocalService.class.getName());

			if (invokableLocalService instanceof TblCodigoPromocionLocalService) {
				_service = (TblCodigoPromocionLocalService)invokableLocalService;
			}
			else {
				_service = new TblCodigoPromocionLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(TblCodigoPromocionLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(TblCodigoPromocionLocalService service) {
	}

	private static TblCodigoPromocionLocalService _service;
}