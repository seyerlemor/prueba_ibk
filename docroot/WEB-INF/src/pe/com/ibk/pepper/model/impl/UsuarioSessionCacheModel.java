/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.UsuarioSession;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UsuarioSession in entity cache.
 *
 * @author Interbank
 * @see UsuarioSession
 * @generated
 */
public class UsuarioSessionCacheModel implements CacheModel<UsuarioSession>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{idUsuarioSession=");
		sb.append(idUsuarioSession);
		sb.append(", idSession=");
		sb.append(idSession);
		sb.append(", fechaRegistro=");
		sb.append(fechaRegistro);
		sb.append(", tienda=");
		sb.append(tienda);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", tiendaId=");
		sb.append(tiendaId);
		sb.append(", establecimiento=");
		sb.append(establecimiento);
		sb.append(", nombreVendedor=");
		sb.append(nombreVendedor);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UsuarioSession toEntityModel() {
		UsuarioSessionImpl usuarioSessionImpl = new UsuarioSessionImpl();

		usuarioSessionImpl.setIdUsuarioSession(idUsuarioSession);

		if (idSession == null) {
			usuarioSessionImpl.setIdSession(StringPool.BLANK);
		}
		else {
			usuarioSessionImpl.setIdSession(idSession);
		}

		if (fechaRegistro == Long.MIN_VALUE) {
			usuarioSessionImpl.setFechaRegistro(null);
		}
		else {
			usuarioSessionImpl.setFechaRegistro(new Date(fechaRegistro));
		}

		if (tienda == null) {
			usuarioSessionImpl.setTienda(StringPool.BLANK);
		}
		else {
			usuarioSessionImpl.setTienda(tienda);
		}

		if (estado == null) {
			usuarioSessionImpl.setEstado(StringPool.BLANK);
		}
		else {
			usuarioSessionImpl.setEstado(estado);
		}

		usuarioSessionImpl.setUserId(userId);
		usuarioSessionImpl.setTiendaId(tiendaId);

		if (establecimiento == null) {
			usuarioSessionImpl.setEstablecimiento(StringPool.BLANK);
		}
		else {
			usuarioSessionImpl.setEstablecimiento(establecimiento);
		}

		if (nombreVendedor == null) {
			usuarioSessionImpl.setNombreVendedor(StringPool.BLANK);
		}
		else {
			usuarioSessionImpl.setNombreVendedor(nombreVendedor);
		}

		usuarioSessionImpl.resetOriginalValues();

		return usuarioSessionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idUsuarioSession = objectInput.readLong();
		idSession = objectInput.readUTF();
		fechaRegistro = objectInput.readLong();
		tienda = objectInput.readUTF();
		estado = objectInput.readUTF();
		userId = objectInput.readLong();
		tiendaId = objectInput.readLong();
		establecimiento = objectInput.readUTF();
		nombreVendedor = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(idUsuarioSession);

		if (idSession == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idSession);
		}

		objectOutput.writeLong(fechaRegistro);

		if (tienda == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tienda);
		}

		if (estado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(estado);
		}

		objectOutput.writeLong(userId);
		objectOutput.writeLong(tiendaId);

		if (establecimiento == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(establecimiento);
		}

		if (nombreVendedor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreVendedor);
		}
	}

	public long idUsuarioSession;
	public String idSession;
	public long fechaRegistro;
	public String tienda;
	public String estado;
	public long userId;
	public long tiendaId;
	public String establecimiento;
	public String nombreVendedor;
}