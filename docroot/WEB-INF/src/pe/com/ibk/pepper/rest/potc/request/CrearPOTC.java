
package pe.com.ibk.pepper.rest.potc.request;

import com.fasterxml.jackson.annotation.JsonCreator;

public class CrearPOTC {

	private String idProducto;
    private String idProductoUsuario;
    private String tipoPOTC;
    private String suitePOTC;
    private String informacionValidacionPOTC;

    @JsonCreator
    public CrearPOTC() {
		super();
	}

	@JsonCreator
    public CrearPOTC(String idProducto, String idProductoUsuario,
			String tipoPOTC, String suitePOTC, String informacionValidacionPOTC) {
		super();
		this.idProducto = idProducto;
		this.idProductoUsuario = idProductoUsuario;
		this.tipoPOTC = tipoPOTC;
		this.suitePOTC = suitePOTC;
		this.informacionValidacionPOTC = informacionValidacionPOTC;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getIdProductoUsuario() {
		return idProductoUsuario;
	}

	public void setIdProductoUsuario(String idProductoUsuario) {
		this.idProductoUsuario = idProductoUsuario;
	}

	public String getTipoPOTC() {
        return tipoPOTC;
    }

    public void setTipoPOTC(String tipoPOTC) {
        this.tipoPOTC = tipoPOTC;
    }

    public String getSuitePOTC() {
        return suitePOTC;
    }

    public void setSuitePOTC(String suitePOTC) {
        this.suitePOTC = suitePOTC;
    }

    public String getInformacionValidacionPOTC() {
        return informacionValidacionPOTC;
	}

    public void setInformacionValidacionPOTC(String informacionValidacionPOTC) {
        this.informacionValidacionPOTC = informacionValidacionPOTC;
    }
    
}
