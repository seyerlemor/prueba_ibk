/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import pe.com.ibk.pepper.service.ClpSerializer;
import pe.com.ibk.pepper.service.UbigeoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Interbank
 */
public class UbigeoClp extends BaseModelImpl<Ubigeo> implements Ubigeo {
	public UbigeoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Ubigeo.class;
	}

	@Override
	public String getModelClassName() {
		return Ubigeo.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _idUbigeo;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdUbigeo(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _idUbigeo;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idUbigeo", getIdUbigeo());
		attributes.put("codigo", getCodigo());
		attributes.put("nombre", getNombre());
		attributes.put("codDepartamento", getCodDepartamento());
		attributes.put("codProvincia", getCodProvincia());
		attributes.put("codDistrito", getCodDistrito());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idUbigeo = (Long)attributes.get("idUbigeo");

		if (idUbigeo != null) {
			setIdUbigeo(idUbigeo);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String codDepartamento = (String)attributes.get("codDepartamento");

		if (codDepartamento != null) {
			setCodDepartamento(codDepartamento);
		}

		String codProvincia = (String)attributes.get("codProvincia");

		if (codProvincia != null) {
			setCodProvincia(codProvincia);
		}

		String codDistrito = (String)attributes.get("codDistrito");

		if (codDistrito != null) {
			setCodDistrito(codDistrito);
		}
	}

	@Override
	public long getIdUbigeo() {
		return _idUbigeo;
	}

	@Override
	public void setIdUbigeo(long idUbigeo) {
		_idUbigeo = idUbigeo;

		if (_ubigeoRemoteModel != null) {
			try {
				Class<?> clazz = _ubigeoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdUbigeo", long.class);

				method.invoke(_ubigeoRemoteModel, idUbigeo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodigo() {
		return _codigo;
	}

	@Override
	public void setCodigo(String codigo) {
		_codigo = codigo;

		if (_ubigeoRemoteModel != null) {
			try {
				Class<?> clazz = _ubigeoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigo", String.class);

				method.invoke(_ubigeoRemoteModel, codigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombre() {
		return _nombre;
	}

	@Override
	public void setNombre(String nombre) {
		_nombre = nombre;

		if (_ubigeoRemoteModel != null) {
			try {
				Class<?> clazz = _ubigeoRemoteModel.getClass();

				Method method = clazz.getMethod("setNombre", String.class);

				method.invoke(_ubigeoRemoteModel, nombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodDepartamento() {
		return _codDepartamento;
	}

	@Override
	public void setCodDepartamento(String codDepartamento) {
		_codDepartamento = codDepartamento;

		if (_ubigeoRemoteModel != null) {
			try {
				Class<?> clazz = _ubigeoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodDepartamento",
						String.class);

				method.invoke(_ubigeoRemoteModel, codDepartamento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodProvincia() {
		return _codProvincia;
	}

	@Override
	public void setCodProvincia(String codProvincia) {
		_codProvincia = codProvincia;

		if (_ubigeoRemoteModel != null) {
			try {
				Class<?> clazz = _ubigeoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodProvincia", String.class);

				method.invoke(_ubigeoRemoteModel, codProvincia);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCodDistrito() {
		return _codDistrito;
	}

	@Override
	public void setCodDistrito(String codDistrito) {
		_codDistrito = codDistrito;

		if (_ubigeoRemoteModel != null) {
			try {
				Class<?> clazz = _ubigeoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodDistrito", String.class);

				method.invoke(_ubigeoRemoteModel, codDistrito);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUbigeoRemoteModel() {
		return _ubigeoRemoteModel;
	}

	public void setUbigeoRemoteModel(BaseModel<?> ubigeoRemoteModel) {
		_ubigeoRemoteModel = ubigeoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _ubigeoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_ubigeoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UbigeoLocalServiceUtil.addUbigeo(this);
		}
		else {
			UbigeoLocalServiceUtil.updateUbigeo(this);
		}
	}

	@Override
	public Ubigeo toEscapedModel() {
		return (Ubigeo)ProxyUtil.newProxyInstance(Ubigeo.class.getClassLoader(),
			new Class[] { Ubigeo.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UbigeoClp clone = new UbigeoClp();

		clone.setIdUbigeo(getIdUbigeo());
		clone.setCodigo(getCodigo());
		clone.setNombre(getNombre());
		clone.setCodDepartamento(getCodDepartamento());
		clone.setCodProvincia(getCodProvincia());
		clone.setCodDistrito(getCodDistrito());

		return clone;
	}

	@Override
	public int compareTo(Ubigeo ubigeo) {
		long primaryKey = ubigeo.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UbigeoClp)) {
			return false;
		}

		UbigeoClp ubigeo = (UbigeoClp)obj;

		long primaryKey = ubigeo.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{idUbigeo=");
		sb.append(getIdUbigeo());
		sb.append(", codigo=");
		sb.append(getCodigo());
		sb.append(", nombre=");
		sb.append(getNombre());
		sb.append(", codDepartamento=");
		sb.append(getCodDepartamento());
		sb.append(", codProvincia=");
		sb.append(getCodProvincia());
		sb.append(", codDistrito=");
		sb.append(getCodDistrito());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("pe.com.ibk.pepper.model.Ubigeo");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idUbigeo</column-name><column-value><![CDATA[");
		sb.append(getIdUbigeo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigo</column-name><column-value><![CDATA[");
		sb.append(getCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombre</column-name><column-value><![CDATA[");
		sb.append(getNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codDepartamento</column-name><column-value><![CDATA[");
		sb.append(getCodDepartamento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codProvincia</column-name><column-value><![CDATA[");
		sb.append(getCodProvincia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codDistrito</column-name><column-value><![CDATA[");
		sb.append(getCodDistrito());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idUbigeo;
	private String _codigo;
	private String _nombre;
	private String _codDepartamento;
	private String _codProvincia;
	private String _codDistrito;
	private BaseModel<?> _ubigeoRemoteModel;
	private Class<?> _clpSerializerClass = pe.com.ibk.pepper.service.ClpSerializer.class;
}