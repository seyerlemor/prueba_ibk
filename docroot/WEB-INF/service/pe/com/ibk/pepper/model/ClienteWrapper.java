/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Cliente}.
 * </p>
 *
 * @author Interbank
 * @see Cliente
 * @generated
 */
public class ClienteWrapper implements Cliente, ModelWrapper<Cliente> {
	public ClienteWrapper(Cliente cliente) {
		_cliente = cliente;
	}

	@Override
	public Class<?> getModelClass() {
		return Cliente.class;
	}

	@Override
	public String getModelClassName() {
		return Cliente.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idDatoCliente", getIdDatoCliente());
		attributes.put("idExpediente", getIdExpediente());
		attributes.put("primerNombre", getPrimerNombre());
		attributes.put("segundoNombre", getSegundoNombre());
		attributes.put("apellidoPaterno", getApellidoPaterno());
		attributes.put("apellidoMaterno", getApellidoMaterno());
		attributes.put("sexo", getSexo());
		attributes.put("correo", getCorreo());
		attributes.put("telefono", getTelefono());
		attributes.put("estadoCivil", getEstadoCivil());
		attributes.put("situacionLaboral", getSituacionLaboral());
		attributes.put("rucEmpresaTrabajo", getRucEmpresaTrabajo());
		attributes.put("nivelEducacion", getNivelEducacion());
		attributes.put("ocupacion", getOcupacion());
		attributes.put("cargoActual", getCargoActual());
		attributes.put("actividadNegocio", getActividadNegocio());
		attributes.put("antiguedadLaboral", getAntiguedadLaboral());
		attributes.put("fechaRegistro", getFechaRegistro());
		attributes.put("porcentajeParticipacion", getPorcentajeParticipacion());
		attributes.put("ingresoMensualFijo", getIngresoMensualFijo());
		attributes.put("ingresoMensualVariable", getIngresoMensualVariable());
		attributes.put("documento", getDocumento());
		attributes.put("terminosCondiciones", getTerminosCondiciones());
		attributes.put("tipoDocumento", getTipoDocumento());
		attributes.put("flagPEP", getFlagPEP());
		attributes.put("fechaNacimiento", getFechaNacimiento());
		attributes.put("operador", getOperador());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idDatoCliente = (Long)attributes.get("idDatoCliente");

		if (idDatoCliente != null) {
			setIdDatoCliente(idDatoCliente);
		}

		Long idExpediente = (Long)attributes.get("idExpediente");

		if (idExpediente != null) {
			setIdExpediente(idExpediente);
		}

		String primerNombre = (String)attributes.get("primerNombre");

		if (primerNombre != null) {
			setPrimerNombre(primerNombre);
		}

		String segundoNombre = (String)attributes.get("segundoNombre");

		if (segundoNombre != null) {
			setSegundoNombre(segundoNombre);
		}

		String apellidoPaterno = (String)attributes.get("apellidoPaterno");

		if (apellidoPaterno != null) {
			setApellidoPaterno(apellidoPaterno);
		}

		String apellidoMaterno = (String)attributes.get("apellidoMaterno");

		if (apellidoMaterno != null) {
			setApellidoMaterno(apellidoMaterno);
		}

		String sexo = (String)attributes.get("sexo");

		if (sexo != null) {
			setSexo(sexo);
		}

		String correo = (String)attributes.get("correo");

		if (correo != null) {
			setCorreo(correo);
		}

		String telefono = (String)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}

		String estadoCivil = (String)attributes.get("estadoCivil");

		if (estadoCivil != null) {
			setEstadoCivil(estadoCivil);
		}

		String situacionLaboral = (String)attributes.get("situacionLaboral");

		if (situacionLaboral != null) {
			setSituacionLaboral(situacionLaboral);
		}

		String rucEmpresaTrabajo = (String)attributes.get("rucEmpresaTrabajo");

		if (rucEmpresaTrabajo != null) {
			setRucEmpresaTrabajo(rucEmpresaTrabajo);
		}

		String nivelEducacion = (String)attributes.get("nivelEducacion");

		if (nivelEducacion != null) {
			setNivelEducacion(nivelEducacion);
		}

		String ocupacion = (String)attributes.get("ocupacion");

		if (ocupacion != null) {
			setOcupacion(ocupacion);
		}

		String cargoActual = (String)attributes.get("cargoActual");

		if (cargoActual != null) {
			setCargoActual(cargoActual);
		}

		String actividadNegocio = (String)attributes.get("actividadNegocio");

		if (actividadNegocio != null) {
			setActividadNegocio(actividadNegocio);
		}

		String antiguedadLaboral = (String)attributes.get("antiguedadLaboral");

		if (antiguedadLaboral != null) {
			setAntiguedadLaboral(antiguedadLaboral);
		}

		Date fechaRegistro = (Date)attributes.get("fechaRegistro");

		if (fechaRegistro != null) {
			setFechaRegistro(fechaRegistro);
		}

		String porcentajeParticipacion = (String)attributes.get(
				"porcentajeParticipacion");

		if (porcentajeParticipacion != null) {
			setPorcentajeParticipacion(porcentajeParticipacion);
		}

		String ingresoMensualFijo = (String)attributes.get("ingresoMensualFijo");

		if (ingresoMensualFijo != null) {
			setIngresoMensualFijo(ingresoMensualFijo);
		}

		String ingresoMensualVariable = (String)attributes.get(
				"ingresoMensualVariable");

		if (ingresoMensualVariable != null) {
			setIngresoMensualVariable(ingresoMensualVariable);
		}

		String documento = (String)attributes.get("documento");

		if (documento != null) {
			setDocumento(documento);
		}

		String terminosCondiciones = (String)attributes.get(
				"terminosCondiciones");

		if (terminosCondiciones != null) {
			setTerminosCondiciones(terminosCondiciones);
		}

		String tipoDocumento = (String)attributes.get("tipoDocumento");

		if (tipoDocumento != null) {
			setTipoDocumento(tipoDocumento);
		}

		String flagPEP = (String)attributes.get("flagPEP");

		if (flagPEP != null) {
			setFlagPEP(flagPEP);
		}

		Date fechaNacimiento = (Date)attributes.get("fechaNacimiento");

		if (fechaNacimiento != null) {
			setFechaNacimiento(fechaNacimiento);
		}

		String operador = (String)attributes.get("operador");

		if (operador != null) {
			setOperador(operador);
		}
	}

	/**
	* Returns the primary key of this cliente.
	*
	* @return the primary key of this cliente
	*/
	@Override
	public long getPrimaryKey() {
		return _cliente.getPrimaryKey();
	}

	/**
	* Sets the primary key of this cliente.
	*
	* @param primaryKey the primary key of this cliente
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_cliente.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id dato cliente of this cliente.
	*
	* @return the id dato cliente of this cliente
	*/
	@Override
	public long getIdDatoCliente() {
		return _cliente.getIdDatoCliente();
	}

	/**
	* Sets the id dato cliente of this cliente.
	*
	* @param idDatoCliente the id dato cliente of this cliente
	*/
	@Override
	public void setIdDatoCliente(long idDatoCliente) {
		_cliente.setIdDatoCliente(idDatoCliente);
	}

	/**
	* Returns the id expediente of this cliente.
	*
	* @return the id expediente of this cliente
	*/
	@Override
	public long getIdExpediente() {
		return _cliente.getIdExpediente();
	}

	/**
	* Sets the id expediente of this cliente.
	*
	* @param idExpediente the id expediente of this cliente
	*/
	@Override
	public void setIdExpediente(long idExpediente) {
		_cliente.setIdExpediente(idExpediente);
	}

	/**
	* Returns the primer nombre of this cliente.
	*
	* @return the primer nombre of this cliente
	*/
	@Override
	public java.lang.String getPrimerNombre() {
		return _cliente.getPrimerNombre();
	}

	/**
	* Sets the primer nombre of this cliente.
	*
	* @param primerNombre the primer nombre of this cliente
	*/
	@Override
	public void setPrimerNombre(java.lang.String primerNombre) {
		_cliente.setPrimerNombre(primerNombre);
	}

	/**
	* Returns the segundo nombre of this cliente.
	*
	* @return the segundo nombre of this cliente
	*/
	@Override
	public java.lang.String getSegundoNombre() {
		return _cliente.getSegundoNombre();
	}

	/**
	* Sets the segundo nombre of this cliente.
	*
	* @param segundoNombre the segundo nombre of this cliente
	*/
	@Override
	public void setSegundoNombre(java.lang.String segundoNombre) {
		_cliente.setSegundoNombre(segundoNombre);
	}

	/**
	* Returns the apellido paterno of this cliente.
	*
	* @return the apellido paterno of this cliente
	*/
	@Override
	public java.lang.String getApellidoPaterno() {
		return _cliente.getApellidoPaterno();
	}

	/**
	* Sets the apellido paterno of this cliente.
	*
	* @param apellidoPaterno the apellido paterno of this cliente
	*/
	@Override
	public void setApellidoPaterno(java.lang.String apellidoPaterno) {
		_cliente.setApellidoPaterno(apellidoPaterno);
	}

	/**
	* Returns the apellido materno of this cliente.
	*
	* @return the apellido materno of this cliente
	*/
	@Override
	public java.lang.String getApellidoMaterno() {
		return _cliente.getApellidoMaterno();
	}

	/**
	* Sets the apellido materno of this cliente.
	*
	* @param apellidoMaterno the apellido materno of this cliente
	*/
	@Override
	public void setApellidoMaterno(java.lang.String apellidoMaterno) {
		_cliente.setApellidoMaterno(apellidoMaterno);
	}

	/**
	* Returns the sexo of this cliente.
	*
	* @return the sexo of this cliente
	*/
	@Override
	public java.lang.String getSexo() {
		return _cliente.getSexo();
	}

	/**
	* Sets the sexo of this cliente.
	*
	* @param sexo the sexo of this cliente
	*/
	@Override
	public void setSexo(java.lang.String sexo) {
		_cliente.setSexo(sexo);
	}

	/**
	* Returns the correo of this cliente.
	*
	* @return the correo of this cliente
	*/
	@Override
	public java.lang.String getCorreo() {
		return _cliente.getCorreo();
	}

	/**
	* Sets the correo of this cliente.
	*
	* @param correo the correo of this cliente
	*/
	@Override
	public void setCorreo(java.lang.String correo) {
		_cliente.setCorreo(correo);
	}

	/**
	* Returns the telefono of this cliente.
	*
	* @return the telefono of this cliente
	*/
	@Override
	public java.lang.String getTelefono() {
		return _cliente.getTelefono();
	}

	/**
	* Sets the telefono of this cliente.
	*
	* @param telefono the telefono of this cliente
	*/
	@Override
	public void setTelefono(java.lang.String telefono) {
		_cliente.setTelefono(telefono);
	}

	/**
	* Returns the estado civil of this cliente.
	*
	* @return the estado civil of this cliente
	*/
	@Override
	public java.lang.String getEstadoCivil() {
		return _cliente.getEstadoCivil();
	}

	/**
	* Sets the estado civil of this cliente.
	*
	* @param estadoCivil the estado civil of this cliente
	*/
	@Override
	public void setEstadoCivil(java.lang.String estadoCivil) {
		_cliente.setEstadoCivil(estadoCivil);
	}

	/**
	* Returns the situacion laboral of this cliente.
	*
	* @return the situacion laboral of this cliente
	*/
	@Override
	public java.lang.String getSituacionLaboral() {
		return _cliente.getSituacionLaboral();
	}

	/**
	* Sets the situacion laboral of this cliente.
	*
	* @param situacionLaboral the situacion laboral of this cliente
	*/
	@Override
	public void setSituacionLaboral(java.lang.String situacionLaboral) {
		_cliente.setSituacionLaboral(situacionLaboral);
	}

	/**
	* Returns the ruc empresa trabajo of this cliente.
	*
	* @return the ruc empresa trabajo of this cliente
	*/
	@Override
	public java.lang.String getRucEmpresaTrabajo() {
		return _cliente.getRucEmpresaTrabajo();
	}

	/**
	* Sets the ruc empresa trabajo of this cliente.
	*
	* @param rucEmpresaTrabajo the ruc empresa trabajo of this cliente
	*/
	@Override
	public void setRucEmpresaTrabajo(java.lang.String rucEmpresaTrabajo) {
		_cliente.setRucEmpresaTrabajo(rucEmpresaTrabajo);
	}

	/**
	* Returns the nivel educacion of this cliente.
	*
	* @return the nivel educacion of this cliente
	*/
	@Override
	public java.lang.String getNivelEducacion() {
		return _cliente.getNivelEducacion();
	}

	/**
	* Sets the nivel educacion of this cliente.
	*
	* @param nivelEducacion the nivel educacion of this cliente
	*/
	@Override
	public void setNivelEducacion(java.lang.String nivelEducacion) {
		_cliente.setNivelEducacion(nivelEducacion);
	}

	/**
	* Returns the ocupacion of this cliente.
	*
	* @return the ocupacion of this cliente
	*/
	@Override
	public java.lang.String getOcupacion() {
		return _cliente.getOcupacion();
	}

	/**
	* Sets the ocupacion of this cliente.
	*
	* @param ocupacion the ocupacion of this cliente
	*/
	@Override
	public void setOcupacion(java.lang.String ocupacion) {
		_cliente.setOcupacion(ocupacion);
	}

	/**
	* Returns the cargo actual of this cliente.
	*
	* @return the cargo actual of this cliente
	*/
	@Override
	public java.lang.String getCargoActual() {
		return _cliente.getCargoActual();
	}

	/**
	* Sets the cargo actual of this cliente.
	*
	* @param cargoActual the cargo actual of this cliente
	*/
	@Override
	public void setCargoActual(java.lang.String cargoActual) {
		_cliente.setCargoActual(cargoActual);
	}

	/**
	* Returns the actividad negocio of this cliente.
	*
	* @return the actividad negocio of this cliente
	*/
	@Override
	public java.lang.String getActividadNegocio() {
		return _cliente.getActividadNegocio();
	}

	/**
	* Sets the actividad negocio of this cliente.
	*
	* @param actividadNegocio the actividad negocio of this cliente
	*/
	@Override
	public void setActividadNegocio(java.lang.String actividadNegocio) {
		_cliente.setActividadNegocio(actividadNegocio);
	}

	/**
	* Returns the antiguedad laboral of this cliente.
	*
	* @return the antiguedad laboral of this cliente
	*/
	@Override
	public java.lang.String getAntiguedadLaboral() {
		return _cliente.getAntiguedadLaboral();
	}

	/**
	* Sets the antiguedad laboral of this cliente.
	*
	* @param antiguedadLaboral the antiguedad laboral of this cliente
	*/
	@Override
	public void setAntiguedadLaboral(java.lang.String antiguedadLaboral) {
		_cliente.setAntiguedadLaboral(antiguedadLaboral);
	}

	/**
	* Returns the fecha registro of this cliente.
	*
	* @return the fecha registro of this cliente
	*/
	@Override
	public java.util.Date getFechaRegistro() {
		return _cliente.getFechaRegistro();
	}

	/**
	* Sets the fecha registro of this cliente.
	*
	* @param fechaRegistro the fecha registro of this cliente
	*/
	@Override
	public void setFechaRegistro(java.util.Date fechaRegistro) {
		_cliente.setFechaRegistro(fechaRegistro);
	}

	/**
	* Returns the porcentaje participacion of this cliente.
	*
	* @return the porcentaje participacion of this cliente
	*/
	@Override
	public java.lang.String getPorcentajeParticipacion() {
		return _cliente.getPorcentajeParticipacion();
	}

	/**
	* Sets the porcentaje participacion of this cliente.
	*
	* @param porcentajeParticipacion the porcentaje participacion of this cliente
	*/
	@Override
	public void setPorcentajeParticipacion(
		java.lang.String porcentajeParticipacion) {
		_cliente.setPorcentajeParticipacion(porcentajeParticipacion);
	}

	/**
	* Returns the ingreso mensual fijo of this cliente.
	*
	* @return the ingreso mensual fijo of this cliente
	*/
	@Override
	public java.lang.String getIngresoMensualFijo() {
		return _cliente.getIngresoMensualFijo();
	}

	/**
	* Sets the ingreso mensual fijo of this cliente.
	*
	* @param ingresoMensualFijo the ingreso mensual fijo of this cliente
	*/
	@Override
	public void setIngresoMensualFijo(java.lang.String ingresoMensualFijo) {
		_cliente.setIngresoMensualFijo(ingresoMensualFijo);
	}

	/**
	* Returns the ingreso mensual variable of this cliente.
	*
	* @return the ingreso mensual variable of this cliente
	*/
	@Override
	public java.lang.String getIngresoMensualVariable() {
		return _cliente.getIngresoMensualVariable();
	}

	/**
	* Sets the ingreso mensual variable of this cliente.
	*
	* @param ingresoMensualVariable the ingreso mensual variable of this cliente
	*/
	@Override
	public void setIngresoMensualVariable(
		java.lang.String ingresoMensualVariable) {
		_cliente.setIngresoMensualVariable(ingresoMensualVariable);
	}

	/**
	* Returns the documento of this cliente.
	*
	* @return the documento of this cliente
	*/
	@Override
	public java.lang.String getDocumento() {
		return _cliente.getDocumento();
	}

	/**
	* Sets the documento of this cliente.
	*
	* @param documento the documento of this cliente
	*/
	@Override
	public void setDocumento(java.lang.String documento) {
		_cliente.setDocumento(documento);
	}

	/**
	* Returns the terminos condiciones of this cliente.
	*
	* @return the terminos condiciones of this cliente
	*/
	@Override
	public java.lang.String getTerminosCondiciones() {
		return _cliente.getTerminosCondiciones();
	}

	/**
	* Sets the terminos condiciones of this cliente.
	*
	* @param terminosCondiciones the terminos condiciones of this cliente
	*/
	@Override
	public void setTerminosCondiciones(java.lang.String terminosCondiciones) {
		_cliente.setTerminosCondiciones(terminosCondiciones);
	}

	/**
	* Returns the tipo documento of this cliente.
	*
	* @return the tipo documento of this cliente
	*/
	@Override
	public java.lang.String getTipoDocumento() {
		return _cliente.getTipoDocumento();
	}

	/**
	* Sets the tipo documento of this cliente.
	*
	* @param tipoDocumento the tipo documento of this cliente
	*/
	@Override
	public void setTipoDocumento(java.lang.String tipoDocumento) {
		_cliente.setTipoDocumento(tipoDocumento);
	}

	/**
	* Returns the flag p e p of this cliente.
	*
	* @return the flag p e p of this cliente
	*/
	@Override
	public java.lang.String getFlagPEP() {
		return _cliente.getFlagPEP();
	}

	/**
	* Sets the flag p e p of this cliente.
	*
	* @param flagPEP the flag p e p of this cliente
	*/
	@Override
	public void setFlagPEP(java.lang.String flagPEP) {
		_cliente.setFlagPEP(flagPEP);
	}

	/**
	* Returns the fecha nacimiento of this cliente.
	*
	* @return the fecha nacimiento of this cliente
	*/
	@Override
	public java.util.Date getFechaNacimiento() {
		return _cliente.getFechaNacimiento();
	}

	/**
	* Sets the fecha nacimiento of this cliente.
	*
	* @param fechaNacimiento the fecha nacimiento of this cliente
	*/
	@Override
	public void setFechaNacimiento(java.util.Date fechaNacimiento) {
		_cliente.setFechaNacimiento(fechaNacimiento);
	}

	/**
	* Returns the operador of this cliente.
	*
	* @return the operador of this cliente
	*/
	@Override
	public java.lang.String getOperador() {
		return _cliente.getOperador();
	}

	/**
	* Sets the operador of this cliente.
	*
	* @param operador the operador of this cliente
	*/
	@Override
	public void setOperador(java.lang.String operador) {
		_cliente.setOperador(operador);
	}

	@Override
	public boolean isNew() {
		return _cliente.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_cliente.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _cliente.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_cliente.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _cliente.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _cliente.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_cliente.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _cliente.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_cliente.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_cliente.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_cliente.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ClienteWrapper((Cliente)_cliente.clone());
	}

	@Override
	public int compareTo(pe.com.ibk.pepper.model.Cliente cliente) {
		return _cliente.compareTo(cliente);
	}

	@Override
	public int hashCode() {
		return _cliente.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.Cliente> toCacheModel() {
		return _cliente.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.Cliente toEscapedModel() {
		return new ClienteWrapper(_cliente.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.Cliente toUnescapedModel() {
		return new ClienteWrapper(_cliente.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _cliente.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _cliente.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_cliente.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClienteWrapper)) {
			return false;
		}

		ClienteWrapper clienteWrapper = (ClienteWrapper)obj;

		if (Validator.equals(_cliente, clienteWrapper._cliente)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Cliente getWrappedCliente() {
		return _cliente;
	}

	@Override
	public Cliente getWrappedModel() {
		return _cliente;
	}

	@Override
	public void resetOriginalValues() {
		_cliente.resetOriginalValues();
	}

	private Cliente _cliente;
}