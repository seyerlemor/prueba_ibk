package pe.com.ibk.pepper.bean;


import java.io.Serializable;

/**
 * The Class JsonBean.
 * @author Interbank Modulo Tarjeta
 */
public class JsonLiquidBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private String value;
	private String label;
	
	
	public JsonLiquidBean(String value, String label)
	{
	  	this.value=value;
	  	this.label=label;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "JsonLiquidBean [value=" + value + ", label=" + label + "]";
	}
	
	
	
}
