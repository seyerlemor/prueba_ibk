/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import pe.com.ibk.pepper.NoSuchUsuarioSessionException;
import pe.com.ibk.pepper.model.UsuarioSession;
import pe.com.ibk.pepper.model.impl.UsuarioSessionImpl;
import pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the usuario session service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Interbank
 * @see UsuarioSessionPersistence
 * @see UsuarioSessionUtil
 * @generated
 */
public class UsuarioSessionPersistenceImpl extends BasePersistenceImpl<UsuarioSession>
	implements UsuarioSessionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UsuarioSessionUtil} to access the usuario session persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UsuarioSessionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED,
			UsuarioSessionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED,
			UsuarioSessionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_US_ID = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED,
			UsuarioSessionImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByUS_ID",
			new String[] { Long.class.getName() },
			UsuarioSessionModelImpl.IDUSUARIOSESSION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_US_ID = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUS_ID",
			new String[] { Long.class.getName() });

	/**
	 * Returns the usuario session where idUsuarioSession = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUsuarioSessionException} if it could not be found.
	 *
	 * @param idUsuarioSession the id usuario session
	 * @return the matching usuario session
	 * @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a matching usuario session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession findByUS_ID(long idUsuarioSession)
		throws NoSuchUsuarioSessionException, SystemException {
		UsuarioSession usuarioSession = fetchByUS_ID(idUsuarioSession);

		if (usuarioSession == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idUsuarioSession=");
			msg.append(idUsuarioSession);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchUsuarioSessionException(msg.toString());
		}

		return usuarioSession;
	}

	/**
	 * Returns the usuario session where idUsuarioSession = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idUsuarioSession the id usuario session
	 * @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession fetchByUS_ID(long idUsuarioSession)
		throws SystemException {
		return fetchByUS_ID(idUsuarioSession, true);
	}

	/**
	 * Returns the usuario session where idUsuarioSession = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idUsuarioSession the id usuario session
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession fetchByUS_ID(long idUsuarioSession,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idUsuarioSession };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_US_ID,
					finderArgs, this);
		}

		if (result instanceof UsuarioSession) {
			UsuarioSession usuarioSession = (UsuarioSession)result;

			if ((idUsuarioSession != usuarioSession.getIdUsuarioSession())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_USUARIOSESSION_WHERE);

			query.append(_FINDER_COLUMN_US_ID_IDUSUARIOSESSION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idUsuarioSession);

				List<UsuarioSession> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_US_ID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"UsuarioSessionPersistenceImpl.fetchByUS_ID(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					UsuarioSession usuarioSession = list.get(0);

					result = usuarioSession;

					cacheResult(usuarioSession);

					if ((usuarioSession.getIdUsuarioSession() != idUsuarioSession)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_US_ID,
							finderArgs, usuarioSession);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_US_ID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (UsuarioSession)result;
		}
	}

	/**
	 * Removes the usuario session where idUsuarioSession = &#63; from the database.
	 *
	 * @param idUsuarioSession the id usuario session
	 * @return the usuario session that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession removeByUS_ID(long idUsuarioSession)
		throws NoSuchUsuarioSessionException, SystemException {
		UsuarioSession usuarioSession = findByUS_ID(idUsuarioSession);

		return remove(usuarioSession);
	}

	/**
	 * Returns the number of usuario sessions where idUsuarioSession = &#63;.
	 *
	 * @param idUsuarioSession the id usuario session
	 * @return the number of matching usuario sessions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUS_ID(long idUsuarioSession) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_US_ID;

		Object[] finderArgs = new Object[] { idUsuarioSession };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USUARIOSESSION_WHERE);

			query.append(_FINDER_COLUMN_US_ID_IDUSUARIOSESSION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(idUsuarioSession);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_US_ID_IDUSUARIOSESSION_2 = "usuarioSession.idUsuarioSession = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_U_S_S = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED,
			UsuarioSessionImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByU_S_S",
			new String[] { String.class.getName() },
			UsuarioSessionModelImpl.IDSESSION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_U_S_S = new FinderPath(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByU_S_S",
			new String[] { String.class.getName() });

	/**
	 * Returns the usuario session where idSession = &#63; or throws a {@link pe.com.ibk.pepper.NoSuchUsuarioSessionException} if it could not be found.
	 *
	 * @param idSession the id session
	 * @return the matching usuario session
	 * @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a matching usuario session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession findByU_S_S(String idSession)
		throws NoSuchUsuarioSessionException, SystemException {
		UsuarioSession usuarioSession = fetchByU_S_S(idSession);

		if (usuarioSession == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("idSession=");
			msg.append(idSession);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchUsuarioSessionException(msg.toString());
		}

		return usuarioSession;
	}

	/**
	 * Returns the usuario session where idSession = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param idSession the id session
	 * @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession fetchByU_S_S(String idSession)
		throws SystemException {
		return fetchByU_S_S(idSession, true);
	}

	/**
	 * Returns the usuario session where idSession = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param idSession the id session
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching usuario session, or <code>null</code> if a matching usuario session could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession fetchByU_S_S(String idSession,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { idSession };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_U_S_S,
					finderArgs, this);
		}

		if (result instanceof UsuarioSession) {
			UsuarioSession usuarioSession = (UsuarioSession)result;

			if (!Validator.equals(idSession, usuarioSession.getIdSession())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_USUARIOSESSION_WHERE);

			boolean bindIdSession = false;

			if (idSession == null) {
				query.append(_FINDER_COLUMN_U_S_S_IDSESSION_1);
			}
			else if (idSession.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_U_S_S_IDSESSION_3);
			}
			else {
				bindIdSession = true;

				query.append(_FINDER_COLUMN_U_S_S_IDSESSION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdSession) {
					qPos.add(idSession);
				}

				List<UsuarioSession> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_U_S_S,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"UsuarioSessionPersistenceImpl.fetchByU_S_S(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					UsuarioSession usuarioSession = list.get(0);

					result = usuarioSession;

					cacheResult(usuarioSession);

					if ((usuarioSession.getIdSession() == null) ||
							!usuarioSession.getIdSession().equals(idSession)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_U_S_S,
							finderArgs, usuarioSession);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_U_S_S,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (UsuarioSession)result;
		}
	}

	/**
	 * Removes the usuario session where idSession = &#63; from the database.
	 *
	 * @param idSession the id session
	 * @return the usuario session that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession removeByU_S_S(String idSession)
		throws NoSuchUsuarioSessionException, SystemException {
		UsuarioSession usuarioSession = findByU_S_S(idSession);

		return remove(usuarioSession);
	}

	/**
	 * Returns the number of usuario sessions where idSession = &#63;.
	 *
	 * @param idSession the id session
	 * @return the number of matching usuario sessions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByU_S_S(String idSession) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_U_S_S;

		Object[] finderArgs = new Object[] { idSession };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USUARIOSESSION_WHERE);

			boolean bindIdSession = false;

			if (idSession == null) {
				query.append(_FINDER_COLUMN_U_S_S_IDSESSION_1);
			}
			else if (idSession.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_U_S_S_IDSESSION_3);
			}
			else {
				bindIdSession = true;

				query.append(_FINDER_COLUMN_U_S_S_IDSESSION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdSession) {
					qPos.add(idSession);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_U_S_S_IDSESSION_1 = "usuarioSession.idSession IS NULL";
	private static final String _FINDER_COLUMN_U_S_S_IDSESSION_2 = "usuarioSession.idSession = ?";
	private static final String _FINDER_COLUMN_U_S_S_IDSESSION_3 = "(usuarioSession.idSession IS NULL OR usuarioSession.idSession = '')";

	public UsuarioSessionPersistenceImpl() {
		setModelClass(UsuarioSession.class);
	}

	/**
	 * Caches the usuario session in the entity cache if it is enabled.
	 *
	 * @param usuarioSession the usuario session
	 */
	@Override
	public void cacheResult(UsuarioSession usuarioSession) {
		EntityCacheUtil.putResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionImpl.class, usuarioSession.getPrimaryKey(),
			usuarioSession);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_US_ID,
			new Object[] { usuarioSession.getIdUsuarioSession() },
			usuarioSession);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_U_S_S,
			new Object[] { usuarioSession.getIdSession() }, usuarioSession);

		usuarioSession.resetOriginalValues();
	}

	/**
	 * Caches the usuario sessions in the entity cache if it is enabled.
	 *
	 * @param usuarioSessions the usuario sessions
	 */
	@Override
	public void cacheResult(List<UsuarioSession> usuarioSessions) {
		for (UsuarioSession usuarioSession : usuarioSessions) {
			if (EntityCacheUtil.getResult(
						UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
						UsuarioSessionImpl.class, usuarioSession.getPrimaryKey()) == null) {
				cacheResult(usuarioSession);
			}
			else {
				usuarioSession.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all usuario sessions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UsuarioSessionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UsuarioSessionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the usuario session.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UsuarioSession usuarioSession) {
		EntityCacheUtil.removeResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionImpl.class, usuarioSession.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(usuarioSession);
	}

	@Override
	public void clearCache(List<UsuarioSession> usuarioSessions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UsuarioSession usuarioSession : usuarioSessions) {
			EntityCacheUtil.removeResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
				UsuarioSessionImpl.class, usuarioSession.getPrimaryKey());

			clearUniqueFindersCache(usuarioSession);
		}
	}

	protected void cacheUniqueFindersCache(UsuarioSession usuarioSession) {
		if (usuarioSession.isNew()) {
			Object[] args = new Object[] { usuarioSession.getIdUsuarioSession() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_US_ID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_US_ID, args,
				usuarioSession);

			args = new Object[] { usuarioSession.getIdSession() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_U_S_S, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_U_S_S, args,
				usuarioSession);
		}
		else {
			UsuarioSessionModelImpl usuarioSessionModelImpl = (UsuarioSessionModelImpl)usuarioSession;

			if ((usuarioSessionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_US_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						usuarioSession.getIdUsuarioSession()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_US_ID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_US_ID, args,
					usuarioSession);
			}

			if ((usuarioSessionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_U_S_S.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { usuarioSession.getIdSession() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_U_S_S, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_U_S_S, args,
					usuarioSession);
			}
		}
	}

	protected void clearUniqueFindersCache(UsuarioSession usuarioSession) {
		UsuarioSessionModelImpl usuarioSessionModelImpl = (UsuarioSessionModelImpl)usuarioSession;

		Object[] args = new Object[] { usuarioSession.getIdUsuarioSession() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_US_ID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_US_ID, args);

		if ((usuarioSessionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_US_ID.getColumnBitmask()) != 0) {
			args = new Object[] {
					usuarioSessionModelImpl.getOriginalIdUsuarioSession()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_US_ID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_US_ID, args);
		}

		args = new Object[] { usuarioSession.getIdSession() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_U_S_S, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_U_S_S, args);

		if ((usuarioSessionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_U_S_S.getColumnBitmask()) != 0) {
			args = new Object[] { usuarioSessionModelImpl.getOriginalIdSession() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_U_S_S, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_U_S_S, args);
		}
	}

	/**
	 * Creates a new usuario session with the primary key. Does not add the usuario session to the database.
	 *
	 * @param idUsuarioSession the primary key for the new usuario session
	 * @return the new usuario session
	 */
	@Override
	public UsuarioSession create(long idUsuarioSession) {
		UsuarioSession usuarioSession = new UsuarioSessionImpl();

		usuarioSession.setNew(true);
		usuarioSession.setPrimaryKey(idUsuarioSession);

		return usuarioSession;
	}

	/**
	 * Removes the usuario session with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idUsuarioSession the primary key of the usuario session
	 * @return the usuario session that was removed
	 * @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a usuario session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession remove(long idUsuarioSession)
		throws NoSuchUsuarioSessionException, SystemException {
		return remove((Serializable)idUsuarioSession);
	}

	/**
	 * Removes the usuario session with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the usuario session
	 * @return the usuario session that was removed
	 * @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a usuario session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession remove(Serializable primaryKey)
		throws NoSuchUsuarioSessionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UsuarioSession usuarioSession = (UsuarioSession)session.get(UsuarioSessionImpl.class,
					primaryKey);

			if (usuarioSession == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUsuarioSessionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(usuarioSession);
		}
		catch (NoSuchUsuarioSessionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UsuarioSession removeImpl(UsuarioSession usuarioSession)
		throws SystemException {
		usuarioSession = toUnwrappedModel(usuarioSession);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(usuarioSession)) {
				usuarioSession = (UsuarioSession)session.get(UsuarioSessionImpl.class,
						usuarioSession.getPrimaryKeyObj());
			}

			if (usuarioSession != null) {
				session.delete(usuarioSession);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (usuarioSession != null) {
			clearCache(usuarioSession);
		}

		return usuarioSession;
	}

	@Override
	public UsuarioSession updateImpl(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession)
		throws SystemException {
		usuarioSession = toUnwrappedModel(usuarioSession);

		boolean isNew = usuarioSession.isNew();

		Session session = null;

		try {
			session = openSession();

			if (usuarioSession.isNew()) {
				session.save(usuarioSession);

				usuarioSession.setNew(false);
			}
			else {
				session.merge(usuarioSession);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UsuarioSessionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
			UsuarioSessionImpl.class, usuarioSession.getPrimaryKey(),
			usuarioSession);

		clearUniqueFindersCache(usuarioSession);
		cacheUniqueFindersCache(usuarioSession);

		return usuarioSession;
	}

	protected UsuarioSession toUnwrappedModel(UsuarioSession usuarioSession) {
		if (usuarioSession instanceof UsuarioSessionImpl) {
			return usuarioSession;
		}

		UsuarioSessionImpl usuarioSessionImpl = new UsuarioSessionImpl();

		usuarioSessionImpl.setNew(usuarioSession.isNew());
		usuarioSessionImpl.setPrimaryKey(usuarioSession.getPrimaryKey());

		usuarioSessionImpl.setIdUsuarioSession(usuarioSession.getIdUsuarioSession());
		usuarioSessionImpl.setIdSession(usuarioSession.getIdSession());
		usuarioSessionImpl.setFechaRegistro(usuarioSession.getFechaRegistro());
		usuarioSessionImpl.setTienda(usuarioSession.getTienda());
		usuarioSessionImpl.setEstado(usuarioSession.getEstado());
		usuarioSessionImpl.setUserId(usuarioSession.getUserId());
		usuarioSessionImpl.setTiendaId(usuarioSession.getTiendaId());
		usuarioSessionImpl.setEstablecimiento(usuarioSession.getEstablecimiento());
		usuarioSessionImpl.setNombreVendedor(usuarioSession.getNombreVendedor());

		return usuarioSessionImpl;
	}

	/**
	 * Returns the usuario session with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the usuario session
	 * @return the usuario session
	 * @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a usuario session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUsuarioSessionException, SystemException {
		UsuarioSession usuarioSession = fetchByPrimaryKey(primaryKey);

		if (usuarioSession == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUsuarioSessionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return usuarioSession;
	}

	/**
	 * Returns the usuario session with the primary key or throws a {@link pe.com.ibk.pepper.NoSuchUsuarioSessionException} if it could not be found.
	 *
	 * @param idUsuarioSession the primary key of the usuario session
	 * @return the usuario session
	 * @throws pe.com.ibk.pepper.NoSuchUsuarioSessionException if a usuario session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession findByPrimaryKey(long idUsuarioSession)
		throws NoSuchUsuarioSessionException, SystemException {
		return findByPrimaryKey((Serializable)idUsuarioSession);
	}

	/**
	 * Returns the usuario session with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the usuario session
	 * @return the usuario session, or <code>null</code> if a usuario session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UsuarioSession usuarioSession = (UsuarioSession)EntityCacheUtil.getResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
				UsuarioSessionImpl.class, primaryKey);

		if (usuarioSession == _nullUsuarioSession) {
			return null;
		}

		if (usuarioSession == null) {
			Session session = null;

			try {
				session = openSession();

				usuarioSession = (UsuarioSession)session.get(UsuarioSessionImpl.class,
						primaryKey);

				if (usuarioSession != null) {
					cacheResult(usuarioSession);
				}
				else {
					EntityCacheUtil.putResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
						UsuarioSessionImpl.class, primaryKey,
						_nullUsuarioSession);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UsuarioSessionModelImpl.ENTITY_CACHE_ENABLED,
					UsuarioSessionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return usuarioSession;
	}

	/**
	 * Returns the usuario session with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idUsuarioSession the primary key of the usuario session
	 * @return the usuario session, or <code>null</code> if a usuario session with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UsuarioSession fetchByPrimaryKey(long idUsuarioSession)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)idUsuarioSession);
	}

	/**
	 * Returns all the usuario sessions.
	 *
	 * @return the usuario sessions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsuarioSession> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the usuario sessions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of usuario sessions
	 * @param end the upper bound of the range of usuario sessions (not inclusive)
	 * @return the range of usuario sessions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsuarioSession> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the usuario sessions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of usuario sessions
	 * @param end the upper bound of the range of usuario sessions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of usuario sessions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UsuarioSession> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UsuarioSession> list = (List<UsuarioSession>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_USUARIOSESSION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USUARIOSESSION;

				if (pagination) {
					sql = sql.concat(UsuarioSessionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UsuarioSession>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UsuarioSession>(list);
				}
				else {
					list = (List<UsuarioSession>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the usuario sessions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UsuarioSession usuarioSession : findAll()) {
			remove(usuarioSession);
		}
	}

	/**
	 * Returns the number of usuario sessions.
	 *
	 * @return the number of usuario sessions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USUARIOSESSION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the usuario session persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.pe.com.ibk.pepper.model.UsuarioSession")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UsuarioSession>> listenersList = new ArrayList<ModelListener<UsuarioSession>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UsuarioSession>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UsuarioSessionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_USUARIOSESSION = "SELECT usuarioSession FROM UsuarioSession usuarioSession";
	private static final String _SQL_SELECT_USUARIOSESSION_WHERE = "SELECT usuarioSession FROM UsuarioSession usuarioSession WHERE ";
	private static final String _SQL_COUNT_USUARIOSESSION = "SELECT COUNT(usuarioSession) FROM UsuarioSession usuarioSession";
	private static final String _SQL_COUNT_USUARIOSESSION_WHERE = "SELECT COUNT(usuarioSession) FROM UsuarioSession usuarioSession WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "usuarioSession.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UsuarioSession exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UsuarioSession exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UsuarioSessionPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"idUsuarioSession", "idSession", "fechaRegistro", "tienda",
				"estado", "userId", "tiendaId", "establecimiento",
				"nombreVendedor"
			});
	private static UsuarioSession _nullUsuarioSession = new UsuarioSessionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UsuarioSession> toCacheModel() {
				return _nullUsuarioSessionCacheModel;
			}
		};

	private static CacheModel<UsuarioSession> _nullUsuarioSessionCacheModel = new CacheModel<UsuarioSession>() {
			@Override
			public UsuarioSession toEntityModel() {
				return _nullUsuarioSession;
			}
		};
}