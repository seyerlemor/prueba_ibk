package pe.com.ibk.pepper.bean;

import java.io.Serializable;

public class ComercioBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String commerceName;
	private String commerceHash;
	private String allowedCards;
	private String contentId;
	private String optMax;
	private String equifaxMax;
	private String securityCode;
	private String uniquePromotionalCode;
	private String pepperOnlineType;
	private DynamicTextBean  dynamicTextBean;
	
	public ComercioBean(){
		super();
		 dynamicTextBean = new DynamicTextBean();
	}
	
	public String getCommerceName() {
		return commerceName;
	}
	public void setCommerceName(String commerceName) {
		this.commerceName = commerceName;
	}
	public String getCommerceHash() {
		return commerceHash;
	}
	public void setCommerceHash(String commerceHash) {
		this.commerceHash = commerceHash;
	}
	public String getAllowedCards() {
		return allowedCards;
	}
	public void setAllowedCards(String allowedCards) {
		this.allowedCards = allowedCards;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getOptMax() {
		return optMax;
	}
	public void setOptMax(String optMax) {
		this.optMax = optMax;
	}
	public String getEquifaxMax() {
		return equifaxMax;
	}
	public void setEquifaxMax(String equifaxMax) {
		this.equifaxMax = equifaxMax;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getUniquePromotionalCode() {
		return uniquePromotionalCode;
	}
	public void setUniquePromotionalCode(String uniquePromotionalCode) {
		this.uniquePromotionalCode = uniquePromotionalCode;
	}
	public DynamicTextBean getDynamicTextBean() {
		return dynamicTextBean;
	}
	public void setDynamicTextBean(DynamicTextBean dynamicTextBean) {
		this.dynamicTextBean = dynamicTextBean;
	}
	public String getPepperOnlineType() {
		return pepperOnlineType;
	}
	public void setPepperOnlineType(String pepperOnlineType) {
		this.pepperOnlineType = pepperOnlineType;
	}
	@Override
	public String toString() {
		return "ComercioBean [commerceName=" + commerceName + ", commerceHash="
				+ commerceHash + ", allowedCards=" + allowedCards
				+ ", contentId=" + contentId + ", optMax=" + optMax
				+ ", equifaxMax=" + equifaxMax + ", securityCode="
				+ securityCode + ", uniquePromotionalCode="
				+ uniquePromotionalCode + ", pepperOnlineType="
				+ pepperOnlineType + ", dynamicTextBean=" + dynamicTextBean
				+ "]";
	}

}
