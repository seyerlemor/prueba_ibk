<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="terminarSolicitud" var="terminarSolicitud" />

            <div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    ${commerceText.v601001}
                    <div class="lh-ff-content-col">
                      <div class="lh-ff-section lh-display-tc" data-target-message="#teleticket-success-message">
                        <div class="lh-ff-section__message text-center">
                          <h3 class="lh-typo__commontitle lh-typo__commontitle--2">${commerceText.v601002}</h3>
                          <p class="lh-typo__list">${commerceText.v601003} ${pasoBean.productoBean.provisionalLinePepper}.</p>
                        	<div class="lh-display-tc__timer text-center lh-typo__sectitle lh-typo__sectitle--2" data-counter-time="${commerceText.v601004}"><span class="nowrap">${commerceText.v601005}</span>
	                          <div class="lh-flex lh-typo__label">
	                            <p>Min</p>
	                            <p>Seg</p>
	                          </div>
	                        </div>
                        </div>
                        <div class="lh-display-tc__message lh-ff-section__message text-center" id="teleticket-success-message">
                        	${commerceText.v601006}
                        </div>
                        <div class="lh-display-tc__card">
                          <div class="lh-flex lh-display-tc__card-item lh-display-tc__card-item--01">
<!--                           <div> -->
                            <picture class="lh-display-tc__card-item-image">
                              <source srcset="/pepper-v2-theme/images/form-framework/card-placeholder-green--sm.png" media="(min-width: 768px)"><img class="lazyload" srcset="/pepper-v2-theme/images/form-framework/card-placeholder-green@2x.png" alt="">
                            </picture>
                            <button class="lh-btn lh-btn--primary lh-btn--small" type="button">${renderRequest.preferences.getValue('resumenBtn001','')}</button>
                          </div>
                          <div class="lh-display-tc__card-item lh-display-tc__card-item--02"></div>
                          <div class="lh-flex lh-display-tc__card-item lh-display-tc__card-item--03">
                            <picture class="lh-display-tc__card-item-image">
                              <source srcset="/pepper-v2-theme/images/tc/pan/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg" media="(min-width: 768px)"><img class="lazyload" srcset="/pepper-v2-theme/images/tc/pan/${pasoBean.productoBean.marcaProducto}${pasoBean.productoBean.tipoProducto}.jpg" alt="">
                            </picture>
                            <div class="lh-display-tc__card-item-detail">
                              <div class="lh-display-tc__copy-message text-center"><span class="lh-typo__label">${renderRequest.preferences.getValue('resumenText004','')}</span></div>
                              <button class="${requestScope.cssTextColor} lh-display-tc__card-number lh-typo__label" data-cnumber="${pasoBean.productoBean.numTarjetaProvisional}" type="button">${pasoBean.productoBean.numTarjetaProvisional}</button>
                              <div class="${requestScope.cssTextColor} lh-display-tc__card-duedate">
                              	<span class="lh-typo__obs">${renderRequest.preferences.getValue('resumenText005','')}</span>
                              	<span class="lh-typo__label">${formatExpirationDate}</span>
                              	<span class="lh-typo__label">&nbsp;&nbsp;&nbsp;</span> 
                              	<c:choose>
									<c:when test="${not empty pasoBean.productoBean.cvvCode}">
										<span class="lh-typo__obs">${renderRequest.preferences.getValue('resumenText006','')}</span>
								    	<span class="lh-typo__label">${pasoBean.productoBean.cvvCode}</span>
								  	</c:when>
								  	<c:otherwise>
								  		<span class="lh-typo__obs">${renderRequest.preferences.getValue('resumenText019','')}</span>
								     	<span class="lh-typo__label">${pasoBean.productoBean.codigo4csc}</span>
								     	<div class="lh-tooltip-info lh-tooltip-info--right2">
								     		<span class="lh-tooltip-info__button">
		                              			<i class="lh-icon-i"></i>
		                              		</span>
		                                	<div class="lh-tooltip-info__content"><span class="lh-box">${renderRequest.preferences.getValue('resumenText021','')}</span></div>
		                                </div>
								     	<br/>
								     	<span class="lh-typo__obs">${renderRequest.preferences.getValue('resumenText020','')}</span>
								     	<span class="lh-typo__label">${pasoBean.productoBean.cvv}</span>
								  	</c:otherwise>
								</c:choose>
                              </div>
                            </div>
                          </div>
                        </div>
                        <c:if test="${not empty pasoBean.productoBean.uniquePromotionalCode}">
                        	<div class="lh-ff-section__message text-center"> 
								<p class="lh-typo__list">${commerceText.v601008}</p>
								<h3 class="lh-typo__commontitle lh-typo__commontitle--2">${pasoBean.productoBean.uniquePromotionalCode}</h3>
							</div>
                        </c:if>
                      </div>
                      <div class="lh-ff-section">
                        <h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">${renderRequest.preferences.getValue('resumenText007','')}</h4>
                        <div class="lh-ff-info">
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText008','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDescripcionProducto">${pasoBean.productoBean.descripcionTipoProducto}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText009','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoLineaCredito"><span class="nowrap">S/ ${formatCreditLine}</span></div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText010','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoSeguroDesgravamen"><span class="nowrap">S/ ${pasoBean.productoBean.seguroDesgravamen}</span></div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText011','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoTea">${pasoBean.productoBean.tea}%</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText012','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoSeguro">S/${pasoBean.productoBean.cobroMembresia}${renderRequest.preferences.getValue('resumenText013','')}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText014','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoDiaPago">${pasoBean.productoBean.diaDePago} ${renderRequest.preferences.getValue('resumenText015','')}</div>
                          </div>
                          <div class="lh-ff-info__field">
                            <label class="lh-ff__label">${renderRequest.preferences.getValue('resumenText016','')}</label>
                            <div class="lh-ff-info__content" id="fpa_infoEnvioEstadoCuenta">${pasoBean.productoBean.emailStateAccount}</div>
                          </div>
                        </div>
                      </div>
                      <c:if test="${not pasoBean.expedienteBean.flagConsultarFechaHora}">
	                      ${commerceText.v601007}
                      </c:if>
                    </div>
                    
                    
                  </div>
                </div>
        </div>
      </div>