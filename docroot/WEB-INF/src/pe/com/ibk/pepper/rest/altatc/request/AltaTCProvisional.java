
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AltaTCProvisional {

    private String codigoMoneda;
    private String importeLinea;
    private String codigoCorrespondencia;
    private String importeLineaPepper;
    private String codigoMarca;
    private String codigoConvenio;
    private String indicadorTipo;
    private String diaPago;
    private String indicadorTipoCliente;
    private String indicadorDevolucionCashback;
    private String codigoVendedor;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AltaTCProvisional() {
    }

    /**
     * 
     * @param indicadorTipoCliente
     * @param importeLineaPepper
     * @param codigoMoneda
     * @param indicadorDevolucionCashback
     * @param codigoConvenio
     * @param codigoMarca
     * @param indicadorTipo
     * @param codigoVendedor
     * @param diaPago
     * @param codigoCorrespondencia
     * @param importeLinea
     */
    public AltaTCProvisional(String codigoMoneda, String importeLinea, String codigoCorrespondencia, String importeLineaPepper, String codigoMarca, String codigoConvenio, String indicadorTipo, String diaPago, String indicadorTipoCliente, String indicadorDevolucionCashback, String codigoVendedor) {
        super();
        this.codigoMoneda = codigoMoneda;
        this.importeLinea = importeLinea;
        this.codigoCorrespondencia = codigoCorrespondencia;
        this.importeLineaPepper = importeLineaPepper;
        this.codigoMarca = codigoMarca;
        this.codigoConvenio = codigoConvenio;
        this.indicadorTipo = indicadorTipo;
        this.diaPago = diaPago;
        this.indicadorTipoCliente = indicadorTipoCliente;
        this.indicadorDevolucionCashback = indicadorDevolucionCashback;
        this.codigoVendedor = codigoVendedor;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getImporteLinea() {
        return importeLinea;
    }

    public void setImporteLinea(String importeLinea) {
        this.importeLinea = importeLinea;
    }

    public String getCodigoCorrespondencia() {
        return codigoCorrespondencia;
    }

    public void setCodigoCorrespondencia(String codigoCorrespondencia) {
        this.codigoCorrespondencia = codigoCorrespondencia;
    }

    public String getImporteLineaPepper() {
        return importeLineaPepper;
    }

    public void setImporteLineaPepper(String importeLineaPepper) {
        this.importeLineaPepper = importeLineaPepper;
    }

    public String getCodigoMarca() {
        return codigoMarca;
    }

    public void setCodigoMarca(String codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    public String getCodigoConvenio() {
        return codigoConvenio;
    }

    public void setCodigoConvenio(String codigoConvenio) {
        this.codigoConvenio = codigoConvenio;
    }

    public String getIndicadorTipo() {
        return indicadorTipo;
    }

    public void setIndicadorTipo(String indicadorTipo) {
        this.indicadorTipo = indicadorTipo;
    }

    public String getDiaPago() {
        return diaPago;
    }

    public void setDiaPago(String diaPago) {
        this.diaPago = diaPago;
    }

    public String getIndicadorTipoCliente() {
        return indicadorTipoCliente;
    }

    public void setIndicadorTipoCliente(String indicadorTipoCliente) {
        this.indicadorTipoCliente = indicadorTipoCliente;
    }

    public String getIndicadorDevolucionCashback() {
        return indicadorDevolucionCashback;
    }

    public void setIndicadorDevolucionCashback(String indicadorDevolucionCashback) {
        this.indicadorDevolucionCashback = indicadorDevolucionCashback;
    }

    public String getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(String codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("codigoMoneda", codigoMoneda).append("importeLinea", importeLinea).append("codigoCorrespondencia", codigoCorrespondencia).append("importeLineaPepper", importeLineaPepper).append("codigoMarca", codigoMarca).append("codigoConvenio", codigoConvenio).append("indicadorTipo", indicadorTipo).append("diaPago", diaPago).append("indicadorTipoCliente", indicadorTipoCliente).append("indicadorDevolucionCashback", indicadorDevolucionCashback).append("codigoVendedor", codigoVendedor).toString();
    }

}
