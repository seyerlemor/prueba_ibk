/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ParametroPadrePO}.
 * </p>
 *
 * @author Interbank
 * @see ParametroPadrePO
 * @generated
 */
public class ParametroPadrePOWrapper implements ParametroPadrePO,
	ModelWrapper<ParametroPadrePO> {
	public ParametroPadrePOWrapper(ParametroPadrePO parametroPadrePO) {
		_parametroPadrePO = parametroPadrePO;
	}

	@Override
	public Class<?> getModelClass() {
		return ParametroPadrePO.class;
	}

	@Override
	public String getModelClassName() {
		return ParametroPadrePO.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idParametroPadre", getIdParametroPadre());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("codigoPadre", getCodigoPadre());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());
		attributes.put("estado", getEstado());
		attributes.put("json", getJson());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idParametroPadre = (Long)attributes.get("idParametroPadre");

		if (idParametroPadre != null) {
			setIdParametroPadre(idParametroPadre);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String codigoPadre = (String)attributes.get("codigoPadre");

		if (codigoPadre != null) {
			setCodigoPadre(codigoPadre);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Boolean estado = (Boolean)attributes.get("estado");

		if (estado != null) {
			setEstado(estado);
		}

		String json = (String)attributes.get("json");

		if (json != null) {
			setJson(json);
		}
	}

	/**
	* Returns the primary key of this parametro padre p o.
	*
	* @return the primary key of this parametro padre p o
	*/
	@Override
	public long getPrimaryKey() {
		return _parametroPadrePO.getPrimaryKey();
	}

	/**
	* Sets the primary key of this parametro padre p o.
	*
	* @param primaryKey the primary key of this parametro padre p o
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_parametroPadrePO.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id parametro padre of this parametro padre p o.
	*
	* @return the id parametro padre of this parametro padre p o
	*/
	@Override
	public long getIdParametroPadre() {
		return _parametroPadrePO.getIdParametroPadre();
	}

	/**
	* Sets the id parametro padre of this parametro padre p o.
	*
	* @param idParametroPadre the id parametro padre of this parametro padre p o
	*/
	@Override
	public void setIdParametroPadre(long idParametroPadre) {
		_parametroPadrePO.setIdParametroPadre(idParametroPadre);
	}

	/**
	* Returns the group ID of this parametro padre p o.
	*
	* @return the group ID of this parametro padre p o
	*/
	@Override
	public long getGroupId() {
		return _parametroPadrePO.getGroupId();
	}

	/**
	* Sets the group ID of this parametro padre p o.
	*
	* @param groupId the group ID of this parametro padre p o
	*/
	@Override
	public void setGroupId(long groupId) {
		_parametroPadrePO.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this parametro padre p o.
	*
	* @return the company ID of this parametro padre p o
	*/
	@Override
	public long getCompanyId() {
		return _parametroPadrePO.getCompanyId();
	}

	/**
	* Sets the company ID of this parametro padre p o.
	*
	* @param companyId the company ID of this parametro padre p o
	*/
	@Override
	public void setCompanyId(long companyId) {
		_parametroPadrePO.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this parametro padre p o.
	*
	* @return the user ID of this parametro padre p o
	*/
	@Override
	public long getUserId() {
		return _parametroPadrePO.getUserId();
	}

	/**
	* Sets the user ID of this parametro padre p o.
	*
	* @param userId the user ID of this parametro padre p o
	*/
	@Override
	public void setUserId(long userId) {
		_parametroPadrePO.setUserId(userId);
	}

	/**
	* Returns the user uuid of this parametro padre p o.
	*
	* @return the user uuid of this parametro padre p o
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _parametroPadrePO.getUserUuid();
	}

	/**
	* Sets the user uuid of this parametro padre p o.
	*
	* @param userUuid the user uuid of this parametro padre p o
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_parametroPadrePO.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this parametro padre p o.
	*
	* @return the user name of this parametro padre p o
	*/
	@Override
	public java.lang.String getUserName() {
		return _parametroPadrePO.getUserName();
	}

	/**
	* Sets the user name of this parametro padre p o.
	*
	* @param userName the user name of this parametro padre p o
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_parametroPadrePO.setUserName(userName);
	}

	/**
	* Returns the create date of this parametro padre p o.
	*
	* @return the create date of this parametro padre p o
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _parametroPadrePO.getCreateDate();
	}

	/**
	* Sets the create date of this parametro padre p o.
	*
	* @param createDate the create date of this parametro padre p o
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_parametroPadrePO.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this parametro padre p o.
	*
	* @return the modified date of this parametro padre p o
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _parametroPadrePO.getModifiedDate();
	}

	/**
	* Sets the modified date of this parametro padre p o.
	*
	* @param modifiedDate the modified date of this parametro padre p o
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_parametroPadrePO.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the codigo padre of this parametro padre p o.
	*
	* @return the codigo padre of this parametro padre p o
	*/
	@Override
	public java.lang.String getCodigoPadre() {
		return _parametroPadrePO.getCodigoPadre();
	}

	/**
	* Sets the codigo padre of this parametro padre p o.
	*
	* @param codigoPadre the codigo padre of this parametro padre p o
	*/
	@Override
	public void setCodigoPadre(java.lang.String codigoPadre) {
		_parametroPadrePO.setCodigoPadre(codigoPadre);
	}

	/**
	* Returns the nombre of this parametro padre p o.
	*
	* @return the nombre of this parametro padre p o
	*/
	@Override
	public java.lang.String getNombre() {
		return _parametroPadrePO.getNombre();
	}

	/**
	* Sets the nombre of this parametro padre p o.
	*
	* @param nombre the nombre of this parametro padre p o
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_parametroPadrePO.setNombre(nombre);
	}

	/**
	* Returns the descripcion of this parametro padre p o.
	*
	* @return the descripcion of this parametro padre p o
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _parametroPadrePO.getDescripcion();
	}

	/**
	* Sets the descripcion of this parametro padre p o.
	*
	* @param descripcion the descripcion of this parametro padre p o
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_parametroPadrePO.setDescripcion(descripcion);
	}

	/**
	* Returns the estado of this parametro padre p o.
	*
	* @return the estado of this parametro padre p o
	*/
	@Override
	public boolean getEstado() {
		return _parametroPadrePO.getEstado();
	}

	/**
	* Returns <code>true</code> if this parametro padre p o is estado.
	*
	* @return <code>true</code> if this parametro padre p o is estado; <code>false</code> otherwise
	*/
	@Override
	public boolean isEstado() {
		return _parametroPadrePO.isEstado();
	}

	/**
	* Sets whether this parametro padre p o is estado.
	*
	* @param estado the estado of this parametro padre p o
	*/
	@Override
	public void setEstado(boolean estado) {
		_parametroPadrePO.setEstado(estado);
	}

	/**
	* Returns the json of this parametro padre p o.
	*
	* @return the json of this parametro padre p o
	*/
	@Override
	public java.lang.String getJson() {
		return _parametroPadrePO.getJson();
	}

	/**
	* Sets the json of this parametro padre p o.
	*
	* @param json the json of this parametro padre p o
	*/
	@Override
	public void setJson(java.lang.String json) {
		_parametroPadrePO.setJson(json);
	}

	@Override
	public boolean isNew() {
		return _parametroPadrePO.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_parametroPadrePO.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _parametroPadrePO.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_parametroPadrePO.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _parametroPadrePO.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _parametroPadrePO.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_parametroPadrePO.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _parametroPadrePO.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_parametroPadrePO.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_parametroPadrePO.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_parametroPadrePO.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ParametroPadrePOWrapper((ParametroPadrePO)_parametroPadrePO.clone());
	}

	@Override
	public int compareTo(
		pe.com.ibk.pepper.model.ParametroPadrePO parametroPadrePO) {
		return _parametroPadrePO.compareTo(parametroPadrePO);
	}

	@Override
	public int hashCode() {
		return _parametroPadrePO.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<pe.com.ibk.pepper.model.ParametroPadrePO> toCacheModel() {
		return _parametroPadrePO.toCacheModel();
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO toEscapedModel() {
		return new ParametroPadrePOWrapper(_parametroPadrePO.toEscapedModel());
	}

	@Override
	public pe.com.ibk.pepper.model.ParametroPadrePO toUnescapedModel() {
		return new ParametroPadrePOWrapper(_parametroPadrePO.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _parametroPadrePO.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _parametroPadrePO.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_parametroPadrePO.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ParametroPadrePOWrapper)) {
			return false;
		}

		ParametroPadrePOWrapper parametroPadrePOWrapper = (ParametroPadrePOWrapper)obj;

		if (Validator.equals(_parametroPadrePO,
					parametroPadrePOWrapper._parametroPadrePO)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ParametroPadrePO getWrappedParametroPadrePO() {
		return _parametroPadrePO;
	}

	@Override
	public ParametroPadrePO getWrappedModel() {
		return _parametroPadrePO;
	}

	@Override
	public void resetOriginalValues() {
		_parametroPadrePO.resetOriginalValues();
	}

	private ParametroPadrePO _parametroPadrePO;
}