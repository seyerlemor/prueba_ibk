package pe.com.ibk.pepper.util;

import java.util.List;

public class Entity {

	private String name;
	private String text;
	private List<Entity> childs;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<Entity> getChilds() {
		return childs;
	}
	public void setChilds(List<Entity> childs) {
		this.childs = childs;
	}
	
}
