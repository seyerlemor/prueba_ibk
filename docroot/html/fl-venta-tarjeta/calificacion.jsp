<%@ include file="/html/init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="validarCalificacionV3" var="validarCalificacionV3" />
<portlet:resourceURL id="getDataConyuge" var="getDataConyuge" />
<portlet:resourceURL id="validateRuc" var="validateRuc" />
<portlet:resourceURL var="getListUbigeo" id="getUbigeo" />

<div class="lq-container">
	<div class="lh-ff">
		<div class="lh-ff-block lh-ff-block--active">
			<div class="lq-row">
				${commerceText.v401001}
				<div class="lh-ff-content-col">
					<form:form action="${validarCalificacionV3}" autocomplete="off" commandName="clienteBean" class="lh-ff-form" novalidate="novalidate">
						<div class="lh-ff-section lh-ff-section--header">
							<div
								class="lh-ff-section__wrapper lh-ff-section__wrapper--image-to-left">
								<div class="lh-ff-section__image"
									style="background-image: url(/pepper-v2-theme/images/tc/icon-tarjeta-chip.png)"></div>
								<div class="lh-ff-section__text">
									<h3 class="lh-typo__commontitle lh-typo__commontitle--1">
										${commerceText.v401002}
									</h3>
									${commerceText.v401003}
								</div>
							</div>
						</div>
						<c:if test="${pasoBean.productoBean.flujoCampana == 3}">
							<div class="lh-ff-section">
								<h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">Datos personales</h4>
									<div class="lh-ff-group">
			                                  <div class="lh-ff-group__item">
			                                    <label class="lh-ff__label">Estado civil</label>
			                                    <div class="lh-ff-group__field">
			                                      <input id="fct_hiddenCivilStatus" type="hidden" name="civilStatus" data-validate-required="radio" data-validate-required-message="Seleccione su estado civil">
			                                            <div class="lh-form__radio">
			                                              <input id="fct_civilStatus1" type="radio" name="civilStatus" value="U">
			                                              <label for="fct_civilStatus1"><span>Soltero</span></label>
			                                            </div>
			                                            <div class="lh-form__radio">
			                                              <input id="fct_civilStatus2" type="radio" name="civilStatus" value="M" data-related="#fct-edit-civil-status2">
			                                              <label for="fct_civilStatus2"><span>Casado</span></label>
			                                            </div>
			                                            <div class="lh-form__radio">
			                                              <input id="fct_civilStatus4" type="radio" name="civilStatus" value="O" data-related="#fct-edit-civil-status2">
			                                              <label for="fct_civilStatus4"><span>Conviviente</span></label>
			                                            </div>
			                                            <div class="lh-form__radio">
			                                              <input id="fct_civilStatus5" type="radio" name="civilStatus" value="D">
			                                              <label for="fct_civilStatus5"><span>Divorciado</span></label>
			                                            </div>
			                                            <div class="lh-form__radio">
			                                              <input id="fct_civilStatus6" type="radio" name="civilStatus" value="W">
			                                              <label for="fct_civilStatus6"><span>Viudo</span></label>
			                                            </div>
			                                    </div>
			                                  </div>
			                          </div>
							</div>
							<div class="lh-ff-related-content lh-ff-section" id="fct-edit-civil-status2">
						                    <h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">${renderRequest.preferences.getValue('calificacionDatosConyuge','')}</h4>
						                                <div class="lh-ff-group">
						                                              <div class="lh-ff-group__item lh-ff-group__item--md">
						                                                <label class="lh-ff__label" for="docPartner">Número de DNI del cónyuge
						                                                </label>
						                                                            <div class="lh-ff-group__field">
						                                                                    <input class="lh-input lh-input--maxlength" id="docPartner" inputmode="numeric" maxlength="8" name="docPartner" pattern="[0-9]*" type="tel" data-allowed-keys="integer" data-validate-required="numeric" data-validate-required-message="Ingrese el número de DNI del cónyuge" data-validate-length="8" data-validate-length-message="El DNI debe contar con 8 caracteres" data-partner-remote="${getDataConyuge}" data-partner-remote-target="#fct_partnerInfo">
						                                                            </div>
						                                                <div class="lh-ff-helper lh-typo__p3 lh-hide" style="color: #9c9c9c;"></div>
						                                              </div>
						                                </div>
						                          <div id="fct_partnerInfo">
						                                  <div class="lh-ff-group lh-hide">
						                                                <div class="lh-ff-group__item">
						                                                  <label class="lh-ff__label" for="fct_namesPartner">Nombre y apellido del cónyuge
						                                                  </label>
						                                                              <div class="lh-ff-group__field">
						                                                                      <input class="lh-input" id="fct_namesPartner" disabled name="namesPartner" maxlength="50" type="text" data-allowed-keys="simpleText" data-validate-required data-validate-required-message="Ingrese el nombre y apellido del cónyuge">
						                                                              </div>
						                                                </div>
						                                  </div>
						                                  <div class="lh-ff-group lh-hide">
						                                                <div class="lh-ff-group__item lh-ff-group__item--md">
						                                                  <label class="lh-ff__label" for="fct_firstNamePartner">Nombre
						                                                  </label>
						                                                              <div class="lh-ff-group__field">
						                                                                      <input class="lh-input" id="fct_firstNamePartner" name="firstnamePartner" maxlength="50" type="text" data-allowed-keys="simpleText" data-validate-required data-validate-required-message="Ingrese el primer nombre del cónyuge">
						                                                              </div>
						                                                </div>
						                                  </div>
						                                  <div class="lh-ff-group lh-hide">
						                                                <div class="lh-ff-group__item lh-ff-group__item--md">
						                                                  <label class="lh-ff__label" for="fct_secondNamePartner">Segundo Nombre
						                                                  </label>
						                                                              <div class="lh-ff-group__field">
						                                                                      <input class="lh-input" id="fct_secondNamePartner" name="secondnamePartner" maxlength="50" type="text" data-allowed-keys="simpleText">
						                                                              </div>
						                                                </div>
						                                  </div>
						                                  <div class="lh-ff-group lh-hide">
						                                                <div class="lh-ff-group__item lh-ff-group__item--md">
						                                                  <label class="lh-ff__label" for="fct_paternalSurnamePartner">Apellido paterno
						                                                  </label>
						                                                              <div class="lh-ff-group__field">
						                                                                      <input class="lh-input" id="fct_paternalSurnamePartner" name="paternalsurnamePartner" maxlength="50" type="text" data-allowed-keys="simpleText" data-validate-required data-validate-required-message="Ingrese el apellido paterno del cónyuge">
						                                                              </div>
						                                                </div>
						                                  </div>
						                                  <div class="lh-ff-group lh-hide">
						                                                <div class="lh-ff-group__item lh-ff-group__item--md">
						                                                  <label class="lh-ff__label" for="fct_maternalSurnamePartner">Apellido materno
						                                                  </label>
						                                                              <div class="lh-ff-group__field">
						                                                                      <input class="lh-input" id="fct_maternalSurnamePartner" name="maternalsurnamePartner" maxlength="50" type="text" data-allowed-keys="simpleText" data-validate-required data-validate-required-message="Ingrese el apellido materno del cónyuge">
						                                                              </div>
						                                                </div>
						                                  </div>
						                                  <div class="lh-ff-group lh-hide">
						                                                <div class="lh-ff-group__item lh-ff-group__item--xs">
						                                                  <label class="lh-ff__label" for="fct_birthdayPartner">Fecha de nacimiento
						                                                  </label>
						                                                              <div class="lh-ff-group__field">
						                                                                      <input class="lh-input lh-input--date" id="fct_birthdayPartner" maxlength="10" name="birthdatePartner" data-validate-date data-validate-date-message="La fecha no es válida" data-validate-date-min="01/01/1920" data-validate-date-min-message="La fecha esta fuera del rango permitido" data-validate-date-max="31/12/1999" data-validate-date-max-message="La fecha esta fuera del rango permitido" data-validate-required data-validate-required-message="Ingrese la fecha de nacimiento del cónyuge" type="text">
						                                                              </div>
						                                                </div>
						                                  </div>
						                          </div>
						                  </div>
						</c:if>
						<c:if test="${pasoBean.clienteBean.flagDireccion=='true' || pasoBean.productoBean.flujoCampana == 3}">
							<div class="lh-ff-section">
								<h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">
									${renderRequest.preferences.getValue('calificacionDatosDomicilio','')}
								</h4>
								<label class="lh-ff__label lh-ff__label--main">
									${renderRequest.preferences.getValue('calificacionText002','')}
								</label>
								<div class="lh-ff-group lh-ff-group__ubigeo">
									<div class="lh-ff-group__item">
										<label class="lh-ff__label" for="fct-addressDepartamento">Departamento
										</label>
										<div class="lh-ff-group__field">
											<div class="lh__select lh__select--checked">
												<select class="ff_selectDepartamento"
													id="fct-addressDepartamento" name="addressDepartamento"
													data-validate-required
													data-validate-required-message="Selecciona un departamento">
													<option value disabled selected>Seleccione</option>
													<c:forEach items="${lstDepartamentos}" var="depar">
														<option value="${depar.key}">${depar.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="lh-ff-group__item">
										<label class="lh-ff__label" for="fct-addressProvincia">Provincia
										</label>
										<div class="lh-ff-group__field">
											<div
												class="lh__select lh__select--checked lh__select__disabled">
												<select class="ff_selectProvincia" id="fct-addressProvincia"
													name="addressProvincia" disabled data-validate-required
													data-validate-required-message="Selecciona una Provincia"
													data-remote="${getListUbigeo}&tipo=1&parent="
													data-remote-bind="#fct-addressDepartamento">
													<option value disabled selected>Seleccione</option>
												</select>
											</div>
										</div>
									</div>
									<div class="lh-ff-group__item">
										<label class="lh-ff__label" for="fct-addressDistrito">Distrito
										</label>
										<div class="lh-ff-group__field">
											<div
												class="lh__select lh__select--checked lh__select__disabled">
												<select class="ff_selectDistrito" id="fct-addressDistrito"
													name="addressDistrito" disabled data-validate-required
													data-validate-required-message="Selecciona un Distrito"
													data-remote="${getListUbigeo}&tipo=2&parent="
													data-remote-bind="#fct-addressProvincia">
													<option value disabled selected>Seleccione</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="lh-ff-group lh-ff-group--custom">
									<div class="lh-ff-group__item lh-ff-group__item--xs">
										<label class="lh-ff__label" for="fct-addressType">Tipo
											de vía </label>
										<div class="lh-ff-group__field">
											<div class="lh__select lh__select--checked">
												<select id="fct-addressType" name="addressType"
													data-validate-required
													data-validate-required-message="Seleccione un tipo de vía">
													<option value disabled selected>Seleccione</option>
													<c:forEach items="${requestScope['lstTipoVia']}" var="via">
														<option value="${via.key}">${via.value}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="lh-ff-group__item">
										<label class="lh-ff__label" for="fct-addressName">Nombre
											de la vía </label>
										<div class="lh-ff-group__field">
											<input class="lh-input" id="fct-addressName" maxlength="37"
												name="addressName" data-allowed-keys="letter"
												data-allowed-keys-add="0-9 " data-validate-required
												data-validate-required-message="Ingrese el nombre de la vía"
												type="text">
										</div>
									</div>
								</div>
								<div class="lh-ff-group-wrapper lh-ff-address-info">
									<div class="lh-ff-group lh-ff-group--small-fields">
										<div class="lh-ff-group__item">
											<label class="lh-ff__label" for="fct-addressNro">Número
											</label>
											<div class="lh-ff-group__field">
												<input class="lh-input lh-ff-address-info__numero"
													id="fct-addressNro" maxlength="5" name="addressNro"
													type="tel" inputmode="numeric" pattern="[0-9]*"
													data-allowed-keys="integer" data-validate>
											</div>
										</div>
										<div class="lh-ff-group__item">
											<label class="lh-ff__label" for="fct-addressMz">Manzana
											</label>
											<div class="lh-ff-group__field">
												<input class="lh-input lh-ff-address-info__mnza"
													id="fct-addressMz" maxlength="5" name="addressMz"
													data-allowed-keys="alphanumeric" data-allowed-keys-add="ñÑ"
													data-validate type="text">
											</div>
										</div>
										<div class="lh-ff-group__item">
											<label class="lh-ff__label" for="fct-addressLt">Lote
											</label>
											<div class="lh-ff-group__field">
												<input class="lh-input lh-ff-address-info__lote"
													id="fct-addressLt" maxlength="5" name="addressLt"
													data-allowed-keys="alphanumeric" data-allowed-keys-add="ñÑ"
													data-validate type="text">
											</div>
										</div>
										<div class="lh-ff-group__item">
											<label class="lh-ff__label" for="fct-addressInt">Interior
											</label>
											<div class="lh-ff-group__field">
												<input class="lh-input lh-ff-address-info__interior"
													id="fct-addressInt" maxlength="5" name="addressInt"
													data-allowed-keys="alphanumeric" data-allowed-keys-add="ñÑ"
													data-validate type="text">
											</div>
										</div>
									</div>
									<input type="hidden"
										data-validate="{&quot;errorContainer&quot;:&quot;.lh-ff-address-info&quot;}"
										data-validate-custom-address-info
										data-validate-custom-address-info-message="Debe ingresar una dirección completa">
								</div>
							</div>
						</c:if>
						<c:if test="${pasoBean.productoBean.flujoCampana == 3 || pasoBean.clienteBean.flagCliente == 'N'}">
							<div class="lh-ff-section">
								<h4 class="lh-typo__thirdtitle lh-typo__thirdtitle--2 lh-ff-section__title">
									${renderRequest.preferences.getValue('calificacionDatosLaborales','')}
								</h4>
								<c:if test="${pasoBean.productoBean.flujoCampana == 3}">
									<div class="lh-ff-group">
										<div class="lh-ff-group__item lh-ff-group__item--md">
											<label class="lh-ff__label" for="fct_employmentStatus">Situación
												laboral </label>
											<div class="lh-ff-group__field-with-link">
												<div class="lh-ff-group__field">
													<div class="lh__select lh__select--checked">
														<select id="fct_employmentStatus"
															name="employmentStatus" data-actions
															data-validate-required="text"
															data-validate-required-message="Selecciona situación laboral"
															data-validate="{&quot;errorContainer&quot;:&quot;.lh-ff-group__field-with-link&quot;}">
															<option value="" selected disabled>Seleccione</option>
															<c:forEach items="${lstSituacionLaboral}" var="lab">
																<option value="${lab.key}" data-action="">${lab.value}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<a class="lh-btn--link with-arrow lh-ff__link" href="#"
												data-lightbox="#ftr_lbSL">${renderRequest.preferences.getValue('calificacionText003','')}</a>
										</div>
									</div>
								</c:if>
								<c:if test="${pasoBean.clienteBean.flagCliente == 'N'}">
									<div class="lh-ff-group">
										<div class="lh-ff-group__item lh-ff-group__item--md">
											<label class="lh-ff__label" for="fct_occupation">Ocupación
											</label>
											<div class="lh-ff-group__field">
												<div class="lh__select lh__select--checked">
													<select id="fct_occupation" name="ocupacion"
														data-validate-required
														data-validate-required-message="Seleccione una ocupación">
														<option value selected disabled>Seleccione</option>
														<c:forEach items="${lstOcupacion}" var="ocu">
															<option value="${ocu.key}" data-action="">${ocu.value}</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div
										class="lh-ff-related-content lh-ff-group-wrapper lh-ff-related-content--active"
										id="fct-group-ruc-razonsocial">
										<div class="lh-ff-group">
											<div class="lh-ff-group__item">
												<label class="lh-ff__label" for="fct_companyName">Razón
													social de tu empresa </label>
												<div class="lh-ff-group__field">
													<input class="lh-input lh-input-ruc-razonsocial"
														id="fct_companyName" name="companyName" maxlength="50"
														data-allowed-keys="letter"
														data-allowed-keys-add="0-9 ®©&amp;.,-"
														data-validate-required
														data-validate-required-message="Ingrese la razón social de su empresa"
														data-no-paste type="text"> <input
														id="companyName" type="hidden" name="companyName"
														as="input">
												</div>
											</div>
										</div>
									</div>
								</c:if>
							</div>
						</c:if>
						<div class="lh-ff-section">
							<div class="lh-ff-group">
								<div class="lh-ff-group__item lh-ff-group__item--buttons">
									<button
										class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit">
										<span>${commerceText.v401004}</span>
									</button>
								</div>
							</div>
							<div class="lh-form__error">
								<div class="lh-typo__p2 lh-form__error-message"
									data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="lh-lightbox" id="ftr_lbSL">
        ${commerceText.v401005}
      </div>