package pe.com.ibk.pepper.rest.generic.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MessageResponse implements Serializable{

	private static final long serialVersionUID = -4616748992281972912L;

	@SerializedName("Body")
	private Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

}
