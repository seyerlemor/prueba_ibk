
package pe.com.ibk.pepper.rest.equifax.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InformacionAdicional {

    private String modelo;
    private String numeroOperacion;
   
    @JsonCreator
    public InformacionAdicional(String modelo) {
		super();
		this.modelo = modelo;
	}
    
    @JsonCreator
    public InformacionAdicional(String modelo, String numeroOperacion) {
		super();
		this.modelo = modelo;
		this.numeroOperacion = numeroOperacion;
	}

    @JsonProperty("modelo")
	public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

	public String getNumeroOperacion() {
		return numeroOperacion;
	}

	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}

}
