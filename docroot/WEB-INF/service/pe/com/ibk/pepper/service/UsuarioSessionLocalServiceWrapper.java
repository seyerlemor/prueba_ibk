/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UsuarioSessionLocalService}.
 *
 * @author Interbank
 * @see UsuarioSessionLocalService
 * @generated
 */
public class UsuarioSessionLocalServiceWrapper
	implements UsuarioSessionLocalService,
		ServiceWrapper<UsuarioSessionLocalService> {
	public UsuarioSessionLocalServiceWrapper(
		UsuarioSessionLocalService usuarioSessionLocalService) {
		_usuarioSessionLocalService = usuarioSessionLocalService;
	}

	/**
	* Adds the usuario session to the database. Also notifies the appropriate model listeners.
	*
	* @param usuarioSession the usuario session
	* @return the usuario session that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.UsuarioSession addUsuarioSession(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.addUsuarioSession(usuarioSession);
	}

	/**
	* Creates a new usuario session with the primary key. Does not add the usuario session to the database.
	*
	* @param idUsuarioSession the primary key for the new usuario session
	* @return the new usuario session
	*/
	@Override
	public pe.com.ibk.pepper.model.UsuarioSession createUsuarioSession(
		long idUsuarioSession) {
		return _usuarioSessionLocalService.createUsuarioSession(idUsuarioSession);
	}

	/**
	* Deletes the usuario session with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idUsuarioSession the primary key of the usuario session
	* @return the usuario session that was removed
	* @throws PortalException if a usuario session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.UsuarioSession deleteUsuarioSession(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.deleteUsuarioSession(idUsuarioSession);
	}

	/**
	* Deletes the usuario session from the database. Also notifies the appropriate model listeners.
	*
	* @param usuarioSession the usuario session
	* @return the usuario session that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.UsuarioSession deleteUsuarioSession(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.deleteUsuarioSession(usuarioSession);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _usuarioSessionLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public pe.com.ibk.pepper.model.UsuarioSession fetchUsuarioSession(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.fetchUsuarioSession(idUsuarioSession);
	}

	/**
	* Returns the usuario session with the primary key.
	*
	* @param idUsuarioSession the primary key of the usuario session
	* @return the usuario session
	* @throws PortalException if a usuario session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.UsuarioSession getUsuarioSession(
		long idUsuarioSession)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.getUsuarioSession(idUsuarioSession);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the usuario sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.UsuarioSessionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of usuario sessions
	* @param end the upper bound of the range of usuario sessions (not inclusive)
	* @return the range of usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.UsuarioSession> getUsuarioSessions(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.getUsuarioSessions(start, end);
	}

	/**
	* Returns the number of usuario sessions.
	*
	* @return the number of usuario sessions
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getUsuarioSessionsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.getUsuarioSessionsCount();
	}

	/**
	* Updates the usuario session in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param usuarioSession the usuario session
	* @return the usuario session that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.UsuarioSession updateUsuarioSession(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.updateUsuarioSession(usuarioSession);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _usuarioSessionLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_usuarioSessionLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _usuarioSessionLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public pe.com.ibk.pepper.model.UsuarioSession registrarUsuarioSession(
		pe.com.ibk.pepper.model.UsuarioSession usuarioSession)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _usuarioSessionLocalService.registrarUsuarioSession(usuarioSession);
	}

	@Override
	public pe.com.ibk.pepper.model.UsuarioSession buscarUsuarioSessionporId(
		long idUsuarioSession) {
		return _usuarioSessionLocalService.buscarUsuarioSessionporId(idUsuarioSession);
	}

	@Override
	public pe.com.ibk.pepper.model.UsuarioSession buscarUsuarioSession(
		java.lang.String idUsuarioSession) {
		return _usuarioSessionLocalService.buscarUsuarioSession(idUsuarioSession);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UsuarioSessionLocalService getWrappedUsuarioSessionLocalService() {
		return _usuarioSessionLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUsuarioSessionLocalService(
		UsuarioSessionLocalService usuarioSessionLocalService) {
		_usuarioSessionLocalService = usuarioSessionLocalService;
	}

	@Override
	public UsuarioSessionLocalService getWrappedService() {
		return _usuarioSessionLocalService;
	}

	@Override
	public void setWrappedService(
		UsuarioSessionLocalService usuarioSessionLocalService) {
		_usuarioSessionLocalService = usuarioSessionLocalService;
	}

	private UsuarioSessionLocalService _usuarioSessionLocalService;
}