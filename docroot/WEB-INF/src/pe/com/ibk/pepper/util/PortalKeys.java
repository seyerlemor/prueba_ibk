package pe.com.ibk.pepper.util;

import com.liferay.util.portlet.PortletProps;

public abstract class PortalKeys {

	public static final String WS_REQUEST_TIMEOUT = "ws.request.timeout";
	public static final String WS_CONNECT_TIMEOUT = "ws.connect.timeout";

	public static final String REST_CONNECT_TIMEOUT = "rs.connect.timeout";
	public static final String REST_READ_TIMEOUT = "rs.read.timeout";

	public static final String REST_UBICACION_JKS = "rs.ubicacion.jks";

	public static final String REST_API_JKS_NAME = "rs.api.jks.name";
	public static final String REST_API_JKS_PASSW = "rs.api.jks.passw";

	public static final String REST_BUS_JKS_NAME = "rs.bus.jks.name";
	public static final String REST_BUS_JKS_PASSW = "rs.bus.jks.passw";

	public static final String REST_R1_ENDPOINT = "rs.r1.endpoint";
	public static final String REST_R1_RESOURCE = "rs.r1.resource";
	public static final String REST_R1_USUARIO = "rs.r1.usuario";
	public static final String REST_R1_PASSW = "rs.r1.passw";

	public static final String REST_CAMPANIA_ENDPOINT = "rs.campania.endpoint";
	public static final String REST_CAMPANIA_RESOURCE = "rs.campania.resource";
	public static final String REST_CAMPANIA_USUARIO = "rs.campania.usuario";
	public static final String REST_CAMPANIA_PASSW = "rs.campania.passw";

	public static final String WS_RENIEC_ENDPOINT = "ws.reniec.endpoint";
	public static final String WS_RENIEC_USUARIO = "ws.reniec.usuario";
	public static final String WS_RENIEC_PASSW = "ws.reniec.passw";
	public static final String WS_RENIEC_RUTA_FOTO = "ws.reniec.ruta.foto";
	public static final String WS_RENIEC_INTENTOS = "ws.reniec.intentos";

	public static final String REST_EQUIFAX_ENDPOINT = "rs.equifax.endpoint";
	public static final String REST_EQUIFAX_CONSULTAR_RESOURCE = "rs.equifax.consultar.resource";
	public static final String REST_EQUIFAX_VALIDAR_RESOURCE = "rs.equifax.validar.resource";
	public static final String REST_EQUIFAX_USUARIO = "rs.equifax.usuario";
	public static final String REST_EQUIFAX_PASSW = "rs.equifax.passw";
	public static final String WS_EQUIFAX_INTENTOS = "ws.equifax.intentos";

	public static final String REST_TIPIFICAR_ENDPOINT = "rs.tipificar.endpoint";
	public static final String REST_TIPIFICAR_RESOURCE = "rs.tipificar.resource";
	public static final String REST_TIPIFICAR_USUARIO = "rs.tipificar.usuario";
	public static final String REST_TIPIFICAR_PASSW = "rs.tipificar.passw";

	public static final String REST_ACT_RESP_ENDPOINT = "rs.act.rpta.endpoint";
	public static final String REST_ACT_RESP_RESOURCE = "rs.act.rpta.resource";
	public static final String REST_ACT_RESP_USUARIO = "rs.act.rpta.usuario";
	public static final String REST_ACT_RESP_PASSW = "rs.act.rpta.passw";

	public static final String REST_OBT_INF_ENDPOINT = "rs.obt.info.endpoint";
	public static final String REST_OBT_INF_RESOURCE = "rs.obt.info.resource";
	public static final String REST_OBT_INF_USUARIO = "rs.obt.info.usuario";
	public static final String REST_OBT_INF_PASSW = "rs.obt.info.passw";

	public static final String REST_CALIF_CDA_ENDPOINT = "rs.calif.cda.endpoint";
	public static final String REST_CALIF_CDA_RESOURCE = "rs.calif.cda.resource";
	public static final String REST_CALIF_CDA_USUARIO = "rs.calif.cda.usuario";
	public static final String REST_CALIF_CDA_PASSW = "rs.calif.cda.passw";

	public static final String REST_AUT_TOKEN_ENDPOINT = "rs.aut.token.endpoint";
	public static final String REST_AUT_TOKEN_RESOURCE = "rs.aut.token.resource";
	public static final String REST_AUT_TOKEN_USUARIO = "rs.aut.token.usuario";
	public static final String REST_AUT_TOKEN_PASSW = "rs.aut.token.passw";
	
	public static final String REST_CONS_AFILIACION_ENDPOINT = "rs.cons.afil.endpoint";
	public static final String REST_CONS_AFILIACION_RESOURCE = "rs.cons.afil.resource";
	public static final String REST_CONS_AFILIACION_USUARIO = "rs.cons.afil.usuario";
	public static final String REST_CONS_AFILIACION_PASSW = "rs.cons.afil.passw";

	//Venta TC
	public static final String REST_VENTA_TC_PROVISIONAL_ENDPOINT = "rs.venta.tc.endpoint";
	public static final String REST_VENTA_TC_PROVISIONAL_RESOURCE = "rs.venta.tc.resource";
	public static final String REST_VENTA_TC_PROVISIONAL_USUARIO = "rs.venta.tc.usuario";
	public static final String REST_VENTA_TC_PROVISIONAL_PASSW = "rs.venta.tc.passw";
	//Consultar POTC
	public static final String REST_CONSULTAR_POTC_ENDPOINT = "rs.cons.potc.endpoint";
	public static final String REST_CONSULTAR_POTC_RESOURCE = "rs.cons.potc.resource";
	public static final String REST_CONSULTAR_POTC_USUARIO = "rs.cons.potc.usuario";
	public static final String REST_CONSULTAR_POTC_PASSW = "rs.cons.potc.passw";
	//Generar POTC
	public static final String REST_GENERAR_POTC_ENDPOINT = "rs.gen.potc.endpoint";
	public static final String REST_GENERAR_POTC_RESOURCE = "rs.gen.potc.resource";
	public static final String REST_GENERAR_POTC_USUARIO = "rs.gen.potc.usuario";
	public static final String REST_GENERAR_POTC_PASSW = "rs.gen.potc.passw";
	//Validacion Token
	public static final String REST_VALID_TOKEN_ENDPOINT = "rs.val.token.endpoint";
	public static final String REST_VALID_TOKEN_RESOURCE = "rs.val.token.resource";
	public static final String REST_VALID_TOKEN_USUARIO = "rs.val.token.usuario";
	public static final String REST_VALID_TOKEN_PASSW = "rs.val.token.passw";
		
	public static final String REST_CAMPANIA_CODIGO_PRODUCTO = "rs.campania.codigo.producto";
	public static final String REST_CAMPANIA_CODIGO_PRESTAMO = "rs.campania.codigo.prestamo";
	public static final String REST_CAMPANIA_CODIGO_SEGURO = "rs.campania.codigo.seguro";
	public static final String REST_CAMPANIA_CODIGO_BAIS = "rs.campania.codigo.bais";

	public static final String REST_EQUIFAX_MODELO1 = "rs.equifax.modelo.uno";
	public static final String REST_EQUIFAX_MODELO2 = "rs.equifax.modelo.dos";	
	
	public static final String REST_SERVER_FIX = "rs.server.fix";
	public static final String REST_HOST_NAME = "rs.server.host.name";
	
	public static final String TIPO_ESTADO = PortletProps.get("reporte.pepper.estado");			
	public static final String REPORTE_BACKEND = PortletProps.get("reporte.backend");	
	
	public static final String WEB_RECAPTCHA_KEY = "codigo.recaptcha.google";
	
	//Consultar Provisional
	public static final String REST_CONSULTAR_PROV_ENDPOINT = "rs.cons.prov.endpoint";
	public static final String REST_CONSULTAR_PROV_RESOURCE = "rs.cons.prov.resource";
	public static final String REST_CONSULTAR_PROV_USUARIO = "rs.cons.prov.usuario";
	public static final String REST_CONSULTAR_PROV_PASSW = "rs.cons.prov.passw";
	
	//Venta TC
		public static final String REST_OBTENER_TARJETA_ENDPOINT = "rs.obt.tarjeta.endpoint";
		public static final String REST_OBTENER_TARJETA_RESOURCE = "rs.obt.tarjeta.resource";
		public static final String REST_OBTENER_TARJETA_USUARIO = "rs.obt.tarjeta.usuario";
		public static final String REST_OBTENER_TARJETA_PASSW = "rs.obt.tarjeta.passw";
	
	public static final String REST_CONSULTAR_TARJETA_ENDPOINT = "rs.cons.tarjeta.endpoint";
	public static final String REST_CONSULTAR_TARJETA_RESOURCE = "rs.cons.tarjeta.resource";
	public static final String REST_CONSULTAR_TARJETA_USUARIO = "rs.cons.tarjeta.usuario";
	public static final String REST_CONSULTAR_TARJETA_PASSW = "rs.cons.tarjeta.passw";
	
	public static final String REST_VENTA_TC_PROVISIONAL_FECHA_PAGO = "rs.venta.tc.fpago";
	
	public static final String REST_ENVIO_CORREO_ENDPOINT = "rs.envio.correo.endpoint";
	public static final String REST_ENVIO_CORREO_RESOURCE = "rs.envio.correo.resource";
	public static final String REST_ENVIO_CORREO_USUARIO = "rs.envio.correo.usuario";
	public static final String REST_ENVIO_CORREO_PASSW = "rs.envio.correo.passw";
	
	public static final String MONTO_INICIAL_MEGALINEA = "pepper.monto.ini.megalinea";
	public static final String HABILITAR_PAS2 = "pepper.habilitar.paso2";
	public static final String HABILITAR_V2CDA = "pepper.habilitar.version2CDA";
	
	//Logica de Reingreso
	public static final String REST_LOGICA_REINGRESO_ENDPOINT = "rs.log.reingreso.endpoint";
	public static final String REST_LOGICA_REINGRESO_RESOURCE = "rs.log.reingreso.resource";
	public static final String REST_LOGICA_REINGRESO_USUARIO = "rs.log.reingreso.usuario";
	public static final String REST_LOGICA_REINGRESO_PASSW = "rs.log.reingreso.passw";
	
	public static final String REST_CONS_SCORE_EFL_ENDPOINT = "rs.cons.efl.endpoint";
	public static final String REST_CONS_SCORE_EFL_RESOURCE = "rs.cons.efl.resource";
	public static final String REST_CONS_SCORE_EFL_USUARIO = "rs.cons.efl.usuario";
	public static final String REST_CONS_SCORE_EFL_PASSW = "rs.cons.efl.passw";
	
	public static final String REST_DEV_AMB = "rs.dev.jks";
	public static final String REST_DEV_JKS_NAME = "rs.dev.jks.name";
	public static final String REST_DEV_JKS_PASSW = "rs.dev.jks.passw";
	
	public static final String HABILITAR_R1 = "pepper.flag.r1";
	
	public static final String REST_ENVIO_AUDITORIA_ENDPOINT = "rs.envio.auditoria.endpoint";
	public static final String REST_ENVIO_AUDITORIA_RESOURCE = "rs.envio.auditoria.resource";
	public static final String REST_ENVIO_AUDITORIA_USUARIO = "rs.envio.auditoria.usuario";
	public static final String REST_ENVIO_AUDITORIA_PASSW = "rs.envio.auditoria.passw";
	
	public static final String REST_VAL_RUC_ENDPOINT = "rs.val.ruc.endpoint";
	public static final String REST_VAL_RUC_RESOURCE = "rs.val.ruc.resource";
	public static final String REST_VAL_RUC_USUARIO = "rs.val.ruc.usuario";
	public static final String REST_VAL_RUC_PASSW = "rs.val.ruc.passw";
	
	public static final String TIPO_AMBIENTE = "pepper.tipo.ambiente";
	
	public static final String REST_VAL_RECAPTCHA_ENDPOINT = "rs.validar.recaptcha.endpoint";
	public static final String REST_VAL_RECAPTCHA_KEY = "rs.validar.recaptcha.key";
	public static final String REST_VAL_RECAPTCHA_SECRET = "rs.validar.recaptcha.secret";
	
	public static final String REST_FHETREGA_ENDPOINT = "rs.fhentrega.endpoint";
	public static final String REST_FHETREGA_RESOURCE = "rs.fhentrega.resource";
	public static final String REST_FHETREGA_USUARIO = "rs.fhentrega.usuario";
	public static final String REST_FHETREGA_PASSW = "rs.fhentrega.passw";
	
	public static final String FLAG_CAMPANIA_V2 = "rs.campania.flag.v2";
	
	public static final String REST_FWK4_HOST = "rest.fwk4.host";
	public static final String REST_API_HOST = "rest.api.host";
	public static final String REST_USUARIO = "rest.usuario";
	public static final String REST_PSSWRD = "rest.password";
	
	private PortalKeys() {
		
	}
	
	

}