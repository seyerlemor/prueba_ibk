/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import pe.com.ibk.pepper.model.TblMaestro;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TblMaestro in entity cache.
 *
 * @author Interbank
 * @see TblMaestro
 * @generated
 */
public class TblMaestroCacheModel implements CacheModel<TblMaestro>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{idMaestro=");
		sb.append(idMaestro);
		sb.append(", nombreComercio=");
		sb.append(nombreComercio);
		sb.append(", idHash=");
		sb.append(idHash);
		sb.append(", tipoTarjeta=");
		sb.append(tipoTarjeta);
		sb.append(", urlImagen=");
		sb.append(urlImagen);
		sb.append(", topeOtp=");
		sb.append(topeOtp);
		sb.append(", topeEquifax=");
		sb.append(topeEquifax);
		sb.append(", codigoSeguridad=");
		sb.append(codigoSeguridad);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TblMaestro toEntityModel() {
		TblMaestroImpl tblMaestroImpl = new TblMaestroImpl();

		tblMaestroImpl.setIdMaestro(idMaestro);

		if (nombreComercio == null) {
			tblMaestroImpl.setNombreComercio(StringPool.BLANK);
		}
		else {
			tblMaestroImpl.setNombreComercio(nombreComercio);
		}

		if (idHash == null) {
			tblMaestroImpl.setIdHash(StringPool.BLANK);
		}
		else {
			tblMaestroImpl.setIdHash(idHash);
		}

		if (tipoTarjeta == null) {
			tblMaestroImpl.setTipoTarjeta(StringPool.BLANK);
		}
		else {
			tblMaestroImpl.setTipoTarjeta(tipoTarjeta);
		}

		if (urlImagen == Long.MIN_VALUE) {
			tblMaestroImpl.setUrlImagen(null);
		}
		else {
			tblMaestroImpl.setUrlImagen(new Date(urlImagen));
		}

		if (topeOtp == null) {
			tblMaestroImpl.setTopeOtp(StringPool.BLANK);
		}
		else {
			tblMaestroImpl.setTopeOtp(topeOtp);
		}

		if (topeEquifax == null) {
			tblMaestroImpl.setTopeEquifax(StringPool.BLANK);
		}
		else {
			tblMaestroImpl.setTopeEquifax(topeEquifax);
		}

		if (codigoSeguridad == null) {
			tblMaestroImpl.setCodigoSeguridad(StringPool.BLANK);
		}
		else {
			tblMaestroImpl.setCodigoSeguridad(codigoSeguridad);
		}

		tblMaestroImpl.resetOriginalValues();

		return tblMaestroImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idMaestro = objectInput.readInt();
		nombreComercio = objectInput.readUTF();
		idHash = objectInput.readUTF();
		tipoTarjeta = objectInput.readUTF();
		urlImagen = objectInput.readLong();
		topeOtp = objectInput.readUTF();
		topeEquifax = objectInput.readUTF();
		codigoSeguridad = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idMaestro);

		if (nombreComercio == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreComercio);
		}

		if (idHash == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idHash);
		}

		if (tipoTarjeta == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoTarjeta);
		}

		objectOutput.writeLong(urlImagen);

		if (topeOtp == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(topeOtp);
		}

		if (topeEquifax == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(topeEquifax);
		}

		if (codigoSeguridad == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoSeguridad);
		}
	}

	public int idMaestro;
	public String nombreComercio;
	public String idHash;
	public String tipoTarjeta;
	public long urlImagen;
	public String topeOtp;
	public String topeEquifax;
	public String codigoSeguridad;
}