/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ExpedienteLocalService}.
 *
 * @author Interbank
 * @see ExpedienteLocalService
 * @generated
 */
public class ExpedienteLocalServiceWrapper implements ExpedienteLocalService,
	ServiceWrapper<ExpedienteLocalService> {
	public ExpedienteLocalServiceWrapper(
		ExpedienteLocalService expedienteLocalService) {
		_expedienteLocalService = expedienteLocalService;
	}

	/**
	* Adds the expediente to the database. Also notifies the appropriate model listeners.
	*
	* @param expediente the expediente
	* @return the expediente that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Expediente addExpediente(
		pe.com.ibk.pepper.model.Expediente expediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.addExpediente(expediente);
	}

	/**
	* Creates a new expediente with the primary key. Does not add the expediente to the database.
	*
	* @param idExpediente the primary key for the new expediente
	* @return the new expediente
	*/
	@Override
	public pe.com.ibk.pepper.model.Expediente createExpediente(
		long idExpediente) {
		return _expedienteLocalService.createExpediente(idExpediente);
	}

	/**
	* Deletes the expediente with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idExpediente the primary key of the expediente
	* @return the expediente that was removed
	* @throws PortalException if a expediente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Expediente deleteExpediente(
		long idExpediente)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.deleteExpediente(idExpediente);
	}

	/**
	* Deletes the expediente from the database. Also notifies the appropriate model listeners.
	*
	* @param expediente the expediente
	* @return the expediente that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Expediente deleteExpediente(
		pe.com.ibk.pepper.model.Expediente expediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.deleteExpediente(expediente);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _expedienteLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente fetchExpediente(long idExpediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.fetchExpediente(idExpediente);
	}

	/**
	* Returns the expediente with the primary key.
	*
	* @param idExpediente the primary key of the expediente
	* @return the expediente
	* @throws PortalException if a expediente with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Expediente getExpediente(long idExpediente)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.getExpediente(idExpediente);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the expedientes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ExpedienteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of expedientes
	* @param end the upper bound of the range of expedientes (not inclusive)
	* @return the range of expedientes
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.Expediente> getExpedientes(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.getExpedientes(start, end);
	}

	/**
	* Returns the number of expedientes.
	*
	* @return the number of expedientes
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getExpedientesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.getExpedientesCount();
	}

	/**
	* Updates the expediente in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param expediente the expediente
	* @return the expediente that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Expediente updateExpediente(
		pe.com.ibk.pepper.model.Expediente expediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.updateExpediente(expediente);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _expedienteLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_expedienteLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _expedienteLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente registrarExpediente(
		pe.com.ibk.pepper.model.Expediente expediente)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _expedienteLocalService.registrarExpediente(expediente);
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente buscarExpedienteporId(
		long idExpediente) {
		return _expedienteLocalService.buscarExpedienteporId(idExpediente);
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente buscarPorDNI_Fecha(
		java.lang.String dni, java.util.Date fecha) {
		return _expedienteLocalService.buscarPorDNI_Fecha(dni, fecha);
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente buscarPorDNI_Periodo(
		java.lang.String dni, java.lang.String periodo) {
		return _expedienteLocalService.buscarPorDNI_Periodo(dni, periodo);
	}

	@Override
	public pe.com.ibk.pepper.model.Expediente obtenerExpedienteDNIFecha(
		java.lang.String documento, java.lang.String periodo) {
		return _expedienteLocalService.obtenerExpedienteDNIFecha(documento,
			periodo);
	}

	@Override
	public java.util.List<java.lang.Object[]> obtenerBusqueda(
		java.lang.Integer filas, java.lang.Integer pagina,
		java.lang.String fechaInicio, java.lang.String fechaFinal,
		java.lang.String estado) {
		return _expedienteLocalService.obtenerBusqueda(filas, pagina,
			fechaInicio, fechaFinal, estado);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ExpedienteLocalService getWrappedExpedienteLocalService() {
		return _expedienteLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedExpedienteLocalService(
		ExpedienteLocalService expedienteLocalService) {
		_expedienteLocalService = expedienteLocalService;
	}

	@Override
	public ExpedienteLocalService getWrappedService() {
		return _expedienteLocalService;
	}

	@Override
	public void setWrappedService(ExpedienteLocalService expedienteLocalService) {
		_expedienteLocalService = expedienteLocalService;
	}

	private ExpedienteLocalService _expedienteLocalService;
}