<%@ include file="/html/init.jsp"%>

<%@ page contentType="text/html; charset=UTF-8" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="resultadoTC" var="resultadoTC" />

<div class="lq-container">
        <div class="lh-ff">
          <div class="lh-ff-block lh-ff-block--active">
            <div class="lq-row">
            	${commerceText.v201001}
              <div class="lh-ff-content-col">
                      <div class="lh-ff-section lh-tarjetas">
                        <div class="lh-ff-section__wrapper">
                          <div class="lh-ff-section__text">
                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 text-center">${commerceText.v201002}</h3>
                            <p class="lh-typo__p1 text-center">${commerceText.v201003}</p>
                            <form:form   action="${resultadoTC}" autocomplete="off" class="lh-ff-form" novalidate="novalidate" >
                              <div class="lh-tarjetas__carrusel">
                              <c:forEach items="${offers}" var="offer" varStatus="ob">
                              <div class="lh-tarjetas__carrusel__slide" data-card-id="id${ob.index+1}" data-table-card="[{&quot;idBrand&quot;:&quot;${ob.index+1}&quot;,&quot;title&quot;:&quot;Beneficios&quot;,&quot;detail&quot;:&quot;&lt;&sol;br&gt;&lt;strong&gt;Cuponera de descuentos&lt;&sol;strong&gt;&lt;&sol;br&gt;En cines, restaurantes y más.&lt;&sol;br&gt;&lt;&sol;br&gt;&lt;strong&gt;Hasta S/100 de devolución mensual&lt;&sol;strong&gt;&lt;&sol;br&gt;En supermercados.&quot;},{&quot;title&quot;:&quot;L&iacute;nea de Cr&eacute;dito&quot;,&quot;detail&quot;:&quot;&lt;strong&gt;S/${offer.formatCreditLine}&lt;&sol;strong&gt;&quot;},{&quot;title&quot;:&quot;Principales seguros&quot;,&quot;detail&quot;:&quot;Compra protegida y garantía extendida para los productos que compres con la tarjeta.&quot;},{&quot;title&quot;:&quot;Membresía&quot;,&quot;detail&quot;:&quot;S/${offer.memberShip} &lt;&sol;br&gt;&lt;strong&gt;*Gratis&lt;&sol;strong&gt; si consumes todos los meses.&quot;},{&quot;title&quot;:&quot;Seguro de desgravamen&quot;,&quot;detail&quot;:&quot;S/${offer.safeDeduction}&lt;&sol;br&gt;Evita que tus familiares hereden la deuda de tu tarjeta.&quot;},{&quot;title&quot;:&quot;Tasa Efectiva Anual(TEA)&quot;,&quot;detail&quot;:&quot;${offer.tea}%&quot;}]">
<%--                                 <div class="lh-tarjetas__carrusel__slide" data-card-id="id${ob.index+1}" data-table-card="${offer.tcDetail}"> --%>
                                  <div class="card-wrap text-center">
                                    <div class="card-wrap__image">
                                      <picture>
                                        <source media="(min-width: 961px)" srcset="/pepper-v2-theme/images/tc/normal/${offer.brandProduct}${offer.typeProduct}.jpg">
                                        <source media="(min-width: 961px) 2x" srcset="/pepper-v2-theme/images/tc/normal/${offer.brandProduct}${offer.typeProduct}.jpg"><img class="lazyload" src="/pepper-v2-theme/images/tc/normal/${offer.brandProduct}${offer.typeProduct}.jpg" alt="#">
                                      </picture>
                                    </div>
                                    <div class="lh-typo__thirdtitle lh-typo__thirdtitle--2">${offer.descriptionType}</div><span class="lh-link lh-link--cta2 show-table">Ver detalle de tarjeta<i class="arrow">
                                        <svg width="9" height="6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path fill-rule="evenodd" clip-rule="evenodd" d="M.342.247a1 1 0 0 1 1.41.095L4.5 3.482 7.247.341a1 1 0 1 1 1.506 1.317l-3.5 4a1 1 0 0 1-1.506 0l-3.5-4A1 1 0 0 1 .342.247z" fill="#00A94F"></path>
                                        </svg></i></span>
                                  </div>
                                </div>
                              </c:forEach>
                              </div>
                              <div class="lh-tarjetas__table"></div>
                              <template id="tableTpl">
                                <div class="lh-tarjetas__table__row">
                                  <div class="lh-tarjetas__table__heading text-center"><span class="lh-typo__caps">{{title}}</span></div>
                                  <div class="lh-tarjetas__table__content text-center"><span class="lh-typo__p3">{{detail}}</span></div>
                                  <input type="hidden" id="idBrand" value="{{idBrand}}" name="idBrand" />
                                </div>
                              </template>
                              <div class="lh-ff-group lh-ff-group--float text-center">
                                <div class="lh-ff-group__item">
                                  <input id="selectedId" type="hidden">
                                  <button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit"><span>${commerceText.v201004}</span></button>
                                </div>
                              </div>
                                    <div class="lh-form__error">
                                      <div class="lh-typo__p2 lh-form__error-message" data-message="${renderRequest.preferences.getValue('generalErrorMassage','')}"></div>
                                    </div>
                            </form:form> 
                          </div>
                        </div>
                      </div>
                    </div>
            </div>
          </div>
        </div>
      </div>   
            