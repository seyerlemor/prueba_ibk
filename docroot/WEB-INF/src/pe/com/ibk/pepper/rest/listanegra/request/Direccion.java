
package pe.com.ibk.pepper.rest.listanegra.request;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Direccion {

    private String codigoDepartamento;
    private String codigoProvincia;
    private String codigoDistrito;
    private String codigoTipoVia;
    private String nombreVia;
    private String numeroManzana;
    private String codigoUrbanizacion;
    private String interiorDepartamento;
    private String lote;
    private String referencia;
    private String sector;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Direccion() {
    	super();
    }

    /**
     * 
     * @param sector
     * @param codigoUrbanizacion
     * @param lote
     * @param nombreVia
     * @param codigoProvincia
     * @param codigoTipoVia
     * @param codigoDepartamento
     * @param interiorDepartamento
     * @param numeroManzana
     * @param codigoDistrito
     * @param referencia
     */
    public Direccion(String codigoDepartamento, String codigoProvincia, String codigoDistrito, String codigoTipoVia, String nombreVia, String numeroManzana, String codigoUrbanizacion, String interiorDepartamento, String lote, String referencia, String sector) {
        super();
        this.codigoDepartamento = codigoDepartamento;
        this.codigoProvincia = codigoProvincia;
        this.codigoDistrito = codigoDistrito;
        this.codigoTipoVia = codigoTipoVia;
        this.nombreVia = nombreVia;
        this.numeroManzana = numeroManzana;
        this.codigoUrbanizacion = codigoUrbanizacion;
        this.interiorDepartamento = interiorDepartamento;
        this.lote = lote;
        this.referencia = referencia;
        this.sector = sector;
    }
    
    public Direccion(String codigoDepartamento, String codigoProvincia, String codigoDistrito) {
        super();
        this.codigoDepartamento = codigoDepartamento;
        this.codigoProvincia = codigoProvincia;
        this.codigoDistrito = codigoDistrito;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    public String getCodigoProvincia() {
        return codigoProvincia;
    }

    public void setCodigoProvincia(String codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }

    public String getCodigoDistrito() {
        return codigoDistrito;
    }

    public void setCodigoDistrito(String codigoDistrito) {
        this.codigoDistrito = codigoDistrito;
    }

    public String getCodigoTipoVia() {
        return codigoTipoVia;
    }

    public void setCodigoTipoVia(String codigoTipoVia) {
        this.codigoTipoVia = codigoTipoVia;
    }

    public String getNombreVia() {
        return nombreVia;
    }

    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    public String getNumeroManzana() {
        return numeroManzana;
    }

    public void setNumeroManzana(String numeroManzana) {
        this.numeroManzana = numeroManzana;
    }

    public String getCodigoUrbanizacion() {
        return codigoUrbanizacion;
    }

    public void setCodigoUrbanizacion(String codigoUrbanizacion) {
        this.codigoUrbanizacion = codigoUrbanizacion;
    }

    public String getInteriorDepartamento() {
        return interiorDepartamento;
    }

    public void setInteriorDepartamento(String interiorDepartamento) {
        this.interiorDepartamento = interiorDepartamento;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("codigoDepartamento", codigoDepartamento).append("codigoProvincia", codigoProvincia).append("codigoDistrito", codigoDistrito).append("codigoTipoVia", codigoTipoVia).append("nombreVia", nombreVia).append("numeroManzana", numeroManzana).append("codigoUrbanizacion", codigoUrbanizacion).append("interiorDepartamento", interiorDepartamento).append("lote", lote).append("referencia", referencia).append("sector", sector).toString();
    }

}
