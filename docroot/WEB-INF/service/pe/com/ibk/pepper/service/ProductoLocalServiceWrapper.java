/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pe.com.ibk.pepper.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProductoLocalService}.
 *
 * @author Interbank
 * @see ProductoLocalService
 * @generated
 */
public class ProductoLocalServiceWrapper implements ProductoLocalService,
	ServiceWrapper<ProductoLocalService> {
	public ProductoLocalServiceWrapper(
		ProductoLocalService productoLocalService) {
		_productoLocalService = productoLocalService;
	}

	/**
	* Adds the producto to the database. Also notifies the appropriate model listeners.
	*
	* @param producto the producto
	* @return the producto that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Producto addProducto(
		pe.com.ibk.pepper.model.Producto producto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.addProducto(producto);
	}

	/**
	* Creates a new producto with the primary key. Does not add the producto to the database.
	*
	* @param idProducto the primary key for the new producto
	* @return the new producto
	*/
	@Override
	public pe.com.ibk.pepper.model.Producto createProducto(long idProducto) {
		return _productoLocalService.createProducto(idProducto);
	}

	/**
	* Deletes the producto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProducto the primary key of the producto
	* @return the producto that was removed
	* @throws PortalException if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Producto deleteProducto(long idProducto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.deleteProducto(idProducto);
	}

	/**
	* Deletes the producto from the database. Also notifies the appropriate model listeners.
	*
	* @param producto the producto
	* @return the producto that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Producto deleteProducto(
		pe.com.ibk.pepper.model.Producto producto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.deleteProducto(producto);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _productoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public pe.com.ibk.pepper.model.Producto fetchProducto(long idProducto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.fetchProducto(idProducto);
	}

	/**
	* Returns the producto with the primary key.
	*
	* @param idProducto the primary key of the producto
	* @return the producto
	* @throws PortalException if a producto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Producto getProducto(long idProducto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.getProducto(idProducto);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the productos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link pe.com.ibk.pepper.model.impl.ProductoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of productos
	* @param end the upper bound of the range of productos (not inclusive)
	* @return the range of productos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<pe.com.ibk.pepper.model.Producto> getProductos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.getProductos(start, end);
	}

	/**
	* Returns the number of productos.
	*
	* @return the number of productos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getProductosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.getProductosCount();
	}

	/**
	* Updates the producto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param producto the producto
	* @return the producto that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public pe.com.ibk.pepper.model.Producto updateProducto(
		pe.com.ibk.pepper.model.Producto producto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.updateProducto(producto);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _productoLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_productoLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _productoLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public pe.com.ibk.pepper.model.Producto registrarProducto(
		pe.com.ibk.pepper.model.Producto producto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productoLocalService.registrarProducto(producto);
	}

	@Override
	public pe.com.ibk.pepper.model.Producto buscarProductoClienteporId(
		long idProducto) {
		return _productoLocalService.buscarProductoClienteporId(idProducto);
	}

	@Override
	public pe.com.ibk.pepper.model.Producto buscarPorExpediente(
		long idExpediente) {
		return _productoLocalService.buscarPorExpediente(idExpediente);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ProductoLocalService getWrappedProductoLocalService() {
		return _productoLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedProductoLocalService(
		ProductoLocalService productoLocalService) {
		_productoLocalService = productoLocalService;
	}

	@Override
	public ProductoLocalService getWrappedService() {
		return _productoLocalService;
	}

	@Override
	public void setWrappedService(ProductoLocalService productoLocalService) {
		_productoLocalService = productoLocalService;
	}

	private ProductoLocalService _productoLocalService;
}