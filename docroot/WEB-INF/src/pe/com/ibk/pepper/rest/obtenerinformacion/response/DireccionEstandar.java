
package pe.com.ibk.pepper.rest.obtenerinformacion.response;


public class DireccionEstandar {

    private String tipoVia;
    private String descripcionTipoVia;
    private String nombreVia;
    private String numero;
    private String manzana;
    private String pisoLote;
    private String interior;
    private String urbanizacion;
    private String referencia;

    public String getTipoVia() {
        return tipoVia;
    }

    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    public String getNombreVia() {
        return nombreVia;
    }

    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public String getPisoLote() {
        return pisoLote;
    }

    public void setPisoLote(String pisoLote) {
        this.pisoLote = pisoLote;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public String getUrbanizacion() {
        return urbanizacion;
    }

    public void setUrbanizacion(String urbanizacion) {
        this.urbanizacion = urbanizacion;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
  
	public String getDescripcionTipoVia() {
		return descripcionTipoVia;
	}

	public void setDescripcionTipoVia(String descripcionTipoVia) {
		this.descripcionTipoVia = descripcionTipoVia;
	}

	@Override
	public String toString() {
		return "DireccionEstandar [tipoVia=" + tipoVia + ", nombreVia="
				+ nombreVia + ", numero=" + numero + ", manzana=" + manzana
				+ ", pisoLote=" + pisoLote + ", interior=" + interior
				+ ", urbanizacion=" + urbanizacion + ", referencia="
				+ referencia + "]";
	}

}
