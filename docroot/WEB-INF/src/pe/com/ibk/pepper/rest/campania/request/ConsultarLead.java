package pe.com.ibk.pepper.rest.campania.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ConsultarLead {

    private String tipoBusqueda;
    private String tipoDocumento;
    private String numeroDocumento;
    private String flagObtenerDatosOfertas;
    private String flagNuevoBus;
    private String flagConsultaDirecta;
    private String codigoCanal;
    
    @JsonCreator
	public ConsultarLead(String tipoBusqueda, String tipoDocumento,
			String numeroDocumento, String flagObtenerDatosOfertas,
			String flagNuevoBus, String flagConsultaDirecta, String codigoCanal) {
		super();
		this.tipoBusqueda = tipoBusqueda;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.flagObtenerDatosOfertas = flagObtenerDatosOfertas;
		this.flagNuevoBus = flagNuevoBus;
		this.flagConsultaDirecta = flagConsultaDirecta;
		this.codigoCanal = codigoCanal;
	}

    @JsonProperty("tipoBusqueda")
	public String getTipoBusqueda() {
		return tipoBusqueda;
	}

	public void setTipoBusqueda(String tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}

	@JsonProperty("tipoDocumento")
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@JsonProperty("numeroDocumento")
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@JsonProperty("flagObtenerDatosOfertas")
	public String getFlagObtenerDatosOfertas() {
		return flagObtenerDatosOfertas;
	}

	public void setFlagObtenerDatosOfertas(String flagObtenerDatosOfertas) {
		this.flagObtenerDatosOfertas = flagObtenerDatosOfertas;
	}

	@JsonProperty("flagNuevoBus")
	public String getFlagNuevoBus() {
		return flagNuevoBus;
	}

	public void setFlagNuevoBus(String flagNuevoBus) {
		this.flagNuevoBus = flagNuevoBus;
	}

	@JsonProperty("flagConsultaDirecta")
	public String getFlagConsultaDirecta() {
		return flagConsultaDirecta;
	}

	public void setFlagConsultaDirecta(String flagConsultaDirecta) {
		this.flagConsultaDirecta = flagConsultaDirecta;
	}

	@JsonProperty("codigoCanal")
	public String getCodigoCanal() {
		return codigoCanal;
	}

	public void setCodigoCanal(String codigoCanal) {
		this.codigoCanal = codigoCanal;
	}

    
}
