<%@ include file="/html/init.jsp"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="aperturaVentaTC" var="aperturaVentaTC" />
	
<div class="lq-container">
        <div class="lh-ff">
                <div class="lh-ff-block lh-ff-block--active">
                  <div class="lq-row">
                    <div class="lh-ff-title-col lh-ff-title-col--trail">
                            <div class="lh-ff-trail">
                              <h2 class="lh-ff-trail__title lh-typo__sub lh-typo__sub--1 text-center">Valida tus datos</h2>
                              <ol class="lh-ff-trail__list">
                                <li class="done">
                                  <div class="item" data-step="1"></div>
                                  <h4 class="lh-typo__label">Empieza tu solicitud</h4>
                                </li>
                                <li class="done">
                                  <div class="item" data-step="2"></div>
                                  <h4 class="lh-typo__label">Conoce tu tarjeta ideal</h4>
                                </li>
                                <li class="active">
                                  <div class="item" data-step="3"></div>
                                  <h4 class="lh-typo__label">Identifícate</h4>
                                </li>
                                <li class="done">
                                  <div class="item" data-step="4"></div>
                                  <h4 class="lh-typo__label">Conoce tu tarjeta ideal</h4>
                                </li>
                                <li class="active">
                                  <div class="item" data-step="5"></div>
                                  <h4 class="lh-typo__label">Valida tus datos</h4>
                                </li>
                              </ol>
                            </div>
                    </div>
                    <div class="lh-ff-content-col">
                      <div class="lh-ff-section lh-ff-section--notification">
                        <div class="lh-ff-section__wrapper">
                          <div class="lh-ff-section__text">
                            <h3 class="lh-typo__thirdtitle lh-typo__thirdtitle--1 lh-ff-section__text__title">Hola ${pasoBean.clienteBean.firstname},</h3>
                            	<p class="lh-typo__p1">Te hemos enviado un código de verificación al correo electrónico  ${requestScope.formatEmailAddress}.</p>
                            	<p class="lh-typo__p1">Te hemos enviado un código de verificación por sms al celular ${requestScope.formatCellNumber}.</p>
							<form:form   action="${aperturaVentaTC}" autocomplete="off" commandName="clienteBean" class="lh-ff-form" novalidate="novalidate" >
                                    <div class="lh-ff-group">
                                                  <div class="lh-ff-group__item lh-ff-group__item--sm">
                                                    <label class="lh-ff__label" for="validationCode">Ingresa el código
                                                    </label>
                                                                <div class="lh-ff-group__field">
                                                                        <input class="lh-input" id="validationCode" maxlength="8" name="validationCode" data-allowed-keys="alphanumeric" data-validate-required data-validate-required-message="Ingrese el código" data-validate-length="8" data-validate-length-message="El código debe contar con 8 dígitos" type="text">
                                                                </div>
                                                  </div>
                                    </div>
                              <div class="lh-ff-group">
                                <div class="lh-ff-group__item lh-ff-group__item--buttons lh-flex--sm">
                                  <button class="lh-btn lh-btn--primary lh-btn--fill lh-btn--block-xxs lh-btn-submit"><span>Listo</span></button>
                                  <p class="lh-typo__p3 lh-flex-vertical-start" id="block-msg-code" style="opacity:0"><span class="lh-color-light-green" id="tryMsg" data-msg-end="Llegaste al límite de envíos por hoy.">¿No has recibido el código? <br></span><a class="lh-link lh-link--bold lh-link--arrow lh-link--underline" id="sendCode" data-action="form-otp--validacion-codigo.json" data-attempts="3">Reenviar código</a><a class="lh-link lh-link--bold lh-link--arrow lh-link--underline lh-hide" id="identificate" data-action="form-otp--validar-otra-forma.json">Identifícate de otra manera</a><span class="lh-color-gray-dark lh-hide" id="countdown" data-counter-time="5">Solicita uno nuevo en <span class="nowrap">01:00</span></span></p>
                                </div>
                              </div>
                                    <div class="lh-form__error">
                                      <div class="lh-typo__p2 lh-form__error-message" data-message="Hubo un problema en el servidor. Por favor, inténtalo nuevamente."></div>
                                    </div>
                            </form:form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>
	
