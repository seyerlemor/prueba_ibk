
package pe.com.ibk.pepper.rest.campania.response;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Additional implements Serializable
{

    @SerializedName("item1")
    @Expose
    private String item1;
    @SerializedName("item2")
    @Expose
    private String item2;
    @SerializedName("item3")
    @Expose
    private String item3;
    @SerializedName("item4")
    @Expose
    private String item4;
    @SerializedName("item5")
    @Expose
    private String item5;
    @SerializedName("item6")
    @Expose
    private String item6;
    @SerializedName("item7")
    @Expose
    private String item7;
    @SerializedName("item8")
    @Expose
    private String item8;
    @SerializedName("item9")
    @Expose
    private String item9;
    @SerializedName("item10")
    @Expose
    private String item10;
    @SerializedName("item11")
    @Expose
    private String item11;
    @SerializedName("item12")
    @Expose
    private String item12;
    @SerializedName("item13")
    @Expose
    private String item13;
    @SerializedName("item14")
    @Expose
    private String item14;
    @SerializedName("item15")
    @Expose
    private String item15;
    private final static long serialVersionUID = -2216903343578438435L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Additional() {
    }

    /**
     * 
     * @param item2
     * @param item1
     * @param item15
     * @param item14
     * @param item13
     * @param item12
     * @param item11
     * @param item10
     * @param item8
     * @param item7
     * @param item9
     * @param item4
     * @param item3
     * @param item6
     * @param item5
     */
    public Additional(String item1, String item2, String item3, String item4, String item5, String item6, String item7, String item8, String item9, String item10, String item11, String item12, String item13, String item14, String item15) {
        super();
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;
        this.item5 = item5;
        this.item6 = item6;
        this.item7 = item7;
        this.item8 = item8;
        this.item9 = item9;
        this.item10 = item10;
        this.item11 = item11;
        this.item12 = item12;
        this.item13 = item13;
        this.item14 = item14;
        this.item15 = item15;
    }

    public String getItem1() {
        return item1;
    }

    public void setItem1(String item1) {
        this.item1 = item1;
    }

    public Additional withItem1(String item1) {
        this.item1 = item1;
        return this;
    }

    public String getItem2() {
        return item2;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }

    public Additional withItem2(String item2) {
        this.item2 = item2;
        return this;
    }

    public String getItem3() {
        return item3;
    }

    public void setItem3(String item3) {
        this.item3 = item3;
    }

    public Additional withItem3(String item3) {
        this.item3 = item3;
        return this;
    }

    public String getItem4() {
        return item4;
    }

    public void setItem4(String item4) {
        this.item4 = item4;
    }

    public Additional withItem4(String item4) {
        this.item4 = item4;
        return this;
    }

    public String getItem5() {
        return item5;
    }

    public void setItem5(String item5) {
        this.item5 = item5;
    }

    public Additional withItem5(String item5) {
        this.item5 = item5;
        return this;
    }

    public String getItem6() {
        return item6;
    }

    public void setItem6(String item6) {
        this.item6 = item6;
    }

    public Additional withItem6(String item6) {
        this.item6 = item6;
        return this;
    }

    public String getItem7() {
        return item7;
    }

    public void setItem7(String item7) {
        this.item7 = item7;
    }

    public Additional withItem7(String item7) {
        this.item7 = item7;
        return this;
    }

    public String getItem8() {
        return item8;
    }

    public void setItem8(String item8) {
        this.item8 = item8;
    }

    public Additional withItem8(String item8) {
        this.item8 = item8;
        return this;
    }

    public String getItem9() {
        return item9;
    }

    public void setItem9(String item9) {
        this.item9 = item9;
    }

    public Additional withItem9(String item9) {
        this.item9 = item9;
        return this;
    }

    public String getItem10() {
        return item10;
    }

    public void setItem10(String item10) {
        this.item10 = item10;
    }

    public Additional withItem10(String item10) {
        this.item10 = item10;
        return this;
    }

    public String getItem11() {
        return item11;
    }

    public void setItem11(String item11) {
        this.item11 = item11;
    }

    public Additional withItem11(String item11) {
        this.item11 = item11;
        return this;
    }

    public String getItem12() {
        return item12;
    }

    public void setItem12(String item12) {
        this.item12 = item12;
    }

    public Additional withItem12(String item12) {
        this.item12 = item12;
        return this;
    }

    public String getItem13() {
        return item13;
    }

    public void setItem13(String item13) {
        this.item13 = item13;
    }

    public Additional withItem13(String item13) {
        this.item13 = item13;
        return this;
    }

    public String getItem14() {
        return item14;
    }

    public void setItem14(String item14) {
        this.item14 = item14;
    }

    public Additional withItem14(String item14) {
        this.item14 = item14;
        return this;
    }

    public String getItem15() {
        return item15;
    }

    public void setItem15(String item15) {
        this.item15 = item15;
    }

    public Additional withItem15(String item15) {
        this.item15 = item15;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("item1", item1).append("item2", item2).append("item3", item3).append("item4", item4).append("item5", item5).append("item6", item6).append("item7", item7).append("item8", item8).append("item9", item9).append("item10", item10).append("item11", item11).append("item12", item12).append("item13", item13).append("item14", item14).append("item15", item15).toString();
    }

}
