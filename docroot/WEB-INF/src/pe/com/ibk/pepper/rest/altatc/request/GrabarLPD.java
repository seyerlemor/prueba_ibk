
package pe.com.ibk.pepper.rest.altatc.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GrabarLPD {

    private String tipoConsentimiento;
    private String flagCliente;
    private String flagLPD;

    /**
     * No args constructor for use in serialization
     * 
     */
    public GrabarLPD() {
    }

    /**
     * 
     * @param flagLPD
     * @param tipoConsentimiento
     * @param flagCliente
     */
    public GrabarLPD(String tipoConsentimiento, String flagCliente, String flagLPD) {
        super();
        this.tipoConsentimiento = tipoConsentimiento;
        this.flagCliente = flagCliente;
        this.flagLPD = flagLPD;
    }

    public String getTipoConsentimiento() {
        return tipoConsentimiento;
    }

    public void setTipoConsentimiento(String tipoConsentimiento) {
        this.tipoConsentimiento = tipoConsentimiento;
    }

    public String getFlagCliente() {
        return flagCliente;
    }

    public void setFlagCliente(String flagCliente) {
        this.flagCliente = flagCliente;
    }

    public String getFlagLPD() {
        return flagLPD;
    }

    public void setFlagLPD(String flagLPD) {
        this.flagLPD = flagLPD;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tipoConsentimiento", tipoConsentimiento).append("flagCliente", flagCliente).append("flagLPD", flagLPD).toString();
    }

}
